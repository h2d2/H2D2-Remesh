//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: MGElementL3.hpp
// Classe : MGElementL3
//************************************************************************
// 28-07-2003  Maxime Derenne      Version initiale
// 04-08-2003  Maxime Derenne      Transfert de méthode inline
// 25-10-2003  Olivier Kaczor      Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor      Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor      Rendre compilable sur GCC
// 16-02-2004  Maude Giasson       TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_L3_HPP_DEJA_INCLU
#define MGELEMENT_L3_HPP_DEJA_INCLU

#include "GOEpsilon.h"

#include "MGConst.h"
#include "MGElementAlgorithme.h"
#include "MGConstElementAlgorithme.h"

//**************************************************************
// Description:
//   Constructeur par défaut, sans argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementL3<TTTraits>::MGElementL3()
: TCSelf::TCElement(0),
  noeud1P(NUL),
  noeud2P(NUL),
  noeud3P(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementL3 ()


//**************************************************************
// Description:
//   Initialise le numéro de l'élément ainsi que ses trois noeuds
//   correspondants.
//
// Entrée:
//   EntierN element     : le numéro de l'élément qui relie les trois noeuds
//   TCNoeudPP &noeuds   : Référence sur un tableau de pointeur sur des noeuds.
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementL3<TTTraits>::MGElementL3(EntierN element, 
  typename MGElementL3<TTTraits>::TCNoeudPP noeudsPP)
: TCSelf::TCElement(element),
  noeud1P(noeudsPP[0]),
  noeud2P(noeudsPP[1]),
  noeud3P(noeudsPP[2])
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementL3


//**************************************************************
// Description:
//   ne fait rien car aucune allocation dynamique n'a été faite
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementL3<TTTraits>::~MGElementL3()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
} // ~MGElementL3 ()

//************************************************************************
// Sommaire: Compare les connectivités et le type d'élément.
//
// Description: Compare les connectivités et le type d'élément.  Retourne VRAI
//              si le type d'élément est inférieur ou si c'est le mème type
//              d'élément, mais que la connectivité  soit inférieure.  Pour que
//              la connectivité soit considérée inférieure, il faut qu'une fois
//              les NoNoeuds ordonnés des deux éléments cet élément-ci (this) possède
//              un noeud avec un # inférieur dans les n premiers noeuds comparés
//              à ceux de l'autre élément.
//
// Entrée:
//   ConstMGElementP elem          : Element à comparer
//
// Sortie:
//   Booleen                           : VRAI ou FAUX
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
Booleen MGElementL3<TTTraits>::compareConnectiviteInferieure(typename MGElementL3<TTTraits>::ConstTCElementP elemP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef MGElementL3<typename MGElementL3<TTTraits>::TCTraits> const * ConstTCSelfP;
   Booleen reponse = FAUX;

   if (this->reqType() != elemP->reqType())
   {
      reponse = this->reqType() < elemP->reqType();
   }
   else
   {
      typename MGElementL3<TTTraits>::TCNoeudP noNo1P, noNo2P, noNo3P;
      noNo1P = noeud1P;
      noNo2P = noeud2P;
      noNo3P = noeud3P;

      if (noNo2P < noNo1P)
      {
         const typename MGElementL3<TTTraits>::TCNoeudP noTmpP = noNo1P;
         noNo1P = noNo2P;
         noNo2P = noTmpP;
      }
      if (noNo3P < noNo1P)
      {
         const typename MGElementL3<TTTraits>::TCNoeudP noTmpP = noNo1P;
         noNo1P = noNo3P;
         noNo3P = noNo2P;
         noNo2P = noTmpP;
      }
      else if (noNo3P < noNo2P)
      {
         const typename MGElementL3<TTTraits>::TCNoeudP noTmpP = noNo2P;
         noNo2P = noNo3P;
         noNo3P = noTmpP;
      }

      typename MGElementL3<TTTraits>::TCNoeudP elemNoNo1P, elemNoNo2P, elemNoNo3P;

      ASSERTION(dynamic_cast<ConstTCSelfP>(elemP) != NUL);
      ConstTCSelfP elemL3P = static_cast<ConstTCSelfP>(elemP);
      elemNoNo1P = elemL3P->noeud1P;
      elemNoNo2P = elemL3P->noeud2P;
      elemNoNo3P = elemL3P->noeud3P;

      if (elemNoNo2P < elemNoNo1P)
      {
         const typename MGElementL3<TTTraits>::TCNoeudP noTmpP = elemNoNo1P;
         elemNoNo1P = elemNoNo2P;
         elemNoNo2P = noTmpP;
      }
      if (elemNoNo3P < elemNoNo1P)
      {
         const typename MGElementL3<TTTraits>::TCNoeudP noTmpP = elemNoNo1P;
         elemNoNo1P = elemNoNo3P;
         elemNoNo3P = elemNoNo2P;
         elemNoNo2P = noTmpP;
      }
      else if (elemNoNo3P < elemNoNo2P)
      {
         const typename MGElementL3<TTTraits>::TCNoeudP noTmpP = elemNoNo2P;
         elemNoNo2P = elemNoNo3P;
         elemNoNo3P = noTmpP;
      }

      if (noNo1P != elemNoNo1P)
      {
         reponse = noNo1P < elemNoNo1P;
      }
      else if (noNo2P != elemNoNo2P)
      {
         reponse = noNo2P < elemNoNo2P;
      }
      else if (noNo3P != elemNoNo3P)
      {
         reponse = noNo3P < elemNoNo3P;
      }
      else
      {
         reponse = FAUX;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return reponse;
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementL3
//
// Entrée: TCAlgo& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL3<TTTraits>::executeAlgorithme(typename MGElementL3<TTTraits>::TCAlgo& algo)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementL3(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementL3.  L'algorithme ne peut
//              modifier l'élément puisque l'élément est constant.
//
// Entrée: TTAlgoConst& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL3<TTTraits>::executeAlgorithme(typename MGElementL3<TTTraits>::TCAlgoConst& algo) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementL3(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline ERMsg MGElementL3<TTTraits>::listeMaille(CLListeP***, EntierN, EntierN, EntierN,
                                                DReel, DReel, DReel, TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline Booleen MGElementL3<TTTraits>::pointInterieur(const TCCoord&, const DReel&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return FAUX;
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline void MGElementL3<TTTraits>::reqVectNoeud(TCCoord &, EntierN)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline Booleen MGElementL3<TTTraits>::transformationInverse(DReel&,
                                                            const TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return FAUX;
}

//************************************************************************
// Description:
//    Retourne le nombre de noeud contenu dans l'élément.
//
// Entrée:
//
// Sortie:
//    EntierN : Nombre de noeud.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline EntierN MGElementL3<TTTraits>::reqNbrNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return 3;
}

//************************************************************************
// Description:
//    Retourne le type de l'élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline typename MGElementL3<TTTraits>::Type MGElementL3<TTTraits>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return TCSelf::TYPE_L3;
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL3<TTTraits>::reqNoeuds(typename MGElementL3<TTTraits>::TCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize)
#else
                                      EntierN /*bufferSize*/)
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL3<TTTraits>::reqNoeuds(typename MGElementL3<TTTraits>::ConstTCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize) const
#else
                                      EntierN /*bufferSize*/) const
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description: Méthode de sauvegarde virtuelle qui sauvegarde les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&                           : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementL3<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::ecrisVirtuel(os);
   }

   if (os)
   {
      os << filtre(noeud1P) << ESPACE
         << filtre(noeud2P) << ESPACE
         << filtre(noeud3P);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_PERSISTANT

//************************************************************************
// Sommaire: Récupère l'élément
//
// Description: Méthode de lecture virtuelle qui lis les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on lit
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementL3<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      TCElement::lisVirtuel(is);
   }

   TCNoeudP n1P, n2P, n3P;
   if (is)
   {
      is >> filtre(n1P) >> filtre(n2P) >> filtre(n3P);
   }
   if (is)
   {
      noeud1P = n1P;
      noeud2P = n2P;
      noeud3P = n3P;
   }
   if (is)
   {
      if (noeud1P == noeud2P || noeud1P == noeud3P || noeud2P == noeud3P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem = "#";
         noElem << reqNoElement();
         msg.ajoute("MSG_ELEMENT_L3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = NUL;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: Exporte l'élément
//
// Description: Méthode d'exportation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on s'exporte
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT 
template <typename TTTraits>
void MGElementL3<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::exporte(os);
   }
   if (os)
   {
      os << (noeud1P->reqNoNoeud()+1) << ESPACE
         << (noeud2P->reqNoNoeud()+1) << ESPACE
         << (noeud3P->reqNoNoeud()+1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire: Importe l'élément
//
// Description: Méthode d'importation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier duquel on s'importe
//   const CLListe& lstNoeuds   : liste contenant les pointeurs aux noeuds.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementL3<TTTraits>::importe(FIFichier& is, const CLListe& lstNoeuds)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if(is)
   {
      TCElement::importe(is,lstNoeuds);
   }

   EntierN n1, n2, n3;
   if (is)
   {
      is >> n1 >> n2 >> n3;
   }
   if (is)
   {
      if (n1 < 1 || n2 < 1 || n3 < 1)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NUMEROTATION_NOEUD");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_L3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
      else
      {
         n1--; n2--; n3--;
      }
   }
   if (is)
   {
      EntierN nbrNoeuds = lstNoeuds.dimension();
      if (n1 < nbrNoeuds && n2 < nbrNoeuds && n3 < nbrNoeuds)
      {
         noeud1P = TCNoeudP(lstNoeuds[n1]);
         noeud2P = TCNoeudP(lstNoeuds[n2]);
         noeud3P = TCNoeudP(lstNoeuds[n3]);
      }
      else
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_CONNECTIVITE_INVALIDE");
         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_L3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }
   if (is)
   {
      if (noeud1P == noeud2P || noeud1P == noeud3P || noeud2P == noeud3P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_L3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = NUL;
      }
   }


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_IMPORT_EXPORT

#endif  // MGELEMENT_L3_HPP_DEJA_INCLU
