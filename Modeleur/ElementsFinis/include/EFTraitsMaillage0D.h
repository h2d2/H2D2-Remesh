//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// $Id$
//
// Classe:  EFTraitsMaillage0D
//
// Description:
//    Trait pour la paramétrisation des classes d'éléments finis
//
//   Le trait défini les types suivants : 
//      TTCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//      TCElement   : type faisant référence aux éléments
//      TCElementXX : type des types d'élément XX pour un maillage
//      TCMaillage  : type du maillage
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef EFTRAITSMAILLAGE0D_H_DEJA_INCLU
#define EFTRAITSMAILLAGE0D_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

struct EFTraitsMaillage0D;

#ifndef MODULE_ELEMENTS_FINIS
   #include "GOCoordonnees0D.h"
   #include "MGNoeud.h"

   #include "MGElement.h"
   #include "MGElementNIL.h"
   #include "MGElementP1.h"

   #include "MGMaillage.h"
#endif //MODULE_ELEMENTS_FINIS

struct EFTraitsMaillage0D
{    
   typedef  EFTraitsMaillage0D   TCSelf;

   typedef  GOCoordonnees0D      TCCoord;
   typedef  MGNoeud<TCCoord>     TCNoeud;

   typedef  MGElement<TCSelf>    TCElement;
   typedef  MGElementP1<TCSelf>  TCElementP1;
   typedef  MGElementNIL<TCSelf> TCElementL2;
   typedef  MGElementNIL<TCSelf> TCElementL3;
   typedef  MGElementNIL<TCSelf> TCElementL3L;
   typedef  MGElementNIL<TCSelf> TCElementT3;
   typedef  MGElementNIL<TCSelf> TCElementT6;
   typedef  MGElementNIL<TCSelf> TCElementT6L;
   typedef  MGElementNIL<TCSelf> TCElementQ4;
 //typedef MGElementNIL<TCSelf>  TCElementQ9;
 //typedef MGElementNIL<TCSelf>  TCElementTH4;
 //typedef MGElementNIL<TCSelf>  TCElementH8;

   typedef  MGMaillage<TCSelf>   TCMaillage;
};

#endif  // EFTRAITSMAILLAGE0D_H_DEJA_INCLU

