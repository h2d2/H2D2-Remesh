//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGConst.h
//
// Description:
//    Ce fichier contient la définition des constantes liées aux
//    éléments.
//
// Notes:
//
//************************************************************************
// 28-01-1993  Yves Secretan     Version initiale
// 26-02-1993  François Gingras
// 21-06-1993  François Gingras
// 16-10-1995  Serge Dufour      Déclaration des variables const en variable extern
// 16-10-1997  Yves Secretan     Passage à la double précision
// 25-07-2003  Eric Larouche     Nettoyage du code
// 05-02-2004  Maude Giasson     EFConst devient MGConst
//************************************************************************
#ifndef MGCONST_H_DEJA_INCLU
#define MGCONST_H_DEJA_INCLU

#include "sytypes.h"

extern const EntierN EF_MAXIMUMENTIERN;

extern const DReel   EF_EPSILON;

extern const EntierN EF_NOMBRENOEUDSMAX;

#endif  // MGCONST_H_DEJA_INCLU
