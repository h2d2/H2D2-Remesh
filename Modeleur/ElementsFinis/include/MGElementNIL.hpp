//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// $Id$
// Classe : MGElementNIL
//************************************************************************
#ifndef MGELEMENTNIL_HPP_DEJA_INCLU
#define MGELEMENTNIL_HPP_DEJA_INCLU

#include "MGConst.h"

//**************************************************************
// Description:
//   Constructeur par défaut, aucun argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementNIL<TTTraits>::MGElementNIL()
: TCElement(0),
  noeud1P(NUL),
  noeud2P(NUL),
  noeud3P(NUL),
  noeud4P(NUL),
  noeud5P(NUL),
  noeud6P(NUL),
  noeud7P(NUL),
  noeud8P(NUL),
  noeud9P(NUL)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementNIL ()


//**************************************************************
// Description:
//   Initialise le numéro de l'élément ainsi que son noeud correspondants.
//
// Entrée:
//   EntierN element     : le numéro de l'élément qui relie les deux noeuds
//   TCNoeudPP &noeuds   : Référence sur un tableau de pointeur sur des noeuds.
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementNIL<TTTraits>::MGElementNIL(EntierN,
                                     typename MGElementNIL<TTTraits>::TCNoeudPP)
: noeud1P(NUL),
  noeud2P(NUL),
  noeud3P(NUL),
  noeud4P(NUL),
  noeud5P(NUL),
  noeud6P(NUL),
  noeud7P(NUL),
  noeud8P(NUL),
  noeud9P(NUL)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementNIL (EntierN, TCNoeudPP)

//**************************************************************
// Description:
//   ne fait rien car aucune allocation dynamique n'a été faite
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementNIL<TTTraits>::~MGElementNIL()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}  // ~MGElementNIL ()

//************************************************************************
// Sommaire: Compare les connectivités et le type d'élément.
//
// Description: Compare les connectivités et le type d'élément.  Retourne VRAI
//              si le type d'élément est inférieur ou si c'est le mème type
//              d'élément, mais que la connectivité  soit inférieure.  Pour que
//              la connectivité soit considérée inférieure, il faut qu'une fois
//              les NoNoeuds ordonnés des deux éléments cet élément-ci (this) possède
//              un noeud avec un # inférieur dans les n premiers noeuds comparés
//              à ceux de l'autre élément.
//
// Entrée:
//   ConstTCElementP elem          : Element à comparer
//
// Sortie:
//   Booleen                           : VRAI ou FAUX
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
Booleen MGElementNIL<TTTraits>::compareConnectiviteInferieure(typename MGElementNIL<TTTraits>::ConstTCElementP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return VRAI;
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementNIL
//
// Entrée: TCAlgo& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementNIL<TTTraits>::executeAlgorithme(typename MGElementNIL<TTTraits>::TCAlgo&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementNIL.  L'algorithme ne peut
//              modifier l'élément puisque l'élément est constant.
//
// Entrée: TCAlgoConst& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementNIL<TTTraits>::executeAlgorithme(typename MGElementNIL<TTTraits>::TCAlgoConst&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGElementNIL<TTTraits>::listeMaille(CLListeP***, EntierN, EntierN, EntierN,
                                          DReel, DReel, DReel, TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Description:
//    Le point est à l'intérieur de l'élément P1 s'il coïncide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename TTTraits>
Booleen 
MGElementNIL<TTTraits>::pointInterieur(const TCCoord&,
                                       const DReel&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return FAUX;
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementNIL<TTTraits>::reqVectNoeud(TCCoord &, EntierN)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Description:
//    Retourne le nombre de noeud contenu dans l'élément.
//
// Entrée:
//
// Sortie:
//    EntierN : Nombre de noeud.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
EntierN MGElementNIL<TTTraits>::reqNbrNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return 0;
}

//************************************************************************
// Description:
//    Retourne le type de l'élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
typename MGElementNIL<TTTraits>::Type MGElementNIL<TTTraits>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return TYPE_NIL;
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementNIL<TTTraits>::reqNoeuds(typename MGElementNIL<TTTraits>::TCNoeudPP&,
                                       EntierN)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TTNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementNIL<TTTraits>::reqNoeuds(typename MGElementNIL<TTTraits>::ConstTCNoeudPP&,
                                      EntierN) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

}

#endif  // MGELEMENTNIL_HPP_DEJA_INCLU

