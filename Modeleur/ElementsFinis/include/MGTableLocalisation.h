//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     $Id$
//
// Classe:      MGTableLocalisation
//
// Sommaire:    Interface de définition de la classe MGTableLocalisation
//
// Description:
//    La table de localisation est une structure auxiliaire du maillage,
//    qui permet de localiser l'élément sous un point. Elle encapsule
//    un algorithme de recherche.
//    Dans sa version générique, la recherche est séquentielle.
//
// Attributs:
//
// Note:
//
//************************************************************************
#ifndef MGTABLELOCALISATION_H_DEJA_INCLU
#define MGTABLELOCALISATION_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

template <typename TTMaillage>
class MGTableLocalisation
{
public:
   typedef          TTMaillage             TCMaillage;
   typedef typename TCMaillage::TCCoord    TCCoord;
   typedef typename TCMaillage::TCElementP TCElementP;

          MGTableLocalisation (const TCMaillage&);
         ~MGTableLocalisation ();

   ERMsg  chercheElement      (TCElementP&, const TCCoord&, const DReel&) const;

protected:
   void  invariant            (ConstCarP) const;

private:
   TCMaillage const* m_maillageP;
};

//************************************************************************
// Description:
//    Contrôle que les indices soient consistant
//
// Entrée:
//     CarP condition:   PRECONDITION ou POSTCONDITION
//
// Sortie:
//     Fin du programme en cas d'erreur.
//
// Notes:
//
//************************************************************************
template <typename TTMaillage>
inline void MGTableLocalisation<TTMaillage>::invariant(ConstCarP conditionP) const
{
   INVARIANT(m_maillageP != NUL, conditionP);
}

#include "MGTableLocalisation.hpp"

#endif   // MGTABLELOCALISATION_H_DEJA_INCLU

