//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: MGElementQ4.hpp
// Classe : MGElementQ4
//************************************************************************
// 28-07-2003  Maxime Derenne      Version initiale
// 04-08-2003  Maxime Derenne      Transfert de méthode inline
// 25-10-2003  Olivier Kaczor      Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor      Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor      Rendre compilable sur GCC
// 16-02-2004  Maude Giasson       TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_Q4_HPP_DEJA_INCLU
#define MGELEMENT_Q4_HPP_DEJA_INCLU

#include "clliste.h"
//#include "clvect.h"

#include "MGConst.h"
#include "GOEpsilon.h"
#include "MGElementT3.h"

#include <algorithm>
#include <vector>

#ifdef MODE_PERSISTANT
#include "fifichie.h"
#include "fimnpflt.h"
#endif // MODE_PERSISTANT

#ifndef MIN_MAX
#define MIN_MAX
inline Entier minLocal (Entier i, Entier j) { return i < j ? i : j ;}
inline Entier maxLocal (Entier i, Entier j) { return i > j ? i : j ;}
#endif  // MIN_MAX

//**************************************************************
// Description:
//   Constructeur par défault, sans argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementQ4<TTTraits>::MGElementQ4()
: TCSelf::TCElement(0),
  noeud1P(NUL),
  noeud2P(NUL),
  noeud3P(NUL),
  noeud4P(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // MGElementQ4 ()


//**************************************************************
// Description:
//   Initialise le numéro de l'élément ainsi que ses quatre noeuds
//   correspondants.
//
// Entrée:
//   EntierN element     : le numéro de l'élément qui relie les quatre noeuds
//   TCNoeudPP &noeuds   : Référence sur un tableau de pointeur sur des noeuds.
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementQ4<TTTraits>::MGElementQ4(EntierN element,
  typename MGElementQ4<TTTraits>::TCNoeudPP noeudsPP)
: TCSelf::TCElement(element),
  noeud1P(noeudsPP[0]),
  noeud2P(noeudsPP[1]),
  noeud3P(noeudsPP[2]),
  noeud4P(noeudsPP[3])
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // MGElementQ4 (EntierN, TCNoeudPP)


//**************************************************************
// Description:
//   ne fait rien car aucune allocation dynamique n'a été faite
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementQ4<TTTraits>::~MGElementQ4()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}  // ~MGElementQ4 ()

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::calculeN(DReel& n1, DReel& n2, DReel& n3, DReel& n4,
                                   DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   n1 = 0.25 * (1.0 - ksi) * (1.0 - eta);
   n2 = 0.25 * (1.0 + ksi) * (1.0 - eta);
   n3 = 0.25 * (1.0 + ksi) * (1.0 + eta);
   n4 = 0.25 * (1.0 - ksi) * (1.0 + eta);

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//    Calcule le jacobien de la transformation.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
DReel MGElementQ4<TTTraits>::calculeDetJ(DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel x1 = noeud1P->reqCoordonnees()[0];
   DReel y1 = noeud1P->reqCoordonnees()[1];
   DReel x2 = noeud2P->reqCoordonnees()[0];
   DReel y2 = noeud2P->reqCoordonnees()[1];
   DReel x3 = noeud3P->reqCoordonnees()[0];
   DReel y3 = noeud3P->reqCoordonnees()[1];
   DReel x4 = noeud4P->reqCoordonnees()[0];
   DReel y4 = noeud4P->reqCoordonnees()[1];

   DReel j11 = 0.25 * ((-x1+x2+x3-x4) + eta*(x1-x2+x3-x4));
   DReel j12 = 0.25 * ((-y1+y2+y3-y4) + eta*(y1-y2+y3-y4));
   DReel j21 = 0.25 * ((-x1-x2+x3+x4) + ksi*(x1-x2+x3-x4));
   DReel j22 = 0.25 * ((-y1-y2+y3+y4) + ksi*(y1-y2+y3-y4));
   DReel detJ = j11*j22 - j12*j21;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return detJ;
}

//**************************************************************
// Sommaire:
//    Calcule le jacobien de la transformation.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::calculeJacobien(DReel& j11, DReel& j12, DReel& j21, DReel& j22,
                                            DReel& detJ,
                                            DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel x1 = noeud1P->reqCoordonnees()[0];
   DReel y1 = noeud1P->reqCoordonnees()[1];
   DReel x2 = noeud2P->reqCoordonnees()[0];
   DReel y2 = noeud2P->reqCoordonnees()[1];
   DReel x3 = noeud3P->reqCoordonnees()[0];
   DReel y3 = noeud3P->reqCoordonnees()[1];
   DReel x4 = noeud4P->reqCoordonnees()[0];
   DReel y4 = noeud4P->reqCoordonnees()[1];

   j11 = 0.25 * ((-x1+x2+x3-x4) + eta*(x1-x2+x3-x4));
   j12 = 0.25 * ((-y1+y2+y3-y4) + eta*(y1-y2+y3-y4));
   j21 = 0.25 * ((-x1-x2+x3+x4) + ksi*(x1-x2+x3-x4));
   j22 = 0.25 * ((-y1-y2+y3+y4) + ksi*(y1-y2+y3-y4));
   detJ = j11*j22 - j12*j21;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::calculeNx(DReel& n1x, DReel& n2x, DReel& n3x, DReel& n4x,
                                    DReel& detJ, DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel j11, j12, j21, j22;
   calculeJacobien(j11, j12, j21, j22, detJ, ksi, eta);

   DReel fact = 0.25 / detJ;
   n1x = ((-1.0+eta)*j22 - (-1.0+ksi)*j12) * fact;
   n2x = (( 1.0-eta)*j22 - (-1.0-ksi)*j12) * fact;
   n3x = (( 1.0+eta)*j22 - ( 1.0+ksi)*j12) * fact;
   n4x = ((-1.0-eta)*j22 - ( 1.0-ksi)*j12) * fact;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::calculeNy(DReel& n1y, DReel& n2y, DReel& n3y, DReel& n4y,
                                      DReel& detJ, DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel j11, j12, j21, j22;
   calculeJacobien(j11, j12, j21, j22, detJ, ksi, eta);

   DReel fact = 0.25 / detJ;
   n1y = (- (-1.0+eta)*j21 + (-1.0+ksi)*j11) * fact;
   n2y = (- ( 1.0-eta)*j21 + (-1.0-ksi)*j11) * fact;
   n3y = (- ( 1.0+eta)*j21 + ( 1.0+ksi)*j11) * fact;
   n4y = (- (-1.0-eta)*j21 + ( 1.0-ksi)*j11) * fact;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Compare les connectivités et le type d'élément.
//
// Description: Compare les connectivités et le type d'élément.  Retourne VRAI
//              si le type d'élément est inférieur ou si c'est le mème type
//              d'élément, mais que la connectivité  soit inférieure.  Pour que
//              la connectivité soit considérée inférieure, il faut qu'une fois
//              les NoNoeuds ordonnés des deux éléments cet élément-ci (this) possède
//              un noeud avec un # inférieur dans les n premiers noeuds comparés
//              à ceux de l'autre élément.
//
// Entrée:
//   ConstMGElementP elem          : Element à comparer
//
// Sortie:
//   Booleen                           : VRAI ou FAUX
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
Booleen MGElementQ4<TTTraits>::compareConnectiviteInferieure(typename MGElementQ4<TTTraits>::ConstTCElementP elemP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef MGElementQ4<typename MGElementQ4<TTTraits>::TCTraits> const * ConstTCSelfP;
   Booleen reponse = FAUX;

   if (this->reqType() != elemP->reqType())
   {
      reponse = this->reqType() < elemP->reqType();
   }
   else
   {
      ASSERTION(dynamic_cast<ConstTCSelfP>(elemP) != NUL);
      ConstTCSelfP elemQ4P = static_cast<ConstTCSelfP>(elemP);
      typedef std::vector<typename MGElementQ4<TTTraits>::TCNoeudP> VectorTCNoeudP;
      VectorTCNoeudP vecNoeudsLocal(4);
      VectorTCNoeudP vecNoeudsElem(4);

      vecNoeudsLocal[0] = noeud1P;
      vecNoeudsLocal[1] = noeud2P;
      vecNoeudsLocal[2] = noeud3P;
      vecNoeudsLocal[3] = noeud4P;
      std::sort(vecNoeudsLocal.begin(), vecNoeudsLocal.end());

      vecNoeudsElem[0] = elemQ4P->noeud1P;
      vecNoeudsElem[1] = elemQ4P->noeud2P;
      vecNoeudsElem[2] = elemQ4P->noeud3P;
      vecNoeudsElem[3] = elemQ4P->noeud4P;
      std::sort(vecNoeudsElem.begin(), vecNoeudsElem.end());

      typename VectorTCNoeudP::iterator       noeudLocalI    = vecNoeudsLocal.begin();
      typename VectorTCNoeudP::const_iterator noeudLocalFinI = vecNoeudsLocal.end();
      typename VectorTCNoeudP::iterator       noeudElemI     = vecNoeudsElem.begin();

      while(noeudLocalI != noeudLocalFinI)
      {
         if (*noeudLocalI != *noeudElemI)
         {
            reponse = *noeudLocalI < *noeudElemI;
         }
         noeudLocalI++;
         noeudElemI++;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return reponse;
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementQ4
//
// Entrée: TCAlgo& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::executeAlgorithme(typename MGElementQ4<TTTraits>::TCAlgo& algo)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementQ4(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementQ4.  L'algorithme ne peut
//              modifier l'élément puisque l'élément est constant.
//
// Entrée: TCAlgoConst& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::executeAlgorithme(typename MGElementQ4<TTTraits>::TCAlgoConst& algo) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementQ4(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//    Identifie les mailles de la table de localisation qui recoupent
//    l'élément et l'ajoute à leurs listes respectives.
//    On boucle sur les côtés afin de determiner pour chaque indice i
//    les indice min et max en j.
//    Pour chaque côté, on repère toutes les mailles intersectées en traitant
//    de manière exhaustive toutes les intersections avec le grille de base.
//    Suivant la pente du côté on traite les itersections soit soit suivant
//    les indices i, soit suivant les indices j. On ne calcule par directement
//    les intersections, mais on contrôle quand une avancée de delX
//    (respectivement delY) provoque un changement d'indice en j (respectivement
//    i).
//    Les jMin et jMax soit stockés dans un repère i local aux mailles de
//    l'élément, ceci pour eviter de dimensionner des vecteurs trop grands pour
//    rien.
//
// Entrée:
//    EntierN          tableIMax  dimension en x de la table de localisation
//    EntierN          tableJMax  dimension en y de la table de localisation
//    EntierN          tableKMax  dimension en z de la table de localisation
//    DReel            delX       pas de la table en x
//    DReel            delY       pas de la table en y
//    DReel            delZ       pas de la table en z
//    SYCoordonnéesXYZ coin       coin inférieur gauche de la table
//
// Sortie:
//    CLListeP***   tableLoca  table de localisation
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
ERMsg MGElementQ4<TTTraits>::listeMaille(CLListeP ***tableLoca, EntierN tableIMax,
                                         EntierN tableJMax, EntierN, DReel delX,
                                         DReel delY, DReel, TCCoord& coin) const
{
#ifdef MODE_DEBUG
   PRECONDITION(tableLoca != NUL);
   PRECONDITION(delX >= 1.0e-8);
   PRECONDITION(delY >= 1.0e-8);
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   TCElementT3 t3;

   if (msg)
   {
      t3.noeud1P = noeud1P;
      t3.noeud2P = noeud2P;
      t3.noeud3P = noeud3P;
      msg = listeMailleT3(tableLoca, tableIMax, tableJMax, 0, delX, delY, 0, coin, &t3);
   }

   if (msg)
   {
      t3.noeud1P = noeud3P;
      t3.noeud2P = noeud4P;
      t3.noeud3P = noeud1P;
      msg = listeMailleT3(tableLoca, tableIMax, tableJMax, 0, delX, delY, 0, coin, &t3);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Description:
//    Identifie les mailles de la table de localisation qui recoupent
//    l'élément et l'ajoute à leurs listes respectives.
//    On boucle sur les côtés afin de determiner pour chaque indice i
//    les indice min et max en j.
//    Pour chaque côté, on repère toutes les mailles intersectées en traitant
//    de manière exhaustive toutes les intersections avec le grille de base.
//    Suivant la pente du côté on traite les itersections soit soit suivant
//    les indices i, soit suivant les indices j. On ne calcule par directement
//    les intersections, mais on contrôle quand une avancée de delX
//    (respectivement delY) provoque un changement d'indice en j (respectivement
//    i).
//    Les jMin et jMax soit stockés dans un repère i local aux mailles de
//    l'élément, ceci pour eviter de dimensionner des vecteurs trop grands pour
//    rien.
//
// Entrée:
//    EntierN          tableIMax  dimension en x de la table de localisation
//    EntierN          tableJMax  dimension en y de la table de localisation
//    EntierN          tableKMax  dimension en z de la table de localisation
//    DReel            delX       pas de la table en x
//    DReel            delY       pas de la table en y
//    DReel            delZ       pas de la table en z
//    SYCoordonnéesXYZ coin       coin inférieur gauche de la table
//
// Sortie:
//    CLListeP***   tableLoca  table de localisation
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
ERMsg MGElementQ4<TTTraits>::listeMailleT3(CLListeP ***tableLoca,
                                           EntierN tableIMax,
                                           EntierN tableJMax,
                                           EntierN,
                                           DReel delX,
                                           DReel delY,
                                           DReel,
                                           typename MGElementQ4<TTTraits>::TCCoord& coin,
                                           TCElementT3P t3P) const
{
#ifdef MODE_DEBUG
   PRECONDITION(tableLoca != NUL);
   PRECONDITION(delX >= 1.0e-8);
   PRECONDITION(delY >= 1.0e-8);
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   EntierN i;
   EntierN iNoeudMin = tableIMax, iNoeudMax = 0;
   EntierN jNoeudMin = tableJMax, jNoeudMax = 0;
   Entier iNoeud[4], jNoeud[4];
   DReel  xNoeud[4], yNoeud[4];

   // --- On place les données des noeuds de l'élément dans un vecteur
   //     pour faciliter l'écriture de la boucle sur les côtés

   // ---  Coordonnées des sommets
   xNoeud[0] = t3P->noeud1P->reqCoordonnees()[0], yNoeud[0] = t3P->noeud1P->reqCoordonnees()[1];
   xNoeud[1] = t3P->noeud2P->reqCoordonnees()[0], yNoeud[1] = t3P->noeud2P->reqCoordonnees()[1];
   xNoeud[2] = t3P->noeud3P->reqCoordonnees()[0], yNoeud[2] = t3P->noeud3P->reqCoordonnees()[1];
   xNoeud[3] = xNoeud[0], yNoeud[3] = yNoeud[0];

   // ---  Indices des sommets dans la table
   DReel f = 0.0F;
   DReelP fP = &f;
   f = (xNoeud[0] - coin[0]) / delX;
   iNoeud[0] = iNoeud[3] = Entier(*fP);

   f = (yNoeud[0] - coin[1]) / delY;
   jNoeud[0] = jNoeud[3] = Entier(*fP);

   f = (xNoeud[1] - coin[0]) / delX;
   iNoeud[1] = Entier(*fP);

   f = (yNoeud[1] - coin[1]) / delY;
   jNoeud[1] = Entier(*fP);

   f = (xNoeud[2] - coin[0]) / delX;
   iNoeud[2] = Entier(*fP);

   f = (yNoeud[2] - coin[1]) / delY;
   jNoeud[2] = Entier(*fP);


   // ---  Min et max des indices
   iNoeudMin = minLocal(iNoeudMin, iNoeud[0]);
   iNoeudMin = minLocal(iNoeudMin, iNoeud[1]);
   iNoeudMin = minLocal(iNoeudMin, iNoeud[2]);
   iNoeudMax = maxLocal(iNoeudMax, iNoeud[0]);
   iNoeudMax = maxLocal(iNoeudMax, iNoeud[1]);
   iNoeudMax = maxLocal(iNoeudMax, iNoeud[2]);
   jNoeudMin = minLocal(jNoeudMin, jNoeud[0]);
   jNoeudMin = minLocal(jNoeudMin, jNoeud[1]);
   jNoeudMin = minLocal(jNoeudMin, jNoeud[2]);
   jNoeudMax = maxLocal(jNoeudMax, jNoeud[0]);
   jNoeudMax = maxLocal(jNoeudMax, jNoeud[1]);
   jNoeudMax = maxLocal(jNoeudMax, jNoeud[2]);
   int ntableIMax = tableIMax, ntableJMax = tableJMax, niNoeudMin = iNoeudMin;
   int niNoeudMax = iNoeudMax, njNoeudMin = jNoeudMin, njNoeudMax = jNoeudMax;
   if (niNoeudMin < 0 || niNoeudMin >= ntableIMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");
   if (niNoeudMax < 0 || niNoeudMax >= ntableIMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");
   if (njNoeudMin < 0 || njNoeudMin >= ntableJMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");
   if (njNoeudMax < 0 || njNoeudMax >= ntableJMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");

   // ---  Vecteurs de travail
   EntierP minJColonneIV = new Entier[iNoeudMax-iNoeudMin+1];
   EntierP maxJColonneIV = new Entier[iNoeudMax-iNoeudMin+1];
   for (i = 0; i <= (iNoeudMax-iNoeudMin); i++)
      minJColonneIV[i] = tableIMax, maxJColonneIV[i] = 0;

   // ---  Prend les sommets
   minJColonneIV[iNoeud[0]-iNoeudMin] = minLocal(jNoeud[0] , minJColonneIV[iNoeud[0]-iNoeudMin]);
   maxJColonneIV[iNoeud[0]-iNoeudMin] = maxLocal(jNoeud[0] , maxJColonneIV[iNoeud[0]-iNoeudMin]);
   minJColonneIV[iNoeud[1]-iNoeudMin] = minLocal(jNoeud[1] , minJColonneIV[iNoeud[1]-iNoeudMin]);
   maxJColonneIV[iNoeud[1]-iNoeudMin] = maxLocal(jNoeud[1] , maxJColonneIV[iNoeud[1]-iNoeudMin]);
   minJColonneIV[iNoeud[2]-iNoeudMin] = minLocal(jNoeud[2] , minJColonneIV[iNoeud[2]-iNoeudMin]);
   maxJColonneIV[iNoeud[2]-iNoeudMin] = maxLocal(jNoeud[2] , maxJColonneIV[iNoeud[2]-iNoeudMin]);

   // ---  Boucle sur les côtés
   Entier iIndLoc, iIndLocMin, iIndLocMax, iIndInc, iTmp;
   Entier jIndGlb, jIndGlbMin, jIndGlbMax, jIndInc, jTmp;
   DReel pente, x, y, v, dv, vlim;
   DReel diffX, diffY;
   for (i = 0; i < 3; i++)
   {
      // ---  Variations en x et y
      diffX = xNoeud[i+1] - xNoeud[i];
      diffY = yNoeud[i+1] - yNoeud[i];

      // ---  Incréments pour le parcours de la table
      if (diffX >= DReel(0.0))
         iIndInc =  1;
      else
         iIndInc = -1;
      if (diffY >= DReel(0.0))
         jIndInc = 1;
      else
         jIndInc = -1;

      // ---  Différencie le sens de balayage d'après la variation max
      if (fabs(diffX) >= fabs(diffY))
      {
         pente = diffY / diffX;
         dv = fabs(delX * pente);
         vlim = delY;
         x = coin[0] + DReel(iNoeud[i] + (iIndInc + 1) / Entier(2)) * delX;
         y = coin[1] + DReel(jNoeud[i] - (jIndInc - 1) / Entier(2)) * delY;
         v = fabs( yNoeud[i] + (x - xNoeud[i]) * pente - y );
         iIndLocMin = iNoeud[i  ] - iNoeudMin;
         iIndLocMax = iNoeud[i+1] - iNoeudMin;
         jIndGlb = jNoeud[i];
         for (iIndLoc  = iIndLocMin;
              iIndLoc != iIndLocMax;
              iIndLoc  = iIndLoc + iIndInc)
         {
            while (v >= vlim)
            {
               v = v - vlim;
               jIndGlb = jIndGlb + jIndInc;
               jIndGlb = minLocal(jIndGlb, jNoeudMax);
               jIndGlb = maxLocal(jIndGlb, jNoeudMin);
               minJColonneIV[iIndLoc] = minLocal(jIndGlb, minJColonneIV[iIndLoc]);
               maxJColonneIV[iIndLoc] = maxLocal(jIndGlb, maxJColonneIV[iIndLoc]);
            }
            iTmp = iIndLoc + iIndInc;
            minJColonneIV[iTmp] = minLocal(jIndGlb, minJColonneIV[iTmp]);
            maxJColonneIV[iTmp] = maxLocal(jIndGlb, maxJColonneIV[iTmp]);
            v = v + dv;
         }
      }
      else
      {
         pente = diffX / diffY;
         dv = fabs(delY * pente);
         vlim = delX;
         x = coin[0] + DReel(iNoeud[i] - (iIndInc - 1) / Entier(2)) * delX;
         y = coin[1] + DReel(jNoeud[i] + (jIndInc + 1) / Entier(2)) * delY;
         v = fabs( xNoeud[i] + (y - yNoeud[i]) * pente - x );
         jIndGlbMin = jNoeud[i  ];
         jIndGlbMax = jNoeud[i+1];
         iIndLoc = iNoeud[i] - iNoeudMin;
         for (jIndGlb  = jIndGlbMin;
              jIndGlb != jIndGlbMax;
              jIndGlb  = jIndGlb + jIndInc)
         {
            while (v >= vlim)
            {
               v = v - vlim;
               iIndLoc = iIndLoc + iIndInc;
               iIndLoc = minLocal(iIndLoc, iNoeudMax-iNoeudMin);
               iIndLoc = maxLocal(iIndLoc, 0);
               minJColonneIV[iIndLoc] = minLocal(jIndGlb, minJColonneIV[iIndLoc]);
               maxJColonneIV[iIndLoc] = maxLocal(jIndGlb, maxJColonneIV[iIndLoc]);
            }
            jTmp = jIndGlb + jIndInc;
            minJColonneIV[iIndLoc] = minLocal(jTmp, minJColonneIV[iIndLoc]);
            maxJColonneIV[iIndLoc] = maxLocal(jTmp, maxJColonneIV[iIndLoc]);
            v = v + dv;
         }
      }  // end if (fabs(diffX) >= fabs(diffY))
   }  // end for (i = 0; i < 3; i++)


   // ---  Balaie l'intérieur du triangle
   Entier iIndGlb;
   for (iIndLoc = 0; iIndLoc <= (Entier)(iNoeudMax-iNoeudMin); iIndLoc++)
   {
      iIndGlb = iIndLoc + iNoeudMin;
      for (jIndGlb  = minJColonneIV[iIndLoc];
           jIndGlb <= maxJColonneIV[iIndLoc]; jIndGlb++)
      {
         tableLoca[iIndGlb][jIndGlb][0]->ajoute(VoidP(this));
      }
   }

   // ---  Récupère la mémoire
   delete[] minJColonneIV;
   delete[] maxJColonneIV;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Description:
//    Opération de dérivation en X d'un element.
//
// Entrée:
//   CLVecteur& vecDer                :
//   CLVecteur& vecDer                :
//   const SNVnoScalaire& vnoScalaire : Valeurs scalaires qui sont dérivées
// Sortie:
//
// Notes:
//
//**************************************************************
/*void MGElementQ4::opDeriveX (CLVecteur& vecDer,
                             CLVecteur& vecSrf,
                             const SNVnoScalaire& vno) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel KSI = 1.0 / sqrt(3.0);
   const DReel ETA = 1.0 / sqrt(3.0);

   DReel der1 = 0.0;
   DReel der2 = 0.0;
   DReel der3 = 0.0;
   DReel der4 = 0.0;
   DReel srf1 = 0.0;
   DReel srf2 = 0.0;
   DReel srf3 = 0.0;
   DReel srf4 = 0.0;

   DReel ddx;
   DReel n1, n2, n3, n4;
   DReel nx1, nx2, nx3, nx4;
   DReel detJ;

   // ---  1. point de Gauss
   calculeN (n1, n2, n3, n4, -KSI, -ETA);
   calculeNx(nx1, nx2, nx3, nx4, detJ, -KSI, -ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddx = nx1 * vno[noeud1P->reqIndVno()]
       + nx2 * vno[noeud2P->reqIndVno()]
       + nx3 * vno[noeud3P->reqIndVno()]
       + nx4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddx;
   der2 += n2 * ddx;
   der3 += n3 * ddx;
   der4 += n4 * ddx;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  2. point de Gauss
   calculeN (n1, n2, n3, n4, KSI, -ETA);
   calculeNx(nx1, nx2, nx3, nx4, detJ, KSI, -ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddx = nx1 * vno[noeud1P->reqIndVno()]
       + nx2 * vno[noeud2P->reqIndVno()]
       + nx3 * vno[noeud3P->reqIndVno()]
       + nx4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddx;
   der2 += n2 * ddx;
   der3 += n3 * ddx;
   der4 += n4 * ddx;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  3. point de Gauss
   calculeN (n1, n2, n3, n4, KSI, ETA);
   calculeNx(nx1, nx2, nx3, nx4, detJ, KSI, ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddx = nx1 * vno[noeud1P->reqIndVno()]
       + nx2 * vno[noeud2P->reqIndVno()]
       + nx3 * vno[noeud3P->reqIndVno()]
       + nx4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddx;
   der2 += n2 * ddx;
   der3 += n3 * ddx;
   der4 += n4 * ddx;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  4. point de Gauss
   calculeN (n1, n2, n3, n4, -KSI, ETA);
   calculeNx(nx1, nx2, nx3, nx4, detJ, -KSI, ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddx = nx1 * vno[noeud1P->reqIndVno()]
       + nx2 * vno[noeud2P->reqIndVno()]
       + nx3 * vno[noeud3P->reqIndVno()]
       + nx4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddx;
   der2 += n2 * ddx;
   der3 += n3 * ddx;
   der4 += n4 * ddx;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  Assemble
   DReel tmp;
   vecDer.reqElement(tmp     , noeud1P->reqNoNoeud());
   vecDer.remplace  (tmp+der1, noeud1P->reqNoNoeud());
   vecDer.reqElement(tmp     , noeud2P->reqNoNoeud());
   vecDer.remplace  (tmp+der2, noeud2P->reqNoNoeud());
   vecDer.reqElement(tmp     , noeud3P->reqNoNoeud());
   vecDer.remplace  (tmp+der3, noeud3P->reqNoNoeud());
   vecDer.reqElement(tmp     , noeud4P->reqNoNoeud());
   vecDer.remplace  (tmp+der4, noeud4P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud1P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf1, noeud1P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud2P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf2, noeud2P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud3P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf3, noeud3P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud4P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf4, noeud4P->reqNoNoeud());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // opDeriveX (..., const SNVnoScalaire&)
*/
//**************************************************************
// Description:
//    Opération de dérivation en Y d'un element.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
/*void MGElementQ4::opDeriveY (CLVecteur& vecDer,
                             CLVecteur& vecSrf,
                             const SNVnoScalaire& vno) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel KSI = 1.0 / sqrt(3.0);
   const DReel ETA = 1.0 / sqrt(3.0);

   DReel der1 = 0.0;
   DReel der2 = 0.0;
   DReel der3 = 0.0;
   DReel der4 = 0.0;
   DReel srf1 = 0.0;
   DReel srf2 = 0.0;
   DReel srf3 = 0.0;
   DReel srf4 = 0.0;

   DReel ddy;
   DReel n1, n2, n3, n4;
   DReel ny1, ny2, ny3, ny4;
   DReel detJ;

   // ---  1. point de Gauss
   calculeN (n1, n2, n3, n4, -KSI, -ETA);
   calculeNy(ny1, ny2, ny3, ny4, detJ, -KSI, -ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddy = ny1 * vno[noeud1P->reqIndVno()]
       + ny2 * vno[noeud2P->reqIndVno()]
       + ny3 * vno[noeud3P->reqIndVno()]
       + ny4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddy;
   der2 += n2 * ddy;
   der3 += n3 * ddy;
   der4 += n4 * ddy;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  2. point de Gauss
   calculeN (n1, n2, n3, n4, KSI, -ETA);
   calculeNy(ny1, ny2, ny3, ny4, detJ, KSI, -ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddy = ny1 * vno[noeud1P->reqIndVno()]
       + ny2 * vno[noeud2P->reqIndVno()]
       + ny3 * vno[noeud3P->reqIndVno()]
       + ny4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddy;
   der2 += n2 * ddy;
   der3 += n3 * ddy;
   der4 += n4 * ddy;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  3. point de Gauss
   calculeN (n1, n2, n3, n4, KSI, ETA);
   calculeNy(ny1, ny2, ny3, ny4, detJ, KSI, ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddy = ny1 * vno[noeud1P->reqIndVno()]
       + ny2 * vno[noeud2P->reqIndVno()]
       + ny3 * vno[noeud3P->reqIndVno()]
       + ny4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddy;
   der2 += n2 * ddy;
   der3 += n3 * ddy;
   der4 += n4 * ddy;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  4. point de Gauss
   calculeN (n1, n2, n3, n4, -KSI, ETA);
   calculeNy(ny1, ny2, ny3, ny4, detJ, -KSI, ETA);
   n1 *= detJ;
   n2 *= detJ;
   n3 *= detJ;
   n4 *= detJ;
   ddy = ny1 * vno[noeud1P->reqIndVno()]
       + ny2 * vno[noeud2P->reqIndVno()]
       + ny3 * vno[noeud3P->reqIndVno()]
       + ny4 * vno[noeud4P->reqIndVno()];
   der1 += n1 * ddy;
   der2 += n2 * ddy;
   der3 += n3 * ddy;
   der4 += n4 * ddy;
   srf1 += n1;
   srf2 += n2;
   srf3 += n3;
   srf4 += n4;

   // ---  Assemble
   DReel tmp;
   vecDer.reqElement(tmp     , noeud1P->reqNoNoeud());
   vecDer.remplace  (tmp+der1, noeud1P->reqNoNoeud());
   vecDer.reqElement(tmp     , noeud2P->reqNoNoeud());
   vecDer.remplace  (tmp+der2, noeud2P->reqNoNoeud());
   vecDer.reqElement(tmp     , noeud3P->reqNoNoeud());
   vecDer.remplace  (tmp+der3, noeud3P->reqNoNoeud());
   vecDer.reqElement(tmp     , noeud4P->reqNoNoeud());
   vecDer.remplace  (tmp+der4, noeud4P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud1P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf1, noeud1P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud2P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf2, noeud2P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud3P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf3, noeud3P->reqNoNoeud());
   vecSrf.reqElement(tmp     , noeud4P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf4, noeud4P->reqNoNoeud());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // opDeriveY (CLVecteur& vecDer, CLVecteur& vecSrf, const SNVnoScalaire& vnoScalaire) const)
*/
//**************************************************************
// Description:
//    Opération d'intégration d'un element.
//
// Entrée:
//
// Sortie:
//    DReel resultat : resultat de l'integration de l'élément.
//
// Notes:
//
//**************************************************************
/*void MGElementQ4::opIntegre (DReel& resultat, const SNVnoScalaire& vno) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel KSI = 1.0 / sqrt(3.0);
   const DReel ETA = 1.0 / sqrt(3.0);

   DReel n1, n2, n3, n4;
   DReel j11, j12, j21, j22;
   DReel val, detJ;

   resultat = 0.0;

   // ---  1. point de Gauss
   calculeN(n1, n2, n3, n4, -KSI, -ETA);
   calculeJacobien(j11, j12, j21, j22, detJ, -KSI, -ETA);
   val = n1 * vno[noeud1P->reqIndVno()]
       + n2 * vno[noeud2P->reqIndVno()]
       + n3 * vno[noeud3P->reqIndVno()]
       + n4 * vno[noeud4P->reqIndVno()];
   resultat += val*detJ;

   // ---  2. point de Gauss
   calculeN (n1, n2, n3, n4, KSI, -ETA);
   calculeJacobien(j11, j12, j21, j22, detJ,  KSI, -ETA);
   val = n1 * vno[noeud1P->reqIndVno()]
       + n2 * vno[noeud2P->reqIndVno()]
       + n3 * vno[noeud3P->reqIndVno()]
       + n4 * vno[noeud4P->reqIndVno()];
   resultat += val*detJ;

   // ---  3. point de Gauss
   calculeN (n1, n2, n3, n4, KSI, ETA);
   calculeJacobien(j11, j12, j21, j22, detJ,  KSI,  ETA);
   val = n1 * vno[noeud1P->reqIndVno()]
       + n2 * vno[noeud2P->reqIndVno()]
       + n3 * vno[noeud3P->reqIndVno()]
       + n4 * vno[noeud4P->reqIndVno()];
   resultat += val*detJ;

   // ---  4. point de Gauss
   calculeN (n1, n2, n3, n4, -KSI, ETA);
   calculeJacobien(j11, j12, j21, j22, detJ, -KSI,  ETA);
   val = n1 * vno[noeud1P->reqIndVno()]
       + n2 * vno[noeud2P->reqIndVno()]
       + n3 * vno[noeud3P->reqIndVno()]
       + n4 * vno[noeud4P->reqIndVno()];
   resultat += val*detJ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // opIntegre (DReel resultat)
*/

//**************************************************************
// Description:
//   Cette méthode vérifie si le point passé en paramètre se situe à
//   l'intérieur de l'élément. Si oui, alors la variable resultat va contenir
//   VRAI au retour, et si non alors resultat va contenir FAUX.
//
// Entrée:
//   SYCoordonnees point
//      Contient le point pour lequel on désire déterminer s'il se situe à
//      l'intérieur de l'élément.
//
// Sortie:
//   Booleen& resultat
//
// Notes:
//    Pour un test d'interieur, il n'y a aucune erreur faite en passant
//    par 2 T3.
//
//**************************************************************
template <typename TTTraits>
Booleen MGElementQ4<TTTraits>::pointInterieur(const TCCoord& point,
                                              const DReel& epsilon) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel ksi, eta;
   Booleen resultat = transformationInverse(ksi, eta, point);
   if (!resultat)
   {
      // --- On refait un test avec un epsilon plus tolérant
      resultat =  !((GOEpsilon(fabs(ksi), fabs(epsilon)) > 1.0) ||
                    (GOEpsilon(fabs(eta), fabs(epsilon)) > 1.0));
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return resultat;
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline void MGElementQ4<TTTraits>::reqVectNoeud(TCCoord &, EntierN)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Description:
//    Retourne le nombre de noeud contenu dans l'élément.
//
// Entrée:
//
// Sortie:
//    EntierN : Nombre de noeud.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline EntierN MGElementQ4<TTTraits>::reqNbrNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return 4;
}

//************************************************************************
// Description:
//    Retourne le type de l'élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline typename MGElementQ4<TTTraits>::Type MGElementQ4<TTTraits>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return TCSelf::TYPE_Q4;
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::reqNoeuds(typename MGElementQ4<TTTraits>::TCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize)
#else
                                      EntierN /*bufferSize*/)
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;
   noeuds[3] = noeud4P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementQ4<TTTraits>::reqNoeuds(typename MGElementQ4<TTTraits>::ConstTCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize) const
#else
                                      EntierN /*bufferSize*/) const
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;
   noeuds[3] = noeud4P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//**************************************************************
// Description:
//   Cette méthode fait la transformation des coordonnées en Ksi, Eta et
//   Zeta.
//
// Entrée:
//   TCCoord& point
//      Contient le point pour lequel on désire déterminer s'il se situe à
//      l'intérieur de l'élément.
//
// Sortie:
//   DReel vKsi, vEta, VZeta : les trois coordonnées dans le système de
//                            référence.
//
// Notes:
// Préconditions:
//   Vérifie qu'aucun des noeuds n'est NUL.
//
//**************************************************************
template <typename TTTraits>
Booleen MGElementQ4<TTTraits>::transformationInverse (DReel& ksi, 
                                                      DReel& eta,
                                                      const TCCoord& point) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel  QUART   = 0.25;
   const DReel  EPSILON = 1.0e-10;
   const Entier MAXITER = 5;

   ksi  = 0.0;
   eta  = 0.0;
   TCCoord dx;
   TCCoord dxdksi;
   TCCoord dxdeta;

   DReel  dksi = 1.0;
   DReel  deta = 1.0;
   Entier iter = 0;
   while (iter < MAXITER && fabs(dksi) > EPSILON && fabs(deta) > EPSILON)
   {
      dx = QUART * (noeud1P->reqCoordonnees() * ((1.0 - ksi) * (1.0 - eta)) +
                    noeud2P->reqCoordonnees() * ((1.0 + ksi) * (1.0 - eta)) +
                    noeud3P->reqCoordonnees() * ((1.0 + ksi) * (1.0 + eta)) +
                    noeud4P->reqCoordonnees() * ((1.0 - ksi) * (1.0 + eta)));
      dx = point - dx;
      dxdksi = QUART * (noeud1P->reqCoordonnees() * (-1.0 + eta) +
                        noeud2P->reqCoordonnees() * ( 1.0 - eta) +
                        noeud3P->reqCoordonnees() * ( 1.0 + eta) +
                        noeud4P->reqCoordonnees() * (-1.0 - eta));
      dxdeta = QUART * (noeud1P->reqCoordonnees() * (-1.0 + ksi) +
                        noeud2P->reqCoordonnees() * (-1.0 - ksi) +
                        noeud3P->reqCoordonnees() * ( 1.0 + ksi) +
                        noeud4P->reqCoordonnees() * ( 1.0 - ksi));

      const DReel jacobien = 1.0 / prodVect(dxdksi, dxdeta)[2];
      dksi = prodVect(dx, dxdeta)[2] * jacobien;
      deta = prodVect(dxdksi, dx)[2] * jacobien;
      ksi += dksi;
      eta += deta;

      ++iter;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return !((GOEpsilon(fabs(ksi)) > 1.0) ||
            (GOEpsilon(fabs(eta)) > 1.0) ||
             iter == MAXITER);
}  // transformationInverse (DReel&, DReel&, DReel&, TCCoord&)

//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description: Méthode de sauvegarde virtuelle qui sauvegarde les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&               : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementQ4<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::ecrisVirtuel(os);
   }

   if (os)
   {
      os << filtre(noeud1P) << ESPACE
         << filtre(noeud2P) << ESPACE
         << filtre(noeud3P) << ESPACE
         << filtre(noeud4P);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_PERSISTANT


//************************************************************************
// Sommaire: Récupère l'élément
//
// Description: Méthode de lecture virtuelle qui lis les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on lit
//
// Sortie:
//   FIFichier&               : Référence au fichier
//
// Notes:
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementQ4<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      TCElement::lisVirtuel(is);
   }

   TCNoeudP n1P, n2P, n3P, n4P;
   if (is)
   {
      is >> filtre(n1P) >> filtre(n2P) >> filtre(n3P) >> filtre(n4P);
   }
   if (is)
   {
      noeud1P = n1P;
      noeud2P = n2P;
      noeud3P = n3P;
      noeud4P = n4P;
   }

   if (is)
   {
      if (noeud1P == noeud2P || noeud1P == noeud3P || noeud1P == noeud4P ||
          noeud2P == noeud3P || noeud2P == noeud4P || noeud3P == noeud4P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem;
         noElem << reqNoElement();
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = noeud4P = NUL;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_PERSISTANT

//************************************************************************
// Sommaire: Exporte l'élément
//
// Description: Méthode d'exportation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on s'exporte
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementQ4<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::exporte(os);
   }
   if (os)
   {
      os << (noeud1P->reqNoNoeud()+1) << ESPACE
         << (noeud2P->reqNoNoeud()+1) << ESPACE
         << (noeud3P->reqNoNoeud()+1) << ESPACE
         << (noeud4P->reqNoNoeud()+1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT 

//************************************************************************
// Sommaire: Importe l'élément
//
// Description: Méthode d'importation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier duquel on s'importe
//   const CLListe& lstNoeuds : liste contenant les pointeurs aux noeuds.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementQ4<TTTraits>::importe(FIFichier& is, const CLListe& lstNoeuds)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      TCElement::importe(is, lstNoeuds);
   }

   EntierN n1, n2, n3, n4;
   if (is)
   {
      is >> n1 >> n2 >> n3 >> n4;
   }
   if (is)
   {
      if (n1 < 1 || n2 < 1 || n3 < 1 || n4 < 1)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NUMEROTATION_NOEUD");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
      else
      {
         n1--; n2--; n3--; n4--;
      }
   }
   if (is)
   {
      EntierN nbrNoeuds = lstNoeuds.dimension();
      if (n1 < nbrNoeuds && n2 < nbrNoeuds && n3 < nbrNoeuds && n4 < nbrNoeuds)
      {
         noeud1P = TCNoeudP(lstNoeuds[n1]);
         noeud2P = TCNoeudP(lstNoeuds[n2]);
         noeud3P = TCNoeudP(lstNoeuds[n3]);
         noeud4P = TCNoeudP(lstNoeuds[n4]);
      }
      else
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_CONNECTIVITE_INVALIDE");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      if (noeud1P == noeud2P || noeud1P == noeud3P || noeud1P == noeud4P ||
          noeud2P == noeud3P || noeud2P == noeud4P || noeud3P == noeud4P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = noeud4P = NUL;
      }
   }

   // --- Ici on construit 4 T3 pour vérifier le déterminant du jacobien
   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3
      DReel determinant;

      //--- On forme le premier élément T3
      varEleT3.noeud1P = noeud1P;
      varEleT3.noeud2P = noeud2P;
      varEleT3.noeud3P = noeud3P;
      determinant = varEleT3.calculeDetJ();
      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3
      DReel determinant;

      //--- On forme le premier élément T3
      varEleT3.noeud1P = noeud2P;
      varEleT3.noeud2P = noeud3P;
      varEleT3.noeud3P = noeud4P;
      determinant = varEleT3.calculeDetJ();
      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3
      DReel determinant;

      //--- On forme le premier élément T3
      varEleT3.noeud1P = noeud3P;
      varEleT3.noeud2P = noeud4P;
      varEleT3.noeud3P = noeud1P;
      determinant = varEleT3.calculeDetJ();
      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3
      DReel determinant;

      //--- On forme le premier élément T3
      varEleT3.noeud1P = noeud4P;
      varEleT3.noeud2P = noeud1P;
      varEleT3.noeud3P = noeud2P;
      determinant = varEleT3.calculeDetJ();
      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_Q4");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_IMPORT_EXPORT

#endif  // MGELEMENT_Q4_HPP_DEJA_INCLU

