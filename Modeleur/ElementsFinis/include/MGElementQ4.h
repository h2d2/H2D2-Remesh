//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElementQ4.h
//
// Classe:  MGElementQ4
//
// Description:
//   La classe MGElementQ4 représente un élément bidimensionnel quadrilatéral
//   à quatre noeuds. Cet élément, isoparamétrique et de continuité C0, a une
//   base d'approximation bilinéaire sur l'élément de référence. La classe
//   regroupe toutes les méthodes de calcul sur cet élément.
//
//   La classe est définie par le paramètre template TTTraits.  TTTrait regroupe 
//   les structures nécessaires aux calculs par la méthode des éléments finis. 
//
//   Le trait utilisé doit définir les types suivants : 
//      TCValeur    : type des valeurs porté par une coordonnée
//      TCCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//
// Attributs:
//   MGNoeudP noeud1P;   Les noeuds formant cet élément
//   MGNoeudP noeud2P
//   MGNoeudP noeud3P
//   MGNoeudP noeud4P
//
// Notes:
//
//************************************************************************
// 29-03-1993  François Gingras  Version Initiale
// 21-06-1993  François Gingras  Modifications
// 21-03-1996  Eric Chamberland  Prepost, entêtes, invariant, ajout des
//                               méthodes pour utiliser les classes de SNVno
// 25-03-1996  Eric Chamberland  Méthodes converties à void
// 09-04-1996  Yves Roy          Configuration de la persistence
// 07-06-1996  Eric Chamberland  +limination des méthodes doublées pour les VNOs
// 28-06-1996  Eric Chamberland  Ajout de executeAlgorithme(...)
// 15-07-1996  Eric Chamberland  Ajout de l'interpolation de SNVnoVecteurs
// 12-11-1996  Eric Chamberland  Ajout de importe/exporte
// 15-11-1996  Serge Dufour      Vérification des dépendances
// 19-11-1996  Eric Chamberland  Ajout de compareConnectiviteInferieure
// 16-10-1997  Yves Secretan     Passage à la double précision : 1.ère passe
// 25-11-1997  Yves Roy          Modification concernant les inclusions des vnos.
// 10-12-1997  Yves Secretan     Implante interpole et le calcul integral
// 30-04-1998  Yves Roy          Élimination de interpole de l'interface de l'élément.
// 29-09-1998  Yves Secretan     Migre traceIsosurface dans un algorithme
// 25-07-2003  Maxime Derenne    Nettoyage du code
// 28-07-2003  Maxime Derenne    Ajout des méthodes reqNbrNoeud, reqNoeuds et reqType
// 04-08-2003  Maxime Derenne    Transfert de méthode inline
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson       TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_Q4_H_DEJA_INCLU
#define MGELEMENT_Q4_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"
#include "MGElementT3.hf"

DECLARE_CLASS(CLListe);
#ifdef MODE_PERSISTANT
   DECLARE_CLASS(FIFichier);
#endif

template <typename TTTraits>
class MGElementQ4 : public MGElement<TTTraits>
{
public:
   typedef          MGElementQ4<TTTraits>   TCSelf;
   typedef typename TCSelf::TCCoord         TCCoord;

   typename MGElementQ4<TTTraits>::TCNoeudP noeud1P;
   typename MGElementQ4<TTTraits>::TCNoeudP noeud2P;
   typename MGElementQ4<TTTraits>::TCNoeudP noeud3P;
   typename MGElementQ4<TTTraits>::TCNoeudP noeud4P;

                      MGElementQ4            ();
                      MGElementQ4            (EntierN, typename MGElementQ4<TTTraits>::TCNoeudPP);
   virtual            ~MGElementQ4           ();

   virtual Booleen    compareConnectiviteInferieure(typename MGElementQ4<TTTraits>::ConstTCElementP) const;
   virtual void       executeAlgorithme      (typename MGElementQ4<TTTraits>::TCAlgo&);
   virtual void       executeAlgorithme      (typename MGElementQ4<TTTraits>::TCAlgoConst&) const;

   virtual ERMsg      listeMaille            (CLListeP***,
                                              EntierN, EntierN, EntierN,
                                              DReel, DReel, DReel,
                                              TCCoord&) const;

   virtual Booleen    pointInterieur         (const TCCoord&, const DReel& = 0) const;

   virtual EntierN    reqNbrNoeud            () const;
   virtual void       reqNoeuds              (typename MGElementQ4<TTTraits>::TCNoeudPP &, EntierN);
   virtual void       reqNoeuds              (typename MGElementQ4<TTTraits>::ConstTCNoeudPP &, EntierN) const;
   virtual typename MGElementQ4<TTTraits>::Type reqType() const;
   virtual void       reqVectNoeud           (TCCoord&, EntierN);

   // ---  Approximation éléments finis
   DReel    calculeDetJ          (DReel, DReel) const;
   void     calculeJacobien      (DReel&, DReel&, DReel&, DReel&, DReel&, DReel, DReel) const;
   void     calculeN             (DReel&, DReel&, DReel&, DReel&, DReel, DReel) const;
   void     calculeNx            (DReel&, DReel&, DReel&, DReel&, DReel&, DReel, DReel) const;
   void     calculeNy            (DReel&, DReel&, DReel&, DReel&, DReel&, DReel, DReel) const;
   Booleen  transformationInverse(DReel&, DReel&, const TCCoord&) const;

protected:
   virtual void       invariant              (ConstCarP) const;

private:
   typedef          MGElementT3<typename MGElementQ4<TTTraits>::TCTraits>     TCElementT3;
   typedef          TCElementT3*              TCElementT3P;
   typedef const    TCElementT3*              ConstTCElementT3P;

   ERMsg listeMailleT3   (CLListeP***, EntierN, EntierN, EntierN,
                          DReel, DReel, DReel, typename MGElementQ4<TTTraits>::TCCoord&, TCElementT3P) const;

#ifdef MODE_IMPORT_EXPORT
public:
   virtual void       exporte                (FIFichier&) const;
   virtual void       importe                (FIFichier&, const CLListe&);
#endif //MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
protected:
   virtual void       ecrisVirtuel           (FIFichier&) const;
   virtual void       lisVirtuel             (FIFichier&);
#endif   // MODE_PERSISTANT
};

//**************************************************************
// Description:
//   Vérifie que les quatre pointeurs soient soit simultanément NUL, soit
//   non égaux deux à deux, donc que l'élément ne contienne pas deux noeuds
//   identiques.
//
// Entrée:
//   CarP methode:     Nom de la méthode qui a violée l'invariant de classe.
//   CarP condition:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTTraits>
inline void MGElementQ4<TTTraits>::invariant(ConstCarP conditionP) const
{
   TCSelf::TCElement::invariant (conditionP);
   INVARIANT(noeud1P != NUL || (noeud2P == NUL && noeud3P == NUL && noeud4P == NUL), conditionP);
   INVARIANT(noeud2P != NUL || (noeud1P == NUL && noeud3P == NUL && noeud4P == NUL), conditionP);
   INVARIANT(noeud3P != NUL || (noeud1P == NUL && noeud2P == NUL && noeud4P == NUL), conditionP);
   INVARIANT(noeud4P != NUL || (noeud1P == NUL && noeud2P == NUL && noeud3P == NUL), conditionP);
   INVARIANT(noeud1P == NUL || (noeud1P != noeud2P && noeud1P != noeud3P && noeud1P != noeud4P && noeud2P != noeud3P && noeud2P != noeud4P && noeud3P != noeud4P), conditionP);
}

#include "MGElementQ4.hpp"

#endif  // MGELEMENT_Q4_H_DEJA_INCLU
