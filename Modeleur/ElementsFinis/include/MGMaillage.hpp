//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGMaillage.hpp
// Classe:  MGMaillage
//************************************************************************
// 13-03-1996  Yves Roy          Transfert de méthode inline
// 25-07-2003  Maxime Derenne    Nettoyage du code
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TCTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 02-02-2004  Maude Giasson     Ajout de quelques static_cast<Entier>
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
// 05-07-2004  Maude Giasson          Ajout de reqNbrDim(..)
//************************************************************************
#ifndef MGMAILLAGE_HPP_DEJA_INCLU
#define MGMAILLAGE_HPP_DEJA_INCLU

#include "clchaine.h"
#include "clliste.h"
//#include "cllstsch.h"
//#include "clvect.h"

#include "MGConst.h"

#include "MGElementP1.h"
#include "MGElementL2.h"
#include "MGElementL3.h"
#include "MGElementL3L.h"
#include "MGElementQ4.h"
#include "MGElementT3.h"
#include "MGElementT6.h"
#include "MGElementT6L.h"

#include "MGTableLocalisation.h"

#ifdef MODE_PERSISTANT
#include "fifichie.h"
#include "fifltmth.h"
#include "fimnpflt.h"
#endif // MODE_PERSISTANT

#include <algorithm>
#include <math.h>
#include <set>
#include <sstream>

#ifdef MODE_NAMESPACE
   using std::less;
   using std::max;
   using std::pair;
   using std::set;
#endif
//************************************************************************
// Description: Appelle les constructeurs des différentes listes. Initialise
//    les attributs.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
MGMaillage<TTTraits>::MGMaillage(TypeMaillage type)
   :  coordMax(TCCoord::GRAND)
   ,  coordMin(TCCoord::PETIT)
   ,  maillageParentP(NUL)
   ,  typeMaillage(type)
   ,  nbrNoeud(0)
   ,  nbrElemTot(0)
   ,  indElemP1(0)
   ,  indElemL2(0)
   ,  indElemL3(0)
   ,  indElemL3L(0)
   ,  indElemT3(0)
   ,  indElemT6L(0)
   ,  indElemT6(0)
   ,  indElemQ4(0)
   ,  indElemQ9(0)
   ,  indElemTH4(0)
   ,  indElemH8(0)
   ,  tableLocalisationP(NUL)
#ifdef MODE_PERSISTANT
   ,  dictTempNoNoeudIndiceP(NUL)
#endif // MODE_PERSISTANT
{   

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}   // MGMaillage::MGMaillage()

//************************************************************************
// Description:
//    Destructeur: Détruit les entités de toutes les listes seulement si
//    le maillage est un maillage parent. La table de localisation par contre
//    est toujours détruite.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
MGMaillage<TTTraits>::~MGMaillage ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   vide();
}   // MGMaillage::~MGMaillage

//************************************************************************
// Sommaire:   Ajoute un élément au maillage.
//
// Description:
//    La méthode publique <code>asgElement(...)</code> crée un élément à partir
//    du type, du numéro d'élément et des connectivités passés en paramètre et
//    l'ajoute à la table des éléments maintenue par l'objet.
//
// Entrée:
//    TypeElement elemType,      // Type de l'élément à créer
//    EntierN noElem,            // numéro de l'élément
//    EntierN nbrNod,            // Nombre de noeuds de l'élément
//    EntierN connec[]           // Vecteur des connectivités
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::asgElement (TypeElement elemType,
                                        EntierN noElem,
#ifdef MODE_DEBUG
                                        EntierN nbrNod,
#else
                                        EntierN /*nbrNod*/,
#endif
                                        EntierN connec[])
{
#ifdef MODE_DEBUG 
   PRECONDITION((elemType != TCElement::TYPE_P1)  || (nbrNod == 1));
   PRECONDITION((elemType != TCElement::TYPE_L2)  || (nbrNod == 2));
   PRECONDITION((elemType != TCElement::TYPE_L3)  || (nbrNod == 3));
   PRECONDITION((elemType != TCElement::TYPE_L3L) || (nbrNod == 3));
   PRECONDITION((elemType != TCElement::TYPE_T3)  || (nbrNod == 3));
   PRECONDITION((elemType != TCElement::TYPE_T6L) || (nbrNod == 6));
   PRECONDITION((elemType != TCElement::TYPE_T6)  || (nbrNod == 6));
   PRECONDITION((elemType != TCElement::TYPE_Q4)  || (nbrNod == 4));
   for (EntierN i=0; i < nbrNod; i++)
   {
      PRECONDITION(connec[i] < nbrNoeud);
   }
   PRECONDITION(typeMaillage != SOUSDOMAINE);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   switch (elemType)
   {
      case TCElement::TYPE_NIL :
         ASSERTION(elemType != TCElement::TYPE_NIL);
         break;
      case TCElement::TYPE_P1 :
      {
         TCElementP1P elemP = new TCElementP1;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         lstElem.insere(elemP, indElemL2);
         indElemL2  = indElemL2  + 1;
         indElemL3  = indElemL3  + 1;
         indElemL3L = indElemL3L + 1;
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }

      case TCElement::TYPE_L2 :
      {
         TCElementL2P elemP = new TCElementL2;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         elemP->noeud2P = TCNoeudP(lstNoeud[connec[1]]);
         lstElem.insere(elemP, indElemL3);
         indElemL3  = indElemL3  + 1;
         indElemL3L = indElemL3L + 1;
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }
      case TCElement::TYPE_L3 :
      {
         TCElementL3P elemP = new TCElementL3;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         elemP->noeud2P = TCNoeudP(lstNoeud[connec[1]]);
         elemP->noeud3P = TCNoeudP(lstNoeud[connec[2]]);
         lstElem.insere(elemP, indElemL3L);
         indElemL3L = indElemL3L + 1;
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }
      case TCElement::TYPE_L3L :
      {
         TCElementL3LP elemP = new TCElementL3L;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         elemP->noeud2P = TCNoeudP(lstNoeud[connec[1]]);
         elemP->noeud3P = TCNoeudP(lstNoeud[connec[2]]);
         lstElem.insere(elemP, indElemT3);
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }
      case TCElement::TYPE_T3 :
      {
         TCElementT3P elemP = new TCElementT3;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         elemP->noeud2P = TCNoeudP(lstNoeud[connec[1]]);
         elemP->noeud3P = TCNoeudP(lstNoeud[connec[2]]);
         lstElem.insere(elemP, indElemT6L);
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }
      case TCElement::TYPE_T6L:
      {
         TCElementT6LP elemP = new TCElementT6L;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         elemP->noeud2P = TCNoeudP(lstNoeud[connec[1]]);
         elemP->noeud3P = TCNoeudP(lstNoeud[connec[2]]);
         elemP->noeud4P = TCNoeudP(lstNoeud[connec[3]]);
         elemP->noeud5P = TCNoeudP(lstNoeud[connec[4]]);
         elemP->noeud6P = TCNoeudP(lstNoeud[connec[5]]);
         lstElem.insere(elemP, indElemT6);
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }
      case TCElement::TYPE_T6 :
      {
         TCElementT6P elemP = new TCElementT6;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         elemP->noeud2P = TCNoeudP(lstNoeud[connec[1]]);
         elemP->noeud3P = TCNoeudP(lstNoeud[connec[2]]);
         elemP->noeud4P = TCNoeudP(lstNoeud[connec[3]]);
         elemP->noeud5P = TCNoeudP(lstNoeud[connec[4]]);
         elemP->noeud6P = TCNoeudP(lstNoeud[connec[5]]);
         lstElem.insere(elemP, indElemQ4);
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }
      case TCElement::TYPE_Q4 :
      {
         TCElementQ4P elemP = new TCElementQ4;
         elemP->asgNoElement(noElem);
         elemP->noeud1P = TCNoeudP(lstNoeud[connec[0]]);
         elemP->noeud2P = TCNoeudP(lstNoeud[connec[1]]);
         elemP->noeud3P = TCNoeudP(lstNoeud[connec[2]]);
         elemP->noeud4P = TCNoeudP(lstNoeud[connec[3]]);
         lstElem.insere(elemP, indElemQ9);
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      }
      case TCElement::TYPE_Q9 :
         ASSERTION(elemType != TCElement::TYPE_Q9);
         break;
      case TCElement::TYPE_TH4:
         ASSERTION(elemType != TCElement::TYPE_TH4);
         break;
      case TCElement::TYPE_H8 :
         ASSERTION(elemType != TCElement::TYPE_H8);
         break;
   }
   ++nbrElemTot;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::asgElement(TypeElement, EntierN, ...)

//************************************************************************
// Sommaire:   Ajoute un élément au maillage.
//
// Description:
//    La méthode publique <code>asgElement(...)</code> ajoute l'élement passé
//    en paramètre à la table des éléments maintenue par l'objet. Il est de la
//    responsabilité de la méthode appelante de maintenir le noeud en vie
//    tant que l'objet est actif, car il n'y a pas de copie.
//
// Entrée:
//    TCElementP elemP                // Pointeur vers l'élément à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::asgElement(TCElementP elemP)
{
#ifdef MODE_DEBUG
   PRECONDITION(elemP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   typename TCElement::Type elemType = elemP->reqType();
   switch (elemType)
   {
      case TCElement::TYPE_NIL :
         ASSERTION(elemType != TCElement::TYPE_NIL);
         break;
      case TCElement::TYPE_P1 :
         lstElem.insere(elemP, indElemL2);
         indElemL2  = indElemL2  + 1;
         indElemL3  = indElemL3  + 1;
         indElemL3L = indElemL3L + 1;
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_L2 :
         lstElem.insere(elemP, indElemL3);
         indElemL3  = indElemL3  + 1;
         indElemL3L = indElemL3L + 1;
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_L3 :
         lstElem.insere(elemP, indElemL3L);
         indElemL3L = indElemL3L + 1;
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_L3L :
         lstElem.insere(elemP, indElemT3);
         indElemT3  = indElemT3  + 1;
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_T3 :
         lstElem.insere(elemP, indElemT6L);
         indElemT6L = indElemT6L + 1;
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_T6L:
         lstElem.insere(elemP, indElemT6);
         indElemT6  = indElemT6  + 1;
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_T6 :
         lstElem.insere(elemP, indElemQ4);
         indElemQ4  = indElemQ4  + 1;
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_Q4 :
         lstElem.insere(elemP, indElemQ9);
         indElemQ9  = indElemQ9  + 1;
         indElemTH4 = indElemTH4 + 1;
         indElemH8  = indElemH8  + 1;
         break;
      case TCElement::TYPE_Q9 :
         ASSERTION(elemType != TCElement::TYPE_Q9);
         break;
      case TCElement::TYPE_TH4:
         ASSERTION(elemType != TCElement::TYPE_TH4);
         break;
      case TCElement::TYPE_H8 :
         ASSERTION(elemType != TCElement::TYPE_H8);
         break;
   }
   ++nbrElemTot;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage<TTTraits>::asgElement(TCElementP);

//************************************************************************
// Description:
//    Assigne le maillage parent.
//
// Entrée:
//    TCSelfP mailP:
//       Le maillage parent
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::asgMaillageParent (TCConstSelfP mailP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   maillageParentP = mailP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::asgMaillageParent (TCSelfP)

//************************************************************************
// Description:
//    Assigne les coord min et le max du maillage
//
// Entrée:
//
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGMaillage<TTTraits>::asgMinMax (const TCCoord& min, const TCCoord& max)
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif  // ifdef MODE_DEBUG
 
   coordMin = min;
   coordMax = max;
 
#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:
//
// Description:
//    Crée un objet TCNoeud, charge les coordonnées et place le noeud dans
//    la liste.
//
// Entrée:
//    EntierN noNoeud         // Numéro du noeud à assigner
//    EntierN indVno          // Indice Vno du noeud
//    const TCCoord& xyz      // Coordonnées
//
// Sortie:
//
// Notes:
//
//************************************************************************
/*
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::asgNoeud (EntierN noNoeud,
                                      EntierN indVno,
                                      const TCCoord& coord)
{
#ifdef MODE_DEBUG
   PRECONDITION(noNoeud == nbrNoeud);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCNoeudP noeudP = new TCNoeud(noNoeud, indVno, coord);
   asgNoeud (noeudP);

#ifdef MODE_DEBUG
   POSTCONDITION(nbrNoeud == lstNoeud.dimension());
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::asgNoeud*/

//************************************************************************
// Sommaire:   Ajoute un noeud au maillage
//
// Description:
//    La méthode publique <code>asgNoeud(...)</code> crée un noeud à partir
//    du numéro de noeud et de la coordonnée passés en paramètre et l'ajoute à
//    la table des noeuds maintenue par l'objet.
//
// Entrée:
//    EntierN noNoeud         // Numéro du noeud à assigner
//    const TCCoord& xyz      // Coordonnées
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::asgNoeud (EntierN noNoeud,
                                      const TCCoord& coord)
{
#ifdef MODE_DEBUG
   PRECONDITION(noNoeud == nbrNoeud);
   PRECONDITION(typeMaillage == BASE || typeMaillage == PLANDECOUPE);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Crée le noeud
   TCNoeudP noeudP = new TCNoeud(noNoeud, noNoeud, coord);

   // ---  Ajoute le noeud dans la liste des noeuds
   lstNoeud.ajoute(noeudP);
   ++nbrNoeud;

#ifdef MODE_DEBUG
   POSTCONDITION(nbrNoeud == lstNoeud.dimension());
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::asgNoeud

//************************************************************************
// Sommaire:   Ajoute un noeud au maillage
//
// Description:
//    La méthode publique <code>asgNoeud(...)</code> ajoute le noeud passé
//    en paramètre à la table des noeuds maintenue par l'objet. Il est de la
//    responsabilité de la méthode appelante de maintenir le noeud en vie
//    tant que l'objet est actif, car il n'y a pas de copie.
//
// Entrée:
//    TCNoeudP noeudP:           Noeud à ajouter à la liste de noeuds.
//
// Sortie:
//
// Notes:
//   Le noeud est déjà alloué dynamiquement, donc pas à le refaire.
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::asgNoeud (TCNoeudP noeudP)
{
#ifdef MODE_DEBUG
   PRECONDITION(noeudP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Ajoute le noeud dans la liste des noeuds
   lstNoeud.ajoute(VoidP(noeudP));
   ++nbrNoeud;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}  // asgNoeud (TCNoeudP)

//************************************************************************
// Description:
//    Assigne le type de maillage.
//
// Entrée:
//    TypeMaillage type       // le type de maillage
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::asgTypeMaillage (TypeMaillage type)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typeMaillage = type;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::asgTypeMaillage (TypeMaillage)


//****************************************************************************
// Description:  Cette méthode publique permet d'assigner la structure
//               d'information à l'objet.  Pour l'instant, on n'a pas de
//               CQInfoDonneeProjet comme attribut; on redirige vers l'entete.
//
// Entrée:
//    const CQInfoDonneeProjet& info
//
// Sortie:
//
// Notes:
//
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
inline void MGMaillage<TTTraits>::asgInfoDonnee(const CQInfoDonneeProjet& info)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   infoMaillage = info;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_PERSISTANT

//************************************************************************
// Description:
//    Localise l'élément qui contient le point
//
// Entrée:
//    TCCoord &point           // Coordonnées du point à localiser
//
// Sortie:
//     TCElementP&   elemP     // Pointeur à l'élément contenant le point
//                             // elemP=NUL si le point est hors du maillage
//                             // ou de la zone.
// Notes:
//    elemP ne doit pas pointer vers une structure existante (i-e pas de new).
//    Fonctionne uniquement dans le cas d'un maillage à deux dimensions (et plus?)
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::chercheElement(TCElementP& elemP,
                                           const TCCoord& point,
                                           const DReel& epsilon) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
  
   ERMsg msg = ERMsg::OK;

   // --- Crée la table si c'est nécessaire
   if (tableLocalisationP == NUL)
   {
      tableLocalisationP = new TCTableLcls(*this);
   }
   if (msg)
   {
      msg = tableLocalisationP->chercheElement(elemP, point, epsilon); 
   }
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}  // MGMaillage::chercheElement

//************************************************************************
// Description:
//    Localise l'élément qui contient le point: on vérifie d'abord l'élément
//    passé en paramètre.
//
// Entrée:
//    TCCoord           &point     // Coordonnées du point à localiser
//    TCElementP        ideeP      // pointeur à un élément susceptible de
//                                    contenir le point.
//
// Sortie:
//    TCElementP&       elemP     // Pointeur à l'élément contenant le point
//                                // elemP=NUL si le point est hors du maillage
//                                // ou de la zone.
//
// Notes:
//    elemP ne doit pas pointer vers une structure existante (i-e pas de new)
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::chercheElement (TCElementP& elemP,
                                            TCCoord& point,
                                            TCElementP ideeP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
 
   // --- On teste d'abord la suggestion
   if (ideeP->pointInterieur(point))
   {
      elemP = ideeP;
   }
   // --- Le point n'était pas dans l'élément. On parcourt la table de localisation.
   else
   {
       msg = chercheElement(elemP, point);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}  // MGMaillage::chercheElement


//****************************************************************************
// Sommaire:   Cree un TCElement.
//
// Description:
//    La méthode protégée <code>creeElement(...)</code> est une méthode
//    utilitaire qui crée un élément suivant le type passe en parametre.
//
// Entrée:
//    TCElementP& elementP               :   L'element a creer.
//    const TCElement::Type& typeElement :   Le type de l'élément.
//
// Sortie:
//
// Notes:
//****************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::creeElement(TCElementP& elementP, 
                                        const TypeElement& typeElement) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   switch (typeElement)
   {
      case TCElement::TYPE_L2 :
         elementP = new TCElementL2();
      break;
      case TCElement::TYPE_L3 :
         elementP = new TCElementL3();
      break;
      case TCElement::TYPE_L3L :
         elementP = new TCElementL3L();
      break;
      case TCElement::TYPE_T3 :
         elementP = new TCElementT3();
      break;
      case TCElement::TYPE_T6L:
         elementP = new TCElementT6L();
      break;
      case TCElement::TYPE_T6 :
         elementP = new TCElementT6();
      break;
      case TCElement::TYPE_Q4 :
         elementP = new TCElementQ4();
      break;
      case TCElement::TYPE_Q9 :
         ASSERTION(typeElement != TCElement::TYPE_Q9);
      break;
      case TCElement::TYPE_TH4:
         ASSERTION(typeElement != TCElement::TYPE_TH4);
      break;
      case TCElement::TYPE_H8 :
         ASSERTION(typeElement != TCElement::TYPE_H8);
      break;
      default:
         msg = ERMsg(ERMsg::ERREUR, "ERR_TYPE_ELEMENT_INTROUVABLE");
         CLChaine ch;
         ch << Entier(typeElement);
         msg.ajoute(ch.reqConstCarP());
      break;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
 return msg;
}

//************************************************************************
// Description:
//    Requete les coord min et le max du maillage
//
// Entrée:
//
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGMaillage<TTTraits>::reqMinMax (TCCoord& min, TCCoord& max) const
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif  // ifdef MODE_DEBUG
 
   min = coordMin;
   max = coordMax;
 
#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Description:
//    Charge les connectivités d'un élément
//
// Entrée:
//    EntierN  noElem,            // Numéro de l'élément
//
// Sortie:
//    TypeElement&  elemType,          // Type de l'élément
//    EntierN&  nbrNod,            // Nombre de noeuds de l'élément
//    EntierN   connec[]           // Vecteur des connectivités
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::reqElement (TypeElement &elemType,
                                        EntierN &nbrNod,
                                        EntierN connec[],
                                        const EntierN& noElem) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noElem < nbrElemTot);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Cherche l'élément
   TCElementP elemP = TCElementP(lstElem[noElem]);

   // --- Charge les connectivités et le type
   if (noElem < indElemL2)
   {
      elemType = TCElement::TYPE_P1;
      nbrNod   = 1;
     connec[0] = TCElementP1P(elemP)->noeud1P->reqNoNoeud();
   }
   else if (noElem < indElemL3)
   {
      elemType = TCElement::TYPE_L2;
      nbrNod   = 2;
      connec[0] = TCElementL2P(elemP)->noeud1P->reqNoNoeud();
      connec[1] = TCElementL2P(elemP)->noeud2P->reqNoNoeud();
   }
   else if (noElem < indElemL3L)
   {
      elemType = TCElement::TYPE_L3;
      nbrNod   = 3;
      connec[0] = TCElementL3P(elemP)->noeud1P->reqNoNoeud();
      connec[1] = TCElementL3P(elemP)->noeud2P->reqNoNoeud();
      connec[2] = TCElementL3P(elemP)->noeud3P->reqNoNoeud();
   }
   else if (noElem < indElemT3)
   {
      elemType = TCElement::TYPE_L3L;
      nbrNod   = 3;
      connec[0] = TCElementL3LP(elemP)->noeud1P->reqNoNoeud();
      connec[1] = TCElementL3LP(elemP)->noeud2P->reqNoNoeud();
      connec[2] = TCElementL3LP(elemP)->noeud3P->reqNoNoeud();
   }
   else if (noElem < indElemT6L)
   {
      elemType = TCElement::TYPE_T3;
      nbrNod   = 3;
      connec[0] = TCElementT3P(elemP)->noeud1P->reqNoNoeud();
      connec[1] = TCElementT3P(elemP)->noeud2P->reqNoNoeud();
      connec[2] = TCElementT3P(elemP)->noeud3P->reqNoNoeud();
   }
   else if (noElem < indElemT6)
   {
      elemType = TCElement::TYPE_T6L;
      nbrNod   = 6;
      connec[0] = TCElementT6LP(elemP)->noeud1P->reqNoNoeud();
      connec[1] = TCElementT6LP(elemP)->noeud2P->reqNoNoeud();
      connec[2] = TCElementT6LP(elemP)->noeud3P->reqNoNoeud();
      connec[3] = TCElementT6LP(elemP)->noeud4P->reqNoNoeud();
      connec[4] = TCElementT6LP(elemP)->noeud5P->reqNoNoeud();
      connec[5] = TCElementT6LP(elemP)->noeud6P->reqNoNoeud();
   }
   else if (noElem < indElemQ4)
   {
      elemType = TCElement::TYPE_T6;
      nbrNod   = 6;
      connec[0] = TCElementT6P(elemP)->noeud1P->reqNoNoeud();
      connec[1] = TCElementT6P(elemP)->noeud2P->reqNoNoeud();
      connec[2] = TCElementT6P(elemP)->noeud3P->reqNoNoeud();
      connec[3] = TCElementT6P(elemP)->noeud4P->reqNoNoeud();
      connec[4] = TCElementT6P(elemP)->noeud5P->reqNoNoeud();
      connec[5] = TCElementT6P(elemP)->noeud6P->reqNoNoeud();
   }
   else if (noElem < indElemQ9)
   {
      elemType = TCElement::TYPE_Q4;
      nbrNod   = 4;
      connec[0] = TCElementQ4P(elemP)->noeud1P->reqNoNoeud();
      connec[1] = TCElementQ4P(elemP)->noeud2P->reqNoNoeud();
      connec[2] = TCElementQ4P(elemP)->noeud3P->reqNoNoeud();
      connec[3] = TCElementQ4P(elemP)->noeud4P->reqNoNoeud();
   }
   else if (noElem < indElemTH4)
   {
/*                     elemType = TCElement::TYPE_Q9;
                     nbrNod   = 9;
                     connec[0] = TCElementQ9P(elemP)->noeud1P->reqNoNoeud();
                     connec[1] = TCElementQ9P(elemP)->noeud2P->reqNoNoeud();
                     connec[2] = TCElementQ9P(elemP)->noeud3P->reqNoNoeud();
                     connec[3] = TCElementQ9P(elemP)->noeud4P->reqNoNoeud();
                     connec[4] = TCElementQ9P(elemP)->noeud5P->reqNoNoeud();
                     connec[5] = TCElementQ9P(elemP)->noeud6P->reqNoNoeud();
                     connec[6] = TCElementQ9P(elemP)->noeud7P->reqNoNoeud();
                     connec[7] = TCElementQ9P(elemP)->noeud8P->reqNoNoeud();
                     connec[8] = TCElementQ9P(elemP)->noeud9P->reqNoNoeud();
*/
      ASSERTION(noElem >= indElemTH4);
   }
   else if (noElem < indElemH8)
   {
/*                        elemType = TCElement::TYPE_TH4;
                        nbrNod   = 4;
                        connec[0] = TCElementTH4P(elemP)->noeud1P->reqNoNoeud();
                        connec[1] = TCElementTH4P(elemP)->noeud2P->reqNoNoeud();
                        connec[2] = TCElementTH4P(elemP)->noeud3P->reqNoNoeud();
                        connec[3] = TCElementTH4P(elemP)->noeud4P->reqNoNoeud();
*/
      ASSERTION(noElem >= indElemH8);
   }
   else
   {
/*                        elemType = TCElement::TYPE_H8;
                        nbrNod   = 8;
                        connec[0] = TCElementH8P(elemP)->noeud1P->reqNoNoeud();
                        connec[1] = TCElementH8P(elemP)->noeud2P->reqNoNoeud();
                        connec[2] = TCElementH8P(elemP)->noeud3P->reqNoNoeud();
                        connec[3] = TCElementH8P(elemP)->noeud4P->reqNoNoeud();
                        connec[4] = TCElementH8P(elemP)->noeud5P->reqNoNoeud();
                        connec[5] = TCElementH8P(elemP)->noeud6P->reqNoNoeud();
                        connec[6] = TCElementH8P(elemP)->noeud7P->reqNoNoeud();
                        connec[7] = TCElementH8P(elemP)->noeud8P->reqNoNoeud();
*/
      ASSERTION(noElem < indElemH8);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqElement (TypeElement,  ...)

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de début sur les éléments
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de début
//               sur les éléments
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurElement
//
// Notes:        Itérateur sur pointeurs à des éléments
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurElement 
MGMaillage<TTTraits>::reqElementDebut()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   iterateurElement elemI;
   if (lstElem.dimension() == 0)
   {
      elemI = 0;
   }
   else
   {
      elemI = iterateurElement(&lstElem[0]);
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return elemI;
}

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de début sur les éléments
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de début
//               sur les éléments constants
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurElementConst
//
// Notes:
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurElementConst 
MGMaillage<TTTraits>::reqElementDebut() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   iterateurElementConst elemI;
   if (lstElem.dimension() == 0)
   {
      elemI = 0;
   }
   else
   {
      elemI = iterateurElementConst(&lstElem[0]);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return elemI;
}

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de fin sur les éléments
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de fin
//               sur les éléments
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurElement
//
// Notes:        Itérateur sur pointeurs à des éléments
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurElement 
MGMaillage<TTTraits>::reqElementFin()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   iterateurElement elemI;
   if (lstElem.dimension() == 0)
   {
      elemI = 0;
   }
   else
   {
      elemI = iterateurElement(&lstElem[nbrElemTot-1]);
      elemI++;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
  return elemI;
}

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de fin sur les éléments
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de fin
//               sur les éléments constants
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurElementConst
//
// Notes:
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurElementConst 
MGMaillage<TTTraits>::reqElementFin() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   iterateurElementConst elemI;
   if (lstElem.dimension() == 0)
   {
      elemI = 0;
   }
   else
   {
      elemI = iterateurElementConst(&lstElem[nbrElemTot-1]);
      elemI++;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
  return elemI;
}

//************************************************************************
// Description:
//    Retourne le maillage parent.
//
// Entrée:
//
// Sortie:
//    TCSelfP mailP:
//       Le maillage parent que l'on retourne
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::reqMaillageParent(TCConstSelfP &mailP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   mailP = maillageParentP;

#ifdef MODE_DEBUG
   POSTCONDITION(mailP == maillageParentP);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqMaillageParent (TCSelfP&)

//************************************************************************
// Description:
//    Retourne le nombre d'éléments pour un type donné.
//
// Entrée:
//     TypeElement   elemType   // Type de l'élément touché par la requète
//
// Sortie:
//     EntierN&  nbrEle     // Nombre d'éléments du type demandé
//
// Notes:
//
//************************************************************************
/*ERMsg MGMaillage::reqNbrElem (EntierN& nbrEle, EntierN& nbrNod,
                                    TypeElement  elemType) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   switch (elemType)
   {
      case TCElement::TYPE_P1 :
         nbrEle = indElemL2 - indElemP1;
         nbrNod = 1;
         break;
      case TCElement::TYPE_L2 :
         nbrEle = indElemL3 - indElemL2;
         nbrNod = 2;
         break;
      case TCElement::TYPE_L3 :
         nbrEle = indElemL3L - indElemL3;
         nbrNod = 3;
         break;
      case TCElement::TYPE_L3L :
         nbrEle = indElemT3 - indElemL3L;
         nbrNod = 3;
         break;
      case TCElement::TYPE_T3 :
         nbrEle = indElemT6L - indElemT3;
         nbrNod = 3;
         break;
      case TCElement::TYPE_T6L:
         nbrEle = indElemT6 - indElemT6L;
         nbrNod = 6;
         break;
      case TCElement::TYPE_T6 :
         nbrEle = indElemQ4 - indElemT6;
         nbrNod = 6;
         break;
      case TCElement::TYPE_Q4 :
         nbrEle = indElemQ9 - indElemQ4;
         nbrNod = 4;
         break;
      case TCElement::TYPE_Q9 :
         nbrEle = indElemTH4 - indElemQ9;
         nbrNod = 9;
         break;
      case TCElement::TYPE_TH4:
         nbrEle = indElemH8 - indElemTH4;
         nbrNod = 4;
         break;
      case TCElement::TYPE_H8 :
         nbrEle = nbrElemTot - indElemH8;
         nbrNod = 8;
         break;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqNbrElem
*/
//************************************************************************
// Description:
//    Retourne le nombre d'éléments pour un type donné.
//
// Entrée:
//     TypeElement   elemType   // Type de l'élément touché par la requète
//
// Sortie:
//     EntierN&  nbrEle     // Nombre d'éléments du type demandé
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
EntierN MGMaillage<TTTraits>::reqNbrElement(const TypeElement& elemType) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN nbrElement = 0;

   switch (elemType)
   {
      case TCElement::TYPE_NIL : 
         ASSERTION(elemType != TCElement::TYPE_NIL);
         nbrElement = 0;
         break;
      case TCElement::TYPE_P1 : 
         nbrElement = indElemL2 - indElemP1;
         break;
      case TCElement::TYPE_L2 :
         nbrElement = indElemL3 - indElemL2;
         break;
      case TCElement::TYPE_L3 :
         nbrElement = indElemL3L- indElemL3;
         break;
      case TCElement::TYPE_L3L :
         nbrElement = indElemT3 - indElemL3L;
         break;
      case TCElement::TYPE_T3 :
         nbrElement = indElemT6L - indElemT3;
         break;
      case TCElement::TYPE_T6L:
         nbrElement = indElemT6 - indElemT6L;
         break;
      case TCElement::TYPE_T6 :
         nbrElement = indElemQ4 - indElemT6;
         break;
      case TCElement::TYPE_Q4 :
         nbrElement = indElemQ9 - indElemQ4;
         break;
      case TCElement::TYPE_Q9 :
         nbrElement = indElemTH4 - indElemQ9;
         break;
      case TCElement::TYPE_TH4:
         nbrElement = indElemH8 - indElemTH4;
         break;
      case TCElement::TYPE_H8 :
         nbrElement = nbrElemTot - indElemH8;
         break;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return nbrElement;
}   // MGMaillage::reqNbrElement

//************************************************************************
// Description:
//    Retourne le nombre total d'éléments
//
// Entrée:
//
// Sortie:
//    EntierN nbrEle          // nombre total d'éléments
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
EntierN MGMaillage<TTTraits>::reqNbrElemTotal () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return nbrElemTot;
}

//************************************************************************
// Description:
//    Retourne le nombre de noeuds du maillage ainsi que la dimension.
//
// Entrée:
//
// Sortie:
//     EntierN&  nod          // Nombre de noeuds
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
EntierN MGMaillage<TTTraits>::reqNbrNoeud () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return nbrNoeud;

}   // MGMaillage::reqNbrNoeud

//************************************************************************
// Description:
//    Méthode statique
//    Retourne le nombre de dimensions des sommets du maillage.
//
// Entrée:
//
// Sortie:
//     EntierN //nombre de dimension
// Notes:
//
//************************************************************************
template <typename TTTraits>
EntierN MGMaillage<TTTraits>::reqNbrDimCoords()
{
//L'appel de l'invariant ne compile pas 
//car cette méthode est statique.
//#ifdef MODE_DEBUG
//   INVARIANTS("PRECONDITION");
//#endif  // ifdef MODE_DEBUG
   return TCCoord::reqNbrDim();
}

//************************************************************************
// Description:
//    Méthode statique
//    Retourne le nombre de dimensions du maillage
//
// Entrée:
//
// Sortie:
//     EntierN //nombre de dimension
//
// Notes:
//    Pour le moment la méthode est statique et retourne le nombre de 
//    dimensions maximales gérées par le maillage (actuellement les 
//    éléments 0d, 1d et 2d sont gérés). Éventuellement pourrait devenir
//    dynamique et retourner le nombre de dimensions maximale
//    des éléments présents dans le maillage courant.
//************************************************************************
template <typename TTTraits>
EntierN MGMaillage<TTTraits>::reqNbrDim() 
{
   return 2;
}

//************************************************************************
// Description:
//    Retourne le nombre de noeuds du maillage parent.  Remonte la chaîne jusqu'au parent.
//
// Entrée:
//
// Sortie:
//     EntierN&  nod          // Nombre de noeuds
//
// Notes:
//
//************************************************************************
/*EntierN MGMaillage::reqNbrNoeudParent () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN nbrNoeudsTmp = reqNbrNoeud();

   if (maillageParentP != NUL)
   {
      nbrNoeudsTmp = maillageParentP->reqNbrNoeudParent();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return nbrNoeudsTmp;
}   // MGMaillage::reqNbrNoeudParent
*/
//************************************************************************
// Description:
//    Retourne le nombre de type d'éléments utilisé.
//
// Entrée:
//
// Sortie:
//     EntierN&  nbrType     // Nombre de type utilisé
//
// Notes:
//
//************************************************************************
/*ERMsg MGMaillage::reqNbrTypeElement (EntierN& nbrType) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   nbrType = 0;
   if ((indElemL2 - indElemP1) != 0)
      nbrType++;
   if ((indElemL3 - indElemL2) != 0)
      nbrType++;
   if ((indElemL3L- indElemL3) != 0)
      nbrType++;
   if ((indElemT3 - indElemL3L)!= 0)
      nbrType++;
   if ((indElemT6L - indElemT3) != 0)
      nbrType++;
   if ((indElemT6 - indElemT6L) != 0)
      nbrType++;
   if ((indElemQ4 - indElemT6) != 0)
      nbrType++;
   if ((indElemQ9 - indElemQ4) != 0)
      nbrType++;
   if ((indElemTH4 - indElemQ9) != 0)
      nbrType++;
   if ((indElemH8 - indElemTH4) != 0)
      nbrType++;
   if ((nbrElemTot - indElemH8) != 0)
      nbrType++;

#ifdef MODE_DEBUG
   POSTCONDITION(nbrType <= 9);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqNbrTypeElement
*/
//************************************************************************
// Description:
//    Retourne les coordonnées d'un noeud
//
// Entrée:
//    EntierN noNoeud       // numéro du noeud
//
// Sortie:
//    TCCoord  xyz           // vecteur des coordonnées du noeud
//
// Notes:
//
//************************************************************************
/*
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::reqNoeud (TCCoord& xyz, EntierN noNoeud) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noNoeud < nbrNoeud);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCNoeudP noeudP = NUL;

   // ---  Copie l'information de la liste
   lstNoeud.reqElement( (VoidP&)(noeudP), noNoeud);

   // ---  Copie les coordonnées
   xyz = noeudP->reqCoordonnees();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqNoeud (DReel xyz[], EntierN noNoeud)
*/
//************************************************************************
// Description:
//    Retourne le noeud de numéro noNoeud.
//
// Entrée:
//    EntierN noNoeud       // numéro du noeud
//
// Sortie:
//    TCNoeud& noeud        // noeud
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::ConstTCNoeudP 
MGMaillage<TTTraits>::reqNoeud(EntierN noNoeud) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noNoeud < nbrNoeud);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return static_cast<ConstTCNoeudP>(lstNoeud[noNoeud]);
}

//************************************************************************
// Description:
//    Retourne le pointeur du noeud de numéro noNoeud.
//
// Entrée:
//    EntierN noNoeud       // numéro du noeud
//
// Sortie:
//    TCNoeudP& noeud        // pointeur vers un noeud
//
// Notes:
//
//************************************************************************
/*ERMsg MGMaillage::reqNoeud (TCNoeudP& noeudP, EntierN noNoeud) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noNoeud < nbrNoeud);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Copie l'information de la liste
   noeudP = TCNoeudP(lstNoeud[noNoeud]);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqNoeud
*/

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de début sur les noeuds
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de début
//               sur les noeuds
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurNoeud
//
// Notes:
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurNoeud 
MGMaillage<TTTraits>::reqNoeudDebut()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   iterateurNoeud noeudI;
   if (lstNoeud.dimension() == 0)
   {
      noeudI = 0;
   }
   else
   {
      noeudI = iterateurNoeud(&lstNoeud[0]);
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return noeudI;
}

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de début sur les noeuds
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de début
//               sur les noeuds constants
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurNoeudConst
//
// Notes:
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurNoeudConst 
MGMaillage<TTTraits>::reqNoeudDebut() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   iterateurNoeudConst noeudI;
   if (lstNoeud.dimension() == 0)
   {
      noeudI = 0;
   }
   else
   {
      noeudI = iterateurNoeudConst(&lstNoeud[0]);
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return noeudI;
}

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de fin sur les noeuds
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de fin
//               sur les noeuds
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurElement
//
// Notes:
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurNoeud 
MGMaillage<TTTraits>::reqNoeudFin()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   iterateurNoeud noeudI;
   if (lstNoeud.dimension() == 0)
   {
      noeudI = 0;
   }
   else
   {
      noeudI = iterateurNoeud(&lstNoeud[nbrNoeud -1]);
      noeudI++;
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return noeudI;
}

//****************************************************************************
// Sommaire:  Renvoie l'itérateur de fin sur les noeuds
//
// Description:  Cette méthode publique permet d'obtenir l'itérateur de fin
//               sur les noeuds constants
//
// Entrée:
//
// Sortie:       MGMaillage::iterateurNoeudConst
//
// Notes:
//
//****************************************************************************
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::iterateurNoeudConst 
MGMaillage<TTTraits>::reqNoeudFin() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   iterateurNoeudConst noeudI;
   if (lstNoeud.dimension() == 0)
   {
      noeudI = 0;
   }
   else
   {
      noeudI = iterateurNoeudConst(&lstNoeud[nbrNoeud -1]);
      noeudI++;
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return noeudI;
}

//************************************************************************
// Description:
//    Retourne le type de maillage.
//
// Entrée:
//
// Sortie:
//    TypeMaillage& type       // le type de maillage
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGMaillage<TTTraits>::reqTypeMaillage (TypeMaillage& type) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   type = typeMaillage;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}   // MGMaillage::reqTypeMaillage (TypeMaillage)

//************************************************************************
// Description:
//    Dimensionne une liste d'éléments
//
// Entrée:
//    TypeElement elemType         // Type de liste à créer
//    EntierN maxElem          // Dimension de la liste d'éléments
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::dimListeElem (EntierN maxElem)
{
#ifdef MODE_DEBUG
   PRECONDITION(maxElem > 0);
   PRECONDITION(&lstElem != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   lstElem.alloue(maxElem);

   indElemP1 = 0;
   indElemL2 = 0;
   indElemL3 = 0;
   indElemL3L= 0;
   indElemT3 = 0;
   indElemT6L= 0;
   indElemT6 = 0;
   indElemQ4 = 0;
   indElemQ9 = 0;
   indElemTH4= 0;
   indElemH8 = 0;

   nbrElemTot = 0;

   return ERMsg(ERMsg::OK);
}   // MGMaillage<TTTraits>::dimListeElem

//************************************************************************
// Description:
//    Dimensionne la liste des noeuds.
//
// Entrée:
//    EntierN  maxNoeuds        // dimension de la liste
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::dimListeNoeuds (EntierN maxNoeuds)
{
#ifdef MODE_DEBUG
   PRECONDITION(maxNoeuds > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Alloue la liste pour les noeuds
   lstNoeud.alloue(maxNoeuds);
   nbrNoeud = 0;

   return ERMsg(ERMsg::OK);
}   // MGMaillage<TTTraits>::dimListeNoeuds


//************************************************************************
// Sommaire:
//    Produit un fichier de format intermédiaire pour un maillage
//
// Description:
//    Cette méthode publique virtuelle prend les données du maillage et
//    les écrit dans un fichier. Ces donnée seront représenté dans le
//    format intermédiaire
//
// Entrée:
//    FIFichier& os: Le fichier dans lequel on écrit.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      exporteDimensions(os);
   }

   if (os)
   {
      exporteNoeuds(os);
   }

   if (os)
   {
      exporteElements(os);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Exporte les dimensions du maillage.
//
// Description:
//    La méthode protégée <code>exporteDimensions(...)</code> est une méthode
//    utilitaire qui exporte les dimensions du maillage.
//
// Entrée:
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::exporteDimensions(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      os << nbrNoeud << ESPACE
         << nbrElemTot << ESPACE
         << EntierN(2) << ESPACE
         << FINL;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Exporte les noeuds du maillage.
//
// Description:
//    La méthode protégée <code>exporteNoeuds(...)</code> est une méthode
//    utilitaire qui exporte les noeuds du maillage.
//
// Entrée:
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::exporteNoeuds(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   for (EntierN i = 0; os && i < nbrNoeud; ++i)
   {
      exporteUnNoeud(*TCNoeudP(lstNoeud[i]), os);
      os << FINL;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Exporte les éléments du maillage.
//
// Description:
//    La méthode protégée <code>exporteElements(...)</code> est une méthode
//    utilitaire qui exporte les éléments du maillage.
//
// Entrée:
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::exporteElements(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      EntierN indice = 0;
      for (TCElement::Type typeElement = TCElement::TYPE_L2;
           typeElement <= TCElement::TYPE_Q4;
           typeElement = static_cast<TCElement::Type>(typeElement+1))
      {
         EntierN nbrE = reqNbrElement(typeElement);
         os << Entier(typeElement) << " " << nbrE << FINL;
         if (os)
         {
            for (EntierN i=0; i < nbrE; ++i)
            {
               static_cast<TCElementP>(lstElem[indice])->exporte(os);
               os << FINL;
               indice++; // --- Compteur global au maillage
               if (!os)
               {
                  // --- Erreur
                  break;
               }
            }
         }
         if (!os)
         {
            // --- Erreur
            break;
         }
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Exporte un noeud du maillage.
//
// Description:
//    La méthode protégée <code>exporteNoeuds(...)</code> est une méthode
//    utilitaire qui exporte un noeud du maillage.
//
// Entrée:
//    const TCNoeud& noeud;      Le noeud à exporter
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::exporteUnNoeud(const TCNoeud& noeud, FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      const Entier nbrDimExport = 2;
      noeud.exporte(os, nbrDimExport);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:
//    Permet la création d'un maillage à partir d'un fichier dans le
//    format intermédiaire
//
// Description:
//    Cette méthode publique permet de construire un maillage à partir
//    de données contenue dans un fichier de formaqt intermédiaire is
//
// Entrée:
//    FIFichier& is: Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::importe(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Importe les dimensions
   if (is)
   {
      importeDimensions(is);
   }

   if (is)
   {
      // ---  Importe les noeuds
      importeNoeuds(is);

      // ---  Importe les éléments
      if (is || is.reqMsgErreur().reqType() == ERMsg::AVERTISSEMENT)
      {
         importeElements(is);
      }

      // --- En cas d'erreur, libère la structure de données
      if (!is && is.reqMsgErreur().reqType() != ERMsg::AVERTISSEMENT)
      {
         vide();
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Importe les dimensions du maillage.
//
// Description:
//    La méthode protégée <code>importeDimensions(...)</code> est une méthode
//    utilitaire qui importe les dimensions du maillage.
//
// Entrée:
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::importeDimensions(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   CLChaine finLigne;

   // ---  Lis les dimensions
   if (is)
   {
      EntierN nbrNoeuds;
      EntierN nbrElements;
      EntierN nbrDimensions;

      is >> nbrNoeuds
         >> nbrElements
         >> nbrDimensions
         >> finLigne;

      if (is)
      {
         if (nbrDimensions < 2 || nbrDimensions > 3)
         {
            ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NBR_DIMENSION");

            CLChaine ch;
            ch << nbrDimensions;
            msg.ajoute(ch.reqConstCarP());
            msg.ajoute("MSG_2D_OU_3D");

            is.asgErreur(msg);
         }
         else
         {
            nbrNoeud = nbrNoeuds;
            nbrElemTot = nbrElements;
         }
      }
      
/*      if (is)
      {
         parametres.asgNbrNoeud(nbrNoeuds);
         parametres.asgNbrElement(nbrElements);
         parametres.asgNbrDimension(nbrDimensions);
      }
*/
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Importe les noeuds du maillage.
//
// Description:
//    La méthode protégée <code>importeNoeuds(...)</code> est une méthode
//    utilitaire qui importe les noeuds du maillage.
//
// Entrée:
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::importeNoeuds(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef set<EntierN, less<EntierN> > SetEntierN;

   CLChaine finLigne;

   // ---  Lis les noeuds
   if (is)
   {
      if (nbrNoeud > 0)
      {
         lstNoeud.alloue(nbrNoeud);
      }
      // ---  Ajoute le nombre de noeuds pour dimensionner la liste
      for (EntierN i = 0; i < nbrNoeud; ++i)
      {
         lstNoeud.ajoute(new TCNoeud());
      }

      // --- Ici on se crée des Set pour vérifier que les noeuds importés sont uniques
      // --- par leurs numéro de noeud, de vno et par leurs coordonnées.
      typedef set<GOCoordonneesXYZ, less<GOCoordonneesXYZ> > SetCoord;

      SetEntierN setNoNoeud, setNoVno;
      SetCoord   setCoord;

      ERMsg msgAccumule = ERMsg::OK;
      for (EntierN i = 0; is && i < nbrNoeud; ++i)
      {
         Booleen noeudEnErreur = FAUX;

         TCNoeud noeudTmp;
         importeUnNoeud(noeudTmp, is);
         if (!is)
         {
            ERMsg msgIs = is.reqMsgErreur();
            if (msgIs.reqNbrMessages() > 0 &&
                (msgIs[0] == "ERR_NUMEROTATION_NOEUD" ||
                 msgIs[0] == "ERR_NUMEROTATION_VNO"))
            {
               msgAccumule.ajoute(msgIs);
               is.initErreur();
               noeudEnErreur = VRAI;
            }
         }

         if (!is)
         {
            ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_LECTURE_NOEUDS");
            CLChaine chaine;
            chaine << "#" << (i+1);
            msg.ajoute(chaine.reqConstCarP());
            is.asgErreur(msg);
         }

         if (is && !noeudEnErreur)
         {
            EntierN noNoeud = noeudTmp.reqNoNoeud();
            if (noNoeud >= nbrNoeud)
            {
               msgAccumule.asgType(ERMsg::ERREUR);
               msgAccumule.ajoute("ERR_NUMEROTATION_NOEUD");
               msgAccumule.ajoute("MSG_DIMENSION_LST_NOEUDS_INVALIDE");
               msgAccumule.ajoute("MSG_NO_NOEUD_INVALIDE");
               CLChaine chaine;
               chaine << "#" << (noNoeud+1);
               msgAccumule.ajoute(chaine.reqConstCarP());
               noeudEnErreur = VRAI;
            }
         }
         if (is && !noeudEnErreur)
         {
            EntierN noVno = noeudTmp.reqIndVno();
            if (noVno >= nbrNoeud)
            {
               msgAccumule.asgType(ERMsg::ERREUR);
               msgAccumule.ajoute("ERR_NUMEROTATION_VNO");
               msgAccumule.ajoute("MSG_NO_VNO_INVALIDE");
               CLChaine chaine;
               chaine << "#" << (noVno+1);
               msgAccumule.ajoute(chaine.reqConstCarP());
               noeudEnErreur = VRAI;
            }
         }
         if (is && !noeudEnErreur)
         {
            Entier noNoeud = noeudTmp.reqNoNoeud();
            pair<SetEntierN::iterator, bool> reponse = setNoNoeud.insert(noNoeud);
            if (!(reponse.second))
            {
               msgAccumule.asgType(ERMsg::ERREUR);
               msgAccumule.ajoute("ERR_NO_NOEUD_EN_DOUBLE");
               CLChaine chaine;
               chaine << "#" << (noNoeud+1);
               msgAccumule.ajoute(chaine.reqConstCarP());
               noeudEnErreur = VRAI;
            }
         }
         if (is && !noeudEnErreur)
         {
            Entier noVno = noeudTmp.reqIndVno();
            pair<SetEntierN::iterator, bool> reponse = setNoVno.insert(noVno);
            if (!(reponse.second))
            {
               msgAccumule.asgType(ERMsg::ERREUR);
               msgAccumule.ajoute("ERR_NO_VNO_EN_DOUBLE");
               CLChaine chaine;
               chaine << "#" << (noVno+1);
               msgAccumule.ajoute(chaine.reqConstCarP());
               noeudEnErreur = VRAI;
            }
         }
         if (is && !noeudEnErreur)
         {
            const GOCoordonneesXYZ& coordNoeud = noeudTmp.reqCoordonnees();
            pair<SetCoord::iterator, bool> reponse = setCoord.insert(coordNoeud);
            if (!(reponse.second))
            {
               msgAccumule.asgType(ERMsg::ERREUR);
               msgAccumule.ajoute("ERR_COORDONNEE_EN_DOUBLE");
               CLChaine chaine;
               chaine << "#" << (noeudTmp.reqNoNoeud()+1);
               msgAccumule.ajoute(chaine.reqConstCarP());

               chaine = "";
               chaine << coordNoeud.x() << " " << coordNoeud.y() << " " << coordNoeud.z() <<FINL;
               msgAccumule.ajoute(chaine.reqConstCarP());
               noeudEnErreur = VRAI;
            }
         }

         if (is && !noeudEnErreur)
         {
            *TCNoeudP(lstNoeud[noeudTmp.reqNoNoeud()]) = noeudTmp;
         }
      }  // for

      if (! msgAccumule)
      {
         if (! is)
            msgAccumule.ajoute(is.reqMsgErreur());
         is.asgErreur(msgAccumule);
      }
   }

   // --- On assigne les minimums et les maximums
   if (is)
   {
      // --- trouve les minimus et les maximums
      const double DOUBLE_MAXIMUM = 1.0e+125;
      TCCoord minGlb( GOCoordonneesXYZ( DOUBLE_MAXIMUM,  DOUBLE_MAXIMUM,  DOUBLE_MAXIMUM) );
      TCCoord maxGlb( GOCoordonneesXYZ(-DOUBLE_MAXIMUM, -DOUBLE_MAXIMUM, -DOUBLE_MAXIMUM) );
      for (EntierN i = 0; i < nbrNoeud; ++i)
      {
         ConstTCNoeudP noeudP = TCNoeudP(lstNoeud[i]);
         noeudP->reqCoordonnees().minGlobal(minGlb);
         noeudP->reqCoordonnees().maxGlobal(maxGlb);
      }

      if (minGlb == maxGlb)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_MIN_MAX_INVALIDE");
         msg.ajoute("MSG_RECTANGLE_DEGENERE");
         msg.ajoute("MSG_MIN");
         std::stringstream ios;
         ios << minGlb << std::endl;
         msg.ajoute(ios.str());
         msg.ajoute("MSG_MAX");
         ios.str("");
         ios.clear();
         ios << maxGlb << std::endl;
         msg.ajoute(ios.str());
         is.asgErreur(msg);
      }

      if (is)
      {
         asgMinMax(minGlb, maxGlb);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Importe un noeud du maillage.
//
// Description:
//    La méthode protégée <code>importeNoeuds(...)</code> est une méthode
//    utilitaire qui importe un noeud du maillage.
//
// Entrée:
//    TCNoeud& noeud;            Le noeud à importer
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::importeUnNoeud(TCNoeud& noeud, FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      const Entier nbrDimImport = 2;
      noeud.importe(is, nbrDimImport);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Sommaire:   Importe les éléments du maillage.
//
// Description:
//    La méthode protégée <code>importeElements(...)</code> est une méthode
//    utilitaire qui importe les éléments du maillage.
//
// Entrée:
//    FIFichier& is:             Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGMaillage<TTTraits>::importeElements(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef set<EntierN, less<EntierN> > SetEntierN;
   typedef compareConnectiviteInferieure<TTTraits> TCCompareElement;
   typedef set<TCElementP, TCCompareElement> SetElements;

   CLChaine finLigne;

   // ---  Lis les éléments
   if (is)
   {
      SetElements setElements;
      SetEntierN  setNoElements;

      ERMsg msgAccumule = ERMsg::OK;

      if (nbrElemTot > 0)
      {
         lstElem.alloue(nbrElemTot);
      }

      EntierN cElem = 0;
      for (TCElement::Type typeElement = TCElement::TYPE_L2;
           typeElement <= TCElement::TYPE_Q4;
           typeElement=static_cast<TCElement::Type>(typeElement+1))
      {
         Entier  typeE;
         EntierN nbrE;
         is >> typeE >> nbrE >> finLigne;

         if (is)
         {
            if (Entier(typeElement) != typeE)
            {
               ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_TYPE_ELEMENT_INVALIDE");
               CLChaine ch;
               ch << typeE;
               msg.ajoute(ch.reqConstCarP());
               is.asgErreur(msg);
            }
         }
         else
         {
            is.asgErreur(ERMsg(ERMsg::ERREUR, "ERR_LECTURE_TYPE_ELEMENT_ET_NOMBRE"));
         }
         if (!is) break;

         for (EntierN i = 0; is && i < nbrE; ++i)
         {
            TCElementP elementP = NUL;

            ERMsg msg2 = creeElement(elementP, typeElement);
            if (msg2)
            {
               lstElem.ajoute(elementP);
               cElem++;
            }
            else
            {
               is.asgErreur(msg2);
            }

            if (is)
            {
               elementP->importe(is, lstNoeud);
            }
            if (is)
            {
               pair<SetElements::iterator, bool> reponse1 = setElements.insert(elementP);
               if (!(reponse1.second))
               {
                  msgAccumule.asgType(ERMsg::ERREUR);
                  msgAccumule.ajoute("ERR_ELEMENTS_CONNECTIVITE_IDENTIQUE");
                  CLChaine chaine;
                  chaine << "#" << elementP->reqNoElement() << " == ";
                  chaine << "#" << (*(reponse1.first))->reqNoElement();
                  msgAccumule.ajoute(chaine.reqConstCarP());
               }
               pair<SetEntierN::iterator, bool> reponse2 = setNoElements.insert(elementP->reqNoElement());
               if (!(reponse2.second))
               {
                  msgAccumule.asgType(ERMsg::ERREUR);
                  msgAccumule.ajoute("ERR_NO_ELEMENT_EN_DOUBLE");
                  CLChaine chaine;
                  chaine << elementP->reqNoElement();
                  msgAccumule.ajoute(chaine.reqConstCarP());
               }
            }
         }  // for elements

         if (is)
         {
            switch (typeElement)
            {
               case TCElement::TYPE_L2:  indElemL3  = cElem; break;
               case TCElement::TYPE_L3:  indElemL3L = cElem; break;
               case TCElement::TYPE_L3L: indElemT3  = cElem; break;
               case TCElement::TYPE_T3:  indElemT6L = cElem; break;
               case TCElement::TYPE_T6L: indElemT6  = cElem; break;
               case TCElement::TYPE_T6:  indElemQ4  = cElem; break;
            }
         }
      } // for typeElement
      if (! msgAccumule)
      {
         if (! is)
            msgAccumule.ajoute(is.reqMsgErreur());
         is.asgErreur(msgAccumule);
      }

      if (is)
      {
         indElemQ9  = cElem;
         indElemTH4 = cElem;
         indElemH8  = cElem;
      }

      if (is && cElem != nbrElemTot)
      {
         is.asgErreur(ERMsg(ERMsg::ERREUR, "ERR_NBR_ELEMENTS_INVALIDE"));
      }
   }
   if (!is)
   {
      // --- On libère la structure de données
      vide();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//****************************************************************************
// Description:  Méthode qui vide le maillage et le laisse dans un état cohérent
//
// Entrée:
//
// Sortie:
//
// Notes:        On ne vérifie pas l'invariant à l'entrée parce que la méthode
//               vide est utilisée pour se mettre dans un état cohérent lors
//               d'une erreur de l'erreur.  A ce moment, il se peut qu'on ne
//               respecte pas l'invariant de la classe.  A la fin cependant,
//               on va le respecter, permettant ainsi la destruction.
//****************************************************************************
template <typename TTTraits>
void MGMaillage<TTTraits>::vide()
{
   switch (typeMaillage)
   {
      case BASE:
      {
         EntierN nbrN = lstNoeud.dimension();
         if (nbrN > 0)
         {
            for (Entier i=nbrN-1; i>=0; i--)
            {
               delete TCNoeudP(lstNoeud[i]);
            }
         }
         EntierN nbrE = lstElem.dimension();
         if (nbrE > 0)
         {
            for (Entier j=nbrE-1; j>=0; j--)
            {
               delete TCElementP(lstElem[j]);
            }
         }
      }
      break;
      case PEAU:
      {
         EntierN nbrE = lstElem.dimension();
         if (nbrE > 0)
         {
            for (Entier j=nbrE-1; j>=0; j--)
            {
               delete TCElementP(lstElem[j]);
            }
         }
      }
      break;
      case PLANDECOUPE:
      {
         EntierN nbrN = lstNoeud.dimension();
         if (nbrN > 0)
         {
            for (Entier i=nbrN-1; i>=0; i--)
            {
               delete TCNoeudP(lstNoeud[i]);
            }
         }
         EntierN nbrE = lstElem.dimension();
         if (nbrE > 0)
         {
            for (Entier j=nbrE-1; j>=0; j--)
            {
               delete TCElementP(lstElem[j]);
            }
         }
      }
      break;
      case SOUSDOMAINE:
      break;
      default:
      {
         EntierN nbrN = lstNoeud.dimension();
         if (nbrN > 0)
         {
            for (Entier i=nbrN-1; i>=0; i--)
            {
               delete TCNoeudP(lstNoeud[i]);
            }
         }
         EntierN nbrE = lstElem.dimension();
         if (nbrE > 0)
         {
            for (Entier j=nbrE-1; j>=0; j--)
            {
               delete TCElementP(lstElem[j]);
            }
         }
      }
      break;
   }

   lstElem.effacerListe();
   lstNoeud.effacerListe();
   
   nbrNoeud = 0;
   nbrElemTot = 0;

   // ---  Detruit la table de localisation
   if (tableLocalisationP != NUL) delete tableLocalisationP;

  maillageParentP = NUL;
  typeMaillage = BASE;
  indElemP1 = 0;
  indElemL2 = 0;
  indElemL3 = 0;
  indElemL3L= 0;
  indElemT3 = 0;
  indElemT6L = 0;
  indElemT6 = 0;
  indElemQ4 = 0;
  indElemQ9 = 0;
  indElemTH4 =0;
  indElemH8 = 0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Description:
//    Retourne le type de l'élément
//
// Entrée:
//    EntierN  noElem,            // Numéro de l'élément
//
// Sortie:
//    TypeElement&  elemType  // Type de l'élément
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::reqTypeElement (TypeElement &elemType,
                                            EntierN noElem)
{
#ifdef MODE_DEBUG
   PRECONDITION(noElem < nbrElemTot);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

// --- Charge les connectivités et le type
   if (noElem < indElemL2)
      elemType = TCElement::TYPE_P1;
   else if (noElem < indElemL3)
      elemType = TCElement::TYPE_L2;
   else if (noElem < indElemL3L)
      elemType = TCElement::TYPE_L3;
   else if (noElem < indElemT3)
      elemType = TCElement::TYPE_L3L;
   else if (noElem < indElemT6L)
      elemType = TCElement::TYPE_T3;
   else if (noElem < indElemT6)
      elemType = TCElement::TYPE_T6L;
   else if (noElem < indElemQ4)
      elemType = TCElement::TYPE_T6;
   else if (noElem < indElemQ9)
      elemType = TCElement::TYPE_Q4;
   else if (noElem < indElemTH4)
      elemType = TCElement::TYPE_Q9;
   else if (noElem < indElemH8)
      elemType = TCElement::TYPE_TH4;
   else
      elemType = TCElement::TYPE_H8;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqTypeElement(TypeElement&, EntierN)

//************************************************************************
// Description:
//    Retourne l'élément correspondant au numéro donné.
//
// Entrée:
//    EntierN     noElem          // Numéro de l'élément
//
// Sortie:
//    TCElementP& elemP           // Pointeur à l'élément
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
ERMsg MGMaillage<TTTraits>::reqElement (TCElementP& elemP,
                                        EntierN noElem) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noElem < nbrElemTot);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   elemP = TCElementP(lstElem[noElem]);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}   // MGMaillage::reqElement

//****************************************************************************
// Description:  Cette méthode publique reqInfoDonnee() permet d'obtenir la
//               structure d'information stockée dans l'objet.  Pour l'instant
//               on va chercher le renseignement dans l'entête.
//
// Entrée:
//
// Sortie:       Retourne un CQInfoDonneeProjet par valeur.
//
// Notes:
//
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
CQInfoDonneeProjet MGMaillage<TTTraits>::reqInfoDonnee() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return infoMaillage;
}
#endif // MODE_PERSISTANT

//****************************************************************************
// Sommaire:   Traduis l'indice en une adresse de noeud.
//
// Description:
//    La méthode protégée <code>traduisIndiceEnSNNoeudP(...)</code> retourne
//    l'adresse de noeud qui correspond à l'indice passé en paramètre (cast
//    en TCNoeudP).
//
// Entrée:
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::TCNoeudP 
MGMaillage<TTTraits>::traduisIndiceEnTCNoeudP(const TCNoeudP& nP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(reinterpret_cast<const EntierN>(nP) < lstNoeud.dimension());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const EntierN ind = reinterpret_cast<const EntierN>(nP);
   TCNoeudP noeudP = TCNoeudP(lstNoeud[ind]);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return noeudP;
}
#endif // MODE_PERSISTANT

//****************************************************************************
// Sommaire:   Traduis l'adresse du noeud en un indice.
//
// Description:
//    La méthode protégée <code>traduisSNNoeudPEnIndice(...)</code> retourne
//    l'indice qui correspond à l'adresse de noeud.
//
// Entrée:
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
inline typename MGMaillage<TTTraits>::TCNoeudP 
MGMaillage<TTTraits>::traduisTCNoeudPEnIndice(const TCNoeudP& nP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(dictTempNoNoeudIndiceP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN ind = dictTempNoNoeudIndiceP[nP->reqNoNoeud()];
   TCNoeudP resP = reinterpret_cast<TCNoeudP>(ind);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return resP;
}
#endif // MODE_PERSISTANT


//****************************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGMaillage<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   PRECONDITION(dictTempNoNoeudIndiceP == NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Monte la table de correspondance noNoeud -> indice
   EntierN noeudMax = 0;
   for (EntierN i = 0; i < nbrNoeud; ++i)
   {
      noeudMax = max(TCNoeudP(lstNoeud[i])->reqNoNoeud(), noeudMax);
   }
   dictTempNoNoeudIndiceP = new EntierN[noeudMax+1];
   for (EntierN i = 0; i < nbrNoeud; ++i)
   {
      dictTempNoNoeudIndiceP[TCNoeudP(lstNoeud[i])->reqNoNoeud()] = i;
   }

   // ---  Ecris les paramètres
   if (os)
   {
      EntierN pasDeTemps = 0;   // Pour la compatibilité avec SNElementsFinis
      os << nbrNoeud    << " "
         << reqNbrDim() << " "
         << pasDeTemps  << " "
         << nbrElemTot  << " "
         << filtre(coordMin) << " "
         << filtre(coordMax) << FINL;
   }

   // ---  Ecris les noeuds
   if (os)
   {
      for (EntierN i = 0; os && i < nbrNoeud; ++i)
      {
         TCNoeudP noeudP = TCNoeudP(lstNoeud[i]);
         dictTempNoNoeudIndiceP[noeudP->reqNoNoeud()] = i;
         os << *noeudP << FINL;
      }
   }

   // ---  Ecris les éléments
   if (os)
   {
      os << indElemL2 << " " << indElemL3 << " " << indElemL3L << " "
         << indElemT3 << " " << indElemT6L << " "
         << indElemT6 << " " << indElemQ4 << " "
         << indElemQ9 << " " << indElemTH4 << " "
         << indElemH8 << FINL;
   }
   if (os)
   {
      os.ajouteFiltre(creeFiltre(this, &MGMaillage<TTTraits>::traduisTCNoeudPEnIndice));

      EntierN indice = 0;
      for (TCElement::Type typeElement = TCElement::TYPE_L2;
           os && typeElement <= TCElement::TYPE_H8;
           typeElement=static_cast<TCElement::Type>(typeElement+1))
      {
         EntierN nbrE = reqNbrElement(typeElement);
         os << Entier(typeElement) << " " << nbrE << FINL;
         if (os)
         {
            for (EntierN i = 0; os && i < nbrE; ++i)
            {
               os << *TCElementP(lstElem[indice]) << FINL;
               indice++; // --- Compteur global au maillage
            }
         }
      }
      os.retireFiltre(typeid(TCNoeudP).name());
   }

   // ---  Récupère la mémoire
   delete [] dictTempNoNoeudIndiceP;
   dictTempNoNoeudIndiceP = NUL;

#ifdef MODE_DEBUG
   POSTCONDITION(dictTempNoNoeudIndiceP == NUL);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_PERSISTANT


//****************************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGMaillage<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   CLChaine finLigne;
   vide();

   // ---  Lis les paramètres
   if (is)
   {
      EntierN pasDeTemps;     // Pour la compatibilité avec SNElementsFinis
      EntierN nbrDimension;
      is >> nbrNoeud
         >> nbrDimension
         >> pasDeTemps
         >> nbrElemTot
         >> filtre(coordMin)
         >> filtre(coordMax)
         >> finLigne;
   }

   // ---  Crée le nombre de noeuds nécessaires
   if (is)
   {
      if (nbrNoeud > 0)
      {
         lstNoeud.alloue(nbrNoeud);
      }
      for (EntierN i = 0; i < nbrNoeud; ++i)
      {
         lstNoeud.ajoute(new TCNoeud());
      }
   }

   // ---  Lis les noeuds
   if (is)
   {
      for (EntierN i = 0; is && i < nbrNoeud; ++i)
      {
         TCNoeudP noeudP = TCNoeudP(lstNoeud[i]);
         is >> (*noeudP) >> finLigne;

/*      !!!!!!!!!!!!!!!!!!!!!
         if (is)
         {
            if (noeudP->reqNoNoeud() >= nbrNoeud ||
                noeudP->reqNoVno()   >= nbrNoeud)
            {
               is.asgErreur(ERMsg(ERMsg::ERREUR, "ERR_DIMENSION_LST_NOEUDS_INVALIDE"));
            }
         }
*/
      }
   }

   TCElement::Type typeDebut = TCElement::TYPE_NIL;
   TCElement::Type typeFin   = TCElement::TYPE_H8;
   if (is)
   {
      CLChaine buf;
      is >> buf;
      Booleen lu = FAUX;
      if (is)  // Version Modeleur2 avec P1
      {
         std::istringstream isb(buf);
         isb >> indElemP1
             >> indElemL2 >> indElemL3  >> indElemL3L
             >> indElemT3 >> indElemT6L >> indElemT6 >> indElemQ4 >> indElemQ9
             >> indElemTH4>> indElemH8;
         if (isb) 
         {
            typeDebut = TCElement::TYPE_P1;
            lu = VRAI;
         }
      }
      if (is && ! lu)  // Version Modeleur1 sans P1
      {
         std::istringstream isb(buf);
         isb >> indElemL2 >> indElemL3  >> indElemL3L
             >> indElemT3 >> indElemT6L >> indElemT6 >> indElemQ4 >> indElemQ9
             >> indElemTH4>> indElemH8;
         if (isb)
            typeDebut = TCElement::TYPE_L2;
         else
            is.asgErreur( ERMsg(ERMsg::ERREUR, "ERR_TYPE_ELEMENT_INVALIDE") );
      }
   }
   if (is)
   {
      ASSERTION(typeDebut != TCElement::TYPE_NIL);
      is.ajouteFiltre(creeFiltre(this, &MGMaillage<TTTraits>::traduisIndiceEnTCNoeudP));

      if (nbrElemTot > 0)
      {
         lstElem.alloue(nbrElemTot);
      }
      EntierN cElem = 0;
      for (TCElement::Type typeElement = typeDebut;
           is && typeElement <= typeFin;
           ++typeElement)
      {
         Entier  typeE;
         EntierN nbrE;
         is >> typeE >> nbrE >> finLigne;
         if (typeDebut == TCElement::TYPE_L2) // Pour Modeleur1
            typeE += TCElement::TYPE_L2;
         if (typeElement != typeE)
         {
            is.asgErreur(ERMsg(ERMsg::ERREUR, "ERR_TYPE_ELEMENT_INVALIDE"));
            break;
         }

         if (is)
         {
            for (EntierN i = 0; is && i < nbrE; ++i)
            {
               TCElementP elementP = NUL;
               ERMsg msg = ERMsg::OK;
               msg = creeElement(elementP, typeElement);
               if (!msg)
               {
                  is.asgErreur(msg);
               }
               if (is)
               {
                  is >> (*elementP);
                  cElem++;
               }
               if (is)
               {
                  lstElem.ajoute(elementP);
               }
            }
         }
      }
      if (cElem != nbrElemTot)
      {
         is.asgErreur(ERMsg(ERMsg::ERREUR, "ERR_NBR_ELEMENTS_INVALIDE"));
      }

      is.retireFiltre(typeid(TCNoeudP).name());
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_PERSISTANT

//****************************************************************************
// Description:
//    Méthode friend à la classe MGMaillage qui permet d'écrire un objet
//    MGMaillage dans un fichier utilisant FIFichier.
//
// Entrée:
//    const MGMaillage& maillage: le maillage à écrire.
//
// Sortie:
//    FIFichier& os : le fichier dans lequel on écrit.
//
// Notes:
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
FIFichier& operator<<(FIFichier& os, const MGMaillage<TTTraits>& maillage)
{
   if (os)
   {
      maillage.ecrisVirtuel(os);
   }
   return os;
}
#endif // MODE_PERSISTANT

//****************************************************************************
// Description:
//    Méthode friend à la classe SNElementsFinis qui permet de lire un objet
//    SNElementsFinis à partir d'un fichier FIFichier.
//
// Entrée:
//    FIFichier& is: le fichier duquel on lit.
//
// Sortie:
//    MGMaillage& maillage: le maillage à remplir.
//
// Notes:
//****************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
FIFichier& operator>>(FIFichier& is, MGMaillage<TTTraits>& maillage)
{
   if (is)
   {
      maillage.lisVirtuel(is);
   }
   return is;
}
#endif // MODE_PERSISTANT


#endif  // MGMAILLAGE_HPP_DEJA_INCLU
