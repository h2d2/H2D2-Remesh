//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     $Id$
//
// Classe:      MGTableLocalisation
//
// Sommaire:    Interface de définition de la classe MGTableLocalisation
//
// Description: 
//    Instanciation de la classe template MGTableLocalisation<T>
//    avec EFMaillage2D.
//
// Attributs:
//
// Note:
//
//************************************************************************
#ifndef MGTABLELOCALISATION_EFMAILLAGE2D_H_DEJA_INCLU
#define MGTABLELOCALISATION_EFMAILLAGE2D_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "EFMaillage2D.h"
#include "MGTableLocalisation.hf"
#include "MGTableLocalisationT3.h"

template <>
class MGTableLocalisation<EFMaillage2D>
   : public MGTableLocalisationT3<EFMaillage2D>
{
public:
            MGTableLocalisation(const EFMaillage2D& m) : MGTableLocalisationT3<EFMaillage2D>(m) {}
   virtual ~MGTableLocalisation() {}
};

#endif //   MGTABLELOCALISATION_EFMAILLAGE2D_H_DEJA_INCLU

