//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: MGElementL2.hpp
// Classe : MGElementL2
//************************************************************************
// 28-07-2003  Maxime Derenne      Version initiale
// 04-08-2003  Maxime Derenne      Transfert de méthode inline
// 25-10-2003  Olivier Kaczor      Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor      Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor      Rendre compilable sur GCC
// 16-02-2004  Maude Giasson       TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_L2_HPP_DEJA_INCLU
#define MGELEMENT_L2_HPP_DEJA_INCLU

#include "MGConst.h"
#include "GOEpsilon.h"

#include "MGElementAlgorithme.h"
#include "MGConstElementAlgorithme.h"

#include <math.h>

//**************************************************************
// Description:
//   Constructeur par défaut, aucun argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementL2<TTTraits>::MGElementL2()
: TCSelf::TCElement(0),
  noeud1P(NUL),
  noeud2P(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementL2 ()


//**************************************************************
// Description:
//   Initialise le numéro de l'élément ainsi que ses deux noeuds
//   correspondants.
//
// Entrée:
//   EntierN element     : le numéro de l'élément qui relie les deux noeuds
//   TCNoeudPP &noeuds   : Référence sur un tableau de pointeur sur des noeuds.
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementL2<TTTraits>::MGElementL2(EntierN element, 
  typename MGElementL2<TTTraits>::TCNoeudPP noeudsPP)
: TCSelf::TCElement(element),
  noeud1P(noeudsPP[0]),
  noeud2P(noeudsPP[1])
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementL2


//**************************************************************
// Description:
//   ne fait rien car aucune allocation dynamique n'a été faite
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementL2<TTTraits>::~MGElementL2()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}  // ~MGElementL2 ()

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::calculeN(DReel& n1, DReel& n2,
                                     DReel ksi) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   n1 = 0.5*(1.0 - ksi);
   n2 = 0.5*(1.0 + ksi);

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//    Calcule le déterminant de la transformation tau
//
// Entrée:
//
// Sortie:     DReel&  la valeur du déterminant
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
DReel MGElementL2<TTTraits>::calculeDetJ() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel l = dist(noeud2P->reqCoordonnees(), noeud1P->reqCoordonnees());
   DReel det = 0.5 * l;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return det;
}

//**************************************************************
// Sommaire:
//    Calcule le jacobien de la transformation.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::calculeJacobien(DReel& j11,
                                            DReel& detJ,
                                            DReel /*ksi*/) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel l = dist(noeud2P->reqCoordonnees(), noeud1P->reqCoordonnees());

   detJ = 0.5 * l;
   j11  = 1.0 / detJ;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::calculeNx(DReel& n1x, DReel& n2x,
                                      DReel& detJ,
                                      DReel  ksi) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel j11;
   calculeJacobien(j11, detJ, ksi);

   n1x = -j11;
   n2x =  j11;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::calculeNy(DReel& n1y, DReel& n2y,
                                      DReel& detJ,
                                      DReel ksi) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel j11;
   calculeJacobien(j11, detJ, ksi);

   n1y = -j11;
   n2y =  j11;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Compare les connectivités et le type d'élément.
//
// Description: Compare les connectivités et le type d'élément.  Retourne VRAI
//              si le type d'élément est inférieur ou si c'est le mème type
//              d'élément, mais que la connectivité  soit inférieure.  Pour que
//              la connectivité soit considérée inférieure, il faut qu'une fois
//              les NoNoeuds ordonnés des deux éléments cet élément-ci (this) possède
//              un noeud avec un # inférieur dans les n premiers noeuds comparés
//              à ceux de l'autre élément.
//
// Entrée:
//   ConstMGElementP elem          : Element à comparer
//
// Sortie:
//   Booleen                           : VRAI ou FAUX
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
Booleen MGElementL2<TTTraits>::compareConnectiviteInferieure(typename MGElementL2<TTTraits>::ConstTCElementP elemP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef MGElementL2<typename MGElementL2<TTTraits>::TCTraits> const * ConstTCSelfP;
   Booleen reponse = FAUX;

   if (this->reqType() != elemP->reqType())
   {
      reponse = this->reqType() < elemP->reqType();
   }
   else
   {
      typename MGElementL2<TTTraits>::TCNoeudP noNo1P, noNo2P;
      noNo1P = noeud1P;
      noNo2P = noeud2P;

      if (noNo2P < noNo1P)
      {
         const typename MGElementL2<TTTraits>::TCNoeudP noTmpP = noNo1P;
         noNo1P = noNo2P;
         noNo2P = noTmpP;
      }

      typename MGElementL2<TTTraits>::TCNoeudP elemNoNo1P, elemNoNo2P;
      ASSERTION(dynamic_cast<ConstTCSelfP>(elemP) != NUL);
      ConstTCSelfP elemL2P = static_cast<ConstTCSelfP>(elemP);
      elemNoNo1P = elemL2P->noeud1P;
      elemNoNo2P = elemL2P->noeud2P;

      if (elemNoNo2P < elemNoNo1P)
      {
         const typename MGElementL2<TTTraits>::TCNoeudP noTmpP = elemNoNo1P;
         elemNoNo1P = elemNoNo2P;
         elemNoNo2P = noTmpP;
      }

      if (noNo1P != elemNoNo1P)
      {
         reponse = noNo1P < elemNoNo1P;
      }
      else if (noNo2P != elemNoNo2P)
      {
         reponse = noNo2P < elemNoNo2P;
      }
      else
      {
         reponse = FAUX;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return reponse;
}

//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description: Méthode de sauvegarde virtuelle qui sauvegarde les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementL2<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::ecrisVirtuel(os);
   }

   if (os)
   {
      os << filtre(noeud1P) << ESPACE
         << filtre(noeud2P);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_PERSISTANT
//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementL2
//
// Entrée: TCAlgo& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::executeAlgorithme(typename MGElementL2<TTTraits>::TCAlgo& algo)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementL2(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementL2.  L'algorithme ne peut
//              modifier l'élément puisque l'élément est constant.
//
// Entrée: TCAlgoConst& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::executeAlgorithme(typename MGElementL2<TTTraits>::TCAlgoConst& algo) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementL2(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
//************************************************************************
// Sommaire: Exporte l'élément
//
// Description: Méthode d'exportation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on s'exporte
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementL2<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::exporte(os);
   }
   if (os)
   {
      os << (noeud1P->reqNoNoeud()+1) << ESPACE
         << (noeud2P->reqNoNoeud()+1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire: Importe l'élément
//
// Description: Méthode d'importation de l'élément
//
// Entrée:
//   FIFichier& os              : Fichier duquel on s'importe
//   const CLListe& lstNoeuds   : liste contenant les pointeurs aux noeuds.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementL2<TTTraits>::importe(FIFichier& is, const CLListe& lstNoeuds)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if(is)
   {
      TCElement::importe(is,lstNoeuds);
   }

   EntierN n1, n2;
   if (is)
   {
      is >> n1 >> n2;
   }
   if (is)
   {
      if (n1 < 1 || n2 < 1)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NUMEROTATION_NOEUD");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_L2");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
      else
      {
         n1--; n2--;
      }
   }
   if (is)
   {
      EntierN nbrNoeuds = lstNoeuds.dimension();
      if( n1 < nbrNoeuds && n2 < nbrNoeuds )
      {
         noeud1P = TCNoeudP(lstNoeuds[n1]);
         noeud2P = TCNoeudP(lstNoeuds[n2]);
      }
      else
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_CONNECTIVITE_INVALIDE");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_L2");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      if (noeud1P == noeud2P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_L2");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = NUL;
      }
   }

   if (is)
   {
      if (noeud1P->reqCoordonnees() == noeud2P->reqCoordonnees())
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_COORDONNEES_IDENTIQUES_DANS_ELEMENT");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_L2");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = NUL;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire: Récupère l'élément
//
// Description: Méthode de lecture virtuelle qui lis les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on lit
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementL2<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      TCElement::lisVirtuel(is);
   }

   TCNoeud* n1P;
   TCNoeud* n2P;
   if (is)
   {
      is >> filtre(n1P) >> filtre(n2P);
   }
   if (is)
   {
      noeud1P = n1P;
      noeud2P = n2P;
   }

   if (is)
   {
      if (noeud1P == noeud2P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem;
         noElem << reqNoElement();
         msg.ajoute("MSG_ELEMENT_L2");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = NUL;
      }
   }
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_PERSISTANT

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline ERMsg MGElementL2<TTTraits>::listeMaille(CLListeP***, EntierN, EntierN, EntierN,
                                                DReel, DReel, DReel, TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Description:
//    Détermine si la coordonnée est comprise dans le segment
//    décrit par L2.
//
// Entrée:
//
// Sortie:
//
// Notes: 
//    Écrit de manière temporaire pour des éléments à une seule
//    coordonnée (x seulement) dans le cadre des séries 1D.
//    Ne vérifie pas vraiment si le point est sur le segment. 
//    et n'emploie pas la marge d'erreur donnée en paramètre. A réécrire.
//    //TODO
//************************************************************************
template <typename TTTraits>
inline Booleen MGElementL2<TTTraits>::pointInterieur(
   const TCCoord& coord, 
   const DReel& /*epsilon*/) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   Booleen interieur = FAUX;
   TCCoord coordDebut = noeud1P->reqCoordonnees();
   TCCoord coordFin = noeud2P->reqCoordonnees();

   if (coord == coordDebut
       || coord == coordFin
       || (coord > coordDebut && coord < coordFin)) 
   {interieur = VRAI;}

   return interieur;
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline void MGElementL2<TTTraits>::reqVectNoeud(TCCoord &, EntierN)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
Booleen 
MGElementL2<TTTraits>::transformationInverse(DReel& ksi,
                                             const typename TCSelf::TCCoord& point) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   // --- On calcule la matrice de transformation associée à l'élément.
   DReel x2x1 = noeud2P->reqCoordonnees()[0] - noeud1P->reqCoordonnees()[0];
   DReel x0x1 = point[0] - noeud1P->reqCoordonnees()[0];

   // --- On calcule la coordonnée ksi sur l'élément de référence
   ksi = x0x1 * (2.0 / x2x1) - 1.0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return !((GOEpsilon(fabs(ksi)) > 1.0));
}

//************************************************************************
// Description:
//    Retourne le nombre de noeud contenu dans l'élément.
//
// Entrée:
//
// Sortie:
//    EntierN : Nombre de noeud.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline EntierN MGElementL2<TTTraits>::reqNbrNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return 2;
}

//************************************************************************
// Description:
//    Retourne le type de l'élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline typename MGElementL2<TTTraits>::Type MGElementL2<TTTraits>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return TCSelf::TYPE_L2;
}

//**************************************************************
// Description:  Opérateur d'intégration sur l'élément
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
/*void MGElementL2::opIntegre (DReel& valeur, const SNVnoScalaire& vnoScalaire) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   valeur = vnoScalaire[noeud1P->reqIndVno()] + vnoScalaire[noeud2P->reqIndVno()];

   GOCoordonneesXYZ vectNorme = noeud2P->coordonnees() - noeud1P->coordonnees();

   valeur *= (0.5 * vectNorme.norme());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} //opIntegre
*/

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::reqNoeuds(typename MGElementL2<TTTraits>::TCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize)
#else
                                      EntierN /*bufferSize*/)
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TTNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementL2<TTTraits>::reqNoeuds(typename MGElementL2<TTTraits>::ConstTCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize) const
#else
                                      EntierN /*bufferSize*/) const
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

#endif  // MGELEMENT_L2_HPP_DEJA_INCLU
