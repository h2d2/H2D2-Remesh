//************************************************************************
// $ID: /cvs/lib_internes/Modeleur2/ElementsFinis/include/MGElementL2.h,v 1.13 2005/11/16 15:03:51 mercieau Exp $
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElementL2.h
//
// Classe:  MGElementL2
//
// Description:
//   La classe MGElementL2 représente un élément unidimensionnel à deux
//   noeuds. Cet élément, isoparamétrique et de continuité C0, a une base
//   d'approximation linéaire sur l'élément de référence. La classe regroupe
//   toutes les méthodes de calcul sur cet élément.
//
//   La classe est définie par le paramètre template TTTraits.  TTTrait regroupe 
//   les structures nécessaires aux calculs par la méthode des éléments finis. 
//
//   Le trait utilisé doit définir les types suivants : 
//      TCValeur    : type des valeurs porté par une coordonnée
//      TCCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//
// Attributs:
//   MGNoeudP noeud1P;   Les noeuds formant cet élément
//   MGNoeudP noeud2P
//
// Notes:
//
//************************************************************************
// 20-01-1993  François Gingras  Version Initiale
// 21-06-1993  François Gingras  Modifications
// 08-12-1995  Eric Paquet       Déclaration des classes avec DECLARE_CLASS
// 21-03-1996  Eric Chamberland  Prepost, entêtes, invariant, ajout des
//                               méthodes pour utiliser les classes de SNVno
// 25-03-1996  Eric Chamberland  Méthodes converties à void
// 09-04-1996  Yves Roy          Configuration de la persistence
// 07-06-1996  Eric Chamberland  +limination des méthodes doublées pour les VNOs
// 28-06-1996  Eric Chamberland  Ajout de executeAlgorithme(...)
// 15-07-1996  Eric Chamberland  Ajout de l'interpolation de SNVnoVecteurs
// 12-11-1996  Eric Chamberland  Ajout de importe/exporte
// 19-11-1996  Eric Chamberland  Ajout de compareConnectiviteInferieure
// 16-10-1997  Yves Secretan     Passage à la double précision : 1.ère passe
// 25-11-1997  Yves Roy          Modification concernant les inclusions des vnos.
// 29-04-1998  Yves Roy          pointInterieur et transformationInverse retournent un Booleen
// 30-04-1998  Yves Roy          Élimination de interpole de l'interface de l'élément.
// 29-09-1998  Yves Secretan     Migre traceIsosurface dans un algorithme
// 25-07-2003  Maxime Derenne    Nettoyage du code
// 28-07-2003  Maxime Derenne    Ajout des méthodes reqNbrNoeud, reqNoeuds et reqType
// 04-08-2003  Maxime Derenne    Transfert de méthode inline
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
// 2005-06-20  Sybil Christen    Rendre compilable sur GCC le constructeur
//                               utilisant des NoeudsVP (vecteur de pointeur)
//************************************************************************
#ifndef MGELEMENT_L2_H_DEJA_INCLU
#define MGELEMENT_L2_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"

#ifdef MODE_PERSISTANT
   DECLARE_CLASS(FIFichier);
#endif
DECLARE_CLASS(CLListe);

template <typename TTTraits>
class MGElementL2 : public MGElement<TTTraits>
{
public:
   typedef          MGElementL2<TTTraits>   TCSelf;
   typedef typename TCSelf::TCCoord         TCCoord;

   typename TCSelf::TCNoeudP noeud1P;
   typename TCSelf::TCNoeudP noeud2P;

                      MGElementL2            ();
                      MGElementL2            (EntierN, typename MGElementL2<TTTraits>::TCNoeudPP);
   virtual            ~MGElementL2           ();

   virtual Booleen    compareConnectiviteInferieure(typename MGElementL2<TTTraits>::ConstTCElementP) const;
   virtual void       executeAlgorithme      (typename MGElementL2<TTTraits>::TCAlgo&);
   virtual void       executeAlgorithme      (typename MGElementL2<TTTraits>::TCAlgoConst&) const;

   virtual ERMsg      listeMaille            (CLListeP***, EntierN, EntierN, EntierN,
                                              DReel, DReel, DReel, TCCoord&) const;

   virtual Booleen    pointInterieur         (const TCCoord&, const DReel& = 0) const;

   virtual EntierN    reqNbrNoeud            () const;
   virtual void       reqNoeuds              (typename MGElementL2<TTTraits>::TCNoeudPP &, EntierN);
   virtual void       reqNoeuds              (typename MGElementL2<TTTraits>::ConstTCNoeudPP &, EntierN) const;
   virtual typename MGElementL2<TTTraits>::Type reqType() const;
   virtual void       reqVectNoeud           (TCCoord&, EntierN);

   // ---  Approximation éléments finis
   DReel    calculeDetJ          () const;
   void     calculeJacobien      (DReel&, DReel&, DReel) const;
   void     calculeN             (DReel&, DReel&, DReel) const;
   void     calculeNx            (DReel&, DReel&, DReel&, DReel) const;
   void     calculeNy            (DReel&, DReel&, DReel&, DReel) const;
   Booleen  transformationInverse(DReel&, const typename TCSelf::TCCoord&) const;

protected:
   virtual void       invariant              (ConstCarP) const;

#ifdef MODE_IMPORT_EXPORT
public:
   virtual void       exporte                (FIFichier&) const;
   virtual void       importe                (FIFichier&, const CLListe&);
#endif   // MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
protected:
   virtual void       ecrisVirtuel           (FIFichier&) const;
   virtual void       lisVirtuel             (FIFichier&);
#endif   // MODE_PERSISTANT
};

//**************************************************************
// Description:
//   Vérifie que les deux pointeurs soient soit simultanément NUL, soit
//   non égaux, donc que l'élément ne contienne pas deux noeuds identiques.
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTTraits>
inline void MGElementL2<TTTraits>::invariant(ConstCarP conditionP) const
{
   TCSelf::TCElement::invariant (conditionP);
   INVARIANT(noeud1P != NUL || noeud2P == NUL, conditionP);
   INVARIANT(noeud2P != NUL || noeud1P == NUL, conditionP);
   INVARIANT(noeud1P == NUL || noeud1P != noeud2P, conditionP);
}

#include "MGElementL2.hpp"

#endif  // MGELEMENT_L2_H_DEJA_INCLU
