//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElement.hpp
// Classe : MGElement
//************************************************************************
// 05-03-1993  François Gingras  Version initiale
// 21-06-1993  François Gingras  Modifié
// 08-12-1995  Eric Paquet       Déclaration des classes avec DECLARE_CLASS
// 11-03-1996  Eric Chamberland  Révision des entêtes, implantation des VNOs
// 01-04-1996  Eric Chamberland  Les opérateurs << et >> sont mis inline
// 30-04-1996  André Gagné       Implantation de lisVirtuel et de ecrisVirtuel
// 14-06-1996  André Gagné       Ajout de la méthode reqTypeChaine
// 27-06-1996  Eric Chamberland  Utilisation de .X(), .Y() et .Z() des coordonnees
// 27-05-1997  Steve Bourassa    Effacé un #include inutile
// 16-10-1997  Yves Secretan     Passage à la double précision : 1.ère passe
// 25-07-2003  Maxime Derenne    Nettoyage du code
// 28-07-2003  Maxime Derenne    Retrait des méthodes reqTypeChaine et entierToAscii
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
//************************************************************************
#ifndef MGELEMENT_HPP_DEJA_INCLU
#define MGELEMENT_HPP_DEJA_INCLU

//**************************************************************
// Sommaire:    Ecrit les attribut de la classe dans un fichier
//
// Description: La méthode publique ecrisVirtuel(FIFichier) ecris les
//              attributs de la classe  dans un fichier
//              passé en paramètre
//
// Entrée:      FIFichier& os: Le fichier dans lequel on ecrit
//
// Sortie:
//
// Notes:
//
// Description: Ce constructeur initialise l'attribut noElement
//              avec le paramètre qui lui est passé.
//
// Entrée:
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElement<TTTraits>::MGElement(EntierN noElem)
: noElement(noElem)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Destructeur.
//
// Description: Ce destructeur ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
MGElement<TTTraits>::~MGElement()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Sommaire:
//    Produit un fichier de format intermédiaire pour un maillage
//
// Description:
//    Cette méthode publique virtuelle prend les données du maillage et
//    les écrit dans un fichier. Ces donnée seront représenté dans le
//    format intermédiaire
//
// Entrée:
//    FIFichier& os: Le fichier dans lequel on écrit.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElement<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      os << (noElement+1) << ESPACE;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT


//****************************************************************************
// Sommaire:
//    Permet la création d'un maillage à partir d'un fichier dans le
//    format intermédiaire
//
// Description:
//    Cette méthode publique permet de construire un maillage à partir
//    de données contenue dans un fichier de formaqt intermédiaire is
//
// Entrée:
//    FIFichier& is: Le fichier de format intermédiaire.
//
// Sortie:
//
// Notes:
//****************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElement<TTTraits>::importe(FIFichier& is,const CLListe&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if(is)
   {
      EntierN no;
      is >> no;
      if (no < 1)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NUMEROTATION_ELEMENT");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         CLChaine ch;
         ch << no;
         msg.ajoute(ch.reqConstCarP());

         is.asgErreur(msg);
      }
      if (is)
      {
         noElement = no-1;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT

//**************************************************************
// Sommaire:    Ecrit les attribut de la classe dans un fichier
//
// Description: La méthode publique ecrisVirtuel(FIFichier) ecris les
//              attributs de la classe  dans un fichier
//              passé en paramètre
//
// Entrée:      FIFichier& os: Le fichier dans lequel on ecrit
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
inline void MGElement<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   PRECONDITION(os);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   os << noElement << ESPACE;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT

//**************************************************************
// Sommaire:    Lit les attribut de la classe du fichier
//
// Description: La méthode publique lisVirtuel(FIFichier) lit les
//              attributs de la classe à partir d'un fichier
//              passé en paramètre
//
// Entrée:      FIFichier& is: Le fichier duquel on lit
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
inline void MGElement<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   PRECONDITION(is);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   is >> noElement;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT


//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description:
//    L'opérateur <code>operator<<(...)</code> est l'opérateur d'insertion
//    qui appelle la méthode <code>ecrisVirtuel(...)</code> qui sauvegarde
//    les informations de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
FIFichier& operator<<(FIFichier& os, const MGElement<TTTraits>& element)
{
   element.ecrisVirtuel(os);
   return os;
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: Lis l'élément
//
// Description:
//    L'opérateur <code>operator>>(...)</code> est l'opérateur d'extraction
//    qui appelle la méthode <code>lisVirtuel(...)</code> qui lit les
//    informations de l'élément.
//
// Entrée:
//   FIFichier& is                  : Fichier duquel on lit
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
FIFichier& operator>>(FIFichier& is, MGElement<TTTraits>& element)
{
   element.lisVirtuel(is);
   return is;
}
#endif   // MODE_PERSISTANT

#endif  // MGELEMENT_HPP_DEJA_INCLU

