//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGNoeud.h
//
// Classe:  MGNoeud
//
// Description:
//    La classe MGNoeud représente un noeud d'un maillage. Il est
//    caractérisé par sa position. Il est également porteur d'information.
//    C'est l'implantation C++ d'une structure en C.
//    
//    La classe est générique pour le type de coordonnées. Le type de coordonnée
//    doit fournir un operator[] pour accéder aux valeurs de la coordonnée. 
//
// Attributs:
//    EntierN          noNoeud;           // Numéro du noeud
//    EntierN          noVno  ;           // Numéro de Vno du noeud
//    TTCoord          coord;             // Coordonnées du noeud
//
// Méthodes:
//
// Fonctions:
//
// Notes:
//************************************************************************
// 27-01-1993  Yves Secretan     Version initiale
// 11-03-1996  Eric Chamberland  Ajout des méthodes retNoNoeud et retNoVNO
// 25-03-1996  Eric Chamberland  Ajout des méthodes asgNoNoeud et asgIndVno.
// 07-06-1996  Eric Chamberland  Elimination des attributs val123 et valcouleur
// 25-06-1996  Eric Chamberland  Transfert de noNoeud et coord vers private:
// 16-10-1997  Yves Secretan     Passage à la double précision
// 28-10-1998  Yves Secretan     Remplace #include "ererreur"
// 15-05-2000  Yves Secretan     Paramètres par défaut sur importe/exporte()
// 24-07-2003  Eric Larouche     Nettoyage du code
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTCoord
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 11-02-2004  Maude Giasson     Modifications templates (TT ->TC)
//************************************************************************
#ifndef MGNOEUD_H_DEJA_INCLU
#define MGNOEUD_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#if defined(MODE_IMPORT_EXPORT) || defined(MODE_PERSISTANT)
   DECLARE_CLASS(FIFichier);
#endif   // MODE_IMPORT_EXPORT

template <typename TTCoord> class MGNoeud;

#ifdef MODE_PERSISTANT
template <typename TTCoord> FIFichier&   operator>> (FIFichier&, MGNoeud<TTCoord>&);
template <typename TTCoord> FIFichier&   operator<< (FIFichier&, const MGNoeud<TTCoord>&);
#endif

template <typename TTCoord>
class MGNoeud
{
public:
   typedef MGNoeud<TTCoord> TCSelf;
   typedef TTCoord          TCCoord;

   typedef TCSelf*          TCNoeudP;

                           MGNoeud     ();
                           MGNoeud     (const EntierN&, const TCCoord&);
                           MGNoeud     (const EntierN&, const EntierN&, const TCCoord&);
                           MGNoeud     (const TCSelf&);
                           ~MGNoeud    ();

   TCSelf&                 operator=   (const TCSelf&);
   Booleen                 operator==  (const TCSelf&) const;
   Booleen                 operator!=  (const TCSelf&) const;
   Booleen                 operator<   (const TCSelf&) const;

   ERMsg                   asgCoordonnees (const TCCoord&);
   ERMsg                   asgNoNoeud     (const EntierN&);
   ERMsg                   asgIndVno      (const EntierN&);
   const TCCoord&          reqCoordonnees () const;
   EntierN                 reqNoNoeud     () const;
   EntierN                 reqIndVno      () const;

protected:
   void                    invariant   (ConstCarP) const;

private:
   EntierN noNoeud;
   EntierN indVno;
   TCCoord coord;

#ifdef MODE_IMPORT_EXPORT
public:
   void                    exporte     (FIFichier&, EntierN, Booleen = VRAI) const;
   void                    importe     (FIFichier&, EntierN, Booleen = VRAI);
#endif   // MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
public:
#ifndef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   friend FIFichier& operator<< <>(FIFichier&, const MGNoeud<TTCoord>&);
   friend FIFichier& operator>> <>(FIFichier&, MGNoeud<TTCoord>&);
#else
   friend FIFichier& operator<<   (FIFichier&, const MGNoeud<TTCoord>&);
   friend FIFichier& operator>>   (FIFichier&, MGNoeud<TTCoord>&);
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
#endif // MODE_PERSISTANT
};

//**************************************************************
// Description:
//   Vérifie que les trois pointeurs soient soit simultanément NUL, soit
//   non égaux deux à deux, donc que l'élément ne contienne pas deux noeuds
//   identiques.
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTCoord>
inline void MGNoeud<TTCoord>::invariant(ConstCarP /*conditionP*/) const
{
}

#include "MGNoeud.hpp"

#endif  // MGNOEUD_H_DEJA_INCLU
