//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: MGElementP1.hpp
// Classe : MGElementP1
//************************************************************************
// 25-09-2003  Olivier Kaczor    Version initiale
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_P1_HPP_DEJA_INCLU
#define MGELEMENT_P1_HPP_DEJA_INCLU

#include "MGConst.h"
#include "MGElementAlgorithme.h"
#include "MGConstElementAlgorithme.h"

//**************************************************************
// Description:
//   Constructeur par défaut, aucun argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementP1<TTTraits>::MGElementP1()
   :  TCSelf::TCElement(0),
      noeud1P(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementP1 ()


//**************************************************************
// Description:
//   Initialise le numéro de l'élément ainsi que son noeud correspondants.
//
// Entrée:
//   EntierN element     : le numéro de l'élément qui relie les deux noeuds
//   TCNoeudPP &noeuds   : Référence sur un tableau de pointeur sur des noeuds.
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementP1<TTTraits>::MGElementP1(EntierN element,
                                   typename MGElementP1<TTTraits>::TCNoeudPP noeudsPP)
   :  TCSelf::TCElement(element),
      noeud1P(noeudsPP[0])
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementP1 (EntierN, TCNoeudPP)

//**************************************************************
// Description:
//   ne fait rien car aucune allocation dynamique n'a été faite
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementP1<TTTraits>::~MGElementP1()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}  // ~MGElementP1 ()

//************************************************************************
// Sommaire: Compare les connectivités et le type d'élément.
//
// Description: Compare les connectivités et le type d'élément.  Retourne VRAI
//              si le type d'élément est inférieur ou si c'est le mème type
//              d'élément, mais que la connectivité  soit inférieure.  Pour que
//              la connectivité soit considérée inférieure, il faut qu'une fois
//              les NoNoeuds ordonnés des deux éléments cet élément-ci (this) possède
//              un noeud avec un # inférieur dans les n premiers noeuds comparés
//              à ceux de l'autre élément.
//
// Entrée:
//   ConstTCElementP elem          : Element à comparer
//
// Sortie:
//   Booleen                           : VRAI ou FAUX
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
Booleen MGElementP1<TTTraits>::compareConnectiviteInferieure(typename MGElementP1<TTTraits>::ConstTCElementP elemP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef MGElementP1<typename MGElementP1<TTTraits>::TCTraits> const * ConstTCSelfP;
   Booleen reponse = FAUX;

   if (this->reqType() != elemP->reqType())
   {
      reponse = this->reqType() < elemP->reqType();
   }
   else
   {
      typename MGElementP1<TTTraits>::TCNoeudP elemNoNo1P;
      ASSERTION(dynamic_cast<ConstTCSelfP>(elemP) != NUL);
      ConstTCSelfP elemP1P = static_cast<ConstTCSelfP>(elemP);
      elemNoNo1P = elemP1P->noeud1P;

     if (elemNoNo1P != noeud1P)
      {
         reponse = noeud1P < elemNoNo1P;
      }
      else
      {
         reponse = FAUX;
      }
   }

/*#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG*/
   return reponse;
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementP1
//
// Entrée: TCAlgo& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementP1<TTTraits>::executeAlgorithme(typename MGElementP1<TTTraits>::TCAlgo& algo)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementP1(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementP1.  L'algorithme ne peut
//              modifier l'élément puisque l'élément est constant.
//
// Entrée: TCAlgoConst& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementP1<TTTraits>::executeAlgorithme(typename MGElementP1<TTTraits>::TCAlgoConst& algo) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementP1(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline ERMsg MGElementP1<TTTraits>::listeMaille(CLListeP***, EntierN, EntierN, EntierN,
                                                DReel, DReel, DReel, typename MGElementP1<TTTraits>::TCCoord&)const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Description:
//    Le point est à l'intérieur de l'élément P1 s'il coïncide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    On ne fait pas utilisation de la marge d'erreur epsilon passée
//    en paramètre. A réécrire éventuellement.
//************************************************************************
template <typename TTTraits>
inline Booleen 
MGElementP1<TTTraits>::pointInterieur(
   const typename MGElementP1<TTTraits>::TCCoord& coord,
   const DReel& /*epsilon*/) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return (coord == noeud1P->reqCoordonnees()) ? VRAI : FAUX;
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline void MGElementP1<TTTraits>::reqVectNoeud(typename MGElementP1<TTTraits>::TCCoord&, EntierN)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Description:
//    Retourne le nombre de noeud contenu dans l'élément.
//
// Entrée:
//
// Sortie:
//    EntierN : Nombre de noeud.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline EntierN MGElementP1<TTTraits>::reqNbrNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return 1;
}

//************************************************************************
// Description:
//    Retourne le type de l'élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline typename MGElementP1<TTTraits>::Type MGElementP1<TTTraits>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return TCSelf::TYPE_P1;
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementP1<TTTraits>::reqNoeuds(typename MGElementP1<TTTraits>::TCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize)
#else
                                      EntierN /*bufferSize*/)
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = MGElementP1<TTTraits>::noeud1P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TTNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementP1<TTTraits>::reqNoeuds(typename MGElementP1<TTTraits>::ConstTCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize) const
#else
                                      EntierN /*bufferSize*/) const
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = MGElementP1<TTTraits>::noeud1P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline Booleen MGElementP1<TTTraits>::transformationInverse(DReel&, DReel&, DReel&,
                                                            const typename MGElementP1<TTTraits>::TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return FAUX;
}

//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description: Méthode de sauvegarde virtuelle qui sauvegarde les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&                           : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementP1<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::ecrisVirtuel(os);
   }

   if (os)
   {
      os << filtre(noeud1P);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_PERSISTANT


//************************************************************************
// Sommaire: Récupère l'élément
//
// Description: Méthode de lecture virtuelle qui lis les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on lit
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementP1<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      TCElement::lisVirtuel(is);
   }

   TCNoeudP n1P;
   if (is)
   {
      is >> filtre(n1P);
   }
   if (is)
   {
      noeud1P = n1P;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT


//************************************************************************
// Sommaire: Exporte l'élément
//
// Description: Méthode d'exportation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on s'exporte
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT 
template <typename TTTraits>
void MGElementP1<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::exporte(os);
   }
   if (os)
   {
      os << (noeud1P->reqNoNoeud()+1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_IMPORT_EXPORT
//************************************************************************
// Sommaire: Importe l'élément
//
// Description: Méthode d'importation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier duquel on s'importe
//   const CLListe& lstNoeuds   : liste contenant les pointeurs aux noeuds.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementP1<TTTraits>::importe(FIFichier& is, const CLListe& lstNoeuds)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if(is)
   {
      TCElement::importe(is,lstNoeuds);
   }

   EntierN n1;
   if (is)
   {
      is >> n1;
   }
   if (is)
   {
      if (n1 < 1)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NUMEROTATION_NOEUD");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_P1");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
      else
      {
         n1--;
      }
   }
   if (is)
   {
      EntierN nbrNoeuds = lstNoeuds.dimension();
      if (n1 < nbrNoeuds)
      {
         noeud1P = TCNoeudP(lstNoeuds[n1]);
      }
      else
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_CONNECTIVITE_INVALIDE");
         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_P1");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }
  

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_IMPORT_EXPORT

#endif  // MGELEMENT_P1_HPP_DEJA_INCLU

