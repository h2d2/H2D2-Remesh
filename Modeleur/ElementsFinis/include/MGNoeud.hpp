//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGNoeud.hpp
// Classe : MGNoeud
//************************************************************************
// 27-01-1993  Yves Secretan     Version initiale
// 11-03-1996  Eric Chamberland  Ajout des méthodes retNoNoeud et retNoVNO
// 18-03-1996  Yves Roy          Fusion des modifications.
// 25-03-1996  Eric Chamberland  Ajout des méthodes asgNoNoeud et asgIndVno.
// 01-04-1996  Eric Chamberland  Destructeur défini inline
// 18-04-1996  Eric Chamberland  Ajustement pour implantation finale des noVno
// 07-06-1996  Eric Chamberland  +limination des attributs val123 et valcouleur
// 16-10-1997  Yves Secretan     Passage à la double précision : 1.ère passe
// 24-07-2003  Eric Larouche     Nettoyage du code
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGNOEUD_HPP_DEJA_INCLU
#define MGNOEUD_HPP_DEJA_INCLU

#ifdef MODE_IMPORT_EXPORT
#  include "fifichie.h"
#  include "fimnpflt.h"
#endif   // MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire:  Constructeur par défaut.
//
// Description: Constructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline MGNoeud<TTCoord>::MGNoeud()
   : noNoeud(0), indVno(0)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur avec paramètres
//
// Description:
//    Le constructeur permet d'initialiser l'objet avec un numéro de noeud
//    et un TTCoord. L'indice de Vno est identique au numéro de noeud.
//
// Entrée:
//    const EntierN& no         : le numéro du noeud.
//    const TTCoord& c          : la coordonneesXYZ du noeud.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline MGNoeud<TTCoord>::MGNoeud(const EntierN& no, const TTCoord& c)
   : noNoeud(no), indVno(no), coord(c)
{
#ifdef MODE_DEBUG
   PRECONDITION(noNoeud == no);
   PRECONDITION(coord == c);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur avec paramètres
//
// Description:
//    Le constructeur permet d'initialiser l'objet avec un numéro de noeud,
//    l'indice de Vno et un TTCoord.
//
// Entrée:
//    const EntierN& no         : le numéro du noeud.
//    const EntierN& iVno       : l'indice de Vno.
//    const TTCoord& c          : la coordonneesXYZ du noeud.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline MGNoeud<TTCoord>::MGNoeud(const EntierN& no,
                                 const EntierN& iVno,
                                 const TTCoord& c)
   : noNoeud(no), indVno(iVno), coord(c)
{
#ifdef MODE_DEBUG
   PRECONDITION(noNoeud == no);
   PRECONDITION(indVno  == iVno);
   PRECONDITION(coord == c);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur copie
//
// Description: Constructeur copie de la classe.
//
// Entrée:
//    const MGNoeud& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline MGNoeud<TTCoord>::MGNoeud(const MGNoeud<TTCoord>& obj)
   : noNoeud(obj.noNoeud), indVno(obj.indVno), coord(obj.coord)
{
#ifdef MODE_DEBUG
   POSTCONDITION(*this == obj);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur par défaut.
//
// Description: Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline MGNoeud<TTCoord>::~MGNoeud()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Opérateur d'assignation
//
// Description: Opérateur d'assignation de la classe.
//
// Entrée:
//    const MGNoeud& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename MGNoeud<TTCoord>::TCSelf& 
MGNoeud<TTCoord>::operator=(const TCSelf & obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

    if (this != &obj)
    {
       noNoeud = obj.noNoeud;
       indVno  = obj.indVno;
       coord   = obj.coord;
    }
#ifdef MODE_DEBUG
   POSTCONDITION(*this == obj);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return *this;
}

//************************************************************************
// Sommaire:  Opérateur de comparaison d'égalité
//
// Description: Cette méthode permet de comparer l'égalité entre deux objets
//              MGNoeud.  On compare le numéro du noeud et sa position.
//
// Entrée:
//    const MGNoeud& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen MGNoeud<TTCoord>::operator==(const TCSelf& obj) const
{
   return noNoeud == obj.noNoeud && coord == obj.coord && indVno == obj.indVno;
}

//************************************************************************
// Sommaire:  Opérateur de comparaison d'inégalité
//
// Description: Cette méthode permet de comparer l'inégalité entre deux objets
//              MGNoeud.  On compare le numéro du noeud et sa position.
//
// Entrée:
//    const MGNoeud& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen MGNoeud<TTCoord>::operator!=(const TCSelf& obj) const
{
   return !(*this == obj);
}

//************************************************************************
// Sommaire:  Opérateur de comparaison d'infériorité
//
// Description: Cette méthode permet de comparer deux objets
//              MGNoeud.  On compare le numéro du noeud seulement.
//
// Entrée:
//    const MGNoeud& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen MGNoeud<TTCoord>::operator<(const TCSelf& obj) const
{
   return noNoeud < obj.noNoeud;
}

//************************************************************************
// Sommaire:    Retourne une référence à la position du noeud.
//
// Description: Cette méthode publique reqCoordonnees() permet d'obtenir la
//              référence à la position du noeud.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline ERMsg 
MGNoeud<TTCoord>::asgCoordonnees(const TTCoord& c)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   coord = c;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Sommaire:  Assigne le numéro de noeud
//
// Description:  Cette méthode publique asgNoNoeud(...) permet d'assigner
//               le numéro de noeud au noeud.
//
// Entrée:
//    const EntierN& no: le numéro de noeud
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline ERMsg MGNoeud<TTCoord>::asgNoNoeud(const EntierN& no)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   noNoeud = no;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Sommaire:  Assigne le numéro de Vno
//
// Description:  Cette méthode publique asgIndVno(...) permet d'assigner
//               l'indice de Vno au noeud.  Cette indice permet d'aller
//               chercher la valeur au noeud dans un SRChampEF.
//
// Entrée:
//    const EntierN& no: le numéro de Vno
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline ERMsg MGNoeud<TTCoord>::asgIndVno(const EntierN& no)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   indVno = no;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Sommaire:    Retourne une référence constante à la position du noeud.
//
// Description: Cette méthode publique coordonnees() permet d'obtenir la
//              référence constante à la position du noeud.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename MGNoeud<TTCoord>::TCCoord& 
MGNoeud<TTCoord>::reqCoordonnees() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return coord;
}

//************************************************************************
// Sommaire:  Retourne le numéro de noeud.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline EntierN MGNoeud<TTCoord>::reqNoNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return noNoeud;
}

//************************************************************************
// Sommaire:  Retourne le numéro de VNO.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline EntierN MGNoeud<TTCoord>::reqIndVno() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return indVno;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode publique <code>exporte(...)</code> exporte un noeud écrit
//    dans le format intermédiaire. Les noeuds peuvent être exportés en 2D ou
//    en 3D, avec ou sans numéro de VNO.
//
// Entrée:
//    FIFichier& os : le fichier dans lequel écrire.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void MGNoeud<TTCoord>::exporte(FIFichier& os,EntierN nbrDimExport, Booleen avecNoVno) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (os)
   {
      os << (noNoeud + 1) << ESPACE;
      if (avecNoVno)
      {
        os << (indVno + 1) << ESPACE;
      }
      os << filtre(coord, os);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return;
}
#endif   // MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire: Importe un noeud du format intermédiaire.
//
// Description:
//    La méthode publique <code>importe(...)</code> importe un noeud écrit
//    dans le format intermédiaire. Les noeuds peuvent être exportés en 2D ou
//    en 3D, avec ou sans numéro de VNO.
//
// Entrée:
//    FIFichier& os : le fichier dans lequel écrire.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void MGNoeud<TTCoord>::importe(FIFichier& is, EntierN nbrDimImport, Booleen avecNoVno)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef MGNoeud<TTCoord>::TCCoord TCCoord;

   if (is)
   {
      Entier noe;
      Entier vno;
      TCCoord pos;
      
      is >> noe;
      if (avecNoVno)
      {
         is >> vno;
      }
      else
      {
         vno = noe;
      }

      if (pos.reqNbrDim() == nbrDimImport)
      {
         is >> pos;
      }
      else
      {
         TCCoord::TCCoord x(0), y(0), z(0);
         if (nbrDimImport >= 1) is >> x;
         if (nbrDimImport >= 2) is >> y;
         if (nbrDimImport >= 3) is >> z;
         GOCoordXYZ<TCCoord::TCCoord> xyz(x, y, z);
         pos = TCCoord(xyz);
      }

      if (is && noe < 1)
      {
         ERMsg msg = ERMsg::ERREUR;
         msg.ajoute("ERR_NUMEROTATION_NOEUDS_INCORRECTE");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         std::ostringstream os;
         os << "MSG_NO_NOEUD_INVALIDE: " << "#" << noe;
         msg.ajoute(os.str());
         is.asgErreur(msg);
      }
      if (is && vno < 1)
      {
         ERMsg msg = ERMsg::ERREUR;
         msg.ajoute("ERR_NUMEROTATION_VNO");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         std::ostringstream os;
         os << "MSG_NO_VNO_INVALIDE: " << "#" << noe;
         msg.ajoute(os.str());
         is.asgErreur(msg);
      }

      if (is)
      {
         noNoeud= noe - 1;
         indVno = vno - 1;
         coord  = filtre(pos, is);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return;
}
#endif   // MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire:    Méthode friend d'écriture dans un FIFichier
//
// Description: Cette méthode friend à SNNoeud permet d'écriture le contenu
//              de l'objet dans un FIFichier.
//
// Entrée:
//    const MGNoeud& noeud: le noeud à écrire
//
// Sortie:
//    FIFichier& os : le fichier dans lequel écrire.
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTCoord>
FIFichier& operator<<(FIFichier& os, const MGNoeud<TTCoord>& noeud)
{
   if (os)
   {
      os << noeud.noNoeud << " " << noeud.indVno << " " << filtre(noeud.coord);
   }
   return os;
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire:    Méthode friend de lecture dans un FIFichier
//
// Description: Cette méthode friend à SNNoeud permet de lire le contenu
//              de l'objet dans un FIFichier.
//
// Entrée:
//    FIFichier& is : le fichier dans lequel lire.
//
// Sortie:
//    const MGNoeud& noeud: le noeud à lire
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTCoord>
FIFichier& operator>>(FIFichier& is, MGNoeud<TTCoord>& noeud)
{
   if (is)
   {
      EntierN noe;
      EntierN vno;
      MGNoeud<TTCoord>::TCCoord pos;
      is >> noe >> vno >> filtre(pos);
      if (is)
      {
         noeud.noNoeud = noe;
         noeud.indVno  = vno;
         noeud.coord   = pos;
      }
   }
   return is;
}
#endif   // MODE_PERSISTANT

#endif  // MGNOEUD_HPP_DEJA_INCLU
