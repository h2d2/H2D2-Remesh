//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElementAlgorithme.hpp
// Classe : MGElementAlgorithme
//************************************************************************
// 28-06-1996  Eric Chamberland  Version initiale
// 27-05-1997  Steve Bourassa    Déplacé dans le .cpp une méthode  pour éviter le #include
// 24-07-2003  Eric Larouche     Nettoyage du code
// 03-10-2003  Olivier Kaczor    Ajout du type d'élément P1
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENTALGORITHME_HPP_DEJA_INCLU
#define MGELEMENTALGORITHME_HPP_DEJA_INCLU

//**************************************************************
// Sommaire: Constructeur par défaut
//
// Description: Constructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
inline MGElementAlgorithme<TTMaillage>::MGElementAlgorithme()
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Destructeur par défaut
//
// Description: Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
inline MGElementAlgorithme<TTMaillage>::~MGElementAlgorithme()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Itère sur tous les éléments du maillage
//
// Description: Itère sur tous les éléments du maillage, et exécute l'appel
//              d'executeAlgorithme sur tous les éléments.
//
// Entrée:  EFElementsFinis& maillage: maillage sur lequel on itère
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void MGElementAlgorithme<TTMaillage>::itereSurTousMGElements(TCMaillage& maillage)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typename TCMaillage::iterateurElement elemI = maillage.reqElementDebut();
   typename TCMaillage::iterateurElement elemFinI = maillage.reqElementFin();

   while (elemI != elemFinI)
   {
      (*elemI++)->executeAlgorithme(*this);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

#endif //MGELEMENTALGORITHME_HPP_DEJA_INCLU
