//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier:     MGInterpole.h
//
// Classe:      MGInterpole
//
// Sommaire:    Algorithme d'interpolation
//
// Description: Classe qui implante un algorithme d'interpolation de valeurs.
//              Cette classe est template du type de champ de valeurs utilisé.
//              Le champ doit définir un typedef sur TTDonnee qui définit le
//              type de données contenu dans le champ et sur Traits qui regroupe
//              les structures nécessaires aux calculs par la méthode des éléments 
//              finis. De plus, le champ doit avoir un comportement comme un 
//              vecteur traditionnel.
//
// Attributs:   
//             const TTTraits::TCMaillage& m_maillage : référence au maillage source
//              const TTChamp&             m_champ    : référence au champ source
//              TCChamp::TCDonnee          m_resultat : valeur résultante interpolée
//              TCCoord                    m_point    : le point d'interpolation
//              ERMsg                      m_msgErr   : message s'il y a erreur
//
// Notes:
//
//************************************************************************
// 30-04-1998  Yves Roy           Version initiale
// 28-10-1998  Yves Secretan      Remplace #include "ererreur"
// 02-05-2000  Yves Secretan      invariant(...) rendu virtual
// 24-07-2003  Eric Larouche      Nettoyage du code
// 03-10-2003  Olivier Kaczor     Ajout du type d'élément P1
// 25-10-2003  Olivier Kaczor     Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor     Ajout des typedef TTTraits, TTDonnee et TTCoord
// 01-12-2003  Olivier Kaczor     Rendre compilable sur GCC
// 02-02-2004  Maude Giasson      Modification dans les typedef
// 16-02-2004  Maude Giasson      TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGINTERPOLE_H_DEJA_INCLU
#define MGINTERPOLE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGConstElementAlgorithme.h"

template <typename TTChamp>
class MGInterpole
   : public MGConstElementAlgorithme<typename TTChamp::TCMaillage>
{
public:
   typedef          MGInterpole<TTChamp>  TCSelf;
   typedef          TTChamp               TCChamp;
   typedef typename TCChamp::TCMaillage   TCMaillage;

   typedef typename TCChamp::TCDonnee     TCDonnee;
   typedef typename TCMaillage::TCCoord   TCCoord;

                 MGInterpole              (const TCMaillage&, const TCChamp&);
   virtual      ~MGInterpole              ();

   virtual void executeAlgoMGElementP1    (const typename TCSelf::TCElementP1&);
   virtual void executeAlgoMGElementL2    (const typename TCSelf::TCElementL2&);
   virtual void executeAlgoMGElementL3    (const typename TCSelf::TCElementL3&);
   virtual void executeAlgoMGElementL3L   (const typename TCSelf::TCElementL3L&);
   virtual void executeAlgoMGElementQ4    (const typename TCSelf::TCElementQ4&);
   virtual void executeAlgoMGElementT3    (const typename TCSelf::TCElementT3&);
   virtual void executeAlgoMGElementT6    (const typename TCSelf::TCElementT6&);
   virtual void executeAlgoMGElementT6L   (const typename TCSelf::TCElementT6L&);

   ERMsg        interpole                 (TCDonnee&, const TCCoord&, const DReel& = 0.0);

protected:

   virtual void  invariant                (ConstCarP) const;

private:
   const TCMaillage&  m_maillage;
   const TCChamp&     m_champ;
   TCDonnee*          m_resultatP;
   TCCoord            m_point;
   ERMsg              m_msgErr;
};

//**************************************************************
// Description:
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
inline void MGInterpole<TTChamp>::invariant(ConstCarP /*conditionP*/) const
{
}

#include "MGInterpole.hpp"

#endif  // MGINTERPOLE_H_DEJA_INCLU

