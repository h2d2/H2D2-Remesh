//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: MGMaillage.h
//
// Classe:  MGMaillage
//
// Description:
//    La classe MGMaillage est la classe qui regroupe toutes les
//    données sur une simulation numérique par éléments finis: le maillage,
//    les valeurs nodales ainsi que les valeurs élémentaires.
//
//    La classe est définie par le paramètre template TTTraits.  TTTrait  
//    regroupe les structures nécessaires aux calculs par la méthode des 
//    éléments finis. 
//    Le trait utilisé doit définir les types suivants :
//       TCCoord:    type de coordonnées des noeuds et nombre de dimensions
//       TCNoeud:    type de noeuds
//
// Attributs:
//    <p>
//    static EntierN  reqNbrDim()        
//       Nombre de dimensions maximale des éléments du maillage.
//    static EntierN  reqNbrDimCoord()        
//       Nombre de dimensions des coordonnées du maillage.
//    <p>
//    TypeMaillage typeMaillage   Type de maillage (BASE, PEAU...) voir ci-dessous
//    TCSelfP  maillageParentP    Pointeur vers maillage parent s'il y a lieu
//    <p>
//    Types:
//    Le maillage de BASE est celui employé habituellement. Il 
//       possède une liste de nuds ainsi quune liste déléments.
//    Un maillage de PEAU est constitué d'éléments linéaires (L2,L3,L3L)
//       qui correspondent au contour externe (+ internes s'il y a lieu) 
//       du maillage de base. Le maillage de peau maintient une référence 
//       vers le maillage de base duquel il est dérivé (maillageParentP).
//       Il possède ses propres éléments, mais ses noeuds sont des références
//       aux noeuds du maillage parent.
//    Un maillage de SOUSDOMAINE fait référence à un sous-ensemble d'éléments 
//       et de noeuds du maillage parent. Ce maillage maintient une référence 
//       vers son maillage parent (maillageParentP).
//    Un maillage de PLANCOUPE est constitué d'éléments linéaires (L2,L3,L3L)
//       qui correspondent à une coupe à travers un maillage. Ce maillage
//       possède donc des éléments qui lui sont propres et également des 
//       noeuds qui lui sont propres. Ce maillage maintient une référence 
//       vers son maillage parent (maillageParentP).
//    <p>
//   EntierN  nbrNoeud           Nombre de noeuds du maillage
//   CLListe  lstNoeud;          Liste des noeuds
//    <p>
//   TTCoord  coordMin           Les coordonnees mininales du maillage 
//   TTCoord  coordMax           Les coordonnees maximales du maillage
//   ------>  NB: Doivent être assignés, sinon insignifiants! 
//            Par défaut valent -1 et 1
//    <p>
//   EntierN  nbrElemTot;        Nombre total d'éléments
//   CLListe  lstElem;           Liste des éléments
//    <p>
//   EntierN  indElemP1;         Indice du premier élément de type P1
//   EntierN  indElemL2;         Indice du premier élément de type L2
//   EntierN  indElemL3;         Indice du premier élément de type L3
//   EntierN  indElemL3L;        Indice du premier élément de type L3L
//   EntierN  indElemT3;         Indice du premier élément de type T3
//   EntierN  indElemT6L;        Indice du premier élément de type T6L
//   EntierN  indElemT6;         Indice du premier élément de type T6
//   EntierN  indElemQ4;         Indice du premier élément de type Q4
//   EntierN  indElemQ9;         Indice du premier élément de type Q9
//   EntierN  indElemTH4;        Indice du premier élément de type TH4
//   EntierN  indElemH8;         Indice du premier élément de type H8
//    <p>
//   Attributs servant à déterminer à quel élément appartient une
//   coordonnée. N'ont pas à être initialisés:
//   TCCoord  origineTable;
//   DReel    tableDelX;         Pas de la grille de la table de
//   DReel    tableDelY;         localisation
//   DReel    tableDelZ;         
//   EntierN  tableIMax;         Dimension de la table dans les
//   EntierN  tableJMax;         3 dimensions
//   EntierN  tableKMax;
//   CLListeP ***tableMatrice;   Matrice de pointeurs à des liste d'éléments
//
// Notes:
//   
//************************************************************************
// 27-01-1993  Yves Secretan          Version initiales
// 25-02-1993  Yves Secretan          Modifications
// 01-03-1993  Yves Secretan          Modifications
// 25-03-1993  Yves Secretan          Modifications
// 31-03-1993  Yves Secretan          Modifications
// 26-04-1993  Stéphanie Bazin        Modifications
// 25-05-1993  François Gingras       Modifications
// 09-06-1993  Marie-Josée L'Heureux  Modifications
// 18-06-1993  François Gingras       Modifications
// 22-07-1993  François Gingras       Modifications
// 30-11-1993  François Gingras       Modifications
// 08-12-1995  Eric Paquet            Déclaration des classes avec DECLARE_CLASS
// 11-03-1996  Eric Chamberland       Implantation des VNOs, révision des entêtes.
// 18-03-1996  Yves Roy &
//             Eric Chamberland       Fusion de modifications
// 02-04-1996  Yves Roy               Enlève l'entète du maillage
// 14-06-1996  André Gagné            Ajout de la méthode exporte
// 18-06-1996  André Gagné            Ajout de la méthode importe
// 03-07-1996  Eric Chamberland       Ajout des méthodes reqNoeudDebut(), Fin(),
//                                    reqElementDebut(), Fin() const et pas const
// 31-07-1996  Eric Chamberland       Ajout de l'interpolation d'un vecteur à NValeurs
// 11-09-1996  Yves Roy               Révision pour compilation sans warning
// 09-10-1996  Eric Chamberland       Ajout de reqNbrNoeudsParent()
// 15-10-1996  Yves Roy               Révision signature de filtrePeau()...
// 21-11-1996  Serge Dufour           Vérification des dépendances
// 16-10-1997  Yves Secretan          Passage à la double précision : 1.ère passe
// 24-11-1997  Yves Roy               Mis attributs privés dans section protégée.
// 25-11-1997  Yves Roy               Modification concernant les inclusions des vnos.
// 21-04-1998  Yves Roy               On expand le domaine de la table de localisation
//                                    et retour des messages d'erreur.
// 17-06-1998  Yves Roy               Compilation conditionnelle de tout le code
//                                    relié à la persistance.
// 29-09-1998  Yves Secretan          Migre traceIsosurface dans un algorithme
// 26-04-2000  Yves Secretan          Revise l'import-export
// 15-05-2000  Yves Secretan          Ajoute les méthodes importe-exporteUnNoeud()
// 22-05-2000  Yves Secretan          Ajoute l'attribut dictTempNoNoeudIndice
// 25-07-2003  Maxime Derenne         Nettoyage du code
// 25-10-2003  Olivier Kaczor         Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor         Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor         Rendre compilable sur GCC
// 03-02-2004  Maude Giasson          Définir les TTElement(XX) selon les MG.
// 11-02-2004  Maude Giasson          Modifications templates (TT ->TC)
// 05-07-2004  Maude Giasson          Ajout de reqNbrDim(..)
// 04-07-2005  Sybil Christen         Mise à jour de l'entête, détail types de maillages
//              supression des méthodes/attribut ElementParent, plus utilisées.
//************************************************************************
#ifndef MGMAILLAGE_H_DEJA_INCLU
#define MGMAILLAGE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"
#include "MGTableLocalisation.hf"

#include "MGElementP1.hf"
#include "MGElementL2.hf"
#include "MGElementL3.hf"
#include "MGElementL3L.hf"
#include "MGElementQ4.hf"
#include "MGElementT3.hf"
#include "MGElementT6.hf"
#include "MGElementT6L.hf"

#include "clliste.h"

#ifdef MODE_PERSISTANT
#include "cqinfprj.h"
DECLARE_CLASS(FIFichier);
#endif // MODE_PERSISTANT


// --- constantes pour la table de localisation
const EntierN EF_MAILLE_ELEMENT = 4;
const EntierN EF_MAX_MAILLE = 200;

DECLARE_CLASS(CLVecteur);

template <typename TTTraits>
class MGMaillage
{
public:
   typedef          MGMaillage<TTTraits>     TCSelf;
   typedef          TCSelf const*            TCConstSelfP;
   typedef          TTTraits                 TCTraits;

   typedef typename TTTraits::TCCoord        TCCoord;
   typedef typename TTTraits::TCNoeud        TCNoeud;
   typedef          TCNoeud*                 TCNoeudP;
   typedef const    TCNoeud*                 ConstTCNoeudP;

   typedef typename TCTraits::TCElement      TCElement;
   typedef typename TCTraits::TCElementP1    TCElementP1;
   typedef typename TCTraits::TCElementL2    TCElementL2;
   typedef typename TCTraits::TCElementL3    TCElementL3;
   typedef typename TCTraits::TCElementL3L   TCElementL3L;
   typedef typename TCTraits::TCElementQ4    TCElementQ4;
   typedef typename TCTraits::TCElementT3    TCElementT3;
   typedef typename TCTraits::TCElementT6    TCElementT6;
   typedef typename TCTraits::TCElementT6L   TCElementT6L;

   typedef          TCElement*               TCElementP;
   typedef          TCElementP1*             TCElementP1P;
   typedef          TCElementL2*             TCElementL2P;
   typedef          TCElementL3*             TCElementL3P;
   typedef          TCElementL3L*            TCElementL3LP;
   typedef          TCElementT3*             TCElementT3P;
   typedef          TCElementT6*             TCElementT6P;
   typedef          TCElementT6L*            TCElementT6LP;
   typedef          TCElementQ4*             TCElementQ4P;
 //typedef          TCElementQ9*             TCElementQ9P;
 //typedef          TCElementTH4*            TCElementTH4P;
 //typedef          TCElementH8*             TCElementH8P;

   typedef typename TCElement::Type          TypeElement;

   enum TypeMaillage {BASE, PEAU, PLANDECOUPE, SOUSDOMAINE};

   typedef       TCElementP*   iterateurElement;
   typedef const TCElementP*   iterateurElementConst;
   typedef       TCNoeudP*     iterateurNoeud;
   typedef ConstTCNoeudP*      iterateurNoeudConst;

                                MGMaillage   (TypeMaillage = BASE);
   virtual                     ~MGMaillage   ();

   virtual ERMsg                asgElement        (TCElementP);
   virtual ERMsg                asgElement        (TypeElement, EntierN, EntierN, EntierNP);
   virtual ERMsg                asgMaillageParent (TCConstSelfP);
   virtual void                 asgMinMax         (const TCCoord&, const TCCoord&); 
   virtual ERMsg                asgNoeud          (EntierN, const TCCoord&);
   //virtual ERMsg                asgNoeud          (EntierN, EntierN, const TCCoord&);
   virtual ERMsg                asgNoeud          (TCNoeudP);
   virtual ERMsg                asgTypeMaillage   (TypeMaillage);

   virtual ERMsg                chercheElement    (TCElementP&, const TCCoord&, const DReel& = 0) const;
   virtual ERMsg                chercheElement    (TCElementP&, TCCoord&, TCElementP) const;

   virtual ERMsg                reqTypeElement   (TypeElement&, EntierN);
   virtual ERMsg                reqElement       (TCElementP&, EntierN) const;
   virtual ERMsg                reqElement       (TypeElement&, EntierN&, 
                                                  EntierNP, const EntierN&) const;
   ERMsg                        creeElement      (TCElementP&, const TypeElement&) const;

   iterateurElement             reqElementDebut  ();
   iterateurElementConst        reqElementDebut  () const;
   iterateurElement             reqElementFin    ();
   iterateurElementConst        reqElementFin    () const;

   virtual ERMsg                reqMaillageParent(TCConstSelfP&) const;
   virtual void                 reqMinMax        (TCCoord&, TCCoord&) const; 
   EntierN                      reqNbrElement    (const TypeElement&) const;
   EntierN                      reqNbrElemTotal  () const;
   EntierN                      reqNbrNoeud      () const;
   static  EntierN              reqNbrDim        ();
   static  EntierN              reqNbrDimCoords  ();
//   virtual ERMsg                reqNoeud         (TCCoord&, EntierN) const;
   ConstTCNoeudP                reqNoeud         (EntierN) const;

   iterateurNoeud               reqNoeudDebut();
   iterateurNoeudConst          reqNoeudDebut() const;
   iterateurNoeud               reqNoeudFin  ();
   iterateurNoeudConst          reqNoeudFin  () const;

   virtual void                 reqTypeMaillage  (TypeMaillage&) const;

   virtual ERMsg                dimListeElem     (EntierN);
   virtual ERMsg                dimListeNoeuds   (EntierN);

   virtual void                 vide();

protected:
   TCCoord        coordMax;
   TCCoord        coordMin;
   TCConstSelfP   maillageParentP;
   TypeMaillage   typeMaillage;
   EntierN        nbrNoeud;
   EntierN        nbrElemTot;
   EntierN        indElemP1;
   EntierN        indElemL2;
   EntierN        indElemL3;
   EntierN        indElemL3L;
   EntierN        indElemT3;
   EntierN        indElemT6L;
   EntierN        indElemT6;
   EntierN        indElemQ4;
   EntierN        indElemQ9;
   EntierN        indElemTH4;
   EntierN        indElemH8;

   CLListe        lstNoeud;
   CLListe        lstElem;

   typedef MGTableLocalisation<TCSelf> TCTableLcls;
   mutable TCTableLcls* tableLocalisationP;

   void  invariant             (ConstCarP) const;

#ifdef MODE_IMPORT_EXPORT
public:
   virtual void       exporte        (FIFichier&) const;
   virtual void       importe        (FIFichier&);

protected:
   virtual void       exporteDimensions(FIFichier&) const;
   virtual void       exporteElements  (FIFichier&) const;
   virtual void       exporteNoeuds    (FIFichier&) const;
   virtual void       exporteUnNoeud   (const TCNoeud&, FIFichier&) const;
   virtual void       importeDimensions(FIFichier&);
   virtual void       importeElements  (FIFichier&);
   virtual void       importeNoeuds    (FIFichier&);
   virtual void       importeUnNoeud   (TCNoeud&, FIFichier&);

#endif // MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
public:
   void               asgInfoDonnee  (const CQInfoDonneeProjet& info);
   CQInfoDonneeProjet reqInfoDonnee  () const;

#ifndef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   friend FIFichier&  operator<<   <>(FIFichier&, const MGMaillage<TTTraits>&);
   friend FIFichier&  operator>>   <>(FIFichier&, MGMaillage<TTTraits>&);
#else
   friend FIFichier& operator<<      (FIFichier&, const MGMaillage<TTTraits>&);
   friend FIFichier& operator>>      (FIFichier&, MGMaillage<TTTraits>&);
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM

protected:
   virtual void       ecrisVirtuel     (FIFichier&) const;
   virtual void       lisVirtuel       (FIFichier&);
           TCNoeudP   traduisTCNoeudPEnIndice(const TCNoeudP&) const;
           TCNoeudP   traduisIndiceEnTCNoeudP(const TCNoeudP&) const;

private:
   CQInfoDonneeProjet infoMaillage;
   mutable EntierNP   dictTempNoNoeudIndiceP;
#endif // MODE_PERSISTANT
};

//************************************************************************
// Description:
//    Contrôle que les indices soient consistant
//
// Entrée:
//     CarP condition:   PRECONDITION ou POSTCONDITION
//
// Sortie:
//     Fin du programme en cas d'erreur.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline void MGMaillage<TTTraits>::invariant(ConstCarP conditionP) const
{
   EntierN tailleListeNoeuds, tailleListeElements;
   lstNoeud.nbrElem (tailleListeNoeuds);
   lstElem.nbrElem (tailleListeElements);
   INVARIANT(indElemP1  <= indElemL2, conditionP);
   INVARIANT(indElemL2  <= indElemL3, conditionP);
   INVARIANT(indElemL3  <= indElemL3L, conditionP);
   INVARIANT(indElemL3L <= indElemT3, conditionP);
   INVARIANT(indElemT3  <= indElemT6L, conditionP);
   INVARIANT(indElemT6L <= indElemT6, conditionP);
   INVARIANT(indElemT6  <= indElemQ4, conditionP);
   INVARIANT(indElemQ4  <= indElemQ9, conditionP);
   INVARIANT(tailleListeNoeuds == 0 || tailleListeNoeuds == nbrNoeud, conditionP);
}

#include "MGMaillage.hpp"

#endif  // MGMAILLAGE_H_DEJA_INCLU
