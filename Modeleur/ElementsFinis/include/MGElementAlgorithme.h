//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElementAlgorithme.h
//
// Classe:  MGElementAlgorithme
//
// Description:
//         Cette classe implante la notion de visiteur sur des élements de la
//         famille MGElement.  Elle est abstraite et définie les méthodes devant
//         exister dans les héritiers.
//
//   La classe est définie par le paramètre template TTTraits.  TTTrait regroupe 
//   les structures nécessaires aux calculs par la méthode des éléments finis. 
//
//   Le trait utilisé doit définir les types suivants : 
//      TCValeur    : type des valeurs porté par une coordonnée
//      TCCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//      TCElement   : type faisant référence aux éléments
//      TCMaillage  : type du maillage
//      TCElementXX : type des types d'élément XX pour un maillage
//      TCAlgo      : type des algorithmes sur un élément
//      TCAlgoConst : type des algorithmes constants sur un élément
//
// Attributs:
//
// Notes: -  Classe construite en considérant que TTTrait::TTElementXX est un MGElement.
//        -  Il serait bien d'ajouter une fonction virtuelle executeAlgo() qui serait redéfinie
//           dans les classes filles. Les classes filles devraient avoir aussi une fontion 
//           initialise(...) afin d'initialiser ou configurer les données membres avant d'exécuter
//           l'algo. À revoir.
//        -  Pour l'instant la fonction itereSurTousMGElements n'est pas utilisée. Revoir si
//           elle doit être mise à profit ailleurs. 
//************************************************************************
// 28-06-1996  Eric Chamberland  Version initiale
// 12-11-1996  Eric Chamberland  Ajout de l'élément L3L
// 14-11-1996  Serge Dufour      Vérification des dépendances
// 28-10-1998  Yves Secretan     Remplace #include "ererreur"
// 24-07-2003  Eric Larouche     Nettoyage du code
// 03-10-2003  Olivier Kaczor    Ajout du type d'élément P1
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 02-02-2004  Maude Giasson      Définir les TTTraits::TCElementXX comme des TTElementXX
//************************************************************************
#ifndef MGELEMENTALGORITHME_H_DEJA_INCLU
#define MGELEMENTALGORITHME_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

template <typename TTMaillage>
class MGElementAlgorithme
{
public:
   typedef TTMaillage   TCMaillage;
   
   typedef typename TCMaillage::TCElement      TCElement;
   typedef typename TCMaillage::TCElementP1    TCElementP1;
   typedef typename TCMaillage::TCElementL2    TCElementL2;
   typedef typename TCMaillage::TCElementL3    TCElementL3;
   typedef typename TCMaillage::TCElementL3L   TCElementL3L;
   typedef typename TCMaillage::TCElementQ4    TCElementQ4;
   typedef typename TCMaillage::TCElementT3    TCElementT3;
   typedef typename TCMaillage::TCElementT6    TCElementT6;
   typedef typename TCMaillage::TCElementT6L   TCElementT6L;

                MGElementAlgorithme    ();
   virtual     ~MGElementAlgorithme    ();

   virtual void executeAlgoMGElementP1 (TCElementP1&)  = 0;
   virtual void executeAlgoMGElementL2 (TCElementL2&)  = 0;
   virtual void executeAlgoMGElementL3 (TCElementL3&)  = 0;
   virtual void executeAlgoMGElementL3L(TCElementL3L&) = 0;
   virtual void executeAlgoMGElementQ4 (TCElementQ4&)  = 0;
   virtual void executeAlgoMGElementT3 (TCElementT3&)  = 0;
   virtual void executeAlgoMGElementT6 (TCElementT6&)  = 0;
   virtual void executeAlgoMGElementT6L(TCElementT6L&) = 0;

protected:
   virtual void invariant               (ConstCarP) const;
   void         itereSurTousMGElements  (TCMaillage&);
};

//**************************************************************
// Description:
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
inline void MGElementAlgorithme<TTMaillage>::invariant(ConstCarP /*conditionP*/) const
{
}

#include "MGElementAlgorithme.hpp"

#endif  // MGELEMENTALGORITHME_H_DEJA_INCLU
