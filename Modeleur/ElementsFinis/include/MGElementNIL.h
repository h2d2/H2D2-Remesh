//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// $Id$
//
// Classe:  MGElementNIL
//
// Description:
//    La classe MGElementNIL représente un élément invalide utilisé pour tenir
//    la place d'un type d'élément non valide dans un maillage. Par example, 
//    pour un maillage 1D, un élément T3 n'a de sens.
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef MGELEMENTNIL_H_DEJA_INCLU
#define MGELEMENTNIL_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"

DECLARE_CLASS(CLListe);

template <typename TTTraits>
class MGElementNIL : public MGElement<TTTraits>
{
public:
   typedef          MGElementNIL<TTTraits>  TCSelf;
   typedef typename TCSelf::TCCoord         TCCoord;

   typename MGElement<TTTraits>::TCNoeudP noeud1P;
   typename MGElement<TTTraits>::TCNoeudP noeud2P;
   typename MGElement<TTTraits>::TCNoeudP noeud3P;
   typename MGElement<TTTraits>::TCNoeudP noeud4P;
   typename MGElement<TTTraits>::TCNoeudP noeud5P;
   typename MGElement<TTTraits>::TCNoeudP noeud6P;
   typename MGElement<TTTraits>::TCNoeudP noeud7P;
   typename MGElement<TTTraits>::TCNoeudP noeud8P;
   typename MGElement<TTTraits>::TCNoeudP noeud9P;

                      MGElementNIL           ();
                      MGElementNIL           (EntierN, typename MGElementNIL<TTTraits>::TCNoeudPP);
   virtual           ~MGElementNIL           ();

   virtual Booleen    compareConnectiviteInferieure(typename MGElementNIL<TTTraits>::ConstTCElementP) const;
   virtual void       executeAlgorithme      (typename MGElementNIL<TTTraits>::TCAlgo&);
   virtual void       executeAlgorithme      (typename MGElementNIL<TTTraits>::TCAlgoConst&) const;

   virtual ERMsg      listeMaille            (CLListeP***, EntierN, EntierN, EntierN,
                                              DReel, DReel, DReel, TCCoord&) const;

   virtual Booleen    pointInterieur         (const TCCoord&, const DReel& = 0) const;

   virtual EntierN    reqNbrNoeud            () const;
   virtual void       reqNoeuds              (typename MGElementNIL<TTTraits>::TCNoeudPP &, EntierN);
   virtual void       reqNoeuds              (typename MGElementNIL<TTTraits>::ConstTCNoeudPP &, EntierN) const;
   virtual typename MGElementNIL<TTTraits>::Type reqType() const;
   virtual void       reqVectNoeud           (TCCoord&, EntierN);

   // ---  Approximation éléments finis
   DReel calculeDetJ     () const { return 0; }
   DReel calculeDetJ     (DReel) const { return 0; }
   DReel calculeDetJ     (DReel, DReel) const { return 0; }

   void  calculeJacobien (DReel&, DReel&, DReel) const { }
   void  calculeJacobien (DReel&, DReel&, DReel&, DReel&, DReel&, DReel, DReel) const { }

   void  calculeN        (DReel&, DReel&, DReel&, DReel, DReel) const { }
   void  calculeN        (DReel&, DReel&, DReel&, DReel&, DReel, DReel) const { }

   void  calculeNx       (DReel&, DReel&, DReel&, DReel&, DReel, DReel) const { }
   void  calculeNx       (DReel&, DReel&, DReel&, DReel&, DReel&, DReel, DReel) const { }

   void  calculeNy       (DReel&, DReel&, DReel&, DReel&, DReel, DReel) const { }
   void  calculeNy       (DReel&, DReel&, DReel&, DReel&, DReel&, DReel, DReel) const { }

   Booleen transformationInverse  (DReel&, const TCCoord&) const { return FAUX; }
   Booleen transformationInverse  (DReel&, DReel&, const TCCoord&) const { return FAUX; }
   Booleen transformationInverse  (DReel&, DReel&, DReel&, const TCCoord&) const { return FAUX; }
   
protected:

   #ifdef MODE_IMPORT_EXPORT
   public:
      virtual void       exporte                (FIFichier&) const { }
      virtual void       importe                (FIFichier&, const CLListe&) { }
   #endif //MODE_IMPORT_EXPORT

   virtual void       invariant              (ConstCarP) const;
};

//**************************************************************
// Description:
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTTraits>
inline void MGElementNIL<TTTraits>::invariant(ConstCarP conditionP) const
{
   INVARIANT(noeud1P == NUL, conditionP);
   INVARIANT(noeud2P == NUL, conditionP);
   INVARIANT(noeud3P == NUL, conditionP);
   INVARIANT(noeud4P == NUL, conditionP);
   INVARIANT(noeud5P == NUL, conditionP);
   INVARIANT(noeud6P == NUL, conditionP);
   INVARIANT(noeud7P == NUL, conditionP);
   INVARIANT(noeud8P == NUL, conditionP);
   INVARIANT(noeud9P == NUL, conditionP);
   INVARIANT(VRAI, conditionP);
   TCSelf::TCElement::invariant (conditionP);
}

#include "MGElementNIL.hpp"

#endif  // MGELEMENTNIL_H_DEJA_INCLU
