//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElementL3L.h
//
// Classe:  MGElementL3L
//
// Description:
//   La classe MGElementL3L représente un élément unidimensionnel à trois
//   noeuds. Cet élément, isoparamétrique et de continuité C0, a une base
//   d'approximation linéaire sur l'élément de référence. La classe regroupe
//   toutes les méthodes de calcul sur cet élément.
//
//   La classe est définie par le paramètre template TTTraits.  TTTrait regroupe 
//   les structures nécessaires aux calculs par la méthode des éléments finis. 
//
//   Le trait utilisé doit définir les types suivants : 
//      TCValeur    : type des valeurs porté par une coordonnée
//      TCCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//
// Attributs:
//   MGNoeudP noeud1P;   Les noeuds formant cet élément
//   MGNoeudP noeud2P
//   MGNoeudP noeud3P
//
// Notes:
//
//************************************************************************
// 12-11-1996  Eric Chamberland  Version initiale  - copie de L2
// 15-11-1996  Serge Dufour      Vérification des dépendances
// 19-11-1996  Eric Chamberland  Ajout de compareConnectiviteInferieure
// 16-10-1997  Yves Secretan     Passage à la double précision
// 25-11-1997  Yves Roy          Modification concernant les inclusions des vnos.
// 29-04-1998  Yves Roy          pointInterieur et transformationInverse retournent un Booleen
// 30-04-1998  Yves Roy          Élimination de interpole de l'interface de l'élément.
// 29-09-1998  Yves Secretan     Migre traceIsosurface dans un algorithme
// 25-07-2003  Maxime Derenne    Nettoyage du code
// 28-07-2003  Maxime Derenne    Ajout des méthodes reqNbrNoeud, reqNoeuds et reqType
// 04-08-2003  Maxime Derenne    Transfert de méthode inline
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_L3L_H_DEJA_INCLU
#define MGELEMENT_L3L_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"
//#include "snvno.h"  Pour opDerive
#include "clvect.h"

#include "MGElement.h"

#ifdef MODE_PERSISTANT
   DECLARE_CLASS(FIFichier);
#endif
DECLARE_CLASS(CLListe);

template <typename TTTraits>
class MGElementL3L : public MGElement<TTTraits>
{
public:
   typedef          MGElementL3L<TTTraits>  TCSelf;
   typedef typename TCSelf::TCCoord         TCCoord;

   typename MGElementL3L<TTTraits>::TCNoeudP noeud1P;
   typename MGElementL3L<TTTraits>::TCNoeudP noeud2P;
   typename MGElementL3L<TTTraits>::TCNoeudP noeud3P;

                      MGElementL3L           ();
                      MGElementL3L           (EntierN, typename MGElementL3L<TTTraits>::TCNoeudPP);
   virtual            ~MGElementL3L          ();

   virtual Booleen    compareConnectiviteInferieure(typename MGElementL3L<TTTraits>::ConstTCElementP) const;
   virtual void       executeAlgorithme      (typename MGElementL3L<TTTraits>::TCAlgo&);
   virtual void       executeAlgorithme      (typename MGElementL3L<TTTraits>::TCAlgoConst&) const;

   virtual ERMsg      listeMaille            (CLListeP***, EntierN, EntierN, EntierN,
                                              DReel, DReel, DReel, TCCoord&) const;

   virtual Booleen    pointInterieur         (const TCCoord&, const DReel& = 0) const;

   virtual EntierN    reqNbrNoeud            () const;
   virtual void       reqNoeuds              (typename MGElementL3L<TTTraits>::TCNoeudPP &, EntierN);
   virtual void       reqNoeuds              (typename MGElementL3L<TTTraits>::ConstTCNoeudPP &, EntierN) const;
   virtual typename MGElementL3L<TTTraits>::Type reqType() const;
   virtual void       reqVectNoeud           (TCCoord&, EntierN);

   // ---  Approximation éléments finis
   //DReel    calculeDetJ          () const;
   //void     calculeJacobien      (DReel&, DReel&, DReel) const;
   //void     calculeN             (DReel&, DReel&, DReel) const;
   //void     calculeNx            (DReel&, DReel&, DReel&, DReel) const;
   //void     calculeNy            (DReel&, DReel&, DReel&, DReel) const;

   Booleen    transformationInverse(DReel&, const typename TCSelf::TCCoord&) const;
   //void       opDeriveX            (CLVecteur&, CLVecteur&, const SNVnoScalaire&) const;
   //void       opDeriveY            (CLVecteur&, CLVecteur&, const SNVnoScalaire&) const;

protected:
   virtual void       invariant              (ConstCarP) const;


#ifdef MODE_IMPORT_EXPORT
public:
   virtual void       exporte                (FIFichier&) const;
   virtual void       importe                (FIFichier&, const CLListe&);
#endif //MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
protected:
   virtual void       ecrisVirtuel           (FIFichier&) const;
   virtual void       lisVirtuel             (FIFichier&);
#endif   // MODE_PERSISTANT

};

//**************************************************************
// Description:
//   Vérifie que les deux pointeurs soient soit simultanément NUL, soit
//   non égaux, donc que l'élément ne contienne pas deux noeuds identiques.
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTTraits>
inline void MGElementL3L<TTTraits>::invariant(ConstCarP conditionP) const
{
   TCSelf::TCElement::invariant (conditionP);
   INVARIANT(noeud1P != NUL || (noeud2P == NUL && noeud3P == NUL), conditionP);
   INVARIANT(noeud2P != NUL || (noeud1P == NUL && noeud3P == NUL), conditionP);
   INVARIANT(noeud3P != NUL || (noeud1P == NUL && noeud2P == NUL), conditionP);
   INVARIANT(noeud1P == NUL || (noeud1P != noeud2P && noeud1P != noeud3P && noeud2P != noeud3P), conditionP);
}

#include "MGElementL3L.hpp"

#endif  // MGELEMENT_L3L_H_DEJA_INCLU
