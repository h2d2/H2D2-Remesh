//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: MGElementT3.hpp
// Classe : MGElementT3
//************************************************************************
// 28-07-2003  Maxime Derenne      Version initiale
// 04-08-2003  Maxime Derenne      Transfert de méthode inline
// 25-10-2003  Olivier Kaczor      Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor      Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor      Rendre compilable sur GCC
// 16-02-2004  Maude Giasson       TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_T3_HPP_DEJA_INCLU
#define MGELEMENT_T3_HPP_DEJA_INCLU

#include "clliste.h"
//#include "clvect.h"

#include "MGConst.h"
#include "MGConstElementAlgorithme.h"
#include "MGElementAlgorithme.h"
#include "GOEpsilon.h"

#ifndef MIN_MAX
#define MIN_MAX
inline Entier minLocal (Entier i, Entier j) { return i < j ? i : j ;}
inline Entier maxLocal (Entier i, Entier j) { return i > j ? i : j ;}
#endif  // MIN_MAX

//**************************************************************
// Description:
//   Constructeur par défaut, sans argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementT3<TTTraits>::MGElementT3()
: TCSelf::TCElement(0),
  noeud1P(NUL),
  noeud2P(NUL),
  noeud3P(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementT3 ()


//**************************************************************
// Description:
//   Initialise le numéro de l'élément ainsi que ses trois noeuds
//   correspondants.
//
// Entrée:
//   EntierN element     : le numéro de l'élément qui relie les trois noeuds
//   TCNoeudPP &noeuds   : Référence sur un tableau de pointeur sur des noeuds.
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementT3<TTTraits>::MGElementT3(EntierN element,
  typename MGElementT3<TTTraits>::TCNoeudPP noeudsPP)
: TCSelf::TCElement(element),
  noeud1P(*(  noeudsPP)),
  noeud2P(*(++noeudsPP)),
  noeud3P(*(++noeudsPP))
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // MGElementT3 (EntierN, TCNoeudPP)


//**************************************************************
// Description:
//   ne fait rien car aucune allocation dynamique n'a été faite
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementT3<TTTraits>::~MGElementT3()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
} // ~MGElementT3 ()

//************************************************************************
// Sommaire: Compare les connectivités et le type d'élément.
//
// Description: Compare les connectivités et le type d'élément.  Retourne VRAI
//              si le type d'élément est inférieur ou si c'est le mème type
//              d'élément, mais que la connectivité  soit inférieure.  Pour que
//              la connectivité soit considérée inférieure, il faut qu'une fois
//              les NoNoeuds ordonnés des deux éléments cet élément-ci (this) possède
//              un noeud avec un # inférieur dans les n premiers noeuds comparés
//              à ceux de l'autre élément.
//
// Entrée:
//   ConstMGElementP elem          : Element à comparer
//
// Sortie:
//   Booleen                           : VRAI ou FAUX
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
Booleen MGElementT3<TTTraits>::compareConnectiviteInferieure(typename MGElementT3<TTTraits>::ConstTCElementP elemP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef MGElementT3<TTTraits> const * ConstTCSelfP;
   Booleen reponse = FAUX;

   if (this->reqType() != elemP->reqType())
   {
      reponse = this->reqType() < elemP->reqType();
   }
   else
   {
      typename MGElementT3<TTTraits>::TCNoeudP noNo1P, noNo2P, noNo3P;
      noNo1P = noeud1P;
      noNo2P = noeud2P;
      noNo3P = noeud3P;

      if (noNo2P < noNo1P)
      {
         const typename MGElementT3<TTTraits>::TCNoeudP noTmpP = noNo1P;
         noNo1P = noNo2P;
         noNo2P = noTmpP;
      }
      if (noNo3P < noNo1P)
      {
         const typename MGElementT3<TTTraits>::TCNoeudP noTmpP = noNo1P;
         noNo1P = noNo3P;
         noNo3P = noNo2P;
         noNo2P = noTmpP;
      }
      else if (noNo3P < noNo2P)
      {
         const typename MGElementT3<TTTraits>::TCNoeudP noTmpP = noNo2P;
         noNo2P = noNo3P;
         noNo3P = noTmpP;
      }

      typename MGElementT3<TTTraits>::TCNoeudP elemNoNo1P, elemNoNo2P, elemNoNo3P;

      ASSERTION(dynamic_cast<ConstTCSelfP>(elemP) != NUL);
      ConstTCSelfP elemT3P = static_cast<ConstTCSelfP>(elemP);
      elemNoNo1P = elemT3P->noeud1P;
      elemNoNo2P = elemT3P->noeud2P;
      elemNoNo3P = elemT3P->noeud3P;

      if (elemNoNo2P < elemNoNo1P)
      {
         const typename MGElementT3<TTTraits>::TCNoeudP noTmpP = elemNoNo1P;
         elemNoNo1P = elemNoNo2P;
         elemNoNo2P = noTmpP;
      }
      if (elemNoNo3P < elemNoNo1P)
      {
         const typename MGElementT3<TTTraits>::TCNoeudP noTmpP = elemNoNo1P;
         elemNoNo1P = elemNoNo3P;
         elemNoNo3P = elemNoNo2P;
         elemNoNo2P = noTmpP;
      }
      else if (elemNoNo3P < elemNoNo2P)
      {
         const typename MGElementT3<TTTraits>::TCNoeudP noTmpP = elemNoNo2P;
         elemNoNo2P = elemNoNo3P;
         elemNoNo3P = noTmpP;
      }

      if (noNo1P != elemNoNo1P)
      {
         reponse = noNo1P < elemNoNo1P;
      }
      else if (noNo2P != elemNoNo2P)
      {
         reponse = noNo2P < elemNoNo2P;
      }
      else if (noNo3P != elemNoNo3P)
      {
         reponse = noNo3P < elemNoNo3P;
      }
      else
      {
         reponse = FAUX;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return reponse;
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::calculeN(DReel& n1, DReel& n2, DReel& n3,
                                     DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   n1 = (1.0 - ksi - eta);
   n2 = ksi;
   n3 = eta;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//    Calcule le déterminant de la transformation tau
//
// Entrée:
//
// Sortie:     DReel&  la valeur du déterminant
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
DReel MGElementT3<TTTraits>::calculeDetJ() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel y3Y1 = noeud3P->reqCoordonnees()[1] - noeud1P->reqCoordonnees()[1];
   DReel x3X1 = noeud3P->reqCoordonnees()[0] - noeud1P->reqCoordonnees()[0];
   DReel y2Y1 = noeud2P->reqCoordonnees()[1] - noeud1P->reqCoordonnees()[1];
   DReel x2X1 = noeud2P->reqCoordonnees()[0] - noeud1P->reqCoordonnees()[0];
   DReel det = ((x2X1 * y3Y1) - (x3X1 * y2Y1));

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return det;
}

//**************************************************************
// Sommaire:
//    Calcule le jacobien de la transformation.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::calculeJacobien(DReel& j11, DReel& j12, DReel& j21, DReel& j22,
                                            DReel& detJ,
                                            DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ASSERTION(FAUX);
   DReel x1 = noeud1P->reqCoordonnees()[0];
   DReel y1 = noeud1P->reqCoordonnees()[1];
   DReel x2 = noeud2P->reqCoordonnees()[0];
   DReel y2 = noeud2P->reqCoordonnees()[1];
   DReel x3 = noeud3P->reqCoordonnees()[0];
   DReel y3 = noeud3P->reqCoordonnees()[1];

   j11 = 0.25 * ((-x1+x2+x3) + eta*(x1-x2+x3));
   j12 = 0.25 * ((-y1+y2+y3) + eta*(y1-y2+y3));
   j21 = 0.25 * ((-x1-x2+x3) + ksi*(x1-x2+x3));
   j22 = 0.25 * ((-y1-y2+y3) + ksi*(y1-y2+y3));
   detJ = j11*j22 - j12*j21;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::calculeNx(DReel& n1x, DReel& n2x, DReel& n3x,
                                      DReel& detJ,
                                      DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel j11, j12, j21, j22;
   calculeJacobien(j11, j12, j21, j22, detJ, ksi, eta);

   DReel fact = 0.25 / detJ;
   n1x = ((-1.0+eta)*j22 - (-1.0+ksi)*j12) * fact;
   n2x = (( 1.0-eta)*j22 - (-1.0-ksi)*j12) * fact;
   n3x = (( 1.0+eta)*j22 - ( 1.0+ksi)*j12) * fact;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::calculeNy(DReel& n1y, DReel& n2y, DReel& n3y,
                                      DReel& detJ,
                                      DReel ksi, DReel eta) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel j11, j12, j21, j22;
   calculeJacobien(j11, j12, j21, j22, detJ, ksi, eta);

   DReel fact = 0.25 / detJ;
   n1y = (- (-1.0+eta)*j21 + (-1.0+ksi)*j11) * fact;
   n2y = (- ( 1.0-eta)*j21 + (-1.0-ksi)*j11) * fact;
   n3y = (- ( 1.0+eta)*j21 + ( 1.0+ksi)*j11) * fact;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementL3
//
// Entrée: TCAlgo& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::executeAlgorithme(typename MGElementT3<TTTraits>::TCAlgo& algo)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementT3(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementL3.  L'algorithme ne peut
//              modifier l'élément puisque l'élément est constant.
//
// Entrée: TCAlgoConst& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::executeAlgorithme(typename MGElementT3<TTTraits>::TCAlgoConst& algo) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementT3(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//    Identifie les mailles de la table de localisation qui recoupent
//    l'élément et l'ajoute à leurs listes respectives.
//    On boucle sur les côtés afin de determiner pour chaque indice i
//    les indice min et max en j.
//    Pour chaque côté, on repère toutes les mailles intersectées en traitant
//    de manière exhaustive toutes les intersections avec le grille de base.
//    Suivant la pente du côté on traite les itersections soit soit suivant
//    les indices i, soit suivant les indices j. On ne calcule par directement
//    les intersections, mais on contrôle quand une avancée de delX
//    (respectivement delY) provoque un changement d'indice en j (respectivement
//    i).
//    Les jMin et jMax soit stockés dans un repère i local aux mailles de
//    l'élément, ceci pour eviter de dimensionner des vecteurs trop grands pour
//    rien.
//
// Entrée:
//    EntierN          tableIMax  dimension en x de la table de localisation
//    EntierN          tableJMax  dimension en y de la table de localisation
//    EntierN          tableKMax  dimension en z de la table de localisation
//    DReel             delX       pas de la table en x
//    DReel             delY       pas de la table en y
//    DReel             delZ       pas de la table en z
//    SYCoordonnéesXYZ coin       coin inférieur gauche de la table
//
// Sortie:
//    CLListeP***   tableLoca  table de localisation
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
ERMsg MGElementT3<TTTraits>::listeMaille(CLListeP ***tableLoca, EntierN tableIMax,
                                         EntierN tableJMax, EntierN, DReel delX,
                                         DReel delY, DReel, TCCoord& coin) const
{
#ifdef MODE_DEBUG
   PRECONDITION(tableLoca != NUL);
   PRECONDITION(delX >= 1.0e-8);
   PRECONDITION(delY >= 1.0e-8);
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   EntierN i;
   EntierN iNoeudMin = tableIMax, iNoeudMax = 0;
   EntierN jNoeudMin = tableJMax, jNoeudMax = 0;
   Entier iNoeud[4], jNoeud[4];
   DReel  xNoeud[4], yNoeud[4];

   // --- On place les données des noeuds de l'élément dans un vecteur
   //     pour faciliter l'écriture de la boucle sur les côtés

   // ---  Coordonnées des sommets
   xNoeud[0] = noeud1P->reqCoordonnees()[0], yNoeud[0] = noeud1P->reqCoordonnees()[1];
   xNoeud[1] = noeud2P->reqCoordonnees()[0], yNoeud[1] = noeud2P->reqCoordonnees()[1];
   xNoeud[2] = noeud3P->reqCoordonnees()[0], yNoeud[2] = noeud3P->reqCoordonnees()[1];
   xNoeud[3] = xNoeud[0], yNoeud[3] = yNoeud[0];

   // ---  Indices des sommets dans la table
   DReel f = 0.0F;
   DReelP fP = &f;
   f = (xNoeud[0] - coin[0]) / delX;
   iNoeud[0] = iNoeud[3] = Entier(*fP);

   f = (yNoeud[0] - coin[1]) / delY;
   jNoeud[0] = jNoeud[3] = Entier(*fP);

   f = (xNoeud[1] - coin[0]) / delX;
   iNoeud[1] = Entier(*fP);

   f = (yNoeud[1] - coin[1]) / delY;
   jNoeud[1] = Entier(*fP);

   f = (xNoeud[2] - coin[0]) / delX;
   iNoeud[2] = Entier(*fP);

   f = (yNoeud[2] - coin[1]) / delY;
   jNoeud[2] = Entier(*fP);

   // ---  Min et max des indices
   iNoeudMin = minLocal(iNoeudMin, iNoeud[0]);
   iNoeudMin = minLocal(iNoeudMin, iNoeud[1]);
   iNoeudMin = minLocal(iNoeudMin, iNoeud[2]);
   iNoeudMax = maxLocal(iNoeudMax, iNoeud[0]);
   iNoeudMax = maxLocal(iNoeudMax, iNoeud[1]);
   iNoeudMax = maxLocal(iNoeudMax, iNoeud[2]);
   jNoeudMin = minLocal(jNoeudMin, jNoeud[0]);
   jNoeudMin = minLocal(jNoeudMin, jNoeud[1]);
   jNoeudMin = minLocal(jNoeudMin, jNoeud[2]);
   jNoeudMax = maxLocal(jNoeudMax, jNoeud[0]);
   jNoeudMax = maxLocal(jNoeudMax, jNoeud[1]);
   jNoeudMax = maxLocal(jNoeudMax, jNoeud[2]);
   Entier ntableIMax = tableIMax, ntableJMax = tableJMax, niNoeudMin = iNoeudMin;
   Entier niNoeudMax = iNoeudMax, njNoeudMin = jNoeudMin, njNoeudMax = jNoeudMax;
   if (niNoeudMin < 0 || niNoeudMin >= ntableIMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");
   if (niNoeudMax < 0 || niNoeudMax >= ntableIMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");
   if (njNoeudMin < 0 || njNoeudMin >= ntableJMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");
   if (njNoeudMax < 0 || njNoeudMax >= ntableJMax) return ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");

   // ---  Vecteurs de travail
   EntierP minJColonneIV = new Entier[iNoeudMax-iNoeudMin+1];
   EntierP maxJColonneIV = new Entier[iNoeudMax-iNoeudMin+1];
   for (i = 0; i <= (iNoeudMax-iNoeudMin); i++)
      minJColonneIV[i] = tableJMax, maxJColonneIV[i] = 0;

   // ---  Prend les sommets
   minJColonneIV[iNoeud[0]-iNoeudMin] = minLocal(jNoeud[0] , minJColonneIV[iNoeud[0]-iNoeudMin]);
   maxJColonneIV[iNoeud[0]-iNoeudMin] = maxLocal(jNoeud[0] , maxJColonneIV[iNoeud[0]-iNoeudMin]);
   minJColonneIV[iNoeud[1]-iNoeudMin] = minLocal(jNoeud[1] , minJColonneIV[iNoeud[1]-iNoeudMin]);
   maxJColonneIV[iNoeud[1]-iNoeudMin] = maxLocal(jNoeud[1] , maxJColonneIV[iNoeud[1]-iNoeudMin]);
   minJColonneIV[iNoeud[2]-iNoeudMin] = minLocal(jNoeud[2] , minJColonneIV[iNoeud[2]-iNoeudMin]);
   maxJColonneIV[iNoeud[2]-iNoeudMin] = maxLocal(jNoeud[2] , maxJColonneIV[iNoeud[2]-iNoeudMin]);

   // ---  Boucle sur les côtés
   Entier iIndLoc, iIndLocMin, iIndLocMax, iIndInc, iTmp;
   Entier jIndGlb, jIndGlbMin, jIndGlbMax, jIndInc, jTmp;
   DReel pente, x, y, v, dv, vlim;
   DReel diffX, diffY;
   for (i = 0; i < 3; i++)
   {
      // ---  Variations en x et y
      diffX = xNoeud[i+1] - xNoeud[i];
      diffY = yNoeud[i+1] - yNoeud[i];

      // ---  Incréments pour le parcours de la table
      if (diffX >= DReel(0.0))
         iIndInc =  1;
      else
         iIndInc = -1;
      if (diffY >= DReel(0.0))
         jIndInc = 1;
      else
         jIndInc = -1;

      // ---  Différencie le sens de balayage d'après la variation max
      if (fabs(diffX) >= fabs(diffY))
      {
         pente = diffY / diffX;
         dv = fabs(delX * pente);
         vlim = delY;
         x = coin[0] + DReel(iNoeud[i] + (iIndInc + 1) / Entier(2)) * delX;
         y = coin[1] + DReel(jNoeud[i] - (jIndInc - 1) / Entier(2)) * delY;
         v = fabs( yNoeud[i] + (x - xNoeud[i]) * pente - y );
         iIndLocMin = iNoeud[i  ] - iNoeudMin;
         iIndLocMax = iNoeud[i+1] - iNoeudMin;
         jIndGlb = jNoeud[i];
//         msg = "x - x y v ";
//         msg << x << " " << y << " " << v << " " << dv << " " << jIndInc << " " << jNoeud[i];
//         ERErreur::logMessage(msg);
//         ERErreur::ecrisMessage(msg<<"\n");
         for (iIndLoc  = iIndLocMin;
              iIndLoc != iIndLocMax;
              iIndLoc  = iIndLoc + iIndInc)
         {
            while (v >= vlim)
            {
               v = v - vlim;
               jIndGlb = jIndGlb + jIndInc;
               jIndGlb = minLocal(jIndGlb, jNoeudMax);
               jIndGlb = maxLocal(jIndGlb, jNoeudMin);
               minJColonneIV[iIndLoc] = minLocal(jIndGlb, minJColonneIV[iIndLoc]);
               maxJColonneIV[iIndLoc] = maxLocal(jIndGlb, maxJColonneIV[iIndLoc]);
//               msg = "iLoc jGlb ";
//               msg << iIndLoc << " " << jIndGlb << " " << v << " " << vlim << " " << dv;
//               ERErreur::logMessage(msg);
//               ERErreur::ecrisMessage(msg<<"\n");
            }
            iTmp = iIndLoc + iIndInc;
            minJColonneIV[iTmp] = minLocal(jIndGlb, minJColonneIV[iTmp]);
            maxJColonneIV[iTmp] = maxLocal(jIndGlb, maxJColonneIV[iTmp]);
            v = v + dv;
//            msg = "iLoc jGlb ";
//            msg << iTmp << " " << jIndGlb << " " << v << " " << vlim << " " << dv;
//            ERErreur::logMessage(msg);
//            ERErreur::ecrisMessage(msg<<"\n");
         }
      }
      else
      {
         pente = diffX / diffY;
         dv = fabs(delY * pente);
         vlim = delX;
         x = coin[0] + DReel(iNoeud[i] - (iIndInc - 1) / Entier(2)) * delX;
         y = coin[1] + DReel(jNoeud[i] + (jIndInc + 1) / Entier(2)) * delY;
         v = fabs( xNoeud[i] + (y - yNoeud[i]) * pente - x );
         jIndGlbMin = jNoeud[i  ];
         jIndGlbMax = jNoeud[i+1];
         iIndLoc = iNoeud[i] - iNoeudMin;
//         msg = "y - x y v ";
//         msg << x << " " << y << " " << v <<  " " << dv << " " << iIndInc << " " << iNoeud[i];
//         ERErreur::logMessage(msg);
//         ERErreur::ecrisMessage(msg<<"\n");
         for (jIndGlb  = jIndGlbMin;
              jIndGlb != jIndGlbMax;
              jIndGlb  = jIndGlb + jIndInc)
         {
            while (v >= vlim)
            {
               v = v - vlim;
               iIndLoc = iIndLoc + iIndInc;
               iIndLoc = minLocal(iIndLoc, iNoeudMax-iNoeudMin);
               iIndLoc = maxLocal(iIndLoc, 0);
               minJColonneIV[iIndLoc] = minLocal(jIndGlb, minJColonneIV[iIndLoc]);
               maxJColonneIV[iIndLoc] = maxLocal(jIndGlb, maxJColonneIV[iIndLoc]);
//               msg = "iLoc jGlb ";
//               msg << iIndLoc << " " << jIndGlb << " " << v << " " << vlim << " " << dv;
//               ERErreur::logMessage(msg);
//               ERErreur::ecrisMessage(msg<<"\n");
            }
            jTmp = jIndGlb + jIndInc;
            minJColonneIV[iIndLoc] = minLocal(jTmp, minJColonneIV[iIndLoc]);
            maxJColonneIV[iIndLoc] = maxLocal(jTmp, maxJColonneIV[iIndLoc]);
            v = v + dv;
//            msg = "iLoc jGlb ";
//            msg << iIndLoc << " " << jTmp << " " << v << " " << vlim << " " << dv;
//            ERErreur::logMessage(msg);
//            ERErreur::ecrisMessage(msg<<"\n");
         }
      }  // end if (fabs(diffX) >= fabs(diffY))
   }  // end for (i = 0; i < 3; i++)


   // ---  Balaie l'intérieur du triangle
   Entier iIndGlb;
   for (iIndLoc = 0; iIndLoc <= (Entier)(iNoeudMax-iNoeudMin); iIndLoc++)
   {
//      msg = "min max ";
//      msg << iIndLoc << " " << minJColonneIV[iIndLoc] << " " << maxJColonneIV[iIndLoc];
//      ERErreur::logMessage(msg);
//      ERErreur::ecrisMessage(msg<<"\n");
      iIndGlb = iIndLoc + iNoeudMin;
      for (jIndGlb  = minJColonneIV[iIndLoc];
           jIndGlb <= maxJColonneIV[iIndLoc]; jIndGlb++)
      {
         tableLoca[iIndGlb][jIndGlb][0]->ajoute(VoidP(this));
      }
   }

   // ---  Récupère la mémoire
   delete[] minJColonneIV;
   delete[] maxJColonneIV;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Description:
//    Opération de dérivation en X d'un element.
//
// Entrée:
//   CLVecteur& vecDer                :
//   CLVecteur& vecDer                :
//   const SNVnoScalaire& vnoScalaire : Valeurs scalaires qui sont dérivées
// Sortie:
//
// Notes:
//
//**************************************************************
/*void MGElementT3::opDeriveX (CLVecteur& vecDer,
                             CLVecteur& vecSrf,
                             const SNVnoScalaire& vnoScalaire) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel x21, x31, y31, y21, srf, der;
   const DReel UN_SIX = 1.0F / 6.0F;

   // --- Calcul de la dérivée
   x21 = noeud2P->coordonnees()[0] - noeud1P->coordonnees()[0];
   y21 = noeud2P->coordonnees()[1] - noeud1P->coordonnees()[1];
   x31 = noeud3P->coordonnees()[0] - noeud1P->coordonnees()[0];
   y31 = noeud3P->coordonnees()[1] - noeud1P->coordonnees()[1];
   srf = UN_SIX * (x21 * y31 - x31 * y21);
   der = UN_SIX * (y31 * DReel(vnoScalaire[noeud2P->reqIndVno()] - vnoScalaire[noeud1P->reqIndVno()])
                   - y21 * DReel(vnoScalaire[noeud3P->reqIndVno()] - vnoScalaire[noeud1P->reqIndVno()]));

   // ---  Assemble
   DReel tmp;
   vecDer.reqElement(tmp    , noeud1P->reqNoNoeud());
   vecDer.remplace  (tmp+der, noeud1P->reqNoNoeud());
   vecDer.reqElement(tmp    , noeud2P->reqNoNoeud());
   vecDer.remplace  (tmp+der, noeud2P->reqNoNoeud());
   vecDer.reqElement(tmp    , noeud3P->reqNoNoeud());
   vecDer.remplace  (tmp+der, noeud3P->reqNoNoeud());
   vecSrf.reqElement(tmp    , noeud1P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf, noeud1P->reqNoNoeud());
   vecSrf.reqElement(tmp    , noeud2P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf, noeud2P->reqNoNoeud());
   vecSrf.reqElement(tmp    , noeud3P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf, noeud3P->reqNoNoeud());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // opDeriveX (..., const SNVnoScalaire&)
*/
//**************************************************************
// Description:
//    Opération de dérivation en Y d'un element.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
/*void MGElementT3::opDeriveY (CLVecteur& vecDer, CLVecteur& vecSrf,
                             const SNVnoScalaire& vnoScalaire) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel x21, x31, y31, y21, srf, der;
   const DReel UN_SIX = 1.0F / 6.0F;

   // --- Calcul de la dérivée
   x21 = noeud2P->coordonnees()[0] - noeud1P->coordonnees()[0];
   y21 = noeud2P->coordonnees()[1] - noeud1P->coordonnees()[1];
   x31 = noeud3P->coordonnees()[0] - noeud1P->coordonnees()[0];
   y31 = noeud3P->coordonnees()[1] - noeud1P->coordonnees()[1];
   srf = UN_SIX * (x21 * y31 - x31 * y21);
   der = UN_SIX * (x21 * DReel(vnoScalaire[noeud3P->reqIndVno()] - vnoScalaire[noeud1P->reqIndVno()])
                   - x31 * DReel(vnoScalaire[noeud2P->reqIndVno()] - vnoScalaire[noeud1P->reqIndVno()]));

   // ---  Assemble
   DReel tmp;
   vecDer.reqElement(tmp    , noeud1P->reqNoNoeud());
   vecDer.remplace  (tmp+der, noeud1P->reqNoNoeud());
   vecDer.reqElement(tmp    , noeud2P->reqNoNoeud());
   vecDer.remplace  (tmp+der, noeud2P->reqNoNoeud());
   vecDer.reqElement(tmp    , noeud3P->reqNoNoeud());
   vecDer.remplace  (tmp+der, noeud3P->reqNoNoeud());
   vecSrf.reqElement(tmp    , noeud1P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf, noeud1P->reqNoNoeud());
   vecSrf.reqElement(tmp    , noeud2P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf, noeud2P->reqNoNoeud());
   vecSrf.reqElement(tmp    , noeud3P->reqNoNoeud());
   vecSrf.remplace  (tmp+srf, noeud3P->reqNoNoeud());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // opDeriveY (CLVecteur& vecDer, CLVecteur& vecSrf, const SNVnoScalaire& vnoScalaire) const)
*/
//**************************************************************
// Description:
//    Opération d'intégration d'un element.
//
// Entrée:
//
// Sortie:
//    DReel resultat : resultat de l'integration de l'élément.
//
// Notes:
//
//**************************************************************
/*void MGElementT3::opIntegre (DReel& resultat, const SNVnoScalaire& vnoScalaire) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel detJ, x21, x31, y31, y21;

   // --- Calcul du déterminant Jacobien.
   x21 = noeud2P->coordonnees()[0] - noeud1P->coordonnees()[0];
   y31 = noeud3P->coordonnees()[1] - noeud1P->coordonnees()[1];
   x31 = noeud3P->coordonnees()[0] - noeud1P->coordonnees()[0];
   y21 = noeud2P->coordonnees()[1] - noeud1P->coordonnees()[1];
   detJ = x21 * y31 - x31 * y21;

   // --- Résultat de l'intégral
   resultat = 0.16666666666666666666666666666666666 *
              (vnoScalaire[noeud1P->reqIndVno()] +
               vnoScalaire[noeud2P->reqIndVno()] +
               vnoScalaire[noeud3P->reqIndVno()]) * detJ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // opIntegre (DReel resultat)
*/
//**************************************************************
// Description:
//   Cette méthode vérifie si le point passé en paramètre se situe à
//   l'intérieur de l'élément. Si oui, alors la variable resultat va contenir
//   VRAI au retour, et si non alors resultat va contenir FAUX.
//
// Entrée:
//   SYCoordonnees point
//      Contient le point pour lequel on désire déterminer s'il se situe à
//      l'intérieur de l'élément.
//
// Sortie:
//   Booleen& resultat
//      Si sa valeur de retour est VRAI, alors le point est à l'intérieur
//      de l'élément et si sa valeur de retour est FAUX, alors le point n'est
//      pas à l'intérieur de l'élément.
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
Booleen MGElementT3<TTTraits>::pointInterieur (const TCCoord& point,
                                               const DReel& epsilon) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel ksi, eta;
   Booleen resultat = transformationInverse(ksi, eta, point);
   if (!resultat)
   {
      // --- On refait un test avec un epsilon plus tolérant
      DReel lambda = 1.0 - ksi - eta;
      resultat = !((GOEpsilon(ksi, epsilon) < 0.0) ||
                   (GOEpsilon(eta, epsilon) < 0.0) ||
                   (GOEpsilon(lambda, epsilon) < 0.0));

   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return resultat;
}

//************************************************************************
// Description:
//    Retourne le nombre de noeud contenu dans l'élément.
//
// Entrée:
//
// Sortie:
//    EntierN : Nombre de noeud.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline EntierN MGElementT3<TTTraits>::reqNbrNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return 3;
}

//************************************************************************
// Description:
//    Retourne le type de l'élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline typename MGElementT3<TTTraits>::Type MGElementT3<TTTraits>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return TCSelf::TYPE_T3;
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::reqNoeuds(typename MGElementT3<TTTraits>::TCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize)
#else
                                      EntierN /*bufferSize*/)
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::reqNoeuds(typename MGElementT3<TTTraits>::ConstTCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize) const
#else
                                      EntierN /*bufferSize*/) const
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//**************************************************************
// Description:
//   Retourne le vecteur résultant de la somme des vecteurs orthogonaux
//   aux cotés de l'élément et relié au noeud (normale 2D à l'élément)
//
// Entrée:
//   EntierN           no             numéro du noeud à étudier
//
// Sortie:
//   TTCoord& vectNoeud      vecteur au noeud
//
// Notes:
//    Cette méthode est employée par les déformées pour connaitre la
//    contribution d'un élément à un de ses noeuds.
//
//**************************************************************
template <typename TTTraits>
void MGElementT3<TTTraits>::reqVectNoeud(TCCoord& vectNoeud, EntierN no)
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typename MGElementT3<TTTraits>::TCCoord v1, v2, tamp;
   DReel             norm;

   if (no == noeud1P->reqNoNoeud())
   {
      v1 = noeud1P->reqCoordonnees() - noeud2P->reqCoordonnees();
      v2 = noeud3P->reqCoordonnees() - noeud1P->reqCoordonnees();
   }
   else
   if (no == noeud2P->reqNoNoeud())
   {
      v1 = noeud2P->reqCoordonnees() - noeud3P->reqCoordonnees();
      v2 = noeud1P->reqCoordonnees() - noeud2P->reqCoordonnees();
   }
   else
   {
      v1 = noeud3P->reqCoordonnees() - noeud1P->reqCoordonnees();
      v2 = noeud2P->reqCoordonnees() - noeud3P->reqCoordonnees();
   }
   v1[2] = v2[2] = 0.0;

   // --- vecteurs orthogonaux (rotation de 90deg dans le sens trigo)
   tamp = v1;
   v1[0] = -v1[1];
   v1[1] = tamp[0];

   tamp = v2;
   v2[0] = -v2[1];
   v2[1] = tamp[0];

   // --- somme des deux cotés
   vectNoeud = v1 + v2;

   // --- normalisation du vecteur
   norm = double(sqrt(vectNoeud[0]*vectNoeud[0]
                    + vectNoeud[1]*vectNoeud[1]
                    + vectNoeud[2]*vectNoeud[2]));
   if (norm > 1.0E-18)
      vectNoeud = vectNoeud / norm;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // reqVectNoeud

//**************************************************************
// Description:
//   Cette méthode fait la transformation des coordonnées en Ksi, Eta et
//   Zeta.
//
// Entrée:
//   TCCoord& point
//      Contient le point pour lequel on désire déterminer s'il se situe à
//      l'intérieur de l'élément.
//
// Sortie:
//   DReel vKsi, vEta, VZeta : les trois coordonnées dans le système de
//                            référence.
//
// Notes:
// Préconditions:
//   Vérifie qu'aucun des noeuds n'est NUL.
//
//**************************************************************
template <typename TTTraits>
Booleen MGElementT3<TTTraits>::transformationInverse (DReel& ksi, DReel& eta,
                                                      const TCCoord& point) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // --- On calcule la matrice de transformation associée à l'élément.
   DReel y3Y1 = noeud3P->reqCoordonnees()[1] - noeud1P->reqCoordonnees()[1];
   DReel x3X1 = noeud3P->reqCoordonnees()[0] - noeud1P->reqCoordonnees()[0];
   DReel y2Y1 = noeud2P->reqCoordonnees()[1] - noeud1P->reqCoordonnees()[1];
   DReel x2X1 = noeud2P->reqCoordonnees()[0] - noeud1P->reqCoordonnees()[0];
   DReel jacobien = 1.0 / ((x2X1 * y3Y1) - (x3X1 * y2Y1));
   DReel x0X1 = point[0] - noeud1P->reqCoordonnees()[0];
   DReel y0Y1 = point[1] - noeud1P->reqCoordonnees()[1];

   // --- On calcule les coordonnées ksi, eta sur l'élément de référence
   ksi = (y3Y1 * x0X1 - x3X1 * y0Y1) * jacobien;
   eta = (x2X1 * y0Y1 - x0X1 * y2Y1) * jacobien;

   DReel lambda = 1.0 - ksi - eta;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return !((GOEpsilon(ksi) < 0.0) || (GOEpsilon(eta) < 0.0) || (GOEpsilon(lambda) < 0.0));
}


//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description: Méthode de sauvegarde virtuelle qui sauvegarde les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&                           : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementT3<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::ecrisVirtuel(os);
   }

   if (os)
   {
      os << filtre(noeud1P) << ESPACE
         << filtre(noeud2P) << ESPACE
         << filtre(noeud3P);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_PERSISTANT


//************************************************************************
// Sommaire: Récupère l'élément
//
// Description: Méthode de lecture virtuelle qui lis les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on lit
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementT3<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      TCElement::lisVirtuel(is);
   }

   TCNoeudP n1P, n2P, n3P;
   if (is)
   {
      is >> filtre(n1P) >> filtre(n2P) >> filtre(n3P);
   }
   if (is)
   {
      noeud1P = n1P;
      noeud2P = n2P;
      noeud3P = n3P;
   }
   if (is)
   {
      if (noeud1P == noeud2P ||
          noeud1P == noeud3P ||
          noeud2P == noeud3P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem;
         noElem << reqNoElement();
         msg.ajoute("MSG_ELEMENT_T3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = NUL;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: Exporte l'élément
//
// Description: Méthode d'exportation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on s'exporte
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementT3<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::exporte(os);
   }
   if (os)
   {
      os << (noeud1P->reqNoNoeud()+1) << ESPACE
         << (noeud2P->reqNoNoeud()+1) << ESPACE
         << (noeud3P->reqNoNoeud()+1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire: Importe l'élément
//
// Description: Méthode d'importation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier duquel on s'importe
//   const CLListe& lstNoeuds   : liste contenant les pointeurs aux noeuds.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementT3<TTTraits>::importe(FIFichier& is, const CLListe& lstNoeuds)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if(is)
   {
      TCElement::importe(is,lstNoeuds);
   }

   EntierN n1, n2, n3;
   if (is)
   {
      is >> n1 >> n2 >> n3;
   }
   if (is)
   {
      if (n1 < 1 || n2 < 1 || n3 < 1)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NUMEROTATION_NOEUD");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
      else
      {
         n1--; n2--; n3--;
      }
   }
   if (is)
   {
      EntierN nbrNoeuds = lstNoeuds.dimension();
      if (n1 < nbrNoeuds && n2 < nbrNoeuds && n3 < nbrNoeuds)
      {
         noeud1P = TCNoeudP(lstNoeuds[n1]);
         noeud2P = TCNoeudP(lstNoeuds[n2]);
         noeud3P = TCNoeudP(lstNoeuds[n3]);
      }
      else
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_CONNECTIVITE_INVALIDE");
         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }
   if (is)
   {
      if (noeud1P == noeud2P ||
          noeud1P == noeud3P ||
          noeud2P == noeud3P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = NUL;
      }
   }

   if (is)
   {
      DReel determinant = calculeDetJ();
      if (determinant == 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_NUL");
         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
      else if (determinant < 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<0");
         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T3");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_IMPORT_EXPORT

#endif  // MGELEMENT_T3_HPP_DEJA_INCLU
