//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// $Id$
//
// Classe:  EFTraitsMaillage3D
//
// Description:
//    Trait pour la paramétrisation des classes d'éléments finis
//
//   Le trait défini les types suivants : 
//      TTCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//      TCElement   : type faisant référence aux éléments
//      TCElementXX : type des types d'élément XX pour un maillage
//      TCMaillage  : type du maillage
//
// Attributs:
//
// Notes:
//
//************************************************************************
// 25-10-2003  Olivier Kaczor    Version initiale
// 03-02-2004  Maude Giasson     Définir les TT en fonction des EF
//************************************************************************
#ifndef EFTRAITSMAILLAGE3D_H_DEJA_INCLU
#define EFTRAITSMAILLAGE3D_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

struct EFTraitsMaillage3D;

#ifndef MODULE_ELEMENTS_FINIS
   #include "GOCoord3.h"
   #include "MGNoeud.h"

   #include "MGElement.h"
   #include "MGElementP1.h"
   #include "MGElementL2.h"
   #include "MGElementL3.h"
   #include "MGElementL3L.h"
   #include "MGElementT3.h"
   #include "MGElementT6.h"
   #include "MGElementT6L.h"
   #include "MGElementQ4.h"
 //#include "MGElementQ9.h"
 //#include "MGElementTH4.h"
 //#include "MGElementH8.h"

   #include "MGMaillage.h"
#endif //MODULE_ELEMENTS_FINIS

struct EFTraitsMaillage3D
{    
   typedef  EFTraitsMaillage3D   TCSelf;

   typedef  GOCoordonneesXYZ     TCCoord;
   typedef  MGNoeud<TCCoord>     TCNoeud;

   typedef  MGElement<TCSelf>    TCElement;
   typedef  MGElementP1<TCSelf>  TCElementP1;
   typedef  MGElementL2<TCSelf>  TCElementL2;
   typedef  MGElementL3<TCSelf>  TCElementL3;
   typedef  MGElementL3L<TCSelf> TCElementL3L;
   typedef  MGElementT3<TCSelf>  TCElementT3;
   typedef  MGElementT6<TCSelf>  TCElementT6;
   typedef  MGElementT6L<TCSelf> TCElementT6L;
   typedef  MGElementQ4<TCSelf>  TCElementQ4;
 //typedef MGElementQ9<TCSelf>   TCElementQ9;
 //typedef MGElementTH4<TCSelf>  TCElementTH4;
 //typedef MGElementH8<TCSelf>   TCElementH8;

   typedef  MGMaillage<TCSelf>   TCMaillage;
};

#include "MGTableLocalisation_EFMaillage3D.h"

#endif  // EFTRAITSMAILLAGE3D_H_DEJA_INCLU

