//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     EFMaillage3D.h
//
// Classe:      EFMaillage3D
//
// Sommaire:    
//    Interface de définition de la classe EFMaillage3D
//
// Description: 
//    Instanciation de la classe template MGMaillage<T> avec EFTraitsElement3D.
//
// Attributs:
//
// Note:
//
//************************************************************************
#ifndef EFMAILLAGE3D_H_DEJA_INCLU
#define EFMAILLAGE3D_H_DEJA_INCLU

#include "sytypes.h"

#include "EFMaillage3D.hf"
#include "MGMaillage.h"
#include "EFTraitsMaillage3D.h"

DECLARE_TYPE(MGMaillage<EFTraitsMaillage3D>, EFMaillage3D);

#endif //EFMAILLAGE3D_H_DEJA_INCLU
