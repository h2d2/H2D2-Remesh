//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElementP1.h
//
// Classe:  MGElementP1
//
// Description:
//   La classe MGElementP1 représente un élément à un seul noeud. La classe 
//   regroupe toutes les méthodes de calcul sur cet élément.
//
//   La classe est définie par le paramètre template TTTraits.  TTTrait regroupe 
//   les structures nécessaires aux calculs par la méthode des éléments finis. 
//
//   Le trait utilisé doit définir les types suivants : 
//      TCValeur    : type des valeurs porté par une coordonnée
//      TCCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//
// Attributs:
//   TCNoeudP noeud1P;   Le noeud formant cet élément
//
// Notes:
//   Certaines fonctions ne sont pas encore implantées comme importe(...) et
//   lisVirtuel(...).
//
//************************************************************************
// 25-09-2003  Olivier Kaczor    Version initiale
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_P1_H_DEJA_INCLU
#define MGELEMENT_P1_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"

#ifdef MODE_PERSISTANT
   DECLARE_CLASS(FIFichier);
#endif
DECLARE_CLASS(CLListe);

template <typename TTTraits>
class MGElementP1 : public MGElement<TTTraits>
{
public:
    typedef  MGElementP1<TTTraits>   TCSelf;

    typename MGElementP1<TTTraits>::TCNoeudP noeud1P;

                      MGElementP1            ();
                      MGElementP1            (EntierN, typename MGElementP1<TTTraits>::TCNoeudPP);
   virtual            ~MGElementP1           ();

   virtual Booleen    compareConnectiviteInferieure(typename MGElementP1<TTTraits>::ConstTCElementP) const;
   virtual void       executeAlgorithme      (typename MGElementP1<TTTraits>::TCAlgo&);
   virtual void       executeAlgorithme      (typename MGElementP1<TTTraits>::TCAlgoConst&) const;

   virtual ERMsg      listeMaille            (CLListeP***, EntierN, EntierN, EntierN,
                                              DReel, DReel, DReel, typename MGElementP1<TTTraits>::TCCoord&) const;

   virtual Booleen    pointInterieur         (const typename MGElementP1<TTTraits>::TCCoord&, const DReel& = 0) const;

   virtual EntierN    reqNbrNoeud            () const;
   virtual void       reqNoeuds              (typename MGElementP1<TTTraits>::TCNoeudPP &, EntierN);
   virtual void       reqNoeuds              (typename MGElementP1<TTTraits>::ConstTCNoeudPP &, EntierN) const;
   virtual typename MGElementP1<TTTraits>::Type reqType() const;
   virtual void       reqVectNoeud           (typename MGElementP1<TTTraits>::TCCoord&, EntierN);

           Booleen transformationInverse     (DReel&, DReel&, DReel&, const typename MGElementP1<TTTraits>::TCCoord&) const;

protected:
   virtual void       invariant              (ConstCarP) const;

#ifdef MODE_IMPORT_EXPORT
public:
   virtual void       exporte                (FIFichier&) const;
   virtual void       importe                (FIFichier&, const CLListe&);
#endif //MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
protected:
   virtual void       ecrisVirtuel           (FIFichier&) const;
   virtual void       lisVirtuel             (FIFichier&);
#endif   // MODE_PERSISTANT
};

//**************************************************************
// Description:
//   Vérifie que les deux pointeurs soient soit simultanément NUL, soit
//   non égaux, donc que l'élément ne contienne pas deux noeuds identiques.
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTTraits>
inline void MGElementP1<TTTraits>::invariant(ConstCarP conditionP) const
{
   TCSelf::TCElement::invariant (conditionP);
}

#include "MGElementP1.hpp"

#endif  // MGELEMENT_P1_H_DEJA_INCLU
