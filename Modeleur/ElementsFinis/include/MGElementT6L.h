//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElementT6L.h
//
// Classe:  MGElementT6L
//
// Description:
//   La classe MGElementT6L représente un élément bidimensionnel triangulaire
//   à six noeuds. Cet élément, subparamétrique et de continuité C0, a une
//   base d'approximation linéaire sur chacun des quatre sous-triangles de
//   l'élément. La classe regroupe toutes les méthodes de calcul sur cet
//   élément.
//
//   La classe est définie par le paramètre template TTTraits.  TTTrait regroupe 
//   les structures nécessaires aux calculs par la méthode des éléments finis. 
//
//   Le trait utilisé doit définir les types suivants : 
//      TCValeur    : type des valeurs porté par une coordonnée
//      TCCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//
// Attributs:
//   MGNoeudP noeud1P;   Les noeuds formant cet élément
//   MGNoeudP noeud2P
//   MGNoeudP noeud3P
//   MGNoeudP noeud4P
//   MGNoeudP noeud5P
//   MGNoeudP noeud6P
//
// Notes:
//
//************************************************************************
// 29-01-1993  François Gingras  Version Initiale
// 21-06-1993  François Gingras  Modifications
// 21-03-1996  Eric Chamberland  Prepost, entêtes, invariant
// 25-03-1996  Eric Chamberland  Surcharge de méthodes pour utiliser les SNVno.
//                               Méthodes converties à void
// 09-04-1996  Yves Roy          Configuration de la persistence
// 07-06-1996  Eric Chamberland  +limination des méthodes doublées pour les VNOs
// 28-06-1996  Eric Chamberland  Ajout de executeAlgorithme(...)
// 15-07-1996  Eric Chamberland  Ajout de l'interpolation de SNVnoVecteurs
// 12-11-1996  Eric Chamberland  Ajout de importe/exporte
// 15-11-1996  Serge Dufour      Vérification des dépendances
// 19-11-1996  Eric Chamberland  Ajout de compareConnectiviteInferieure
// 04-12-1996  Eric Chamberland  Ajout de opIntegre, deriveX et Y.
// 13-12-1996  Eric Chamberland  Ajout de listeMaille
// 23-09-1997  Yves Roy          Ajout de l'interpolation pour vnoGlace et ajout
//                               d'un méthode utilitaire pour l'interpolation:
//                               calculPourInterpolation
// 16-10-1997  Yves Secretan     Passage à la double précision : 1.ère passe
// 26-10-1997  Yves Secretan     Pointeurs par référence dans calculPourInterpolation
// 25-11-1997  Yves Roy          Modification concernant les inclusions des vnos.
// 30-04-1998  Yves Roy          Élimination de interpole de l'interface de l'élément.
// 29-09-1998  Yves Secretan     Migre traceIsosurface dans un algorithme
// 25-07-2003  Maxime Derenne    Nettoyage du code
// 28-07-2003  Maxime Derenne    Ajout des méthodes reqNbrNoeud, reqNoeuds et reqType
// 04-08-2003  Maxime Derenne    Transfert de méthode inline
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGElEMENT_T6L_H_DEJA_INCLU
#define MGElEMENT_T6L_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"

#ifdef MODE_PERSISTANT
   DECLARE_CLASS(FIFichier);
#endif
DECLARE_CLASS(CLListe);

template <typename TTTraits>
class MGElementT6L : public MGElement<TTTraits>
{
public:
   typedef          MGElementT6L<TTTraits>  TCSelf;
   typedef typename TCSelf::TCCoord         TCCoord;

   typename MGElementT6L<TTTraits>::TCNoeudP noeud1P;
   typename MGElementT6L<TTTraits>::TCNoeudP noeud2P;
   typename MGElementT6L<TTTraits>::TCNoeudP noeud3P;
   typename MGElementT6L<TTTraits>::TCNoeudP noeud4P;
   typename MGElementT6L<TTTraits>::TCNoeudP noeud5P;
   typename MGElementT6L<TTTraits>::TCNoeudP noeud6P;

                      MGElementT6L           ();
                      MGElementT6L           (EntierN element, typename MGElementT6L<TTTraits>::TCNoeudPP);
   virtual            ~MGElementT6L          ();

   virtual Booleen    compareConnectiviteInferieure(typename MGElementT6L<TTTraits>::ConstTCElementP) const;
   virtual void       executeAlgorithme      (typename MGElementT6L<TTTraits>::TCAlgo&);
   virtual void       executeAlgorithme      (typename MGElementT6L<TTTraits>::TCAlgoConst&) const;

   virtual ERMsg      listeMaille            (CLListeP***, EntierN, EntierN, EntierN,
                                              DReel, DReel, DReel, TCCoord&) const;

   virtual Booleen    pointInterieur         (const TCCoord&, const DReel& = 0) const;

   virtual EntierN    reqNbrNoeud            () const;
   virtual void       reqNoeuds              (typename MGElementT6L<TTTraits>::TCNoeudPP &, EntierN);
   virtual void       reqNoeuds              (typename MGElementT6L<TTTraits>::ConstTCNoeudPP &, EntierN) const;
   virtual typename MGElementT6L<TTTraits>::Type reqType() const;
   virtual void       reqVectNoeud           (TCCoord&, EntierN);
   
   Booleen  transformationInverse(DReel&, DReel&, const TCCoord&) const;

protected:
   virtual void       invariant              (ConstCarP) const;

private:
   typedef          MGElementT3<typename MGElementT6L<TTTraits>::TCTraits>     TCElementT3;

#ifdef MODE_IMPORT_EXPORT
public:
   virtual void       exporte                (FIFichier&) const;
   virtual void       importe                (FIFichier&, const CLListe&);
#endif //MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
protected:
   virtual void       ecrisVirtuel           (FIFichier&) const;
   virtual void       lisVirtuel             (FIFichier&);
#endif   // MODE_PERSISTANT
};

//**************************************************************
// Description:
//   Vérifie que les six pointeurs soient soit simultanément NUL, soit
//   non égaux deux à deux, donc que l'élément ne contienne pas deux noeuds
//   identiques.
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTTraits>
inline void MGElementT6L<TTTraits>::invariant(ConstCarP conditionP) const
{
   TCSelf::TCElement::invariant (conditionP);
   INVARIANT(!((noeud1P == NUL &&
         (noeud2P != NUL || noeud3P != NUL || noeud4P != NUL ||
          noeud5P != NUL || noeud6P != NUL)) ||
       (noeud2P == NUL &&
         (noeud1P != NUL || noeud3P != NUL || noeud4P != NUL ||
          noeud5P != NUL || noeud6P != NUL)) ||
       (noeud3P == NUL &&
         (noeud1P != NUL || noeud2P != NUL || noeud4P != NUL ||
          noeud5P != NUL || noeud6P != NUL)) ||
       (noeud4P == NUL &&
         (noeud1P != NUL || noeud2P != NUL || noeud3P != NUL ||
          noeud5P != NUL || noeud6P != NUL)) ||
       (noeud5P == NUL &&
         (noeud1P != NUL || noeud2P != NUL || noeud3P != NUL ||
          noeud4P != NUL || noeud6P != NUL)) ||
       (noeud6P == NUL &&
         (noeud1P != NUL || noeud2P != NUL || noeud3P != NUL ||
          noeud4P != NUL || noeud5P != NUL)) ||
       (noeud1P != NUL &&
         (noeud1P == noeud2P || noeud1P == noeud3P || noeud1P == noeud4P ||
          noeud1P == noeud5P || noeud1P == noeud6P || noeud2P == noeud3P ||
          noeud2P == noeud4P || noeud2P == noeud5P || noeud2P == noeud6P ||
          noeud3P == noeud4P || noeud3P == noeud5P || noeud3P == noeud6P ||
          noeud4P == noeud5P || noeud4P == noeud6P || noeud5P == noeud6P))), conditionP);

}

#include "MGElementT6L.hpp"

#endif  // MGElEMENT_T6L_H_DEJA_INCLU
