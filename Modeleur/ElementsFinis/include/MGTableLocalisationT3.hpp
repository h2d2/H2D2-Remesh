//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     $Id$
//
// Classe:      EFMaillage2D
//
// Sommaire:    Interface de définition de la classe EFMaillage2D
//
// Description: 
//    Instanciation de la classe template MGMaillage<T> avec EFTraitsElement3D.
//
// Attributs:
//
// Note:
//
//************************************************************************
#ifndef MGTABLELOCALISATIONT3_HPP_DEJA_INCLU
#define MGTABLELOCALISATIONT3_HPP_DEJA_INCLU

#include <sstream>

template <typename TTMaillage>
const EntierN MGTableLocalisationT3<TTMaillage>::EF_MAILLE_ELEMENT = 4;
template <typename TTMaillage>
const EntierN MGTableLocalisationT3<TTMaillage>::EF_MAX_MAILLE = 50 /*200*/;

//**************************************************************
// Sommaire:
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
MGTableLocalisationT3<TTMaillage>::MGTableLocalisationT3(const TCMaillage& maillage)
   :  maillageP(&maillage)
   ,  origineTable()
   ,  tableDelX(DReel(0.0))
   ,  tableDelY(DReel(0.0))
   ,  tableDelZ(DReel(0.0))
   ,  tableIMax(0)
   ,  tableJMax(0)
   ,  tableKMax(0)
   ,  tableMatrice(NUL)
   ,  nbrElemIllegaux(0)
   ,  nbrElemErr(0)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
MGTableLocalisationT3<TTMaillage>::~MGTableLocalisationT3()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   videTable();
}

//**************************************************************
// Sommaire: Insère l'élément P1 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementP1& elemP1: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementP1(const typename TCMaillage::TCElementP1& /*elemP1*/)
{
   ++nbrElemIllegaux;
}
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementL2(const typename TCMaillage::TCElementL2& /*elemL2*/)
{
   ++nbrElemIllegaux;
}
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementL3(const typename TCMaillage::TCElementL3& /*elemL3*/)
{
   ++nbrElemIllegaux;
}
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L& /*elemL3L*/)
{
   ++nbrElemIllegaux;
}

//**************************************************************
// Sommaire:  Construit et ajoute les éléments de peau du Q4.
//    dans le setElementP commun.
//
// Description: 
//
// Entrée:  const TCElementQ4& elemQ4: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementQ4(const typename TCMaillage::TCElementQ4& elemQ4)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = elemQ4.listeMaille(tableMatrice,
                                  tableIMax, tableJMax, tableKMax,
                                  tableDelX, tableDelY, tableDelZ,
                                  origineTable);
   if (!msg)
   {
      nbrElemErr++;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T3
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT3& elemT3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementT3(const typename TCMaillage::TCElementT3& elemT3)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = elemT3.listeMaille(tableMatrice,
                                  tableIMax, tableJMax, tableKMax,
                                  tableDelX, tableDelY, tableDelZ,
                                  origineTable);
   if (!msg)
   {
      nbrElemErr++;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6& elemT6: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementT6(const typename TCMaillage::TCElementT6& /*elemT6*/)
{
   ++nbrElemIllegaux;
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6L
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6L& elemT6L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L& elemT6L)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = elemT6L.listeMaille(tableMatrice,
                                   tableIMax, tableJMax, tableKMax,
                                   tableDelX, tableDelY, tableDelZ,
                                   origineTable);
   if (!msg)
   {
      nbrElemErr++;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Description:
//    Localise l'élément qui contient le point
//
// Entrée:
//    TCCoord &point           // Coordonnées du point à localiser
//
// Sortie:
//     TCElementP&   elemP     // Pointeur à l'élément contenant le point
//                             // elemP=NUL si le point est hors du maillage
//                             // ou de la zone.
// Notes:
//    elemP ne doit pas pointer vers une structure existante (i-e pas de new).
//    IMPORTANT: Fonctionne UNIQUEMENT dans le cas d'un maillage d'éléments T6L.
//
//************************************************************************
template <typename TTMaillage>
ERMsg 
MGTableLocalisationT3<TTMaillage>::chercheElement (TCElementP& elemP,
                                                   const TCCoord& point,
                                                   const DReel& epsilon) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
  
   ERMsg msg = ERMsg::OK;

   // --- Crée la table si c'est nécessaire
   if (tableMatrice == NUL)
   {
      msg = const_cast<TCSelf*>(this)->creeTable();
      if (!msg)
      {
         elemP=NUL;
         return msg;
      }
   }
 
   // ---  Recherche des indices dans la table
   // --- Pour régler le problème de précision de calcul en passant
   // --- de DReel à EntierN, on doit passer par une variable temporaire
   // --- de type DReel.  En effet, lors d'un calcul, tous les DReel sont
   // --- convertis en LReel (long double) et une erreur s'insère à la
   // --- fin du chiffre (erreur aléatoire selon nos tests).  Donc le
   // --- résultat en LReel peut ne pas être exact.  Il doit être convertit
   // --- en DReel d'abord pour tenter de diminuer l'erreur.
   // --- Cette variable est utilisé pour convertir en EntierN.  Par
   // --- contre, notre fameux compilateur optimise tout et puisque
   // --- le processeur numérique contient toujours la valeur de la
   // --- variable temporaire, c'est ce registre qui est lu et non la
   // --- variable en mémoire.  Pour esquiver cette lacune, on passe
   // --- par un pointeur à notre variable temporaire.  Le compilateur est
   // --- donc forcé d'utiliser la valeur en mémoire et non celle du
   // --- processeur numérique.
   Entier  i, j, k;
   EntierN nbrEleMaille;
 
   DReel f = 0.0;
   DReelP fP = &f;
   f = (point.x() - origineTable.x()) / tableDelX;
   i = static_cast<Entier>(*fP);
 
   Entier ntableIMax = tableIMax;
   if (i >= 0 && i < ntableIMax)
   {
      f = (point.y() - origineTable.y()) / tableDelY;
      j= static_cast<Entier>(*fP);
      Entier ntableJMax = tableJMax;
      if ( j >= 0 && j < ntableJMax)
      {
         //   f = (point.y() - noeudMin.coordonnees().y()) / tableDelY;
         //   k= *fP;         // signé ... Marie-Josée
         k = Entier(0);
         Entier ntableKMax = tableKMax;
         if ( k >= 0 && k < ntableKMax)
         {
            // ---  Boucle sur les éléments de la liste
            tableMatrice[i][j][k]->nbrElem(nbrEleMaille);
            for (EntierN indice = 0; indice < nbrEleMaille; indice++)
            {
               elemP = TCElementP((*tableMatrice[i][j][k])[indice]);
               if (elemP->pointInterieur(point, epsilon))
                  return msg;  // --- on vient de trouver le bon élément.
            }
         } // fin de la condition sur k
     } // fin de la condition sur j
 
   } // fin de la condition sur i
  
 // --- Initialise à aucun élément trouvé
   elemP = NUL;
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Crée la table de localisation
//
// Description:
//    Crée la table de localisation. Cette table est une grille régulière
//    de type différences finies qui stocke pour chaque maille les adresses
//    des éléments couverts par la maille.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTMaillage>
ERMsg MGTableLocalisationT3<TTMaillage>::creeTable()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   EntierN  i, j, k;
   // ---  Detruit l'ancienne table si nécessaire
   videTable();

   // ---  Initialise la table de localisation
   msg = initTable();

   // ---  Dimensionne la matrice de pointeurs de CLListe
   if (msg)
   {
      tableMatrice = new CLListeP**[tableIMax];
      for (i = 0; i < tableIMax; i++)
      {
         tableMatrice[i] = new CLListeP*[tableJMax];
         for (j = 0; j < tableJMax; j++)
         {
            tableMatrice[i][j] = new CLListeP[tableKMax];
            for (k = 0; k < tableKMax; k++)
            {
               tableMatrice[i][j][k] = new CLListe;
            }
         }
      }
   }

   // ---  Boucle sur les éléments pour remplir la liste
   if (msg)
   {
      this->itereSurTousMGElements(*maillageP);

      if (nbrElemErr > 0)
      {
         msg = ERMsg(ERMsg::ERREUR, "ERR_TABLE_INVALIDE");
         std::ostringstream os;
         os << "MSG_NBR_ELEMENTS_INVALIDE: " << nbrElemErr;
         msg.ajoute(os.str());
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Description:
//    calcule les paramètres de la table de localisation.
//
// Entrée:
//
// Sortie:
//
// Notes: pour déterminer le nombre souhaitable de mailles dans la table,
//        on multiplie le nombre total d'éléments dans le maillage par un
//        facteur constant EF_MAILLE_ELEMENT.  On essaie de garder une forme
//        carrée aux mailles en se basant sur le rapport longueur/largeur.
//
//************************************************************************
template <typename TTMaillage>
ERMsg MGTableLocalisationT3<TTMaillage>::initTable()
{
#ifdef MODE_DEBUG
   PRECONDITION(maillageP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   TCCoord coordMin;
   TCCoord coordMax;
   maillageP->reqMinMax(coordMin, coordMax); 

   const DReel longueur = coordMax[0] - coordMin[0];
   const DReel largeur  = coordMax[1] - coordMin[1];
   const DReel hauteur  = 0;
   const DReel forme = longueur / largeur;

   // --- Afin d'éviter de surcharger la mémoire, on limite le nombre
   // --- de mailles à EF_MAX_MAILLE**2.
   EntierN nbrElemTot = maillageP->reqNbrElemTotal();
   EntierN nombreMailles = nbrElemTot * EF_MAILLE_ELEMENT;
   EntierN maxMailles = (EF_MAX_MAILLE * EF_MAX_MAILLE);
   nombreMailles = std::min(nombreMailles, maxMailles);

   Entier mailleX = static_cast<Entier>(sqrt(nombreMailles * forme)) + 1;
   Entier mailleY = static_cast<Entier>(sqrt(nombreMailles / forme)) + 1;

   // ---  Pas de la maille
   tableDelX = longueur / (DReel(mailleX) - 0.5);
   tableDelY = largeur  / (DReel(mailleY) - 0.5);
   tableDelZ = hauteur * 1.05 + 1.0e-8;     // En attendant le 3D

   // --- Nombre de mailles dans chaque direction
   tableIMax = EntierN(ceil(longueur / tableDelX));
   tableJMax = EntierN(ceil(largeur  / tableDelY));
   tableKMax = EntierN(ceil(hauteur  / tableDelZ));

   // --- Pour être bien certain de coincer tout le domaine, on s'assure
   // --- qu'il y a au moins une maille dans toutes les directions
   if (tableIMax < 1) tableIMax = 1;
   if (tableJMax < 1) tableJMax = 1;
   if (tableKMax < 1) tableKMax = 1;

   // --- Décale de 0.2 * dimension pour être sur d'englober tout le domaine.
   origineTable = coordMin - 0.2 * TCCoord(tableDelX, tableDelY);
   tableDelX = (longueur + 0.4 * tableDelX) / tableIMax;
   tableDelY = (largeur + 0.4 * tableDelY) / tableJMax;
   tableDelZ = (hauteur + 0.4 * tableDelZ) / tableKMax;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename TTMaillage>
void MGTableLocalisationT3<TTMaillage>::videTable()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Detruit la table de localisation
   if (tableMatrice != NUL)
   {
      for (EntierN ii=0;  ii<tableIMax; ii++)
      {
         for (EntierN jj = 0; jj < tableJMax; jj++)
         {
            for (EntierN kk = 0; kk < tableKMax; kk++)
            {
               delete tableMatrice[ii][jj][kk];
            }
            delete [] tableMatrice[ii][jj];
         }
         delete [] tableMatrice[ii];
      }
      delete [] tableMatrice;
      tableMatrice = NUL;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

#endif //   MGTABLELOCALISATIONT3_HPP_DEJA_INCLU

