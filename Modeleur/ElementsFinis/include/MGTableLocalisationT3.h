//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     $Id$
//
// Classe:      MGTableLocalisation
//
// Sommaire:    Interface de définition de la classe MGTableLocalisation
//
// Description: 
//    Instanciation de la classe template MGMaillage<T> avec EFTraitsElement3D.
//
// Attributs:
//
// Note:
//
//************************************************************************
#ifndef MGTABLELOCALISATIONT3_H_DEJA_INCLU
#define MGTABLELOCALISATIONT3_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGConstElementAlgorithme.h"

template <typename TTMaillage>
class MGTableLocalisationT3
   : public MGConstElementAlgorithme<TTMaillage>
{
public:
   typedef MGTableLocalisationT3<TTMaillage> TCSelf;
   typedef          TTMaillage               TCMaillage;
   typedef typename TCMaillage::TCCoord      TCCoord;
   typedef typename TCMaillage::TCElementP   TCElementP;

                  MGTableLocalisationT3  (const TCMaillage&);
   virtual       ~MGTableLocalisationT3  ();

   virtual void   executeAlgoMGElementP1 (const typename TCMaillage::TCElementP1&);
   virtual void   executeAlgoMGElementL2 (const typename TCMaillage::TCElementL2&);
   virtual void   executeAlgoMGElementL3 (const typename TCMaillage::TCElementL3&);
   virtual void   executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L&);
   virtual void   executeAlgoMGElementQ4 (const typename TCMaillage::TCElementQ4&);
   virtual void   executeAlgoMGElementT3 (const typename TCMaillage::TCElementT3&);
   virtual void   executeAlgoMGElementT6 (const typename TCMaillage::TCElementT6&);
   virtual void   executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L&);

           ERMsg  chercheElement         (TCElementP&, const TCCoord&, const DReel&) const;

protected:
   virtual void   invariant              (ConstCarP) const;

private:
   TCMaillage const* maillageP;
   TCCoord  origineTable;
   DReel    tableDelX;
   DReel    tableDelY;
   DReel    tableDelZ;
   EntierN  tableIMax;
   EntierN  tableJMax;
   EntierN  tableKMax;
   CLListeP ***tableMatrice;

   Entier nbrElemIllegaux;
   Entier nbrElemErr;

   ERMsg creeTable();
   ERMsg initTable();
   void  videTable();

   static const EntierN EF_MAILLE_ELEMENT;
   static const EntierN EF_MAX_MAILLE;
};

//************************************************************************
// Description:
//    Contrôle que les indices soient consistant
//
// Entrée:
//     CarP condition:   PRECONDITION ou POSTCONDITION
//
// Sortie:
//     Fin du programme en cas d'erreur.
//
// Notes:
//
//************************************************************************
template <typename TTMaillage>
inline void MGTableLocalisationT3<TTMaillage>::invariant(ConstCarP conditionP) const
{
   MGConstElementAlgorithme<TTMaillage>::invariant(conditionP);
   INVARIANT(maillageP != NUL, conditionP);
}

#include "MGTableLocalisationT3.hpp"

#endif //MGTABLELOCALISATIONT3_H_DEJA_INCLU

