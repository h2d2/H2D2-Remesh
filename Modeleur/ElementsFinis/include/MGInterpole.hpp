//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGInterpole.hpp
// Classe : MGInterpole
//************************************************************************
// 30-04-1998  Yves Roy          Version initiale
// 24-07-2003  Eric Larouche     Nettoyage du code
// 03-10-2003  Olivier Kaczor    Ajout du type d'élément P1
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 02-02-2004  Maude Giasson     Remplacer les TTTrait::TTElementXX par des TTElementXX
// 16-02-2004  Maude Giasson     TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGINTERPOLE_HPP_DEJA_INCLU
#define MGINTERPOLE_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:     Constructeur de l'algorithme d'interpolation.
//
// Description:  Le constructeur de MGInterpole permet d'initialiser les
//               attributs de l'algorithme de telle sorte que celui-ci puisse
//               répondre à des demandes d'interpolation.
//
// Entrée:       const TTTraits::TCMaillage& unMaillage : le maillage à utiliser
//               const TCChamp& unChamp : le champ de valeurs
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTChamp>
MGInterpole<TTChamp>::MGInterpole(const TCMaillage& unMaillage,
                                  const TCChamp& unChamp)
   : m_maillage(unMaillage)
   , m_champ(unChamp)
   , m_msgErr(ERMsg::OK)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Destructeur
//
// Description:  Destructeur
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTChamp>
MGInterpole<TTChamp>::~MGInterpole()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément P1
//
// Description:  La méthode executeAlgoMGElementP1(const TCTraits::TCElementP1&) 
//               permet d'exécuter l'algorithme sur un élément P1.
//               Fournit la valeur correspodnant au noeud.
//
// Entrée:       const TCTraits::TCElementP1& : non tenu en compte.
//
// Sortie:
//
// Notes:
//    Les opération sur TCDonnee sont écrites de manière à éviter toute
//    assignation et toute création de variables temporaires.
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementP1(const typename TCSelf::TCElementP1& elemP1)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //la valeur correspond à celle du noeud.
   (*m_resultatP) += m_champ[elemP1.noeud1P->reqIndVno()];
   

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément L2
//
// Description:  La méthode executeAlgoMGElementL2(const TCTraits::TCElementL2&)
//               permet d'exécuter l'algorithme sur un élément L2.
//               Fournit la valeur correspondant au point s'il coïncide avec
//               l'un des noeuds. Autrement, fournit la valeur proportionelle
//               aux valeurs des extrémités en fonction de la distance
//               aux extrémités.
//
// Entrée:       const TCTraits::TCElementL2& : non tenu en compte.
//
// Sortie:
//
// Notes:
//    Permettre d'interpoler sur des éléments variant seulement sur l'axe 
//    des X (dans le cadre des séries 1d).
//
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementL2(const typename TCSelf::TCElementL2& elemL2)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel ksi;
   elemL2.transformationInverse(ksi, m_point);

   // --- transformationInverse peut retourner un booléen à FAUX
   // --- mais dans ce cas-ci on extrapole en sachant que l'erreur
   // --- est très faible et acceptable.
   (*m_resultatP) += m_champ[elemL2.noeud1P->reqIndVno()] * 0.5 * (1.0 - ksi);
   (*m_resultatP) += m_champ[elemL2.noeud2P->reqIndVno()] * 0.5 * (1.0 + ksi);


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément L3
//
// Description:  La méthode executeAlgoMGElementL3(const TCTraits::TCElementL3&) permet
//               d'exécuter l'algorithme sur un élément L3 qui dans ce cas-ci
//               ne peut interpoler et donc il initialise un message d'erreur.
//               Ce message est un attribut de l'algorithme et on en tiendra
//               compte dans le traitement de la méthode interpole()...
//
// Entrée:       const TCTraits::TCElementL3& : non tenu en compte.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementL3(const typename TCSelf::TCElementL3&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_msgErr = ERMsg(ERMsg::ERREUR, "ERR_INTERPOLATION_IMPOSSIBLE");
   m_msgErr.ajoute("MSG_ELEMENTL3");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément L3L
//
// Description:  La méthode executeAlgoMGElementL3L(const TCTraits::TCElementL3L&) permet
//               d'exécuter l'algorithme sur un élément L3L qui dans ce cas-ci
//               ne peut interpoler et donc il initialise un message d'erreur.
//               Ce message est un attribut de l'algorithme et on en tiendra
//               compte dans le traitement de la méthode interpole()...
//
// Entrée:       const TCTraits::TCElementL3L& : non tenu en compte.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementL3L(const typename TCSelf::TCElementL3L&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_msgErr = ERMsg(ERMsg::ERREUR, "ERR_INTERPOLATION_IMPOSSIBLE");
   m_msgErr.ajoute("MSG_ELEMENTL3L");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément Q4
//
// Description:  La méthode executeAlgoMGElementQ4(const TCTraits::TCElementQ4&)
//               permet d'exécuter l'algorithme sur un élément Q4.
//
// Entrée:       const TCTraits::TCElementQ4& : L'élément sur lequel on interpole.
//
// Sortie:
//
// Notes:
//    Les opération sur TCDonnee sont écrites de manière à éviter toute
//    assignation et toute création de variables temporaires.
//
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementQ4(const typename TCSelf::TCElementQ4& elemQ4)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel ksi, eta;
   elemQ4.transformationInverse(ksi, eta, m_point);

   // --- On calcule la nouvelle valeur
   (*m_resultatP) += m_champ[elemQ4.noeud1P->reqIndVno()] * 0.25 * (1.0 - ksi) * (1.0 - eta);
   (*m_resultatP) += m_champ[elemQ4.noeud2P->reqIndVno()] * 0.25 * (1.0 + ksi) * (1.0 - eta);
   (*m_resultatP) += m_champ[elemQ4.noeud3P->reqIndVno()] * 0.25 * (1.0 + ksi) * (1.0 + eta);
   (*m_resultatP) += m_champ[elemQ4.noeud4P->reqIndVno()] * 0.25 * (1.0 - ksi) * (1.0 + eta);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément T3
//
// Description:  La méthode executeAlgoMGElementT3(const TCTraits::TCElementT3&) 
//               permet d'exécuter l'algorithme sur un élément T3.
//
// Entrée:       const TCTraits::TCElementT3& : L'élément sur lequel on interpole.
//
// Sortie:
//
// Notes:
//    Les opération sur TCDonnee sont écrites de manière à éviter toute
//    assignation et toute création de variables temporaires.
//
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementT3(const typename TCSelf::TCElementT3& elemT3)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel ksi;
   DReel eta;
   elemT3.transformationInverse(ksi, eta, m_point);

   // --- transformationInverse peut retourner un booléen à FAUX
   // --- mais dans ce cas-ci on extrapole en sachant que l'erreur
   // --- est très faible et acceptable.
   DReel lambda = 1.0 - ksi - eta;
   (*m_resultatP) += m_champ[elemT3.noeud1P->reqIndVno()] * lambda;
   (*m_resultatP) += m_champ[elemT3.noeud2P->reqIndVno()] * ksi;
   (*m_resultatP) += m_champ[elemT3.noeud3P->reqIndVno()] * eta;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément T6
//
// Description:  La méthode executeAlgoMGElementT6(const TTTraits::TCElementT6&) 
//               permet d'exécuter l'algorithme sur un élément T6.
//
// Entrée:       const TCTraits::TCElementT6& : L'élément sur lequel on interpole.
//
// Sortie:
//
// Notes:
//    Les opération sur TCDonnee sont écrites de manière à éviter toute
//    assignation et toute création de variables temporaires.
//
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementT6(const typename TCSelf::TCElementT6& elemT6)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel ksi;
   DReel eta;
   elemT6.transformationInverse(ksi, eta, m_point);

   // --- transformationInverse peut retourner un booléen à FAUX
   // --- mais dans ce cas-ci on extrapole en sachant que l'erreur
   // --- est très faible et acceptable.
   DReel lambda = 1.0 - ksi - eta;
   (*m_resultatP) += m_champ[elemT6.noeud1P->reqIndVno()] * lambda * (2.0*lambda -1.0);
   (*m_resultatP) += m_champ[elemT6.noeud2P->reqIndVno()] * 4.0*ksi*lambda;
   (*m_resultatP) += m_champ[elemT6.noeud3P->reqIndVno()] * ksi*(2.0*ksi -1.0);
   (*m_resultatP) += m_champ[elemT6.noeud4P->reqIndVno()] * 4.0*ksi*eta;
   (*m_resultatP) += m_champ[elemT6.noeud5P->reqIndVno()] * eta*(2.0*eta -1.0);
   (*m_resultatP) += m_champ[elemT6.noeud6P->reqIndVno()] * 4.0*eta*lambda;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Exécute l'algorithme sur un élément T6L
//
// Description:  La méthode executeAlgoMGElementT6L(const TCTraits::TCElementT6L&) 
//               permet d'exécuter l'algorithme sur un élément T6L.
//
// Entrée:       const TCTraits::TCElementT6L& : L'élément sur lequel on interpole.
//
// Sortie:
//
// Notes:
//    Les opération sur TCDonnee sont écrites de manière à éviter toute
//    assignation et toute création de variables temporaires.
//
//************************************************************************
template <typename TTChamp>
void MGInterpole<TTChamp>::executeAlgoMGElementT6L(const typename TCSelf::TCElementT6L& elemT6L)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::TCNoeud* TCNoeudP;

   DReel ksi;
   DReel eta;
   elemT6L.transformationInverse(ksi, eta, m_point);

   // --- transformationInverse peut retourner un booléen à FAUX
   // --- mais dans ce cas-ci on extrapole en sachant que l'erreur
   // --- est très faible et acceptable.
   DReel lambda = 1.0 - ksi - eta;

   // --- On cherche dans quel sous domaine T3 nous sommes
   TCNoeudP noeudTmp1P;
   TCNoeudP noeudTmp2P;
   TCNoeudP noeudTmp3P;
   if (ksi >= 0.5)
   {
      noeudTmp1P = elemT6L.noeud2P;
      noeudTmp2P = elemT6L.noeud3P;
      noeudTmp3P = elemT6L.noeud4P;
   }
   else if (eta >= 0.5)
   {
      noeudTmp1P = elemT6L.noeud6P;
      noeudTmp2P = elemT6L.noeud4P;
      noeudTmp3P = elemT6L.noeud5P;
   }
   else if (lambda >= 0.5)
   {
      noeudTmp1P = elemT6L.noeud1P;
      noeudTmp2P = elemT6L.noeud2P;
      noeudTmp3P = elemT6L.noeud6P;
   }
   else
   {
      noeudTmp1P = elemT6L.noeud6P;
      noeudTmp2P = elemT6L.noeud2P;
      noeudTmp3P = elemT6L.noeud4P;
   }
   ASSERTION(noeudTmp1P != NUL);
   ASSERTION(noeudTmp2P != NUL);
   ASSERTION(noeudTmp3P != NUL);

   DReel t3Y3Y1 = noeudTmp3P->reqCoordonnees().y() - noeudTmp1P->reqCoordonnees().y();
   DReel t3X3X1 = noeudTmp3P->reqCoordonnees().x() - noeudTmp1P->reqCoordonnees().x();
   DReel t3Y2Y1 = noeudTmp2P->reqCoordonnees().y() - noeudTmp1P->reqCoordonnees().y();
   DReel t3X2X1 = noeudTmp2P->reqCoordonnees().x() - noeudTmp1P->reqCoordonnees().x();
   DReel t3Jacobien = 1.0 / ((t3X2X1 * t3Y3Y1) - (t3X3X1 * t3Y2Y1));
   DReel t3X0X1 = m_point.x() - noeudTmp1P->reqCoordonnees().x();
   DReel t3Y0Y1 = m_point.y() - noeudTmp1P->reqCoordonnees().y();

   // --- On calcule les coordonnées ksi, eta sur l'élément de référence
   DReel t3Ksi = (t3Y3Y1 * t3X0X1 - t3X3X1 * t3Y0Y1) * t3Jacobien;
   DReel t3Eta = (t3X2X1 * t3Y0Y1 - t3X0X1 * t3Y2Y1) * t3Jacobien;
   DReel t3Lambda = 1.0 - t3Ksi - t3Eta;

   // --- On calcule la nouvelle valeur
   (*m_resultatP) += m_champ[noeudTmp1P->reqIndVno()] * t3Lambda;
   (*m_resultatP) += m_champ[noeudTmp2P->reqIndVno()] * t3Ksi;
   (*m_resultatP) += m_champ[noeudTmp3P->reqIndVno()] * t3Eta;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:     Méthode qui déclenche l'exécution de l'interpolation.
//
// Description:  La méthode interpole(...) permet de faire le traitement de
//               l'interpolation en utilisant les algorithmes de repérage
//               du maillage pour ensuite appeler l'exécution de cet algorithme
//               sur l'élément à l'intérieur duquel on doit interpoler.
//
// Entrée:       const TCCoord& pt : le point auquel on doit interpoler.
//               const DReel& epsilon : le degré de tolérance, par défaut 0.0
//
// Sortie:       TCDonnee& valeur : le résultat de l'interpolation
//
// Notes:        On présume que tous les éléments du maillage sont de même type.
// 
//************************************************************************
template <typename TTChamp>
ERMsg MGInterpole<TTChamp>::interpole(TCDonnee& valeur,
                                      const TCCoord& point,
                                      const DReel& epsilon)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Recherche l'élément contenant le point
   typename TCMaillage::TCElement* elementP = NUL;
   msg = m_maillage.chercheElement(elementP, point, epsilon);
   if (elementP == NUL)
   {
      msg = ERMsg(ERMsg::ERREUR, "ERR_INTERPOLATION_ELEMENT_INTROUVABLE");
   }

   // --- Interpolation au sein de l'élément
   if (msg)
   {
      m_point = point;
      m_resultatP = &valeur;
      (*m_resultatP) *= 0.0;

      m_msgErr = ERMsg::OK;   // le message de l'algo
      elementP->executeAlgorithme(*this);

      if (! m_msgErr)
      {
         msg = m_msgErr;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif  // MGINTERPOLE_HPP_DEJA_INCLU

