//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: MGElementT6.hpp
// Classe : MGElementT6
//************************************************************************
// 28-07-2003  Maxime Derenne      Version initiale
// 04-08-2003  Maxime Derenne      Transfert de méthode inline
// 25-10-2003  Olivier Kaczor      Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor      Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor      Rendre compilable sur GCC
// 16-02-2004  Maude Giasson       TT->TC à l'intérieur de la classe
//************************************************************************
#ifndef MGELEMENT_T6_HPP_DEJA_INCLU
#define MGELEMENT_T6_HPP_DEJA_INCLU

#include "clliste.h"
//#include "clvect.h"

#include "MGConst.h"
#include "MGElementT3.h"
//#include "MGElementAlgorithme.h"
//#include "MGConstElementAlgorithme.h"

#include <algorithm>
#include <vector>

//**************************************************************
// Description:
//   Constructeur par défaut, sans argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementT6<TTTraits>::MGElementT6()
: TCSelf::TCElement(0),
  noeud1P(NUL),
  noeud2P(NUL),
  noeud3P(NUL),
  noeud4P(NUL),
  noeud5P(NUL),
  noeud6P(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // MGElementT6 ()


//**************************************************************
// Description:
//   Initialise le numéro de l'élément ainsi que ses six noeuds
//   correspondants.
//
// Entrée:
//   EntierN element     : le numéro de l'élément qui relie les six noeuds
//   TCNoeudPP &noeuds   : Référence sur un tableau de pointeur sur des noeuds.
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementT6<TTTraits>::MGElementT6(EntierN element, 
  typename MGElementT6<TTTraits>::TCNoeudPP noeudsPP)
: TCSelf::TCElement(element),
  noeud1P(noeudsPP[0]),
  noeud2P(noeudsPP[1]),
  noeud3P(noeudsPP[2]),
  noeud4P(noeudsPP[3]),
  noeud5P(noeudsPP[4]),
  noeud6P(noeudsPP[5])
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // MGElementT6 (EntierN, TCNoeudPP)


//**************************************************************
// Description:
//   ne fait rien car aucune allocation dynamique n'a été faite
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
MGElementT6<TTTraits>::~MGElementT6()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}  // ~MGElementT6 ()

//************************************************************************
// Sommaire: Compare les connectivités et le type d'élément.
//
// Description: Compare les connectivités et le type d'élément.  Retourne VRAI
//              si le type d'élément est inférieur ou si c'est le mème type
//              d'élément, mais que la connectivité  soit inférieure.  Pour que
//              la connectivité soit considérée inférieure, il faut qu'une fois
//              les NoNoeuds ordonnés des deux éléments cet élément-ci (this) possède
//              un noeud avec un # inférieur dans les n premiers noeuds comparés
//              à ceux de l'autre élément.
//
// Entrée:
//   ConstMGElementP elem          : Element à comparer
//
// Sortie:
//   Booleen                           : VRAI ou FAUX
//
// Notes:
//  L'algorithmie n'est pas complètement implantée, car nous n'en avons pas
//  besoin pour l'instant dans un T6.
//************************************************************************
template <typename TTTraits>
Booleen MGElementT6<TTTraits>::compareConnectiviteInferieure(typename MGElementT6<TTTraits>::ConstTCElementP elemP) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef MGElementT6<typename MGElementT6<TTTraits>::TCTraits> const * ConstTCSelfP;
   Booleen reponse = FAUX;

   if (this->reqType() != elemP->reqType())
   {
      reponse = this->reqType() < elemP->reqType();
   }
   else
   {
      ASSERTION(dynamic_cast<ConstTCSelfP>(elemP) != NUL);
      ConstTCSelfP elemT6P = static_cast<ConstTCSelfP>(elemP);
      typedef std::vector<typename MGElementT6<TTTraits>::TCNoeudP> VectorTCNoeudP;
      VectorTCNoeudP vecNoeudsLocal(6);
      VectorTCNoeudP vecNoeudsElem(6);

      vecNoeudsLocal[0] = noeud1P;
      vecNoeudsLocal[1] = noeud2P;
      vecNoeudsLocal[2] = noeud3P;
      vecNoeudsLocal[3] = noeud4P;
      vecNoeudsLocal[4] = noeud5P;
      vecNoeudsLocal[5] = noeud6P;
      std::sort(vecNoeudsLocal.begin(), vecNoeudsLocal.end());

      vecNoeudsElem[0] = elemT6P->noeud1P;
      vecNoeudsElem[1] = elemT6P->noeud2P;
      vecNoeudsElem[2] = elemT6P->noeud3P;
      vecNoeudsElem[3] = elemT6P->noeud4P;
      vecNoeudsElem[4] = elemT6P->noeud5P;
      vecNoeudsElem[5] = elemT6P->noeud6P;
      std::sort(vecNoeudsElem.begin(), vecNoeudsElem.end());

      typename VectorTCNoeudP::iterator noeudLocalI = vecNoeudsLocal.begin();
      typename VectorTCNoeudP::const_iterator noeudLocalFinI = vecNoeudsLocal.end();
      typename VectorTCNoeudP::iterator noeudElemI = vecNoeudsElem.begin();

      while(noeudLocalI != noeudLocalFinI)
      {
        if (*noeudLocalI != *noeudElemI)
         {
            reponse = *noeudLocalI < *noeudElemI;
         }
         noeudLocalI++;
         noeudElemI++;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return reponse;
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementT6
//
// Entrée: TCAlgo& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT6<TTTraits>::executeAlgorithme(typename MGElementT6<TTTraits>::TCAlgo& algo)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementT6(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Execute l'algorithme passé en argument
//
// Description: Execute l'algorithme passé en argument en appelant la méthode
//              correspondant à la classe MGElementT6.  L'algorithme ne peut
//              modifier l'élément puisque l'élément est constant.
//
// Entrée: TCAlgoConst& algo: algorithme à ètre exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT6<TTTraits>::executeAlgorithme(typename MGElementT6<TTTraits>::TCAlgoConst& algo) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   algo.executeAlgoMGElementT6(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline ERMsg MGElementT6<TTTraits>::listeMaille(CLListeP***, EntierN, EntierN, EntierN,
                                                DReel, DReel, DReel, TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return ERMsg(ERMsg::OK);
}

//**************************************************************
// Description:
//   Ordonne (du plus petit au plus grand) les trois noeuds passés en
//   paramètre selon leur numéro de noeud respectif. Très utile pour
//   trouver la peau de l'élément T6.
//
// Entrée:
//   TCNoeudP no1P, no2P, no3P:
//      Les trois pointeurs de noeuds formant un côté de l'élément.
//
// Sortie:
//   EntierN& no1, no2, no3:
//      Les trois noeuds qui vont être dans le bon ordre
//
// Notes:
// Préconditions:
//   Les trois pointeurs de noeuds doivent être non-nuls.
//
//**************************************************************
template <typename TTTraits>
void   MGElementT6<TTTraits>::ordonneNoeuds (EntierN& no1, EntierN& no2, EntierN& no3,
                                             typename MGElementT6<TTTraits>::TCNoeudP no1P, typename MGElementT6<TTTraits>::TCNoeudP no2P, typename MGElementT6<TTTraits>::TCNoeudP no3P)
{
#ifdef MODE_DEBUG
   PRECONDITION(no1P != NUL);
   PRECONDITION(no2P != NUL);
   PRECONDITION(no3P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (no1P->reqNoNoeud() < no2P->reqNoNoeud())
      if (no2P->reqNoNoeud() < no3P->reqNoNoeud())
      {
         no1 = no1P->reqNoNoeud(); no2 = no2P->reqNoNoeud(); no3 = no3P->reqNoNoeud();
      }
      else
         if (no1P->reqNoNoeud() < no3P->reqNoNoeud())
         {
            no1 = no1P->reqNoNoeud(); no2 = no3P->reqNoNoeud(); no3 = no2P->reqNoNoeud();
         }
         else
         {
            no1 = no3P->reqNoNoeud(); no2 = no1P->reqNoNoeud(); no3 = no2P->reqNoNoeud();
         }
   else
      if (no3P->reqNoNoeud() < no2P->reqNoNoeud())
      {
         no1 = no3P->reqNoNoeud(); no2 = no2P->reqNoNoeud(); no3 = no1P->reqNoNoeud();
      }
      else
         if (no1P->reqNoNoeud() < no3P->reqNoNoeud())
         {
            no1 = no2P->reqNoNoeud(); no2 = no1P->reqNoNoeud(); no3 = no3P->reqNoNoeud();
         }
         else
         {
            no1 = no2P->reqNoNoeud(); no2 = no3P->reqNoNoeud(); no3 = no1P->reqNoNoeud();
         }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // ordonneNoeuds (EntierN&, EntierN&, ...)

//**************************************************************
// Description:
//    interpole sur la valeurs nodale (scalaire) du point passé en argument
//
// Entrée:
//    TCCoord point     point où on fait l'interpolation
//
// Sortie:
//    TCNoeud&         resultat  structure de noeud contenant les valeurs
//                               interpolées
//
// Notes:
//    En cas d'extrapolation, les calculs sont effectués tout de même mais
//    on retourne une erreur.
//
//**************************************************************
template <typename TTTraits>
Booleen  MGElementT6<TTTraits>::pointInterieur (const TCCoord& point,
                                                const DReel& epsilon) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel ksi, eta;
   Booleen resultat = transformationInverse(ksi, eta, point);
   if (!resultat)
   {
      // --- On refait un test avec un epsilon plus tolérant
      DReel lambda = 1.0 - ksi - eta;
      resultat = !((GOEpsilon(ksi, epsilon) < 0.0) ||
                   (GOEpsilon(eta, epsilon) < 0.0) ||
                   (GOEpsilon(lambda, epsilon) < 0.0));

   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return resultat;
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TCNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT6<TTTraits>::reqNoeuds(typename MGElementT6<TTTraits>::TCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize)
#else
                                      EntierN /*bufferSize*/)
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;
   noeuds[3] = noeud4P;
   noeuds[4] = noeud5P;
   noeuds[5] = noeud6P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire: Retourne la liste des noeuds contenus dans l'élément.
//
// Description:
//    Cette méthode permet d'obtenir la liste des noeuds contenus dans
//    l'élément sans toutefois connaître le type de l'élément manipulé.
//
// Entrée:
//    TTNoeudPP &noeuds  : Référence sur un tableau de pointeur sur des noeuds.
//    EntierN bufferSize : Dimension du tableau passé précédemment.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
void MGElementT6<TTTraits>::reqNoeuds(typename MGElementT6<TTTraits>::ConstTCNoeudPP &noeuds,
#ifdef MODE_DEBUG
                                      EntierN bufferSize) const
#else
                                      EntierN /*bufferSize*/) const
#endif
{
#ifdef MODE_DEBUG
   PRECONDITION(bufferSize >= reqNbrNoeud());
   INVARIANTS("PRECONDITION");
#endif

   noeuds[0] = noeud1P;
   noeuds[1] = noeud2P;
   noeuds[2] = noeud3P;
   noeuds[3] = noeud4P;
   noeuds[4] = noeud5P;
   noeuds[5] = noeud6P;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Description:
//    Ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline void MGElementT6<TTTraits>::reqVectNoeud(TCCoord &, EntierN)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Description:
//    Retourne le nombre de noeud contenu dans l'élément.
//
// Entrée:
//
// Sortie:
//    EntierN : Nombre de noeud.
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline EntierN MGElementT6<TTTraits>::reqNbrNoeud() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return 6;
}

//************************************************************************
// Description:
//    Retourne le type de l'élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTTraits>
inline typename MGElementT6<TTTraits>::Type MGElementT6<TTTraits>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return TCSelf::TYPE_T6;
}

//**************************************************************
// Sommaire:     Fonction utilitaire de calcul des fonctions d'interpolation.
//
// Description:  La fonction utilitaire transformationInverse(...) permet,
//               pour un point d'interpolation, de calculer les trois fonctions
//               d'interpolation, soient ksi, eta et lambda.
//
// Entrée:       const TTCoord& point: le point à interpoler.
//
// Sortie:       DReel& ksi    : composante x normalisée
//               DReel& eta    : composante y normalisée
//               DReel& lambda : 1 - ksi - eta
//
// Notes:
//
//**************************************************************
template <typename TTTraits>
Booleen MGElementT6<TTTraits>::transformationInverse(DReel& ksi, DReel& eta,
                                                     const TCCoord& point) const
{
#ifdef MODE_DEBUG
   PRECONDITION(noeud1P != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel   EPSILON = 1.0e-10;
   const EntierN MAXITER = 6;

   DReel x0 = point[0];
   DReel x1 = noeud1P->reqCoordonnees()[0];
   DReel x2 = noeud2P->reqCoordonnees()[0];
   DReel x3 = noeud3P->reqCoordonnees()[0];
   DReel x4 = noeud4P->reqCoordonnees()[0];
   DReel x5 = noeud5P->reqCoordonnees()[0];
   DReel x6 = noeud6P->reqCoordonnees()[0];

   DReel y0 = point[1];
   DReel y1 = noeud1P->reqCoordonnees()[1];
   DReel y2 = noeud2P->reqCoordonnees()[1];
   DReel y3 = noeud3P->reqCoordonnees()[1];
   DReel y4 = noeud4P->reqCoordonnees()[1];
   DReel y5 = noeud5P->reqCoordonnees()[1];
   DReel y6 = noeud6P->reqCoordonnees()[1];

   DReel a11, a12, a21, a22, xi, yi;
   DReel deltaKsi, deltaEta, dKsi, dEta;

   ksi = 0.0;
   eta = 0.0;
   EntierN iter = 0;

   do
   {
      DReel lambda = 1.0 - ksi - eta;

      a11 = x1*(1.0- 4.0*lambda) + 4.0*x2*(lambda - ksi) + x3 * (4.0 * ksi -1.0) +
            4.0 * x4 * eta - 4.0* x6 * eta;
      a12 = y1*(1.0- 4.0*lambda) + 4.0*y2*(lambda - ksi) + y3 * (4.0 * ksi -1.0) +
            4.0 * y4 * eta - 4.0* y6 * eta;
      a21 = x1*(1.0- 4.0*lambda) - 4.0*x2*ksi + 4.0*x4*ksi + x5*(1.0- 4.0*eta)    +
            x6*(4.0*eta*ksi);
      a22 = y1*(1.0- 4.0*lambda) - 4.0*y2*ksi + 4.0*y4*ksi + y5*(1.0- 4.0*eta)    +
            4.0*y6*(lambda - eta);

      xi = x1 * lambda * (2.0*lambda -1.0) + 4.0*x2*ksi*lambda + x3*ksi*(2.0*ksi -1.0) +
           4.0*x4*ksi*eta + x5*eta*(2.0*eta -1.0) + 4.0*x6*eta*lambda;

      yi = y1 * lambda * (2.0*lambda -1.0) + 4.0*y2*ksi*lambda + y3*ksi*(2.0*ksi -1.0) +
           4.0*y4*ksi*eta + y5*eta*(2.0*eta -1.0) + 4.0*y6*eta*lambda;

      deltaKsi = (( x0 - xi) * a22 + (yi - y0)*a12)/(a11*a22 - a12*a21);
      deltaEta = (( xi - x0) * a21 + (y0 - yi)*a11)/(a11*a22 - a12*a21);

      ksi += deltaKsi;
      eta += deltaEta;
      dKsi = fabs(deltaKsi);
      dEta = fabs(deltaEta);
   } while ((dKsi > EPSILON || dEta > EPSILON) && (iter++ < MAXITER));

   DReel lambda = 1.0 - ksi - eta;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return  !((GOEpsilon(ksi) < 0.0) ||
             (GOEpsilon(eta) < 0.0) ||
             (GOEpsilon(lambda) < 0.0) ||
             iter == MAXITER);
}


//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description: Méthode de sauvegarde virtuelle qui sauvegarde les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&                           : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementT6<TTTraits>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::ecrisVirtuel(os);
   }

   if (os)
   {
      os << filtre(noeud1P) << ESPACE
         << filtre(noeud2P) << ESPACE
         << filtre(noeud3P) << ESPACE
         << filtre(noeud4P) << ESPACE
         << filtre(noeud5P) << ESPACE
         << filtre(noeud6P);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   //   MODE_PERSISTANT



//************************************************************************
// Sommaire: Récupère l'élément
//
// Description: Méthode de lecture virtuelle qui lis les informations
//              de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on lit
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTTraits>
void MGElementT6<TTTraits>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      TCElement::lisVirtuel(is);
   }

   TCNoeudP n1P, n2P, n3P, n4P, n5P, n6P;
   if (is)
   {
      is >> filtre(n1P) >> filtre(n2P) >> filtre(n3P) >> filtre(n4P) >> filtre(n5P) >> filtre(n6P);
   }
   if (is)
   {
      noeud1P = n1P;
      noeud2P = n2P;
      noeud3P = n3P;
      noeud4P = n4P;
      noeud5P = n5P;
      noeud6P = n6P;
   }
   if (is)
   {
      if (noeud1P == noeud2P || noeud1P == noeud3P || noeud1P == noeud4P ||
          noeud1P == noeud5P || noeud1P == noeud6P || noeud2P == noeud3P ||
          noeud2P == noeud4P || noeud2P == noeud5P || noeud2P == noeud6P ||
          noeud3P == noeud4P || noeud3P == noeud5P || noeud3P == noeud6P ||
          noeud4P == noeud5P || noeud4P == noeud6P || noeud5P == noeud6P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem;
         noElem << reqNoElement();
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = noeud4P = noeud5P = noeud6P = NUL;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_PERSISTANT

//************************************************************************
// Sommaire: Exporte l'élément
//
// Description: Méthode d'exportation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier dans lequel on s'exporte
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementT6<TTTraits>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCElement::exporte(os);
   }
   if (os)
   {
      os << (noeud1P->reqNoNoeud()+1) << ESPACE
         << (noeud2P->reqNoNoeud()+1) << ESPACE
         << (noeud3P->reqNoNoeud()+1) << ESPACE
         << (noeud4P->reqNoNoeud()+1) << ESPACE
         << (noeud5P->reqNoNoeud()+1) << ESPACE
         << (noeud6P->reqNoNoeud()+1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_IMPORT_EXPORT
//************************************************************************
// Sommaire: Importe l'élément
//
// Description: Méthode d'importation de l'élément
//
// Entrée:
//   FIFichier& os            : Fichier duquel on s'importe
//   const CLListe& lstNoeuds : liste contenant les pointeurs aux noeuds.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTTraits>
void MGElementT6<TTTraits>::importe(FIFichier& is, const CLListe& lstNoeuds)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if(is)
   {
      TCElement::importe(is,lstNoeuds);
   }

   EntierN n1, n2, n3, n4, n5, n6;
   if (is)
   {
      is >> n1 >> n2 >> n3 >> n4 >> n5 >> n6;
   }
   if (is)
   {
      if (n1 < 1 || n2 < 1 || n3 < 1 || n4 < 1 || n5 < 1 || n6 < 1)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NUMEROTATION_NOEUD");
         msg.ajoute("MSG_NUMEROTATION_A_PARTIR_DE_1");

         CLChaine noElem = "#";
         noElem << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
      else
      {
         n1--; n2--; n3--; n4--; n5--; n6--;
      }
   }
   if (is)
   {
      EntierN nbrNoeuds = lstNoeuds.dimension();
      if( n1 < nbrNoeuds && n2 < nbrNoeuds && n3 < nbrNoeuds  &&
          n4 < nbrNoeuds && n5 < nbrNoeuds && n6 < nbrNoeuds)
      {
         noeud1P = TCNoeudP(lstNoeuds[n1]);
         noeud2P = TCNoeudP(lstNoeuds[n2]);
         noeud3P = TCNoeudP(lstNoeuds[n3]);
         noeud4P = TCNoeudP(lstNoeuds[n4]);
         noeud5P = TCNoeudP(lstNoeuds[n5]);
         noeud6P = TCNoeudP(lstNoeuds[n6]);
      }
      else
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_CONNECTIVITE_INVALIDE");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }
   if (is)
   {
      if (noeud1P == noeud2P || noeud1P == noeud3P || noeud1P == noeud4P ||
          noeud1P == noeud5P || noeud1P == noeud6P || noeud2P == noeud3P ||
          noeud2P == noeud4P || noeud2P == noeud5P || noeud2P == noeud6P ||
          noeud3P == noeud4P || noeud3P == noeud5P || noeud3P == noeud6P ||
          noeud4P == noeud5P || noeud4P == noeud6P || noeud5P == noeud6P)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_NOEUDS_IDENTIQUE_DANS_ELEMENT");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);

         noeud1P = noeud2P = noeud3P = noeud4P = noeud5P = noeud6P = NUL;
      }
   }

   // --- Ici, on construit 4 T3 pour au moins vérifier si avec des fonctions
   // --- d'approximations linéaires, le déterminant du jacobien est positif.
   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3

      //--- On forme le premier élément T3
      varEleT3.noeud1P = noeud1P;
      varEleT3.noeud2P = noeud2P;
      varEleT3.noeud3P = noeud6P;
      DReel determinant = varEleT3.calculeDetJ();
      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3

      //--- On forme le second élément T3
      varEleT3.noeud1P = noeud2P;
      varEleT3.noeud2P = noeud3P;
      varEleT3.noeud3P = noeud4P;
       DReel determinant = varEleT3.calculeDetJ();

      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3

      //--- On forme le troisième élément T3
      varEleT3.noeud1P = noeud6P;
      varEleT3.noeud2P = noeud4P;
      varEleT3.noeud3P = noeud5P;
      DReel determinant = varEleT3.calculeDetJ();

      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

   if (is)
   {
      TCElementT3 varEleT3;   // Une variable de l'élément T3

      //--- On forme le dernier élément T3
      varEleT3.noeud1P = noeud4P;
      varEleT3.noeud2P = noeud6P;
      varEleT3.noeud3P = noeud2P;
      DReel determinant = varEleT3.calculeDetJ();

      if (determinant <= 0)
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DETERMINANT_<EG0");
         CLChaine noElem;
         noElem << "#" << (reqNoElement()+1);
         msg.ajoute("MSG_ELEMENT_T6");
         msg.ajoute(noElem.reqConstCarP());
         is.asgErreur(msg);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif //MODE_IMPORT_EXPORT

#endif  // MGELEMENT_T6_HPP_DEJA_INCLU
