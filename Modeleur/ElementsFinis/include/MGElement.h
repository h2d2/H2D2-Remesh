//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGElement.h
//
// Classe:  MGElement
//
// Description:
//    La classe MGElement est la classe abstraite qui regroupe les classes
//    de tous les éléments.
//    <p>
//    La classe est définie par le paramètre template TTTraits.  TTTrait
//    regroupe les structures nécessaires aux calculs par la méthode des
//    éléments finis.
//    <p>
//    Le trait utilisé doit définir les types suivants :
//       TCCoord:    type de coordonnées des noeuds et nombre de dimensions
//       TCNoeud:    type de noeuds
//
// Attributs:
//       EntierN noElement: numéro de l'élément
//
// Notes:
//       typedef     TCNoeud**               TCNoeudPP;
//       Ce type représente un pointeur vers un tableau de noeuds.
//       On n'emploie pas directement un tableau de noeuds, puisque
//       sa taille varie et que gcc ne l'accepte pas en paramètre de
//       méthode template si la taille n'est pas définie.
//       Il faut dimensinner la variable lors de son instanciation:
//          TCNoeudP* noeudsPP = new TCNoeudP[2];
//
//************************************************************************
// 05-03-1993  François Gingras  Version initiale
// 21-06-1993  François Gingras  Modifié
// 08-12-1995  Eric Paquet       Déclaration des classes avec DECLARE_CLASS
// 11-03-1996  Eric Chamberland  Révision des entêtes, implantation des VNOs
// 01-04-1996  Eric Chamberland  Méthodes integre, opDeriveX et Y, interpole et
//                               traceDeformee1D sont mises =0.
// 30-04-1996  André Gagné       Implantation de lisVirtuel et ecrisVirtuel
//                               donc elle ne sont plus à 0
// 07-06-1996  Eric Chamberland  +limination des méthodes doublées pour les VNOs
// 28-06-1996  Eric Chamberland  Ajout de executeAlgorithme(...)
// 12-11-1996  Eric Chamberland  Ajout de importe/exporte, TYPE_L3L
// 15-11-1996  Serge Dufour      Vérification des dépendances
// 19-11-1996  Eric Chamberland  Ajout de compareConnectiviteInferieure
// 16-10-1997  Yves Secretan     Passage à la double précision : 1.ère passe
// 25-11-1997  Yves Roy          Modification concernant les inclusions des vnos.
// 29-04-1998  Yves Roy          Change signature de pointInterieur et transformationInverse...
// 30-04-1998  Yves Roy          Élimination de interpole de l'interface de l'élément.
// 22-07-1998  Yves Secretan     Passe aux includes du C++
// 29-09-1998  Yves Secretan     Migre traceIsosurface dans un algorithme
// 25-07-2003  Maxime Derenne    Nettoyage du code
// 28-07-2003  Maxime Derenne    Retrait des méthodes reqTypeChaine et entierToAscii
//                               Ajout des méthodes reqNbrNoeud, reqNoeuds et reqType
// 04-08-2003  Maxime Derenne    Transfert du destructeur dans le cpp
// 20-09-2003  Olivier Kaczor    Ajout du type d'élément P1
// 25-10-2003  Olivier Kaczor    Change l'abréviation EF pour MG
// 25-10-2003  Olivier Kaczor    Rendre la classe template de TTTraits
// 01-12-2003  Olivier Kaczor    Rendre compilable sur GCC
// 03-02-2004  Maude Giasson     Diminue types définis par le TTTraits
//************************************************************************
#ifndef MGELEMENT_H_DEJA_INCLU
#define MGELEMENT_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.hf"
#include "MGMaillage.hf"
#include "MGElementAlgorithme.hf"
#include "MGConstElementAlgorithme.hf"

DECLARE_CLASS(CLListe);
#ifdef MODE_IMPORT_EXPORT
   DECLARE_CLASS(FIFichier);
#endif

template <typename TTTraits>
class MGElement
{
public:
   typedef          MGElement<TTTraits>         TCSelf;
   typedef          TTTraits                    TCTraits;

   typedef typename TCTraits::TCNoeud           TCNoeud;
   typedef          TCNoeud*                    TCNoeudP;
   typedef typename TCTraits::TCCoord           TCCoord;

   typedef MGMaillage<TCTraits>                 TCMaillage;
   typedef MGElementAlgorithme<TCMaillage>      TCAlgo;
   typedef MGConstElementAlgorithme<TCMaillage> TCAlgoConst;

   typedef       TCNoeud**               TCNoeudPP;
   typedef const TCNoeud*                ConstTCNoeudP;
   typedef ConstTCNoeudP*                ConstTCNoeudPP;

   typedef       MGElement<TTTraits>     TCElement;
   typedef       TCElement*              TCElementP;
   typedef const TCElement*              ConstTCElementP;

   enum Type
   {
      TYPE_NIL,
      TYPE_P1,
      TYPE_L2,
      TYPE_L3,
      TYPE_L3L,
      TYPE_T3,
      TYPE_T6L,
      TYPE_T6,
      TYPE_Q4,
      TYPE_Q9,
      TYPE_TH4,
      TYPE_H8
   };
   friend Type operator++(Type& t)      { return t = (Type)(t + 1); }
   friend Type operator++(Type& t, int) { return t = (Type)(t + 1); }
   friend Type operator--(Type& t)      { return t = (Type)(t - 1); }
   friend Type operator--(Type& t, int) { return t = (Type)(t - 1); }

                      MGElement              (EntierN);
   virtual           ~MGElement              ();

   virtual Booleen    compareConnectiviteInferieure(ConstTCElementP) const = 0;
   virtual void       executeAlgorithme      (TCAlgo&) = 0;
   virtual void       executeAlgorithme      (TCAlgoConst&) const = 0;

   virtual ERMsg      listeMaille            (CLListeP***, EntierN, EntierN, EntierN,
                                              DReel, DReel, DReel, TCCoord&) const = 0;
   virtual Booleen    pointInterieur         (const TCCoord&, const DReel& = 0) const = 0;
   virtual EntierN    reqNbrNoeud            () const = 0;
   virtual void       reqNoeuds              (TCNoeudPP &, EntierN) = 0;
   virtual void       reqNoeuds              (ConstTCNoeudPP &, EntierN) const = 0;
   virtual Type       reqType                () const = 0;

   // Cette méthode est implantée dans les MGElements mais n'y fait rien!
   virtual void       reqVectNoeud           (TCCoord&, EntierN) = 0;
//           EntierN&   reqNoElement           () {return noElement;};
           EntierN    reqNoElement           () const {return noElement;};
           void       asgNoElement           (EntierN noElem) {noElement = noElem;};



protected:
   virtual void       invariant              (ConstCarP) const;

private:
   EntierN noElement;

#ifdef MODE_IMPORT_EXPORT
public:
   virtual void       exporte                (FIFichier&) const = 0;
   virtual void       importe                (FIFichier&, const CLListe&) = 0;
#endif   // MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
public:
#ifndef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   friend FIFichier& operator<< <>(FIFichier&, const MGElement<TTTraits>&);
   friend FIFichier& operator>> <>(FIFichier&, MGElement<TTTraits>&);
#else
   friend FIFichier& operator<<   (FIFichier&, const MGElement<TTTraits>&);
   friend FIFichier& operator>>   (FIFichier&, MGElement<TTTraits>&);
#endif  //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM

protected:
   virtual void       ecrisVirtuel           (FIFichier&) const;
   virtual void       lisVirtuel             (FIFichier&);
#endif   // MODE_PERSISTANT
};

template <typename TTTraits>
class compareConnectiviteInferieure
{
public:
   typedef const MGElement<TTTraits>* TCConstElemP;
   Booleen operator()(const TCConstElemP& elem1P, const TCConstElemP& elem2P) const
   {
      return elem1P->compareConnectiviteInferieure(elem2P);
   }
};

//**************************************************************
// Description:
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTTraits>
inline void MGElement<TTTraits>::invariant(ConstCarP /*conditionP*/) const
{

}

#include "MGElement.hpp"

#endif  // MGELEMENT_H_DEJA_INCLU
