//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     $Id$
//
// Classe:      MGTableLocalisation
//
// Sommaire:    Interface de définition de la classe MGTableLocalisation
//
// Description: 
//
// Attributs:
//
// Note:
//
//************************************************************************
#ifndef MGTABLELOCALISATION_HPP_DEJA_INCLU
#define MGTABLELOCALISATION_HPP_DEJA_INCLU

//**************************************************************
// Sommaire:
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
MGTableLocalisation<TTMaillage>::MGTableLocalisation(const TCMaillage& maillage)
   :  m_maillageP(&maillage)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
MGTableLocalisation<TTMaillage>::~MGTableLocalisation()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//************************************************************************
// Description:
//    Localise l'élément qui contient le point
//
// Entrée:
//    TCCoord &point           // Coordonnées du point à localiser
//
// Sortie:
//     TCElementP&   elemP     // Pointeur à l'élément contenant le point
//                             // elemP=NUL si le point est hors du maillage
//                             // ou de la zone.
// Notes:
//
//************************************************************************
template <typename TTMaillage>
ERMsg 
MGTableLocalisation<TTMaillage>::chercheElement(TCElementP& elemP,
                                                const TCCoord& point,
                                                const DReel& epsilon) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
  
   typedef typename TCMaillage::iterateurElementConst TCIterateurMaillage;

   ERMsg msg = ERMsg::OK;

   elemP = NUL;

         TCIterateurMaillage eleI    = m_maillageP->reqElementDebut();
   const TCIterateurMaillage eleFinI = m_maillageP->reqElementFin();
   for ( ; eleI != eleFinI; ++eleI)
   {            
      if ((*eleI)->pointInterieur(point, epsilon))
      {
         elemP = *eleI;
         break;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif //   MGTABLELOCALISATION_HPP_DEJA_INCLU

