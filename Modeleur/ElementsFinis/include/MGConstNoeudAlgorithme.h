//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// Fichier: MGConstNoeudAlgorithme.h
//
// Classe:  MGConstNoeudtAlgorithme
//
// Description:
//         Classe abstraite d'algorithmes devant itérer sur les noeuds d'un maillage.
//
//   La classe est définie par le paramètre template TTTraits.  TTTrait regroupe 
//   les structures nécessaires aux calculs par la méthode des éléments finis. 
//
//   Le trait utilisé doit définir les types suivants : 
//      TCValeur    : type des valeurs porté par une coordonnée
//      TCCoord     : type de coordonnées des noeuds
//      TCNoeud     : type faisant référence aux noeuds
//      TCElement   : type faisant référence aux éléments
//      TCMaillage  : type du maillage
//      TCElementXX : type des types d'élément XX pour un maillage
//      TCAlgo      : type des algorithmes sur un élément
//      TCAlgoConst : type des algorithmes constants sur un élément
//
// Attributs:
//
// Notes: - Classe construite en considérant que TTTrait::TCElementXX est un MGElement,
//        - Dans le futur, on pourrait avoir une fonction executeAlgo() virtuelle. 
//          Dans les classes filles, on retrouverait une fonction d'initialisation +
//          la définition de la fonction executeAlgo(). À voir.
//************************************************************************
// 02-02-2004  Maude Giasson     Version initiale
//************************************************************************
#ifndef MGCONSTNOEUDALGORITHME_H_DEJA_INCLU
#define MGCONSTNOEUDALGORITHME_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

template <typename TTMaillage>
class MGConstNoeudAlgorithme
{
public:
   typedef TTMaillage TCMaillage;
   
             MGConstNoeudAlgorithme ();
   virtual  ~MGConstNoeudAlgorithme ();

protected:
   virtual void invariant                 (ConstCarP) const;
};

//**************************************************************
// Description:
//
// Entrée:
//   ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie: Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
inline void MGConstNoeudAlgorithme<TTMaillage>::invariant(ConstCarP /*conditionP*/) const
{
}

#include "MGConstNoeudAlgorithme.hpp"

#endif  // MGCONSTNOEUDALGORITHME_H_DEJA_INCLU
