//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: MGConst.cpp
//
// Description:
//    Ce fichier contient la définition des constantes liées aux
//    éléments.
//
// Notes:
//
//************************************************************************
// 16-10-1995  Serge Dufour    Déclaration des variables const en variable extern
// 16-10-1997  Yves Secretan   Passage à la double précision
// 24-07-2003  Eric Larouche   Nettoyage du code
// 05-02-2004  Maude Giasson   EFConst devient MGConst
//************************************************************************

#include "MGConst.h"

const EntierN EF_MAXIMUMENTIERN = ~0;

const DReel   EF_EPSILON = 10E-6;

const EntierN EF_NOMBRENOEUDSMAX = 9;
