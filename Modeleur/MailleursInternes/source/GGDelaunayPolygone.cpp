//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunayPolygone.cpp
// Classe:   GGDelaunayPolygone
//*****************************************************************************
// 05-12-98  Yves Secretan    Version initiale
// 25-09-98  Yves Secretan    Change le scope des for et while
// 08-01-99  Yves Secretan    Tolère les domaines triangulaires
// 21-01-99  Yves Secretan    Ajoute reqAire()
//*****************************************************************************

#include "GGDelaunayPolygone.h"

#include "GGDelaunayAngleFront.h"
#include "GGDelaunayArete.h"
#include "GGDelaunaySommet.h"
#include "GGDelaunaySurface.h"
#include "GGContainerAreteARetourner.h"

#include "GGMailleurDelaunay.h"
#include "polypartition.h"

#include <algorithm>
#include <map>
#include <set>
#include <utility>
#include <vector>

#ifdef MODE_NAMESPACE
using namespace std;
//using std::map;
//using std::multiset;   // L'utilisation de using séparés semble créer un
//using std::less;       // problème au niveau du compilateur. Il n'y a pas
//using std::find_if;    // vraiment de contre indication à utiliser tout
//using std::ptr_fun;    // le namespace ici.
#endif

//************************************************************************
// Sommaire:  Constructeur par défaut.
//
// Description:
//    Le constructeur publique <code>GGMailleurDelaunay()</code> est le
//    constructeur par défaut de la classe.
//
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayPolygone::GGDelaunayPolygone()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur.
//
// Description:
//    Le destructeur publique <code>~GGMailleurDelaunay()</code> vide
//    les listes maintenues par l'objet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayPolygone::~GGDelaunayPolygone()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   sommetsPeau.erase(sommetsPeau.begin(), sommetsPeau.end());
   aretesPeau.erase(aretesPeau.begin(), aretesPeau.end());
}

//************************************************************************
// Sommaire:  Ajoute une arête au polygone.
//
// Description:
//    La méthode publique <code>ajouteArete(...)</code> ajoute une arête
//    à la liste des arêtes qui forment le polygone. Les arêtes doivent
//    être ajoutées dans l'ordre de parcours du polygone. De plus, elles
//    doivent correspondre aux sommets, le première arête doit contenir le
//    premier et second sommet, etc...
//
// Entrée:
//    GGDelaunayAreteP aP             Arête à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayPolygone::ajouteArete(GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aP != NUL);
   PRECONDITION(aP->reqSommet0() == sommetsPeau[aretesPeau.size()] ||
                aP->reqSommet1() == sommetsPeau[aretesPeau.size()]);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   aretesPeau.push_back(aP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Ajoute un sommet au polygone.
//
// Description:
//    La méthode publique <code>ajouteSommet(...)</code> ajoute un sommet
//    à la liste des sommets du polygone. Les sommets doivent être ajoutés
//    dans l'ordre de parcours du polygone.
//
// Entrée:
//    GGDelaunaySommetP sP             Sommet à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayPolygone::ajouteSommet(GGDelaunaySommetP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   sommetsPeau.push_back(sP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Génère la maillage.
//
// Description:
//    La méthode publique <code>genereMaillage(...)</code> maille le polygone
//    qui doit avoir été spécifié auparavant à l'aide de sommets et d'arêtes.
//
// Entrée:
//    GGMailleurDelaunayP mP     Mailleur à notifier de la construction
//
// Sortie:
//    DReel& avancement          Permet de suivre l'avancement du maillage
//
// Notes:
// 1) YSe - 2022-11-12: Mise en commentaire
//    Le retournement des arêtes n'est pas possible ici.
//    Les arêtes externes sont déjà liées aux 2 anciennes
//    surfaces ainsi qu'à une nouvelle surface.
//    Il n'y a plus de place et une précondition saute.
//    Retourne le polygone non balancé, charge à l'appelant
//    de le balancer après avoir détaché les anciennes surfaces.
//
//************************************************************************
ERMsg GGDelaunayPolygone::genereMaillage(GGMailleurDelaunayP mP, Entier lvl)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommetsPeau.size() >= 3);
   PRECONDITION(aretesInternes.size() == 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Contrôles
   if (reqAire() < 1.0e-6) msg = ERMsg(ERMsg::ERREUR, "ERR_POLYGONE_DEGENERE");

/* YSe c.f. note 1)
   // ---  Sauvegarde l'état des arêtes
   //      Bloque inconditionnellement le balancement des arêtes
   vector<Booleen> statusAretesPeau;
   if (msg)
   {
      statusAretesPeau.reserve(aretesPeau.size());
      const TCContainerArete::iterator eoaI = aretesPeau.end();
      for (TCContainerArete::iterator aI = aretesPeau.begin(); aI != eoaI; ++aI)
      {
         statusAretesPeau.push_back((*aI)->estRetournable());
         (*aI)->libereRetournement(FAUX);
      }
   }
*/

   // ---  Forme le maillage à partir de la peau
   if (msg)
   {
      msg = genereMaillagePeau(mP, lvl);
   }

   // ---  Retourne les arêtes internes du maillage
/* YSe c.f. note 1)
   if (msg)
   {
      msg = retourneAretes();
   }

   // ---  Réinitialise l'état des arêtes de la peau
   if (msg)
   {
      TCContainerArete::iterator aI = aretesPeau.begin();
            vector<Booleen>::const_iterator bI   = statusAretesPeau.begin();
      const vector<Booleen>::const_iterator eobI = statusAretesPeau.end();
      while(bI != eobI)
      {
         (*aI++)->libereRetournement(*bI++);
      }
   }
*/

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Génère le maillage de base avec les sommets de la peau.
//
// Description:
//    La méthode privée <code>genereMaillagePeau(...)</code> génère le premier
//    maillage à partir des sommets de la peau.
//    <p>
//    Le maillage est obtenu par une méthode frontale en éliminant successivement
//    les angles les plus aigus du front.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGDelaunayPolygone::genereMaillagePeau(GGMailleurDelaunayP mP, Entier lvl)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommetsPeau.size() >= 3);
   PRECONDITION(sommetsPeau.size() == aretesPeau.size());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   if (false)
      msg = genereMaillagePeau_I(mP, lvl);
   else
      msg = genereMaillagePeau_E(mP, lvl);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Génère le maillage de base avec les sommets de la peau.
//
// Description:
//    La méthode privée <code>genereMaillagePeau(...)</code> génère le premier
//    maillage à partir des sommets de la peau.
//    <p>
//    Le maillage est obtenu par une méthode frontale en éliminant successivement
//    les angles les plus aigus du front.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGDelaunayPolygone::genereMaillagePeau_I(GGMailleurDelaunayP mP, Entier lvl)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommetsPeau.size() >= 3);
   PRECONDITION(sommetsPeau.size() == aretesPeau.size());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   const GGDelaunayAreteArguments   artArgs = GGDelaunayAreteArguments().niveauRaff(lvl);
   const GGDelaunaySurfaceArguments srfArgs = GGDelaunaySurfaceArguments().niveauRaff(lvl);
   DReel aireGeneree = 0.0;

   // ---  Forme les angles du front
   typedef multiset<GGDelaunayAngleFront, less<GGDelaunayAngleFront> > TCContainerAngle;
   typedef map<GGDelaunaySommetP, TCContainerAngle::iterator, less<void*> > TTMapSommetAngle;
   TCContainerAngle angles;
   TTMapSommetAngle angleDuSommet;
   {
      TCContainerSommet::iterator sGI = sommetsPeau.end() - 1;
      TCContainerSommet::iterator sCI = sommetsPeau.begin();
      TCContainerSommet::iterator sDI = sCI + 1;
      TCContainerArete::iterator  aGI = aretesPeau.end() - 1;
      TCContainerArete::iterator  aDI = aretesPeau.begin();
      while (sCI != sommetsPeau.end())
      {
         TCContainerAngle::iterator angI = angles.insert(GGDelaunayAngleFront(*sCI, *sGI, *sDI, *aGI, *aDI));
         angleDuSommet.insert(TTMapSommetAngle::value_type(*sCI, angI));

         sGI = sCI;
         ++sCI;
         ++sDI;
         if (sDI == sommetsPeau.end())
            sDI = sommetsPeau.begin();
         aGI = aDI;
         ++aDI;
      }
   }

   // ---  Élimine les angles du front, toujours par le plus petit
   while (msg && angles.size() > 2)
   {
      const GGDelaunayAngleFront& aC = *(angles.begin());
      GGDelaunaySommetP sGP = aC.reqSommetGauche();
      GGDelaunaySommetP sCP = aC.reqSommetCentre();
      GGDelaunaySommetP sDP = aC.reqSommetDroite();
      GGDelaunayAreteP  aGP = aC.reqAreteGauche();
      GGDelaunayAreteP  aDP = aC.reqAreteDroite();

      // ---  Forme l'arête opposée et contrôle si elle existe déjà
      GGDelaunayAreteP aOP = NULL;
      TCContainerArete::iterator aI = find_if(aretesInternes.begin(), aretesInternes.end(), [&](auto const& aP) { return aP->aSommets(sGP, sDP); } );
      if (aI == aretesInternes.end())
      {
         aOP = new GGDelaunayArete(mP, sGP, sDP, artArgs);
         aOP->enleveReference();
         aretesInternes.push_back(aOP);
      }
      else
      {
         aOP = *aI;
      }

      // ---  Forme la surface
      const DReel aireSurface = GGDelaunaySurface::reqAireEuclide(sGP, sCP, sDP);
      if (fabs(aireSurface) < 1.0e-6)
      {
         msg = ERMsg(ERMsg::ERREUR, "ERR_SURFACE_DEGENEREE");
      }
      else if (aireSurface > 0)
      {
         GGDelaunaySurfaceP sP = new GGDelaunaySurface(mP, sGP, sCP, sDP, aGP, aDP, aOP, srfArgs);
         sP->enleveReference();
         surfacesInternes.push_back(sP);
         aireGeneree += aireSurface;
      }
      else
      {
         GGDelaunaySurfaceP sP = new GGDelaunaySurface(mP, sDP, sCP, sGP, aDP, aGP, aOP, srfArgs);
         sP->enleveReference();
         surfacesInternes.push_back(sP);
         aireGeneree -= aireSurface;
      }

      // ---  Elimine l'angle
      if (msg)
      {
         angles.erase(angles.begin());
         angleDuSommet.erase(angleDuSommet.find(sCP));
      }

      // ---  Change le sommet gauche sGP
      if (msg)
      {
         TTMapSommetAngle::iterator somGI = angleDuSommet.find(sGP);
         TCContainerAngle::iterator angGI = (*somGI).second;
         GGDelaunayAngleFront aG = *angGI;
         angles.erase(angGI);
         angleDuSommet.erase(somGI);
         if (angles.size() > 1)      // Si ce n'est pas le dernier triangle
         {
            aG.changeVoisinDroite(sDP, aOP);
            angGI = angles.insert(aG);
            angleDuSommet.insert(TTMapSommetAngle::value_type(sGP, angGI));
         }
      }

      // ---  Change le sommet droite sDP
      if (msg)
      {
         TTMapSommetAngle::iterator somDI = angleDuSommet.find(sDP);
         TCContainerAngle::iterator angDI = (*somDI).second;
         GGDelaunayAngleFront aD = *angDI;
         angles.erase(angDI);
         angleDuSommet.erase(somDI);
         if (angles.size() > 1)      // Si ce n'est pas le dernier triangle
         {
            aD.changeVoisinGauche(sGP, aOP);
            angDI = angles.insert(aD);
            angleDuSommet.insert(TTMapSommetAngle::value_type(sDP, angDI));
         }
      }
   }
   ASSERTION(!msg || angles.size() == 0);

   if (msg)
   {
      const DReel aireDemandee = reqAire();
      const DReel rapp = fabs((aireGeneree-aireDemandee) / aireDemandee);
      if (rapp > 0.0001)
      {
         msg = ERMsg(ERMsg::ERREUR, "ERR_DOMAINE_TROP_COMPLEXE");
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Génère le maillage de base avec les sommets de la peau.
//
// Description:
//    La méthode privée <code>genereMaillagePeau(...)</code> génère le premier
//    maillage à partir des sommets de la peau.
//    <p>
//    Le maillage est obtenu par une méthode frontale en éliminant successivement
//    les angles les plus aigus du front.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGDelaunayPolygone::genereMaillagePeau_E(GGMailleurDelaunayP mP, Entier lvl)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommetsPeau.size() >= 3);
   PRECONDITION(sommetsPeau.size() == aretesPeau.size());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   typedef std::list<TPPLPoly> TTList;

   const GGDelaunayAreteArguments   artArgs = GGDelaunayAreteArguments().niveauRaff(lvl);
   const GGDelaunaySurfaceArguments srfArgs = GGDelaunaySurfaceArguments().niveauRaff(lvl);

   // ---  Forme le pourtour
   const size_t nbSommets = sommetsPeau.size();
   TPPLPoly poly;
   poly.Init( static_cast<long>(nbSommets) );
   TCContainerSommet::const_iterator sCI = sommetsPeau.begin();
   for (int i = 0; i < nbSommets; ++i)
   {
      const GOCoordonneesXYZ& p = (*sCI++)->reqPosition();
      poly[i].x = p.x();
      poly[i].y = p.y();
   }

   // ---  Triangule
   TTList triangles;
   TPPLPartition tpl = TPPLPartition();
   if (triangles.size() == 0) tpl.Triangulate_OPT(&poly, &triangles);
   if (triangles.size() == 0) tpl.Triangulate_EC (&poly, &triangles);

   // ---  Forme les triangles
   DReel aireGeneree = 0.0;
   for(TTList::iterator iter = triangles.begin(); iter != triangles.end(); ++iter)
   {
      ASSERTION((*iter).GetNumPoints() == 3);

      // ---  Les points
      GOCoordonneesXYZ p1 = GOCoordonneesXYZ((*iter)[0].x, (*iter)[0].y);
      GOCoordonneesXYZ p2 = GOCoordonneesXYZ((*iter)[1].x, (*iter)[1].y);
      GOCoordonneesXYZ p3 = GOCoordonneesXYZ((*iter)[2].x, (*iter)[2].y);

      // ---  Les sommets
      TCContainerSommet::iterator s1I = find_if(sommetsPeau.begin(), sommetsPeau.end(), [&](auto const& sP) { return sP->reqPosition() == p1; });
      TCContainerSommet::iterator s2I = find_if(sommetsPeau.begin(), sommetsPeau.end(), [&](auto const& sP) { return sP->reqPosition() == p2; });
      TCContainerSommet::iterator s3I = find_if(sommetsPeau.begin(), sommetsPeau.end(), [&](auto const& sP) { return sP->reqPosition() == p3; });
      ASSERTION(s1I != sommetsPeau.end());
      ASSERTION(s2I != sommetsPeau.end());
      ASSERTION(s3I != sommetsPeau.end());
      GGDelaunaySommetP s1P = (*s1I);
      GGDelaunaySommetP s2P = (*s2I);
      GGDelaunaySommetP s3P = (*s3I);

      // ---  Les arêtes
      TCContainerArete::iterator a1I = aretesPeau.end();
      a1I = (a1I != aretesPeau.end()) ? a1I : find_if(aretesPeau.begin(),     aretesPeau.end(),     [&](auto const& aP) { return aP->aSommets(s1P, s2P); });
      a1I = (a1I != aretesPeau.end()) ? a1I : find_if(aretesInternes.begin(), aretesInternes.end(), [&](auto const& aP) { return aP->aSommets(s1P, s2P); });
      TCContainerArete::iterator a2I = aretesPeau.end();
      a2I = (a2I != aretesPeau.end()) ? a2I : find_if(aretesPeau.begin(),     aretesPeau.end(),     [&](auto const& aP) { return aP->aSommets(s2P, s3P); });
      a2I = (a2I != aretesPeau.end()) ? a2I : find_if(aretesInternes.begin(), aretesInternes.end(), [&](auto const& aP) { return aP->aSommets(s2P, s3P); });
      TCContainerArete::iterator a3I = aretesPeau.end();
      a3I = (a3I != aretesPeau.end()) ? a3I : find_if(aretesPeau.begin(),     aretesPeau.end(),     [&](auto const& aP) { return aP->aSommets(s3P, s1P); });
      a3I = (a3I != aretesPeau.end()) ? a3I : find_if(aretesInternes.begin(), aretesInternes.end(), [&](auto const& aP) { return aP->aSommets(s3P, s1P); });
      GGDelaunayAreteP a1P = (a1I == aretesInternes.end()) ? NUL : (*a1I);
      GGDelaunayAreteP a2P = (a2I == aretesInternes.end()) ? NUL : (*a2I);
      GGDelaunayAreteP a3P = (a3I == aretesInternes.end()) ? NUL : (*a3I);

      // ---  Construis les arêtes manquantes
      if (a1P == NUL)
      {
         a1P = new GGDelaunayArete(mP, s1P, s2P, artArgs);
         a1P->enleveReference();
         aretesInternes.push_back(a1P);
      }
      if (a2P == NUL)
      {
         a2P = new GGDelaunayArete(mP, s2P, s3P, artArgs);
         a2P->enleveReference();
         aretesInternes.push_back(a2P);
      }
      if (a3P == NUL)
      {
         a3P = new GGDelaunayArete(mP, s3P, s1P, artArgs);
         a3P->enleveReference();
         aretesInternes.push_back(a3P);
      }

      // ---  Forme la surface
      const DReel aireSurface = GGDelaunaySurface::reqAireEuclide(s1P, s2P, s3P);
      ASSERTION(aireSurface > 0.0);
      GGDelaunaySurfaceP sP = new GGDelaunaySurface(mP, s1P, s2P, s3P, a1P, a2P, a3P, srfArgs);
      sP->enleveReference();
      surfacesInternes.push_back(sP);
      aireGeneree += aireSurface;
   }

   if (msg)
   {
      const DReel aireDemandee = reqAire();
      const DReel rapp = fabs((aireGeneree-aireDemandee) / aireDemandee);
      if (rapp > 0.0001)
      {
         msg = ERMsg(ERMsg::ERREUR, "ERR_DOMAINE_TROP_COMPLEXE");
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Calcule l'aire du polygone.
//
// Description:
//    La méthode privée <code>reqAire(...)</code> retourne l'aire du
//    polygone.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGDelaunayPolygone::reqAire() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommetsPeau.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   DReel surface = 0.0;
   TCContainerSommet::const_iterator s0I = sommetsPeau.begin();
   TCContainerSommet::const_iterator s1I = s0I+1;
   TCContainerSommet::const_iterator s2I = s1I+1;
   while (s2I != sommetsPeau.end())
   {
      surface += GGDelaunaySurface::reqAireEuclide(*s0I, *s1I, *s2I);
      s1I = s2I;
      ++s2I;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return surface;
}

//************************************************************************
// Sommaire:  Retourne toutes les arêtes internes.
//
// Description:
//    La méthode privée <code>retourneAretes()</code> retourne toutes les
//    arêtes interne du maillage interne au polygone.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    En sortie, le container aretesInternes n'est plus valide
//
//************************************************************************
ERMsg GGDelaunayPolygone::retourneAretes()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aretesPeau.size()  >= 3);
   PRECONDITION(sommetsPeau.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Sauvegarde l'état des arêtes
   //      Bloque inconditionnellement le balancement des arêtes de la peau
   vector<Booleen> statusAretesPeau;
   if (msg)
   {
      statusAretesPeau.reserve(aretesPeau.size());
      const TCContainerArete::iterator eoaI = aretesPeau.end();
      for (TCContainerArete::iterator aI = aretesPeau.begin(); aI != eoaI; ++aI)
      {
         statusAretesPeau.push_back((*aI)->estRetournable());
         (*aI)->libereRetournement(FAUX);
      }
   }

   // ---  Transvase toutes les arêtes
   GGContainerAreteARetourner containerBalance;
   for (TCContainerArete::iterator i = aretesInternes.begin(); i != aretesInternes.end(); ++i)
      containerBalance.ajouteSansControle(*i);

   // ---  Balance toutes les arêtes du maillage
   while (containerBalance.reqDimension() != 0)
   {
      GGDelaunayAreteP artP = *containerBalance.reqAreteDebut();
      artP->ajouteReference();                      // Pour garantir la survie
      artP->retourne(containerBalance);             // Retourne
      artP->enleveReference();                      // Relâche
   }

   // ---  Réinitialise l'état des arêtes de la peau
   if (msg)
   {
      TCContainerArete::iterator aI = aretesPeau.begin();
      vector<Booleen>::const_iterator bI = statusAretesPeau.begin();
      const vector<Booleen>::const_iterator eobI = statusAretesPeau.end();
      while (bI != eobI)
      {
         (*aI++)->libereRetournement(*bI++);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

