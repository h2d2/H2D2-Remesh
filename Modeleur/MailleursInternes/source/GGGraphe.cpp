//************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGGraphe.cpp
// Classe:  GGGraphe
//************************************************************************
// 27-10-94  Yves Secretan     Version initiale
// 28-06-96  Eric Chamberland  Ajout d'appel aux invariants
// 12-10-96  Yves Secretan     Introduction des messages d'erreur
//                             en cas d'erreur tente d'autres algo
// 22-01-97  Serge Dufour      Changement dans les token de messages
//************************************************************************
#include "GGGraphe.h"

#include <iostream>

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
GGGraphe::GGGraphe () : vecNoeuds(1024), lstConnec(1024)
{

   poidDegre = 2;
   poidDist  = 1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
GGGraphe::~GGGraphe ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN  i;
   ChainonP tP;

   // ---  Efface les listes chainées
   for (i = 0; i < lstConnec.dimension(); i++)
   {
      while (lstConnec[i] != lstConnec[i]->suivantP)
      {
         tP = lstConnec[i]->suivantP;
         delete lstConnec[i];
         lstConnec[i] = tP;
      }
   }
   lstConnec.effaceTout();

}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::ajouteLien (const EntierN indNoeud1, const EntierN indNoeud2)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   ChainonP tP;

   // ---  Recherche si le lien existe déjà
   for (tP = lstConnec[indNoeud1]; tP != tP->suivantP; tP = tP->suivantP)
      if (tP->indNoeud == indNoeud2) return;

   // ---  Lien noeud1-noeud2
   tP = new Chainon;
   tP->indNoeud = indNoeud2;
   tP->suivantP = lstConnec[indNoeud1];
   lstConnec[indNoeud1] = tP;

   // ---  Lien noeud2-noeud1
   tP = new Chainon;
   tP->indNoeud = indNoeud1;
   tP->suivantP = lstConnec[indNoeud2];
   lstConnec[indNoeud2] = tP;

   // ---  Met à jour les degrés
   vecNoeuds[indNoeud1].degre++;
   vecNoeuds[indNoeud2].degre++;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::asgNiveaux (const EntierN indDepart)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN  i, niveau, nbrChange;
   ChainonP tP;

   // ---  Remet les niveaux à zéro
   for (i = 0; i < vecNoeuds.dimension(); i++)
   {
      vecNoeuds[i].niveau = 0;
      vecNoeuds[i].etat   = Noeud::INACTIF;
   }

   // ---  Niveau du noeud de départ
   vecNoeuds[indDepart].niveau = 1;
   vecNoeuds[indDepart].etat   = Noeud::ACTIF;

   // ---  Niveau de noeuds voisins du noeud de départ
   for (tP = lstConnec[indDepart]; tP != tP->suivantP; tP = tP->suivantP)
      vecNoeuds[tP->indNoeud].niveau = 2;

   // ---  Boucle sur les autres noeuds
   do
   {
      nbrChange = 0;
      for (i = 0; i < vecNoeuds.dimension(); i++)
      {
         niveau = vecNoeuds[i].niveau;
         if (niveau == 0 || vecNoeuds[i].etat == Noeud::ACTIF) continue;

         niveau++;
         for (tP = lstConnec[i]; tP != tP->suivantP; tP = tP->suivantP)
         {
            if (vecNoeuds[tP->indNoeud].niveau == 0)
            {
               vecNoeuds[tP->indNoeud].niveau = niveau;
               nbrChange++;
            }
         }
         vecNoeuds[i].etat = Noeud::ACTIF;
      }  // for (i = 0; i < vecNoeuds.dimension(); i++)

   }
   while (nbrChange != 0);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::asgPoids (const EntierN degre, const EntierN dist)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   poidDegre = degre;
   poidDist  = dist;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::initialise (const EntierN nbrNoeuds)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Noeud noeud;
   for (EntierN i = 0; i < nbrNoeuds; i++)
   {
      noeud.indice = i;
      vecNoeuds << noeud;
      lstConnec << (new Chainon);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
Entier GGGraphe::reqCheminMin (TPVecteurSimple<Noeud>& lst,
                             const EntierN indMin,
                             const EntierN indMax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN  niveau;
   ChainonP chP;

   // ---  Controle le niveau
   if (lst.dimension() >= vecNoeuds[indMin].niveau) return !OK;
   niveau = vecNoeuds[indMin].niveau + 1;

   lst << vecNoeuds[indMin];
   for (chP = lstConnec[indMin]; chP != chP->suivantP; chP = chP->suivantP)
   {
      if (chP->indNoeud == indMax)
      {
         lst << vecNoeuds[indMax];
         return OK;
      }
      if (vecNoeuds[chP->indNoeud].niveau == niveau)
         if (OK == reqCheminMin (lst, chP->indNoeud, indMax)) return OK;
   }
   lst.enleve(lst.dimension()-1);

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return !OK;
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqDegreMax (EntierN& degre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   degre = 0;
   for (EntierN i = 0; i < vecNoeuds.dimension(); i++)
      if (vecNoeuds[i].degre > degre)
         degre = vecNoeuds[i].degre;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqIndices (TPVecteurSimple<EntierN>& index)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   index.vide();

   for (EntierN i = 0; i < vecNoeuds.dimension(); i++)
   {
      index << vecNoeuds[i].indice;
      if (vecNoeuds[i].indice >= vecNoeuds.dimension()) ;
//         cout << "Erreur d'indice:" << i << endl;

   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqLargeurBande (EntierN& largeurMin,
                              EntierN& largeurMax,
                              EntierN& largeurMoy)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN  i, largeur, noeudMin, noeudMax;
   ChainonP tP;

   largeurMin = -1;
   largeurMax = 0;
   largeurMoy = 0;

   for (i = 0; i < vecNoeuds.dimension(); i++)
   {
      noeudMin = vecNoeuds[i].indice;
      noeudMax = noeudMin;
      for (tP = lstConnec[i]; tP != tP->suivantP; tP = tP->suivantP)
      {
         if (vecNoeuds[tP->indNoeud].indice < noeudMin) noeudMin = vecNoeuds[tP->indNoeud].indice;
         if (vecNoeuds[tP->indNoeud].indice > noeudMax) noeudMax = vecNoeuds[tP->indNoeud].indice;
      }
      largeur = noeudMax - noeudMin;
      if (largeur < largeurMin) largeurMin = largeur;
      if (largeur > largeurMax) largeurMax = largeur;
      largeurMoy += largeur;
   }

   largeurMoy /= vecNoeuds.dimension();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqLargeur (EntierN& larg, const EntierN niv)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN i;

   larg = 0;
   for (i = 0; i < vecNoeuds.dimension(); i++)
      if (vecNoeuds[i].niveau == niv) larg++;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqLargeurMax (EntierN& lMax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN i, niveauMax;
   TPVecteurSimple<EntierN> largV(128);

   reqNiveauMax (niveauMax);
   for (i = 0; i <= niveauMax; i++)
      largV << 0;

   for (i = 0; i < vecNoeuds.dimension(); i++)
      largV[vecNoeuds[i].niveau]++;

   lMax = 0;
   for (i = 0; i <= niveauMax; i++)
      if (largV[i] > lMax) lMax = largV[i];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqNiveauMax (EntierN& niveau)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   niveau = 0;
   for (EntierN i = 0; i < vecNoeuds.dimension(); i++)
      if (vecNoeuds[i].niveau > niveau)
         niveau = vecNoeuds[i].niveau;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqNoeudDegreMin (EntierN& ind)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN  i, degre;

   degre = -1;
   for (i = 0; i < vecNoeuds.dimension(); i++)
   {
      if (vecNoeuds[i].degre < degre)
      {
         degre = vecNoeuds[i].degre;
         ind = i;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqNoeudDegreMax (EntierN& ind)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN  i, degre;

   degre = 0;
   for (i = 0; i < vecNoeuds.dimension(); i++)
   {
      if (vecNoeuds[i].degre > degre)
      {
         degre = vecNoeuds[i].degre;
         ind = i;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqNoeudsDegre (TPVecteurSimple<Noeud>& vecMax, const EntierN degre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   vecMax.vide();
   for (EntierN i = 0; i < vecNoeuds.dimension(); i++)
      if (vecNoeuds[i].degre == degre)
         vecMax << vecNoeuds[i];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}


//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqNoeudsNiveau (TPVecteurSimple<Noeud>& vecMax, const EntierN niveau)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   vecMax.vide();
   for (EntierN i = 0; i < vecNoeuds.dimension(); i++)
      if (vecNoeuds[i].niveau == niveau)
         vecMax << vecNoeuds[i];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void GGGraphe::reqNoeudsPeriph (EntierN& noeudMin, EntierN& noeudMax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN i, indMax;
   EntierN niveauMax;
   EntierN noeud, niveau, largeur;
   EntierN largeurMin = -1;
   TPVecteurSimple<Noeud> vecNoeudMax;
   Booleen termine = FAUX;

   // ---  Assigne les niveaux avec le noeud de degré min
   reqNoeudDegreMin(noeudMin);
   asgNiveaux(noeudMin);

   // ---  Boucle pour trouver le noeud de degré max
   while (! termine)
   {
      // cout << "Noeud min: " << noeudMin << endl;
      // ---  Cherche les noeuds de niveau Max
      reqNiveauMax    (niveauMax);
      reqNoeudsNiveau (vecNoeudMax, niveauMax);
      // ---  Trie par degré croissant
      vecNoeudMax.trie(Noeud::compDegre);

      // ---  Boucle sur les noeuds de niveau max
      indMax = (vecNoeudMax.dimension() + 2) / 2;
      if (indMax > vecNoeudMax.dimension()) indMax = vecNoeudMax.dimension();
      for (i = 0; i < indMax; i++)
      {
         // ---  Assigne les niveaux avec le noeud
         noeud = vecNoeudMax[i].indice;
         asgNiveaux(noeud);
         // ---  Cherche le niveau max et la largeur max
         reqNiveauMax (niveau);
         reqLargeurMax(largeur);
         // ---  Test pour contiuer
         if (largeur <= largeurMin)
         {
            if (niveau > niveauMax)
            {
               noeudMin = noeud;
               break;
            }
            else
            {
               largeurMin = largeur;
               noeudMax = noeud;
               // cout << "Noeud max: " << noeudMax << endl;
            }
         }
      }
      if (noeudMin != noeud)
         termine = VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
ERMsg GGGraphe::renumerote (const EntierN noeudMin, const EntierN noeudMax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   EntierN  i, j, ind, noeud, degreMax, priorite, indiceGlobal = 0;
   ChainonP chP, tP;
   TPVecteurSimple<EntierN> queue;

   // ---  Assigne les niveaux du noeud max
   asgNiveaux(noeudMax);

   // ---  Assigne les priorités
   reqDegreMax(degreMax);
   for (i = 0; i < vecNoeuds.dimension(); i++)
   {
      priorite = (degreMax - vecNoeuds[i].degre)*poidDegre + (vecNoeuds[i].niveau-1)*poidDist;
      vecNoeuds[i].priorite = priorite;
      vecNoeuds[i].etat = Noeud::INACTIF;
   }

   // ---  Boucle sur les noeuds
   vecNoeuds[noeudMin].etat = Noeud::ACTIF;
   queue << noeudMin;
   do
   {
      // ---  Cherche le noeud de priorite Max
      priorite = 0;
      for (i = 0; i < queue.dimension(); i++)
      {
         if (vecNoeuds[queue[i]].priorite > priorite)
         {
            noeud = queue[i];
            priorite = vecNoeuds[noeud].priorite;
            ind = i;
         }
      }
      if (queue.dimension() == 1)
         queue.vide();
      else
         queue.enleve(ind);

      // --- Met à jour la queue des priorites
      if (vecNoeuds[noeud].etat == Noeud::ACTIF)
      {
         for (chP = lstConnec[noeud]; chP != chP->suivantP; chP = chP->suivantP)
         {
            i = chP->indNoeud;
            vecNoeuds[i].priorite += poidDegre;
            if (vecNoeuds[i].etat == Noeud::INACTIF)
            {
               vecNoeuds[i].etat = Noeud::ACTIF;
               queue << i;
            }
         }
      }

      // ---  Assigne le numéro de noeud
      vecNoeuds[noeud].indice = indiceGlobal++;
      vecNoeuds[noeud].etat = Noeud::POSTACTIF;

      // --- Met à jour la queue des priorites
      for (chP = lstConnec[noeud]; chP != chP->suivantP; chP = chP->suivantP)
      {
         i = chP->indNoeud;
         if (vecNoeuds[i].etat != Noeud::ACTIF) continue;

         vecNoeuds[i].priorite += poidDegre;
         vecNoeuds[i].etat = Noeud::ACTIF;

         for (tP = lstConnec[i]; tP != tP->suivantP; tP = tP->suivantP)
         {
            j = tP->indNoeud;
            vecNoeuds[j].priorite += poidDegre;
            if (vecNoeuds[j].etat == Noeud::INACTIF)
            {
               vecNoeuds[j].etat = Noeud::ACTIF;
               queue << j;
            }
         }
      }

   }
   while (queue.dimension() > 0);

   if (indiceGlobal != vecNoeuds.dimension())
      msg = ERMsg(ERMsg::ERREUR, "ERR_BLOCAGE_ALGO");

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
ERMsg GGGraphe::renumerote2 (const EntierN noeudMin, const EntierN noeudMax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   EntierN  i, j, ind, noeud, degreMax, indiceGlobal = 0;
   ChainonP chP;
   TPVecteurSimple<EntierN> queue;

   // ---  Assigne les priorités
   reqDegreMax(degreMax);
   for (i = 0; i < vecNoeuds.dimension(); i++)
   {
      vecNoeuds[i].indice  = i;
      vecNoeuds[i].niveau  = 0;
      vecNoeuds[i].priorite= (degreMax - vecNoeuds[i].degre) * poidDegre;
      vecNoeuds[i].etat = Noeud::INACTIF;
   }

   // ---  Boucle sur les noeuds
   vecNoeuds[noeudMin].etat = Noeud::ACTIF;
   queue << noeudMin;
   Entier largeur, largeurMax;
   do
   {
      // ---  Cherche la largeur de bande Max
      largeurMax = -1;
      for (i = 0; i < queue.dimension(); i++)
      {
         largeur  = indiceGlobal - vecNoeuds[queue[i]].niveau;
         largeur += vecNoeuds[queue[i]].priorite;
         for (chP = lstConnec[queue[i]]; chP != chP->suivantP; chP = chP->suivantP)
         {
            j = chP->indNoeud;
            if (vecNoeuds[j].etat != Noeud::INACTIF)
            {
               largeur += indiceGlobal - vecNoeuds[j].niveau;
               largeur += vecNoeuds[j].priorite;
            }
            largeur ++;
         }
         if (largeur > largeurMax)
         {
            largeurMax = largeur;
            ind = i;
         }
      }
      noeud = queue[ind];
      queue.enleve(ind);

      // --- Met à jour les voisins
      for (chP = lstConnec[noeud]; chP != chP->suivantP; chP = chP->suivantP)
      {
         i = chP->indNoeud;
         if (vecNoeuds[i].niveau == 0 || vecNoeuds[i].niveau > indiceGlobal)
            vecNoeuds[i].niveau = indiceGlobal;
         vecNoeuds[i].priorite += poidDegre;
         if (vecNoeuds[i].etat == Noeud::INACTIF)
         {
            vecNoeuds[i].etat = Noeud::ACTIF;
            queue << i;
         }
       }

      // ---  Assigne le numéro de noeud
      vecNoeuds[noeud].indice = indiceGlobal++;
      vecNoeuds[noeud].etat = Noeud::POSTACTIF;

   }
   while (queue.dimension() > 0);

   if (indiceGlobal != vecNoeuds.dimension())
      msg = ERMsg(ERMsg::ERREUR, "ERR_BLOCAGE_ALGO");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
ERMsg GGGraphe::renumerote3 (const EntierN noeudMin, const EntierN noeudMax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   EntierN  i, j, ind, indiceGlobal = 0;
   Noeud pivot;
//   TPVecteurSimple<EntierN> queue;
   TPVecteurSimple<Noeud> cheminMin, noeudsNiveau;

   // ---  Réinitialise les indices
   for (i = 0; i < vecNoeuds.dimension(); i++)
      vecNoeuds[i].indice  = i;

   // ---  Assigne les niveaux à partir du noeud min
   asgNiveaux(noeudMin);

   // ---  Cherche le chemin min
   reqCheminMin (cheminMin, noeudMin, noeudMax);

   // ---  Avance le long du chemin min
   for (i = 0; i < cheminMin.dimension(); i++)
   {
      pivot = cheminMin[i];

      // ---  Largeur de chaque niveau
      noeudsNiveau.vide();
      reqNoeudsNiveau (noeudsNiveau, i);
//      if (noeudsNiveau.dimension() > 0)
//         noeudsNiveau.trie(Noeud::compDegre);

      // ---  Assigne les nouveaux numéros de noeuds
      for (j = 0; j < noeudsNiveau.dimension(); j++)
      {
         ind = noeudsNiveau[j].indice;
         vecNoeuds[ind].indice = indiceGlobal++;
      }
   }

   if (indiceGlobal != vecNoeuds.dimension())
      msg = ERMsg(ERMsg::ERREUR, "ERR_BLOCAGE_ALGO");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
ERMsg GGGraphe::renum(TPVecteurSimple<EntierN>& index)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg;
   EntierN noeudMin, noeudMax;

   // ---  Largeur de bande initiale
//   EntierN lmin, lmax, lmoy;
//   reqLargeurBande(lmin, lmax, lmoy);
   //cout << "Largeur de bande init. : " << lmin << " " << lmax << " " << lmoy << endl;

   // ---  Calcul du diamètre
//   cout << "Calcul du Pseudo-Diametre" << endl;
   reqNoeudsPeriph (noeudMin, noeudMax);
//   cout << "Pseudo-Diametre: " << noeudMin << "   " << noeudMax << endl;

   // ---  Renumérote - 1ere tentative
   msg = renumerote2 (noeudMin, noeudMax);
//   reqLargeurBande(lmin, lmax, lmoy);
//   cout << "Largeur de bande finale: " << lmin << " " << lmax << " " << lmoy << endl;

   // ---  Renumérote - 2eme tentative
   if (!msg)
      msg = renumerote (noeudMin, noeudMax);
//   reqLargeurBande(lmin, lmax, lmoy);
//   cout << "Largeur de bande finale: " << lmin << " " << lmax << " " << lmoy << endl;

   // ---  Renumérote - 3eme tentative
   if (!msg)
      renumerote3 (noeudMin, noeudMax);
//   reqLargeurBande(lmin, lmax, lmoy);
//   cout << "Largeur de bande finale: " << lmin << " " << lmax << " " << lmoy << endl;

   // ---  Retourne les indices
   index.vide();
   if (msg)
   {
      reqIndices (index);
   }
   else
   {
      for (EntierN i = 0; i < vecNoeuds.dimension(); i++)
         index << i;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}





