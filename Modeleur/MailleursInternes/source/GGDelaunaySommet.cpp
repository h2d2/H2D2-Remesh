//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunaySommet.cpp
// Classe:   GGDelaunaySommet
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
// 13-11-98  Yves Secretan    Ajoute regulariseBarycentrique
//*****************************************************************************

#include "GGDelaunaySommet.h"
#include "GGDelaunayArete.h"
#include "GGDelaunaySurface.h"
#include "GGDelaunayInfo.h"

#include "GGDelaunayPolygone.h"
#include "GGContainerAreteARetourner.h"
#include "GGMailleurDelaunay.h"

#include <algorithm>
#include <limits>

Entier GGDelaunaySommet::niveauRaffMin = -2;
Entier GGDelaunaySommet::niveauRaffMax = 2;

//************************************************************************
// Sommaire:  Constructeur avec paramètres.
//
// Description:
//    Le constructeur public <code>GGDelaunaySommet(...)</code> construit
//    un sommet de position p et de critère de raffinement c.
//
// Entrée:
//    GGMailleurDelaunayP       mP   Le mailleur à notifier de la construction
//    const GOCoordonneesXYZ&   p    Position du sommet
//    Booleen                   bou  VRAI si le sommet est bougeable
//    Booleen                   der  VRAI si le sommet est déraffinable
//    Booleen                   peau VRAI si le sommet est sur la peau
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySommet::GGDelaunaySommet (GGMailleurDelaunayP     mP,
                                    const GOCoordonneesXYZ& p,
                                    const GGDelaunaySommetArguments& kwargs)
   : compteRef(1)
   , niveauRaff(kwargs.m_lvl)
   , mailleurP(mP)
   , critere  (0.0)
   , position (p)
   , infoClientP(NULL)
   , bougeable(kwargs.m_bou)
   , deraffinable(kwargs.m_der)
   , surPeau(kwargs.m_peau)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(mP != NUL);
#endif

   const TCContainerArete::size_type NBR_ARETES_INITIAL = 10;
   const TCContainerSurface::size_type NBR_SURFACES_INITIAL = 10;
   aretes.reserve(NBR_ARETES_INITIAL);
   surfaces.reserve(NBR_SURFACES_INITIAL);

   // ---  Demande la métrique au mailleur
   metrique = mailleurP->reqMetrique(position);

   // ---  Enregistre auprès du mailleur
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur avec paramètres.
//
// Description:
//    Le constructeur public <code>GGDelaunaySommet(...)</code> construit
//    un sommet de position p et de critère de raffinement c.
//
// Entrée:
//    GGMailleurDelaunayP       mP   Le mailleur à notifier de la construction
//    const GOCoordonneesXYZ&   p    Position du sommet
//    const GGMetriqueMaillage& c    Critère de raffinement
//    Booleen                   bou  VRAI si le sommet est bougeable
//    Booleen                   der  VRAI si le sommet est déraffinable
//    Booleen                   peau VRAI si le sommet est sur la peau
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySommet::GGDelaunaySommet (GGMailleurDelaunayP       mP,
                                    const GOCoordonneesXYZ&   p,
                                    const GGMetriqueMaillage& m,
                                    const GGDelaunaySommetArguments& kwargs)
   : compteRef(1)
   , niveauRaff(kwargs.m_lvl)
   , mailleurP(mP)
   , critere  (0.0)
   , metrique (m)
   , position (p)
   , infoClientP(NULL)
   , bougeable(kwargs.m_bou)
   , deraffinable(kwargs.m_der)
   , surPeau(kwargs.m_peau)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(mP != NUL);
#endif

   const TCContainerArete::size_type NBR_ARETES_INITIAL = 10;
   const TCContainerSurface::size_type NBR_SURFACES_INITIAL = 10;
   aretes.reserve(NBR_ARETES_INITIAL);
   surfaces.reserve(NBR_SURFACES_INITIAL);

   // ---  Enregistre auprès du maillage
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur de l'objet.
//
// Description:
//    Le destructeur privé <code>~GGDelaunaySommet()</code> enlève toutes
//    les références que le sommet possède.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySommet::~GGDelaunaySommet()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef == 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (mailleurP != NUL)
   {
      mailleurP->retire(this);
      mailleurP = NUL;
   }

   const TCContainerArete::iterator eoaI = aretes.end();
   for (TCContainerArete::iterator aI = aretes.begin(); aI != eoaI; ++aI)
      (*aI)->enleveReference();
   aretes.erase(aretes.begin(), aretes.end());

   const TCContainerSurface::iterator eosI = surfaces.end();
   for (TCContainerSurface::iterator sI = surfaces.begin(); sI != eosI; ++sI)
      (*sI)->enleveReference();
   surfaces.erase(surfaces.begin(), surfaces.end());

   if (infoClientP != NULL) delete infoClientP;
}

//************************************************************************
// Sommaire:  Ajoute un lien avec une arête.
//
// Description:
//    La méthode publique <code>ajouteLienArete(...)</code> ajoute un lien
//    entre le sommet et l'arête passée en argument.
//
// Entrée:
//    GGDelaunayAreteP aP     Arête avec laquelle se lier
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::ajouteLienArete(GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   aP->ajouteReference();
   aretes.push_back(aP);

   mailleurP->retire(this);
   calculeCritere();
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Ajoute un lien avec une surface.
//
// Description:
//    La méthode publique <code>ajouteLienSurface(...)</code> ajoute un lien
//    entre le sommet et la surface passée en argument.
//
// Entrée:
//    GGDelaunaySurfaceP sP     Surface avec laquelle se lier
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::ajouteLienSurface(GGDelaunaySurfaceP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   sP->ajouteReference();
   surfaces.push_back(sP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Demande si une arête peut bouger.
//
// Description:
//    La méthode publique <code>aretePeutBouger()</code> permet de
//    demander si l'arête peut être bougée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GGDelaunaySommet::aretePeutBouger(GGDelaunayAreteP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return VRAI;
}

//************************************************************************
// Sommaire: Notifie qu'une arête a bougé.
//
// Description:
//    La méthode publique <code>areteABouge(..)</code> permet de notifier
//    le sommet lorsqu'une de ses arêtes a bougé.
//
// Entrée:
//    GGDelaunayAreteP           Arête qui a bougé
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::areteABouge(GGDelaunayAreteP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   mailleurP->retire(this);
   calculeCritere();
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Calcule le critère de déraffinement.
//
// Description:
//    La méthode publique <code>calculeCritere()</code> calcule le critère
//    de déraffinement. Celui-ci est basé sur la longueur moyenne des arêtes
//    adhérentes au sommet.
//    <p>
//    La méthode appelante doit gérer l'utilisation du critère comme clef
//    de tri.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::calculeCritere()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

/*
   Le critère du min mène à des résultats de déraffinage aberrants.
   Sur un maillage régulier et suivant l'ordre dans lequel les sommets sont
   éliminés, une des arêtes des surfaces est toujours une petite arête et force
   le déraffinage de proche en proche de tout le maillage. On ne respecte donc
   pas vraiment la métrique.

   critere = std::numeric_limits<double>::max();
   TCContainerArete::const_iterator eoaI = aretes.end();
   for (TCContainerArete::const_iterator aI = aretes.begin(); aI != eoaI; ++aI)
   {
      critere = std::min(critere, (*aI)->reqCritere());
   }
*/
   critere = 0.0;
   if (aretes.size() > 0)
   {
      TCContainerArete::const_iterator aI   = aretes.begin();
      TCContainerArete::const_iterator eoaI = aretes.end();
      while (aI != eoaI) critere += (*aI++)->reqCritere();
      critere /= aretes.size();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Contrôle le compte de référence.
//
// Description:
//    La méthode privée <code>ctrlCompteReference()</code> contrôle
//    les comptes de références de l'objet. Le contrôle n'est valide que
//    dans un état stable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGDelaunaySommet::ctrlCompteReference() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   if (compteRef != (aretes.size()+surfaces.size()))
      msg = ERMsg(ERMsg::ERREUR, "GGDelaunaySommet");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Déraffine le sommet.
//
// Description:
//    La méthode publique <code>deraffine()</code> déraffine le sommet
//    critère de raffinement n'est pas satisfait.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//
// Sortie:
//    Retourne VRAI si le sommet a été déraffiné.
//
// Notes:
//
//************************************************************************
ERMsg GGDelaunaySommet::deraffine(Booleen& change)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aretes.size()   > 1);
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   >= surfaces.size());
   PRECONDITION(compteRef > 2);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Si le critère n'est pas vérifié déraffine
   change = FAUX;
   if (deraffinable && doitEtreDeraffine())
   {
      if (aretes.size() == surfaces.size())
         msg = deraffineInterne();
      else
         msg = deraffineFrontiere();
      if (msg) change = VRAI;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Déraffine un sommet frontière.
//
// Description:
//    La méthode privée <code>deraffineFrontiere()</code> déraffine le sommet
//    situé sur la frontière du domaine.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    En déraffinement, le nouveau niveau est basée sur
//    le min des arêtes et des surfaces
//************************************************************************
ERMsg GGDelaunaySommet::deraffineFrontiere()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aretes.size()   > 1);
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > surfaces.size());
   PRECONDITION(compteRef > 2);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   GGContainerAreteARetourner containerAretes;

   // ---  Si l'on a plus d'un élément
   if (surfaces.size() > 1)
   {
      GGDelaunayPolygone cavite;
      Entier lvl = niveauRaff - 1;

      // ---  Cherche l'arête de début
      GGDelaunayAreteP a0P = NUL;
      TCContainerArete::const_iterator eoaI = aretes.end();
      for (TCContainerArete::const_iterator aI = aretes.begin(); aI != eoaI; ++aI)
      {
         if ((*aI)->reqSurface1() == NUL)
         {
            GGDelaunaySurfaceP sP = (*aI)->reqSurface0();
            GGDelaunaySommetP  pP = (*aI)->reqAutreSommet(this);
            if (GGDelaunaySurface::reqAireEuclide(this, pP, sP->reqAutreSommet(this, pP)) > 0.0)
            {
               a0P = *aI;
               break;
            }
         }
      }
      ASSERTION(a0P != NUL);
      GGDelaunaySommetP p0P = a0P->reqAutreSommet(this);

      // ---  Forme la cavité
      GGDelaunayAreteP   aP = a0P;
      GGDelaunaySommetP  pP = p0P;
      GGDelaunaySurfaceP sP = aP->reqSurface0();
      ASSERTION(GGDelaunaySurface::reqAireEuclide(this, pP, sP->reqAutreSommet(this, pP)) > 0.0);
      cavite.ajouteSommet(pP);
      for (EntierN i = 0; i < (aretes.size()-1); ++i)
      {
         ASSERTION(sP != NUL);
         GGDelaunaySommetP tmpP = sP->reqAutreSommet(pP, this);
         aP = sP->reqArete(tmpP, pP);
         pP = tmpP;

         containerAretes.ajouteSansControle(aP);
         cavite.ajouteArete(aP);
         cavite.ajouteSommet(pP);
         lvl = std::min({lvl, aP->reqNiveauRaff(), sP->reqNiveauRaff()});

         aP = sP->reqArete(pP, this);
         sP = (aP->reqSurface1() == NUL) ? NUL : aP->reqAutreSurface(sP);
      }
      aP = new GGDelaunayArete(pP, p0P, a0P, aP);
      cavite.ajouteArete(aP);

      // ---  Génère le maillage de la cavité
      msg = cavite.genereMaillage(mailleurP, lvl);

      // ---  Enlève la référence crée par le new
      aP->enleveReference();
   }
   else
   {
      GGDelaunaySommetP p0P = aretes[0]->reqAutreSommet(this);
      GGDelaunaySommetP p1P = aretes[1]->reqAutreSommet(this);
      GGDelaunayAreteP  aP  = surfaces[0]->reqArete(p0P, p1P);
      aP->asgRetournable(false);
      aP->asgEstSurPeau (p0P->estSurPeau() && p1P->estSurPeau());
   }

   // ---  Détache les surfaces et les arêtes
   if (msg)
   {
      while (surfaces.size() != 0)
      {
         GGDelaunaySurfaceP srfP = *(surfaces.rbegin());
         srfP->ajouteReference();
         srfP->detache();
         srfP->enleveReference();
      }
   }
   if (msg)
   {
      while (aretes.size() != 0)
      {
         GGDelaunayAreteP artP = *(aretes.rbegin());
         artP->ajouteReference();
         artP->detache();
         artP->enleveReference();
      }
   }

   // ---  Balance les arêtes
   if (msg)
   {
      EntierN cnt = 0;
      while (containerAretes.reqDimension() != 0)
      {
         GGDelaunayAreteP aP = *containerAretes.reqAreteDebut();
         aP->ajouteReference();        // Pour garantir la survie
         aP->retourne(containerAretes);
         aP->enleveReference();
         ++cnt;
         if (cnt == 100)
            break;
      }
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Déraffine un sommet interne.
//
// Description:
//    La méthode privée <code>deraffineInterne()</code> déraffine le sommet
//    situé à l'intérieur du domaine.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    En déraffinement, le nouveau niveau est basée sur
//    le min des arêtes et des surfaces
//************************************************************************
ERMsg GGDelaunaySommet::deraffineInterne()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aretes.size()   > 2);
   PRECONDITION(surfaces.size() > 2);
   PRECONDITION(aretes.size()   == surfaces.size());
   PRECONDITION(compteRef > 2);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   GGContainerAreteARetourner containerAretes;
   GGDelaunayPolygone cavite;
   Entier lvl = niveauRaff - 1;

   // ---  Forme la cavité
   GGDelaunayAreteP   aP = *aretes.begin();
   GGDelaunaySommetP  pP = aP->reqAutreSommet(this);
   GGDelaunaySurfaceP sP = aP->reqSurface0();
   if (GGDelaunaySurface::reqAireEuclide(this, pP, sP->reqAutreSommet(this, pP)) > 0.0)
      sP = aP->reqSurface1();
   for (EntierN i = 0; i < aretes.size(); ++i)
   {
      sP = aP->reqAutreSurface(sP);
      GGDelaunaySommetP tmpP = sP->reqAutreSommet(pP, this);
      aP = sP->reqArete(tmpP, pP);
      containerAretes.ajouteSansControle(aP);
      cavite.ajouteSommet(pP);
      cavite.ajouteArete(aP);

      lvl = std::min({lvl, aP->reqNiveauRaff(), sP->reqNiveauRaff()});

      pP = tmpP;
      aP = sP->reqArete(pP, this);
   }

   // ---  Génère le maillage de la cavité
   msg = cavite.genereMaillage(mailleurP, lvl);

   // ---  Détache les surfaces et les arêtes
   if (msg)
   {
      while (surfaces.size() != 0)
      {
         GGDelaunaySurfaceP srfP = *(surfaces.rbegin());
         srfP->ajouteReference();
         srfP->detache();
         srfP->enleveReference();
      }
   }
   if (msg)
   {
      while (msg && aretes.size() != 0)
      {
         GGDelaunayAreteP artP = *(aretes.rbegin());
         artP->ajouteReference();
         artP->detache();
         artP->enleveReference();
      }
   }

   // ---  Balance les arêtes internes de la cavité
   if (msg)
   {
      msg = cavite.retourneAretes();
   }

   // ---  Balance les arêtes externes
   if (msg)
   {
      EntierN cnt = 0;
      while (containerAretes.reqDimension() != 0)
      {
         GGDelaunayAreteP aP = *containerAretes.reqAreteDebut();
         aP->ajouteReference();        // Pour garantir la survie
         aP->retourne(containerAretes);
         aP->enleveReference();
         ++cnt;
         if (cnt == 100)
            break;
      }
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Enlève un lien avec une arête.
//
// Description:
//    La méthode publique <code>enleveLienArete(...)</code> enlève le lien
//    que le sommet maintient avec l'arête passée en argument.
//
// Entrée:
//    GGDelaunayAreteP aP     Arête avec laquelle se délier
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::enleveLienArete (GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const TCContainerArete::iterator endI = aretes.end();
   for (TCContainerArete::iterator aI = aretes.begin(); aI != endI; ++aI)
   {
      if (*aI == aP)
      {
         *aI = aretes.back();
         aretes.pop_back();
         aP->enleveReference();
         break;
      }
   }

   mailleurP->retire(this);
   calculeCritere();
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Enlève le lien à une surface.
//
// Description:
//    La méthode publique <code>enleveLienSurface(...)</code> enlève le lien
//    que le sommet maintient avec la surface passée en argument.
//
// Entrée:
//    GGDelaunaySurfaceP sP     Surface avec laquelle se délier
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::enleveLienSurface (GGDelaunaySurfaceP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const TCContainerSurface::iterator endI = surfaces.end();
   for (TCContainerSurface::iterator sI = surfaces.begin(); sI != endI; ++sI)
   {
      if (*sI == sP)
      {
         *sI = surfaces.back();
         surfaces.pop_back();
         sP->enleveReference();
         break;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Retourne l'arête à l'indice demandé.
//
// Description:
//    La méthode publique <code>reqArete(...)</code> retourne l'arête à l'indice
//    passé en argument.
//
// Entrée:
//    Entier ind           Indice de l'arête
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayAreteP GGDelaunaySommet::reqArete(EntierN ind) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(ind <  aretes.size());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes[ind];
}

//************************************************************************
// Sommaire:  Régularise le sommet par ses surfaces.
//
// Description:
//    La méthode publique <code>regulariseBarycentrique(...)</code> régularise
//    le sommet par rapport à ses surfaces. Pour chaque surface, on calcule le
//    barycentre euclidien, puis la position finale est définie comme le
//    barycentre de toutes ces positions.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::regulariseBarycentrique()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (!bougeable || surfaces.size() == 0) return;

   // ---  Calcule la nouvelle position
   GOCoordonneesXYZ pc;
   if (estSurPeau())
   {
      ConstGGDelaunayAreteP a1P = NUL;
      ConstGGDelaunayAreteP a2P = NUL;
            TCContainerArete::iterator aI   = aretes.begin();
      const TCContainerArete::iterator eoaI = aretes.end();
      for (; aI != eoaI && a1P == NUL; ++aI)
         if ((*aI)->estSurPeau()) a1P = *aI;
      ASSERTION(aI != eoaI);
      for (; aI != eoaI && a2P == NUL; ++aI)
         if ((*aI)->estSurPeau()) a2P = *aI;
      pc += a1P->reqAutreSommet(this)->reqPosition();
      pc += a2P->reqAutreSommet(this)->reqPosition();
      pc /= 2.0;
   }
   else
   {
            TCContainerSurface::iterator sI   = surfaces.begin();
      const TCContainerSurface::iterator eosI = surfaces.end();
      for (; sI != eosI; ++sI)
         pc += (*sI)->reqBarycentre();
      pc /= static_cast<double>(surfaces.size());
   }

   // ---  Détermine si la nouvelle position est valide
   Booleen peutBouger = VRAI;
   const TCContainerArete::iterator eoaI = aretes.end();
   for (TCContainerArete::iterator aI = aretes.begin(); peutBouger && aI != eoaI; ++aI)
      peutBouger &= (*aI)->sommetPeutBouger(this, pc);
   const TCContainerSurface::iterator eosI = surfaces.end();
   for (TCContainerSurface::iterator sI = surfaces.begin(); peutBouger && sI != eosI; ++sI)
      peutBouger &= (*sI)->sommetPeutBouger(this, pc);

   // ---  Bouge à la nouvelle position
   if (peutBouger)
   {
      position = pc;
      //metrique = mailleurP->reqMetrique(position);
      const TCContainerArete::iterator eoaI = aretes.end();
      for (TCContainerArete::iterator aI = aretes.begin(); aI != eoaI; ++aI)
         (*aI)->sommetABouge(this);
      const TCContainerSurface::iterator eosI = surfaces.end();
      for (TCContainerSurface::iterator sI = surfaces.begin(); sI != eosI; ++sI)
         (*sI)->sommetABouge(this);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Régularise les étoiles, sommets avec trop d'arêtes.
//
// Description:
//    La méthode publique <code>regulariseEtoiles(...)</code> régularise les
//    étoiles, sommet avec trop d'arêtes. Les sommets avec plus de 8 arêtes
//    voient leur arête de critère max raffinée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::regulariseEtoiles()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   static const EntierN MAX_ARETES = 8;
   if (aretes.size() < MAX_ARETES) return;

   DReel cmax = 0.0;
   GGDelaunayAreteP aP = NUL;
   const TCContainerArete::const_iterator eoaI = aretes.end();
   for (TCContainerArete::const_iterator aI = aretes.begin(); aI != eoaI; ++aI)
   {
      if ((*aI)->estRaffinable())
      {
         DReel c = (*aI)->reqCritere();
         if (c > cmax)
         {
            cmax = c;
            aP = (*aI);
         }
      }
   }

   if (aP != NUL)
   {
      aP->ajouteReference();
      aP->regulariseEtoiles(this);
      aP->enleveReference();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Régularise le sommet par ses arêtes.
//
// Description:
//    La méthode publique <code>regulariseParAretes(...)</code> régularise le
//    sommet par rapport à ses arêtes. Pour chaque arête, on calcule la
//    position idéale du sommet, puis la position finale est définie comme le
//    barycentre de toutes ces positions. L'argument permet de gérer la pondération
//    entre l'ancienne et la nouvelle position:
//    <pre>
//       nouvellePosition = moyenne( alfa*positionIdeale + (1.0-alfa)*anciennePosition))
//    </pre>
//
// Entrée:
//    DReel alfa        Coefficient de pondération
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::regulariseParAretes(DReel alfa)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (bougeable)
   {
      const DReel DSEUIL = 0.1;
      const DReel EPS = 1.0e-7;

      EntierN ctr = 0;
      const GOCoordonneesXYZ p_ori = position;
      const GOCoordonneesXYZ dp = p_ori*EPS;
      const GOCoordonneesXYZ p_pdx = position + GOCoordonneesXYZ(dp.x(), 0, 0);
      const GOCoordonneesXYZ p_mdx = position - GOCoordonneesXYZ(dp.x(), 0, 0);
      const GOCoordonneesXYZ p_pdy = position + GOCoordonneesXYZ(0, dp.y(), 0);
      const GOCoordonneesXYZ p_mdy = position - GOCoordonneesXYZ(0, dp.y(), 0);

      DReel dpdx = 0;
      DReel dpdy = 0;
      DReel d2pdx2 = 0;
      DReel d2pdy2 = 0;
      const TCContainerArete::const_iterator eoaI = aretes.end();
      for (TCContainerArete::const_iterator aI = aretes.begin(); aI != eoaI; ++aI)
      {
         DReel l0 = (*aI)->reqCritere();
         l0 *= l0;

         position = p_mdx;
         (*aI)->majLongueurMetrique();
         DReel l_mdx = (*aI)->reqCritere();
         l_mdx *= l_mdx;

         position = p_pdx;
         (*aI)->majLongueurMetrique();
         DReel l_pdx = (*aI)->reqCritere();
         l_pdx *= l_pdx;

         position = p_mdy;
         (*aI)->majLongueurMetrique();
         DReel l_mdy = (*aI)->reqCritere();
         l_mdy *= l_mdy;

         position = p_pdy;
         (*aI)->majLongueurMetrique();
         DReel l_pdy = (*aI)->reqCritere();
         l_pdy *= l_pdy;

         // ---  Reset
         position = p_ori;
         (*aI)->majLongueurMetrique();

         dpdx += (l_pdx - l_mdx) / (dp.x()+dp.x());
         dpdy += (l_pdy - l_mdy) / (dp.y()+dp.y());
         d2pdx2 += (l_pdx - l0-l0 + l_mdx) / (dp.x()*dp.x());
         d2pdy2 += (l_pdy - l0-l0 + l_mdy) / (dp.y()*dp.y());

         ++ctr;
      }
      GOCoordonneesXYZ d(dpdx/d2pdx2, dpdy/d2pdy2, 0);
      GOCoordonneesXYZ p_new = p_ori;
      if (ctr > 0 && d.norme() > DSEUIL)
      {
         bool  accepted = false;
         DReel alfa = 1.0;
         for (int i = 0; i < 4 && ! accepted; ++i)
         {
            p_new = p_ori - alfa*d;
            accepted = true;
            const TCContainerSurface::iterator eosI = surfaces.end();
            for (TCContainerSurface::iterator sI = surfaces.begin(); sI != eosI; ++sI)
            {
               GGDelaunaySommetP s0P = (*sI)->reqSommet0();
               GGDelaunaySommetP s1P = (*sI)->reqSommet1();
               GGDelaunaySommetP s2P = (*sI)->reqSommet2();
               position = p_new;
               accepted = accepted && (GGDelaunaySurface::reqAireEuclide(s0P, s1P, s2P) > 1.0e-6);
               position = p_ori;
            }
            alfa = 0.5*alfa;
         }

         if (accepted)
         {
            Booleen peutBouger = VRAI;

            const TCContainerArete::iterator eoaI = aretes.end();
            for (TCContainerArete::iterator aI = aretes.begin(); aI != eoaI; ++aI)
               peutBouger &= (*aI)->sommetPeutBouger(this, p_new);
            const TCContainerSurface::iterator eosI = surfaces.end();
            for (TCContainerSurface::iterator sI = surfaces.begin(); sI != eosI; ++sI)
               peutBouger &= (*sI)->sommetPeutBouger(this, p_new);

            // ---  Rollback en cas d'erreur
            if (peutBouger)
            {
               position = p_new;
               //metrique = mailleurP->reqMetrique(position);
               const TCContainerArete::iterator eoaI = aretes.end();
               for (TCContainerArete::iterator aI = aretes.begin(); aI != eoaI; ++aI)
                  (*aI)->sommetABouge(this);
               const TCContainerSurface::iterator eosI = surfaces.end();
               for (TCContainerSurface::iterator sI = surfaces.begin(); sI != eosI; ++sI)
                  (*sI)->sommetABouge(this);
            }
         }
         else
         {
            position = p_ori;
         }
      }

//      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Régularise le sommet par ses surfaces.
//
// Description:
//    La méthode publique <code>regulariseParSurfaces(...)</code> régularise
//    le sommet par rapport à ses surfaces. Pour chaque surface, on calcule la
//    position idéale du sommet, puis la position finale est définie comme le
//    barycentre de toutes ces positions. L'argument permet de gérer la
//    pondération entre l'ancienne et la nouvelle position:
//    <pre>
//       nouvellePosition = moyenne( alfa*positionIdeale + (1.0-alfa)*anciennePosition))
//    </pre>
//
// Entrée:
//    DReel alfa        Coefficient de pondération
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySommet::regulariseParSurfaces(DReel alfa)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (bougeable)
   {
      GOCoordonneesXYZ pc;
      for (TCContainerSurface::iterator sI = surfaces.begin(); sI != surfaces.end(); ++sI)
      {
         pc += alfa*(*sI)->reqPosiIdeale(this) + (1.0-alfa)*position;
      }
      const TCContainerSurface::size_type ctr = surfaces.size();
      if (ctr > 0)
      {
         Booleen peutBouger = VRAI;

         pc /= static_cast<double>(ctr);
         const TCContainerArete::iterator eoaI = aretes.end();
         for (TCContainerArete::iterator aI = aretes.begin(); aI != eoaI; ++aI)
            peutBouger &= (*aI)->sommetPeutBouger(this, pc);
         const TCContainerSurface::iterator eosI = surfaces.end();
         for (TCContainerSurface::iterator sI = surfaces.begin(); sI != eosI; ++sI)
            peutBouger &= (*sI)->sommetPeutBouger(this, pc);

         if (peutBouger)
         {
            position = pc;
            //metrique = mailleurP->reqMetrique(position);

            const TCContainerArete::iterator eoaI = aretes.end();
            for (TCContainerArete::iterator aI = aretes.begin(); aI != eoaI; ++aI)
               (*aI)->sommetABouge(this);
            const TCContainerSurface::iterator eosI = surfaces.end();
            for (TCContainerSurface::iterator sI = surfaces.begin(); sI != eosI; ++sI)
               (*sI)->sommetABouge(this);
         }
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}
