//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunayArete.cpp
// Classe:   GGDelaunayArete
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
// 13-11-98  Yves Secretan    Corrige calculeLongueur
//                            Modifie creeSommetMilieu
// 14-05-00  Yves Secretan    Modifie creeSommetMilieu
//*****************************************************************************

#include "GGDelaunayArete.h"
#include "GGDelaunaySommet.h"
#include "GGDelaunaySurface.h"
#include "GGDelaunayInfo.h"

#include "GGContainerAreteARetourner.h"
#include "GGMailleurDelaunay.h"
#include "GGMetriqueMaillage.h"

#include <algorithm>

DReel  GGDelaunayArete::longueurCible = 1.732;    // ~ sqrt(3.0)
Entier GGDelaunayArete::niveauRaffMin = -2;
Entier GGDelaunayArete::niveauRaffMax =  2;
const DReel GGDelaunayArete::TOLERANCE_DEFORMATION = 10.0;

//************************************************************************
// Sommaire:  Construit une arête à partir de deux sommets
//
// Description:
//    Le constructeur public <code>GGDelaunayArete(...)</code> construit une
//    arête à partir des deux sommets passés en arguments. Il ajoute un lien
//    entre ces sommets et l'arête.
//
// Entrée:
//    GGMailleurDelaunayBaseP mP     Le mailleur à notifier de la construction
//    GGDelaunaySommetP       p1P    Les deux sommets de l'arête
//    GGDelaunaySommetP       p2P
//    Booleen                 raf    VRAI si l'arête est raffinable
//    Booleen                 ret    VRAI si l'arête est retournable
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayArete::GGDelaunayArete (GGMailleurDelaunayP mP,
                                  GGDelaunaySommetP p0P,
                                  GGDelaunaySommetP p1P,
                                  const GGDelaunayAreteArguments& kwargs)
   : compteRef(1)
   , niveauRaff(kwargs.m_lvl)
   , mailleurP(mP)
   , longueur(0.0)
   , infoClientP(NULL)
   , raffinable(kwargs.m_raff)
   , retournable(kwargs.m_swap)
   , surPeau(kwargs.m_peau)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(mP  != NUL);
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p0P != p1P);
#endif   // ifdef MODE_DEBUG

   sommets[0] = p0P;
   sommets[1] = p1P;

   surfaces[0] = NUL;
   surfaces[1] = NUL;
   surfaces[2] = NUL;

   majLongueurMetrique();

   p0P->ajouteReference();
   p1P->ajouteReference();

   p0P->ajouteLienArete(this);
   p1P->ajouteLienArete(this);

   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Construit une arête à partir de deux sommets
//
// Description:
//    Le constructeur public <code>GGDelaunayArete(...)</code> construit une
//    arête à partir des deux sommets passés en arguments. Il ajoute un lien
//    entre ces sommets et l'arête.
//
// Entrée:
//    GGMailleurDelaunayBaseP mP     Le mailleur à notifier de la construction
//    GGDelaunaySommetP       p1P    Les deux sommets de l'arête
//    GGDelaunaySommetP       p2P
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayArete::GGDelaunayArete (GGDelaunaySommetP p0P,
                                  GGDelaunaySommetP p1P,
                                  GGDelaunayAreteP  aP,
                                  const GGDelaunayAreteArguments& kwargs)
   : compteRef  (1)
   , niveauRaff (kwargs.m_lvl)
   , mailleurP  (NUL)
   , longueur   (0.0)
   , infoClientP(NULL)
   , raffinable (VRAI)
   , retournable(VRAI)
   , surPeau    (FAUX)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p0P != p1P);
   PRECONDITION(aP  != NUL);
#endif   // ifdef MODE_DEBUG

   mailleurP = aP->mailleurP;

   sommets[0] = p0P;
   sommets[1] = p1P;

   surfaces[0] = NUL;
   surfaces[1] = NUL;
   surfaces[2] = NUL;

   majLongueurMetrique();

   p0P->ajouteReference();
   p1P->ajouteReference();

   p0P->ajouteLienArete(this);
   p1P->ajouteLienArete(this);

   mailleurP->ajoute(this);

   retournable = aP->estRetournable();
   raffinable  = aP->estRaffinable();
   surPeau     = aP->estSurPeau();
   if (aP->infoClientP != NULL) infoClientP = aP->infoClientP->clone();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Construit une arête à partir de deux sommets
//
// Description:
//    Le constructeur public <code>GGDelaunayArete(...)</code> construit une
//    nouvelle arête à partir des deux arêtes passées en arguments. La
//    nouvelle arête relie les 2 sommets non partagés.
//    Il ajoute un lien entre ces sommets et l'arête.
//
// Entrée:
//    GGDelaunayAreteP a0P    Les deux arêtes
//    GGDelaunayAreteP a1P
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayArete::GGDelaunayArete(GGDelaunaySommetP p0P,
                                 GGDelaunaySommetP p1P,
                                 GGDelaunayAreteP  a0P,
                                 GGDelaunayAreteP  a1P,
                                 const GGDelaunayAreteArguments& kwargs)
   : compteRef(1)
   , niveauRaff(kwargs.m_lvl)
   , mailleurP(NULL)
   , longueur(0.0)
   , infoClientP(NULL)
   , raffinable(VRAI)
   , retournable(VRAI)
   , surPeau(FAUX)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p0P != p1P);
   PRECONDITION(a0P != NUL);
   PRECONDITION(a1P != NUL);
   PRECONDITION(a0P != a1P);
#endif   // ifdef MODE_DEBUG

   mailleurP = a0P->mailleurP;

   sommets[0] = p0P;
   sommets[1] = p1P;

   surfaces[0] = NUL;
   surfaces[1] = NUL;
   surfaces[2] = NUL;

   majLongueurMetrique();

   p0P->ajouteReference();
   p1P->ajouteReference();

   p0P->ajouteLienArete(this);
   p1P->ajouteLienArete(this);

   mailleurP->ajoute(this);

   retournable = (a0P->estRetournable() && a1P->estRetournable());
   raffinable  = (a0P->estRaffinable()  && a1P->estRaffinable());
   surPeau     = (a0P->estSurPeau()     || a1P->estSurPeau());
   if (a0P->reqInfoClient() == 0)
   {
      if (a1P->reqInfoClient() == 0)
      {
//         pass
      }
      else
      {
         infoClientP = a1P->reqInfoClient()->clone();
      }
   }
   else
   {
      if (a1P->reqInfoClient() == 0)
      {
//         pass
      }
      else
      {
         infoClientP = a0P->reqInfoClient()->clone();
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur de l'arête.
//
// Description:
//    Le destructeur privé <code>~GGDelaunayArete()</code> enlève toutes
//    les références que possède l'arête.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayArete::~GGDelaunayArete()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef == 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (mailleurP != NUL)
   {
      mailleurP->retire(this);
      mailleurP = NUL;
   }

   if (sommets[0] != NUL)
      sommets[0]->enleveReference();
   if (sommets[1] != NUL)
      sommets[1]->enleveReference();

   if (surfaces[0] != NUL)
      surfaces[0]->enleveReference();
   if (surfaces[1] != NUL)
      surfaces[1]->enleveReference();
   if (surfaces[2] != NUL)
      surfaces[2]->enleveReference();

   if (infoClientP != NULL) delete infoClientP;
}

//************************************************************************
// Sommaire:  Ajoute un lien vers une surface.
//
// Description:
//    La méthode public <code>ajouteLienSurface(...)</code> ajoute un lien
//    entre l'arête et la surface passée en argument.
//
// Entrée:
//    GGDelaunaySurfaceP sP      // la surface
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayArete::ajouteLienSurface(GGDelaunaySurfaceP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   PRECONDITION(surfaces[2] == NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Ajoute uniquement si la surface n'est pas connue
   if (surfaces[0] == NUL)
   {
      surfaces[0] = sP;
      sP->ajouteReference();
   }
   else if (surfaces[0] != sP)
   {
      if (surfaces[1] == NUL)
      {
         surfaces[1] = sP;
         sP->ajouteReference();
      }
      else if (surfaces[1] != sP)
      {
         if (surfaces[2] == NUL)
         {
            surfaces[2] = sP;
            sP->ajouteReference();
         }
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Calcule la longueur de l'arête.
//
// Description:
//    La méthode publique <code>reqLongueurMetrique()</code> calcule la longueur
//    de l'arête dans la métrique des sommets. C'est une approximation de
//    l'intégrale le long de l'arête qui est faite sur deux points de Gauss.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGDelaunayArete::reqLongueurMetrique(const GGMetriqueMaillage& cri0, const GOCoordonneesXYZ& pos0,
                                           const GGMetriqueMaillage& cri1, const GOCoordonneesXYZ& pos1)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   static const DReel x0 = 0.5 * (1.0 - 1.0 / sqrt(3.0));
   static const DReel x1 = 0.5 * (1.0 + 1.0 / sqrt(3.0));

   const GGMetriqueMaillage c0 = cri0 * (1.0 - x0) + cri1 * x0;
   const GGMetriqueMaillage c1 = cri0 * (1.0 - x1) + cri1 * x1;

   const DReel l0 = c0.calculeLongueurDirecte(pos0, pos1);
   const DReel l1 = c1.calculeLongueurDirecte(pos0, pos1);

   const DReel l = 0.5 * (l0 + l1);

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return l;
}

//************************************************************************
// Sommaire:  Calcule la longueur de l'arête.
//
// Description:
//    La méthode publique <code>majLongueurMetrique()</code> calcule la longueur
//    de l'arête dans la métrique des sommets. C'est une approximation de
//    l'intégrale le long de l'arête qui est faite sur deux points de Gauss.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayArete::majLongueurMetrique()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   static const DReel x0 = 0.5 * (1.0 - 1.0/sqrt(3.0));
   static const DReel x1 = 0.5 * (1.0 + 1.0/sqrt(3.0));

   const GGMetriqueMaillage& cri0 = sommets[0]->reqMetrique();
   const GGMetriqueMaillage& cri1 = sommets[1]->reqMetrique();

   const GGMetriqueMaillage c0 = cri0*(1.0-x0) + cri1*x0;
   const GGMetriqueMaillage c1 = cri0*(1.0-x1) + cri1*x1;

   const DReel l0 = c0.calculeLongueurDirecte(sommets[0]->reqPosition(), sommets[1]->reqPosition());
   const DReel l1 = c1.calculeLongueurDirecte(sommets[0]->reqPosition(), sommets[1]->reqPosition());

   longueur = 0.5 * (l0 + l1);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Calcule la position du milieu.
//
// Description:
//    La méthode privée <code>creeSommetMilieu(...)</code> crée le sommet qui
//    est au milieu de l'arête. Celui-ci est créé de telle manière que
//    les longueurs dans les métriques soient identiques.
//
// Entrée:
//    Booleen b         VRAI si le sommet est bougeable
//    Booleen d         VRAI si le sommet est déraffinable
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySommetP GGDelaunayArete::creeSommetMilieu(const GGDelaunaySommetArguments& kwargs) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   static const Entier MAXITER = 5;
   static const DReel  EPS = 0.05;
   static const DReel  X0  = 0.5 * (1.0 - 1.0/sqrt(3.0));
   static const DReel  X1  = 0.5 * (1.0 + 1.0/sqrt(3.0));


   // ---  Ordonne les sommets pour que l'arête soit toujours dans le même sens
   GGDelaunaySommetP s0P = sommets[0];
   GGDelaunaySommetP s1P = sommets[1];
   if (s0P->reqPosition() < s1P->reqPosition())
   {
      s0P = s1P;
      s1P = sommets[0];
   }

   // ---  Récupère les positions et les métriques
   const GOCoordonneesXYZ&   pos0 = s0P->reqPosition();
   const GGMetriqueMaillage& cri0 = s0P->reqMetrique();
   const GOCoordonneesXYZ&   pos1 = s1P->reqPosition();
   const GGMetriqueMaillage& cri1 = s1P->reqMetrique();

   GOCoordonneesXYZ   posM;
   GGMetriqueMaillage criM;
   GGMetriqueMaillage c0;
   GGMetriqueMaillage c1;
   DReel l0;
   DReel l1;
   DReel alfaInf = 0.0;
   DReel alfaSup = 1.0;
   DReel longInf = 0.0;
   DReel longSup = longueur;
   DReel alfa    = 0.5*(alfaInf+alfaSup);

   Entier iter = 0;
   do
   {
      posM = pos0*(1.0-alfa) + pos1*alfa;
      criM = cri0*(1.0-alfa) + cri1*alfa;

      c0 = cri0*(1.0-X0) + criM*X0;
      c1 = cri0*(1.0-X1) + criM*X1;
      l0 = 0.5 * (c0.calculeLongueurDirecte(pos0, posM) +
                  c1.calculeLongueurDirecte(pos0, posM));

      c0 = criM*(1.0-X0) + cri1*X0;
      c1 = criM*(1.0-X1) + cri1*X1;
      l1 = 0.5 * (c0.calculeLongueurDirecte(posM, pos1) +
                  c1.calculeLongueurDirecte(posM, pos1));

      if (l0 < l1)
      {
         alfaInf = alfa;
         longInf = l0;
         longSup = l0 + l1;
      }
      else
      {
         alfaSup = alfa;
         longSup = l0;
      }
      l0 = 0.5 * (l0 + l1);
      alfa = alfaInf + (alfaSup-alfaInf)*(l0-longInf)/(longSup-longInf);

      ASSERTION(alfaSup > alfaInf);
      ASSERTION(longSup > longInf);
      ASSERTION(alfa >= 0.0 && alfa <= 1.0);

      ++iter;
   }
   while (fabs(1.0-l0/l1) > EPS && iter < MAXITER);

   if (fabs(1.0-l0/l1) > EPS) alfa = 0.5 *(alfaInf+alfaSup);
   posM = pos0*(1.0-alfa) + pos1*alfa;
   criM = cri0*(1.0-alfa) + cri1*alfa;

   GGDelaunaySommetP somP = new GGDelaunaySommet(mailleurP, posM, criM, kwargs);
//   GGDelaunaySommetP somP = new GGDelaunaySommet(mailleurP, posM, b, d, p);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return somP;
}

//************************************************************************
// Sommaire:  Contrôle le compte de référence.
//
// Description:
//    La méthode privée <code>ctrlCompteReference()</code> contrôle
//    les comptes de références de l'objet. Le contrôle n'est valide que
//    dans un état stable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGDelaunayArete::ctrlCompteReference() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   Entier n = 0;
   if (sommets[0] != NUL) ++n;
   if (sommets[1] != NUL) ++n;
   if (surfaces[0] != NUL) ++n;
   if (surfaces[1] != NUL) ++n;
   if (surfaces[2] != NUL) ++n;
   if (compteRef != n)
      msg = ERMsg(ERMsg::ERREUR, "GGDelaunayArete");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Détache l'objet des ses sommets.
//
// Description:
//    La méthode publique <code>detache()</code> détache l'arête des sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayArete::detache()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != NUL);
   PRECONDITION(compteRef > 2);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Délie des sommets
   sommets[0]->enleveLienArete(this);
   sommets[1]->enleveLienArete(this);

#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Retourne VRAI si l'arête doit être raffinée.
//
// Description:
//    La méthode publique <code>doitEtreRaffinee()</code> retourne VRAI si
//    l'arête doit être raffinée. Dans la métrique de l'arête, celle-ci
//    doit être raffinée si sa longueur est supérieur à celle d'un coté de
//    triangle équilatéral sur le cercle unitaire.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GGDelaunayArete::doitEtreRaffinee() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(surfaces[2] == NUL);
#endif   // ifdef MODE_DEBUG

   static const DReel TOL = 1.414213562; // sqrt(2.0);
   bool aRafinner =  (niveauRaff < GGDelaunayArete::niveauRaffMax)
                  && (reqCritere() > TOL);

   if (aRafinner && surfaces[0] != NUL)
   {
      DReel elong0 = surfaces[0]->reqCritereElong();
      DReel elong1 = elong0;
      if (surfaces[1] != NUL)
         elong1 = surfaces[1]->reqCritereElong();
      aRafinner = (std::min(elong0, elong1) > 1.0e-06) &&
                  (std::max(elong0, elong1) < 1.0e+06);
   }

   return aRafinner;
}

//************************************************************************
// Sommaire:  Enlève un lien avec une surface.
//
// Description:
//    La méthode publique <code>enleveLienSurface(...)</code> enlève le lien
//    qui existe entre l'arête et la surface passée en argument.
//
// Entrée:
//    GGDelaunaySurfaceP sP         La surface
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayArete::enleveLienSurface(GGDelaunaySurfaceP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Enlève uniquement si la surface est connue
   if (surfaces[0] == sP)
   {
      surfaces[0] = surfaces[1];
      surfaces[1] = surfaces[2];
      surfaces[2] = NUL;
      sP->enleveReference();
   }
   else if (surfaces[1] == sP)
   {
      surfaces[1] = surfaces[2];
      surfaces[2] = NUL;
      sP->enleveReference();
   }
   else if (surfaces[2] == sP)
   {
      surfaces[2] = NUL;
      sP->enleveReference();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Raffine l'arête.
//
// Description:
//    La méthode publique <code>raffine()</code> raffine l'arête si le
//    critère de raffinement n'est pas satisfait.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//
// Sortie:
//    Retourne VRAI si l'arête a été raffinée.
//
// Notes:
//
//************************************************************************
Booleen GGDelaunayArete::raffine()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != 0);
   PRECONDITION(surfaces[2] == 0);
   PRECONDITION(compteRef > 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen change = FAUX;

   // ---  Si le critère n'est pas vérifié raffine
   if (raffinable && doitEtreRaffinee())
   {
      if (surfaces[1] != NUL)
         change = raffineInterne();
      else
         change = raffineFrontiere();
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return change;
}

//************************************************************************
// Sommaire:  Raffine l'arête interne.
//
// Description:
//    La méthode privée <code>raffineFrontiere()</code> raffine une arête
//    frontière, donc une arête qui a une seule surface voisine.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GGDelaunayArete::raffineFrontiere()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != 0);
   PRECONDITION(surfaces[2] == 0);
   PRECONDITION(compteRef > 3);
   PRECONDITION(raffinable);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Cherche les 3 sommets existants
   GGDelaunaySommetP p0P = sommets[0];
   GGDelaunaySommetP p1P = surfaces[0]->reqAutreSommet(sommets[0], sommets[1]);
   GGDelaunaySommetP p2P = sommets[1];
   if (GGDelaunaySurface::reqAireEuclide(p0P, p1P, p2P) <= 0)
   {
      GGDelaunaySommetP tmpP = p0P;
      p0P = p2P;
      p2P = tmpP;
   }

   // ---  Cherche les 2 arêtes existantes
   GGDelaunayAreteP a01P = surfaces[0]->reqArete(p0P, p1P);
   GGDelaunayAreteP a12P = surfaces[0]->reqArete(p1P, p2P);

   // ---  Détache la surface
   GGDelaunaySurfaceP sP = surfaces[0];
   sP->ajouteReference();
   sP->detache();
   sP->enleveReference();

   // ---  Détache soi-même des sommets et des surfaces
   detache();
   ASSERTION(compteRef > 0);

   // ---  Niveau de raffinement et statut
   const Entier  lvl = niveauRaff + 1;
   const Booleen bgl = p0P->estBougeable() || p1P->estBougeable();

   // ---  Crée le nouveau sommet
   const GGDelaunaySommetArguments somArgs = GGDelaunaySommetArguments().niveauRaff(lvl)
                                                                        //.bougeable(retournable)
                                                                        .bougeable(bgl)
                                                                        .deraffinable(raffinable)
                                                                        .surPeau(surPeau);
   GGDelaunaySommetP pcP = creeSommetMilieu(somArgs);

   // ---  Crée les 3 arêtes issues du nouveau sommet
   const GGDelaunayAreteArguments artArgs = GGDelaunayAreteArguments().niveauRaff(lvl);
   GGDelaunayAreteP ac0P = new GGDelaunayArete(pcP, p0P, this, artArgs);
   GGDelaunayAreteP ac1P = new GGDelaunayArete(mailleurP, pcP, p1P, artArgs);
   GGDelaunayAreteP ac2P = new GGDelaunayArete(pcP, p2P, this, artArgs);

   // ---  Crée les 2 sous-surfaces
   const GGDelaunaySurfaceArguments srfArgs = GGDelaunaySurfaceArguments().niveauRaff(lvl);
   GGDelaunaySurfaceP s0P = new GGDelaunaySurface(mailleurP, pcP, p0P, p1P, ac0P, a01P, ac1P, srfArgs);
   GGDelaunaySurfaceP s1P = new GGDelaunaySurface(mailleurP, pcP, p1P, p2P, ac1P, a12P, ac2P, srfArgs);

   // ---  Balance les arêtes
   GGContainerAreteARetourner containerAretes;
   EntierN cnt = 0;
   containerAretes.ajouteSansControle(a01P);
   containerAretes.ajouteSansControle(a12P);
   while (containerAretes.reqDimension() != 0)
   {
      GGDelaunayAreteP aP = *containerAretes.reqAreteDebut();
      aP->ajouteReference();        // Pour garantir la survie
      aP->retourne(containerAretes, pcP);
      aP->enleveReference();
      ++cnt;
      if (cnt == 100)
         break;
   }

   // ---  Enlève les références crées par les new
   pcP->enleveReference();
   ac0P->enleveReference();
   ac1P->enleveReference();
   ac2P->enleveReference();
   s0P->enleveReference();
   s1P->enleveReference();

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return VRAI;
}

//************************************************************************
// Sommaire:  Raffine l'arête interne.
//
// Description:
//    La méthode privée <code>raffineInterne()</code> raffine une arête
//    à l'intérieur du domaine, donc une arête qui a deux surfaces
//    voisines.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GGDelaunayArete::raffineInterne()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[1] != 0);
   PRECONDITION(surfaces[2] == 0);
   PRECONDITION(compteRef > 4);
   PRECONDITION(raffinable);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Cherche les 4 sommets existants
   GGDelaunaySommetP p0P = sommets[0];
   GGDelaunaySommetP p1P = surfaces[0]->reqAutreSommet(sommets[0], sommets[1]);
   GGDelaunaySommetP p2P = sommets[1];
   GGDelaunaySommetP p3P = surfaces[1]->reqAutreSommet(sommets[0], sommets[1]);
   if (GGDelaunaySurface::reqAireEuclide(p0P, p1P, p2P) <= 0)
   {
      GGDelaunaySommetP tmpP = p0P;
      p0P = p2P;
      p2P = tmpP;
   }

   // ---  Cherche les 4 arêtes existantes
   GGDelaunayAreteP a01P = surfaces[0]->reqArete(p0P, p1P);
   GGDelaunayAreteP a12P = surfaces[0]->reqArete(p1P, p2P);
   GGDelaunayAreteP a23P = surfaces[1]->reqArete(p2P, p3P);
   GGDelaunayAreteP a30P = surfaces[1]->reqArete(p3P, p0P);

   // ---  Détache les surfaces
   GGDelaunaySurfaceP sP = surfaces[1];
   sP->ajouteReference();
   sP->detache();
   sP->enleveReference();
   sP = surfaces[0];
   sP->ajouteReference();
   sP->detache();
   sP->enleveReference();

   // ---  Détache soi-même des sommets et des surfaces
   detache();
   ASSERTION(compteRef > 0);

   // ---  Niveau de raffinement
   const Entier lvl = niveauRaff + 1;

   // ---  Crée le nouveau sommet
   const GGDelaunaySommetArguments somArgs =
      GGDelaunaySommetArguments().niveauRaff(lvl)
                                 .bougeable(retournable)
                                 .deraffinable(raffinable)
                                 .surPeau(surPeau);
   GGDelaunaySommetP pcP = creeSommetMilieu(somArgs);

   // ---  Crée les 4 arêtes issues du nouveau sommet
   const GGDelaunayAreteArguments artArgs =
      GGDelaunayAreteArguments().niveauRaff(lvl)
                                .raffinable(retournable)    // Utilisé par ac0P et ac2P
                                .retournable(raffinable);   // mais sans effet sur ac1P et ac3P
   GGDelaunayAreteP ac0P = new GGDelaunayArete(mailleurP, pcP, p0P, artArgs);
   GGDelaunayAreteP ac1P = new GGDelaunayArete(mailleurP, pcP, p1P, artArgs);
   GGDelaunayAreteP ac2P = new GGDelaunayArete(mailleurP, pcP, p2P, artArgs);
   GGDelaunayAreteP ac3P = new GGDelaunayArete(mailleurP, pcP, p3P, artArgs);

   // ---  Crée les 4 sous-surfaces
   const GGDelaunaySurfaceArguments srfArgs = GGDelaunaySurfaceArguments().niveauRaff(lvl);
   GGDelaunaySurfaceP s0P = new GGDelaunaySurface(mailleurP, pcP, p0P, p1P, ac0P, a01P, ac1P, srfArgs);
   GGDelaunaySurfaceP s1P = new GGDelaunaySurface(mailleurP, pcP, p1P, p2P, ac1P, a12P, ac2P, srfArgs);
   GGDelaunaySurfaceP s2P = new GGDelaunaySurface(mailleurP, pcP, p2P, p3P, ac2P, a23P, ac3P, srfArgs);
   GGDelaunaySurfaceP s3P = new GGDelaunaySurface(mailleurP, pcP, p3P, p0P, ac3P, a30P, ac0P, srfArgs);

   // ---  Enlève les références crées par les new
   pcP->enleveReference();
   ac0P->enleveReference();
   ac1P->enleveReference();
   ac2P->enleveReference();
   ac3P->enleveReference();
   s0P->enleveReference();
   s1P->enleveReference();
   s2P->enleveReference();
   s3P->enleveReference();

   // ---  Balance les arêtes
   GGContainerAreteARetourner containerAretes;
   EntierN cnt = 0;
   containerAretes.ajouteSansControle(a01P);
   containerAretes.ajouteSansControle(a12P);
   containerAretes.ajouteSansControle(a23P);
   containerAretes.ajouteSansControle(a30P);
   while (containerAretes.reqDimension() != 0)
   {
      GGDelaunayAreteP aP = *containerAretes.reqAreteDebut();
      aP->ajouteReference();        // Pour garantir la survie
      aP->retourne(containerAretes, pcP);
      aP->enleveReference();
      ++cnt;
      if (cnt == 100)
         break;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return VRAI;
}

//************************************************************************
// Sommaire:  Régularise les étoiles, sommets avec trop d'arêtes.
//
// Description:
//    La méthode publique <code>regulariseEtoiles(...)</code> régularise le
//    sommet par rapport à ses arêtes.
//
// Entrée:
//    DReel alfa        Coefficient de pondération
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GGDelaunayArete::regulariseEtoiles(GGDelaunaySommetP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP == sommets[0] || sP == sommets[1]);
   PRECONDITION(surfaces[2] == 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen change = FAUX;

   // ---  Si le critère n'est pas vérifié raffine
   if (raffinable && (doitEtreRaffinee() || ! doitEtreDeraffinee()))
   {
      if (surfaces[1] != NUL)
         change = raffineInterne();
      else
         change = raffineFrontiere();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return change;
}

//************************************************************************
// Sommaire:  Raffine l'arête.
//
// Description:
//    La méthode publique <code>raffine()</code> raffine l'arête si le
//    critère de raffinement n'est pas satisfait.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//
// Sortie:
//    Retourne VRAI si l'arête a été raffinée.
//
// Notes:
//
//************************************************************************
Booleen GGDelaunayArete::soignePont()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != 0);
   PRECONDITION(surfaces[2] == 0);
   PRECONDITION(compteRef > 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen change = FAUX;

   if (raffinable &&                   // l'arête est raffinable
      !estSurPeau() &&                 // l'arête n'est pas sur la peau
      sommets[0]->estSurPeau() &&      // les sommets sont sur la peau
      sommets[1]->estSurPeau())
   {
      ASSERTION(surfaces[1] != NUL);
      change = raffineInterne();
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return change;
}

//************************************************************************
// Sommaire:  Retourne la longueur de l'arête
//
// Description:
//    La méthode statique publique <code>reqLongueurEuclide(...)</code> retourne
//    la longueur de l'arête délimitée par les deux sommets passés en arguments.
//    Cette longueur est calculée dans la métrique euclidienne.
//
// Entrée:
//    GGDelaunaySommetP p0P      Les deux sommets de l'arête
//    GGDelaunaySommetP p1P
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGDelaunayArete::reqLongueurEuclide(const GOCoordonneesXYZ& p0,
                                          const GOCoordonneesXYZ& p1)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   return dist(p0, p1);
}

//************************************************************************
// Sommaire:  Retourne la longueur de l'arête
//
// Description:
//    La méthode statique publique <code>reqLongueurEuclide(...)</code> retourne
//    la longueur de l'arête délimitée par les deux sommets passés en arguments.
//    Cette longueur est calculée dans la métrique euclidienne.
//
// Entrée:
//    GGDelaunaySommetP p0P      Les deux sommets de l'arête
//    GGDelaunaySommetP p1P
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGDelaunayArete::reqLongueurEuclide(ConstGGDelaunaySommetP p0P,
                                          ConstGGDelaunaySommetP p1P)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p0P != p1P);
#endif   // ifdef MODE_DEBUG

   return reqLongueurEuclide(p0P->reqPosition(), p1P->reqPosition());
}

//************************************************************************
// Sommaire:  Retourne la position idéale du sommet.
//
// Description:
//    La méthode publique <code>reqPosiIdeale(...)</code> retourne la position
//    idéale du sommet passé en argument par rapport à la métrique de l'arête
//    et la position du sommet opposé.
//
// Entrée:
//    GGDelaunaySommetP p0P         Le sommet à évaluer
//
// Sortie:
//    GOCoordonneesXYZ              La position idéale
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ GGDelaunayArete::reqPosiIdeale(ConstGGDelaunaySommetP p0P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p0P == sommets[0] || p0P == sommets[1]);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Cherche l'autre sommet
   ConstGGDelaunaySommetP p1P;
   if (p0P == sommets[0])
      p1P = sommets[1];
   else
      p1P = sommets[0];

   // ---  Calcule la nouvelle position
   const GOCoordonneesXYZ& p0 = p0P->reqPosition();
   const GOCoordonneesXYZ& p1 = p1P->reqPosition();
   GOCoordonneesXYZ pn = p1 + (p0 - p1) / reqCritere();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return pn;
}

//************************************************************************
// Sommaire:  Retourne l'arête.
//
// Description:
//    La méthode publique <code>retourne(...)</code> retourne l'arête en se basant
//    sur les deux surfaces auxquelles elle est liée. Ce retournement
//    n'a lieu que si l'arête n'est pas imposée. Si l'arête est retournée et
//    que le sommet pivot est non NUL, la méthode ajoute dans le setAretes les
//    arêtes opposées au sommet pivot. Ceci permet de propager le retournement
//    par rapport à un sommet.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//    GGContainerAreteARetourner* containerAretesP    Container des arêtes à retourner
//    GGDelaunaySommetP           pivP                Sommet pivot
//
// Sortie:
//
// Notes:
//    Les arêtes de frontière ne sont pas retournables. On a donc seulement le
//    cas ou 2 arêtes internes sont concernées. Dans le cas ou l'ancienne
//    porte un info, celle-ci est transmise à la nouvelle arête.
//
//    En retournement, le nouveau niveau est le max des arêtes et surfaces.
//************************************************************************
Booleen GGDelaunayArete::retourne(GGContainerAreteARetourner* containerAretesP,
                                  GGDelaunaySommetP pivP,
                                  DReel swapTol)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 2);
   PRECONDITION(surfaces[0] != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen change = FAUX;

   // ---  Enlève du set d'arêtes
   if (containerAretesP != NUL)
      containerAretesP->retire(this);

   // ---  Élimine directement les arêtes de la peau et les arêtes imposées
   if (surfaces[1] == NUL || !retournable)
      return change;

   // ---  Cherche les 4 sommets
   ASSERTION(surfaces[1] != NUL);
   ASSERTION(surfaces[2] == NUL);
   GGDelaunaySommetP p0P = sommets[0];
   GGDelaunaySommetP p1P = surfaces[0]->reqAutreSommet(sommets[0], sommets[1]);
   GGDelaunaySommetP p2P = sommets[1];
   GGDelaunaySommetP p3P = surfaces[1]->reqAutreSommet(sommets[0], sommets[1]);

   // ---  Contrôle les "ponts"
   if (p1P->estSurPeau() && p3P->estSurPeau())
      return change;

   // ---  Force l'aire positive
   if (GGDelaunaySurface::reqAireEuclide(p0P, p1P, p2P) <= 0)
   {
      GGDelaunaySommetP tmpP = p0P;
      p0P = p2P;
      p2P = tmpP;
   }

   // ---  Contrôle la nouvelle configuration
   if (GGDelaunaySurface::reqAireEuclide(p0P, p1P, p3P) <= 0 ||
       GGDelaunaySurface::reqAireEuclide(p2P, p3P, p1P) <= 0)
       return change;

   // ---  Cherche les 4 arêtes externes
   GGDelaunayAreteP a0P = surfaces[0]->reqArete(p0P, p1P);
   GGDelaunayAreteP a1P = surfaces[0]->reqArete(p1P, p2P);
   GGDelaunayAreteP a2P = surfaces[1]->reqArete(p2P, p3P);
   GGDelaunayAreteP a3P = surfaces[1]->reqArete(p3P, p0P);

   // --- Le niveau
   Entier lvl = std::max({niveauRaff + 1,
                          a0P->niveauRaff, a1P->niveauRaff, a2P->niveauRaff, a3P->niveauRaff,
                          surfaces[0]->reqNiveauRaff(), surfaces[1]->reqNiveauRaff()});

   // ---  Crée l'autre diagonale
   const GGDelaunayAreteArguments artArgs = GGDelaunayAreteArguments().niveauRaff(lvl);
   GGDelaunayAreteP aP = new GGDelaunayArete(mailleurP, p1P, p3P, artArgs);

   // ---  Calcule les critères de chaque configuration
   const DReel cf1 = std::min(surfaces[0]->reqCritereElong(),
                              surfaces[1]->reqCritereElong());
   const DReel cf2 = std::min(GGDelaunaySurface::reqCritereElong(a0P, aP, a3P),
                              GGDelaunaySurface::reqCritereElong(a2P, aP, a1P));
   Booleen doSwap = (cf2/cf1) > swapTol;

   // ---  Balance si nécessaire
   if (doSwap)
   {
      // ---  Transfert l'info
      if (reqInfoClient()) aP->asgInfoClient( reqInfoClient() ); // c.f. note

      // ---  Détache soi-même des sommets
      detache();

      // ---  Crée les nouvelles surfaces
      const GGDelaunaySurfaceArguments srfArgs = GGDelaunaySurfaceArguments().niveauRaff(lvl);
      GGDelaunaySurfaceP s0P = new GGDelaunaySurface(mailleurP, p0P, p1P, p3P, a0P, aP, a3P, srfArgs);
      GGDelaunaySurfaceP s1P = new GGDelaunaySurface(mailleurP, p2P, p3P, p1P, a2P, aP, a1P, srfArgs);

      // ---  Transfert l'info
      if (surfaces[0]->reqInfoClient()) s0P->asgInfoClient(surfaces[0]->reqInfoClient());
      if (surfaces[1]->reqInfoClient()) s1P->asgInfoClient(surfaces[1]->reqInfoClient());

      // ---  Détache les anciennes surfaces
      GGDelaunaySurfaceP surfP = surfaces[0];
      surfP->ajouteReference();        // Pour garantir la survie
      surfP->detache();
      surfP->enleveReference();
      ASSERTION(surfaces[1] == NUL);
      ASSERTION(surfaces[2] == NUL);
      surfP = surfaces[0];
      surfP->ajouteReference();
      surfP->detache();
      surfP->enleveReference();

      // ---  Propage le balancement
      if (containerAretesP != NUL)
      {
         if (pivP == NUL)
         {
            if (a2P->surfaces[1] != NUL && a2P->estRetournable())
               containerAretesP->ajoute(a2P);
            if (a3P->surfaces[1] != NUL && a3P->estRetournable())
               containerAretesP->ajoute(a3P);
            if (a0P->surfaces[1] != NUL && a0P->estRetournable())
               containerAretesP->ajoute(a0P);
            if (a1P->surfaces[1] != NUL && a1P->estRetournable())
               containerAretesP->ajoute(a1P);
         }
         else if (pivP == p1P)
         {
            if (a2P->surfaces[1] != NUL && a2P->estRetournable())
               containerAretesP->ajoute(a2P);
            if (a3P->surfaces[1] != NUL && a3P->estRetournable())
               containerAretesP->ajoute(a3P);
         }
         else if (pivP == p3P)
         {
            if (a0P->surfaces[1] != NUL && a0P->estRetournable())
               containerAretesP->ajoute(a0P);
            if (a1P->surfaces[1] != NUL && a1P->estRetournable())
               containerAretesP->ajoute(a1P);
         }
      }

      // ---  Enlève les références crées par les new des surfaces
      s0P->enleveReference();
      s1P->enleveReference();

      change = VRAI;
   }

   // ---  Enlève les références crées par les new de l'arête
   if (! change)
      aP->detache();
   aP->enleveReference();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return change;
}

//************************************************************************
// Sommaire: Demande si un sommet peut bouger.
//
// Description:
//    La méthode publique <code>sommetPeutBouger()</code> permet de
//    demander si le sommet peut être bougé.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GGDelaunayArete::sommetPeutBouger(GGDelaunaySommetP sP,
                                          const GOCoordonneesXYZ& p) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   PRECONDITION(sP == sommets[0] || sP == sommets[1]);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const GGMetriqueMaillage& c0 = sommets[0]->reqMetrique();
   const GGMetriqueMaillage& c1 = sommets[1]->reqMetrique();
   const DReel lactu  = longueur;
   const DReel lcible = GGDelaunayArete::longueurCible;
   DReel   l;
   Booleen canMove;

   if (sP == sommets[0])
      l = GGDelaunayArete::reqLongueurMetrique(c0, sommets[0]->reqPosition(), c1, p);
   else
      l = GGDelaunayArete::reqLongueurMetrique(c0, p, c1, sommets[1]->reqPosition());

   canMove = (l < lactu && (lcible/l) < TOLERANCE_DEFORMATION) ||
             (l > lactu && (l/lcible) < TOLERANCE_DEFORMATION);

//   if (sP == sommets[0])
//      canMove = (reqLongueurEuclide(p, sommets[1]->reqPosition()) > 1.0e-6);
//   else
//      canMove = (reqLongueurEuclide(sommets[0]->reqPosition(), p) > 1.0e-6);
   return canMove;
}

//************************************************************************
// Sommaire: Notifie qu'un sommet a bougé.
//
// Description:
//    La méthode publique <code>sommetABouge(...)</code> permet de notifier
//    l'arête lorsqu'un de ses sommets a bougé.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayArete::sommetABouge(GGDelaunaySommetP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   mailleurP->retire(this);
   majLongueurMetrique();
   sommets[0]->areteABouge(this);
   sommets[1]->areteABouge(this);
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}
