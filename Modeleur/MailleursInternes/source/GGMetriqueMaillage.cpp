//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGMetriqueMaillage.cpp
// Classe:   GGMetriqueMaillage
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************

#include "GGMetriqueMaillage.h"

#include "GOEllipseErreur.h"
#include "GOCoord3.h"

#include <math.h>

//************************************************************************
// Sommaire:  Constructeur.
//
// Description:
//    Le constructeur public <code>GGMetriqueMaillage(...)</code> construit
//    un objet à partir des arguments.
//
// Entrée:
//   DReel g              // Paramètres de l'ellipse
//   DReel p
//   DReel a
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGMetriqueMaillage::GGMetriqueMaillage (DReel g, DReel p, DReel a)
   : GOEllipseErreur(g, p, a)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur.
//
// Description:
//    Le constructeur public <code>GGMetriqueMaillage(...)</code> construit
//    un objet à partir d'une ellipse d'erreur.
//
// Entrée:
//   const GOEllipseErreur& e             // Ellipse
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGMetriqueMaillage::GGMetriqueMaillage (const GOEllipseErreur& e)
   : GOEllipseErreur(e)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur.
//
// Description:
//    Le destructeur public <code>~GGMetriqueMaillage()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGMetriqueMaillage::~GGMetriqueMaillage()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire:  Retourne la longueur entre deux points.
//
// Description:
//    La méthode publique <code>calculeLongueurDirecte(...)</code> calcule la
//    longueur entre deux points. Les deux points sont dans le domaine réel
//    et la longueur est dans la métrique de l'objet donc dans le domaine
//    de référence.
//
// Entrée:
//    const GOCoordonneesXYZ& x0          // Les deux points
//    const GOCoordonneesXYZ& x1
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGMetriqueMaillage::calculeLongueurDirecte(const GOCoordonneesXYZ& x0,
                                                 const GOCoordonneesXYZ& x1) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel g = reqGrandAxe();
   const DReel p = reqPetitAxe();
   const DReel a = reqInclinaison();
   
   const GOCoordonneesXYZ del = x1 - x0;

   const DReel cosAlfa = cos(a);
   const DReel sinAlfa = sin(a);

   const DReel rot0_x =  del.x()*cosAlfa + del.y()*sinAlfa;
   const DReel rot0_y = -del.x()*sinAlfa + del.y()*cosAlfa;
   const DReel lamb_x = rot0_x / (g*g);
   const DReel lamb_y = rot0_y / (p*p);
   const DReel rot1_x = lamb_x*cosAlfa - lamb_y*sinAlfa;
   const DReel rot1_y = lamb_x*sinAlfa + lamb_y*cosAlfa;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return sqrt(del.x()*rot1_x + del.y()*rot1_y);
}

//************************************************************************
// Sommaire:  Retourne la longueur entre deux points.
//
// Description:
//    La méthode publique <code>calculeLongueurInverse(...)</code> calcule la
//    longueur entre deux points. Les deux points sont dans le domaine de
//    référence (métrique de l'objet) et la longueur est dans le domaine réel.
//
// Entrée:
//    const GOCoordonneesXYZ& x0    // Les deux points
//    const GOCoordonneesXYZ& x1
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGMetriqueMaillage::calculeLongueurInverse(const GOCoordonneesXYZ& x0,
                                                 const GOCoordonneesXYZ& x1) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel g = reqGrandAxe();
   const DReel p = reqPetitAxe();
   const DReel a = reqInclinaison();

   const GOCoordonneesXYZ del = x1 - x0;

   const DReel cosAlfa = cos(a);
   const DReel sinAlfa = sin(a);

   const DReel rot0_x =  del.x()*cosAlfa - del.y()*sinAlfa;
   const DReel rot0_y =  del.x()*sinAlfa + del.y()*cosAlfa;
   const DReel lamb_x = rot0_x * (g*g);
   const DReel lamb_y = rot0_y * (p*p);
   const DReel rot1_x =   lamb_x*cosAlfa + lamb_y*sinAlfa;
   const DReel rot1_y = - lamb_x*sinAlfa + lamb_y*cosAlfa;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return sqrt(del.x()*rot1_x + del.y()*rot1_y);
}

//************************************************************************
// Sommaire:  Transformation directe du point.
//
// Description:
//    La méthode publique <code>transformeDirecte()</code> transforme le point
//    du domaine utilisateur au domaine de référence de la métrique.
//
// Entrée:
//    const GOCoordonneesXYZ& x0    // Le point
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ GGMetriqueMaillage::transformeDirecte(const GOCoordonneesXYZ& x0) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel g = reqGrandAxe();
   const DReel p = reqPetitAxe();
   const DReel a = reqInclinaison();

   const DReel cosAlfa = cos(a);
   const DReel sinAlfa = sin(a);
   const DReel x = ( x0.x()*cosAlfa + x0.y()*sinAlfa) / g;
   const DReel y = (-x0.x()*sinAlfa + x0.y()*cosAlfa) / p;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return GOCoordonneesXYZ(x, y, x0.z());
}

//************************************************************************
// Sommaire:  Transformation inverse du point.
//
// Description:
//    La méthode publique <code>transformeDirecte(...)</code> transforme le point
//    du domaine de référence de la métrique au domaine utilisateur.
//
// Entrée:
//    const GOCoordonneesXYZ& x0    // Le point
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ GGMetriqueMaillage::transformeInverse(const GOCoordonneesXYZ& x0) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel g = reqGrandAxe();
   const DReel p = reqPetitAxe();
   const DReel a = reqInclinaison();

   const DReel cosAlfa = cos(a);
   const DReel sinAlfa = sin(a);

   const DReel lmb_x = x0.x() * g;
   const DReel lmb_y = x0.y() * p;
   const DReel rot_x = lmb_x*cosAlfa - lmb_y*sinAlfa;
   const DReel rot_y = lmb_x*sinAlfa + lmb_y*cosAlfa;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return GOCoordonneesXYZ(rot_x, rot_y, x0.z());
}
