//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunayScenario.cpp
// Classe:   GGDelaunayScenario
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
// 18-05-99  Yves Secretan    Supporte les iostream du Standard
//*****************************************************************************

#include "GGDelaunayScenario.h"
#include "GGDelaunayOperation.h"

#include "clchaine.h"

#include <iostream>

//************************************************************************
// Sommaire:  Constructeur par défaut.
//
// Description:
//    Le constructeur public <code>GGDelaunayScenario()</code> initialise
//    l'objet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayScenario::GGDelaunayScenario ()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur de l'objet.
//
// Description:
//    Le destructeur public <code>~GGDelaunayScenario()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayScenario::~GGDelaunayScenario()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire:  Ajoute un scénario.
//
// Description:
//    La méthode publique <code>ajouteOperation(...)</code> ajoute les
//    opérations du scénario passé en argument à l'objet.
//
// Entrée:
//    const GGDelaunayScenario& scenario         Scénario à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayScenario::ajouteOperation(const GGDelaunayScenario& scenario,
                                         EntierN nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (nbr == 1)
      operations.insert(operations.end(), scenario.operations.begin(), scenario.operations.end());
   else
      for (EntierN i = 0; i < nbr; ++i)
         operations.insert(operations.end(), scenario.operations.begin(), scenario.operations.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Ajoute une opération.
//
// Description:
//    La méthode publique <code>ajouteOperation(...)</code> ajoute une
//    opération au scénario.
//
// Entrée:
//    Operation oper          Opération à ajouter
//    EntierN   nbr           Nombre d'itérations
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayScenario::ajouteOperation(const GGDelaunayOperation& oper, EntierN nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(nbr > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (nbr == 1)
      operations.push_back(oper);
   else
      for (EntierN i = 0; i < nbr; ++i)
         operations.push_back(oper);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de début des opérations.
//
// Description:
//    La méthode publique <code>reqOperationDebut()</code> retourne l'itérateur
//    de début des opérations du scenario.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayScenario::TCContainerOperations::const_iterator
GGDelaunayScenario::reqOperationDebut() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return operations.begin();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de fin des opérations.
//
// Description:
//    La méthode publique <code>reqOperationFin()</code> retourne l'itérateur
//    de fin des opérations du scenario.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayScenario::TCContainerOperations::const_iterator
GGDelaunayScenario::reqOperationFin() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return operations.end();
}

//************************************************************************
// Sommaire: Opérateur >> d'extraction d'un stream.
//
// Description:
//    L'opérateur public <code>operator >>(...)</code> lit un objet
//    <code>GGDelaunayScenario</code> de <code>stream</code> passé en
//    argument.
//
// Entrée:
//    istream& is                Stream de lecture
//
// Sortie:
//    GGDelaunayScenario& s      Objet à lire
//
// Notes:
//
//************************************************************************
INRS_IOS_STD::istream&
operator >> (INRS_IOS_STD::istream& is, GGDelaunayScenario& s)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   CLChaine ch = " ";
   while (! is.eof() && ch != "")
   {
      is >> ch;
      ch.enleveEspaces();
      ch.majuscule();
      if (ch == "MAILLE_PEAU")
         s.ajouteOperation(GGDelaunayOperation::MAILLE_PEAU);
      else if (ch == "LISSE_METRIQUES")
         s.ajouteOperation(GGDelaunayOperation::LISSE_METRIQUES);
      else if (ch == "RESCALE_METRIQUES")
         s.ajouteOperation(GGDelaunayOperation::RESCALE_METRIQUES);
      else if (ch == "RETOURNE_ARETES")
         s.ajouteOperation(GGDelaunayOperation::RETOURNE_ARETES);
      else if (ch == "SOIGNE_PEAU")
         s.ajouteOperation(GGDelaunayOperation::SOIGNE_PEAU);
      else if (ch == "SOIGNE_PONT")
         s.ajouteOperation(GGDelaunayOperation::SOIGNE_PONT);
      else if (ch == "DERAFFINE_PAR_SOMMETS")
         s.ajouteOperation(GGDelaunayOperation::DERAFFINE_PAR_SOMMETS);
//      else if (ch == "DERAFFINE_PAR_SURFACES")
//         s.ajouteOperation(GGDelaunayScenario::DERAFFINE_PAR_SURFACES);
      else if (ch == "RAFFINE_PAR_ARETES")
         s.ajouteOperation(GGDelaunayOperation::RAFFINE_PAR_ARETES);
      else if (ch == "RAFFINE_PAR_SURFACES")
         s.ajouteOperation(GGDelaunayOperation::RAFFINE_PAR_SURFACES);
      else if (ch == "REGULARISE_PAR_ARETES")
         s.ajouteOperation(GGDelaunayOperation::REGULARISE_PAR_ARETES);
      else if (ch == "REGULARISE_PAR_SURFACES")
         s.ajouteOperation(GGDelaunayOperation::REGULARISE_PAR_SURFACES);
      else if (ch == "REGULARISE_BARYCENTRIQUE")
         s.ajouteOperation(GGDelaunayOperation::REGULARISE_BARYCENTRIQUE);
      else if (ch == "REGULARISE_ETOILES")
         s.ajouteOperation(GGDelaunayOperation::REGULARISE_ETOILES);
   }

   return is;
}

