//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGMailleurDelaunay.cpp
// Classe:   GGMailleurDelaunay
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
// 13-11-98  Yves Secretan    Ajoute regulariseBarycentrique
// 08-01-99  Yves Secretan    Tolère les domaines triangulaires
//*****************************************************************************

#include "GGMailleurDelaunay.h"

#include "GGMetriqueMaillage.h"
#include "GGDelaunayAvancement.h"
#include "GGDelaunaySommet.h"
#include "GGDelaunayArete.h"
#include "GGDelaunaySurface.h"
#include "GGDelaunayPolygone.h"
#include "GGDelaunayOperation.h"
#include "GGDelaunayScenario.h"
#include "GGContainerAreteARetourner.h"

#include "ALLisseChampEF.h"
#include "SRChamp.h"
#include "SRChampEFEllipseErreur.h"
#include "GOCoord3.h"

#include <limits>
#include <iomanip>

/* 0 debut-fin globlal
   1 fin de chaque item du scenario
   2 fin de chaque iter d'algo */
static const Entier CTRL_COMPTEREF_LVL = 1;

// La fonction std::advance ne retourne pas un iterator
template <typename TTIter>
TTIter avance(TTIter it, size_t n)
{
   std::advance(it, n);
   return it;
};


//************************************************************************
// Sommaire:  Constructeur par défaut.
//
// Description:
//    Le constructeur publique <code>GGMailleurDelaunay()</code> est le
//    constructeur par défaut de la classe.
//
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGMailleurDelaunay::GGMailleurDelaunay()
   : carteMetriquesP(NUL)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur.
//
// Description:
//    Le destructeur publique <code>~GGMailleurDelaunay()</code> vide
//    les listes maintenues par l'objet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGMailleurDelaunay::~GGMailleurDelaunay()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   vide();
}

//************************************************************************
// Sommaire:  Ajoute une arête.
//
// Description:
//    La méthode publique <code>ajouteArete(...)</code> ajoute une arête
//    à la liste des arêtes.
//
// Entrée:
//    GGDelaunaySommetP s1P         Les deux sommets
//    GGDelaunaySommetP s2P
//    Booleen           raf         VRAI si l'arête est raffinable
//    Booleen           ret         VRAI si l'arête est retournable
//    Booleen           peau        VRAI si l'arête est sur la peau
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayAreteP GGMailleurDelaunay::ajouteArete(GGDelaunaySommetP s1P,
                                                 GGDelaunaySommetP s2P,
                                                 Booleen raf,
                                                 Booleen ret,
                                                 Booleen peau)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const GGDelaunayAreteArguments artArgs =
      GGDelaunayAreteArguments()
         .raffinable(raf)
         .retournable(ret)
         .surPeau(peau);
   GGDelaunayAreteP artP = new GGDelaunayArete(this, s1P, s2P, artArgs);
   artP->enleveReference();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return artP;
}

//************************************************************************
// Sommaire:  Ajoute un sommet.
//
// Description:
//    La méthode publique <code>ajouteSommet(...)</code> ajoute un sommet à la
//    liste des sommets maintenue par le mailleur. Le mailleur maintient une
//    référence sur le sommet afin de garantir son existante. Ces références
//    doivent être enlevées par une appel à <code>enleveReferencesAjouteSommet()</code>
//    une fois tous les sommets, arêtes et surface ajoutées.
//
// Entrée:
//    const GOCoordonneesXYZ&   p        Position du sommet
//    Booleen                   bou      VRAI si le sommet est bougeable
//    Booleen                   der      VRAI si le sommet est deraffinable
//    Booleen                   peau     VRAI si le sommet est sur la peau
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySommetP GGMailleurDelaunay::ajouteSommet(const GOCoordonneesXYZ& p,
                                                   Booleen bou,
                                                   Booleen der,
                                                   Booleen peau)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const GGDelaunaySommetArguments somArgs =
      GGDelaunaySommetArguments()
      .bougeable(bou)
      .deraffinable(der)
      .surPeau(peau);
   GGDelaunaySommetP somP = new GGDelaunaySommet(this, p, somArgs);
   sommetsAssignes.push_back(somP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return somP;
}

//************************************************************************
// Sommaire:  Ajoute un sommet de peau.
//
// Description:
//    La méthode publique <code>ajouteSommetPeau(...)</code> ajoute un sommet
//    à la liste des sommets maintenue par le mailleur. Ces sommets forment
//    la peau du domaine à mailler, ils doivent alors être ajoutés dans l'ordre
//    de parcours du polygone de peau.
//
// Entrée:
//    const GOCoordonneesXYZ&   p        Position du sommet
//    Booleen                   bou      VRAI si le sommet est bougeable
//    Booleen                   der      VRAI si le sommet est deraffinable
//    Booleen                   peau     VRAI si le sommet est sur la peau
//
// Sortie:
//
// Notes:
//    La référence crée par le new est maintenue par sommetsAssignes.
//
//************************************************************************
GGDelaunaySommetP GGMailleurDelaunay::ajouteSommetPeau(const GOCoordonneesXYZ& p,
                                                       Booleen bou,
                                                       Booleen der,
                                                       Booleen peau)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GGDelaunaySommetP somP = ajouteSommet(p, bou, der, peau);
   sommetsAssignes.push_back(somP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return somP;
}

//************************************************************************
// Sommaire:  Ajoute une surface.
//
// Description:
//    La méthode publique <code>ajouteSurface(...)</code> ajoute une surface
//    à la liste des surface.
//
// Entrée:
//    GGDelaunaySommetP   p0P      // Les trois sommets
//    GGDelaunaySommetP   p1P
//    GGDelaunaySommetP   p2P
//    GGDelaunayAreteP    a0P      // Les trois arêtes
//    GGDelaunayAreteP    a1P
//    GGDelaunayAreteP    a2P
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySurfaceP GGMailleurDelaunay::ajouteSurface(GGDelaunaySommetP p0P,
                                                     GGDelaunaySommetP p1P,
                                                     GGDelaunaySommetP p2P,
                                                     GGDelaunayAreteP  a0P,
                                                     GGDelaunayAreteP  a1P,
                                                     GGDelaunayAreteP  a2P)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GGDelaunaySurfaceP srfP = new GGDelaunaySurface(this, p0P, p1P, p2P, a0P, a1P, a2P);
   srfP->enleveReference();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return srfP;
}

//************************************************************************
// Sommaire:  Applique le scénario.
//
// Description:
//    La méthode publique <code>appliqueScenario(...)</code> applique tour à
//    tour les opérations contenues dans le scénario.
//
// Entrée:
//    GGDelaunayAvancement& info             // Info pour le suivi
//    const GGDelaunayScenario& scenario     // Scénario à appliquer
//    Entier nntCible                        // Nombre de noeuds target
//    DReel  htgt                            // Taille de maille cible dans la métrique
//    Entier rMin                            // Niveau relatif min de déraffinement
//    Entier rMax                            // Niveau relatif max de raffinement
//
// Sortie:
//    GGDelaunayAvancement& info             // Info pour le suivi
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::appliqueScenario(GGDelaunayAvancement& info,
                                           const GGDelaunayScenario& scenario,
                                           Entier nntCible,
                                           DReel  htgt,
                                           Entier rMin,
                                           Entier rMax)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   const TCContainerSommet::size_type nntCible_l = (nntCible < 0) ?
               std::numeric_limits<TCContainerSommet::size_type>::max() :
               nntCible;

   msg = ctrlCompteReference(0);

   // ---  Niveau de raffinement
   if (msg)
   {
      GGDelaunaySommet::asgNiveauRaff(rMin, rMax);
      GGDelaunayArete::asgNiveauRaff(rMin, rMax);
      GGDelaunaySurface::asgNiveauRaff(rMin, rMax);
   }

   // ---  Taille de maille cible
   if (msg)
   {
      GGDelaunayArete::asgLongueurCible(htgt);
      GGDelaunaySurface::asgLongueurCible(htgt);
      msg = rehashMetriques();
   }

   // ---  Stat initiales
   if (msg)
   {
      msg = remplisInfo(GGDelaunayOperation::ETAT_INITIAL, info);
   }

   // ---  Exécute tour à tour chaque opération
   typedef GGDelaunayScenario::TCContainerOperations::const_iterator TTIter;
   for (TTIter i = scenario.reqOperationDebut(); msg && i != scenario.reqOperationFin(); ++i)
   {

      switch ((*i).type)
      {
         case GGDelaunayOperation::MAILLE_PEAU:
            msg = genereMaillagePeau();
            break;
         case GGDelaunayOperation::LISSE_METRIQUES:
            msg = lisseMetriques();
            break;
         case GGDelaunayOperation::RESCALE_METRIQUES:
            msg = rescaleMetriques();
            break;
         case GGDelaunayOperation::RETOURNE_ARETES:
           msg = retourneAretes();
           break;
         case GGDelaunayOperation::SOIGNE_PEAU:
            msg = soignePeau();
            break;
         case GGDelaunayOperation::SOIGNE_PONT:
            msg = soignePont();
            break;
         case GGDelaunayOperation::DERAFFINE_PAR_SOMMETS:
           msg = deraffineParSommets();
           break;
//         case GGDelaunayOperation::DERAFFINE_PAR_SURFACES:
//            deraffineParSurfaces();
//            break;
         case GGDelaunayOperation::RAFFINE_PAR_SURFACES:
            msg = raffineParSurfaces(nntCible_l);
            break;
         case GGDelaunayOperation::RAFFINE_PAR_ARETES:
            msg = raffineParAretes(nntCible_l);
            break;
         case GGDelaunayOperation::REGULARISE_PAR_SURFACES:
            msg = regulariseParSurfaces();
            break;
         case GGDelaunayOperation::REGULARISE_PAR_ARETES:
            msg = regulariseParAretes();
            break;
         case GGDelaunayOperation::REGULARISE_BARYCENTRIQUE:
            msg = regulariseBarycentrique();
            break;
         case GGDelaunayOperation::REGULARISE_ETOILES:
            msg = regulariseEtoiles();
            break;
         default:
            ASSERTION((*i).type > GGDelaunayOperation::REGULARISE_PAR_ARETES);
      }
      if (msg) msg = ctrlCompteReference(1);
      if (!msg) break;

      msg = remplisInfo((*i).type, info);
   }

   if (msg) msg = ctrlCompteReference(0);
   if (msg) msg = remplisInfo(GGDelaunayOperation::ETAT_FINAL, info);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Assigne la carte des métriques.
//
// Description:
//    La méthode publique <code>asgCarteMetrique()</code> assigne au mailleur
//    la carte des métriques à utiliser. Les métriques sont transférées aux
//    sommets avant de forcer le recalcule des critères.
//
// Entrée:
//    const TCChampErr& carte
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::asgCarteMetrique(const TCChampErr& carte)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   delete carteMetriquesP;
   carteMetriquesP = new TCChampErr(carte);
   msg = asgMetriques();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute une carte des métriques.
//
// Description:
//    La méthode publique <code>ajtCarteMetrique()</code> ajoute au mailleur
//    une carte des métriques. Les métriques sont transférées aux
//    sommets avant de forcer le recalcule des critères.
//
// Entrée:
//    const TCChampErr& carte
//
// Sortie:
//
// Notes:
//    La méthode n'est pas fonctionnelle. Elle est identique à asgCarteMetrique
//************************************************************************
ERMsg GGMailleurDelaunay::ajtCarteMetrique(const TCChampErr& carte)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(carteMetriquesP != NUL);
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   ASSERTION(false);

/*
   delete carteMetriquesP;
   carteMetriquesP = new TCChampErr(carte);
   msg = asgMetriques();
*/

   msg = ERMsg(ERMsg::ERREUR, "Méthode GGMailleurDelaunay::ajtCarteMetrique non implantée");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Assigne les métriques.
//
// Description:
//    La méthode privée <code>asgMetrique()</code> assigne les métriques à
//    partir de la carte des métriques. Les métriques sont transférées aux
//    sommets avant de forcer le recalcule des critères.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::asgMetriques()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   if (msg)
   {
      TCContainerSommet::iterator pI   = sommets.begin();
      TCContainerSommet::iterator eopI = sommets.end();
      for ( ; msg && pI != eopI; ++pI)
      {
         TCChampErr::TCDonnee e;
         const GOCoordonneesXYZ& p = (*pI)->reqPosition();
         msg = carteMetriquesP->reqValeur(e, p);
         ASSERTION(msg);
         if (msg) (*pI)->asgMetrique(e);
      }
   }

   if (msg)
   {
      msg = rehashMetriques();
   }


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Assigne les métriques.
//
// Description:
//    La méthode privée <code>asgMetrique()</code> assigne les métriques à
//    partir de la carte des métriques. Les métriques sont transférées aux
//    sommets avant de forcer le recalcule des critères.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::rehashMetriques()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   if (msg)
   {
      TCContainerSurface::iterator sI   = surfaces.begin();
      TCContainerSurface::iterator eosI = surfaces.end();
      while (sI != eosI)
      {
         GGDelaunaySurfaceP sP = (*sI);
         surfaces.erase(sI++);
         sP->majAireMetrique();
         surfaces.insert(sP);
      }
   }

   if (msg)
   {
      TCContainerArete::iterator aI   = aretes.begin();
      TCContainerArete::iterator eoaI = aretes.end();
      while (aI != eoaI)
      {
         GGDelaunayAreteP aP = (*aI);
         aretes.erase(aI++);
         aP->majLongueurMetrique();
         aretes.insert(aP);
      }
   }

   if (msg)
   {
      TCContainerSommet::iterator pI   = sommets.begin();
      TCContainerSommet::iterator eopI = sommets.end();
      while (pI != eopI)
      {
         GGDelaunaySommetP pP = (*pI);
         sommets.erase(pI++);
         pP->calculeCritere();
         sommets.insert(pP);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Contrôle les comptes de référence.
//
// Description:
//    La méthode privée <code>ctrlCompteReference()</code> contrôle
//    les comptes de références des entités. Le contrôle n'est valide que
//    dans un état stable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::ctrlCompteReference(Entier lvl) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   if (lvl > CTRL_COMPTEREF_LVL) return msg;

   if (msg)
   {
      TCContainerSurface::const_iterator sI   = surfaces.begin();
      TCContainerSurface::const_iterator eosI = surfaces.end();
      while (msg && sI != eosI) msg = (*sI++)->ctrlCompteReference();
   }

   if (msg)
   {
      TCContainerArete::const_iterator aI   = aretes.begin();
      TCContainerArete::const_iterator eoaI = aretes.end();
      while (msg && aI != eoaI) msg = (*aI++)->ctrlCompteReference();
   }

   if (msg)
   {
      TCContainerSommet::const_iterator pI   = sommets.begin();
      TCContainerSommet::const_iterator eopI = sommets.end();
      while (msg && pI != eopI) msg = (*pI++)->ctrlCompteReference();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Interpole la métrique.
//
// Description:
//    La méthode publique <code>reqMetrique()</code> retourne la métrique
//    du point passé en argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOEllipseErreur GGMailleurDelaunay::reqMetrique(const GOCoordonneesXYZ& p) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   TCChampErr::TCDonnee e;
   if (carteMetriquesP != NUL)
   {
      ERMsg msg = ERMsg::OK;
      msg = carteMetriquesP->reqValeur(e, p);
      ASSERTION(msg);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return e;
}

//************************************************************************
// Sommaire:  Déraffine le maillage par les sommets.
//
// Description:
//    La méthode privée <code>deraffineParSommets()</code> déraffine par les
//    sommets le maillage tant que le critère de déraffinement n'est pas
//    rempli.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Le cas "pas en erreur" et "pas changé" n'est pas traité
//************************************************************************
ERMsg GGMailleurDelaunay::deraffineParSommets()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

//   std::cout << "Déraffine : " << std::endl;

   // ---  Déraffine tous les sommets
   Booleen termine = FAUX;
   Booleen change  = FAUX;
   EntierN ctr = 0;
   while (msg && !termine)
   {
      TCContainerSommet::reverse_iterator sI   = sommets.rbegin();
      TCContainerSommet::reverse_iterator eosI = sommets.rend();
      while (sI != eosI && ! (*sI)->estDeraffinable()) ++sI;

      if (msg && sI != eosI && (*sI)->doitEtreDeraffine())
      {
//         if (ctr%100 == 0)
//            std::cout<< " " << std::setprecision(15) << (*sI)->reqCritere() << std::endl;

         GGDelaunaySommetP somP = *(sI);
               somP->ajouteReference();                      // Pour garantir la survie
         msg = somP->deraffine(change);                      // Raffine
               somP->enleveReference();                      // Relache
         if (msg) msg = ctrlCompteReference(CTRL_COMPTEREF_LVL+1);
      }
      else
      {
         termine = VRAI;
      }
      ++ctr;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Déraffine le maillage par les surfaces.
//
// Description:
//    La méthode privée <code>deraffineParSurfaces()</code> déraffine les
//    surfaces du maillage tant que le critère de déraffinement n'est pas
//    rempli.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::deraffineParSurfaces()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Enlève les références aux sommets.
//
// Description:
//    La méthode publique <code>enleveReferencesAjouteSommet()</code> enlève
//    les références sur les sommets maintenues par le mailleur afin d'en
//    garantir l’existence. Ces références doivent être enlevées
//    une fois tous les sommets, arêtes et surface ajoutées.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::enleveReferencesAjouteSommet()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Enlève les références aux sommets assignées
   std::vector<GGDelaunaySommetP>::iterator spI;
   for (spI = sommetsAssignes.begin(); spI != sommetsAssignes.end(); ++spI)
   {
      (*spI)->enleveReference();
   }
   sommetsAssignes.erase(sommetsAssignes.begin(), sommetsAssignes.end());

#ifdef MODE_DEBUG
   POSTCONDITION(sommetsAssignes.size() == 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Génère le maillage de base avec les sommets de la peau.
//
// Description:
//    La méthode privée <code>genereMaillagePeau()</code> génère le premier
//    maillage à partir des sommets. Les sommets doivent donc être dans
//    l'ordre de parcours d'un polygone.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::genereMaillagePeau()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets.size() >= 3);
   PRECONDITION(surfaces.size() == 0);
   PRECONDITION(aretes.size()   == 0);
   PRECONDITION(sommetsAssignes.size() == sommets.size());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transfert les sommets
   GGDelaunayPolygone polygone;
   for (TCContainerSommetAssigne::iterator sI = sommetsAssignes.begin();
        sI != sommetsAssignes.end();
        ++sI)
      polygone.ajouteSommet(*sI);

   // ---  Crée toutes les arêtes de la peau et transfert
   const GGDelaunayAreteArguments artArgs =
      GGDelaunayAreteArguments()
      .raffinable(VRAI)
      .retournable(FAUX)
      .surPeau(VRAI);
   TCContainerSommetAssigne::iterator p1I = sommetsAssignes.begin();
   TCContainerSommetAssigne::iterator p2I = p1I + 1;
   while (p1I != sommetsAssignes.end())
   {
      GGDelaunayAreteP aP = new GGDelaunayArete(this, *p1I, *p2I, artArgs);
      aP->enleveReference();
      polygone.ajouteArete(aP);

      ++p1I;
      ++p2I;
      if (p2I == sommetsAssignes.end())
         p2I = sommetsAssignes.begin();
   }

   // ---  Génère le maillage
   msg = polygone.genereMaillage(this);

   // ---  Supprime les références des sommets
   if (msg)
      msg = enleveReferencesAjouteSommet();

#ifdef MODE_DEBUG
   POSTCONDITION(!msg || surfaces.size() > 0);
   POSTCONDITION(!msg || aretes.size() >= sommets.size());
   POSTCONDITION(!msg || sommetsAssignes.size() == sommets.size());
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Raffine le maillage par les arêtes.
//
// Description:
//    La méthode privée <code>raffineParAretes()</code> raffine les arêtes
//    du maillage tant que le critère de raffinement n'est pas rempli.
//    <p>
//    Le raffinement traite toujours l'arête la plus longue, et le traitement
//    se termine lorsque toutes les arêtes remplissent le critère de raffinement
//    ou lorsque l'arête à traiter n'est pas raffinable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::raffineParAretes(TCContainerSommet::size_type nntCible)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Raffine toutes les arêtes
   Booleen termine = FAUX;
   while (!termine && sommets.size() < nntCible)
   {
      TCContainerArete::iterator aI   = aretes.begin();
      TCContainerArete::iterator eoaI = aretes.end();
//      while (aI != eoaI && ! (*aI)->estRaffinable()) ++aI;

      bool change = FAUX;
      while (aI != eoaI && ! change)
      {
         GGDelaunayAreteP artP = *(aI);
         artP->ajouteReference();                      // Pour garantir la survie
         change = artP->raffine();                     // Raffine
         artP->enleveReference();                      // Relâche
         ++aI;
      }
      termine = !change;

/*
      if (aI != eoaI && (*aI)->doitEtreRaffinee())
      {
         GGDelaunayAreteP artP = *(aI);
         artP->ajouteReference();                      // Pour garantir la survie
         artP->raffine();                              // Raffine
         artP->enleveReference();                      // Relâche
      }
      else
      {
         termine = VRAI;
      }
*/
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Raffine le maillage par les surfaces.
//
// Description:
//    La méthode privée <code>raffineParSurfaces()</code> raffine les surfaces
//    du maillage tant que le critère de raffinement n'est pas rempli.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::raffineParSurfaces(TCContainerSommet::size_type nntCible)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Raffine toutes les surfaces
   Booleen termine = FAUX;
   while (!termine && sommets.size() < nntCible)
   {
      if ( (*aretes.begin())->estRaffinable() &&
          !(*aretes.begin())->estRetournable() &&
           (*aretes.begin())->doitEtreRaffinee())
      {
         GGDelaunayAreteP artP = *aretes.begin();
         artP->ajouteReference();                      // Pour garantir la survie
         artP->raffine();                              // Raffine
         artP->enleveReference();                      // Relâche
      }
      else if ((*surfaces.begin())->doitEtreRaffinee())
      {
         GGDelaunaySurfaceP srfP = *surfaces.begin();
         srfP->ajouteReference();                      // Pour garantir la survie
         srfP->raffine();                              // Raffine
         srfP->enleveReference();                      // Relâche
      }
      else
         termine = VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Régularise le maillage par lissage barycentrique.
//
// Description:
//    La méthode privée <code>regulariseBarycentrique()</code> régularise le
//    maillage par recentrement barycentrique des sommets par rapport à leurs
//    voisins.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::regulariseBarycentrique()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Copie les pointeurs dans un vecteur temp, car sommets sera
   //      changé sous nos pieds
   std::vector<GGDelaunaySommetP> tmp;
   tmp.reserve(sommets.size());
   for (TCContainerSommet::iterator sI = sommets.begin(); sI != sommets.end(); ++sI)
      tmp.push_back(*sI);

   // ---  Lisse le maillage
   for (std::vector<GGDelaunaySommetP>::iterator iI = tmp.begin(); iI != tmp.end(); ++iI)
      (*iI)->regulariseBarycentrique();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Régularise les "étoiles".
//
// Description:
//    La méthode privée <code>regulariseEtoiles()</code> régularise les
//    étoiles, noeuds entourés de trop d'éléments.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::regulariseEtoiles()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Copie les pointeurs dans un vecteur temp, car sommets sera
   //      changé sous nos pieds
   std::vector<GGDelaunaySommetP> tmp;
   tmp.reserve(sommets.size());
   for (TCContainerSommet::iterator sI = sommets.begin(); sI != sommets.end(); ++sI)
      tmp.push_back(*sI);

   // ---  Lisse le maillage
   for (std::vector<GGDelaunaySommetP>::iterator iI = tmp.begin(); iI != tmp.end(); ++iI)
   {
      GGDelaunaySommetP somP = *(iI);
      somP->ajouteReference();                      // Pour garantir la survie
      somP->regulariseEtoiles();
      somP->enleveReference();                      // Relâche
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Lisse les métriques par lissage barycentrique.
//
// Description:
//    La méthode privée <code>lisseMetriques()</code> régularise le
//    métriques par lissage.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::lisseMetriques()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   ALLisseChampEF<TCChampErr> algo;
   msg = algo.lisse(*carteMetriquesP);

   if (msg)
      msg = asgMetriques();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Met les métriques à l'échelle.
//
// Description:
//    La méthode privée <code>rescaleMetriques()</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::rescaleMetriques()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

/*
   ALLisseChampEF<TCChampErr> algo;
   msg = algo.lisse(*carteMetriquesP);

   if (msg)
      msg = asgMetriques();
*/

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Régularise le maillage par les arêtes.
//
// Description:
//    La méthode privée <code>regulariseParAretes()</code> régularise le
//    maillage par les arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::regulariseParAretes()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Copie les pointeurs dans un vecteur temp, car sommets sera
   //      changé sous nos pieds
   std::vector<GGDelaunaySommetP> tmp;
   tmp.reserve(sommets.size());
   for (TCContainerSommet::iterator sI = sommets.begin(); sI != sommets.end(); ++sI)
      tmp.push_back(*sI);

   // ---  Lisse le maillage
   for (std::vector<GGDelaunaySommetP>::iterator iI = tmp.begin(); iI != tmp.end(); ++iI)
      (*iI)->regulariseParAretes();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Régularise le maillage par les surfaces.
//
// Description:
//    La méthode privée <code>regulariseParSurfaces()</code> régularise le
//    maillage par les surfaces.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::regulariseParSurfaces()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Copie les pointeurs dans un vecteur temp, car sommets sera
   //      changé sous nos pieds
   std::vector<GGDelaunaySommetP> tmp;
   tmp.reserve(sommets.size());
   for (TCContainerSommet::iterator sI = sommets.begin(); sI != sommets.end(); ++sI)
      tmp.push_back(*sI);

   // ---  Lisse le maillage
   for (std::vector<GGDelaunaySommetP>::iterator iI = tmp.begin(); iI != tmp.end(); ++iI)
      (*iI)->regulariseParSurfaces();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Retourne toutes les arêtes.
//
// Description:
//    La méthode privée <code>retourneAretes()</code> retourne (balance)
//    toutes les arêtes du maillage.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::retourneAretes()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transvase toutes les arêtes
   GGContainerAreteARetourner containerBalance;
   for (TCContainerArete::iterator i = aretes.begin(); i != aretes.end(); ++i)
      containerBalance.ajouteSansControle(*i);

   // ---  Balance toutes les arêtes du maillage
   while (containerBalance.reqDimension() != 0)
   {
      GGDelaunayAreteP artP = *containerBalance.reqAreteDebut();
      artP->ajouteReference();                      // Pour garantir la survie
      artP->retourne(containerBalance);             // Retourne
      artP->enleveReference();                      // Relâche
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Retourne les arêtes des éléments à 2 arêtes sur la peau.
//
// Description:
//    La méthode privée <code>soignePeau()</code> retourne (balance)
//    les arêtes des éléments qui ont deux arêtes sur la peau pour
//    les éliminer.
//    Cette opération ne prend pas en compte les métriques.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::soignePeau()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size()   > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Balance les arêtes
   Booleen change = VRAI;
   while (change)
   {
      change = FAUX;
      for (TCContainerSurface::iterator sI = surfaces.begin();
           sI != surfaces.end() && !change;
           ++sI)
      {
         GGDelaunaySurfaceP srfP = *sI;
         srfP->ajouteReference();                      // Pour garantir la survie
         change = srfP->soignePeau();                  // Retourne
         srfP->enleveReference();                      // Relâche
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Retourne les arêtes des éléments à .
//
// Description:
//    La méthode privée <code>soignePont()</code> raffine les arêtes
//    qui ne sont pas sur la peau et qui ont leurs deux sommets
//    sur la peau.
//    Cette opération ne prend pas en compte les métriques.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::soignePont()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces.size() > 0);
   PRECONDITION(aretes.size() > 0);
   PRECONDITION(sommets.size() >= 3);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Balance toutes les arêtes du maillage
   Booleen change = VRAI;
   while (change)
   {
      change = FAUX;
      for (TCContainerArete::iterator aI = aretes.begin();
         aI != aretes.end() && !change;
         ++aI)
      {
         GGDelaunayAreteP artP = *aI;
         artP->ajouteReference();                      // Pour garantir la survie
         change = artP->soignePont();                  // Retourne
         artP->enleveReference();                      // Relâche
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Remplis la structure d'information.
//
// Description:
//    La méthode publique <code>remplisInfo(...)</code> remplis la
//    structure d'information avec les statistiques courantes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGMailleurDelaunay::remplisInfo(GGDelaunayOperation::Type t, GGDelaunayAvancement& info) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   DReel q = 0.0;
         TCContainerArete::const_iterator aI   = aretes.begin();
   const TCContainerArete::const_iterator eoaI = aretes.end();
   while (aI != eoaI) q += (*aI++)->reqCritere();
   q /= aretes.size();

   info.operation   = t;
   info.qualite     = exp(q);
   info.nbrSommets  = static_cast<long>(sommets.size());
   info.nbrSurfaces = static_cast<long>(surfaces.size());

   // ---  Sommets: min
   {
            TCContainerSommet::const_reverse_iterator sI   = sommets.rbegin();
      const TCContainerSommet::const_reverse_iterator eosI = sommets.rend();
      while (sI != eosI && ! (*sI)->estDeraffinable()) ++sI;
      info.sommetCmin  = (*sI)->reqCritere();
   }
   // ---  Sommets: med
   {
      info.sommetCmed  = (*avance(sommets.begin(), sommets.size()/2))->reqCritere();
   }
   // ---  Sommets: max
   {
            TCContainerSommet::const_iterator sI   = sommets.begin();
      const TCContainerSommet::const_iterator eosI = sommets.end();
      while (sI != eosI && !(*sI)->estDeraffinable()) ++sI;
      info.sommetCmax  = (*sI)->reqCritere();
   }

   // ---  Arêtes: min
   {
            TCContainerArete::const_reverse_iterator aI   = aretes.rbegin();
      const TCContainerArete::const_reverse_iterator eoaI = aretes.rend();
      while (aI != eoaI && ! (*aI)->estRaffinable()) ++aI;
      info.areteCmin   = (*aI)->reqCritere();
   }
   // ---  Arêtes: med
   {
      info.areteCmed   = (*avance(aretes.begin(), aretes.size()/2))->reqCritere();
   }
   // ---  Arêtes: max
   {
            TCContainerArete::const_iterator aI   = aretes.begin();
      const TCContainerArete::const_iterator eoaI = aretes.end();
      while (aI != eoaI && ! (*aI)->estRaffinable()) ++aI;
      info.areteCmax   = (*aI)->reqCritere();
   }

   // ---  Call-back de notification
   info();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Vide l'objet.
//
// Description:
//    La méthode privée <code>vide()</code> vide l'objet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGMailleurDelaunay::vide()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Enlève les références aux sommets assignées
   std::vector<GGDelaunaySommetP>::iterator spI;
   for (spI = sommetsAssignes.begin(); spI != sommetsAssignes.end(); ++spI)
   {
      (*spI)->enleveReference();
   }
   sommetsAssignes.erase(sommetsAssignes.begin(), sommetsAssignes.end());

   // ---  Détache les surfaces et les arêtes
   while (surfaces.size() != 0)
   {
      GGDelaunaySurfaceP srfP = *(surfaces.rbegin());
      srfP->ajouteReference();
      srfP->detache();
      srfP->enleveReference();
   }
   while (aretes.size() != 0)
   {
      GGDelaunayAreteP artP = *(aretes.rbegin());
      artP->ajouteReference();
      artP->detache();
      artP->enleveReference();
   }

#ifdef MODE_DEBUG
   POSTCONDITION(sommetsAssignes.size() == 0);
   POSTCONDITION(surfaces.size() == 0);
   POSTCONDITION(aretes.size()   == 0);
   POSTCONDITION(sommets.size()  == 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Functor de comparaison entre deux sommets, arêtes, surfaces.
//
// Description:
//    Les méthodes protégées <code>ajoute()</code> comparent deux sommets,
//    arêtes ou sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Ces méthodes devraient être inline, mais elles demande l'inclusion de
//    GGDelaunaySommet.h, GGDelaunayArete.h et GGDelaunaySurface.h
//
//************************************************************************
Booleen
GGMailleurDelaunay::GGDelaunayAreteEstSup::operator()(GGDelaunayAreteP i1P, GGDelaunayAreteP i2P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   DReel c1 = i1P->reqCritere();
   DReel c2 = i2P->reqCritere();
   return (c2 < c1 || (c2 == c1 && i2P < i1P));
}

Booleen
GGMailleurDelaunay::GGDelaunaySommetEstSup::operator()(GGDelaunaySommetP i1P, GGDelaunaySommetP i2P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   DReel c1 = i1P->reqCritere();
   DReel c2 = i2P->reqCritere();
   return (c2 < c1 || (c2 == c1 && i2P < i1P));
}

Booleen
GGMailleurDelaunay::GGDelaunaySurfaceEstSup::operator()(GGDelaunaySurfaceP i1P, GGDelaunaySurfaceP i2P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   DReel c1 = i1P->reqCritere();
   DReel c2 = i2P->reqCritere();
   return (c2 < c1 || (c2 == c1 && i2P < i1P));
}

#ifdef MODE_DEBUG
Booleen
GGMailleurDelaunay::GGDelaunayAreteExiste::operator()(GGDelaunayAreteP i1P, GGDelaunayAreteP i2P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   if (GGDelaunayArete::ontMemeSommets(i1P, i2P)) return false;
   if (i1P->reqSommet0() <  i2P->reqSommet0()) return true;
   if (i1P->reqSommet0() >  i2P->reqSommet0()) return false;
   if (i1P->reqSommet1() <  i2P->reqSommet1()) return true;
// if (i1P->reqSommet1() >  i2P->reqSommet1()) return false;
   return false;
}

Booleen
GGMailleurDelaunay::GGDelaunaySommetExiste::operator()(GGDelaunaySommetP i1P, GGDelaunaySommetP i2P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return (i1P < i2P);
}

Booleen
GGMailleurDelaunay::GGDelaunaySurfaceExiste::operator()(GGDelaunaySurfaceP i1P, GGDelaunaySurfaceP i2P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   if (GGDelaunaySurface::ontMemeSommets(i1P, i2P)) return false;
   if (i1P->reqSommet0() <  i2P->reqSommet0()) return true;
   if (i1P->reqSommet0() >  i2P->reqSommet0()) return false;
   if (i1P->reqSommet1() <  i2P->reqSommet1()) return true;
   if (i1P->reqSommet1() >  i2P->reqSommet1()) return false;
   if (i1P->reqSommet2() <  i2P->reqSommet2()) return true;
// if (i1P->reqSommet2() >  i2P->reqSommet2()) return false;
   return false;
}
#endif   // ifdef MODE_DEBUG
