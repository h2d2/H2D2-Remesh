//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGContainerAreteARetourner.cpp
// Classe:   GGContainerAreteARetourner
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************

#include "GGContainerAreteARetourner.h"

//************************************************************************
// Sommaire:  Constructeur par défaut.
//
// Description:
//    Le constructeur public <code>GGContainerAreteARetourner()</code>
//    initialise le container en réservant un espace minimum.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGContainerAreteARetourner::GGContainerAreteARetourner()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur de la classe.
//
// Description:
//    Le destructeur public <code>~GGContainerAreteARetourner()</code>
//    ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGContainerAreteARetourner::~GGContainerAreteARetourner()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire:  Ajoute une arête à la liste.
//
// Description:
//    La méthode publique <code>ajoute(...)</code> ajoute à la liste
//    l'arête passée en argument. Celle-ci n'est ajoutée que si elle n'est
//    pas déjà dans la liste.
//
// Entrée:
//    GGDelaunayAreteP  aP          Pointeur à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGContainerAreteARetourner::ajoute(GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   aretes.insert(aP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Retire une arête de la liste.
//
// Description:
//    La méthode publique <code>retire(...)</code> retire de la liste
//    l'arête passée en argument.
//
// Entrée:
//    GGDelaunayAreteP  aP          Pointeur à retirer
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGContainerAreteARetourner::retire(GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   aretes.erase(aP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

