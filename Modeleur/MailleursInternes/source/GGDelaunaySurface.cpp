//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunaySurface.cpp
// Classe:   GGDelaunaySurface
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
// 13-11-98  Yves Secretan    Ajoute reqBarycentre
//                            Corrige reqCritereElong
//*****************************************************************************

#include "GGDelaunaySurface.h"
#include "GGDelaunayArete.h"
#include "GGDelaunaySommet.h"
#include "GGDelaunayInfo.h"
#include "GGContainerAreteARetourner.h"

#include "GGMailleurDelaunay.h"
#include <algorithm>
#include <set>

/*
L'invariant, qui est appelé par majAireMetrique(), contient un test sur l'aire
qui doit être strictement positif. Dans le constructeur, il faut donc
initialiser l'aire à une valeur positive pour passer le test.  De plus,
les invariants sont réputés non valides avant la fin du constructeur.
Cette solution est préférée car elle permet de conserver les invariants
les plus contraignants.
*/
#ifdef MODE_DEBUG
#  include <limits>
   const DReel AIRE_CTR = std::numeric_limits<double>::epsilon();
#else
   const DReel AIRE_CTR = 0.0;
#endif   // ifdef MODE_DEBUG

// ---  Aire d'un triangle équilatéral de côté sqrt(3)
DReel  GGDelaunaySurface::surfaceCible = 1.3;
Entier GGDelaunaySurface::niveauRaffMin = -2;
Entier GGDelaunaySurface::niveauRaffMax = 2;
const DReel GGDelaunaySurface::TOLERANCE_DEFORMATION = GGDelaunayArete::TOLERANCE_DEFORMATION * GGDelaunayArete::TOLERANCE_DEFORMATION;

//************************************************************************
// Sommaire:  Constructeur avec trois sommets et trois arêtes.
//
// Description:
//    Le constructeur public <code>GGDelaunaySurface()</code> construit une
//    surface à partir des trois sommets et des trois arêtes passés en argument.
//    L'objet créé se lie aux sommets et aux arêtes, et lie également les
//    sommets ainsi que les arêtes à lui-même. De plus, il s'enregistre auprès
//    du mailleur.
//
// Entrée:
//    GGMailleurDelaunayP mP       // Le mailleur à notifier de la construction
//    GGDelaunaySommetP   p0P      // Les trois sommets
//    GGDelaunaySommetP   p1P
//    GGDelaunaySommetP   p2P
//    GGDelaunayAreteP    a0P      // Les trois arêtes
//    GGDelaunayAreteP    a1P
//    GGDelaunayAreteP    a2P
//
// Sortie:
//
// Notes:
//    Tous les appels pour faire les liens sont à la fin du constructeur
//    car les objets appelés peuvent nous appeler en retour. Ils doivent
//    alors être en présence d'un objet valide. C'est pas très beau !!
//
//************************************************************************
GGDelaunaySurface::GGDelaunaySurface (GGMailleurDelaunayP mP,
                                      GGDelaunaySommetP   p0P,
                                      GGDelaunaySommetP   p1P,
                                      GGDelaunaySommetP   p2P,
                                      GGDelaunayAreteP    a0P,
                                      GGDelaunayAreteP    a1P,
                                      GGDelaunayAreteP    a2P,
                                      const GGDelaunaySurfaceArguments& kwargs)
   : compteRef  (1)
   , niveauRaff (kwargs.m_lvl)
   , mailleurP  (mP)
   , aire       (AIRE_CTR)
   , infoClientP(NULL)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(mP != NUL);
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p2P != NUL);
   PRECONDITION(p0P != p1P && p0P != p2P && p1P != p2P);
   PRECONDITION(a0P != NUL);
   PRECONDITION(a1P != NUL);
   PRECONDITION(a2P != NUL);
   PRECONDITION(a0P != a1P && a0P != a2P && a1P != a2P);
#endif   // ifdef MODE_DEBUG

   // ---  Initialise les sommets et les arêtes
   sommets[0] = p0P;
   sommets[1] = p1P;
   sommets[2] = p2P;
   aretes[0]  = a0P;
   aretes[1]  = a1P;
   aretes[2]  = a2P;

   // ---  Calcule l'aire
   majAireMetrique();

   // ---  Fait les liens
   p0P->ajouteReference();
   p1P->ajouteReference();
   p2P->ajouteReference();

   a0P->ajouteReference();
   a1P->ajouteReference();
   a2P->ajouteReference();

   p0P->ajouteLienSurface(this);
   p1P->ajouteLienSurface(this);
   p2P->ajouteLienSurface(this);

   a0P->ajouteLienSurface(this);
   a1P->ajouteLienSurface(this);
   a2P->ajouteLienSurface(this);

   // ---  Enregistre auprès du maillage
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   POSTCONDITION(sommets[0] != NUL);
   POSTCONDITION(aretes[0] != NUL);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur de la classe.
//
// Description:
//    Le destructeur privé <code>~GGDelaunaySurface()</code> enlève les
//    références que l'objet à sur ses sommets et ses arêtes. Il retire
//    l'objet du mailleur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySurface::~GGDelaunaySurface()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef == 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (mailleurP != NUL)
   {
      mailleurP->retire(this);
      mailleurP = NUL;
   }

   if (sommets[0] != NUL)
   {
      sommets[0]->enleveReference();
      sommets[0] = NUL;
   }
   if (sommets[1] != NUL)
   {
      sommets[1]->enleveReference();
      sommets[1] = NUL;
   }
   if (sommets[2] != NUL)
   {
      sommets[2]->enleveReference();
      sommets[2] = NUL;
   }

   if (aretes[0] != NUL)
   {
      aretes[0]->enleveReference();
      aretes[0] = NUL;
   }
   if (aretes[1] != NUL)
   {
      aretes[1]->enleveReference();
      aretes[1] = NUL;
   }
   if (aretes[2] != NUL)
   {
      aretes[2]->enleveReference();
      aretes[2] = NUL;
   }

   if (infoClientP != NULL) delete infoClientP;
}

//************************************************************************
// Sommaire:  Calcule l'aire de la surface.
//
// Description:
//    La méthode privée <code>majAireMetrique()</code> calcule l'aire de la
//    surface dans la métrique des sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySurface::majAireMetrique()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Calcule la métrique de la surface
   GGMetriqueMaillage criSurf = (sommets[0]->reqMetrique() +
                                 sommets[1]->reqMetrique() +
                                 sommets[2]->reqMetrique()) / 3.0;

   // ---  Calcule l'aire dans la métrique de la surface
   aire = reqAireEuclide(sommets[0], sommets[1], sommets[2]) * criSurf.reqDetJ();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Calcule l'aire de la surface.
//
// Description:
//    La méthode statique <code>reqAireMetrique()</code> calcule l'aire de la
//    surface dans la métrique des sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGDelaunaySurface::reqAireMetrique(const GGMetriqueMaillage& c0, const GOCoordonneesXYZ& p0,
                                         const GGMetriqueMaillage& c1, const GOCoordonneesXYZ& p1,
                                         const GGMetriqueMaillage& c2, const GOCoordonneesXYZ& p2)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   // ---  Calcule la métrique de la surface
   const GGMetriqueMaillage criSurf = (c0 + c1 + c2) / 3.0;

   // ---  Calcule l'aire dans la métrique de la surface
   const DReel aire = GGDelaunaySurface::reqAireEuclide(p0, p1, p2) * criSurf.reqDetJ();

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return aire;
}

//************************************************************************
// Sommaire:  Contrôle le compte de référence.
//
// Description:
//    La méthode privée <code>ctrlCompteReference()</code> contrôle
//    les comptes de références de l'objet. Le contrôle n'est valide que
//    dans un état stable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GGDelaunaySurface::ctrlCompteReference() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   if (compteRef != 6)
      msg = ERMsg(ERMsg::ERREUR, "GGDelaunaySurface");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Détache l'objet de tous ses voisins.
//
// Description:
//    La méthode publique <code>detache()</code> enlève les références
//    que l'objet maintient sur ses sommets et ses arêtes.
//    <p>
//    Comme l'objet pourrait être détruis par l'appel, la méthode appelante
//    doit prendre une référence avant l'appel pour la relâcher après l'appel.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySurface::detache()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != 0);
   PRECONDITION(compteRef > 6);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Délie des sommets
   sommets[0]->enleveLienSurface(this);
   sommets[1]->enleveLienSurface(this);
   sommets[2]->enleveLienSurface(this);

   // ---  Délie des arêtes
   aretes[0]->enleveLienSurface(this);
   aretes[1]->enleveLienSurface(this);
   aretes[2]->enleveLienSurface(this);

#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Raffine la surface.
//
// Description:
//    La méthode publique <code>raffine</code> raffine le surface si le
//    critère de raffinement n'est pas satisfait.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//
// Sortie:
//    Retourne VRAI si la surface a été raffinée.
//
// Notes:
//
//************************************************************************
Booleen GGDelaunaySurface::raffine()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != 0);
   PRECONDITION(compteRef > 6);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen change = FAUX;

   // ---  Si le critère n'est pas vérifié raffine
   if (doitEtreRaffinee())
   {
      // ---  Détache soi-même des sommets et des arêtes
      detache();
      ASSERTION(compteRef > 0);

      // ---  Calcule la position du nouveau sommet
      GOCoordonneesXYZ posi = (sommets[0]->reqPosition() +
                               sommets[1]->reqPosition() +
                               sommets[2]->reqPosition()) / 3.0;

      // ---  Niveau de raffinement
      const Entier lvl = niveauRaff + 1;

      // ---  Crée le nouveau sommet avec la métrique de la surface
      const GGDelaunaySommetArguments somArgs = GGDelaunaySommetArguments().niveauRaff(lvl);
      GGDelaunaySommetP pcP = new GGDelaunaySommet(mailleurP, posi, reqMetrique(), somArgs);
//      GGDelaunaySommetP pcP = new GGDelaunaySommet(mailleurP, posi);

      // ---  Crée les 3 arêtes
      const GGDelaunayAreteArguments artArgs = GGDelaunayAreteArguments().niveauRaff(lvl);
      GGDelaunayAreteP ac0P = new GGDelaunayArete(mailleurP, pcP, sommets[0], artArgs);
      GGDelaunayAreteP ac1P = new GGDelaunayArete(mailleurP, pcP, sommets[1], artArgs);
      GGDelaunayAreteP ac2P = new GGDelaunayArete(mailleurP, pcP, sommets[2], artArgs);

      // ---  Crée les 3 sous-surfaces
      const GGDelaunaySurfaceArguments srfArgs = GGDelaunaySurfaceArguments().niveauRaff(lvl);
      GGDelaunaySurfaceP s0P = new GGDelaunaySurface(mailleurP, pcP, sommets[0], sommets[1], ac0P, aretes[0], ac1P, srfArgs);
      GGDelaunaySurfaceP s1P = new GGDelaunaySurface(mailleurP, pcP, sommets[1], sommets[2], ac1P, aretes[1], ac2P, srfArgs);
      GGDelaunaySurfaceP s2P = new GGDelaunaySurface(mailleurP, pcP, sommets[2], sommets[0], ac2P, aretes[2], ac0P, srfArgs);

      // ---  Balance les arêtes
      GGContainerAreteARetourner containerAretes;
      EntierN cnt = 0;
      containerAretes.ajouteSansControle(aretes[0]);
      containerAretes.ajouteSansControle(aretes[1]);
      containerAretes.ajouteSansControle(aretes[2]);
      while (containerAretes.reqDimension() != 0)
      {
         GGDelaunayAreteP aP = *containerAretes.reqAreteDebut();
         aP->ajouteReference();        // Pour garantir la survie
         aP->retourne(containerAretes, pcP);
         aP->enleveReference();
         ++cnt;
         if (cnt == 100)
         {
//            CLChaine msg = "Débordement";
//            ERErreur::logMessage(msg);
//            ERErreur::ecrisMessage(msg);
            break;
         }
      }
      {
//         CLChaine msg;
//         msg << cnt;
//         ERErreur::logMessage(msg);
//         ERErreur::ecrisMessage(msg);
      }

      // ---  Enlève les références crées par les new
      pcP->enleveReference();
      ac0P->enleveReference();
      ac1P->enleveReference();
      ac2P->enleveReference();
      s0P->enleveReference();
      s1P->enleveReference();
      s2P->enleveReference();

      change = VRAI;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return change;
}

//************************************************************************
// Sommaire:  Raffine la surface.
//
// Description:
//    La méthode publique <code>soignePeau</code> raffine le surface si le
//    critère de raffinement n'est pas satisfait.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//
// Sortie:
//    Retourne VRAI si la surface a été raffinée.
//
// Notes:
//
//************************************************************************
Booleen GGDelaunaySurface::soignePeau()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != 0);
   PRECONDITION(compteRef > 6);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen change = FAUX;
   const DReel swapTol = 0.1e0;

   int idArete = -1;
   if (aretes[0]->estSurPeau())
   {
      if (aretes[1]->estSurPeau())
         idArete = 2;
      else if (aretes[2]->estSurPeau())
         idArete = 1;
   }
   else if (aretes[1]->estSurPeau())
   {
      if (aretes[2]->estSurPeau())
         idArete = 0;
   }

   if (idArete >= 0)
   {
      // ---  L'autre élément ne doit pas avoir d'arête sur peau
      ConstGGDelaunayAreteP   aP = aretes[idArete];
      ConstGGDelaunaySurfaceP sP = aP->reqAutreSurface(this);
      ConstGGDelaunaySommetP  pP = sP->reqAutreSommet(aP->reqSommet0(), aP->reqSommet1());
      ConstGGDelaunayAreteP   a0P = sP->reqArete(pP, aP->reqSommet0());
      ConstGGDelaunayAreteP   a1P = sP->reqArete(pP, aP->reqSommet1());
      if (!a0P->estSurPeau() && !a1P->estSurPeau())
         change = aretes[idArete]->retourne(swapTol);
   }

#ifdef MODE_DEBUG
   POSTCONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return change;
}

//************************************************************************
// Sommaire:  Retourne l'aire de la surface.
//
// Description:
//    La méthode statique publique <code>reqAireEuclide(...)</code> retourne l'aire
//    de la surface délimitée par les trois sommets passés en arguments.
//    Cette aire est calculée dans la métrique euclidienne.
//
// Entrée:
//    GGDelaunaySommetP p0P      Les trois sommets de la surface
//    GGDelaunaySommetP p1P
//    GGDelaunaySommetP p2P
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGDelaunaySurface::reqAireEuclide(const GOCoordonneesXYZ& p0,
                                        const GOCoordonneesXYZ& p1,
                                        const GOCoordonneesXYZ& p2)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   const GOCoordonneesXYZ x10 = p1 - p0;
   const GOCoordonneesXYZ x20 = p2 - p0;
   DReel aire = 0.5 * prodVect(x10, x20).z();

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return aire;
}

//************************************************************************
// Sommaire:  Retourne l'aire de la surface.
//
// Description:
//    La méthode statique publique <code>reqAireEuclide(...)</code> retourne l'aire
//    de la surface délimitée par les trois sommets passés en arguments.
//    Cette aire est calculée dans la métrique euclidienne.
//
// Entrée:
//    GGDelaunaySommetP p0P      Les trois sommets de la surface
//    GGDelaunaySommetP p1P
//    GGDelaunaySommetP p2P
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GGDelaunaySurface::reqAireEuclide(ConstGGDelaunaySommetP p0P,
                                        ConstGGDelaunaySommetP p1P,
                                        ConstGGDelaunaySommetP p2P)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p2P != NUL);
   PRECONDITION(p0P != p1P);
   PRECONDITION(p0P != p2P);
   PRECONDITION(p1P != p2P);
#endif   // ifdef MODE_DEBUG

   return reqAireEuclide(p0P->reqPosition(), p1P->reqPosition(), p2P->reqPosition());
}

//************************************************************************
// Sommaire:  Retourne l'arête correspondant aux sommets.
//
// Description:
//    La méthode publique <code>reqArete(...)</code> retourne l'arête dont les
//    sommets correspondent aux sommets passés en argument.
//
// Entrée:
//    GGDelaunaySommetP p0P      Premier sommet
//    GGDelaunaySommetP p1P      Deuxième sommet
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayAreteP GGDelaunaySurface::reqArete(ConstGGDelaunaySommetP p0P,
                                             ConstGGDelaunaySommetP p1P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p0P != p1P);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GGDelaunayAreteP aP = NUL;
   if (aretes[0]->aSommets(p0P, p1P))
      aP = aretes[0];
   else if (aretes[1]->aSommets(p0P, p1P))
      aP = aretes[1];
   else if (aretes[2]->aSommets(p0P, p1P))
      aP = aretes[2];

#ifdef MODE_DEBUG
   POSTCONDITION(aP != NUL);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return aP;
}

//************************************************************************
// Sommaire:  Retourne le troisième sommet.
//
// Description:
//    La méthode publique <code>reqAutreSommet(...)</code> retourne le sommet
//    qui n'est pas égal aux sommets passés en argument.
//
// Entrée:
//    GGDelaunaySommetP p0P      Premier sommet
//    GGDelaunaySommetP p1P      Deuxième sommet
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunaySommetP GGDelaunaySurface::reqAutreSommet(ConstGGDelaunaySommetP p0P,
                                                    ConstGGDelaunaySommetP p1P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p0P != p1P);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GGDelaunaySommetP pP;
   if (sommets[0] == p0P)
   {
      if (sommets[1] == p1P)
         pP = sommets[2];
      else
         pP = sommets[1];
   }
   else if (sommets[0] == p1P)
   {
      if (sommets[1] == p0P)
         pP = sommets[2];
      else
         pP = sommets[1];
   }
   else
   {
      pP = sommets[0];
   }

#ifdef MODE_DEBUG
   POSTCONDITION(pP != NUL);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return pP;
}

//************************************************************************
// Sommaire:  Retourne le barycentre de la surface.
//
// Description:
//    La méthode publique <code>reqBarycentre()</code> retourne le barycentre
//    de la surface en métrique euclidienne.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ GGDelaunaySurface::reqBarycentre() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GOCoordonneesXYZ barycentre = (sommets[0]->reqPosition() +
                                  sommets[1]->reqPosition() +
                                  sommets[2]->reqPosition()) / 3.0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return barycentre;
}

//************************************************************************
// Sommaire:  Retourne le critère d'élongation de la surface.
//
// Description:
//    La méthode publique <code>reqCritereElong(...)</code> retourne le critère
//    d'élongation de la surface. Le critère est le rapport entre l'aire
//    du cercle inscrit et l'aire de triangle. On utilise une
//    formule équivalente faisant intervenir les longueurs des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    A cause d'imprécisions numériques, on borne le critère vers le bas.
//
//************************************************************************
DReel GGDelaunaySurface::reqCritereElong(ConstGGDelaunayAreteP a0P,
                                         ConstGGDelaunayAreteP a1P,
                                         ConstGGDelaunayAreteP a2P)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(a0P != NUL);
   PRECONDITION(a1P != NUL);
   PRECONDITION(a2P != NUL);
   PRECONDITION(a0P != a1P);
   PRECONDITION(a0P != a2P);
   PRECONDITION(a1P != a2P);
#endif   // ifdef MODE_DEBUG

   // ---  Calcule la métrique moyenne
   const GGMetriqueMaillage metrique = (a0P->reqSommet0()->reqMetrique() +
                                        a0P->reqSommet1()->reqMetrique() +
                                        a1P->reqSommet0()->reqMetrique() +
                                        a1P->reqSommet1()->reqMetrique() +
                                        a2P->reqSommet0()->reqMetrique() +
                                        a2P->reqSommet1()->reqMetrique()) / 6.0;

   // ---  Calcule le critère
   const DReel l0 = metrique.calculeLongueurDirecte(a0P->reqSommet0()->reqPosition(),
                                                    a0P->reqSommet1()->reqPosition());
   const DReel l1 = metrique.calculeLongueurDirecte(a1P->reqSommet0()->reqPosition(),
                                                    a1P->reqSommet1()->reqPosition());
   const DReel l2 = metrique.calculeLongueurDirecte(a2P->reqSommet0()->reqPosition(),
                                                    a2P->reqSommet1()->reqPosition());
   const DReel p = 0.5*(l0 + l1 + l2);
   const DReel val = std::max(((p-l0)*(p-l1)*(p-l2)) / (p*p*p), 0.0);

#ifdef MODE_DEBUG
   POSTCONDITION(val >= 0.0);
#endif   // ifdef MODE_DEBUG
   return val;
}

//************************************************************************
// Sommaire:  Retourne la métrique de raffinement du sommet.
//
// Description:
//    La méthode publique <code>reqMetrique()</code> retourne la métrique
//    du critère de raffinement de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGMetriqueMaillage GGDelaunaySurface::reqMetrique() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GGMetriqueMaillage metrique = (sommets[0]->reqMetrique() +
                                  sommets[1]->reqMetrique() +
                                  sommets[2]->reqMetrique()) / 3.0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return metrique;
}

//************************************************************************
// Sommaire:  Retourne la position idéale des sommets.
//
// Description:
//    La méthode publique <code>reqPosiIdeale(...)</code> retourne la position
//    idéale que devrait occuper le sommet passé en argument par rapport à
//    l'arête opposée à ce sommet.
//
// Entrée:
//    ConstGGDelaunaySommetP pP
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ GGDelaunaySurface::reqPosiIdeale(ConstGGDelaunaySommetP pP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(pP != NUL);
   PRECONDITION(pP == sommets[0] || pP == sommets[1] || pP == sommets[2]);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Cherche les deux sommets opposés
   GGDelaunaySommetP p0P;
   GGDelaunaySommetP p1P;
   if (pP == sommets[0])
   {
      p0P = sommets[1];
      p1P = sommets[2];
   }
   else if (pP == sommets[1])
   {
      p0P = sommets[2];
      p1P = sommets[0];
   }
   else
   {
      p0P = sommets[0];
      p1P = sommets[1];
   }

   // ---  Calcule la métrique de la surface
   static const GGMetriqueMaillage criRota(2.0, 2.0, 1.047197551);
   GGMetriqueMaillage criSurf = reqMetrique();

   GOCoordonneesXYZ p = p1P->reqPosition() - p0P->reqPosition();
   p = criSurf.transformeDirecte(p);
   p = criRota.transformeInverse(p);
   p = criSurf.transformeInverse(p);
   p += pP->reqPosition();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return p;
}

//************************************************************************
// Sommaire: Demande si un sommet peut bouger.
//
// Description:
//    La méthode publique <code>sommetPeutBouger()</code> permet de
//    demander si le sommet peut être bougé.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GGDelaunaySurface::sommetPeutBouger(GGDelaunaySommetP sP,
                                            const GOCoordonneesXYZ& p) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   PRECONDITION(sP == sommets[0] || sP == sommets[1] || sP == sommets[2]);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const GGMetriqueMaillage& c0 = sommets[0]->reqMetrique();
   const GGMetriqueMaillage& c1 = sommets[1]->reqMetrique();
   const GGMetriqueMaillage& c2 = sommets[2]->reqMetrique();
   const GOCoordonneesXYZ&   p0 = sommets[0]->reqPosition();
   const GOCoordonneesXYZ&   p1 = sommets[1]->reqPosition();
   const GOCoordonneesXYZ&   p2 = sommets[2]->reqPosition();
   const DReel aactu  = aire;
   const DReel acible = GGDelaunaySurface::surfaceCible;
   DReel   a;
   Booleen canMove;

   if (sP == sommets[0])
      a = GGDelaunaySurface::reqAireMetrique(c0, p, c1, p1, c2, p2);
   else if (sP == sommets[1])
      a = GGDelaunaySurface::reqAireMetrique(c0, p0, c1, p, c2, p2);
   else
      a = GGDelaunaySurface::reqAireMetrique(c0, p0, c1, p1, c2, p);
   canMove = (a > 1.0e-6 && a < aactu && (acible / a) < TOLERANCE_DEFORMATION) ||
             (a > 1.0e-6 && a > aactu && (a / acible) < TOLERANCE_DEFORMATION);

   return canMove;
}

//************************************************************************
// Sommaire: Notifie qu'un sommet a bougé.
//
// Description:
//    La méthode publique <code>sommetABouge(..)</code> permet de notifier
//    la surface lorsque un de ses sommets a bougé.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunaySurface::sommetABouge(GGDelaunaySommetP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   mailleurP->retire(this);
   majAireMetrique();
   sommets[0]->surfaceABouge(this);
   sommets[1]->surfaceABouge(this);
   sommets[2]->surfaceABouge(this);
   mailleurP->ajoute(this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}
