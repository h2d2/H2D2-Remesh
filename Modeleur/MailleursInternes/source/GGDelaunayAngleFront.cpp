//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayAngleFront.h
// Classe: GGDelaunayAngleFront
//************************************************************************
// 08-04-98  Yves Secretan    Version initiale
// 21-01-99  Yves Secretan    Base le critère sur l'aire
//************************************************************************

#include "GGDelaunayAngleFront.h"
#include "GGDelaunaySommet.h"
#include "GOCoord3.h"

const DReel GGDelaunayAngleFront::ANGLE_MAX     = (170.0 * atan(1.0)) / 45.0;
const DReel GGDelaunayAngleFront::DEUX_PI       = 8.0 * atan(1.0);
const DReel GGDelaunayAngleFront::CRITERE_GRAND = 1.0e+80;

//************************************************************************
// Sommaire: Constructeur de l'objet.
//
// Description:
//    Le constructeur public <code>genereMaillage(...)</code> initialise l'angle
//    avec les arguments. Il calcule également le critère.
//
// Entrée:
//    GGDelaunaySommetP sCP      Pointeur au sommet de l'angle
//    GGDelaunaySommetP sGP      Pointeur au sommet gauche
//    GGDelaunaySommetP sDP      Pointeur au sommet droite
//    GGDelaunayAreteP  aGP      Pointeur à l'arête gauche
//    GGDelaunayAreteP  aDP      Pointeur à l'arête droite
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayAngleFront::GGDelaunayAngleFront(GGDelaunaySommetP sCP,
                                           GGDelaunaySommetP sGP, GGDelaunaySommetP sDP,
                                           GGDelaunayAreteP aGP, GGDelaunayAreteP aDP)
   :  sommetCentreP(sCP),
      sommetGaucheP(sGP), sommetDroiteP(sDP),
      areteGaucheP(aGP), areteDroiteP(aDP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sGP != NUL);
   PRECONDITION(sCP != NUL);
   PRECONDITION(sDP != NUL);
   PRECONDITION(aGP != NUL);
   PRECONDITION(aDP != NUL);
#endif  // ifdef MODE_DEBUG

   calculeCritere();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire: Destructeur de l'objet.
//
// Description:
//    Le destructeur public <code>~GGDelaunayAngleFront()</code> ne fait
//    rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GGDelaunayAngleFront::~GGDelaunayAngleFront()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire: Calcule le critère de l'angle du front
//
// Description:
//    La méthode privée <code>calculeCritere()</code> calcule le critère
//    de l'angle du front formé par le sommet de centre et ses voisins
//    gauche et droite.
//    <p>
//    Le calcul n'est valable qu'en 2D.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayAngleFront::calculeCritere()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   static DReel PI = atan(1.0)*4.0;

   GOCoordonneesXYZ GC = sommetGaucheP->reqPosition() - sommetCentreP->reqPosition();
   GOCoordonneesXYZ DC = sommetDroiteP->reqPosition() - sommetCentreP->reqPosition();

   critere = atan2(prodVect(DC, GC).z(), prodScal(DC, GC));
   if (critere <= 0.0) critere += 2*PI;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Change le voisin de droite.
//
// Description:
//    La méthode publique <code>changeVoisinDroite(...)</code> va changer
//    le sommet de droite et l'arête de droite. Le critère est recalculé.
//
// Entrée:
//    GGDelaunaySommetP sDP      Pointeur au nouveau sommet droite
//    GGDelaunayAreteP  aDP      Pointeur à la nouvelle arête droite
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayAngleFront::changeVoisinDroite(GGDelaunaySommetP sDP, GGDelaunayAreteP aDP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sDP != NUL);
   PRECONDITION(aDP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   sommetDroiteP = sDP;
   areteDroiteP = aDP;
   calculeCritere();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Change le voisin de gauche.
//
// Description:
//    La méthode publique <code>changeVoisinGauche(...)</code> va changer
//    le sommet de gauche et l'arête de gauche. Le critère est recalculé.
//
// Entrée:
//    GGDelaunaySommetP sGP      Pointeur au nouveau sommet gauche
//    GGDelaunayAreteP  aGP      Pointeur à la nouvelle arête gauche
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GGDelaunayAngleFront::changeVoisinGauche(GGDelaunaySommetP sGP, GGDelaunayAreteP aGP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sGP != NUL);
   PRECONDITION(aGP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   sommetGaucheP = sGP;
   areteGaucheP = aGP;
   calculeCritere();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

