//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayAngleFront.h
//
// Classe: GGDelaunayAngleFront
//
// Sommaire: Classe d'angle dans le front pour une génération de maillage de Delaunay.
//
// Description:
//    La classe <code>GGDelaunayAngleFront</code> représente un angle utilisé
//    dans un front de maillage pour un algorithme de génération de maillage
//    par une méthode de Delaunay. C'est une classe utilitaire de
//    <code>GGDelaunayPolygone</code>.
//
// Attributs:
//   GGDelaunaySommetP sommetCentreP;
//   GGDelaunaySommetP sommetGaucheP;
//   GGDelaunaySommetP sommetDroiteP;
//   GGDelaunayAreteP  areteGaucheP;
//   GGDelaunayAreteP  areteDroiteP;
//   DReel             angle;
//
// Notes:
//
//************************************************************************
// 08-04-98  Yves Secretan    Version initiale
// 28-10-98  Yves Secretan    Remplace #include "ererreur"
// 11-01-99  Yves Secretan    Ajoute constantes ANGLE_MAX et CRITERE_GRAND
//************************************************************************
#ifndef GGDELAUNAYANGLEFRONT_H_DEJA_INCLU
#define GGDELAUNAYANGLEFRONT_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunayArete);

DECLARE_CLASS(GGDelaunayAngleFront);

class GGDelaunayAngleFront
{
public:
           GGDelaunayAngleFront (GGDelaunaySommetP,
                                 GGDelaunaySommetP, GGDelaunaySommetP,
                                 GGDelaunayAreteP, GGDelaunayAreteP);
           ~GGDelaunayAngleFront();

   void    changeVoisinDroite   (GGDelaunaySommetP, GGDelaunayAreteP);
   void    changeVoisinGauche   (GGDelaunaySommetP, GGDelaunayAreteP);

   GGDelaunayAreteP  reqAreteGauche () const;
   GGDelaunayAreteP  reqAreteDroite () const;
   GGDelaunaySommetP reqSommetCentre() const;
   GGDelaunaySommetP reqSommetGauche() const;
   GGDelaunaySommetP reqSommetDroite() const;

   Booleen operator < (const GGDelaunayAngleFront&) const;

protected:
   void invariant(ConstCarP) const;

private:
   static const DReel ANGLE_MAX;
   static const DReel DEUX_PI;
   static const DReel CRITERE_GRAND;

   GGDelaunaySommetP sommetCentreP;
   GGDelaunaySommetP sommetGaucheP;
   GGDelaunaySommetP sommetDroiteP;
   GGDelaunayAreteP  areteGaucheP;
   GGDelaunayAreteP  areteDroiteP;
   DReel             critere;

   void calculeCritere();
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGDelaunayAngleFront::invariant(ConstCarP conditionP) const
{
   INVARIANT(sommetGaucheP != NUL, conditionP);
   INVARIANT(sommetCentreP != NUL, conditionP);
   INVARIANT(sommetDroiteP != NUL, conditionP);
   INVARIANT(sommetGaucheP != sommetCentreP, conditionP);
   INVARIANT(sommetGaucheP != sommetDroiteP, conditionP);
   INVARIANT(sommetCentreP != sommetDroiteP, conditionP);
   INVARIANT(areteGaucheP != NUL, conditionP);
   INVARIANT(areteDroiteP != NUL, conditionP);
   INVARIANT(areteGaucheP != areteDroiteP, conditionP);
}
#else
inline void GGDelaunayAngleFront::invariant(ConstCarP) const
{
}
#endif

#include "GGDelaunayAngleFront.hpp"

#endif   // #ifndef GGDELAUNAYANGLEFRONT_H_DEJA_INCLU
