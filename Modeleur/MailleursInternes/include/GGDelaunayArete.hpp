//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunayArete.hpp
// Classe:   GGDelaunayArete
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************
#ifndef GGDELAUNAYARETE_HPP_DEJA_INCLU
#define GGDELAUNAYARETE_HPP_DEJA_INCLU

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn -inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#include "GGDelaunayInfo.h"

//************************************************************************
// Sommaire:  Ajoute une référence à l'objet.
//
// Description:
//    La méthode publique <code>ajouteReference()</code> incrémente le compteur
//    de références. Le compteur permet de gérer la destruction automatique
//    de l'objet des que celui-ci n'est plus référencé. Il est donc important
//    que les objets qui utilisent le symbole s'enregistrent.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::ajouteReference()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ++compteRef;

#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Retourne VRAI si l'arête est liée aux sommets.
//
// Description:
//    La méthode public <code>aSommets(...)</code> retourne VRAI si les
//    sommets de l'objet et les sommets passés en argument sont identiques.
//
// Entrée:
//    GGDelaunaySommetP p0P      // les sommets
//    GGDelaunaySommetP p1P      //
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::aSommets(ConstGGDelaunaySommetP p0P,
                                         ConstGGDelaunaySommetP p1P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL);
   PRECONDITION(p1P != NUL);
   PRECONDITION(p0P != p1P);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen b = (sommets[0] == p0P && sommets[1] == p1P) ||
               (sommets[1] == p0P && sommets[0] == p1P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return b;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>asgEstSurPeau(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::asgEstSurPeau(bool b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   surPeau = b;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>asgRetournable(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::asgRetournable(bool b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   retournable = b;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>asgRaffinable(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::asgRaffinable(bool b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   raffinable = b;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>asgInfoClient(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::asgInfoClient(GGDelaunayInfoBaseP infoP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (infoClientP != NULL)
   {
      delete infoClientP;
      infoClientP = NULL;
   }
   if (infoP != NULL)
   {
      infoClientP = infoP->clone();
   }
}

//************************************************************************
// Sommaire:  Retourne VRAI si l'arête doit être déraffinée.
//
// Description:
//    La méthode publique <code>doitEtreDeraffinee()</code> retourne VRAI si
//    l'arête doit être déraffinée. Dans la métrique de l'arête, celle-ci
//    doit être déraffinée si sa longueur est inférieur à celle d'un coté de
//    triangle équilatéral sur le cercle unitaire.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::doitEtreDeraffinee() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel TOL = 0.7071067814;  // 1.0 / sqrt(2.0);
   bool aDeraffiner =  (niveauRaff > GGDelaunayArete::niveauRaffMin)
                    && (reqCritere() < TOL);
   return aDeraffiner;
}

//************************************************************************
// Sommaire:  Enlève une référence à l'objet.
//
// Description:
//    La méthode publique <code>enleveReference()</code> décrémente le
//    compteur de références. Lorsque celui-ci tombe à zéro, l'objet est
//    automatiquement détruis. Il est donc dangereux d'utiliser un pointeur
//    après un appel à <code>enleveReference()</code> car la mémoire de celui-ci
//    peut avoir été désallouée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::enleveReference()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   --compteRef;
   if (compteRef == 0)
      delete this;

#ifdef MODE_DEBUG
   // ---  Comme dans les destructeurs on ne peut tester les invariants
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Retourne VRAI si la surface est raffinable.
//
// Description:
//    La méthode publique <code>estRaffinable()</code> retourne VRAI si
//    l'arête est raffinable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::estRaffinable() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return raffinable;
}

//************************************************************************
// Sommaire:  Retourne VRAI si la surface est retournable.
//
// Description:
//    La méthode publique <code>estRetournable()</code> retourne VRAI si
//    l'arête est retournable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::estRetournable() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return retournable;
}

//************************************************************************
// Sommaire:  Retourne VRAI si la surface est retournable.
//
// Description:
//    La méthode publique <code>estRetournable()</code> retourne VRAI si
//    l'arête est retournable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::estSurPeau() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return surPeau;
}

//************************************************************************
// Sommaire:  Libère le raffinement l'arête.
//
// Description:
//    La méthode publique <code>libereRaffinement(...)</code> autorise ou
//    empêche le raffinement de l'arête.
//
// Entrée:
//    Booleen b         VRAI si l'arête peut être raffinée
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::libereRaffinement(Booleen b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   raffinable = b;
}

//************************************************************************
// Sommaire:  Libère le retournement de l'arête.
//
// Description:
//    La méthode publique <code>libereRetournement(...)</code> autorise ou
//    empêche le retournement de l'arête.
//
// Entrée:
//    Booleen b         VRAI si l'arête peut être retournée
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::libereRetournement(Booleen b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   retournable = b;
}

//************************************************************************
// Sommaire:  Retourne VRAI si l'arête est composée des deux sommets.
//
// Description:
//    La méthode statique publique <code>ontMemeSommets(...)</code> retourne
//    VRAI si les deux arêtes passées en arguments ont les mêmes sommets.
//
// Entrée:
//    GGDelaunayAreteP  a1P       // Arêtes
//    GGDelaunayAreteP  a2P
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::ontMemeSommets(ConstGGDelaunayAreteP a1P,
                                               ConstGGDelaunayAreteP a2P)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(a1P != NUL);
   PRECONDITION(a2P != NUL);
#endif   // ifdef MODE_DEBUG

   return a1P->aSommets(a2P->reqSommet0(), a2P->reqSommet1());
}

//************************************************************************
// Sommaire:  Retourne le second sommet
//
// Description:
//    La méthode publique <code>reqAutreSommet(...)</code> retourne le sommet
//    qui n'est pas égal au sommet passé en argument.
//
// Entrée:
//    GGDelaunaySommetP p0P      Premier sommet
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunayArete::reqAutreSommet(ConstGGDelaunaySommetP p0P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != NUL);
   PRECONDITION(p0P != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GGDelaunaySommetP pP = NUL;
   if (sommets[0] == p0P)
   {
      pP = sommets[1];
   }
   else if (sommets[1] == p0P)
   {
      pP = sommets[0];
   }

#ifdef MODE_DEBUG
   POSTCONDITION(pP != NUL);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return pP;
}

//************************************************************************
// Sommaire:  Retourne la seconde surface
//
// Description:
//    La méthode publique <code>reqAutreSurface(...)</code> retourne la surface
//    qui n'est pas égale à la surface passée en argument.
//
// Entrée:
//    ConstGGDelaunaySurfaceP sP      Première surface
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySurfaceP GGDelaunayArete::reqAutreSurface(ConstGGDelaunaySurfaceP s0P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(surfaces[0] != NUL);
   PRECONDITION(s0P != NUL);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GGDelaunaySurfaceP sP = NUL;
   if (surfaces[0] == s0P)
   {
      sP = surfaces[1];
   }
   else if (surfaces[1] == s0P)
   {
      sP = surfaces[0];
   }

#ifdef MODE_DEBUG
   POSTCONDITION(sP != NUL);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return sP;
}

//************************************************************************
// Sommaire:  Retourne le critère de raffinement l'arête.
//
// Description:
//    La méthode publique <code>reqCritere()</code> retourne le critère de
//    raffinement de l'arête, à savoir le rapport entre la longueur dans
//    la métrique de l'objet et la longueur de l'arête d'un triangle
//    équilatéral (sqrt(3)).
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Est-ce:
//       reqLongueurCible() * LONGUEUR_IDEALE)
//    ou juste:
//       reqLongueurCible()
//************************************************************************
inline DReel GGDelaunayArete::reqCritere() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (longueur / reqLongueurCible());
}

//************************************************************************
// Sommaire:  Retourne la longueur dans la métrique.
//
// Description:
//    La méthode publique <code>reqLongueurMetrique()</code> la longueur dans
//    la métrique de l'objet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
inline DReel GGDelaunayArete::reqLongueurMetrique() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return longueur;
}

//************************************************************************
// Sommaire:  Retourne le pointeur au sommet d'indice 0.
//
// Description:
//    La méthode publique <code>reqSommet0()</code> retourne le sommet
//    d'indice 0 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunayArete::reqSommet0() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets[0];
}

//************************************************************************
// Sommaire:  Retourne le pointeur au sommet d'indice 1.
//
// Description:
//    La méthode publique <code>reqSommet1()</code> retourne le sommet
//    d'indice 1 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunayArete::reqSommet1() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets[1];
}

//************************************************************************
// Sommaire:  Retourne le pointeur à la surface d'indice 0.
//
// Description:
//    La méthode publique <code>reqSurface0()</code> retourne la surface
//    d'indice 0 de l'arête.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySurfaceP GGDelaunayArete::reqSurface0() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return surfaces[0];
}

//************************************************************************
// Sommaire:  Retourne le pointeur à la surface d'indice 1.
//
// Description:
//    La méthode publique <code>reqSurface1()</code> retourne la surface
//    d'indice 1 de l'arête.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySurfaceP GGDelaunayArete::reqSurface1() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return surfaces[1];
}

//************************************************************************
// Sommaire:  Retourne l'arête.
//
// Description:
//    La méthode publique <code>retourne(...)</code> retourne l'arête en se
//    basant sur les deux surfaces auxquelles elle est liée. Ce retournement
//    n'a lieu que si l'arête n'est pas imposée. Si l'arête est retournée et
//    que le sommet pivot est non NUL, la méthode ajoute dans le setAretes les
//    arêtes opposées au sommet pivot. Ceci permet de propager le retournement
//    par rapport à un sommet.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//    GGContainerAreteARetourner& containerAretes     Container des arêtes à retourner
//    GGDelaunaySommetP           pivP                Sommet pivot
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::retourne(GGContainerAreteARetourner& containerAretes,
                                         GGDelaunaySommetP pivP,
                                         DReel swapTol)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return retourne(&containerAretes, pivP, swapTol);
}

//************************************************************************
// Sommaire:  Retourne l'arête.
//
// Description:
//    La méthode publique <code>retourne()</code> retourne l'arête en se
//    basant sur les deux surfaces auxquelles elle est liée. Ce retournement
//    n'a lieu que si l'arête n'est pas imposée.
//    <p>
//    La méthode appelante doit prendre une référence avant l'appel pour la
//    relâcher après l'appel.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayArete::retourne(DReel swapTol)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return retourne(NUL, NUL, swapTol);
}

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn .inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>reqInfoClient(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayInfoBaseP GGDelaunayArete::reqInfoClient() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return infoClientP;
}

//************************************************************************
// Sommaire:  Retourne le niveau de raffinement
//
// Description:
//    La méthode publique <code>reqNiveauRaff()</code> retourne le niveau
//    de raffinement par rapport au niveau initial de 0.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Entier GGDelaunayArete::reqNiveauRaff() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return niveauRaff;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode statique publique <code>asgLongueurCible()</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunayArete::asgLongueurCible(const DReel& l)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(l > 1.0e-7);
#endif   // ifdef MODE_DEBUG

   longueurCible = l;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Assigne les niveaux min et max de raffinemnet.
//
// Description:
//    La méthode statique publique <code>asgNiveauRaff()</code> assigne
//    les niveaux min et max de raffinement,
//
// Entrée:
// Entier   rMin     Niveau min
// Entier   rMax     Niveau max
//
// Sortie:
//
// Notes:
//************************************************************************
inline void GGDelaunayArete::asgNiveauRaff(Entier rMin, Entier rMax)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   niveauRaffMin = rMin;
   niveauRaffMax = rMax;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode statique publique <code>reqLongueurCible()</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline DReel GGDelaunayArete::reqLongueurCible()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   return longueurCible;
}

#endif   // #ifndef GGDELAUNAYARETE_HPP_DEJA_INCLU

