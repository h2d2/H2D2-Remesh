//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGMailleurDelaunay.hpp
// Classe:   GGMailleurDelaunay
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************
#ifndef GGMAILLEURDELAUNAY_HPP_DEJA_INCLU
#define GGMAILLEURDELAUNAY_HPP_DEJA_INCLU

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn -inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

//************************************************************************
// Sommaire:  Enregistre un sommet.
//
// Description:
//    La méthode publique <code>ajoute(...)</code> permet d'ajouter un sommet
//    à la liste des sommets maintenus par l'objet. L'objet ne prend pas
//    de références sur le sommet, il est uniquement notifié lors de la
//    construction et de la destruction.
//
// Entrée:
//    GGDelaunaySommetP sP    Sommet à enregistrer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGMailleurDelaunay::ajoute(GGDelaunaySommetP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   PRECONDITION(sommetsCtrl.find(sP) == sommetsCtrl.end());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   sommetsCtrl.insert(sP);
#endif   // ifdef MODE_DEBUG
   sommets.insert(sP);

#ifdef MODE_DEBUG
   POSTCONDITION(sommets.find(sP) != sommets.end());
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Enregistre une arête.
//
// Description:
//    La méthode publique <code>ajoute(...)</code> permet d'ajouter une arête
//    à la liste des arêtes maintenues par l'objet. L'objet ne prend pas
//    de références sur l'arête, il est uniquement notifié lors de la
//    construction et de la destruction.
//
// Entrée:
//    GGDelaunayAreteP aP    Arête à enregistrer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGMailleurDelaunay::ajoute(GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aP != NUL);
   PRECONDITION(aretesCtrl.find(aP) == aretesCtrl.end());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   aretesCtrl.insert(aP);
#endif   // ifdef MODE_DEBUG
   aretes.insert(aP);

#ifdef MODE_DEBUG
   POSTCONDITION(aretes.find(aP) != aretes.end());
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Enregistre une surface.
//
// Description:
//    La méthode publique <code>ajoute(...)</code> permet d'ajouter une surface
//    à la liste des surfaces maintenues par l'objet. L'objet ne prend pas
//    de références sur la surface, il est uniquement notifié lors de la
//    construction et de la destruction.
//
// Entrée:
//    GGDelaunaySurfaceP sP    Surface à enregistrer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGMailleurDelaunay::ajoute(GGDelaunaySurfaceP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   PRECONDITION(surfacesCtrl.find(sP) == surfacesCtrl.end());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   surfacesCtrl.insert(sP);
#endif   // ifdef MODE_DEBUG
   surfaces.insert(sP);

#ifdef MODE_DEBUG
   POSTCONDITION(surfaces.find(sP) != surfaces.end());
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Retourne le nombre d'arêtes.
//
// Description:
//    La méthode publique <code>reqNbrAretes()</code> retourne le nombre
//    d'arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerArete::size_type
GGMailleurDelaunay::reqNbrAretes() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.size();
}

//************************************************************************
// Sommaire:  Retourne le nombre de sommets.
//
// Description:
//    La méthode publique <code>reqNbrSommets()</code> retourne le nombre
//    de sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerSommet::size_type
GGMailleurDelaunay::reqNbrSommets() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets.size();
}

//************************************************************************
// Sommaire:  Retourne le nombre de surfaces.
//
// Description:
//    La méthode publique <code>reqNbrSurfaces()</code> retourne le nombre
//    de surfaces.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerSurface::size_type
GGMailleurDelaunay::reqNbrSurfaces() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return surfaces.size();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur du début des arêtes.
//
// Description:
//    La méthode publique <code>reqAreteDebut()</code> retourne l'itérateur
//    du début des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerArete::const_iterator
GGMailleurDelaunay::reqAreteDebut() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.begin();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de fin des arêtes.
//
// Description:
//    La méthode publique <code>reqAreteDebut()</code> retourne l'itérateur
//    de fin des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerArete::const_iterator
GGMailleurDelaunay::reqAreteFin() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.end();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur du début des sommets.
//
// Description:
//    La méthode publique <code>reqSommetDebut()</code> retourne l'itérateur
//    du début des sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerSommet::const_iterator
GGMailleurDelaunay::reqSommetDebut() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets.begin();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de fin des sommets.
//
// Description:
//    La méthode publique <code>reqSommetDebut()</code> retourne l'itérateur
//    de fin des sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerSommet::const_iterator
GGMailleurDelaunay::reqSommetFin() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets.end();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur du début des surfaces.
//
// Description:
//    La méthode publique <code>reqSurfaceDebut()</code> retourne l'itérateur
//    du début des surfaces.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerSurface::const_iterator
GGMailleurDelaunay::reqSurfaceDebut() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return surfaces.begin();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de fin des surfaces.
//
// Description:
//    La méthode publique <code>reqSurfaceDebut()</code> retourne l'itérateur
//    de fin des surfaces.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGMailleurDelaunay::TCContainerSurface::const_iterator
GGMailleurDelaunay::reqSurfaceFin() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return surfaces.end();
}

//************************************************************************
// Sommaire:  Retire un sommet.
//
// Description:
//    La méthode publique <code>retire(...)</code> retire le sommet passé en
//    argument de la liste des sommets maintenue par l'objet.
//
// Entrée:
//    GGDelaunaySommetP sP    Sommet à retirer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGMailleurDelaunay::retire(GGDelaunaySommetP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   PRECONDITION(sommetsCtrl.find(sP) != sommetsCtrl.end());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   sommetsCtrl.erase(sP);
#endif   // ifdef MODE_DEBUG
   sommets.erase(sP);

#ifdef MODE_DEBUG
   POSTCONDITION(sommets.find(sP) == sommets.end());
   POSTCONDITION(sommetsCtrl.find(sP) == sommetsCtrl.end());
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Retire une arête.
//
// Description:
//    La méthode publique <code>retire(...)</code> retire l'arête passée en
//    argument de la liste des arêtes maintenue par l'objet.
//
// Entrée:
//    GGDelaunayAreteP aP    Arête à retirer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGMailleurDelaunay::retire(GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(aP != NUL);
   PRECONDITION(aretesCtrl.find(aP) != aretesCtrl.end());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   aretesCtrl.erase(aP);
#endif   // ifdef MODE_DEBUG
   aretes.erase(aP);

#ifdef MODE_DEBUG
   POSTCONDITION(aretes.find(aP) == aretes.end());
   POSTCONDITION(aretesCtrl.find(aP) == aretesCtrl.end());
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  Retire une surface.
//
// Description:
//    La méthode publique <code>retire(...)</code> retire la surface passée en
//    argument de la liste des surfaces maintenue par l'objet.
//
// Entrée:
//    GGDelaunaySurfaceP sP    Surface à retirer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGMailleurDelaunay::retire(GGDelaunaySurfaceP sP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sP != NUL);
   PRECONDITION(surfacesCtrl.find(sP) != surfacesCtrl.end());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   surfacesCtrl.erase(sP);
#endif   // ifdef MODE_DEBUG
   surfaces.erase(sP);

#ifdef MODE_DEBUG
   POSTCONDITION(surfaces.find(sP) == surfaces.end());
   POSTCONDITION(surfacesCtrl.find(sP) == surfacesCtrl.end());
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn .inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#endif   // #ifndef GGMAILLEURDELAUNAY_HPP_DEJA_INCLU

