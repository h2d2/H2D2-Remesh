//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGMetriqueMaillage.h
//
// Classe: GGMetriqueMaillage
//
// Sommaire:  Classe de métrique de maillage.
//
// Description:
//    La classe <code>GGMetriqueMaillage</code> implante la notion de métrique
//    utilisée dans la génération de maillage. Cette métrique se laisse
//    représenter sous la forme d'une ellipse.
//    <p>
//    Le grand axe et le petit axe correspondent aux rayons de l'ellipse.
//    L'angle est spécifié en radians.
//
// Attributs:
//   DReel grandAxe     // Paramètres de l'ellipse
//   DReel petitAxe
//   DReel angle
//
// Notes:
//
//************************************************************************
// 05-12-96  Yves Secretan    Version originale
// 28-10-98  Yves Secretan    Remplace #include "ererreur"
//************************************************************************
#ifndef GGMETRIQUEMAILLAGE_H_DEJA_INCLU
#define GGMETRIQUEMAILLAGE_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"

#include "GOCoord3.hf"
#include "GOEllipseErreur.h"
DECLARE_CLASS(GGMetriqueMaillage);

class DLL_IMPEXP(MAILLEURS_INTERNES) GGMetriqueMaillage
   : public GOEllipseErreur
{
public:
         GGMetriqueMaillage(DReel = 1.0, DReel = 1.0, DReel = 0.0);
         GGMetriqueMaillage(const GOEllipseErreur&);
         ~GGMetriqueMaillage();

   DReel calculeLongueurDirecte(const GOCoordonneesXYZ&, const GOCoordonneesXYZ&) const;
   DReel calculeLongueurInverse(const GOCoordonneesXYZ&, const GOCoordonneesXYZ&) const;
   DReel reqDetJ        () const;

   GGMetriqueMaillage reqInverse() const;
   GOCoordonneesXYZ   transformeDirecte(const GOCoordonneesXYZ&) const;
   GOCoordonneesXYZ   transformeInverse(const GOCoordonneesXYZ&) const;

protected:
   void invariant (ConstCarP) const;
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGMetriqueMaillage::invariant(ConstCarP conditionP) const
{
   GOEllipseErreur::invariant(conditionP);
}
#else
inline void GGMetriqueMaillage::invariant(ConstCarP) const
{
}
#endif

#include "GGMetriqueMaillage.hpp"

#endif // ifndef GGMETRIQUEMAILLAGE_H_DEJA_INCLU

