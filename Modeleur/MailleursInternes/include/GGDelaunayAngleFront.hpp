//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayAngleFront.hpp
// Classe: GGDelaunayAngleFront
//************************************************************************
// 08-04-98  Yves Secretan    Version initiale
//************************************************************************
#ifndef GGDELAUNAYANGLEFRONT_HPP_DEJA_INCLU
#define GGDELAUNAYANGLEFRONT_HPP_DEJA_INCLU

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn -inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

//************************************************************************
// Sommaire: Opérateur <.
//
// Description:
//    L'opérateur public <code>operator<(...)</code> compare deux objets
//    sur la base de leur critère.
//
// Entrée:
//    const GGDelaunayAngleFront& o    Objet à comparer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunayAngleFront::operator < (const GGDelaunayAngleFront& o) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return (critere < o.critere);
}

//************************************************************************
// Sommaire: Retourne le sommet du centre.
//
// Description:
//    La méthode publique <code>reqSommetCentre()</code> retourne le pointeur
//    au sommet de centre.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunayAngleFront::reqSommetCentre() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return sommetCentreP;
}

//************************************************************************
// Sommaire: Retourne le sommet droite.
//
// Description:
//    La méthode publique <code>reqSommetDroite()</code> retourne le pointeur
//    au sommet de droite.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunayAngleFront::reqSommetDroite() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return sommetDroiteP;
}

//************************************************************************
// Sommaire: Retourne le sommet gauche.
//
// Description:
//    La méthode publique <code>reqSommetGauche()</code> retourne le pointeur
//    au sommet de gauche.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunayAngleFront::reqSommetGauche() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return sommetGaucheP;
}

//************************************************************************
// Sommaire: Retourne l'arête droite.
//
// Description:
//    La méthode publique <code>reqAreteDroite()</code> retourne le pointeur
//    à l'arête droite.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayAreteP GGDelaunayAngleFront::reqAreteDroite() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return areteDroiteP;
}

//************************************************************************
// Sommaire: Retourne l'arête gauche.
//
// Description:
//    La méthode publique <code>reqAreteGauche()</code> retourne le pointeur
//    à l'arête gauche.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayAreteP GGDelaunayAngleFront::reqAreteGauche() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return areteGaucheP;
}

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn .inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#endif   // #ifndef GGDELAUNAYANGLEFRONT_HPP_DEJA_INCLU

