//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayOperation.h
//
// Classe: GGDelaunayOperation
//
// Sommaire: Classe d'opération de scénario pour un maillage de Delaunay.
//
// Description:
//    La classe <code>GGDelaunayOperation</code> représente une opération
//    de scénario de remaillage ou d'adaptation de maillage par une méthode
//    de Delaunay.
//    Un scénario est une suite d'opérations unitaires comme une
//    passe de raffinement ou de régularisation.
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef GGDELAUNAYOPERATION_H_DEJA_INCLU
#define GGDELAUNAYOPERATION_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"

DECLARE_CLASS(GGDelaunayOperation);

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayOperation
{
public:
   enum Type
   {
      INDEFINI,
      ETAT_INITIAL,
      ETAT_FINAL,
      MAILLE_PEAU,
      LISSE_METRIQUES,
      RESCALE_METRIQUES,
      RETOURNE_ARETES,
      SOIGNE_PEAU,
      SOIGNE_PONT,
      DERAFFINE_PAR_SOMMETS,
//      DERAFFINE_PAR_SURFACES,
      RAFFINE_PAR_ARETES,
      RAFFINE_PAR_SURFACES,
      REGULARISE_PAR_ARETES,
      REGULARISE_PAR_SURFACES,
      REGULARISE_BARYCENTRIQUE,
      REGULARISE_ETOILES
   };

         GGDelaunayOperation  () : type(INDEFINI) {}
         GGDelaunayOperation  (Type t) : type(t) {}
//         GGDelaunayOperation  (Type t, Entier i) : type(t), ival(i) {}
//         GGDelaunayOperation  (Type t, DReel r)  : type(t), rval(r) {}
         GGDelaunayOperation  (Type t, DReel r1, DReel r2) : type(t) {rval2[0]=r1, rval2[1]=r2;}
         ~GGDelaunayOperation () {}

   Type type;
   union
   {
//      Entier ival;
//      DReel  rval;
      DReel  rval2[2];
   };
};

#endif // ifndef GGDELAUNAYOPERATION_H_DEJA_INCLU
