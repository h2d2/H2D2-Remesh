//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunaySommet.h
//
// Classe: GGDelaunaySommet
//
// Sommaire: Classe de sommet pour une génération de maillage de Delaunay.
//
// Description:
//    La classe <code>GGDelaunaySommet</code> représente un sommet dans
//    un algorithme de génération de maillage par une méthode de Delaunay.
//    Le sommet porte la métrique du maillage.
//    <p>
//    Un sommet est lié à des arêtes et des surfaces. Il incorpore tous
//    les algorithmes basés sur les sommets comme le déraffinage et la
//    régularisation.
//
// Attributs:
//    Entier             compteRef     // Nombre de références à l'objet
//    GOCoordonneesXYZ   position      // Position du sommet
//    GGMetriqueMaillage metrique      // Métrique, critère de raffinement
//    TTSetArete         aretes        // Set des arêtes du sommet
//    TTSetSurface       surfaces      // Set des surfaces du sommet
//    Booleen            bougeable     // VRAI si le sommet est bougeable
//    Booleen            deraffinable  // VRAi si le sommet est déraffinable
//
// Notes:
//
//************************************************************************
// 08-03-93  Yves Secretan
// 28-10-98  Yves Secretan    Remplace #include "ererreur"
// 13-11-98  Yves Secretan    Ajoute regulariseBarycentrique
//************************************************************************
#ifndef GGDELAUNAYSOMMET_H_DEJA_INCLU
#define GGDELAUNAYSOMMET_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GGMetriqueMaillage.h"
#include "GOCoord3.h"
#include <vector>

DECLARE_CLASS(GGMailleurDelaunay);
DECLARE_CLASS(GGDelaunayArete);
DECLARE_CLASS(GGDelaunaySurface);
DECLARE_CLASS(GGDelaunayInfoBase);

DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunaySommetArguments);

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySommetArguments
{
public:
   Entier m_lvl;
   Booleen m_bou, m_der, m_peau;

   GGDelaunaySommetArguments()
      : m_lvl (0)
      , m_bou (VRAI)
      , m_der (VRAI)
      , m_peau(FAUX)
   {}

   GGDelaunaySommetArguments& niveauRaff  (Entier  i) { m_lvl = i; return *this; }
   GGDelaunaySommetArguments& bougeable   (Booleen b) { m_bou = b; return *this; }
   GGDelaunaySommetArguments& deraffinable(Booleen b) { m_der = b; return *this; }
   GGDelaunaySommetArguments& surPeau     (Booleen b) { m_peau= b; return *this; }
};

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySommet
{
public:
         GGDelaunaySommet  (GGMailleurDelaunayP,
                            const GOCoordonneesXYZ&,
                            const GGDelaunaySommetArguments& = GGDelaunaySommetArguments());
         GGDelaunaySommet  (GGMailleurDelaunayP,
                            const GOCoordonneesXYZ&,
                            const GGMetriqueMaillage&,
                            const GGDelaunaySommetArguments& = GGDelaunaySommetArguments());

   void  ajouteLienArete    (GGDelaunayAreteP);
   void  ajouteLienSurface  (GGDelaunaySurfaceP);
   void  ajouteReference    ();
   void  enleveLienArete    (GGDelaunayAreteP);
   void  enleveLienSurface  (GGDelaunaySurfaceP);
   void  enleveReference    ();
   ERMsg ctrlCompteReference() const;

   void                      asgBougeable           (bool);
   void                      asgDeraffinable        (bool);
   void                      asgEstSurPeau          (bool);
   void                      asgInfoClient          (GGDelaunayInfoBaseP);
   void                      asgMetrique            (const GGMetriqueMaillage&);
   Booleen                   aretePeutBouger        (GGDelaunayAreteP) const;
   void                      areteABouge            (GGDelaunayAreteP);
   void                      calculeCritere         ();
   ERMsg                     deraffine              (Booleen&);
   Booleen                   doitEtreDeraffine      () const;
   Booleen                   estBougeable           () const;
   Booleen                   estDeraffinable        () const;
   Booleen                   estSurPeau             () const;
   void                      regulariseBarycentrique();
   void                      regulariseEtoiles      ();
   void                      regulariseParAretes    (DReel = 1.0);
   void                      regulariseParSurfaces  (DReel = 1.0);
   GGDelaunayAreteP          reqArete               (EntierN) const;
   EntierN                   reqNbAretes            () const;
   GGDelaunayInfoBaseP       reqInfoClient          () const;
   DReel                     reqCritere             () const;
   Entier                    reqNiveauRaff          () const;
   const GGMetriqueMaillage& reqMetrique            () const;
   const GOCoordonneesXYZ&   reqPosition            () const;
   Booleen                   surfacePeutBouger      (GGDelaunaySurfaceP) const;
   void                      surfaceABouge          (GGDelaunaySurfaceP);

   static void         asgNiveauRaff   (Entier, Entier);

protected:
   void invariant (ConstCarP) const;

private:
   typedef std::vector<GGDelaunayAreteP>   TCContainerArete;
   typedef std::vector<GGDelaunaySurfaceP> TCContainerSurface;

   Entier              compteRef;
   Entier              niveauRaff;
   GGMailleurDelaunayP mailleurP;
   DReel               critere;
   GGMetriqueMaillage  metrique;
   GOCoordonneesXYZ    position;
   TCContainerArete    aretes;
   TCContainerSurface  surfaces;
   GGDelaunayInfoBaseP infoClientP;
   Booleen             bougeable;
   Booleen             deraffinable;
   Booleen             surPeau;

   static Entier       niveauRaffMin;
   static Entier       niveauRaffMax;

         GGDelaunaySommet  ();
         GGDelaunaySommet  (const GGDelaunaySommet&);
         ~GGDelaunaySommet ();

   ERMsg deraffineInterne  ();
   ERMsg deraffineFrontiere();
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGDelaunaySommet::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GGDelaunaySommet::invariant(ConstCarP) const
{
}
#endif

#include "GGDelaunaySommet.hpp"

#endif // ifndef GGDELAUNAYSOMMET_H_DEJA_INCLU
