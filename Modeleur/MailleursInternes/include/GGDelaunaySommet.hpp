//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunaySommet.hpp
// Classe:   GGDelaunaySommet
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************
#ifndef GGDELAUNAYSOMMET_HPP_DEJA_INCLU
#define GGDELAUNAYSOMMET_HPP_DEJA_INCLU

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn -inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#include "GGDelaunayInfo.h"

//************************************************************************
// Sommaire:  Ajoute une référence à l'objet.
//
// Description:
//    La méthode publique <code>ajouteReference()</code> incrémente le compteur
//    de références. Le compteur permet de gérer la destruction automatique
//    de l'objet des que celui-ci n'est plus référencé. Il est donc important
//    que les objets qui utilisent le symbole s'enregistrent.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::ajouteReference()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ++compteRef;

#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Assigne la métrique du sommet.
//
// Description:
//    La méthode publique <code>asgBougeable()</code> assigne au sommet son
//    état, à savoir s'il peut être bougé.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::asgBougeable(bool b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   bougeable = b;
}

//************************************************************************
// Sommaire:  Assigne la métrique du sommet.
//
// Description:
//    La méthode publique <code>asgDeraffinable()</code> assigne au sommet son
//    état, à savoir s'il peut être déraffiné.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::asgDeraffinable(bool b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   deraffinable = b;
}

//************************************************************************
// Sommaire:  Assigne la métrique du sommet.
//
// Description:
//    La méthode publique <code>asgDeraffinable()</code> assigne au sommet son
//    état, à savoir s'il peut être déraffiné.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::asgEstSurPeau(bool b)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   surPeau = b;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>asgInfoClient(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::asgInfoClient(GGDelaunayInfoBaseP infoP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   infoClientP = infoP->clone();
}

//************************************************************************
// Sommaire:  Assigne la métrique du sommet.
//
// Description:
//    La méthode publique <code>asgMetrique()</code> assigne au sommet la
//    métrique qui lui est associée. Cet appel, une fois toutes les métriques
//    assignées doit être suivi d'appels à calculeCritere pour remettre l'objet
//    dans un état cohérent.
//
// Entrée:
//    const GGMetriqueMaillage& m      La métrique
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::asgMetrique(const GGMetriqueMaillage& m)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   metrique = m;
}

//************************************************************************
// Sommaire:  VRAI si le sommet doit être déraffiné.
//
// Description:
//    La méthode publique <code>doitEtreDeraffine()</code> retourne VRAI si
//    le sommet doit être déraffiné.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Comme le critère est basé sur les arêtes, on utilise le même test
//    que l'arête.
//
//************************************************************************
inline Booleen GGDelaunaySommet::doitEtreDeraffine() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel TOL = 0.7071067814;  // 1.0 / sqrt(2.0);
   bool aDeraffiner =  (niveauRaff > GGDelaunaySommet::niveauRaffMin)
                    && (reqCritere() < TOL);
   return aDeraffiner;

}

//************************************************************************
// Sommaire:  Enlève une référence à l'objet.
//
// Description:
//    La méthode publique <code>enleveReference()</code> décrémente le
//    compteur de références. Lorsque celui-ci tombe à zéro, l'objet est
//    automatiquement détruis. Il est donc dangereux d'utiliser un pointeur
//    après un appel à <code>enleveReference()</code> car la mémoire de celui-ci
//    peut avoir été désallouée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::enleveReference()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   --compteRef;
   if (compteRef == 0)
      delete this;

#ifdef MODE_DEBUG
   // ---  Comme dans les destructeurs on ne peut tester les invariants
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  VRAI si le sommet est bougeable.
//
// Description:
//    La méthode publique <code>estBougeable()</code> retourne VRAI si le
//    sommet peut être bougé.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunaySommet::estBougeable() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return bougeable;
}

//************************************************************************
// Sommaire:  VRAI si le sommet est déraffinable.
//
// Description:
//    La méthode publique <code>estBougeable()</code> retourne VRAI si le
//    sommet peut être déraffiné.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunaySommet::estDeraffinable() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return deraffinable;
}

//************************************************************************
// Sommaire:  VRAI si le sommet est sur la peau.
//
// Description:
//    La méthode publique <code>estSurPeau()</code> retourne VRAI si le
//    sommet est sur la peau.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunaySommet::estSurPeau() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return surPeau;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>reqInfoClient(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayInfoBaseP GGDelaunaySommet::reqInfoClient() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return infoClientP;
}

//************************************************************************
// Sommaire:  Retourne le critère de raffinement.
//
// Description:
//    La méthode publique <code>reqCritere()</code> retourne la critère de
//    raffinement.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline DReel GGDelaunaySommet::reqCritere() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return critere;
}

//************************************************************************
// Sommaire:  Retourne la métrique de raffinement du sommet.
//
// Description:
//    La méthode publique <code>reqMetrique()</code> retourne la métrique
//    du critère de raffinement du sommet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline const GGMetriqueMaillage& GGDelaunaySommet::reqMetrique() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return metrique;
}

//************************************************************************
// Sommaire:  Retourne le nombre d'arêtes du sommet.
//
// Description:
//    La méthode publique <code>reqNbAretes()</code> retourne le nombre
//    d'arêtes connectées sur le sommet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline EntierN GGDelaunaySommet::reqNbAretes() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return static_cast<EntierN>(aretes.size());
}

//************************************************************************
// Sommaire:  Retourne le niveau de raffinement
//
// Description:
//    La méthode publique <code>reqNiveauRaff()</code> retourne le niveau
//    de raffinement par rapport au niveau initial de 0.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Entier GGDelaunaySommet::reqNiveauRaff() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return niveauRaff;
}

//************************************************************************
// Sommaire:  Retourne la position du sommet.
//
// Description:
//    La méthode publique <code>reqPosition()</code> retourne la position du
//    sommet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline const GOCoordonneesXYZ& GGDelaunaySommet::reqPosition() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return position;
}

//************************************************************************
// Sommaire: Demande si une surface peut bouger.
//
// Description:
//    La méthode publique <code>surfacePeutBouger()</code> permet de
//    demander si la surface peut être bougée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunaySommet::surfacePeutBouger(GGDelaunaySurfaceP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return VRAI;
}

//************************************************************************
// Sommaire: Notifie qu'une surface a bougé.
//
// Description:
//    La méthode publique <code>surfaceABouge()</code> permet de notifier
//    le sommet lorsqu'une de ses surfaces a bougé.
//    <p>
//    La méthode ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySommet::surfaceABouge(GGDelaunaySurfaceP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return;
}

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn .inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

//************************************************************************
// Sommaire:  Assigne les niveaux min et max de raffinemnet.
//
// Description:
//    La méthode statique publique <code>asgNiveauRaff()</code> assigne
//    les niveaux min et max de raffinement,
//
// Entrée:
// Entier   rMin     Niveau min
// Entier   rMax     Niveau max
//
// Sortie:
//
// Notes:
//************************************************************************
inline void GGDelaunaySommet::asgNiveauRaff(Entier rMin, Entier rMax)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   niveauRaffMin = rMin;
   niveauRaffMax = rMax;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
}

#endif   // #ifndef GGDELAUNAYSOMMET_HPP_DEJA_INCLU

