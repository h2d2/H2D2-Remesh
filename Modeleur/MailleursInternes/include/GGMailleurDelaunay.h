//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGMailleurDelaunay.h
//
// Classe: GGMailleurDelaunay
//
// Sommaire: Classe de génération de maillage de Delaunay.
//
// Description:
//    La classe <code>GGMailleurDelaunay</code> implante un algorithme
//    de génération de maillage par une méthode de Delaunay en se basant
//    sur une carte de métrique.
//
// Attributs:
//    vector<GGDelaunaySommetP> sommetsPeau;
//    TCContainerSommet  sommets;
//    TCContainerArete   aretes;
//    TCContainerSurface surfaces;
//
// Notes:
//    A la construction, les sommets, arêtes et surfaces vont directement
//    se lier à leurs voisins. De plus, les arêtes vont demander à leurs
//    sommets de se lier à elles et les surfaces vont demander à leurs arêtes
//    et sommet de se lier à elles. Ainsi, dès la fin de la construction tous
//    les liens sont faits. Les seules entités qui peuvent survivre indépendamment
//    sont les sommets; les arêtes sont obligatoirement liées à des sommets
//    et les surfaces à des arêtes et des sommets. Du fait de ces liens
//    bidirectionnels, la destruction doit se faire en ordre inverse de
//    construction donc d'abord les surfaces, puis les arêtes et enfin les
//    sommets. De toute manière les liens maintenus interdisent de procéder
//    dans un autre ordre. Il faut par contre détacher les surfaces de leurs
//    voisins afin de les libérer et rendre leur destruction possible.
//
//************************************************************************
// 24-04-98  Yves Secretan    Version initiale
// 28-10-98  Yves Secretan    Remplace #include "ererreur"
// 13-11-98  Yves Secretan    Ajoute regulariseBarycentrique
//************************************************************************
#ifndef GGMAILLEURDELAUNAY_H_DEJA_INCLU
#define GGMAILLEURDELAUNAY_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GGDelaunayOperation.h"
#include <set>
#include <vector>

#include "GOCoord3.hf"
//#include "GOEllipseErreur.hf"
//#include "SRChamp.hf"
#include "SRChampEFEllipseErreur.hf"
#include "GOPolygone.hf"
DECLARE_CLASS(GGDelaunayAvancement);
DECLARE_CLASS(GGDelaunayInfoBase);
DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunayArete);
DECLARE_CLASS(GGDelaunaySurface);
DECLARE_CLASS(GGDelaunayScenario);
DECLARE_CLASS(GGMetriqueMaillage);
DECLARE_CLASS(GGMailleurDelaunay);


class DLL_IMPEXP(MAILLEURS_INTERNES) GGMailleurDelaunay
{
public:
//   typedef SRChamp<GOEllipseErreur, GOCoordonneesXYZ> TCChampErr;
   typedef SRChampEFEllipseErreur TCChampErr;

   struct DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayAreteEstSup
   {
      Booleen operator()(GGDelaunayAreteP, GGDelaunayAreteP) const;
   };
   struct DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySommetEstSup
   {
      Booleen operator()(GGDelaunaySommetP, GGDelaunaySommetP) const;
   };
   struct DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySurfaceEstSup
   {
      Booleen operator()(GGDelaunaySurfaceP, GGDelaunaySurfaceP) const;
   };
   typedef std::set<GGDelaunayAreteP,   GGDelaunayAreteEstSup>   TCContainerArete;
   typedef std::set<GGDelaunaySommetP,  GGDelaunaySommetEstSup>  TCContainerSommet;
   typedef std::set<GGDelaunaySurfaceP, GGDelaunaySurfaceEstSup> TCContainerSurface;

#ifdef MODE_DEBUG
   struct DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayAreteExiste
   {
      Booleen operator()(GGDelaunayAreteP, GGDelaunayAreteP) const;
   };
   struct DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySommetExiste
   {
      Booleen operator()(GGDelaunaySommetP, GGDelaunaySommetP) const;
   };
   struct DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySurfaceExiste
   {
      Booleen operator()(GGDelaunaySurfaceP, GGDelaunaySurfaceP) const;
   };
   typedef std::set<GGDelaunayAreteP,   GGDelaunayAreteExiste>   TCControlerArete;
   typedef std::set<GGDelaunaySommetP,  GGDelaunaySommetExiste>  TCControlerSommet;
   typedef std::set<GGDelaunaySurfaceP, GGDelaunaySurfaceExiste> TCControlerSurface;
#endif   // MODE_DEBUG

public:
                      GGMailleurDelaunay ();
                      ~GGMailleurDelaunay();

   GGDelaunayAreteP   ajouteArete     (GGDelaunaySommetP, GGDelaunaySommetP, Booleen, Booleen, Booleen);
   GGDelaunaySommetP  ajouteSommet    (const GOCoordonneesXYZ&, Booleen, Booleen, Booleen);
   GGDelaunaySommetP  ajouteSommetPeau(const GOCoordonneesXYZ&, Booleen, Booleen, Booleen = VRAI);
   GGDelaunaySurfaceP ajouteSurface   (GGDelaunaySommetP, GGDelaunaySommetP, GGDelaunaySommetP,
                                       GGDelaunayAreteP, GGDelaunayAreteP, GGDelaunayAreteP);
   ERMsg              asgCarteMetrique(const TCChampErr&);
   ERMsg              ajtCarteMetrique(const TCChampErr&);
   GOEllipseErreur    reqMetrique     (const GOCoordonneesXYZ&) const;
   ERMsg              appliqueScenario(GGDelaunayAvancement&, const GGDelaunayScenario&,
                                       Entier = -1, DReel = 1.0, Entier = -2, Entier = +2);
   ERMsg              enleveReferencesAjouteSommet();

//   GGDelaunayAreteP   bloqueAretesPeau();
//   GGDelaunayAreteP   bloqueSommetsPeau();
//   GGDelaunayAreteP   bloquePeau();

   void  ajoute (GGDelaunaySommetP);
   void  ajoute (GGDelaunayAreteP);
   void  ajoute (GGDelaunaySurfaceP);
   void  retire (GGDelaunaySommetP);
   void  retire (GGDelaunayAreteP);
   void  retire (GGDelaunaySurfaceP);

   TCContainerArete::size_type        reqNbrAretes   () const;
   TCContainerSommet::size_type       reqNbrSommets  () const;
   TCContainerSurface::size_type      reqNbrSurfaces () const;

   TCContainerArete::const_iterator   reqAreteDebut  () const;
   TCContainerArete::const_iterator   reqAreteFin    () const;
   TCContainerSommet::const_iterator  reqSommetDebut () const;
   TCContainerSommet::const_iterator  reqSommetFin   () const;
   TCContainerSurface::const_iterator reqSurfaceDebut() const;
   TCContainerSurface::const_iterator reqSurfaceFin  () const;

protected:
   void invariant (ConstCarP) const;

private:
   typedef std::vector<GGDelaunaySommetP> TCContainerSommetAssigne;
   
   TCChampErr*              carteMetriquesP;
   TCContainerSommetAssigne sommetsAssignes;
   TCContainerArete         aretes;
   TCContainerSommet        sommets;
   TCContainerSurface       surfaces;
#ifdef MODE_DEBUG
   TCControlerArete         aretesCtrl;
   TCControlerSommet        sommetsCtrl;
   TCControlerSurface       surfacesCtrl;
#endif // MODE_DEBUG   

   ERMsg asgMetriques            ();
   ERMsg rehashMetriques         ();
   ERMsg ctrlCompteReference     (Entier) const;
   ERMsg lisseMetriques          ();
   ERMsg rescaleMetriques        ();
   ERMsg deraffineParSommets     ();
   ERMsg deraffineParSurfaces    ();
   ERMsg genereMaillagePeau      ();
   ERMsg raffineParAretes        (TCContainerSommet::size_type);
   ERMsg raffineParSurfaces      (TCContainerSommet::size_type);
   ERMsg regulariseBarycentrique ();
   ERMsg regulariseEtoiles       ();
   ERMsg regulariseParAretes     ();
   ERMsg regulariseParSurfaces   ();
   ERMsg retourneAretes          ();
   ERMsg soignePeau              ();
   ERMsg soignePont              ();
   ERMsg remplisInfo             (GGDelaunayOperation::Type, GGDelaunayAvancement&) const;
   
   void vide                    ();
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGMailleurDelaunay::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GGMailleurDelaunay::invariant(ConstCarP) const
{
}
#endif

#include "GGMailleurDelaunay.hpp"

#endif   // #ifndef GGMAILLEURDELAUNAY_H_DEJA_INCLU

