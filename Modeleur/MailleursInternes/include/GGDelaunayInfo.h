//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayInfo.h
//
// Classe: GGDelaunayInfoBase
//         GGDelaunayInfoNull
//         GGDelaunayInfoSommet
//         GGDelaunayInfoArete
//         GGDelaunayInfoSurface
//
// Sommaire:
//
// Description:
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef GGDELAUNAYINFO_H_DEJA_INCLU
#define GGDELAUNAYINFO_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunayArete);
DECLARE_CLASS(GGDelaunayInfoBase);
DECLARE_CLASS(GGDelaunayInfoNull);
DECLARE_CLASS(GGDelaunayInfoSommet);
DECLARE_CLASS(GGDelaunayInfoArete);
DECLARE_CLASS(GGDelaunayInfoSurface);

//=============================================================================
//
//=============================================================================
class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayInfoBase
{
public:
            GGDelaunayInfoBase() {};
            virtual ~GGDelaunayInfoBase() {};

   virtual GGDelaunayInfoBaseP clone() const = 0;

protected:
   void invariant (ConstCarP) const;
};

//=============================================================================
//
//=============================================================================
class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayInfoNull : public GGDelaunayInfoBase
{
public:
   static GGDelaunayInfoNull INFO_NIL;

   virtual ~GGDelaunayInfoNull() {};

   virtual GGDelaunayInfoNullP clone() const { return &INFO_NIL; } ;

private:
            GGDelaunayInfoNull() {};
};

//=============================================================================
//
//=============================================================================
class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayInfoSommet : public GGDelaunayInfoBase
{
public:
            GGDelaunayInfoSommet  () : noNoeud(0) {};
   virtual ~GGDelaunayInfoSommet  () {};

   virtual GGDelaunayInfoSommetP clone() const
   {
      return new GGDelaunayInfoSommet(*this);
   };

   void   asgNoNoeud (Entier i) { noNoeud = i; };
   Entier reqNoNoeud () const   {return noNoeud; };

protected:
   void invariant (ConstCarP) const;

private:
            GGDelaunayInfoSommet (const GGDelaunayInfoSommet& o) : GGDelaunayInfoBase(o), noNoeud(o.noNoeud) {};

   Entier noNoeud;
};


//=============================================================================
//
//=============================================================================
class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayInfoArete : public GGDelaunayInfoBase
{
public:
            GGDelaunayInfoArete () : crc32(0), noNoeud(-1) {};
   virtual ~GGDelaunayInfoArete () {};

   virtual GGDelaunayInfoAreteP clone() const 
   {
      return new GGDelaunayInfoArete(*this);
   };

   void   asgCrc32   (void* i) { crc32 = i; };
   void   asgNoNoeud (Entier i) { noNoeud = i; };
   void*  reqCrc32   () const   {return crc32; };
   Entier reqNoNoeud () const   {return noNoeud; };

protected:
   void invariant (ConstCarP) const;

private:
            GGDelaunayInfoArete (const GGDelaunayInfoArete& o) : GGDelaunayInfoBase(o), crc32(o.crc32), noNoeud(o.noNoeud) {};
//            GGDelaunayInfoArete  (const GGDelaunayArete& o) : crc32(o.crc32), noNoeud(o.noNoeud) {};

   void*  crc32;
   Entier noNoeud;
};

//=============================================================================
//
//=============================================================================
class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayInfoSurface : public GGDelaunayInfoBase
{
public:
   GGDelaunayInfoSurface() : noElem(0) {};
   virtual ~GGDelaunayInfoSurface() {};

   virtual GGDelaunayInfoSurfaceP clone() const
   {
      return new GGDelaunayInfoSurface(*this);
   };

   void   asgNoElem(Entier i) { noElem = i; };
   Entier reqNoElem() const { return noElem; };

protected:
   void invariant(ConstCarP) const;

private:
   GGDelaunayInfoSurface(const GGDelaunayInfoSurface& o) : GGDelaunayInfoBase(o), noElem(o.noElem) {};

   Entier noElem;
};


//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGDelaunayInfoBase::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GGDelaunayInfoBase::invariant(ConstCarP) const
{
}
#endif

//#include "GGDelaunayInfoBase.hpp"

#endif // ifndef GGDELAUNAYINFO_H_DEJA_INCLU

