//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayAvancement.h
//
// Classe: GGDelaunayAvancement
//
// Sommaire: Classe de suivi d'avancement d'adaptation.
//
// Description:
//    La classe <code>GGDelaunayAvancement</code>
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef GGDELAUNAYAVANCEMENT_H_DEJA_INCLU
#define GGDELAUNAYAVANCEMENT_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

DECLARE_CLASS(GGDelaunayAvancement);

class GGDelaunayAvancement
{
public:
   long int operation;
   long int nbrSommets;
   long int nbrSurfaces;
   double   qualite;
   double   sommetCmin;
   double   sommetCmed;
   double   sommetCmax;
   double   areteCmin;
   double   areteCmed;
   double   areteCmax;

   virtual void operator() () { ; }
};

class GGDelaunayAvancementFct : public GGDelaunayAvancement
{
public:
   typedef void (*TTFuncPtr)(GGDelaunayAvancement&);

   GGDelaunayAvancementFct(TTFuncPtr cb) : m_cb(cb) { ; }
   void operator() () { return m_cb(*this); }

private:
   GGDelaunayAvancementFct();

   TTFuncPtr m_cb;
};


template <typename TTClass>
class GGDelaunayAvancementMth : public GGDelaunayAvancement
{
public:
   typedef void (TTClass:: *TTFuncPtr)(GGDelaunayAvancement&);

   GGDelaunayAvancementMth(TTClass* o, TTFuncPtr cb) : m_o(o), m_cb(cb) { ; }
   void operator() () { return (m_o->*m_cb)(*this); }

private:
   GGDelaunayAvancementMth();

   TTClass  *m_o;
   TTFuncPtr m_cb;
};

#endif   // #ifndef GGDELAUNAYAVANCEMENT_H_DEJA_INCLU

