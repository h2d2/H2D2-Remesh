//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGMetriqueMaillage.cpp
// Classe:   GGMetriqueMaillage
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************
#ifndef GGMETRIQUEMAILLAGE_HPP_DEJA_INCLU
#define GGMETRIQUEMAILLAGE_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:  Retourne le déterminant du jacobien.
//
// Description:
//    La méthode publique <code>reqDetJ()</code> retourne le déterminant du
//    Jacobien de la transformation.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GGMetriqueMaillage::reqDetJ() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return 1.0 / (reqGrandAxe()*reqPetitAxe());
}

//************************************************************************
// Sommaire:  Retourne la métrique inverse.
//
// Description:
//    La méthode publique <code>reqInverse()</code> retourne la transformée
//    inverse à celle de l'objet.
//
// Entrée:
//
// Sortie:
//    GGMetriqueMaillage       // L'inverse
//
// Notes:
//
//************************************************************************
inline 
GGMetriqueMaillage GGMetriqueMaillage::reqInverse() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   DReel g = 1.0 / reqGrandAxe();
   DReel p = 1.0 / reqPetitAxe();
   DReel a = reqInclinaison();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return GGMetriqueMaillage(g, p, a);;
}

#endif // ifndef GGMETRIQUEMAILLAGE_HPP_DEJA_INCLU
