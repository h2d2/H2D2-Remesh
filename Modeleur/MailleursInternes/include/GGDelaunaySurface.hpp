//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGDelaunaySurface.hpp
// Classe:   GGDelaunaySurface
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************
#ifndef GGDELAUNAYSURFACE_HPP_DEJA_INCLU
#define GGDELAUNAYSURFACE_HPP_DEJA_INCLU

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn -inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#include "GGDelaunayInfo.h"

//************************************************************************
// Sommaire:  Retourne VRAI si l'arête est liée aux sommets.
//
// Description:
//    La méthode public <code>aSommets(...)</code> retourne VRAI si les
//    sommets de l'objet et les sommets passés en argument sont identiques.
//
// Entrée:
//    GGDelaunaySommetP p0P      // les sommets
//    GGDelaunaySommetP p1P      //
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunaySurface::aSommets(ConstGGDelaunaySommetP p0P,
                                           ConstGGDelaunaySommetP p1P,
                                           ConstGGDelaunaySommetP p2P) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(p0P != NUL && p1P != NUL && p2P != NUL);
   PRECONDITION(p0P != p1P && p0P != p2P && p1P != p2P);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Booleen b = ((sommets[0] == p0P && sommets[1] == p1P && sommets[2] == p2P) ||
                (sommets[0] == p0P && sommets[1] == p2P && sommets[2] == p1P) ||
                (sommets[0] == p1P && sommets[1] == p0P && sommets[2] == p2P) ||
                (sommets[0] == p1P && sommets[1] == p2P && sommets[2] == p0P) ||
                (sommets[0] == p2P && sommets[1] == p0P && sommets[2] == p1P) ||
                (sommets[0] == p2P && sommets[1] == p1P && sommets[2] == p0P));

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return b;
}

//************************************************************************
// Sommaire:  Ajoute une référence à l'objet.
//
// Description:
//    La méthode publique <code>ajouteReference()</code> incrémente le compteur
//    de références. Le compteur permet de gérer la destruction automatique
//    de l'objet des que celui-ci n'est plus référencé. Il est donc important
//    que les objets qui utilisent le symbole s'enregistrent.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySurface::ajouteReference()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ++compteRef;

#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>asgInfoClient(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySurface::asgInfoClient(GGDelaunayInfoBaseP infoP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (infoClientP != NULL)
   {
      delete infoClientP;
      infoClientP = NULL;
   }
   if (infoP != NULL)
   {
      infoClientP = infoP->clone();
   }
}

//************************************************************************
// Sommaire:  Retourne VRAI si la surface doit être raffinée.
//
// Description:
//    La méthode publique <code>doitEtreRaffinee()</code> retourne VRAI si
//    la surface doit être raffinée. Dans la métrique de la surface, celle-ci
//    doit être raffinée si son aire est supérieur à celle d'un triangle
//    équilatéral sur le cercle unitaire.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunaySurface::doitEtreRaffinee() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   bool aRafinner =  (niveauRaff < GGDelaunaySurface::niveauRaffMax)
                  && (reqCritere() > 1.0);
   return aRafinner;
}

//************************************************************************
// Sommaire:  Enlève une référence à l'objet.
//
// Description:
//    La méthode publique <code>enleveReference()</code> décrémente le
//    compteur de références. Lorsque celui-ci tombe à zéro, l'objet est
//    automatiquement détruis. Il est donc dangereux d'utiliser un pointeur
//    après un appel à <code>enleveReference()</code> car la mémoire de celui-ci
//    peut avoir été désallouée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySurface::enleveReference()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   --compteRef;
   if (compteRef == 0)
      delete this;

#ifdef MODE_DEBUG
   // ---  Comme dans les destructeurs on ne peut tester les invariants
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Retourne VRAI si l'arête est composée des deux sommets.
//
// Description:
//    La méthode statique publique <code>ontMemeSommets(...)</code> retourne
//    VRAI si les deux arêtes passées en arguments ont les mêmes sommets.
//
// Entrée:
//    GGDelaunayAreteP  a1P       // Arêtes
//    GGDelaunayAreteP  a2P
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen GGDelaunaySurface::ontMemeSommets(ConstGGDelaunaySurfaceP s1P,
                                                 ConstGGDelaunaySurfaceP s2P)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(s1P != NUL);
   PRECONDITION(s2P != NUL);
#endif   // ifdef MODE_DEBUG

   return s1P->aSommets(s2P->reqSommet0(), s2P->reqSommet1(), s2P->reqSommet2());
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>reqAireMetrique(...)</code> retourne l'aire
//    dans la métrique des sommets.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline DReel GGDelaunaySurface::reqAireMetrique() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aire;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode public <code>reqInfoClient(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayInfoBaseP GGDelaunaySurface::reqInfoClient() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(compteRef > 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return infoClientP;
}

//************************************************************************
// Sommaire:  Retourne le niveau de raffinement
//
// Description:
//    La méthode publique <code>reqNiveauRaff()</code> retourne le niveau
//    de raffinement par rapport au niveau initial de 0.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Entier GGDelaunaySurface::reqNiveauRaff() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return niveauRaff;
}

//************************************************************************
// Sommaire:  Retourne le pointeur à l'arête d'indice 0.
//
// Description:
//    La méthode publique <code>reqArete0()</code> retourne l'arête
//    d'indice 0 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayAreteP GGDelaunaySurface::reqArete0() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes[0];
}

//************************************************************************
// Sommaire:  Retourne le pointeur à l'arête d'indice 1.
//
// Description:
//    La méthode publique <code>reqArete1()</code> retourne l'arête
//    d'indice 1 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayAreteP GGDelaunaySurface::reqArete1() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes[1];
}

//************************************************************************
// Sommaire:  Retourne le pointeur à l'arête d'indice 2.
//
// Description:
//    La méthode publique <code>reqArete0()</code> retourne l'arête
//    d'indice 2 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunayAreteP GGDelaunaySurface::reqArete2() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes[2];
}

//************************************************************************
// Sommaire:  Retourne le pointeur au sommet d'indice 0.
//
// Description:
//    La méthode publique <code>reqSommet0()</code> retourne le sommet
//    d'indice 0 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunaySurface::reqSommet0() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets[0];
}

//************************************************************************
// Sommaire:  Retourne le pointeur au sommet d'indice 1.
//
// Description:
//    La méthode publique <code>reqSommet1()</code> retourne le sommet
//    d'indice 1 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunaySurface::reqSommet1() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets[1];
}

//************************************************************************
// Sommaire:  Retourne le pointeur au sommet d'indice 2.
//
// Description:
//    La méthode publique <code>reqSommet0()</code> retourne le sommet
//    d'indice 2 de la surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GGDelaunaySommetP GGDelaunaySurface::reqSommet2() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sommets[2];
}

//************************************************************************
// Sommaire:  Retourne le critère d'élongation de la surface.
//
// Description:
//    La méthode publique <code>reqCritereElong()</code> retourne le critère
//    d'élongation de la surface. Le critère est le rapport entre l'aire
//    du cercle inscrit et l'aire de triangle. On utilise une
//    formule équivalente faisant intervenir les longueurs des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline DReel GGDelaunaySurface::reqCritereElong() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sommets[0] != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return reqCritereElong(aretes[0], aretes[1], aretes[2]);
}

//************************************************************************
// Sommaire:  Retourne l'aire de la surface
//
// Description:
//    La méthode publique <code>reqCritere()</code> retourne le critère de
//    raffinement, à savoir le rapport entre l'aire de la surface calculée
//    dans la métrique de la surface et la surface idéale donnée par
//    un triangle equilatéral.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline DReel GGDelaunaySurface::reqCritere() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (aire / reqSurfaceCible());
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode publique <code>asgLongueurCible()</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGDelaunaySurface::asgLongueurCible(const DReel& l)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(l > 1.0e-7);
#endif   // ifdef MODE_DEBUG

   surfaceCible = l*l * sqrt(3.0) / 4;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Assigne les niveaux min et max de raffinemnet.
//
// Description:
//    La méthode statique publique <code>asgNiveauRaff()</code> assigne
//    les niveaux min et max de raffinement,
//
// Entrée:
// Entier   rMin     Niveau min
// Entier   rMax     Niveau max
//
// Sortie:
//
// Notes:
//************************************************************************
inline void GGDelaunaySurface::asgNiveauRaff(Entier rMin, Entier rMax)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   niveauRaffMin = rMin;
   niveauRaffMax = rMax;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode publique <code>reqSurfaceCible()</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline DReel GGDelaunaySurface::reqSurfaceCible()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   return surfaceCible;
}

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn .inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#endif   // #ifndef GGDELAUNAYSURFACE_HPP_DEJA_INCLU

