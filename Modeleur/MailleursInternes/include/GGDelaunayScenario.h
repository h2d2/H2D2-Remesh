//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayScenario.h
//
// Classe: GGDelaunayScenario
//
// Sommaire: Classe de scénario pour une génération de maillage de Delaunay.
//
// Description:
//    La classe <code>GGDelaunayScenario</code> représente un scénario de
//    ramaillage ou d'adaptation de maillage par une méthode de Delaunay.
//    Un scénarion est une suite d'opérations unitaires comme une
//    passe de raffinement ou de régularisation.
//
// Attributs:
//    TCContainerOperations operations       Container des opérations
//
// Notes:
//
//************************************************************************
// 08-03-93  Yves Secretan
// 28-10-98  Yves Secretan    Remplace #include "ererreur"
// 13-11-98  Yves Secretan    Ajoute REGULARISE_BARYCENTRIQUE
// 04-05-99  Yves Secretan    Supporte les stream du Standard
//************************************************************************
#ifndef GGDELAUNAYSCENARIO_H_DEJA_INCLU
#define GGDELAUNAYSCENARIO_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"

#include <vector>

#ifndef INRS_SANS_STREAMS_DU_STANDARD
#  include <iosfwd>
#else
#  include "syiosfwd.hf"
#endif   // INRS_SANS_STREAMS_DU_STANDARD

DECLARE_CLASS(GGDelaunayScenario);
DECLARE_CLASS(GGDelaunayOperation);

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayScenario
{
public:
   typedef std::vector<GGDelaunayOperation> TCContainerOperations;

         GGDelaunayScenario  ();
         ~GGDelaunayScenario ();

   void  ajouteOperation     (const GGDelaunayScenario&,  EntierN = 1);
   void  ajouteOperation     (const GGDelaunayOperation&, EntierN = 1);

   TCContainerOperations::const_iterator reqOperationDebut() const;
   TCContainerOperations::const_iterator reqOperationFin() const;

   friend INRS_IOS_STD::istream& operator >> (INRS_IOS_STD::istream&, GGDelaunayScenario&);

protected:
   void invariant(ConstCarP) const;

private:
   TCContainerOperations operations;

   GGDelaunayScenario(const GGDelaunayScenario&);
   GGDelaunayScenario& operator= (const GGDelaunayScenario&);
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGDelaunayScenario::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GGDelaunayScenario::invariant(ConstCarP) const
{
}
#endif

#endif // ifndef GGDELAUNAYSCENARIO_H_DEJA_INCLU

