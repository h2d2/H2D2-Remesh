//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayArete.h
//
// Classe: GGDelaunayArete
//
// Sommaire: Classe d'arête pour une génération de maillage de Delaunay.
//
// Description:
//    La classe <code>GGDelaunayArete</code> représente une arête dans
//    un algorithme de génération de maillage par une méthode de Delaunay.
//    <p>
//    L'arête maintient des liens avec les sommets et les surfaces. De plus
//    elle incorpore tous les algorithmes sur une arête, comme le retournement
//    ou le raffinement.
//
// Attributs:
//    Entier             compteRef        // Compteur de références
//    GGDelaunaySommetP  sommets[2]       // Sommets de l'arête
//    GGDelaunaySurfaceP surfaces[2]      // Surfaces de l'arête
//    Reel               longueur         // Longueur de l'arête
//
// Notes:
//
//************************************************************************
// 08-03-93  Yves Secretan
// 28-10-98  Yves Secretan      Remplace #include "ererreur"
//************************************************************************
#ifndef GGDELAUNAYARETE_H_DEJA_INCLU
#define GGDELAUNAYARETE_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOCoord3.hf"
DECLARE_CLASS(GGContainerAreteARetourner);
DECLARE_CLASS(GGDelaunayInfoBase);
DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunaySommetArguments);
DECLARE_CLASS(GGDelaunaySurface);
DECLARE_CLASS(GGMailleurDelaunay);
DECLARE_CLASS(GGMetriqueMaillage);
DECLARE_CLASS(GGDelaunayInfoBase);

DECLARE_CLASS(GGDelaunayArete);
DECLARE_CLASS(GGDelaunayAreteArguments);

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayAreteArguments
{
public:
   Entier  m_lvl;
   Booleen m_raff, m_swap, m_peau;

   GGDelaunayAreteArguments()
      : m_lvl(0)
      , m_raff(VRAI)
      , m_swap(VRAI)
      , m_peau(FAUX)
   {}

   GGDelaunayAreteArguments& niveauRaff  (Entier  i) { m_lvl  = i; return *this; }
   GGDelaunayAreteArguments& raffinable  (Booleen b) { m_raff = b; return *this; }
   GGDelaunayAreteArguments& retournable (Booleen b) { m_swap = b; return *this; }
   GGDelaunayAreteArguments& surPeau     (Booleen b) { m_peau = b; return *this; }
};

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayArete
{
public:
   static const DReel TOLERANCE_DEFORMATION;

            GGDelaunayArete  (GGMailleurDelaunayP,
                              GGDelaunaySommetP, GGDelaunaySommetP,
                              const GGDelaunayAreteArguments& = GGDelaunayAreteArguments());
            GGDelaunayArete  (GGDelaunaySommetP, GGDelaunaySommetP,
                              GGDelaunayAreteP,
                              const GGDelaunayAreteArguments& = GGDelaunayAreteArguments());
            GGDelaunayArete  (GGDelaunaySommetP, GGDelaunaySommetP,
                              GGDelaunayAreteP,  GGDelaunayAreteP,
                              const GGDelaunayAreteArguments& = GGDelaunayAreteArguments());

   void  ajouteLienSurface  (GGDelaunaySurfaceP);
   void  ajouteReference    ();
   void  enleveLienSurface  (GGDelaunaySurfaceP);
   void  enleveReference    ();
   ERMsg ctrlCompteReference() const;
   void  detache            ();

   Booleen             aSommets           (ConstGGDelaunaySommetP, ConstGGDelaunaySommetP) const;
   static Booleen      ontMemeSommets     (ConstGGDelaunayAreteP, ConstGGDelaunayAreteP);
   GGDelaunaySurfaceP  reqAutreSurface    (ConstGGDelaunaySurfaceP) const;
   GGDelaunaySommetP   reqAutreSommet     (ConstGGDelaunaySommetP) const;
   GGDelaunaySommetP   reqSommet0         () const;
   GGDelaunaySommetP   reqSommet1         () const;
   GGDelaunaySurfaceP  reqSurface0        () const;
   GGDelaunaySurfaceP  reqSurface1        () const;

   void                asgEstSurPeau      (bool);
   void                asgRaffinable      (bool);
   void                asgRetournable     (bool);
   void                asgInfoClient      (GGDelaunayInfoBaseP);
   void                majLongueurMetrique();
   Booleen             doitEtreRaffinee   () const;
   Booleen             doitEtreDeraffinee () const;
   Booleen             estRaffinable      () const;
   Booleen             estRetournable     () const;
   Booleen             estSurPeau         () const;
   void                libereRaffinement  (Booleen);
   void                libereRetournement (Booleen);
   Booleen             raffine            ();
   Booleen             regulariseEtoiles  (GGDelaunaySommetP);
   DReel               reqCritere         () const;
   GGDelaunayInfoBaseP reqInfoClient      () const;
   DReel               reqLongueurMetrique() const;
   Entier              reqNiveauRaff      () const;
   GOCoordonneesXYZ    reqPosiIdeale      (ConstGGDelaunaySommetP) const;
   Booleen             retourne           (DReel = 1.0e0);
   Booleen             retourne           (GGContainerAreteARetourner&, GGDelaunaySommetP = NUL, DReel = 1.0e0);
   Booleen             soignePont         ();
   Booleen             sommetPeutBouger   (GGDelaunaySommetP, const GOCoordonneesXYZ&) const;
   void                sommetABouge       (GGDelaunaySommetP);

   static void         asgLongueurCible   (const DReel&);
   static void         asgNiveauRaff      (Entier, Entier);
   static DReel        reqLongueurMetrique(const GGMetriqueMaillage&, const GOCoordonneesXYZ&, const GGMetriqueMaillage&, const GOCoordonneesXYZ&);
   static DReel        reqLongueurEuclide (ConstGGDelaunaySommetP, ConstGGDelaunaySommetP);
   static DReel        reqLongueurCible   ();

protected:
   void invariant (ConstCarP) const;

private:
   Entier              compteRef;
   Entier              niveauRaff;
   GGMailleurDelaunayP mailleurP;
   GGDelaunaySommetP   sommets[2];
   GGDelaunaySurfaceP  surfaces[3];
   DReel               longueur;
   GGDelaunayInfoBaseP infoClientP;
   Booleen             raffinable;
   Booleen             retournable;
   Booleen             surPeau;

   static DReel        longueurCible;
   static Entier       niveauRaffMin;
   static Entier       niveauRaffMax;

                     GGDelaunayArete   ();
                     GGDelaunayArete   (const GGDelaunayArete&);
                     ~GGDelaunayArete  ();
   GGDelaunaySommetP creeSommetMilieu  (const GGDelaunaySommetArguments&) const;
   Booleen           raffineFrontiere  ();
   Booleen           raffineInterne    ();
   Booleen           retourne          (GGContainerAreteARetourner*, GGDelaunaySommetP, DReel);
   static DReel      reqLongueurEuclide(const GOCoordonneesXYZ&, const GOCoordonneesXYZ&);
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGDelaunayArete::invariant(ConstCarP conditionP) const
{
   Booleen cnd0 = sommets[0] == NUL && sommets[1] == NUL;
   Booleen cnd1 = sommets[0] != NUL && sommets[1] != NUL && sommets[0] != sommets[1];
   INVARIANT(cnd0 || cnd1, conditionP);
   INVARIANT(surfaces[0] == NUL || surfaces[0] != surfaces[1], conditionP);
}
#else
inline void GGDelaunayArete::invariant(ConstCarP) const
{
}
#endif

#include "GGDelaunayArete.hpp"

#endif // ifndef GGDELAUNAYARETE_H_DEJA_INCLU

