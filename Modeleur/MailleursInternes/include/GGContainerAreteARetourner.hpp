//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GGContainerAreteARetourner.hpp
// Classe:   GGContainerAreteARetourner
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************
#ifndef GGCONTAINERARETEARETOURNER_HPP_DEJA_INCLU
#define GGCONTAINERARETEARETOURNER_HPP_DEJA_INCLU

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn -inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

//************************************************************************
// Sommaire:  Ajoute une arête à la liste.
//
// Description:
//    La méthode publique <code>ajouteSansControle(...)</code> ajoute
//    inconditionnellement à la liste l'arête passée en argument.
//
// Entrée:
//    GGDelaunayAreteP  aP          Pointeur à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline void GGContainerAreteARetourner::ajouteSansControle(GGDelaunayAreteP aP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   aretes.insert(aP);
}

//************************************************************************
// Sommaire:  Retourne l'itérateur du début des arêtes.
//
// Description:
//    La méthode publique <code>reqAreteDebut()</code> retourne l'itérateur
//    du début des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGContainerAreteARetourner::TCContainerArete::iterator
GGContainerAreteARetourner::reqAreteDebut()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.begin();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur du début des arêtes.
//
// Description:
//    La méthode publique <code>reqAreteDebut()</code> retourne l'itérateur
//    du début des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGContainerAreteARetourner::TCContainerArete::const_iterator
GGContainerAreteARetourner::reqAreteDebut() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.begin();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de fin des arêtes.
//
// Description:
//    La méthode publique <code>reqAreteDebut()</code> retourne l'itérateur
//    de fin des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGContainerAreteARetourner::TCContainerArete::iterator
GGContainerAreteARetourner::reqAreteFin()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.end();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de fin des arêtes.
//
// Description:
//    La méthode publique <code>reqAreteDebut()</code> retourne l'itérateur
//    de fin des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGContainerAreteARetourner::TCContainerArete::const_iterator
GGContainerAreteARetourner::reqAreteFin() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.end();
}

//************************************************************************
// Sommaire:  Retourne l'itérateur de fin des arêtes.
//
// Description:
//    La méthode publique <code>reqAreteDebut()</code> retourne l'itérateur
//    de fin des arêtes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
GGContainerAreteARetourner::TCContainerArete::size_type
GGContainerAreteARetourner::reqDimension() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return aretes.size();
}

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn .inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#endif  // #ifndef GGCONTAINERARETEARETOURNER_HPP_DEJA_INCLU
