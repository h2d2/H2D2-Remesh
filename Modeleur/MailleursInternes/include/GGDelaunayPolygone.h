//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunayPolygone.h
//
// Classe: GGDelaunayPolygone
//
// Sommaire: Classe de polygone pour une génération de maillage de Delaunay.
//
// Description:
//    La classe <code>GGDelaunayPolygone</code> représente un polygone pour la
//    génération de maillage par une méthode de Delaunay. Le polygone permet de
//    mailler un domaine polygonale sans ajouter de sommet.
//
// Attributs:
//    TCContainerSommet  sommets          Sommets du polygone
//    TCContainerArete   aretes           Arêtes du polygone
//
// Notes:
//
//************************************************************************
// 05-12-98  Yves Secretan      Version initiale
// 28-10-98  Yves Secretan      Remplace #include "ererreur"
// 21-01-99  Yves Secretan      Ajoute reqAire()
//************************************************************************
#ifndef GGDELAUNAYPOLYGONE_H_DEJA_INCLU
#define GGDELAUNAYPOLYGONE_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include <vector>

DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunayArete);
DECLARE_CLASS(GGDelaunaySurface);
DECLARE_CLASS(GGMailleurDelaunay);

DECLARE_CLASS(GGDelaunayPolygone);

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunayPolygone
{
public:
         GGDelaunayPolygone();
         ~GGDelaunayPolygone();

   void  ajouteArete   (GGDelaunayAreteP);
   void  ajouteSommet  (GGDelaunaySommetP);
   ERMsg genereMaillage(GGMailleurDelaunayP, Entier = 0);
   ERMsg retourneAretes();

protected:
   void  invariant     (ConstCarP) const;

private:
   typedef std::vector<GGDelaunaySommetP>  TCContainerSommet;
   typedef std::vector<GGDelaunayAreteP>   TCContainerArete;
   typedef std::vector<GGDelaunaySurfaceP> TCContainerSurface;
   TCContainerSommet  sommetsPeau;
   TCContainerArete   aretesPeau;
   TCContainerSommet  sommetsInternes;
   TCContainerArete   aretesInternes;
   TCContainerSurface surfacesInternes;

   ERMsg genereMaillagePeau  (GGMailleurDelaunayP, Entier);
   ERMsg genereMaillagePeau_I(GGMailleurDelaunayP, Entier);
   ERMsg genereMaillagePeau_E(GGMailleurDelaunayP, Entier);
   DReel reqAire             () const;
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGDelaunayPolygone::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GGDelaunayPolygone::invariant(ConstCarP) const
{
}
#endif

#endif   // #ifndef GGDELAUNAYPOLYGONE_H_DEJA_INCLU

