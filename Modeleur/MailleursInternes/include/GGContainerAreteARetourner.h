//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGContainerAreteARetourner.h
//
// Classe: GGContainerAreteARetourner
//
// Sommaire: Classe de container pour les arêtes à retourner.
//
// Description:
//    La classe <code>GGContainerAreteARetourner</code> est un container
//    pour les arêtes à retourner. A l'insertion on contrôle qu'il n'y a
//    pas de dédoublements.
//
// Attributs:
//    TCContainerArete aretes       Container pour les arêtes
//
// Notes:
//
//************************************************************************
// 08-03-93  Yves Secretan
// 28-10-98  Yves Secretan      Remplace #include "ererreur"
//************************************************************************
#ifndef GGCONTAINERARETEARETOURNER_H_DEJA_INCLU
#define GGCONTAINERARETEARETOURNER_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include "GGMetriqueMaillage.h"
#include <set>

DECLARE_CLASS(GGDelaunayArete);
DECLARE_CLASS(GGContainerAreteARetourner);

class GGContainerAreteARetourner
{
public:
   typedef std::set<GGDelaunayAreteP> TCContainerArete;

        GGContainerAreteARetourner ();
        ~GGContainerAreteARetourner();

   void ajoute            (GGDelaunayAreteP);
   void ajouteSansControle(GGDelaunayAreteP);
   void retire            (GGDelaunayAreteP);

   TCContainerArete::size_type      reqDimension () const;
   TCContainerArete::iterator       reqAreteDebut();
   TCContainerArete::const_iterator reqAreteDebut() const;
   TCContainerArete::iterator       reqAreteFin  ();
   TCContainerArete::const_iterator reqAreteFin  () const;

protected:
   void invariant (ConstCarP) const;

private:
   TCContainerArete aretes;
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGContainerAreteARetourner::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GGContainerAreteARetourner::invariant(ConstCarP) const
{
}
#endif

#include "GGContainerAreteARetourner.hpp"

#endif // ifndef GGCONTAINERARETEARETOURNER_H_DEJA_INCLU

