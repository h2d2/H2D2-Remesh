//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GGDelaunaySurface.h
//
// Classe: GGDelaunaySurface
//
// Sommaire: Classe de surface pour une génération de maillage de Delaunay.
//
// Description:
//    La classe <code>GGDelaunaySurface</code> représente une surface dans
//    un algorithme de génération de maillage par une méthode de Delaunay.
//
// Attributs:
//    Entier            compteRef      // Compteur des références
//    GGMailleurDelaunayP mP           // Le mailleur
//    GGDelaunaySommetP sommets[3]     // Sommets de la surface
//    GGDelaunayAreteP  aretes[3]      // Arêtes de la surface
//    DReel             aire           // Aire de la surface
//
// Notes:
//
//************************************************************************
// 08-03-93  Yves Secretan
// 28-10-98  Yves Secretan    Remplace #include "ererreur"
// 13-11-98  Yves Secretan    Ajoute reqBarycentre
//************************************************************************
#ifndef GGDELAUNAYSURFACE_H_DEJA_INCLU
#define GGDELAUNAYSURFACE_H_DEJA_INCLU

#ifndef MAILLEURS_INTERNES
#  define MAILLEURS_INTERNES 1
#endif // MAILLEURS_INTERNES

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GGMetriqueMaillage.h"

#include "GOCoord3.hf"
DECLARE_CLASS(GGMailleurDelaunay);
DECLARE_CLASS(GGDelaunayInfoBase);
DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunayArete);

DECLARE_CLASS(GGDelaunaySurface);
DECLARE_CLASS(GGDelaunaySurfaceArguments);

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySurfaceArguments
{
public:
   Entier m_lvl;

   GGDelaunaySurfaceArguments()
      : m_lvl(0)
   {}

   GGDelaunaySurfaceArguments& niveauRaff(Entier  i) { m_lvl = i; return *this; }
};

class DLL_IMPEXP(MAILLEURS_INTERNES) GGDelaunaySurface
{
public:
   static const DReel TOLERANCE_DEFORMATION;

         GGDelaunaySurface  (GGMailleurDelaunayP,
                             GGDelaunaySommetP, GGDelaunaySommetP, GGDelaunaySommetP,
                             GGDelaunayAreteP, GGDelaunayAreteP, GGDelaunayAreteP,
                             const GGDelaunaySurfaceArguments& = GGDelaunaySurfaceArguments());

   void  ajouteReference    ();
   void  enleveReference    ();
   ERMsg ctrlCompteReference() const;
   void  detache            ();

   Booleen             aSommets        (ConstGGDelaunaySommetP, ConstGGDelaunaySommetP, ConstGGDelaunaySommetP) const;
   static Booleen      ontMemeSommets  (ConstGGDelaunaySurfaceP, ConstGGDelaunaySurfaceP);
   GGDelaunayAreteP    reqArete        (ConstGGDelaunaySommetP, ConstGGDelaunaySommetP) const;
   GGDelaunayAreteP    reqArete0       () const;
   GGDelaunayAreteP    reqArete1       () const;
   GGDelaunayAreteP    reqArete2       () const;
   GGDelaunaySommetP   reqAutreSommet  (ConstGGDelaunaySommetP, ConstGGDelaunaySommetP) const;
   GGDelaunaySommetP   reqSommet0      () const;
   GGDelaunaySommetP   reqSommet1      () const;
   GGDelaunaySommetP   reqSommet2      () const;

   void                asgInfoClient   (GGDelaunayInfoBaseP);
   void                majAireMetrique ();
   Booleen             doitEtreRaffinee() const;
   Booleen             raffine         ();
   Booleen             soignePeau      ();

   DReel               reqAireMetrique () const;
   DReel               reqCritere      () const;
   DReel               reqCritereElong () const;
   GGDelaunayInfoBaseP reqInfoClient   () const;
   Entier              reqNiveauRaff   () const;

   GOCoordonneesXYZ    reqBarycentre   () const;
   GGMetriqueMaillage  reqMetrique     () const;
   GOCoordonneesXYZ    reqPosiIdeale   (ConstGGDelaunaySommetP) const;

   Booleen             sommetPeutBouger(GGDelaunaySommetP, const GOCoordonneesXYZ&) const;
   void                sommetABouge    (GGDelaunaySommetP);

   static void         asgLongueurCible(const DReel&);
   static void         asgNiveauRaff   (Entier, Entier);
   static DReel        reqSurfaceCible ();
   static DReel        reqAireEuclide  (ConstGGDelaunaySommetP, ConstGGDelaunaySommetP, ConstGGDelaunaySommetP);
   static DReel        reqAireMetrique (const GGMetriqueMaillage&, const GOCoordonneesXYZ&, const GGMetriqueMaillage&, const GOCoordonneesXYZ&, const GGMetriqueMaillage&, const GOCoordonneesXYZ&);
   static DReel        reqCritereElong (ConstGGDelaunayAreteP, ConstGGDelaunayAreteP, ConstGGDelaunayAreteP);

protected:
   void invariant (ConstCarP) const;

private:
   Entier              compteRef;
   Entier              niveauRaff;
   GGMailleurDelaunayP mailleurP;
   GGDelaunaySommetP   sommets[3];
   GGDelaunayAreteP    aretes[3];
   DReel               aire;
   GGDelaunayInfoBaseP infoClientP;

   static DReel        surfaceCible;
   static Entier       niveauRaffMin;
   static Entier       niveauRaffMax;

                      GGDelaunaySurface ();
                      GGDelaunaySurface (const GGDelaunaySurface&);
                      ~GGDelaunaySurface();
   GGDelaunaySurface& operator =        (const GGDelaunaySurface&);

   static DReel       reqAireEuclide    (const GOCoordonneesXYZ&, const GOCoordonneesXYZ&, const GOCoordonneesXYZ&);
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
inline void GGDelaunaySurface::invariant(ConstCarP conditionP) const
{
   Booleen cnd1, cnd2;
   cnd1 = sommets[0] == NUL && sommets[1] == NUL && sommets[2] == NUL;
   cnd2 = sommets[0] != NUL && sommets[1] != NUL && sommets[2] != NUL &&
          sommets[0] != sommets[1] &&
          sommets[0] != sommets[2] &&
          sommets[1] != sommets[2];
   INVARIANT(cnd1 || cnd2, conditionP);
   cnd1 = aretes[0] == NUL && aretes[1] == NUL && aretes[2] == NUL;
   cnd2 = aretes[0] != NUL && aretes[1] != NUL && aretes[2] != NUL &&
          aretes[0] != aretes[1] &&
          aretes[0] != aretes[2] &&
          aretes[1] != aretes[2];
   INVARIANT(cnd1 || cnd2, conditionP);
   INVARIANT(sommets[0] == NUL || GGDelaunaySurface::reqAireEuclide(sommets[0], sommets[1], sommets[2]) > 0.0, conditionP);
}
#else
inline void GGDelaunaySurface::invariant(ConstCarP) const
{
}
#endif

#include "GGDelaunaySurface.hpp"

#endif // ifndef GGDELAUNAYSURFACE_H_DEJA_INCLU

