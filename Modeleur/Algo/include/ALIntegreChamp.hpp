//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id :$
// Classe:  ALIntegreChamp
//************************************************************************
#ifndef ALINTEGRECHAMP_HPP_DEJA_INCLU
#define ALINTEGRECHAMP_HPP_DEJA_INCLU

//**************************************************************
// Sommaire: Constructeur par défaut
//
// Description: Constructeur par défaut, initialise attributs privés.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
ALIntegreChamp<TTChamp>::ALIntegreChamp()
   : intgP(NUL)
   , champP(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Destructeur par défaut
//
// Description: Destructeur par défaut
//
// Entrée:
//
// Sortie:
//
// Notes: 
//****************************************************************
template <typename TTChamp>
ALIntegreChamp<TTChamp>::~ALIntegreChamp()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire: Insère l'élément P1 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementP1& elemP1: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementP1(const typename TCMaillage::TCElementP1& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   (*intgP) += (*champP)[ele.noeud1P->reqIndVno()];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L2 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL2& elemL2: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementL2(const typename TCMaillage::TCElementL2& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // --- Déterminant du jacobien
   DReel coef = 0.5 * ele.calculeDetJ();

   // --- Résultat de l'intégral
   (*intgP) += coef * (*champP)[ele.noeud1P->reqIndVno()];
   (*intgP) += coef * (*champP)[ele.noeud2P->reqIndVno()];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L3 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL3& elemL3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementL3(const typename TCMaillage::TCElementL3& /*elemL3*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire: Insère l'élément L3L dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL3L& elemL3L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typename TCMaillage::TCElementL2 varEleL2;

   varEleL2.noeud1P = ele.noeud1P;
   varEleL2.noeud2P = ele.noeud2P;
   executeAlgoMGElementL2(varEleL2);

   varEleL2.noeud1P = ele.noeud2P;
   varEleL2.noeud2P = ele.noeud3P;
   executeAlgoMGElementL2(varEleL2);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description: 
//
// Entrée:  const TCElementQ4& elemQ4: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementQ4(const typename TCMaillage::TCElementQ4& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel KSI = 1.0 / sqrt(3.0);
   const DReel ETA = 1.0 / sqrt(3.0);

   DReel n1, n2, n3, n4, detJ;
   DReel sn1, sn2, sn3, sn4;

   // ---  Initialise les sommes de ni*detJ
   sn1 = 0.0;
   sn2 = 0.0;
   sn3 = 0.0;
   sn4 = 0.0;

   // ---  1. point de Gauss
   ele.calculeN(n1, n2, n3, n4, -KSI, -ETA);
   detJ = ele.calculeDetJ(-KSI, -ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

   // ---  2. point de Gauss
   ele.calculeN (n1, n2, n3, n4, KSI, -ETA);
   detJ = ele.calculeDetJ(KSI, -ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

   // ---  3. point de Gauss
   ele.calculeN (n1, n2, n3, n4, KSI, ETA);
   detJ = ele.calculeDetJ(KSI, ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

   // ---  4. point de Gauss
   ele.calculeN (n1, n2, n3, n4, -KSI, ETA);
   detJ = ele.calculeDetJ(-KSI, ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

   (*intgP) += sn1 * (*champP)[ele.noeud1P->reqIndVno()];
   (*intgP) += sn2 * (*champP)[ele.noeud2P->reqIndVno()];
   (*intgP) += sn3 * (*champP)[ele.noeud3P->reqIndVno()];
   (*intgP) += sn4 * (*champP)[ele.noeud4P->reqIndVno()];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T3
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT3& elemT3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementT3(const typename TCMaillage::TCElementT3& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // --- Déterminant du jacobien
   DReel detJ = ele.calculeDetJ();

   // --- Résultat de l'intégral
   (*intgP) += 0.16666666666666666666666666666666666 *
              ((*champP)[ele.noeud1P->reqIndVno()] +
               (*champP)[ele.noeud2P->reqIndVno()] +
               (*champP)[ele.noeud3P->reqIndVno()]) * detJ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6& elemT6: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementT6(const typename TCMaillage::TCElementT6& /*elemT6*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6L
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6L& elemT6L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALIntegreChamp<TTChamp>::executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typename TCMaillage::TCElementT3 varEleT3;

   varEleT3.noeud1P = ele.noeud1P;
   varEleT3.noeud2P = ele.noeud2P;
   varEleT3.noeud3P = ele.noeud6P;
   executeAlgoMGElementT3(varEleT3);

   varEleT3.noeud1P = ele.noeud2P;
   varEleT3.noeud2P = ele.noeud3P;
   varEleT3.noeud3P = ele.noeud4P;
   executeAlgoMGElementT3(varEleT3);

   varEleT3.noeud1P = ele.noeud6P;
   varEleT3.noeud2P = ele.noeud4P;
   varEleT3.noeud3P = ele.noeud5P;
   executeAlgoMGElementT3(varEleT3);

   varEleT3.noeud1P = ele.noeud4P;
   varEleT3.noeud2P = ele.noeud6P;
   varEleT3.noeud3P = ele.noeud2P;
   executeAlgoMGElementT3(varEleT3);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Méthode porte d'entrée de l'algorithme:
//
// Description: 
//
// Entrée: 
//
// Sortie:
//
// Notes:
//****************************************************************
template <typename TTChamp>
ERMsg 
ALIntegreChamp<TTChamp>::integre(TCDonnee& res, const TCChamp& champ)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   intgP  = &res;
   champP = &champ;

   // --- Initialise
   (*intgP) *= 0.0;

   // --- Itère pour assembler
   if (msg)
   {
      this->itereSurTousMGElements(*(champP->reqMaillage()));
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif   // ifndef ALINTEGRECHAMP_HPP_DEJA_INCLU
