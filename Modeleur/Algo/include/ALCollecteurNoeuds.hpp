//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Classe:  ALCollecteurNoeuds
//************************************************************************
#ifndef ALCOLLECTEURNOEUDS_HPP_DEJA_INCLU
#define ALCOLLECTEURNOEUDS_HPP_DEJA_INCLU

//**************************************************************
// Sommaire: Constructeur par défaut
//
// Description: Constructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
ALCollecteurNoeuds<TTMaillage>::ALCollecteurNoeuds() : setNoeudsP(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Destructeur par défaut
//
// Description: Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
ALCollecteurNoeuds<TTMaillage>::~ALCollecteurNoeuds()
{
   setNoeudsP = NUL;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementP1& elemP1: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementP1(const typename TCMaillage::TCElementP1 &elemP1)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

      setNoeudsP->insert(elemP1.noeud1P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementL2& elemL2: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementL2(const typename TCMaillage::TCElementL2 &elemL2)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   setNoeudsP->insert(elemL2.noeud1P);
   setNoeudsP->insert(elemL2.noeud2P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementL3& elemL3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementL3(const typename TCMaillage::TCElementL3 &elemL3)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   setNoeudsP->insert(elemL3.noeud1P);
   setNoeudsP->insert(elemL3.noeud2P);
   setNoeudsP->insert(elemL3.noeud3P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementL3L& elemL3L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L &elemL3L)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   setNoeudsP->insert(elemL3L.noeud1P);
   setNoeudsP->insert(elemL3L.noeud2P);
   setNoeudsP->insert(elemL3L.noeud3P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementQ4& elemQ4: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementQ4(const typename TCMaillage::TCElementQ4 &elemQ4)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   setNoeudsP->insert(elemQ4.noeud1P);
   setNoeudsP->insert(elemQ4.noeud2P);
   setNoeudsP->insert(elemQ4.noeud3P);
   setNoeudsP->insert(elemQ4.noeud4P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementT3& elemT3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementT3(const typename TCMaillage::TCElementT3 &elemT3)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   setNoeudsP->insert(elemT3.noeud1P);
   setNoeudsP->insert(elemT3.noeud2P);
   setNoeudsP->insert(elemT3.noeud3P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementT6& elemT6: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementT6(const typename TCMaillage::TCElementT6 &elemT6)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   setNoeudsP->insert(elemT6.noeud1P);
   setNoeudsP->insert(elemT6.noeud2P);
   setNoeudsP->insert(elemT6.noeud3P);
   setNoeudsP->insert(elemT6.noeud4P);
   setNoeudsP->insert(elemT6.noeud5P);
   setNoeudsP->insert(elemT6.noeud6P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère les noeuds de l'élément dans le set commun.
//
// Description:
//
// Entrée:  TCElementT6L& elemT6L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALCollecteurNoeuds<TTMaillage>::executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L &elemT6L)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   setNoeudsP->insert(elemT6L.noeud1P);
   setNoeudsP->insert(elemT6L.noeud2P);
   setNoeudsP->insert(elemT6L.noeud3P);
   setNoeudsP->insert(elemT6L.noeud4P);
   setNoeudsP->insert(elemT6L.noeud5P);
   setNoeudsP->insert(elemT6L.noeud6P);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Méthode porte d'entrée de l'algorithme.
//
// Description: Rassemble les noeuds d'un set d'éléments dans 
//    dans un set de noeuds. Les noeuds ne sont PAS copiés.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void 
ALCollecteurNoeuds<TTMaillage>::collecteNoeuds(TCSetNoeuds& setNoeudsP_param,
                                               const typename TCSetElements::iterator& debutI,
                                               const typename TCSetElements::iterator& finI)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //set dans lequel il faut fournir la réponse
  setNoeudsP = &setNoeudsP_param;

  typename TCSetElements::iterator iterateur = debutI;
  while (iterateur != finI)
  {
     (*iterateur)->executeAlgorithme(*this);
     iterateur++;
  }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

#endif   //ifndef ALCOLLECTEURNOEUDS_HPP_DEJA_INCLU
