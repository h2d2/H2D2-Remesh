//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id :$
// Classe:  ALFiltrePeau
//************************************************************************
#ifndef ALFILTREPEAU_HPP_DEJA_INCLU
#define ALFILTREPEAU_HPP_DEJA_INCLU

#include "ALCollecteurNoeuds.h"
#include <utility> //pair

//**************************************************************
// Sommaire: Constructeur par défaut
//
// Description: Constructeur par défaut, initialise attributs privés.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
ALFiltrePeau<TTMaillage>::ALFiltrePeau()
   : setElementP()
   , setNoeudP()
   , nbrElementsIllegaux(0)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Destructeur par défaut
//
// Description: Destructeur par défaut, détruit éléments inutilisés.
//
// Entrée:
//
// Sortie:
//
// Notes: 
//    Ne PAS détruire les noeuds et éléments, car le maillage
//    en devient propriétaire.
//****************************************************************
template <typename TTMaillage>
ALFiltrePeau<TTMaillage>::~ALFiltrePeau()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire:  Ajoute un élément de peau dans l'ensemble 
//    setElementP.
//
// Description: 
//    Tente d'ajouter un élément de peau (arête) dans l'ensemble 
//    setElementP. S'il s'y trouve déjà, les deux 
//    éléments/arêtes sont supprimées, puisqu'il s'agit d'arêtes
//    partagées qui ne font donc pas partie de la peau.
//
// Entrée:  TCElementP& elemP: élément à insérer
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::ajouteElementPeau(TCElementP& elemP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   std::pair<typename TCSetElementP::iterator, bool> reponse = setElementP.insert(elemP);

   if (!(reponse.second))       
   {  // Si l'insertion n'a pas eu lieu
      // Efface l'élément en double puisqu'il s'agit d'un arête partagée
      setElementP.erase(reponse.first);
      // Efface l'élément passé en paramètre, pusiqu'il ne sert à rien.
      delete elemP;
      elemP = NUL;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément P1 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementP1& elemP1: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementP1(const typename TCMaillage::TCElementP1& /*elemP1*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ++nbrElementsIllegaux;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L2 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL2& elemL2: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementL2(const typename TCMaillage::TCElementL2& /*elemL2*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ++nbrElementsIllegaux;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L3 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL3& elemL3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementL3(const typename TCMaillage::TCElementL3& /*elemL3*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ++nbrElementsIllegaux;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L3L dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL3L& elemL3L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L& /*elemL3L*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ++nbrElementsIllegaux;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:  Construit et ajoute les éléments de peau du Q4.
//    dans le setElementP commun.
//
// Description: 
//
// Entrée:  const TCElementQ4& elemQ4: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementQ4(const typename TCMaillage::TCElementQ4& elemQ4)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::TCElementL2 TCElementL2;

   TCElementP elemP;
   TCNoeudP  noeudsP[2];
   TCNoeudP* noeudsPP = &noeudsP[0];

   // --- Côté 1-2
   noeudsPP[0] = elemQ4.noeud1P;
   noeudsPP[1] = elemQ4.noeud2P;
   elemP = new TCElementL2(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 2-3
   noeudsPP[0] = elemQ4.noeud2P;
   noeudsPP[1] = elemQ4.noeud3P;
   elemP = new TCElementL2(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 3-4
   noeudsPP[0] = elemQ4.noeud3P;
   noeudsPP[1] = elemQ4.noeud4P;
   elemP = new TCElementL2(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 4-1
   noeudsPP[0] = elemQ4.noeud4P;
   noeudsPP[1] = elemQ4.noeud1P;
   elemP = new TCElementL2(0, noeudsPP);
   ajouteElementPeau(elemP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T3
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT3& elemT3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementT3(const typename TCMaillage::TCElementT3& elemT3)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::TCElementL2 TCElementL2;

   TCElementP elemP;
   TCNoeudP  noeudsP[2];
   TCNoeudP* noeudsPP = &noeudsP[0];

   // --- Côté 1-2
   noeudsPP[0] = elemT3.noeud1P;
   noeudsPP[1] = elemT3.noeud2P;
   elemP = new TCElementL2(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 2-3
   noeudsPP[0] = elemT3.noeud2P;
   noeudsPP[1] = elemT3.noeud3P;
   elemP = new TCElementL2(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 3-1
   noeudsPP[0] = elemT3.noeud3P;
   noeudsPP[1] = elemT3.noeud1P;
   elemP = new TCElementL2(0, noeudsPP);
   ajouteElementPeau(elemP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6& elemT6: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementT6(const typename TCMaillage::TCElementT6& elemT6)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::TCElementL3 TCElementL3;

   TCElementP elemP;
   TCNoeudP  noeudsP[3];
   TCNoeudP* noeudsPP = &noeudsP[0];

   // --- Côté 1-2-3
   noeudsPP[0] = elemT6.noeud1P;
   noeudsPP[1] = elemT6.noeud2P;
   noeudsPP[2] = elemT6.noeud3P;
   elemP = new TCElementL3(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 3-4-5
   noeudsPP[0] = elemT6.noeud3P;
   noeudsPP[1] = elemT6.noeud4P;
   noeudsPP[2] = elemT6.noeud5P;
   elemP = new TCElementL3(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 5-6-1
   noeudsPP[0] = elemT6.noeud5P;
   noeudsPP[1] = elemT6.noeud6P;
   noeudsPP[2] = elemT6.noeud1P;
   elemP = new TCElementL3(0, noeudsPP);
   ajouteElementPeau(elemP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6L
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6L& elemT6L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void ALFiltrePeau<TTMaillage>::executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L& elemT6L)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::TCElementL3L TCElementL3L;

   TCElementP elemP;
   TCNoeudP  noeudsP[3];
   TCNoeudP* noeudsPP = &noeudsP[0];
   
   // --- Côté 1-2-3
   noeudsPP[0] = elemT6L.noeud1P;
   noeudsPP[1] = elemT6L.noeud2P;
   noeudsPP[2] = elemT6L.noeud3P;
   elemP = new TCElementL3L(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 3-4-5
   noeudsPP[0] = elemT6L.noeud3P;
   noeudsPP[1] = elemT6L.noeud4P;
   noeudsPP[2] = elemT6L.noeud5P;
   elemP = new TCElementL3L(0, noeudsPP);
   ajouteElementPeau(elemP);

   // --- Côté 5-6-1
   noeudsPP[0] = elemT6L.noeud5P;
   noeudsPP[1] = elemT6L.noeud6P;
   noeudsPP[2] = elemT6L.noeud1P;
   elemP = new TCElementL3L(0, noeudsPP);
   ajouteElementPeau(elemP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Méthode porte d'entrée de l'algorithme:
//    Filtre la peau d'un maillage.
//
// Description: 
//    Filtre la peau (contour) du maillage passé en paramètre
//    met dans le peauMaillage passé en paramètre.
//    Le maillage de peau est un maillage constitué d'éléments
//    linéaires (L2) qui correspondent au contours externe (et
//    interne s'il y a lieu) du maillage. Ceux-cis ne sont pas 
//    ordonnés.
//
// Entrée: 
//    const TCMaillage& maillage: Maillage dont on veut la peau
//
// Sortie:
//    TCMaillage& peauMaillage: Maillage qui contiendra la peau
//
// Notes:
//    Recherche séquentielle des éléments en connection
//    Il serait plus efficace de créer un container dans lequel
//    on place les morceaux des séquences d'éléments connectés
//    accessibles par no de noeud aux deux bouts (map) et d'ajouter
//    chaque élément tour à tour aux séquences, puis de rassembler les
//    séquences à la fin.
//****************************************************************
template <typename TTMaillage>
ERMsg 
ALFiltrePeau<TTMaillage>::filtrePeau(TCMaillage& peauMaillage, 
                                     const TCMaillage& maillage)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   //Le maillage passé en paramètre ne devrait pas être de type peau.
   typename TCMaillage::TypeMaillage typeMaillage;
   maillage.reqTypeMaillage(typeMaillage);
   PRECONDITION(typeMaillage != TCMaillage::PEAU);
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   //Le maillage passé en paramètre ne doit pas être vide.
   if (maillage.reqNbrElemTotal() == 0)
   {
      msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_VIDE");
   }

   // --- Trouve l'ensemble des éléments de peau
   if (msg)
   {
      // Chaque élément insère ses arêtes dans l'attribut de classe setElementP
      // et supprime toute arête en double; il ne reste plus que 
      // les arêtes de peau dans le setElementP pour finir.
      nbrElementsIllegaux = 0;
      this->itereSurTousMGElements(maillage);

      // Vérifie qu'il y aie des résultats
      if (setElementP.size() == 0)
      {
         msg = ERMsg(ERMsg::AVERTISSEMENT, "AVR_PEAU_MAILLAGE_VIDE");
      }
   }

   // --- Trouve l'ensemble des noeuds de la peau à partir de
   //     l'ensemble des éléments de peau
   if (msg)
   {
            typename TCSetElementP::iterator elementPI = setElementP.begin();
      const typename TCSetElementP::iterator elementFinI = setElementP.end();
      ALCollecteurNoeuds<TCMaillage> collecteurNoeuds;
      collecteurNoeuds.collecteNoeuds(setNoeudP, elementPI, elementFinI);
   }

   // --- Construit le maillage de peau
   if (msg)
   {
      // ---  Assigne les attributs de base au maillage de peau
      peauMaillage.vide();
      peauMaillage.asgTypeMaillage(TCMaillage::PEAU); //ne retourne jamais de msg
      peauMaillage.asgMaillageParent(&maillage);      //ne retourne jamais de msg
      //peauMaillage.asgMinMax() lorsque cet attribut sera utilisé...

      // ---  Assigne les noeuds au maillage de peau DANS N'IMPORTE QUEL ORDRE
      msg = peauMaillage.dimListeNoeuds( static_cast<EntierN>(setNoeudP.size()) );
      if (msg)
      {
               typename TCSetNoeudP::iterator noeudI = setNoeudP.begin();
         const typename TCSetNoeudP::iterator noeudFinI = setNoeudP.end();
         while(noeudI != noeudFinI)
         {
            peauMaillage.asgNoeud(*noeudI); //ne retourne jamais de msg
            ++noeudI;
         }
      }//if msg

      // ---  Assigne les éléments de peau au maillage de peau DANS N'IMPORTE QUEL ORDRE
      //      Le maillage prend possesion des éléments et désalloue la mémoire
      if (msg)
      {
         typename TCSetElementP::iterator elementPI = setElementP.begin();
         EntierN compteur = 0;

         while (elementPI != setElementP.end())
         {
            (*elementPI)->asgNoElement(compteur++);
            peauMaillage.asgElement(*elementPI); //ne retourne jamais de msg
            ++elementPI;
         }
      }//if msg
   }//if msg

   // Vérifie que le maillage ne contienne pas d'éléments
   // dont on ne peut filtrer la peau
   if (msg && nbrElementsIllegaux > 0)
   {
      msg = ERMsg(ERMsg::AVERTISSEMENT, "AVR_MAILLAGE_CONTIENT_ELEMENTS_NON_FILTRABLE");
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Sommaire: Génère un région à partir d'un maillage de base.
//
// Description: 
//    Filtre la peau (contour) du maillage passé en paramètre
//    ordonne les éléments du maillagePeau afin de 
//    générer la GORegion correspondante.
//
// Entrée: 
//    const TCMaillage& maillage: Maillage dont on veut la région
//
// Sortie:  
//    GORegion& region: Region résultante
//
// Notes: 
//
//****************************************************************
template <typename TTMaillage>
ERMsg 
ALFiltrePeau<TTMaillage>::filtreRegion(GORegion& region, 
                                       const TCMaillage& maillage)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   GOMultiPolygone   mult;

   std::vector<GOPoint>  points;
   std::vector<TCElementP> listeElementPTrie;
   // NB. constante (no de noeuds max des éléments gérés par modeleur)
   // plutôt que variable pour plaire à gcc.
   // si MGElements offrait un iterateur sur les noeuds, 
   //plutôt que de les fournir dans un TCNoued**, ce serait plus beau.
   const EntierN NBR_NOEUDS = 9;
   TCNoeudP  noeudsElemP[NBR_NOEUDS];
   TCNoeudP* noeudsElemPP = &noeudsElemP[0];

   //--- Obtient le maillage peau 
   // (remplit l'attribut de classe setElementsP avec les éléments de peau)
   TCMaillage maillagePeau;
   msg = this->filtrePeau(maillagePeau, maillage);

   //---  Construit la région à partir des éléments du maillage peau.
   //     Ordonne les éléments de peau pour former des géom. fermées 
   //     jusqu'à ce que le set soit vide.
   while (msg && !setElementP.empty())
   {
      // ---  Premier élément de la future polyligne
      typename TCSetElementP::iterator elementCourantPI = setElementP.begin();
      (*elementCourantPI)->reqNoeuds(noeudsElemPP, NBR_NOEUDS);

      const EntierN  indNoeudDeb = 0;
      const EntierN  indNoeudFin = (*elementCourantPI)->reqNbrNoeud() - 1;
      TCNoeudP premierNoeudP = noeudsElemPP[indNoeudDeb];
      TCNoeudP dernierNoeudP = noeudsElemPP[indNoeudFin];

      // No de noeud débutant la première série triée, permet de vérifier si
      // on obtient une géométrie fermée
      EntierN noNoeudDebut   = premierNoeudP->reqNoNoeud();
      // Indice du noeud qui connecte à l'élément suivant selon type element
      // conserve le noNoeud du dernier noeud de l'élément
      // afin de trouver l'élément qui connecte
      EntierN noNoeudSuivant = dernierNoeudP->reqNoNoeud();

      // ---  Créer le premier noeud dans la région
      const GOPoint pt(premierNoeudP->reqCoordonnees());
      points.push_back(pt);

      // ---  Supprime l'élement du set et ajoute à la liste triée
      listeElementPTrie.push_back(*elementCourantPI);
      setElementP.erase(elementCourantPI);

      // ---  Cherche l'élément qui suit dans le set jusqu'à ce 
      //      qu'une géométrie fermée soit obtenue.
      while (msg && noNoeudSuivant != noNoeudDebut)
      {
         typename TCSetElementP::iterator elementPI = setElementP.begin();

         Booleen elemSuivantTrouve = FAUX;
         for( ; elementPI != setElementP.end(); ++elementPI)
         {
            (*elementPI)->reqNoeuds(noeudsElemPP, NBR_NOEUDS); 
            const EntierN iDeb = 0;
            const EntierN iFin = (*elementPI)->reqNbrNoeud() - 1;

            if (noeudsElemPP[iDeb]->reqNoNoeud() == noNoeudSuivant)
            {
               premierNoeudP = noeudsElemPP[iDeb];
               dernierNoeudP = noeudsElemPP[iFin];
               elemSuivantTrouve = VRAI;
            }
            else if (noeudsElemPP[iFin]->reqNoNoeud() == noNoeudSuivant)
            {
               premierNoeudP = noeudsElemPP[iFin];
               dernierNoeudP = noeudsElemPP[iDeb];
               elemSuivantTrouve = VRAI;
            }

            if (elemSuivantTrouve) break;
         }  //for !elemSuivantTrouve

         // ---  Elément trouvé - insère le point et passse au suivant
         if (elemSuivantTrouve)
         {
            const GOPoint pt(premierNoeudP->reqCoordonnees());
            points.push_back(pt);
            noNoeudSuivant = dernierNoeudP->reqNoNoeud();
            listeElementPTrie.push_back(*elementPI);
            setElementP.erase(elementPI);
         }

         // Sinon, géométrie incomplète, génère erreur
         else
         {
            msg = ERMsg(ERMsg::ERREUR, "ERR_ALGO_MAILLAGE");
            msg.ajoute("Peau de maillage incomplète.");
         }

      }  //while(noNoeudElemSuivant != noNoeudDebut)

      // ---  Construis la multipolyligne fermée pour la région
      if (msg)
      {
         GOPolyligneFermee ligne;
         msg = ligne.assigne(points.begin(), points.end());
         msg = mult.ajoutePolygone(ligne);
         if (!msg) 
         {
            msg = ERMsg(ERMsg::ERREUR, "ERR_ALGO_MAILLAGE");
            msg.ajoute("Problème à la création de la géométrie.");
         }
      }
      points.empty();

   }//while( !(setElementP.empty())

   // ---  Assigne le multipolygone à la region passée en paramètre
   if (msg)
   {
      region.asgMultiPolygone(mult);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif   // ifndef ALFILTREPEAU_HPP_DEJA_INCLU
