//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id :$
// Classe:  ALLisseChampEF
//************************************************************************
#ifndef ALLISSECHAMPEF_HPP_DEJA_INCLU
#define ALLISSECHAMPEF_HPP_DEJA_INCLU

//**************************************************************
// Sommaire: Constructeur par défaut
//
// Description: Constructeur par défaut, initialise attributs privés.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
ALLisseChampEF<TTChamp>::ALLisseChampEF()
   : champP(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Destructeur par défaut
//
// Description: Destructeur par défaut
//
// Entrée:
//
// Sortie:
//
// Notes: 
//****************************************************************
template <typename TTChamp>
ALLisseChampEF<TTChamp>::~ALLisseChampEF()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire: Insère l'élément P1 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementP1& elemP1: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementP1(const typename TCMaillage::TCElementP1& /*ele*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

//   (*intgP) += (*champP)[ele.noeud1P->reqIndVno()];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L2 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL2& elemL2: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementL2(const typename TCMaillage::TCElementL2& /*ele*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

/*
   // --- Déterminant du jacobien
   DReel coef = 0.5 * ele.calculeDetJ();

   // --- Résultat de l'intégral
   (*intgP) += coef * (*champP)[ele.noeud1P->reqIndVno()];
   (*intgP) += coef * (*champP)[ele.noeud2P->reqIndVno()];
*/

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L3 dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL3& elemL3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementL3(const typename TCMaillage::TCElementL3& /*elemL3*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire: Insère l'élément L3L dans le setElementP illégaux
//
// Description: 
//
// Entrée:  const TCElementL3L& elemL3L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typename TCMaillage::TCElementL2 varEleL2;

   varEleL2.noeud1P = ele.noeud1P;
   varEleL2.noeud2P = ele.noeud2P;
   executeAlgoMGElementL2(varEleL2);

   varEleL2.noeud1P = ele.noeud2P;
   varEleL2.noeud2P = ele.noeud3P;
   executeAlgoMGElementL2(varEleL2);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:
//
// Description: 
//
// Entrée:  const TCElementQ4& elemQ4: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementQ4(const typename TCMaillage::TCElementQ4& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel KSI = 1.0 / sqrt(3.0);
   const DReel ETA = 1.0 / sqrt(3.0);

   DReel n1, n2, n3, n4, detJ;
   DReel sn1, sn2, sn3, sn4;

   // ---  Initialise les sommes de ni*detJ
   sn1 = 0.0;
   sn2 = 0.0;
   sn3 = 0.0;
   sn4 = 0.0;

   // ---  1. point de Gauss
   ele.calculeN(n1, n2, n3, n4, -KSI, -ETA);
   detJ = ele.calculeDetJ(-KSI, -ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

   // ---  2. point de Gauss
   ele.calculeN (n1, n2, n3, n4, KSI, -ETA);
   detJ = ele.calculeDetJ(KSI, -ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

   // ---  3. point de Gauss
   ele.calculeN (n1, n2, n3, n4, KSI, ETA);
   detJ = ele.calculeDetJ(KSI, ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

   // ---  4. point de Gauss
   ele.calculeN (n1, n2, n3, n4, -KSI, ETA);
   detJ = ele.calculeDetJ(-KSI, ETA);
   sn1 += (n1*detJ);
   sn2 += (n2*detJ);
   sn3 += (n3*detJ);
   sn4 += (n4*detJ);

/*
   (*intgP) += sn1 * (*champP)[ele.noeud1P->reqIndVno()];
   (*intgP) += sn2 * (*champP)[ele.noeud2P->reqIndVno()];
   (*intgP) += sn3 * (*champP)[ele.noeud3P->reqIndVno()];
   (*intgP) += sn4 * (*champP)[ele.noeud4P->reqIndVno()];
*/

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T3
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT3& elemT3: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementT3(const typename TCMaillage::TCElementT3& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const DReel UN_6  = 1.0 /  6.0;
   const DReel UN_24 = 1.0 / 24.0;

   // --- Indices de vno
   Entier ivno1 = ele.noeud1P->reqIndVno();
   Entier ivno2 = ele.noeud2P->reqIndVno();
   Entier ivno3 = ele.noeud3P->reqIndVno();

   // --- Déterminant du jacobien
   DReel detJ = ele.calculeDetJ();

   // ---  Valeurs nodales
   const typename TTChamp::TCDonnee& v1 = (*champP)[ele.noeud1P->reqIndVno()];
   const typename TTChamp::TCDonnee& v2 = (*champP)[ele.noeud2P->reqIndVno()];
   const typename TTChamp::TCDonnee& v3 = (*champP)[ele.noeud3P->reqIndVno()];
         typename TTChamp::TCDonnee  vs = v1 + v2 + v3;
      
   // --- Assemble les valeurs
   v[ivno1] += UN_24 * detJ * (vs + v1);
   v[ivno2] += UN_24 * detJ * (vs + v2);
   v[ivno3] += UN_24 * detJ * (vs + v3);

   // --- Assemble la matrice masse lumpée
   m[ivno1] += UN_6 * detJ;
   m[ivno2] += UN_6 * detJ;
   m[ivno3] += UN_6 * detJ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6& elemT6: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementT6(const typename TCMaillage::TCElementT6& /*elemT6*/)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Construit et ajoute les éléments de peau du T6L
//    dans le setElementP commun.
//
// Description:
//
// Entrée:  const TCElementT6L& elemT6L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTChamp>
void ALLisseChampEF<TTChamp>::executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typename TCMaillage::TCElementT3 varEleT3;

   varEleT3.noeud1P = ele.noeud1P;
   varEleT3.noeud2P = ele.noeud2P;
   varEleT3.noeud3P = ele.noeud6P;
   executeAlgoMGElementT3(varEleT3);

   varEleT3.noeud1P = ele.noeud2P;
   varEleT3.noeud2P = ele.noeud3P;
   varEleT3.noeud3P = ele.noeud4P;
   executeAlgoMGElementT3(varEleT3);

   varEleT3.noeud1P = ele.noeud6P;
   varEleT3.noeud2P = ele.noeud4P;
   varEleT3.noeud3P = ele.noeud5P;
   executeAlgoMGElementT3(varEleT3);

   varEleT3.noeud1P = ele.noeud4P;
   varEleT3.noeud2P = ele.noeud6P;
   varEleT3.noeud3P = ele.noeud2P;
   executeAlgoMGElementT3(varEleT3);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Méthode porte d'entrée de l'algorithme:
//
// Description: 
//
// Entrée: 
//
// Sortie:
//
// Notes:
//****************************************************************
template <typename TTChamp>
ERMsg 
ALLisseChampEF<TTChamp>::lisse(TCChamp& champ)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   champP = &champ;

   // ---  Dimensionne les tables de travail
   m.resize(champP->reqNbrValeursNodales(), 0.0);
   v.resize(champP->reqNbrValeursNodales(), TCDonnee()*0.0);

   // --- Itère pour assembler
   if (msg)
   {
      this->itereSurTousMGElements(*(champP->reqMaillage()));
   }

   // --- Calcule les valeurs pondérées
   if (msg)
   {
      typedef std::vector<DReel>::size_type TTSize;
      for (TTSize i = 0; i < m.size(); ++i)
         v[i] /= m[i];
   }
      
   // --- Assigne le champ
   if (msg)
   {
      champP->asgValeurs(v.begin(), v.end());
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif   // ifndef ALLISSECHAMPEF_HPP_DEJA_INCLU
