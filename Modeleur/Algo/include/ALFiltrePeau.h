//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: ALFiltrePeau.h
//
// Classe: ALFiltrePeau
//
// Description:
//    Implante un algorithme de filtrage de peau d'un maillage
//    Extrait la "peau" (ou le contour) externe du maillage d'éléments 
//    finis 2D, ainsi que ses contours internes s'il y a lieu.
//    Retourne un maillage de type PEAU, c'est à dire un maillage 
//    constitué d'éléments linéaires qui forment les contours.
//    Algorithme également employé pour obtenir une région 2D
//    à partir d'un maillage.
//
// Attributs:
//    setElementP;    set qui contiendra les éléments linéaires de la peau
//    setNoeudP;      set qui contiendra les noeuds de la peau
//    setElementPIllegaux;  
//                   set qui contiendra les éléments dont on ne peut 
//                   filtrer la peau (eg peau d'un élément linaire = 
//                   2 points). Bien que ceci constitue une géométrie  
//                   acceptable, on ne le permet pas (pourquoi? inutile jsuqu'à présent?) 
//
// Notes:
//    N'extrait pas la peau d'éléments 1D ou 0D.
// <p>
//    Actuellement, fonctionne seulement avec EFMaillage3D/EFTraitsElement3D.
//    Voir s'il faut rendre la classe template pour MGMaillage:
//    En ce qui concerne la méthode filtrePeau, aucun problème.
//    En ce qui concerne la méthode filtreRegion, celle-ci présume
//    que l'on travaille avec des GOCoordonnées et retourne une GORegion
//    et non une SRRegion pouvant avoir des coordonnées de tout type.
//************************************************************************
#ifndef ALFILTREPEAU_H_DEJA_INCLU
#define ALFILTREPEAU_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"
#include "GORegion.hf"
#include "MGConstElementAlgorithme.h"

#include <set>
#include <list>

template <typename TTMaillage>
class ALFiltrePeau
   : public MGConstElementAlgorithme<TTMaillage>
{
public:
   typedef          TTMaillage               TCMaillage;          
   typedef typename TCMaillage::TCElement    TCElement;
   typedef typename TCMaillage::TCElementP   TCElementP;
   typedef          const TCElement*         TCConstElementP;
   typedef typename TCMaillage::TCNoeud      TCNoeud;
   typedef typename TCMaillage::TCNoeudP     TCNoeudP;

                ALFiltrePeau   ();
   virtual     ~ALFiltrePeau   ();

   virtual void executeAlgoMGElementP1 (const typename TCMaillage::TCElementP1&);
   virtual void executeAlgoMGElementL2 (const typename TCMaillage::TCElementL2&);
   virtual void executeAlgoMGElementL3 (const typename TCMaillage::TCElementL3&);
   virtual void executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L&);
   virtual void executeAlgoMGElementQ4 (const typename TCMaillage::TCElementQ4&);
   virtual void executeAlgoMGElementT3 (const typename TCMaillage::TCElementT3&);
   virtual void executeAlgoMGElementT6 (const typename TCMaillage::TCElementT6&);
   virtual void executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L&);

   ERMsg        filtrePeau     (TCMaillage&, const TCMaillage&);
   ERMsg        filtreRegion   (GORegion&, const TCMaillage&);

protected:
   virtual void  invariant       (ConstCarP) const;

private:
   typedef compareConnectiviteInferieure<typename TCMaillage::TCTraits> TCCompareElement;
   
   //Un set trié par connectivité, càd liste des no de noeuds des éléments
   //eg. 123 < 345 pour des éléments T3
   typedef std::set<TCElementP, TCCompareElement> TCSetElementP;
   typedef std::set<TCNoeudP>                     TCSetNoeudP;

   TCSetElementP setElementP;
   TCSetNoeudP   setNoeudP;
   Entier        nbrElementsIllegaux;

   void ajouteElementPeau(TCElementP&);
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
template <typename TTMaillage>
inline void ALFiltrePeau<TTMaillage>::invariant(ConstCarP /*conditionP*/) const
{
}
#else
template <typename TTMaillage>
inline void ALFiltrePeau<TTMaillage>::invariant(ConstCarP) const
{
}
#endif

#include "ALFiltrePeau.hpp"

#endif // ifndef ALFILTREPEAU_H_DEJA_INCLU
