//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe: ALCollecteurNoeuds
//
// Description:
//    Algorithme qui collecte tous les noeuds à partir d'un set d'éléments.
//    Les noeuds ne sont pas copiés.
//    Implanté avec l'algorithme visiteur sur des élements de la
//    famille EFElement ne faisant pas nécéssairement partie d'un maillage.
//   
// Attributs:
//
// Notes:
//    Voir s'il faut rendre l'algo template pour des MGElements.
//    Pour l'instant fonctionne avec des EFElements.
//************************************************************************
#ifndef ALCOLLECTEURNOEUDS_H_DEJA_INCLU
#define ALCOLLECTEURNOEUDS_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGConstElementAlgorithme.h"
#include "MGElement.h"

#include <functional>
#include <set>

template <typename TTMaillage>
class ALCollecteurNoeuds
   : public MGConstElementAlgorithme<TTMaillage>
{
public:
   typedef          TTMaillage               TCMaillage;          
   typedef typename TCMaillage::TCElement    TCElement;
   typedef typename TCMaillage::TCElementP   TCElementP;
   typedef typename TCMaillage::TCNoeud      TCNoeud;
   typedef typename TCMaillage::TCNoeudP     TCNoeudP;

   typedef std::set<TCNoeudP, std::less<TCNoeudP> > TCSetNoeuds;
   typedef compareConnectiviteInferieure<typename TCMaillage::TCTraits> TCCompareElement;
   typedef std::set<TCElementP, TCCompareElement> TCSetElements;

                ALCollecteurNoeuds     ();
   virtual     ~ALCollecteurNoeuds     ();

   virtual void executeAlgoMGElementP1 (const typename TCMaillage::TCElementP1&);
   virtual void executeAlgoMGElementL2 (const typename TCMaillage::TCElementL2&);
   virtual void executeAlgoMGElementL3 (const typename TCMaillage::TCElementL3&);
   virtual void executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L&);
   virtual void executeAlgoMGElementQ4 (const typename TCMaillage::TCElementQ4&);
   virtual void executeAlgoMGElementT3 (const typename TCMaillage::TCElementT3&);
   virtual void executeAlgoMGElementT6 (const typename TCMaillage::TCElementT6&);
   virtual void executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L&);

           void collecteNoeuds         (TCSetNoeuds&,
                                        const typename TCSetElements::iterator&,
                                        const typename TCSetElements::iterator&);

protected:
   virtual void invariant              (ConstCarP) const;

private:
   TCSetNoeuds* setNoeudsP;
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
template <typename TTMaillage>
inline void ALCollecteurNoeuds<TTMaillage>::invariant(ConstCarP /*conditionP*/) const
{
}
#else
template <typename TTMaillage>
inline void ALCollecteurNoeuds<TTMaillage>::invariant(ConstCarP) const
{
}
#endif   // MODE_DEBUG

#include "ALCollecteurNoeuds.hpp"

#endif // ifndef ALCOLLECTEURNOEUDS_H_DEJA_INCLU
