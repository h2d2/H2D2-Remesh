//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe: ALIntegreChamp
//
// Description:
//    Implante un algorithme d'intégration d'un champ
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef ALINTEGRECHAMP_H_DEJA_INCLU
#define ALINTEGRECHAMP_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GORegion.hf"
#include "MGConstElementAlgorithme.h"

#include <set>
#include <list>

template <typename TTChamp>
class ALIntegreChamp
   : public MGConstElementAlgorithme<typename TTChamp::TCMaillage>
{
public:
   typedef          TTChamp               TCChamp;
   typedef typename TCChamp::TCMaillage   TCMaillage;          
   typedef typename TCChamp::TCDonnee     TCDonnee;          

                ALIntegreChamp   ();
   virtual     ~ALIntegreChamp   ();

   virtual void executeAlgoMGElementP1 (const typename TCMaillage::TCElementP1&);
   virtual void executeAlgoMGElementL2 (const typename TCMaillage::TCElementL2&);
   virtual void executeAlgoMGElementL3 (const typename TCMaillage::TCElementL3&);
   virtual void executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L&);
   virtual void executeAlgoMGElementQ4 (const typename TCMaillage::TCElementQ4&);
   virtual void executeAlgoMGElementT3 (const typename TCMaillage::TCElementT3&);
   virtual void executeAlgoMGElementT6 (const typename TCMaillage::TCElementT6&);
   virtual void executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L&);

   ERMsg        integre     (TCDonnee&, const TCChamp&);

protected:
   virtual void  invariant       (ConstCarP) const;

private:
   TCDonnee*      intgP;
   const TCChamp* champP;
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
template <typename TTChamp>
inline void ALIntegreChamp<TTChamp>::invariant(ConstCarP /*conditionP*/) const
{
}
#else
template <typename TTChamp>
inline void ALIntegreChamp<TTChamp>::invariant(ConstCarP) const
{
}
#endif

#include "ALIntegreChamp.hpp"

#endif // ifndef ALINTEGRECHAMP_H_DEJA_INCLU
