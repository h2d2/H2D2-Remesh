//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Classe : SRRegion
//************************************************************************

#include "SRRegion_GOCoordonneesX.h"

//************************************************************************
// Sommaire:  
//    Constructeur par défaut.
//
// Description:
//    Le constructeur public <code>SRRegion(...)</code> de la classe
//    construit une région vide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonneesX>::SRRegion()
   : m_enveloppe()
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:
//    Constructeur par copie
//
// Description: 
//    Constructeur par copie de la classe.
//
// Entrée:
//    const SRRegion<GOCoordonneesX>& : un objet du même type.
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonneesX>::SRRegion(const TCSelf& obj)
   : m_enveloppe(obj.m_enveloppe)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:
//    Destructeur
//
// Description: 
//    Destructeur de la classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonneesX>::~SRRegion()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Assigne les limites de la région 1D
//
// Description: 
//    Les limites sont simplement conservées dans l'enveloppe
//
// Entrée:
//    GOCoordonneesX& min : coordonnée minimale
//    GOCoordonneesX& max : coordonnée maximale
//
// Sortie:
//    ERMsg : Message contenant l'état du résultat de la requête (réussite, ...)
//
// Notes:
//
//************************************************************************
ERMsg SRRegion<GOCoordonneesX>::asgLimites(const GOCoordonneesX& min, 
                                          const GOCoordonneesX& max)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   msg = m_enveloppe.asgLimites(min, max);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  
//    Opérateur d'assignation
//
// Description: 
//    Opérateur d'assignation de la classe.
//
// Entrée:
//    const SRRegion& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonneesX>::TCSelf&
SRRegion<GOCoordonneesX>::operator=(const TCSelf& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (this != &obj)
   {
      m_enveloppe = obj.m_enveloppe;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:
//    Test de validité de la région.
//
// Description: 
//    La fonction publique <code>estValide()</code> permet de vérifier
//    si une région est valide ou non.
//
// Entrée:
//
// Sortie:
//    Booleen : VRAI si la région est valide, FAUX sinon.
//
// Notes:
//     La région est valide si son enveloppe est également valide
//    (coordMin < coordMax, ...).
//
//************************************************************************
Booleen SRRegion<GOCoordonneesX>::estValide() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_enveloppe.estValide();
}

//************************************************************************
// Sommaire:
//    Retourne l'enveloppe (le bounding box) de la région.
//
// Description: 
//    La fonction publique <code>reqEnveloppe()</code> retourne l'enveloppe
//    (le bounding box) de la région.
//
// Entrée:
//
// Sortie:
//    TCEnvoleppe& : Référence vers l'enveloppe associée à la région.
//
// Notes:
//
//************************************************************************
const SRRegion<GOCoordonneesX>::TCEnveloppe&
SRRegion<GOCoordonneesX>::reqEnveloppe() const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return m_enveloppe;
}

//************************************************************************
// Sommaire:
//    Détermine si un point se trouve à l'intérieur de la région.
//
// Description: 
//    La fonction publique <code>pointInterieur(...)</code> détermine
//    si un point se trouve à l'intérieur de la région.
//
// Entrée:
//    const TCCoord& point : le point à tester.
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen SRRegion<GOCoordonneesX>::pointInterieur(const TCCoord& point) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_enveloppe.pointInterieur(point);
}

//************************************************************************
// Sommaire: 
//    Operateur d'égalité
//
// Description: 
//    L'opérateur public <code>operator==(...)</code> compare la région
//    à celle passée en argument.
//
// Entrée:
//    const TCSelf& reg : l'objet du même type à comparer
//
// Sortie:
//    Booleen : VRAI si les régions sont identiques, FAUX sinon.
//
// Notes:
//
//************************************************************************
Booleen
SRRegion<GOCoordonneesX>::operator== (const TCSelf& reg) const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return (m_enveloppe == reg.m_enveloppe);
}  

//************************************************************************
// Sommaire: 
//    Opérateur de différence
//
// Description: 
//    L'opérateur public <code>operator!=(...)</code> compare la région
//    à celle passée en argument.
//
// Entrée:
//    const TCSelf& reg : l'objet du même type à comparer
//
// Sortie:
//    Booleen : VRAI si les régions sont identiques, FAUX sinon.
//
// Notes:
//************************************************************************
Booleen
SRRegion<GOCoordonneesX>::operator!= (const TCSelf& reg) const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return !(*this == reg);
}
