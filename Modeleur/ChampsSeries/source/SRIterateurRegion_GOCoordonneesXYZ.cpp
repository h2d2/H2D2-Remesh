//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
#include "SRIterateurRegion_GOCoordonneesXYZ.h"

#include <cmath>

//************************************************************************
// Sommaire:  Constructeur un itérateur de fin.
//
// Description:
//    Le constructeur public <code>SRIterateurRegion</code> construit
//    un itérateur qui marque la fin de la région. C'est l'itérateur 
//    à utiliser comme marque de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonneesXYZ>::SRIterateurRegion()
 : SRIterateurRegionBase<GOCoordonneesXYZ>()
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur d'un itérateur de sous-région 2D
//
// Description: 
//    Le constructeur public <code>SRIterateurRegion(...)</code> construit
//    un itérateur de sous-région 2D. 
//
// Entrée:
//    const TTCoord& pasA        : le premier pas dans lequel itérer
//    const TTCoord& pasB        : le deuxième pas dans lequel itérer
//    const TTRegion& region     : La région sur laquelle itérer
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonneesXYZ>::SRIterateurRegion(const TCCoord& pasA, 
                                                       const TCCoord& pasB, 
                                                       const TCRegion* rP)
   : SRIterateurRegionBase<GOCoordonneesXYZ>(rP)
{
#ifdef MODE_DEBUG
   PRECONDITION(pasA != TCCoord());
   PRECONDITION(pasB != TCCoord());
   // ---  Vecteurs linéairement indépendants
   PRECONDITION(prodVect(pasA, pasB) != TCCoord());
   // ---  pasA et pasB ne doivent pas être dans le même quadrant
   PRECONDITION(!(pasA[0] > 0 && pasA[1] > 0 && pasB[0] >= 0 && pasB[1] >= 0));
   PRECONDITION(!(pasA[0] < 0 && pasA[1] > 0 && pasB[0] <= 0 && pasB[1] >= 0));
   PRECONDITION(!(pasA[0] < 0 && pasA[1] < 0 && pasB[0] <= 0 && pasB[1] <= 0));
   PRECONDITION(!(pasA[0] > 0 && pasA[1] < 0 && pasB[0] >= 0 && pasB[1] <= 0));
#endif  // ifdef MODE_DEBUG

   const TCEnveloppe& enveloppe = (TCEnveloppe&)regionP->reqEnveloppe();

   // ---  Cherche les points de départ de l'itération
   TCCoord depart = TCUtil::trouveDepart(pasA, pasB, enveloppe);

   // ---  Initialise les itérateurs
   const TCCoord cMin = TCCoord(enveloppe.reqCoordMin());
   const TCCoord cMax = TCCoord(enveloppe.reqCoordMax());
   iterA  = TCIterateurSpatial(cMin, cMax, depart, pasA); 
   iterB  = TCIterateurSpatial(cMin, cMax, depart, projettePas(pasA, pasB));
   
   // ---  Assigne dans les limites
   horsLimite = FAUX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur d'un itérateur de region 3D
//
// Description: 
//    Le constructeur public <code>SRIterateurRegion(...)</code> construit
//    un itérateur de région 3D. 
//
// Entrée:
//    const TTCoord& pasA        : le premier pas dans lequel itérer
//    const TTCoord& pasB        : le deuxième pas dans lequel itérer
//    const TTCoord& pasC        : le troisième pas dans lequel itérer
//    const TTRegion& region     : La région sur laquelle itérer
//
// Sortie:
//
// Notes:
//    Pas encore implanté: En fait on itère commme sur une région 2D
//
//************************************************************************
SRIterateurRegion<GOCoordonneesXYZ>::SRIterateurRegion(const TCCoord& pasA, 
                                                       const TCCoord& pasB, 
                                                       const TCCoord& pasC, 
                                                       const TCRegion* rP)
   : SRIterateurRegionBase<GOCoordonneesXYZ>(rP)
{
#ifdef MODE_DEBUG
   PRECONDITION(pasA != TCCoord());
   PRECONDITION(pasB != TCCoord());
   PRECONDITION(pasC != TCCoord());
   
   typedef TCCoord::TCCoord TCValeur;
   //trouver le det de la matrice des vecteurs... si = 0 alors dependant!!
   TCValeur det = pasA[0] * (pasB[1] * pasC[2] - pasB[2] * pasC[1]) 
                - pasA[1] * (pasB[0] * pasC[2] - pasB[2] * pasC[0]) 
                + pasA[2] * (pasB[0] * pasC[1] - pasB[1] * pasC[0]);
   PRECONDITION(det != 0);   
#endif  // ifdef MODE_DEBUG

/*
   YSe --- Ancien code à adapter
  iterA(m_bbox.reqInfGauche(), m_bbox.reqSupDroit(), pasA), 
  iterB(m_bbox.reqInfGauche(), m_bbox.reqSupDroit(), projettePas(pasA, pasB)), 
  iterB2(m_bbox.reqInfGauche(), m_bbox.reqSupDroit(), reqPasB2(pasA, pasB)), 
  iterC(m_bbox.reqInfGauche(), m_bbox.reqSupDroit(), pasC)                                           
*/
   //trouver les points de départ de l'itération
   //TTCoord depart   = trouveDepart  (pasA, pasB, pasC);
   //TTCoord departB2 = trouveDepartB2(pasA, pasB, reqPasB2(pasA, pasB));

   //iterA.asgEmplacement(depart);
   //iterB.asgEmplacement(depart);
   //iterB2.asgEmplacement(departB2);
   //iterC.asgEmplacement(depart);

   const TCEnveloppe& enveloppe = (TCEnveloppe&)regionP->reqEnveloppe();

   // ---  Cherche les points de départ de l'itération
   TCCoord depart = TCUtil::trouveDepart(pasA, pasB, enveloppe);

   // ---  Initialise les itérateurs
   const TCCoord cMin = TCCoord(enveloppe.reqCoordMin());
   const TCCoord cMax = TCCoord(enveloppe.reqCoordMax());
   iterA  = TCIterateurSpatial(cMin, cMax, depart, pasA); 
   iterB  = TCIterateurSpatial(cMin, cMax, depart, projettePas(pasA, pasB));
   
   // ---  Assigne dans les limites
   horsLimite = FAUX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Destructeur.
//
// Description: 
//    Le destructeur public <code>~SRIterateurRegion()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonneesXYZ>::~SRIterateurRegion()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire:  Retourne le pas B2
//
// Description: 
//    La fonction privée <code>reqPasB2(...)</code> détermine le pas de
//    l'itérateur B2 selon les autres pas.
//
// Entrée:
//    TCCoord pasA : le pas de l'itérateur A
//    TCCoord pasB : le pas de l'itérateur B
//
// Sortie:
//    TCCoord : le pas de l'itérateur B2
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonneesXYZ>::TCCoord 
SRIterateurRegion<GOCoordonneesXYZ>::reqPasB2(const TCCoord& pasA,
                                              const TCCoord& pasB)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   return TCUtil::reqPasB2(pasA, pasB);
}

//************************************************************************
// Sommaire:   Détermine le point de départ pour l'itérateur B2
//
// Description: 
//    La méthode virtuelle protégée <code>trouveDepartB2(...)</code> détermine
//    le point de départ de l'itérateur B2. L'itérateur B2 est perpendiculaire à 
//    l'itérateur B et permet de scanner le bounding box d'une région. La
//    coordonnée est déterminée selon les différents pas d'itération.
//
// Entrée:
//    const TCCoord& pasA           : le pas de l'itérateur A
//    const TCCoord& pasB           : le pas de l'itérateur B
//    const TCEnveloppe& enveloppe  : le bounding-box de la région
//
// Sortie:
//    TCCoord : la coordonnée de départ de l'itérateur B2
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonneesXYZ>::TCCoord 
SRIterateurRegion<GOCoordonneesXYZ>::trouveDepartB2(const TCCoord& pasA,
                                                    const TCCoord& pasB,
                                                    const TCEnveloppe& enveloppe)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   return TCUtil::trouveDepartB2(pasA, pasB, enveloppe);
}
