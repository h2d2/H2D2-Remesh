//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

#include <string>

// fonction Python servant à créer un module et lui ajouter du code
std::string importCode = std::string("def importCode(code, nom, module1=None, module2=None):\n") +
                         std::string("    import imp\n") +
                         std::string("    import sys\n") +
                         std::string("    module = imp.new_module(nom)\n") +
                         std::string("    sys.modules[nom] = module\n") +
                         std::string("    if (module1 != None): module.__dict__[module1.__name__] = module1\n") +
                         std::string("    if (module2 != None): module.__dict__[module2.__name__] = module2\n") +
                         std::string("    exec code in module.__dict__\n") +
                         std::string("    return module\n");
