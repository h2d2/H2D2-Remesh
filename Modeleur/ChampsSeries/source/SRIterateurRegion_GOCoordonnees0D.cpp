//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: $Id$
// Classe: SRIterateurRegion
//************************************************************************
#include "SRIterateurRegion_GOCoordonnees0D.h"

//************************************************************************
// Sommaire:  Constructeur un itérateur de fin.
//
// Description:
//    Le constructeur public <code>SRIterateurRegion</code> construit
//    un itérateur qui marque la fin de la région. C'est l'itérateur 
//    à utiliser comme marque de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonnees0D>::SRIterateurRegion()
 : SRIterateurRegionBase<GOCoordonnees0D>()
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur un itérateur de fin.
//
// Description:
//    Le destructeur public <code>SRIterateurRegion</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonnees0D>::~SRIterateurRegion()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire:  Retourne le pas B2
//
// Description: 
//    La fonction privée <code>reqPasB2(...)</code> détermine le pas de
//    l'itérateur B2 selon les autres pas.
//
// Entrée:
//    TCCoord pasA : le pas de l'itérateur A
//    TCCoord pasB : le pas de l'itérateur B
//
// Sortie:
//    TCCoord : le pas de l'itérateur B2
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonnees0D>::TCCoord 
SRIterateurRegion<GOCoordonnees0D>::reqPasB2(const TCCoord& /*pasA*/,
                                             const TCCoord& /*pasB*/)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   return TCCoord();
}

//************************************************************************
// Sommaire:   Détermine le point de départ pour l'itérateur B2
//
// Description: 
//    La fonction privée <code>trouveDepartB2(...)</code> détermine le point
//    de départ de l'itérateur B2. L'itérateur B2 est perpendiculaire à 
//    l'itérateur B et permet de scanner le bounding box d'une région. La
//    coordonnée est déterminée selon les différents pas d'itération.
//
// Entrée:
//    const TCCoord& pasA           : le pas de l'itérateur A
//    const TCCoord& pasB           : le pas de l'itérateur B
//    const TCEnveloppe& enveloppe  : le bounding-box de la région
//
// Sortie:
//    TTCoord : la coordonnée de départ de l'itérateur B2
//
// Notes:
//
//************************************************************************
SRIterateurRegion<GOCoordonnees0D>::TCCoord 
SRIterateurRegion<GOCoordonnees0D>::trouveDepartB2(const TCCoord& /*pasA*/,
                                                   const TCCoord& /*pasB*/,
                                                   const TCEnveloppe& enveloppe)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   return enveloppe.reqCoordMax();
}
