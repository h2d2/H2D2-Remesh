//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Classe : SRRegion
//************************************************************************

#include "SRRegion_GOCoordonnees0D.h"

//************************************************************************
// Sommaire:  
//    Constructeur par défaut.
//
// Description:
//    Le constructeur public Region construit une region nulle
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonnees0D>::SRRegion()
 : m_enveloppe()
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Constructeur copie
//
// Description: 
//    Constructeur copie de la classe.
//
// Entrée:
//    const Region& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonnees0D>::SRRegion(const TCSelf& obj)
 : m_enveloppe(obj.m_enveloppe)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonnees0D>::~SRRegion()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Opérateur d'assignation
//
// Description: 
//    Opérateur d'assignation de la classe.
//
// Entrée:
//    const SRRegion& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
SRRegion<GOCoordonnees0D>::TCSelf&
SRRegion<GOCoordonnees0D>::operator=(const TCSelf& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_enveloppe = obj.m_enveloppe;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  
//
// Description: 
//    La fonction publique estValide() retourne VRAI si la région est
//    valide. La région est valide si son enveloppe est également valide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen SRRegion<GOCoordonnees0D>::estValide() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_enveloppe.estValide();
}

//************************************************************************
// Sommaire:  
//    La fonction publique reqEnveloppe() retourne l'enveloppe 
//    (le bounding box) de la région.
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
const SRRegion<GOCoordonnees0D>::TCEnveloppe&
SRRegion<GOCoordonnees0D>::reqEnveloppe() const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return m_enveloppe;
}

//************************************************************************
// Sommaire:  
//    La fonction publique pointInterieur(...) détermine si un point se trouve
//    à l'intérieur de la région.
//
// Description: 
//    La fonction publique pointInterieur(...) détermine si un point se trouve
//    à l'intérieur de la région. Le teste est effectué avec les algorithmes de
//
// Entrée:
//    const TCCoord& point : le point à vérifier
//
// Sortie:
//
// Notes:
//************************************************************************
Booleen SRRegion<GOCoordonnees0D>::pointInterieur(const TCCoord& point) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_enveloppe.pointInterieur(point);
}

//************************************************************************
// Sommaire: 
//    Operateur==
//
// Description: 
//    L'opérateur public operator==(...) compare la région à celle passée
//    en argument. Deux régions sont égales si leur TePolygonSet sont égaux.
//
// Entrée:
//    const TCSelf& reg : une région
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen
SRRegion<GOCoordonnees0D>::operator== (const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return m_enveloppe == obj.m_enveloppe;
}  

//************************************************************************
// Sommaire: 
//    Opérateur !=
//
// Description: 
//    L'opérateur public operator!=(...) compare la région à celle passée
//    en argument. Deux régions sont égales si leur TePolygonSet sont égaux.
//
// Entrée:
//    const SRRegion<GOCoordonnees0D>& reg : une région
//
// Sortie:
//
// Notes:
//************************************************************************
Booleen
SRRegion<GOCoordonnees0D>::operator!= (const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return !(*this == obj);
}
