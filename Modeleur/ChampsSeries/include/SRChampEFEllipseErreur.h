//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Sommaire:    
//    Interface de définition de la classe SRChampEFEllipseErreur
//
// Description: 
//    Définit les "typedef" pour le type définit SRChampEFEllipseErreur
//    représentant un champs EllipseErreur constituer des type de données 
//    double et de EFTraits
//
// Attributs:
//
// Note:
//
//************************************************************************

#ifndef SRCHAMPEFELLIPSEERREUR_H_DEJA_INCLU
#define SRCHAMPEFELLIPSEERREUR_H_DEJA_INCLU

#include "sytypes.h"

#include "EFMaillage3D.h"
#include "SRChampElementsFinis.h"

typedef SRChampElementsFinis<GOEllipseErreur, EFMaillage3D>  SRChampEFEllipseErreur;
typedef SRChampEFEllipseErreur                        *SRChampEFEllipseErreurP;
typedef const SRChampEFEllipseErreur                  *ConstSRChampEFEllipseErreurP;

#endif //SRCHAMPEFELLIPSEERREUR_H_DEJA_INCLU

