//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRIterateurRegion
//
// Description:
//    La classe <code>SRIterateurRegion</code> représente un itérateur
//    sur une région 1D.
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef SRITERATEURREGION_GOCOORDONNEESX_H_DEJA_INCLU
#define SRITERATEURREGION_GOCOORDONNEESX_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRIterateurRegion.hf"
#include "SRIterateurRegionBase.h"

#include "GOCoord1.h"

template <>
class SRIterateurRegion<GOCoordonneesX>
   : public SRIterateurRegionBase<GOCoordonneesX>
{
public:
           SRIterateurRegion ();
           SRIterateurRegion (const TCCoord&, const TCRegion*);
          ~SRIterateurRegion ();

   static TCCoord reqPasB2          (const TCCoord&, const TCCoord&);
   static TCCoord trouveDepartB2    (const TCCoord&, const TCCoord&, const TCEnveloppe&);

protected:
   void    invariant         (ConstCarP) const;
};

//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
inline void SRIterateurRegion<GOCoordonneesX>::invariant(ConstCarP conditionP) const
{
   SRIterateurRegionBase<GOCoordonneesX>::invariant(conditionP);
}
#else
inline void SRIterateurRegion<GOCoordonneesX>::invariant(ConstCarP) const
{
}
#endif

#endif  // SRITERATEURREGION_GOCOORDONNEESX_H_DEJA_INCLU
