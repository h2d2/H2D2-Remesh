//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRIterateurChamp
//
// Description:
//    La classe <code>SRIterateurChamp2D</code> représente un itérateur
//    de champs sur une région 2D.
//    <p>
//    Un iterateur de champ est un iterateur de région qui, lorsque déréférencé,
//    retourne la paire <coordonnée, valeur> du champ auquel il réfère.
//
// Attributs:
//    ConstTTChampP  champP : pointeur sur le champ
//
// Notes:
// 1) Si msvc 7.0 supportait la spécialisation partielle des template on 
//    pourrait ecrire:
//          template <typename TTDonnee>
//          class SRIterateurChamp<TTDonnee, GOCoordonneesXYZ>
//             : public SRIterateurRegion<GOCoordonneesXYZ>
//    et se passer de la classe intermédiaire
//          class SRIterateurChampXYZ<TTDonnee>
// 2) SRChamp définit 3 méthodes reqDebut, suivant la dimension. Les 3
//    constructeur sont demandés même s'ils ne sont pas utilisés. Il
//    faudrait revoire SRChamp.
//************************************************************************
#ifndef SRITERATEURCHAMP_GOCOORDONNEESXYZ_H_DEJA_INCLU
#define SRITERATEURCHAMP_GOCOORDONNEESXYZ_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRIterateurChamp.h"
#include "SRIterateurRegion.h"
#include "GOCoord3.h"

#include "GOCoord3.h"         // A cause de msvc 7.0
#include "GOEllipseErreur.h"  // A cause de msvc 7.0

#include <utility>

#include "SRChamp.hf"

template <typename TTDonnee>
class SRIterateurChampXYZ
   : public SRIterateurRegion<GOCoordonneesXYZ>
{
public:
   typedef  GOCoordonneesXYZ              TTCoord;
   typedef  TTDonnee                      TCDonnee;

   typedef  SRIterateurChampXYZ<TTDonnee> TCSelf;
   typedef  SRIterateurRegion<TTCoord>    TCParent;

   typedef  SRChamp<TTDonnee, TTCoord>    TCChamp;
   typedef  TCChamp const*                TCConstChampP;
   typedef  std::pair<TTCoord, TTDonnee>  TCPair;

               SRIterateurChampXYZ();
               SRIterateurChampXYZ(const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
               SRIterateurChampXYZ(const TCCoord&, const TCCoord&, const TCChamp&);
               SRIterateurChampXYZ(const TCCoord&, const TCCoord&, const TCCoord&, const TCChamp&);
              ~SRIterateurChampXYZ();

   Booleen     operator==         (const TCSelf&) const;
   Booleen     operator!=         (const TCSelf&) const;

   TCPair      operator*          () const; 
  
protected:
   void        invariant          (ConstCarP) const;

private:
   TCConstChampP  champP;
};

template <>
class SRIterateurChamp<DReel, GOCoordonneesXYZ>
   : public SRIterateurChampXYZ<DReel>
{
public:
   typedef  SRIterateurChampXYZ<DReel> TCParent;

   SRIterateurChamp() : TCParent() {}
   SRIterateurChamp(const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, c) {}
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCCoord& p3,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, p3, c) {}
};
template <>
class SRIterateurChamp<GOCoordonneesXYZ, GOCoordonneesXYZ>
   : public SRIterateurChampXYZ<GOCoordonneesXYZ>
{
public:
   typedef  SRIterateurChampXYZ<GOCoordonneesXYZ> TCParent;

   SRIterateurChamp() : TCParent() {}
   SRIterateurChamp(const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, c) {}
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCCoord& p3,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, p3, c) {}
};
template <>
class SRIterateurChamp<GOEllipseErreur, GOCoordonneesXYZ>
   : public SRIterateurChampXYZ<GOEllipseErreur>
{
public:
   typedef  SRIterateurChampXYZ<GOEllipseErreur> TCParent;

   SRIterateurChamp() : TCParent() {}
   SRIterateurChamp(const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, c) {}
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCCoord& p3,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, p3, c) {}
};

//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
template <typename TTDonnee>
inline void SRIterateurChampXYZ<TTDonnee>::invariant(ConstCarP conditionP) const
{
   TCParent::invariant(conditionP);
}
#else
template <typename TTDonnee>
inline void SRIterateurChampXYZ<TTDonnee>::invariant(ConstCarP) const
{
}
#endif

#include "SRIterateurChamp_GOCoordonneesXYZ.hpp"

#endif  // SRITERATEURCHAMP_GOCOORDONNEESXYZ_H_DEJA_INCLU
