//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: SREnveloppe.h
//
// Classe:  SREnveloppe
//
// Description:
//    La classe template SREnveloppe représente le contour extérieur d'un
//    volume de 0 à N dimensions. Ce concept est utile pour approximer
//    rapidement certains calculs géométriques.
//    Ce contour (point, ligne, rectangle, cube...) est représenté par 
//    une paire de coordonnées rassemblant les valeurs minimales et  
//    maximales de chacune des dimensions du volume.
//    
//    La spécialisation du template pour les GOCoordonneesXYZ (spatial 2D)
//    correspond à un rectangle employé pour simplifier certains calculs
//    géométriques (concept d'Envelope de GEOS (OpenGIS)).
//
// Attributs:
//    TCPaire paire; Paire (std::pair) de coordonnées min. et maximales.
//
// Notes:
//    TTCoord doit être un type de coordonnées avec un TTDonnee pouvant être 
//    convertit en double. 
//    TTCoord doit implanter la méthode estDedans(), car SREnveloppe s'en 
//    sert dans sa méthode pointIntérieur.
//************************************************************************
#ifndef SRENVELOPPE_H_DEJA_INCLU
#define SRENVELOPPE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include <utility>

template <typename TTCoord>
class SREnveloppe
{
public:
   typedef SREnveloppe<TTCoord>        TCSelf;
   typedef std::pair<TTCoord,TTCoord>  TCPaire;
   typedef TTCoord                     TCCoord;

                  SREnveloppe    ();
                  SREnveloppe    (const TCSelf&);
                 ~SREnveloppe    ();
   SREnveloppe&   operator=      (const TCSelf&); 

   ERMsg          asgLimites     (const TCCoord&, const TCCoord&);
   Booleen        estValide      () const;
   Booleen        pointInterieur (const TCCoord&) const;

   TCCoord        reqCoordMin    () const;
   TCCoord        reqCoordMax    () const;  

   Booleen        operator==     (const TCSelf&) const;
   Booleen        operator!=     (const TCSelf&) const;
          
protected:
   void     invariant       (ConstCarP) const;

private:
   TCPaire paire;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
template <typename TTCoord>
inline void SREnveloppe<TTCoord>::invariant(ConstCarP /*conditionP*/) const
{
}
#else
template <typename TTCoord>
inline void SREnveloppe<TTCoord>::invariant(ConstCarP) const
{
}
#endif      

#include "SREnveloppe.hpp"

#include "SREnveloppe_GOCoordonneesXY.h"  //spécialisation
#include "SREnveloppe_GOCoordonneesXYZ.h" //spécialisation

#endif  // SRENVELOPPE_H_DEJA_INCLU
