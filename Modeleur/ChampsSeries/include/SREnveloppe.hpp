//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: SREnveloppe.hpp
// Classe : SREnveloppe
//************************************************************************
#ifndef SRENVELOPPE_HPP_DEJA_INCLU
#define SRENVELOPPE_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:  
//    Constructeur
//
// Description:
//    Constructeur par défaut
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SREnveloppe<TTCoord>::SREnveloppe()
: paire()
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Constructeur copie
//
// Description: 
//    Constructeur copie de la classe.
//
// Entrée:
//    const SREnveloppe& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SREnveloppe<TTCoord>::SREnveloppe(const TCSelf& obj)
   : paire(obj.paire)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SREnveloppe<TTCoord>::~SREnveloppe()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Opérateur d'assignation
//
// Description: 
//    Opérateur d'assignation de la classe.
//
// Entrée:
//    const SREnveloppe& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename SREnveloppe<TTCoord>::TCSelf&
SREnveloppe<TTCoord>::operator=(const TCSelf& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   paire = obj.paire;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  
//    Fonction publique asgLimites
//
// Description: 
//    La fonction publique asgLimites(...) assigne les coordonnées min
//    et max.
//
// Entrée:
//    TTCoord min : la coordonnée minimale pour toutes les dimensions
//    TTCoord max : la coordonnée maximale
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
ERMsg SREnveloppe<TTCoord>::asgLimites(const TCCoord& min, 
                                       const TCCoord& max)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   if (min < max || (min == max && min == TCCoord()))
   {
      paire.first = min;
      paire.second = max;
   }
   else
   {
      msg = ERMsg(ERMsg::ERREUR, "ERR_LIMITES_ENVELOPPE_INVALIDE");
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  
//    La fonction publique estValide() retourne VRAI si l'enveloppe
//    est valide.
//
// Description: 
//    Si l'enveloppe est valide (coordMin < coordMax, ...), 
//    estValide() retourne vrai.
//
// Entrée:
//
// Sortie:
//    Booleen : VRAI si valide, FAUX sinon
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen SREnveloppe<TTCoord>::estValide() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return ( reqCoordMin() <  reqCoordMax() ||
           (reqCoordMin() == reqCoordMax() && reqCoordMin() == TCCoord()));
}

//************************************************************************
// Sommaire:  
//    La fonction publique pointInterieur(...) détermine si un point se trouve
//    à l'intérieur de la boite.
//
// Description: 
//    Si le point est sur le contour, estDedans retourne vrai.
//
// Entrée:
//    const TCCoord& point : la coordonnée (point) à vérifier
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen SREnveloppe<TTCoord>::pointInterieur(const TCCoord& coord) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return coord.estDedans(paire.first, paire.second);
}

//************************************************************************
// Sommaire:  
//    La fonction publique reqCoordMin() retourne la coordonnée minimale
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename SREnveloppe<TTCoord>::TCCoord 
SREnveloppe<TTCoord>::reqCoordMin() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return paire.first;
}

//************************************************************************
// Sommaire: 
//    La fonction publique reqCoordMax() retourne la coordonnée maximale
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename SREnveloppe<TTCoord>::TCCoord 
SREnveloppe<TTCoord>::reqCoordMax() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return paire.second;
}

//************************************************************************
// Sommaire: 
//    Operateur==
//
// Description: 
//    L'opérateur public operator==(...) compare la boite à celle passée
//    en argument. Deux boites sont égales si leur paire est égale.
//
// Entrée:
//    const TCSelf& reg : une boite
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
SREnveloppe<TTCoord>::operator== (const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return paire == obj.paire;
}  

//************************************************************************
// Sommaire: 
//    Opérateur !=
//
// Description: 
//    L'opérateur public operator==(...) compare la boite à celle passée
//    en argument. Deux boites sont égales si leur paire est égale.
//
// Entrée:
//    const SREnveloppe<TTCoord>& reg : une boite
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
SREnveloppe<TTCoord>::operator!= (const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return !(*this == obj);
}  

#endif  // SRENVELOPPE_HPP_DEJA_INCLU
