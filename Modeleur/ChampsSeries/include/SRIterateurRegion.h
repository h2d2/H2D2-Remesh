//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRIterateurRegion
//
// Description:
//    La classe <code>SRIterateurRegion</code> représente un itérateur
//    sur une région 2D.
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef SRITERATEURREGION_H_DEJA_INCLU
#define SRITERATEURREGION_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

template <typename TTCoord> class SRIterateurRegion;

#include "SRIterateurRegion_GOCoordonnees0D.h"  // Spécialisation
#include "SRIterateurRegion_GOCoordonneesX.h"   // Spécialisation
#include "SRIterateurRegion_GOCoordonneesXY.h"  // Spécialisation
#include "SRIterateurRegion_GOCoordonneesXYZ.h" // Spécialisation

#endif  // SRITERATEURREGION_H_DEJA_INCLU
