//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRRegion
//
// Description:
//    La classe template SRRegion définit une région de 0 à n-dimensions. 
//    Une région peut être spatiale (telle qu'employée dans les Champs)
//    ou non spatiale (telle qu'employée dans les Séries, où elle
//    correspond alors à un espace de solution). 
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef SRREGION_GOCOORDONNEES0D_H_DEJA_INCLU
#define SRREGION_GOCOORDONNEES0D_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRRegion.hf"

#include "GOCoordonnees0D.h"
#include "SREnveloppe.h"

template <>
class SRRegion<GOCoordonnees0D>
{
public:
   typedef GOCoordonnees0D       TCCoord;
   typedef SRRegion<TCCoord>     TCSelf;
   typedef SREnveloppe<TCCoord>  TCEnveloppe;

                        SRRegion       ();
                        SRRegion       (const TCSelf&);
                       ~SRRegion       ();
   SRRegion&            operator=      (const TCSelf&); 

   const TCEnveloppe&   reqEnveloppe   () const;
   Booleen              estValide      () const;
   Booleen              pointInterieur (const TCCoord&) const;

   Booleen              operator==     (const TCSelf&) const;
   Booleen              operator!=     (const TCSelf&) const;
          
protected:
   void                 invariant      (ConstCarP) const;

private:
   TCEnveloppe m_enveloppe;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//************************************************************************
#ifdef MODE_DEBUG
inline void SRRegion<GOCoordonnees0D>::invariant(ConstCarP /*conditionP*/) const
{
}      
#else
inline void SRRegion<GOCoordonnees0D>::invariant(ConstCarP) const
{
}      
#endif

#endif  // SRREGION_GOCOORDONNEES0D_H_DEJA_INCLU
