//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRIterateurRegionBase
//
// Description:
//    La classe <code>SRIterateurRegionBase</code> représente les itérateurs
//    de région. Un itérateur de région sert à scanner une région. Une région
//    est un exemple de structure spatiale. Un itérateur de région est donc
//    composé de plusieurs itérateurs spatiaux.
//    <p>
//    Pour faciliter litération, litérateur de région se déplace sur le plus
//    petit rectangle contenant la région (« bounding box »).  À chaque
//    itération, litérateur vérifie sil se trouve dans la région sinon il doit
//    continuer à itérer.
//
// Attributs:
//    TCRegion const*    regionP      : La région sur laquelle itérer
//    TCIterateurSpatial iterA        : Le premier   itérateur spatial
//    TCIterateurSpatial iterB        : Le deuxième  itérateur spatial (en 2D)
//    TCIterateurSpatial iterC        : Le troisième itérateur spatial (en 3D)
//    Booleen            iterB2DejaConstruit: VRAI si l'itérateur B2 est construit
//    Booleen            horsLimite   : Indique si l'itérateur se trouve hors
//                                      des limites de l'itération
//
// Notes:
//    Les itérateurs de régions actuels ne supportent que les régions en
//    deux dimensions ou moins.
//
//************************************************************************

#ifndef SRITERATEURREGIONBASE_H_DEJA_INCLU
#define SRITERATEURREGIONBASE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SREnveloppe.h"
#include "SRIterateurSpatial.h"
#include "SRRegion.h"

#include <cstddef>
#include <iterator>

template <typename TTCoord>
class SRIterateurRegionBase
{
public:
   typedef SRIterateurRegionBase<TTCoord> TCSelf;
   typedef TTCoord                        TCCoord;

   typedef SREnveloppe<TCCoord>           TCEnveloppe;
   typedef SRIterateurSpatial<TCCoord>    TCIterateurSpatial;
   typedef SRRegion<TCCoord>              TCRegion;

   using iterator_category = std::bidirectional_iterator_tag;
   using value_type        = TTCoord;
   using difference_type   = std::ptrdiff_t;
   using pointer           = TTCoord*;
   using reference         = TTCoord&;

            SRIterateurRegionBase();
            SRIterateurRegionBase(const TCRegion* rP);
           ~SRIterateurRegionBase();

   TCSelf&  operator++           ();
   TCSelf   operator++           (int);
   TCSelf&  operator--           ();
   TCSelf   operator--           (int);

   TCCoord& operator*            () const;

   Booleen  operator==           (const TCSelf&) const;
   Booleen  operator!=           (const TCSelf&) const;

protected:
   void    invariant     (ConstCarP) const;

   static Booleen  asIterB2      (const TCCoord&);
   static TCCoord  projettePas   (const TCCoord&, const TCCoord&);
   static TCCoord  reqPasB2      (const TCCoord& p1, const TCCoord& p2);
   static TCCoord  trouveDepartB2(const TCCoord& p1, const TCCoord& p2, const TCEnveloppe& e);

   const TCRegion*    regionP;
   TCIterateurSpatial iterA;
   TCIterateurSpatial iterB;
   TCIterateurSpatial iterC;
   Booleen            iterB2DejaConstruit;
   Booleen            horsLimite;
};

//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
template <typename TTCoord>
inline void SRIterateurRegionBase<TTCoord>::invariant(ConstCarP conditionP) const
{
   INVARIANT((regionP == NUL && horsLimite) || regionP != NUL, conditionP);
}
#else
template <typename TTCoord>
inline void SRIterateurRegionBase<TTCoord>::invariant(ConstCarP) const
{
}
#endif

#include "SRIterateurRegionBase.hpp"

#endif  // SRITERATEURREGIONBASE_H_DEJA_INCLU
