//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Classe:  SRChampElementsFinis
//
// Description:
//    La classe SRChampElementsFinis représente les champs de type éléments
//    finis. Un champ éléments finis est une structure de données rassemblant
//    les valeurs nodales portées par un maillage éléments finis. 
//    (ON PARLE ICI D'UN MGMaillage AVEC COORDONNEES AU CHOIX, ET NON D'UN
//    EFMAillage avec coordonnées réelles 3d!!).
//    Les champs éléments finis sont donc toujours associés à un maillage. 
//    <p>
//    La classe est définie par le paramètre template TTTraits.  TTTrait  
//    regroupe les structures nécessaires aux calculs par la méthode des 
//    éléments finis.
//    <p>
//    Le trait utilisé doit définir les types suivants :
//    TCCoord:    type de coordonnées des noeuds et nombre de dimensions
//    TCNoeud:    type de noeuds
//
// Attributs:
//    std::vector<TTDonnee> valeursNodales : Les valeurs nodales du champ
//    TTMaillageP           maillageP      : Le maillage associé au champ
//
// Notes:
//
//************************************************************************

#ifndef SRCHAMPELEMENTSFINIS_H_DEJA_INCLU
#define SRCHAMPELEMENTSFINIS_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRChamp.h"

// ---  Fonction template pour tourner autour du problème sous MSVC
//      de la définition de méthodes template d'une classe template
namespace SRChampElementsFinisDetail
{
template <typename TTContainer, typename TTMaillage, typename TTIter>
ERMsg asgValeurs(TTContainer&, TTMaillage*, TTIter, TTIter);

#ifdef MODE_IMPORT_EXPORT
template <typename TTDonnee>
void init(TTDonnee&);
template <typename TTDonnee>
void exporte(TTDonnee&, FIFichier&);
template <typename TTDonnee>
void importe(TTDonnee&, FIFichier&);
#endif //MODE_IMPORT_EXPORT
}

template <typename TTDonnee, typename TTMaillage>
class SRChampElementsFinis : public SRChamp<TTDonnee, typename TTMaillage::TCCoord>
{
public:
   typedef  SRChampElementsFinis<TTDonnee, TTMaillage>       TCSelf;
   typedef  SRChamp<TTDonnee, typename TTMaillage::TCCoord>  TCParent;
   typedef  TTMaillage                                       TCMaillage;
   typedef  TTMaillage const*                                ConstTCMaillageP;
   typedef  typename TCMaillage::TCCoord                     TCCoord;

   typedef           std::vector<TTDonnee>          TCContainer;
   typedef  typename TCContainer::const_iterator    TCConstVnoIterateur;
   typedef  typename TCContainer::iterator          TCVnoIterateur;

                       SRChampElementsFinis();
                       SRChampElementsFinis(ConstTCMaillageP, typename TCSelf::ConstTCRegionP);
                       SRChampElementsFinis(const TCSelf&);
   virtual            ~SRChampElementsFinis();
   TCSelf&             operator=           (const TCSelf&);

/*
   virtual TTDonnee&   opDeriveX           (const TTCoord&) const;
   virtual TTDonnee&   opDeriveY           (const TTCoord&) const;
   virtual TTDonnee&   opDeriveZ           (const TTCoord&) const;

   virtual void        opDiffDivergence    (TTDonnee&, const TTCoord&); 
   virtual void        opDiffGradient      (CLVecteur&, CLVecteur&, 
                                            CLVecteur&, CLVecteur&); 
   virtual void        opDiffRotationnel   (CLVecteur&, CLVecteur&,
                                            CLVecteur&, CLVecteur&); 
*/
   virtual ERMsg       opIntegre           (typename TCSelf::TCDonnee&) const;

   virtual ERMsg       executeAlgorithme   (typename TCSelf::AlgorithmeChamp&);
   virtual ERMsg       executeAlgorithme   (typename TCSelf::ConstAlgorithmeChamp&) const;
   virtual ERMsg       reqValeur           (typename TCSelf::TCDonnee&, const typename SRChampElementsFinis::TCCoord&) const;

           TCSelf      operator-           () const;

           TCSelf      operator+           (const TCSelf&) const;
           TCSelf&     operator+=          (const TCSelf&);
           TCSelf      operator-           (const TCSelf&) const;
           TCSelf&     operator-=          (const TCSelf&);
           TCSelf      operator*           (const DReel&) const;
           TCSelf&     operator*=          (const DReel&);
           TCSelf      operator/           (const DReel&) const;
           TCSelf&     operator/=          (const DReel&);

           TCSelf      operator+           (const TCParent&) const;
           TCSelf&     operator+=          (const TCParent&);
           TCSelf      operator-           (const TCParent&) const;
           TCSelf&     operator-=          (const TCParent&);

           TCSelf      operator*           (const SRChamp<DReel, TCCoord>&) const;
           TCSelf&     operator*=          (const SRChamp<DReel, TCCoord>&);
           TCSelf      operator/           (const SRChamp<DReel, TCCoord>&) const;
           TCSelf&     operator/=          (const SRChamp<DReel, TCCoord>&);

         typename SRChampElementsFinis::TCDonnee& operator [](const EntierN&); 
   const typename SRChampElementsFinis::TCDonnee& operator [](const EntierN&) const;
   
   ERMsg                asgEpsilon  (const DReel&);
   template <typename TTIter>
   ERMsg                asgValeurs  (TTIter d, TTIter f) { return SRChampElementsFinisDetail::asgValeurs(valeursNodales, maillageP, d, f); }

   Booleen              estCharge   () const;
   EntierN              reqNbrValeursNodales() const;

   ConstTCMaillageP     reqMaillage () const;
   TCVnoIterateur       reqVnoDebut ();
   TCConstVnoIterateur  reqVnoDebut () const;
   TCVnoIterateur       reqVnoFin   ();
   TCConstVnoIterateur  reqVnoFin   () const;
/*
   iterateurNoeud           reqNoeudDebut();
   iterateurNoeudConst      reqNoeudDebut() const;
   iterateurNoeud           reqNoeudFin  ();
   iterateurNoeudConst      reqNoeudFin  () const;
*/
   typename SRChampElementsFinis::TCDonnee reqVnoMin() const;
   typename SRChampElementsFinis::TCDonnee reqVnoMax() const;
   
//   void asgMaillage(ConstTCMaillageP p){ maillageP = p;}

protected:
   virtual void             invariant   (ConstCarP) const;
   
private:
   ConstTCMaillageP         maillageP;
   TCContainer              valeursNodales;
   DReel                    epsilon;

#ifdef MODE_IMPORT_EXPORT
public:
      virtual void         exporte               (FIFichier&) const;
      virtual void         importe               (FIFichier&);
#endif //MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
protected:
      virtual void       ecrisVirtuel    (FIFichier&) const;
      virtual void       lisVirtuel      (FIFichier&);
#endif //MODE_PERSISTANT   
};

template <typename TTDonnee, typename TTMaillage>
inline SRChampElementsFinis<TTDonnee, TTMaillage>
operator* (const DReel& v, const SRChampElementsFinis<TTDonnee, TTMaillage>& o)
{
   return (o*v);
}

template <typename TTDonnee, typename TTMaillage>
inline SRChampElementsFinis<TTDonnee, TTMaillage>
operator+ (const SRChamp<TTDonnee, typename TTMaillage::TCCoord>& l, const SRChampElementsFinis<TTDonnee, TTMaillage>& r)
{
   return (r+l);
}

template <typename TTDonnee, typename TTMaillage>
inline SRChampElementsFinis<TTDonnee, TTMaillage>
operator- (const SRChamp<TTDonnee, typename TTMaillage::TCCoord>& l, const SRChampElementsFinis<TTDonnee, TTMaillage>& r)
{
   SRChampElementsFinis<TTDonnee, TTMaillage> retour = -r;
   retour += l;
   return retour;
}

template <typename TTDonnee, typename TTMaillage>
inline SRChampElementsFinis<TTDonnee, TTMaillage>
operator* (const SRChamp<DReel, typename TTMaillage::TCCoord>& l, const SRChampElementsFinis<TTDonnee, TTMaillage>& r)
{
   return (r*l);
}


//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
template <typename TTDonnee, typename TTMaillage>
inline void 
SRChampElementsFinis<TTDonnee, TTMaillage>::invariant(ConstCarP conditionP) const
{
   INVARIANT((valeursNodales.size() == 0) ||
             (maillageP != NUL && maillageP->reqNbrNoeud() == valeursNodales.size()), conditionP);
}
#else
template <typename TTDonnee, typename TTMaillage>
inline void 
SRChampElementsFinis<TTDonnee, TTMaillage>::invariant(ConstCarP) const
{
}
#endif

#include "SRChampElementsFinis.hpp"

#endif  // SRCHAMPELEMENTSFINIS_H_DEJA_INCLU
