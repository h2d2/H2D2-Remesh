//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRIterateurRegionUtil
//
// Description:
//    La classe <code>SRIterateurRegionUtil</code> regroupe des fonctions
//    utilitaires pour les itérateurs de région.
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef SRITERATEURREGIONUTIL_H_DEJA_INCLU
#define SRITERATEURREGIONUTIL_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SREnveloppe.h"

template <typename TTCoord>
class SRIterateurRegionUtil
{
public:
   typedef TTCoord               TCCoord;
   typedef SREnveloppe<TCCoord>  TCEnveloppe;

   static Booleen  asIterB2      (const TCCoord&);
   static TCCoord  projettePas   (const TCCoord&, const TCCoord&);
   static TCCoord  reqPasB2      (const TCCoord&, const TCCoord&);
   static TCCoord  trouveDepartB2(const TCCoord&, const TCCoord&, const TCEnveloppe&);
   static TCCoord  trouveDepart  (const TCCoord&, const TCCoord&, const TCEnveloppe&);
};

#include "SRIterateurRegionUtil.hpp"

#endif  // SRITERATEURREGIONUTIL_H_DEJA_INCLU
