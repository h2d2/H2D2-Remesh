//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: $Id$
// Classe:  SRIterateurChampXYZ
//************************************************************************
#ifndef SRITERATEURCHAMP_GOCOORDONNEESXYZ_HPP_DEJA_INCLU
#define SRITERATEURCHAMP_GOCOORDONNEESXYZ_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:   Constructeur par défaut, marque de fin.
//
// Description: 
//    Le constructeur public <code>SRIterateurChampXYZ()</code> construit
//    un itérateur qui marque la fin du champ. C'est l'itérateur à utiliser
//    comme marque de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee>
SRIterateurChampXYZ<TTDonnee>::SRIterateurChampXYZ()
   :  TCParent(),
      champP(NUL)  
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur d'un itérateur de champ .
//
// Description: 
//    Le constructeur public <code>SRIterateurChampXYZ(...)</code construit un
//    itérateur de champ . 
//
// Entrée:
//    TCCoord pasA  : le premier pas dans lequel itérer
//    TCCoord pasB  : le deuxième pas dans lequel itérer
//    TCChamp champ : le champ sur lequel itérer
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee>
SRIterateurChampXYZ<TTDonnee>::SRIterateurChampXYZ(const TCCoord& pasA, 
                                                   const TCCoord& pasB, 
                                                   const TCChamp& champ)
   :  TCParent(pasA, pasB, champ.reqRegion())
   ,  champP(&champ)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (!champP->pointInterieur(TCParent::operator*()))
   {
      ++(*this);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur d'un itérateur de champ .
//
// Description: 
//    Le constructeur public <code>SRIterateurChampXYZ(...)</code construit un
//    itérateur de champ . 
//
// Entrée:
//    TCCoord pasA  : le premier pas dans lequel itérer
//    TCCoord pasB  : le deuxième pas dans lequel itérer
//    TCChamp champ : le champ sur lequel itérer
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee>
SRIterateurChampXYZ<TTDonnee>::SRIterateurChampXYZ(const TCCoord& pasA, 
                                                   const TCCoord& pasB, 
                                                   const TCCoord& pasC, 
                                                   const TCChamp& champ)
   :  TCParent(pasA, pasB, pasC, champ.reqRegion())
   ,  champP(&champ)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (!champP->pointInterieur(TCParent::operator*()))
   {
      ++(*this);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur.
//
// Description: 
//    Le destructeur <code>~SRIterateurChampXYZ()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee>
SRIterateurChampXYZ<TTDonnee>::~SRIterateurChampXYZ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Comparaison pour l'inégalité.
//
// Description: 
//    L'opérateur public <code>operator!=(...)</code> compare l'objet à
//    l'itérateur passé en argument. Deux itérateurs sont égaux si 
//    1) leurs attributs sont égaux
//    2) ils sont hors limites
//
// Entrée:
//    const TTSelf&  obj      : Objet à comparer pour l'inégalité
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee>
inline Booleen
SRIterateurChampXYZ<TTDonnee>::operator!=(const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return !((*this) == obj);
}

//************************************************************************
// Sommaire:  Comparaison pour l'égalité.
//
// Description: 
//    L'opérateur public <code>operator==(...)</code> compare l'objet à
//    l'itérateur passé en argument. Deux itérateurs sont égaux si 
//    1) leurs attributs sont égaux
//    2) ils sont hors limites
//
// Entrée:
//    const TTSelf&  obj      : Objet à comparer pour l'égalité
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee>
Booleen SRIterateurChampXYZ<TTDonnee>::operator==(const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return (TCParent::operator==(obj) && champP == obj.champP)
          || ((*this).horsLimite && obj.horsLimite);
}

//************************************************************************
// Sommaire:  Retourne la position actuelle.
//
// Description:
//    L'opérateur public <code>operator*()</code> déréférence l'itérateur
//    et retourne la paire (position, valeur). Il n'est pas valide d'utiliser
//    cet opérateur sur un itérateur de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee>
typename SRIterateurChampXYZ<TTDonnee>::TCPair 
SRIterateurChampXYZ<TTDonnee>::operator*() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   
   TCDonnee valeur;
   const TTCoord& coord = TCParent::operator*();
   msg = champP->reqValeur(valeur, coord);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return TCPair(coord, valeur);
}

#endif  // SRITERATEURCHAMP_GOCOORDONNEESXYZ_HPP_DEJA_INCLU
