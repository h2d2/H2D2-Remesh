//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRIterateurChamp
//
// Description:
//    La classe <code>SRIterateurChamp</code> représente un itérateur
//    de champs sur une région 2D.
//    <p>
//    Un iterateur de champ est un iterateur de région qui, lorsque déréférencé,
//    retourne la paire <coordonnée, valeur> du champ auquel il réfère.
//
// Attributs:
//    ConstTTChampP  champP : pointeur sur le champ
//
// Notes:
//
//************************************************************************
#ifndef SRITERATEURCHAMP_H_DEJA_INCLU
#define SRITERATEURCHAMP_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

template <typename TTDonnee, typename TTCoord> class SRIterateurChamp;

#include "SRIterateurChamp_GOCoordonnees0D.h"   // Spécialisation
#include "SRIterateurChamp_GOCoordonneesX.h"    // Spécialisation
#include "SRIterateurChamp_GOCoordonneesXY.h"   // Spécialisation
#include "SRIterateurChamp_GOCoordonneesXYZ.h"  // Spécialisation

#endif  // SRITERATEURCHAMP_H_DEJA_INCLU
