//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
#ifndef SRITERATEURREGIONUTIL_HPP_DEJA_INCLU
#define SRITERATEURREGIONUTIL_HPP_DEJA_INCLU

#include <cmath>

//************************************************************************
// Sommaire:  Retourne le pas B2
//
// Description: 
//    La fonction privée <code>reqPasB2(...)</code> détermine le pas de
//    l'itérateur B2 selon les autres pas.
//
// Entrée:
//    TCCoord pasA : le pas de l'itérateur A
//    TCCoord pasB : le pas de l'itérateur B
//
// Sortie:
//    TCCoord : le pas de l'itérateur B2
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord SRIterateurRegionUtil<TTCoord>::reqPasB2(const TCCoord& pasA,
                                                 const TCCoord& pasB)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

#ifndef M_PI
   const double M_PI = 3.14159265358979323846;
#endif

   TCCoord axeX;
   TCCoord axeY;
   TCCoord pas;
   axeX[0] = 1;
   axeY[1] = 1; 

   // ---  Vecteur orthogonal à pasA
   TCCoord orthoA;
   orthoA[0] = -pasA[1];
   orthoA[1] =  pasA[0]; 

   // ---  Projection de pasB sur le vecteur orthoA
   ASSERTION(orthoA.norme() != 0);
   TCCoord projPasB = orthoA * (prodScal(pasB, orthoA)/prodScal(orthoA, orthoA));

   // ---  pasA < 90
   if (pasA[0] > 0 && pasA[1] >= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         pas[1] = tan(acos(prodScal(axeX, normalise(pasA)))) * pasB[0] * -1;
      }
      else
      {
         pas[0] = tan(acos(prodScal(axeY, normalise(pasA)))) * pasB[1] * -1;
      }
   }
   // ---  pasA < 180
   else if(pasA[0] <= 0  && pasA[1] > 0)
   {
      // ---  projPasB < 90
      if(projPasB[0] > 0 && projPasB[1] >= 0)
      {
         pas[1] = tan(M_PI - acos(prodScal(axeX, normalise(pasA)))) * pasB[0];
      }
      else
      {
         pas[0] = tan(acos(prodScal(axeY, normalise(pasA)))) * pasB[1];
      }
   }
   // ---  pasA < 360
   else if (pasA[0] >= 0 && pasA[1] < 0) 
   {
      // ---  projPasB < 90
      if (projPasB[0] > 0 && projPasB[1] >= 0)
      {
         pas[0] = tan(M_PI - acos(prodScal(axeY, normalise(pasA)))) * pasB[1];
      }
      else
      {
         pas[1] = tan(acos(prodScal(axeX, normalise(pasA)))) * pasB[0];
      }
   }
   // ---  pasA < 270
   else if (pasA[0] < 0  && pasA[1] <= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         pas[0] = tan(M_PI - acos(prodScal(axeY, normalise(pasA)))) * pasB[1] * -1;
      }
      else
      {
         pas[1] = tan(M_PI - acos(prodScal(axeX, normalise(pasA)))) * pasB[0] * -1;
      }
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return pas;
}

//************************************************************************
// Sommaire:   Détermine le point de départ de l'itération
//
// Description: 
//    La fonction privée <code>trouveDepart(...)</code> détermine
//    le point de départ d'une itération scannant la région. Le point de départ
//    est déterminé selon les différents pas d'itération.
//
// Entrée:
//    const TCCoord& pasA : le premier pas d'itération
//    const TCCoord& pasB : le deuxième pas d'itération
//    const TCEnveloppe& enveloppe : le bounding-box de la région
//
// Sortie:
//    TCCoord : la coordonnée de départ
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord 
SRIterateurRegionUtil<TTCoord>::trouveDepart(const TCCoord& pasA, 
                                             const TCCoord& pasB,
                                             const TCEnveloppe& enveloppe)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   TCCoord pointDepart;
   TCCoord supDroit  = TCCoord(enveloppe.reqCoordMax());
   TCCoord infGauche = TCCoord(enveloppe.reqCoordMin());
   TCCoord infDroit  = TCCoord(enveloppe.reqCoordMax().x(),
                               enveloppe.reqCoordMin().y());
   TCCoord supGauche = TCCoord(enveloppe.reqCoordMin().x(),
                               enveloppe.reqCoordMax().y());

   // ---  Vecteur orthogonal à pasA
   TCCoord orthoA;
   orthoA[0] = -pasA[1];
   orthoA[1] =  pasA[0]; 

   // ---  Projection de pasB sur le vecteur orthoA
   ASSERTION(orthoA.norme() != 0);
   TCCoord projPasB = orthoA * (prodScal(pasB, orthoA)/prodScal(orthoA, orthoA));

   // ---  pasA < 90
   if (pasA[0] > 0 && pasA[1] >= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         // ---  pasA = 0
         if (pasA[0] > 0 && pasA[1] == 0)
         {
            pointDepart = infGauche;
         }
         else
         {
            pointDepart = infDroit;
         }
      }
      else
      {
         pointDepart = supGauche;
      }
   }
   // ---  pasA < 180
   else if (pasA[0] <= 0  && pasA[1] > 0)
   {
      // ---  projPasB < 90
      if (projPasB[0] > 0 && projPasB[1] >= 0)
      {
         pointDepart = infGauche;
      }
      else
      {
         // ---  pasA = 90
         if (pasA[0] == 0 && pasA[1] != 0)
         {
            pointDepart = infDroit;
         }
         else
         {
            pointDepart = supDroit;
         }
      }
   }
   // ---  pasA < 360
   else if (pasA[0] >= 0 && pasA[1] < 0) 
   {
      // ---  projPasB < 90
      if (projPasB[0] > 0 && projPasB[1] >= 0)
      {
         // ---  pasA = 270
         if (pasA[0] == 0 && pasA[1] < 0)
         {
            pointDepart = supGauche;
         }
         else
         {
            pointDepart = infGauche;
         }
      }
      else
      {
         pointDepart = supDroit;
      }
   }
   // ---  pasA < 270
   else if (pasA[0] < 0  && pasA[1] <= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         pointDepart = infDroit;
      }
      else
      {
         // ---  pasA = 180
         if(pasA[0] < 0 && pasA[1] == 0)
         {
            pointDepart = supDroit;
         }
         else
         {
            pointDepart = supGauche;
         }
      }
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return pointDepart;
}

//************************************************************************
// Sommaire:   Détermine le point de départ pour l'itérateur B2
//
// Description: 
//    La fonction privée <code>trouveDepartB2(...)</code> détermine le point
//    de départ de l'itérateur B2. L'itérateur B2 est perpendiculaire à 
//    l'itérateur B et permet de scanner le bounding box d'une région. La
//    coordonnée est déterminée selon les différents pas d'itération.
//
// Entrée:
//    const TCCoord& pasA           : le pas de l'itérateur A
//    const TCCoord& pasB           : le pas de l'itérateur B
//    const TCEnveloppe& enveloppe  : le bounding-box de la région
//
// Sortie:
//    TCCoord : la coordonnée de départ de l'itérateur B2
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord 
SRIterateurRegionUtil<TTCoord>::trouveDepartB2(const TCCoord& pasA,
                                               const TCCoord& pasB,
                                               const TCEnveloppe& enveloppe)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

#ifndef M_PI
   const double M_PI = 3.14159265358979323846;
#endif

   TCCoord axeX;
   TCCoord axeY; 
   axeX[0] = 1;
   axeY[1] = 1;

   TCCoord supDroit  = TCCoord(enveloppe.reqCoordMax());
   TCCoord infGauche = TCCoord(enveloppe.reqCoordMin());
   TCCoord infDroit  = TCCoord(enveloppe.reqCoordMax().x(),
                               enveloppe.reqCoordMin().y());
   TCCoord supGauche = TCCoord(enveloppe.reqCoordMin().x(),
                               enveloppe.reqCoordMax().y());
   TCCoord departB2;

   DReel longRegion, restant;
   if (pasB[0] == 0)
   {
      longRegion = supDroit[0] - supGauche[0];
      Entier tmp = static_cast<Entier>(longRegion / std::fabs(pasB[1]));
      restant = (tmp+1) * std::fabs(pasB[1]) - longRegion;
   }
   else
   {
      longRegion = supGauche[1] - enveloppe.reqCoordMin()[1];
      Entier tmp = static_cast<Entier>(longRegion / std::fabs(pasB[0]));
      restant = (tmp+1) * std::fabs(pasB[0]) - longRegion;
   }

   // ---  Vecteur orthogonal à pasA
   TCCoord orthoA;
   orthoA[0] = -pasA[1];
   orthoA[1] =  pasA[0]; 

   // ---  Projection de pasB sur le vecteur orthoA
   ASSERTION(orthoA.norme() != 0);
   TCCoord projPasB = orthoA * (prodScal(pasB, orthoA)/prodScal(orthoA, orthoA));

   // ---  pasA < 90
   if (pasA[0] > 0 && pasA[1] >= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         DReel angle = acos(prodScal(axeY, normalise(pasA)));
         TCCoord coinRegion = TCCoord(enveloppe.reqCoordMin());

         departB2 = coinRegion;
         departB2[1] = coinRegion[1] + (restant / tan(angle));
      }
      else
      {
          DReel angle = acos(prodScal(normalise(pasB), normalise(pasA)));
          TCCoord coinRegion = TCCoord(enveloppe.reqCoordMin());

          departB2 = coinRegion;
          departB2[0] = coinRegion[0] + (restant * tan(M_PI - angle));
      }
   }
   // ---  pasA < 180
   else if(pasA[0] <= 0  && pasA[1] > 0)
   {
      // ---  projPasB < 90
      if (projPasB[0] > 0 && projPasB[1] >= 0)
      {
         DReel angle = acos(prodScal(axeX, normalise(pasA)));
         TCCoord coinRegion = infDroit;

         departB2 = coinRegion;
         departB2[1] = coinRegion[1] + (restant * tan(M_PI - angle));
      }
      else
      {
         DReel angle = acos(prodScal(axeY, normalise(pasA)));
         TCCoord coinRegion = infDroit;

         departB2 = coinRegion;
         departB2[0] = coinRegion[0] - (restant * tan(angle));
      }
   }
   // ---  pasA < 360
   else if (pasA[0] >= 0 && pasA[1] < 0) 
   {
      // ---  projPasB < 90
      if (projPasB[0] > 0 && projPasB[1] >= 0)
      {
         DReel angle = acos(prodScal(axeY, normalise(pasA)));
         TCCoord coinRegion = supGauche;

         departB2 = coinRegion;
         departB2[0] = coinRegion[0] + (restant * tan(M_PI - angle));
      }
      else
      {
         DReel angle = acos(prodScal(axeX, normalise(pasA)));
         TCCoord coinRegion = supGauche;

         departB2 = coinRegion;
         departB2[1] = coinRegion[1] - (restant * tan(angle));
      }
   }
   // ---  pasA < 270
   else if (pasA[0] < 0  && pasA[1] <= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         DReel angle = acos(prodScal(axeY, normalise(pasA)));
         TCCoord coinRegion = supDroit;

         departB2 = coinRegion;
         departB2[0] = coinRegion[0] - (restant * tan(M_PI - angle));
      }
      else
      {
         DReel angle = acos(prodScal(axeX, normalise(pasA)));
         TCCoord coinRegion = supDroit;

         departB2 = coinRegion;
         departB2[1] = coinRegion[1] - (restant * tan(M_PI - angle));
      }
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return departB2;
}

#endif   // ifndef SRITERATEURREGIONUTIL_HPP_DEJA_INCLU
