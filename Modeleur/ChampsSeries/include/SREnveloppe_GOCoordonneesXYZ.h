//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: SREnveloppe_GOCoordonneesXYZ.h
//
// Classe:  SREnveloppe template spécialisé pour les GOCoordonneesXYZ
//
// Description:
//    Spécialisation du template SREnveloppe pour les GOCoordonneesXYZ. 
//    Correspond à un rectangle employé pour simplifier certains calculs
//    géométriques (concept d'Envelope de GEOS (OpenGIS)).
//    La classe est définie dans GOEnveloppe.cpp, ce qui la rend utilisable
//    indépendemment du module ChampSeries. 
//
// Attributs:
//    TCPaire paire    std::pair de coordonnées contenant les valeurs
//                     x,y,z minimales et x,y,z maximales de l'enveloppe.
//
// Notes:
//    TTCoord doit être un type de coordonnées avec un TTDonnee pouvant être 
//    convertit en double. 
//************************************************************************
#ifndef SRENVELOPPE_GOCOORDONNEESXYZ_H_DEJA_INCLU
#define SRENVELOPPE_GOCOORDONNEESXYZ_H_DEJA_INCLU

/*
#ifndef MODULE_CHAMPSSERIES
#  define MODULE_CHAMPSSERIES 1
#endif
*/

#include "GOEnveloppe.h"
#include "GOCoord3.h"

template <>
class SREnveloppe<GOCoordonneesXYZ> : public GOEnveloppe
{
public:
      SREnveloppe() : GOEnveloppe() {}
      SREnveloppe(const SREnveloppe<GOCoordonneesXYZ>& o) : GOEnveloppe(o) {}

};     

#endif  // SRENVELOPPE_GOCOORDONNEESXYZ_H_DEJA_INCLU
