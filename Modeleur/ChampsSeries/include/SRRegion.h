//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: SRRegion.h
//
// Classe:  SRRegion
//
// Description:
//    La classe template SRRegion définit une région de 0 à n-dimensions. 
//    Une région peut être spatiale (telle qu'employée dans les Champs)
//    ou non spatiale (telle qu'employée dans les Séries, où elle
//    correspond alors à une espace de solution). 
//
//    La spécialisation du template pour les GOCoordonneesXYZ 
//    (voir SRRegion_GOCoordonneesXYZ)
//    correspond à une région spatiale compatible avec GEOS (OpenGIS).
//
// Attributs:
//    TePolygonSet multiPoly : un multi-polygone identifiant la région
//    SREnveloppe  enveloppe : l'enveloppe "bouding-box" de la région
//         --> l'obtenir du multipolygone directement lorsque TerraLib
//             sera abandonnée.
//
// Notes:
//    TTCoord doit être un type de coordonnées avec un TTDonnee pouvant être 
//    convertit en double. 
//    TTCoord doit être un type de coordonnées offrant la méthode estDedans.
//************************************************************************
#ifndef SRREGION_H_DEJA_INCLU
#define SRREGION_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRRegion.hf"

#include "SRRegion_GOCoordonnees0D.h"     // Spécialisation
#include "SRRegion_GOCoordonneesX.h"      // Spécialisation
#include "SRRegion_GOCoordonneesXY.h"     // Spécialisation
#include "SRRegion_GOCoordonneesXYZ.h"    // Spécialisation

#endif  // SRREGION_H_DEJA_INCLU
