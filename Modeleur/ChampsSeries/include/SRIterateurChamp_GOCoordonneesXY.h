//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRIterateurChamp
//
// Description:
//    La classe <code>SRIterateurChamp2D</code> représente un itérateur
//    de champs sur une région 2D.
//    <p>
//    Un iterateur de champ est un iterateur de région qui, lorsque déréférencé,
//    retourne la paire <coordonnée, valeur> du champ auquel il réfère.
//
// Attributs:
//    ConstTTChampP  champP : pointeur sur le champ
//
// Notes:
// 1) Si msvc 7.0 supportait la spécialisation partielle des template on 
//    pourrait ecrire:
//          template <typename TTDonnee>
//          class SRIterateurChamp<TTDonnee, GOCoordonneesXY>
//             : public SRIterateurRegion<GOCoordonneesXY>
//    et se passer de la classe intermédiaire
//          class SRIterateurChampXY<TTDonnee>
// 2) SRChamp définit 3 méthodes reqDebut, suivant la dimension. Les 3
//    constructeur sont demandés même s'ils ne sont pas utilisés. Il
//    faudrait revoire SRChamp.
//************************************************************************
#ifndef SRITERATEURCHAMP_GOCOORDONNEESXY_H_DEJA_INCLU
#define SRITERATEURCHAMP_GOCOORDONNEESXY_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRIterateurChamp.h"
#include "SRIterateurRegion.h"
#include "GOCoord2.h"

#include "GOCoord3.h"         // A cause de msvc 7.0
#include "GOEllipseErreur.h"  // A cause de msvc 7.0

#include <utility>

#include "SRChamp.hf"

template <typename TTDonnee>
class SRIterateurChampXY
   : public SRIterateurRegion<GOCoordonneesXY>
{
public:
   typedef  GOCoordonneesXY               TTCoord;
   typedef  TTDonnee                      TCDonnee;

   typedef  SRIterateurChampXY<TTDonnee>  TCSelf;
   typedef  SRIterateurRegion<TTCoord>    TCParent;

   typedef  SRChamp<TTDonnee, TTCoord>    TCChamp;
   typedef  TCChamp const*                TCConstChampP;
   typedef  std::pair<TTCoord, TTDonnee>  TCPair;

               SRIterateurChampXY ();
               SRIterateurChampXY (const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
               SRIterateurChampXY (const TCCoord&, const TCCoord&, const TCChamp&);
              ~SRIterateurChampXY ();

   Booleen     operator==         (const TCSelf&) const;
   Booleen     operator!=         (const TCSelf&) const;

   TCPair      operator*          () const; 
  
protected:
   void        invariant          (ConstCarP) const;

private:
   TCConstChampP  champP;
};

template <>
class SRIterateurChamp<DReel, GOCoordonneesXY>
   : public SRIterateurChampXY<DReel>
{
public:
   typedef  SRIterateurChampXY<DReel> TCParent;

   SRIterateurChamp() : TCParent() {}
   SRIterateurChamp(const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, c) {}
};
template <>
class SRIterateurChamp<GOCoordonneesXYZ, GOCoordonneesXY>
   : public SRIterateurChampXY<GOCoordonneesXYZ>
{
public:
   typedef  SRIterateurChampXY<GOCoordonneesXYZ> TCParent;

   SRIterateurChamp() : TCParent() {}
   SRIterateurChamp(const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, c) {}
};
template <>
class SRIterateurChamp<GOEllipseErreur, GOCoordonneesXY>
   : public SRIterateurChampXY<GOEllipseErreur>
{
public:
   typedef  SRIterateurChampXY<GOEllipseErreur> TCParent;

   SRIterateurChamp() : TCParent() {}
   SRIterateurChamp(const TCCoord&, const TCChamp&) { ASSERTION(FAUX); } // cf. Note
   SRIterateurChamp(const TCParent::TCCoord& p1,
                    const TCParent::TCCoord& p2,
                    const TCParent::TCChamp& c)
      : TCParent(p1, p2, c) {}
};

//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
template <typename TTDonnee>
inline void SRIterateurChampXY<TTDonnee>::invariant(ConstCarP conditionP) const
{
   TCParent::invariant(conditionP);
}
#else
template <typename TTDonnee>
inline void SRIterateurChampXY<TTDonnee>::invariant(ConstCarP) const
{
}
#endif

#include "SRIterateurChamp_GOCoordonneesXY.hpp"

#endif  // SRITERATEURCHAMP_GOCOORDONNEESXY_H_DEJA_INCLU
