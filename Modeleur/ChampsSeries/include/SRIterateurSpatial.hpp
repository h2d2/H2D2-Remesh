//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

#ifndef SRITERATEURSPATIAL_HPP_DEJA_INCLU
#define SRITERATEURSPATIAL_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:   Constructeur par défaut, marqueur de fin.
//
// Description:
//    Le constructeur public <code>SRIterateurSpatial()</code> construit
//    un itérateur qui marque la fin de l'espace sur lequel itérer. 
//    C'est l'itérateur à utiliser comme marque de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurSpatial<TTCoord>::SRIterateurSpatial()
 : m_horsLimite(VRAI)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur
//
// Description: 
//    Le constructeur public <code>SRIterateurSpatial(...)</code> construit
//    un objet avec un début, une fin et un pas. L'itérateur est initia
//
// Entrée:
//    TTCoord debut : le début de l'espace à itérer
//    TTCoord fin   : la fin de l'espace à itérer
//    TTCoord pas   : le pas d'itération
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurSpatial<TTCoord>::SRIterateurSpatial(const TTCoord& min_box, 
                                                const TTCoord& max_box, 
                                                const TTCoord& depart,
                                                const TTCoord& pas)
 : m_emplacement(depart),
   m_pas(pas), 
   m_min_box(min_box), 
   m_max_box(max_box), 
   m_horsLimite(FAUX)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Destructeur.
//
// Description: 
//    Le destructeur <code>~STIterateurSpatial()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurSpatial<TTCoord>::~SRIterateurSpatial()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Retourne la coordonnée minimum de l'espace à itérer.
//
// Description:
//    La méthode publique <code>reqMinBox()</code> retourne le début
//    de l'espace à itérer.
//
// Entrée:
//
// Sortie:
//    TTCoord : la coordonnée minimum de l'espace à itérer
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord SRIterateurSpatial<TTCoord>::reqMinBox() const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return m_min_box;
}

//************************************************************************
// Sommaire:   Retourne la coordonnée maximum de l'espace à itérer.
//
// Description:
//    La méthode publique <code>reqMaxBox()</code> retourne la fin de l'espace
//    à itérer.
//
// Entrée:
//
// Sortie:
//    TTCoord : la coordonnée maximum de l'espace à itérer
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord SRIterateurSpatial<TTCoord>::reqMaxBox() const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return m_max_box;
}

//************************************************************************
// Sommaire:  Retourne le pas d'itération.
//
// Description:
//    La méthode publique <code>reqPas()</code> retourne le pas d'itération.
//
// Entrée:
//
// Sortie:
//    TTCoord : le pas d'itération
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord SRIterateurSpatial<TTCoord>::reqPas() const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return m_pas;
}

//************************************************************************
// Sommaire:  Avance d'un pas d'itération (post-incrément).
//
// Description: 
//    L'opérateur public <code>operator++(int)</code> incrémente l'itérateur,
//    donc avance d'un pas d'itération vers la fin. Il n'est pas valide d'utiliser
//    cet opérateur sur un itérateur de fin.
//
// Entrée:
//    int : Indicateur de post-incrément.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline SRIterateurSpatial<TTCoord>
SRIterateurSpatial<TTCoord>::operator++(int)
{
#ifdef MODE_DEBUG
   PRECONDITION(! m_horsLimite);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TTSelf tmp = *this;
   ++(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return tmp;
}

//************************************************************************
// Sommaire:  Avance d'un pas d'itération (pré-incrément).
//
// Description: 
//    L'opérateur public <code>operator++()</code> incrémente l'itérateur, 
//    donc avance selon le pas d'itération. Il n'est pas valide d'utiliser
//    cet opérateur sur un itérateur de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurSpatial<TTCoord>&
SRIterateurSpatial<TTCoord>::operator++()
{
#ifdef MODE_DEBUG
   PRECONDITION(! m_horsLimite);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (! m_horsLimite)
   {
      m_emplacement += m_pas;
      if(!(m_emplacement.estDedans(m_min_box, m_max_box)))
      {
         m_emplacement = TTCoord();
         m_horsLimite  = VRAI;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  Recule d'un pas d'itération (post-décrément).
//
// Description: 
//    L'opérateur public <code>operator--(int)</code> décrémente l'itérateur,
//    donc recule selon le pas d'itération. Il n'est pas valide d'utiliser
//    cet opérateur sur un itérateur de fin.
//
// Entrée:
//    int : Indicateur de post-décrément.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline SRIterateurSpatial<TTCoord>
SRIterateurSpatial<TTCoord>::operator--(int)
{
#ifdef MODE_DEBUG
   PRECONDITION(! m_horsLimite);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TTSelf tmp = *this;
   --(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return tmp;
}

//************************************************************************
// Sommaire:  Recule d'un pas d'itération (pré-décrément).
//
// Description: 
//    L'opérateur public <code>operator--()</code> décrémente l'itérateur,
//    donc recule d'un pas d'itération en directon du début. Il n'est pas
//    valide d'utiliser cet opérateur sur un itérateur de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurSpatial<TTCoord>&
SRIterateurSpatial<TTCoord>::operator--()
{
#ifdef MODE_DEBUG
   PRECONDITION(! m_horsLimite);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (! m_horsLimite)
   {   
      m_emplacement -= m_pas;
      if(!(m_emplacement.estDedans(m_min_box, m_max_box)))
      {
         m_emplacement = TTCoord();
         m_horsLimite  = VRAI;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  Retourne la position actuelle.
//
// Description:
//    L'opérateur public <code>operator*()</code> déréférence l'itérateur
//    et retourne la position de l'itérateur. Il n'est pas valide d'utiliser
//    cet opérateur sur un itérateur de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord& SRIterateurSpatial<TTCoord>::operator*() const
{
#ifdef MODE_DEBUG
   PRECONDITION(! m_horsLimite);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return (m_valeur = m_emplacement);
}

//************************************************************************
// Sommaire:  Comparaison pour l'inégalité.
//
// Description: 
//    L'opérateur public <code>operator!=(...)</code> compare l'objet à
//    l'itérateur passé en argument. Deux itérateurs sont égaux si 
//    1) leurs attributs sont égaux
//    2) ils sont hors limites
//
// Entrée:
//    const TTSelf&  obj      : Objet à comparer pour l'inégalité
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen 
SRIterateurSpatial<TTCoord>::operator!=(const TTSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return !((*this) == obj);
}

//************************************************************************
// Sommaire:  Comparaison pour l'égalité.
//
// Description: 
//    L'opérateur public <code>operator==(...)</code> compare l'objet à
//    l'itérateur passé en argument. Deux itérateurs sont égaux si 
//    1) leurs attributs sont égaux
//    2) ils sont hors limites
//
// Entrée:
//    const TTSelf&  obj      : Objet à comparer pour l'égalité
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen SRIterateurSpatial<TTCoord>::operator==(const TTSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return (m_min_box == obj.m_min_box && 
           m_max_box == obj.m_max_box &&
           m_emplacement == obj.m_emplacement &&
           m_pas == obj.m_pas) || 
          (m_horsLimite && obj.m_horsLimite);
}

#endif  // SRITERATEURSPATIAL_HPP_DEJA_INCLU
