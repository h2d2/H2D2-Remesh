//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// $Id$
//************************************************************************

#ifndef SRITERATEURREGIONBASE_HPP_DEJA_INCLU
#define SRITERATEURREGIONBASE_HPP_DEJA_INCLU

#include <cmath>

//************************************************************************
// Sommaire:  Constructeur un itérateur de fin.
//
// Description:
//    Le constructeur public <code>SRIterateurRegionBase</code> construit
//    un itérateur qui marque la fin de la région. C'est l'itérateur 
//    à utiliser comme marque de fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurRegionBase<TTCoord>::SRIterateurRegionBase()
   : regionP(NUL)
   , iterB2DejaConstruit(FAUX)
   , horsLimite(VRAI)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur un itérateur de fin.
//
// Description:
//    Le constructeur public <code>SRIterateurRegionBase</code> construit
//    un itérateur avec une région.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurRegionBase<TTCoord>::SRIterateurRegionBase(const TCRegion* rP)
   : regionP(rP)
   , iterB2DejaConstruit(FAUX)
   , horsLimite(VRAI)
{
#ifdef MODE_DEBUG
   PRECONDITION(rP != NUL);
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Destructeur.
//
// Description: 
//    Le destructeur public <code>~SRIterateurRegionBase()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurRegionBase<TTCoord>::~SRIterateurRegionBase()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   regionP = NUL;
}

//************************************************************************
// Sommaire:  Retourne VRAI si on a besoin de l'itérateur B2.
//
// Description: 
//    La fonction privée <code>asIterB2(...)</code> retourne VRAI si
//    l'itérateur B doit être continué par un itérateur B2 pour couvrire
//    toute la région.
//
// Entrée:
//    const TCCoord& pasA        : le pas de l'itérateur A
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen SRIterateurRegionBase<TTCoord>::asIterB2(const TCCoord& pasA)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   Booleen retour = VRAI;
   if (pasA[0] == 0 || pasA[1] == 0)
   {
      retour = FAUX;
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: Projette le pasB sur l'axe x ou y 
//
// Description: 
//    La fonction privée <code>projettePas(...)</code> détermine le pas
//    de l'itérateur B sur un axe (x ou y) selon le pas de B.
//
// Entrée:
//    const TCCoord& pasA : le pas de l'itérateur A
//    const TCCoord& pasB : le pas de l'itérateur B
//
// Sortie:
//    TCCoord : le pas de l'itérateur B sur un axe
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord SRIterateurRegionBase<TTCoord>::projettePas(const TCCoord& pasA, 
                                                    const TCCoord& pasB)                                   
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   TCCoord axeX;
   TCCoord axeY;
   TCCoord nouvPasB;
   axeX[0] = 1;
   axeY[1] = 1; 

   // ---  Vecteur orthogonal à pasA
   TCCoord orthoA;
   orthoA[0] = -pasA[1];
   orthoA[1] =  pasA[0]; 

   // ---  Projection de pasB sur le vecteur orthoA
   ASSERTION(orthoA.norme() != 0);
   TCCoord projPasB = orthoA * (prodScal(pasB, orthoA)/prodScal(orthoA, orthoA));

   // ---  pasA < 90
   if (pasA[0] > 0 && pasA[1] >= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         // ---  pasA = 0
         if (pasA[0] > 0 && pasA[1] == 0)
         {
            nouvPasB = axeY * (prodScal(pasB, axeY)/prodScal(axeY, axeY));
         }
         else
         {
            nouvPasB = axeX * (prodScal(pasB, axeX)/prodScal(axeX, axeX));
         }
      }
      else
      {
         nouvPasB = axeY * (prodScal(pasB, axeY)/prodScal(axeY, axeY));
      }
   }
   // ---  pasA < 180
   else if (pasA[0] <= 0  && pasA[1] > 0)
   {
      // ---  projPasB < 90
      if (projPasB[0] > 0 && projPasB[1] >= 0)
      {
         nouvPasB = axeX * (prodScal(pasB, axeX)/prodScal(axeX, axeX));
      }
      else
      {
         // ---  pasA = 90
         if (pasA[0] == 0 && pasA[1] != 0)
         {
            nouvPasB = axeX * (prodScal(pasB, axeX)/prodScal(axeX, axeX));
         }
         else
         {
            nouvPasB = axeY * (prodScal(pasB, axeY)/prodScal(axeY, axeY));
         }
      }
   }
   // ---  pasA < 360
   else if (pasA[0] >= 0 && pasA[1] < 0) 
   {
      // ---  projPasB < 90
      if (projPasB[0] > 0 && projPasB[1] >= 0)
      {
         // ---  pasA = 270
         if (pasA[0] == 0 && pasA[1] < 0)
         {
            nouvPasB = axeX * (prodScal(pasB, axeX)/prodScal(axeX, axeX));
         }
         else
         {
            nouvPasB = axeY * (prodScal(pasB, axeY)/prodScal(axeY, axeY));
         }
      }
      else
      {
         nouvPasB = axeX * (prodScal(pasB, axeX)/prodScal(axeX, axeX));
      }
   }
   // ---  pasA < 270
   else if (pasA[0] < 0  && pasA[1] <= 0)
   {
      // ---  projPasB < 180
      if (projPasB[0] <= 0  && projPasB[1] > 0)
      {
         nouvPasB = axeY * (prodScal(pasB, axeY)/prodScal(axeY, axeY));
      }
      else
      {
         //pasA = 180
         if (pasA[0] < 0 && pasA[1] == 0)
         {
            nouvPasB = axeY * (prodScal(pasB, axeY)/prodScal(axeY, axeY));
         }
         else
         {
            nouvPasB = axeX * (prodScal(pasB, axeX)/prodScal(axeX, axeX));
         }
      }
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return nouvPasB;
}

//************************************************************************
// Sommaire:  Retourne le pas B2
//
// Description: 
//    La fonction privée <code>reqPasB2(...)</code> détermine le pas de
//    l'itérateur B2 selon les autres pas. L'appel est redispatché à
//    la l'itérateur de région spécialisé.
//
// Entrée:
//    TCCoord pasA : le pas de l'itérateur A
//    TCCoord pasB : le pas de l'itérateur B
//
// Sortie:
//    TCCoord : le pas de l'itérateur B2
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename SRIterateurRegionBase<TTCoord>::TCCoord
SRIterateurRegionBase<TTCoord>::reqPasB2(const TCCoord& pasA, const TCCoord& pasB)
{
   return SRIterateurRegion<TCCoord>::reqPasB2(pasA, pasB);
}

//************************************************************************
// Sommaire:   Détermine le point de départ pour l'itérateur B2
//
// Description: 
//    La fonction privée <code>trouveDepartB2(...)</code> détermine le point
//    de départ de l'itérateur B2. L'itérateur B2 est perpendiculaire à 
//    l'itérateur B et permet de scanner le bounding box d'une région. La
//    coordonnée est déterminée selon les différents pas d'itération.
//
// Entrée:
//    const TCCoord& pasA           : le pas de l'itérateur A
//    const TCCoord& pasB           : le pas de l'itérateur B
//    const TCEnveloppe& enveloppe  : le bounding-box de la région
//
// Sortie:
//    TTCoord : la coordonnée de départ de l'itérateur B2
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename SRIterateurRegionBase<TTCoord>::TCCoord
SRIterateurRegionBase<TTCoord>::trouveDepartB2(const TCCoord& pasA,
                                               const TCCoord& pasB,
                                               const TCEnveloppe& enveloppe)
{
   return SRIterateurRegion<TCCoord>::trouveDepartB2(pasA, pasB, enveloppe);
}

//************************************************************************
// Sommaire:  Avance d'un pas d'itération (post-incrément).
//
// Description: 
//    L'opérateur public <code>operator++(...)</code> incrémente l'itérateur,
//    donc avance selon les pas dans une des directions prévues.
//
// Entrée:
//    int : Indicateur de post-incrément.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline SRIterateurRegionBase<TTCoord>
SRIterateurRegionBase<TTCoord>::operator++(int)
{
#ifdef MODE_DEBUG
   PRECONDITION(! horsLimite);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCSelf tmp = *this;
   ++(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return tmp;
}

//************************************************************************
// Sommaire:  Avance d'un pas d'itération (pré-incrément).
//
// Description: 
//    L'opérateur public <code>operator++()</code> incrémente l'itérateur,
//    donc avance selon les pas dans une des directions prévues.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurRegionBase<TTCoord>&
SRIterateurRegionBase<TTCoord>::operator++()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (! horsLimite)
   {
      const TCIterateurSpatial iterFin;

      ASSERTION(iterA != iterFin);
      if (++iterA != iterFin)
      {
      }
      else
      {
         if (++iterB != iterFin)
         {
            iterA = TCIterateurSpatial(iterA.reqMinBox(), iterA.reqMaxBox(), *iterB, iterA.reqPas());
         }
         else
         {
            if (asIterB2(iterA.reqPas()) && ! iterB2DejaConstruit)
            {
               const TCEnveloppe& enveloppe = (TCEnveloppe&)regionP->reqEnveloppe();
               const TCCoord pasB2 = reqPasB2      (iterA.reqPas(), iterB.reqPas());
               const TCCoord depB2 = trouveDepartB2(iterA.reqPas(), iterB.reqPas(), enveloppe);
               iterA = TCIterateurSpatial(iterA.reqMinBox(), iterA.reqMaxBox(), depB2, iterA.reqPas());
               iterB = TCIterateurSpatial(iterB.reqMinBox(), iterB.reqMaxBox(), depB2, pasB2); 
               iterB2DejaConstruit = VRAI;
            }
            else
            {
               horsLimite = VRAI;
            }
         }
      }
   }

   if (! horsLimite && !regionP->pointInterieur(*iterA))
   {
      ++(*this);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  Recule d'un pas d'itération (post-décrément).
//
// Description: 
//    L'opérateur public <code>operator--(...)</code> décrémente l'itérateur,
//    donc avance à l'opposé de l'une des directions prévues.
//
// Entrée:
//    int : Indicateur de post-décrément.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline SRIterateurRegionBase<TTCoord>
SRIterateurRegionBase<TTCoord>::operator--(int)
{
#ifdef MODE_DEBUG
   PRECONDITION(! horsLimite);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCSelf tmp = *this;
   ++(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return tmp;
}

//************************************************************************
// Sommaire:  Recule d'un pas d'itération (pré-décrément).
//
// Description: 
//    L'opérateur public <code>operator--()</code> décrémente l'itérateur,
//    donc avance à l'opposé de l'une des directions prévues.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
SRIterateurRegionBase<TTCoord>&
SRIterateurRegionBase<TTCoord>::operator--()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (! horsLimite)
   {
      const TCIterateurSpatial iterFin;

      ASSERTION(iterA != iterFin);
      if (--iterA != iterFin)
      {
      }
      else
      {
         if (--iterB != iterFin)
         {
            iterA = TCIterateurSpatial(iterA.reqMinBox(), iterA.reqMaxBox(), *iterB, iterA.reqPas());
         }
         else
         {
            if (asIterB2(iterA.reqPas()) && ! iterB2DejaConstruit)
            {
               const TCEnveloppe& enveloppe = regionP->reqEnveloppe();
               const TCCoord pasB2 = reqPasB2      (iterA.reqPas(), iterB.reqPas());
               const TCCoord depB2 = trouveDepartB2(iterA.reqPas(), iterB.reqPas(), enveloppe);
               iterA = TCIterateurSpatial(iterA.reqMinBox(), iterA.reqMaxBox(), depB2, iterA.reqPas());
               iterB = TCIterateurSpatial(iterB.reqMinBox(), iterB.reqMaxBox(), depB2, pasB2); 
               iterB2DejaConstruit = VRAI;
            }
            else
            {
               horsLimite = VRAI;
            }
         }
      }
   }

   if (! horsLimite && !regionP->pointInterieur(*iterA))
   {
      --(*this);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  Retourne la position actuelle.
//
// Description:
//    L'opérateur public <code>operator*()</code> déréférence l'itérateur
//    et retourne la position de l'itérateur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    
//************************************************************************
template <typename TTCoord>
TTCoord& SRIterateurRegionBase<TTCoord>::operator*() const
{
#ifdef MODE_DEBUG
   PRECONDITION(! horsLimite);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   
   return *iterA;
}

//************************************************************************
// Sommaire:  Comparaison pour l'inégalité.
//
// Description: 
//    L'opérateur public <code>operator!=(...)</code> compare l'objet à
//    l'itérateur passé en argument. Deux itérateurs sont égaux si 
//    1) leurs attributs sont égaux
//    2) ils sont hors limites
//
// Entrée:
//    const TCSelf&  obj      : Objet à comparer pour l'inégalité
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen 
SRIterateurRegionBase<TTCoord>::operator!=(const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return !((*this) == obj);
}

//************************************************************************
// Sommaire:  Comparaison pour l'égalité.
//
// Description: 
//    L'opérateur public <code>operator==(...)</code> compare l'objet à
//    l'itérateur passé en argument. Deux itérateurs sont égaux si 
//    1) leurs attributs sont égaux
//    2) ils sont hors limites
//
// Entrée:
//    const TCSelf&  obj      : Objet à comparer pour l'égalité
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen SRIterateurRegionBase<TTCoord>::operator==(const TCSelf& obj)const
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return (iterA == obj.iterA && 
           iterB == obj.iterB &&
           iterC == obj.iterC &&
           regionP == obj.regionP &&
           horsLimite == obj.horsLimite) ||
          (horsLimite && obj.horsLimite);
}

#endif  // SRITERATEURREGIONBASE_HPP_DEJA_INCLU
