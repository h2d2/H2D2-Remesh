//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Classe:  SRIterateurSpatial
//
// Description:
//    La classe <code>SRIterateurSpatial</code> représente un itérateur
//    permettant de se déplacer dans lespace.
//    <p>
//    Un itérateur spatial se déplace selon un pas ditération sur un espace
//    défini avec une coordonnée maximale et une coordonnée minimale.  Le type
//    de coordonnées utilisé doit fournir une fonction estDedans(à) retournant
//    vrai si une coordonnée est entre deux autres coordonnées et faux sinon.
//    <p>
//    Un itérateur spatial est un itérateur bidirectionnel.
//
// Attributs:
//   TTCoord m_valeur      : Valeur pour la déréférence
//   TTCoord m_emplacement : L'emplacement de l'itérateur dans l'espace
//   TTCoord m_pas         : Le pas d'itération
//   TTCoord m_min_box     : La coordonnée minimum de l'espace sur lequel on itère
//   TTCoord m_fin         : La coordonnée maximum de l'espace sur lequel on itère
//   Booleen m_horsLimite  : Indique si l'itérateur se trouve hors des limites
//                           de l'itération
//
// Notes:
//
//************************************************************************

#ifndef SRITERATEURSPATIAL_H_DEJA_INCLU
#define SRITERATEURSPATIAL_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include <cstddef>
#include <iterator>

template <typename TTCoord>
class SRIterateurSpatial
{
public:
   typedef SRIterateurSpatial<TTCoord>  TTSelf;

   using iterator_category = std::bidirectional_iterator_tag;
   using value_type        = TTCoord;
   using difference_type   = std::ptrdiff_t;
   using pointer           = TTCoord*;
   using reference         = TTCoord&;

              SRIterateurSpatial  ();
              SRIterateurSpatial  (const TTCoord&,
                                   const TTCoord&,
                                   const TTCoord&,
                                   const TTCoord&);
              ~SRIterateurSpatial ();

   TTCoord    reqMinBox           () const;
   TTCoord    reqMaxBox           () const;
   TTCoord    reqPas              () const;

   TTSelf&    operator++          ();
   TTSelf     operator++          (int);
   TTSelf&    operator--          ();
   TTSelf     operator--          (int);

   TTCoord&   operator*           () const;

   Booleen    operator==          (const TTSelf&) const;
   Booleen    operator!=          (const TTSelf&) const;

protected:
   void       invariant           (ConstCarP) const;

private:
   mutable TTCoord m_valeur;
           TTCoord m_emplacement;
           TTCoord m_pas;
           TTCoord m_min_box;
           TTCoord m_max_box;
           Booleen m_horsLimite;
};

//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
template <typename TTCoord>
inline void SRIterateurSpatial<TTCoord>::invariant(ConstCarP conditionP) const
{
   INVARIANT(m_pas != TTCoord() || m_horsLimite == VRAI, conditionP);
}
#else
template <typename TTCoord>
inline void SRIterateurSpatial<TTCoord>::invariant(ConstCarP) const
{
}
#endif

#include "SRIterateurSpatial.hpp"

#endif  // SRITERATEURSPATIAL_H_DEJA_INCLU
