//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

#ifndef SRCHAMP_HPP_DEJA_INCLU
#define SRCHAMP_HPP_DEJA_INCLU

#include <algorithm>
//************************************************************************
// Sommaire:  
//    Constructeur de champ
//
// Description:
//    Le constructeur public <code>SRChamp()</code> constuit un champ vide,
//    non associé à une région.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
SRChamp<TTDonnee, TTCoord>::SRChamp()
   : m_regionP(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Constructeur avec une région en paramètre
//
// Description:
//    Le constructeur public <code>SRChamp()</code> construit un champ 
//    avec une région associée.
//
// Entrée:
//    ConstTCRegionP regP     : la région associée au champ
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
SRChamp<TTDonnee, TTCoord>::SRChamp(ConstTCRegionP regP)
   : m_regionP(regP)
{
#ifdef MODE_DEBUG
   PRECONDITION(regP != NUL);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Constructeur copie
//
// Description:
//    Le constructeur public <code>SRChamp()</code> construit un champ 
//    à partir d'un autre champ.
//
// Entrée:
//    const TCSelf& obj     : un autre champ
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
SRChamp<TTDonnee, TTCoord>::SRChamp(const TCSelf& obj)
   : m_regionP(obj.m_regionP)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
//************************************************************************
// Sommaire:   Destructeur.
//
// Description: 
//    Le destructeur public <code>~SRChamp()</code> désassocie le champ
//    de la région.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTTraits>
SRChamp<TTDonnee, TTTraits>::~SRChamp()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   m_regionP = NUL;
}

//************************************************************************
// Sommaire:  
//    Constructeur avec une région en paramètre
//
// Description:
//    Le constructeur public <code>SRChamp()</code> construit un champ 
//    avec une région associée.
//
// Entrée:
//    ConstTCRegionP regP     : la région associée au champ
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
SRChamp<TTDonnee, TTCoord>&
SRChamp<TTDonnee, TTCoord>::operator=(const TCSelf& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   if (this != &obj)
   {
      m_regionP = obj.m_regionP;
   }

   return *this;
}


//************************************************************************
// Sommaire: Assigne l'information à l'objet.
//
// Description:
//    Cette méthode permet d'assigner la structure d'information décrivant
//    cet objet.
//
// Entrée:
//    const CQInfoDonneeProjet& prmInfo: la structure d'information à assigner.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
inline void SRChamp<TTDonnee, TTCoord>::asgInfoDonnee(const CQInfoDonneeProjet& prmInfo)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   info = prmInfo;

#ifdef MODE_DEBUG
   POSTCONDITION(info == prmInfo);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: 
//    Retourne VRAI si le point est à l'intérieur du champ
//
// Description: 
//    La méthode publique <code>pointInterieur(...)</code> détermine 
//    si le point est à l'intérieur du champ
//
// Entrée:
//    TCCoord point : un point de l'espace
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
Booleen SRChamp<TTDonnee, TTCoord>::pointInterieur(const TCCoord& point) const 
{
#ifdef MODE_DEBUG
   PRECONDITION(m_regionP != NUL);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return m_regionP->pointInterieur(point);
}

//************************************************************************
// Sommaire: Retoure la structure d'information.
//
// Description:
//    Retourne la structure d'information contenu à l'intérieur de cet objet.
//
// Entrée:
//
// Sortie:
//     CQInfoDonneeProjet    :  Strcuture d'information
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
inline CQInfoDonneeProjet SRChamp<TTDonnee, TTCoord>::reqInfoDonnee() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return info;
}
#endif   // MODE_PERSISTANT


//************************************************************************
// Sommaire: Retoure une référence aux paramètres du vno.
//
// Description:
//    Cette méthode publique reqParametres() permet d'obtenir une référence
//    non constante aux paramètres du vno.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
inline SNParametresVNO<TTDonnee>& SRChamp<TTDonnee, TTCoord>::reqParametres()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return parametres;
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: Retoure une référence constante aux paramètres du vno.
//
// Description:
//    Cette méthode publique reqParametres() permet d'obtenir une référence
//    constante aux paramètres du vno.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
inline const SNParametresVNO<TTDonnee>& SRChamp<TTDonnee, TTCoord>::reqParametres() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return parametres;
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: 
//    Retourne la région associée au champ
//
// Description: 
//    La méthode publique <code>reqRegion()</code> retourne la région 
//    associée au champ.
//
// Entrée:
//
// Sortie:
//    TTRegion : la région assocée au champ
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
const typename SRChamp<TTDonnee, TTCoord>::TCRegion*
SRChamp<TTDonnee, TTCoord>::reqRegion() const 
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_regionP;
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de champ 1D
//
// Description:
//    La méthode publique <code>reqDebut()</code> retourne un itérateur de
//    champ 1D.
//  
// Entrée:
//    TCCoord pas : le pas de l'itérateur du champ
//    
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
typename SRChamp<TTDonnee, TTCoord>::TCIterateurChamp
SRChamp<TTDonnee, TTCoord>::reqDebut(const TCCoord& pasA) const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_regionP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCIterateurChamp iter(pasA, *this);
   return iter;
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de champ 2D
//
// Description:
//    La méthode publique <code>reqDebut()</code> retourne un itérateur de
//    champ 2D.
//  
// Entrée:
//    TCCoord pasA : le premier pas de l'itérateur du champ
//    TCCoord pasB : le deuxième pas de l'itérateur du champ
//    
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
typename SRChamp<TTDonnee, TTCoord>::TCIterateurChamp
SRChamp<TTDonnee, TTCoord>::reqDebut(const TCCoord& pasA,
                                     const TCCoord& pasB) const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_regionP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCIterateurChamp iter(pasA, pasB, *this);
   return iter;
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de champ 3D
//
// Description:
//    La méthode publique <code>reqDebut()</code> retourne un itérateur de
//    champ 3D.
//  
// Entrée:
//    TCCoord pasA : le premier pas de l'itérateur du champ
//    TCCoord pasB : le deuxième pas de l'itérateur du champ
//    TCCoord pasC : le troisième pas de l'itérateur du champ
//    
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
typename SRChamp<TTDonnee, TTCoord>::TCIterateurChamp
SRChamp<TTDonnee, TTCoord>::reqDebut(const TCCoord& pasA,
                                     const TCCoord& pasB,
                                     const TCCoord& pasC) const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_regionP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCIterateurChamp iter(pasA, pasB, pasC, *this);
   return iter;
}

//************************************************************************
// Sommaire:  
//    Retourne un itérateur de champ marquant la fin
//
// Description:
//    La méthode publique <code>reqFin()</code> retourne un itérateur de
//    champ marquant la fin.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
typename SRChamp<TTDonnee, TTCoord>::TCIterateurChamp
SRChamp<TTDonnee, TTCoord>::reqFin() const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_regionP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return TCIterateurChamp();
}

//************************************************************************
// Sommaire:  
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
typename SRChamp<TTDonnee, TTCoord>::TCPlageIteration
SRChamp<TTDonnee, TTCoord>::reqPlageIteration(const TCCoord& pasA) const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_regionP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return TCPlageIteration(reqDebut(pasA), reqFin());
}

//************************************************************************
// Sommaire:  
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
typename SRChamp<TTDonnee, TTCoord>::TCPlageIteration
SRChamp<TTDonnee, TTCoord>::reqPlageIteration(const TCCoord& pasA,
                                              const TCCoord& pasB) const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_regionP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return TCPlageIteration(reqDebut(pasA, pasB), reqFin());
}

#endif  // SRCHAMP_HPP_DEJA_INCLU


//************************************************************************
// Sommaire: Retourne la dimension du vecteur.
//
// Description: Retourne la dimension du vecteur en appelant la métode size()
//              du vector de STL.
//
// Entrée:
//
// Sortie:
//    EntierN     :  Dimension su vecteur
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTCoord>
inline EntierN SRChamp<TTDonnee, TTCoord>::size() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return vect.size();
}

//************************************************************************
// Sommaire:
//    +cris les données du VNO dans un fichier de format intermédiaire
//
// Description:
//    Cette méthode publique virtuelle écris les données du VNO dans un
//    fichier. Ce fichier est de format intermédiaire. Il est utiliser
//    pour l'exportation du VNO.
//
// Entrée:
//    FIFichier& os: le fichier dans lequel on écrit.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTDonnee, typename TTCoord>
void SRChamp<TTDonnee, TTCoord>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      const EntierN dim = reqParametres().reqNbrValeurs();
      os << dim << FINL;
      for (EntierN i = 0; i < dim; ++i)
      {
         // --- Méchante PATCH à CAUSE DE OS2!!!
         const TCDonnee& elem (vect[i]);
         os << elem << FINL;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_IMPORT_EXPORT


//************************************************************************
// Sommaire:
//    Lis les données du fichier de format intermédiaire
//
// Description:
//   Cette méthode publique lis les données du fichier de format intermédiaire
//   lors de l'importation de données. en fait C'est elle qui connait le format
//   intermédiaire et sais comment reconstruire un objet à partir de ce format
//   de données. Elle reconstruit le vecteur qui à déjà été dimensionné lors de
//   sa construction.
//
// Entrée:
//   FIFichier& is: Le fichier de format intermédiaire
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
namespace SRValeurNodaleHelper
{
template <typename TTDonnee>
void importe (TTDonnee& e, FIFichier& is) { e.importe(is); }

template <>
inline void importe<double> (double& e, FIFichier& is) { is >> e; }
}
#endif //MODE_IMPORT_EXPORT

#ifdef MODE_IMPORT_EXPORT
template <typename TTDonnee, typename TTCoord>
void SRChamp<TTDonnee, TTCoord>::importe(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (is)
   {
      CLChaine finligne;
      EntierN dim;
      is >> dim >> finligne;

      if (dim != size())
      {
         ERMsg msg = ERMsg(ERMsg::ERREUR, "ERR_DIMENSION");

         CLChaine ch;
         ch << dim;
         msg.ajoute(ch.reqConstCarP());

         ch = "";
         ch << size();
         msg.ajoute(ch.reqConstCarP());

         is.asgErreur(msg);
      }

      if (is)
      {
         for (EntierN i = 0; i < dim; ++i)
         {
            SRValeurNodaleHelper::importe(vect[i], is);
            if (!is)
            {
               break;
            }
         }
      }
      if (is)
      {
         // --- On évalue les minimums et les maximums.
         reqParametres().asgMinimum(*std::min_element(vect.begin(), vect.end()));
         reqParametres().asgMaximum(*std::max_element(vect.begin(), vect.end()));
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire: Sauvegarde l'élément
//
// Description:
//    L'opérateur <code>operator<<(...)</code> est l'opérateur d'insertion
//    qui appelle la méthode <code>ecrisVirtuel(...)</code> qui sauvegarde
//    les informations de l'élément.
//
// Entrée:
//   FIFichier& os                  : Fichier dans lequel on sauvegarde
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
FIFichier& operator<<(FIFichier& os, const SRChamp<TTDonnee, TTCoord>& champ)
{
   champ.ecrisVirtuel(os);
   return os;
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: Lis l'élément
//
// Description:
//    L'opérateur <code>operator>>(...)</code> est l'opérateur d'extraction
//    qui appelle la méthode <code>lisVirtuel(...)</code> qui lit les
//    informations de l'élément.
//
// Entrée:
//   FIFichier& is                  : Fichier duquel on lit
//
// Sortie:
//   FIFichier&                     : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
FIFichier& operator>>(FIFichier& is, SRChamp<TTDonnee, TTCoord>& champ)
{
   champ.lisVirtuel(is);
   return is;
}
#endif   // MODE_PERSISTANT

//**************************************************************
// Sommaire:    Ecrit les attribut de la classe dans un fichier
//
// Description: La méthode publique ecrisVirtuel(FIFichier) ecris les
//              attributs de la classe  dans un fichier
//              passé en paramètre
//
// Entrée:      FIFichier& os: Le fichier dans lequel on ecrit
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
void SRChamp<TTDonnee, TTCoord>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   PRECONDITION(os);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   os << parametres <<  ESPACE;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT

//**************************************************************
// Sommaire:    Lit les attribut de la classe du fichier
//
// Description: La méthode publique lisVirtuel(FIFichier) lit les
//              attributs de la classe à partir d'un fichier
//              passé en paramètre
//
// Entrée:      FIFichier& is: Le fichier duquel on lit
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTCoord>
void SRChamp<TTDonnee, TTCoord> ::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   PRECONDITION(is);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   is >> parametres ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT


