//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Classe:  SRRegion template spécialisé pour les GOCoordonneesX.
//
// Description:
//    Spécialisation du template SRRegion pour les GOCoordonneesX. 
//    Décrit une région spatiale 1D.
//
// Attributs:
//************************************************************************
#ifndef SRREGION_GOCOORDONNEESX_H_DEJA_INCLU
#define SRREGION_GOCOORDONNEESX_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRRegion.hf"

#include "GOCoord1.h"
#include "SREnveloppe.h"

template <>
class SRRegion<GOCoordonneesX>
{
public:
   typedef GOCoordonneesX        TCCoord;
   typedef SRRegion<TCCoord>     TCSelf;
   typedef SREnveloppe<TCCoord>  TCEnveloppe;

                        SRRegion       (); 
                        SRRegion       (const TCSelf&);
                       ~SRRegion       ();
   TCSelf&              operator=      (const TCSelf&); 

   ERMsg                asgLimites     (const TCCoord&, const TCCoord&); 
   Booleen              estValide      () const;
   const TCEnveloppe&   reqEnveloppe   () const;
   Booleen              pointInterieur (const TCCoord&) const;

   Booleen              operator==     (const TCSelf&) const;
   Booleen              operator!=     (const TCSelf&) const;
          
protected:
   void                 invariant      (ConstCarP) const;

private:
   TCEnveloppe m_enveloppe;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//************************************************************************
#ifdef MODE_DEBUG
inline void SRRegion<GOCoordonneesX>::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void SRRegion<GOCoordonneesX>::invariant(ConstCarP) const
{
}      
#endif

#endif  // SRREGION_GOCOORDONNEESX_H_DEJA_INCLU
