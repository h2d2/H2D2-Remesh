//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Classe:  SRChamp
//
// Description:
//    La classe SRChamp est la classe abstraite qui regroupe les classes
//    de tous les types de champ. Un champ représente une donnée spatiale 
//    stationnaire.  Les données d'un champ doivent pouvoir être additionnées
//    et multipliées par des DReel. Elles doivent répondre aux propriétés
//    de la structure algébrique de type algèbre. Le champ peut servir de données
//    alors le champ doit aussi respecter les propriétés de la S.A. algèbre.
//   <p>
//    Tous les types de champ possèdent des points en commun.  
//    On peut regrouper les méthodes des champs en catégories : 
//   <p>
//    - des méthodes de gestion (ex. reqTypeChamp);
//    - des méthodes d'opérations sur les champs (ex. opDerive, opIntegre);
//    - des méthodes supportant sa structure spatiale (ex. pointInterieur);
//    - des méthodes retrouvant ses valeurs (ex. reqValeur);
//    - des méthodes du champ en tant que structure algébrique (ex. +, -, *, /).
//    <p>
//    La région du champ n'est pas nécéssairement spatiale.
//    Par exemple, dans une série, le champ possède N dimensions et représente
//    un espace de solution avec des coordonnées non nécéssairement réelles.
//
// Attributs:
//    SRRegion<TTCoord>* regionP : région (généralement spatiale) correspondant
//    au champ.
//
// Notes:
//    A revoire: SRChamp définit 3 méthodes reqDebut, suivant la dimension.
//    dont 2 sont chaques fois inutiles mais demandées par le compilateur.
//************************************************************************
#ifndef SRCHAMP_H_DEJA_INCLU
#define SRCHAMP_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "SRRegion.h"

#include "SRIterateurChamp.h"
#include "SRPlageIteration.h"

#include <vector>

#ifdef MODE_PERSISTANT
#  include "cqinfprj.h"
#  include "snparvno.h"
#  include "SRChamp.hf"
#endif   // MODE_PERSISTANT

template <typename TTDonnee, typename TTCoord>
class SRChamp
{
public:
   //temp
   typedef int       AlgorithmeChamp;
   typedef const int ConstAlgorithmeChamp;
   

   typedef SRChamp<TTDonnee, TTCoord>          TCSelf;
   typedef TTDonnee                            TCDonnee;
   typedef TTCoord                             TCCoord;

   typedef SRRegion<TTCoord>                   TCRegion;
   typedef TCRegion const*                     ConstTCRegionP;
   typedef SRIterateurChamp<TCDonnee, TCCoord> TCIterateurChamp;
   typedef SRPlageIteration<TCIterateurChamp>  TCPlageIteration;


                       SRChamp             ();
                       SRChamp             (ConstTCRegionP);
                       SRChamp             (const TCSelf&);
   virtual             ~SRChamp            ();
           TCSelf&     operator=           (const TCSelf&);

/*
   virtual TCDonnee&   opDeriveX           (const TTCoord&) const = 0;
   virtual TCDonnee&   opDeriveY           (const TTCoord&) const = 0;
   virtual TCDonnee&   opDeriveZ           (const TTCoord&) const = 0;

   virtual void        opDiffDivergence    (TCDonnee&, const TTCoord&) = 0; 
   virtual void        opDiffGradient      (CLVecteur&, CLVecteur&, 
                                            CLVecteur&, CLVecteur&) = 0; 
   virtual void        opDiffRotationnel   (CLVecteur&, CLVecteur&,
                                            CLVecteur&, CLVecteur&) = 0; 
   virtual ERMsg       opIntegre           (TCDonnee&) const = 0;


   virtual TCSelf      operator+           (const TCSelf&) const = 0;
   virtual TCSelf&     operator+=          (const TCSelf&) = 0;
   virtual TCSelf      operator-           (const TCSelf&) const = 0;
   virtual TCSelf&     operator-=          (const TCSelf&) = 0;
   virtual TCSelf      operator*           (const DReel&) const = 0;
   virtual TCSelf&     operator*=          (const DReel&) = 0;
   virtual TCSelf      operator/           (const DReel&) const = 0;
   virtual TCSelf&     operator/=          (const DReel&) = 0;
*/

   virtual ERMsg              executeAlgorithme (AlgorithmeChamp&) = 0;
   virtual ERMsg              executeAlgorithme (ConstAlgorithmeChamp&) const = 0;
   virtual ERMsg              reqValeur         (TCDonnee&, const TCCoord&) const = 0;
  
           Booleen            pointInterieur    (const TCCoord&) const;
           TCIterateurChamp   reqDebut          (const TCCoord&) const;
           TCIterateurChamp   reqDebut          (const TCCoord&, const TCCoord&) const;
           TCIterateurChamp   reqDebut          (const TCCoord&,
                                                 const TCCoord&,
                                                 const TCCoord&) const;
           TCIterateurChamp   reqFin            () const;
           ConstTCRegionP     reqRegion         () const;

           TCPlageIteration   reqPlageIteration (const TCCoord&) const;
           TCPlageIteration   reqPlageIteration (const TCCoord&, const TCCoord&) const;
           EntierN            size() const;

protected:
   virtual void               invariant         (ConstCarP) const;
   ConstTCRegionP             m_regionP;
   std::vector<TTDonnee>      vect;

#ifdef MODE_IMPORT_EXPORT
public:
   virtual void                       exporte         (FIFichier&) const;
   virtual void                       importe         (FIFichier&);
#endif //MODE_IMPORT_EXPORT

#ifdef MODE_PERSISTANT
public:
   typedef SNParametresVNO<TTDonnee>  TCParametres;

   void                               asgInfoDonnee   (const CQInfoDonneeProjet&);
   CQInfoDonneeProjet                 reqInfoDonnee   () const;
   TCParametres&                      reqParametres   ();
   const TCParametres&                reqParametres   () const;

   friend FIFichier& operator<<  <>(FIFichier&, const SRChamp<TTDonnee, TTCoord>&);
   friend FIFichier& operator>>  <>(FIFichier&, SRChamp<TTDonnee, TTCoord>&);

protected:
   virtual void       ecrisVirtuel    (FIFichier&) const;
   virtual void       lisVirtuel      (FIFichier&);

  CQInfoDonneeProjet info;
  TCParametres       parametres;
#endif   // MODE_PERSISTANT
};

namespace SRValeurNodaleHelper
{
template <typename TTDonnee>
void importe (TTDonnee&, FIFichier&);
template <>
void importe<double> (double&, FIFichier&);
}


//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
template <typename TCDonnee, typename TTCoord>
inline void SRChamp<TCDonnee, TTCoord>::invariant(ConstCarP /*conditionP*/) const
{
   //TODO: région ne devrait pas être nulle... une fois les régions mises
   //en place pour les champs analytiques.
}
#else
template <typename TCDonnee, typename TTCoord>
inline void SRChamp<TCDonnee, TTCoord>::invariant(ConstCarP) const
{
}
#endif

#include "SRChamp.hpp"

#endif  // SRCHAMP_H_DEJA_INCLU
