//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Classe:  SRPlageIteration
//
// Description:
//    La classe SRPlageIteration représente une plage d'itération donnée
//    par deux itérateurs, un de début et un de fin.
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef SRPLAGEITERATION_H_DEJA_INCLU
#define SRPLAGEITERATION_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

template <typename TTIter>
class SRPlageIteration
{
public:
   SRPlageIteration  () : m_debut(), m_fin()  {}
   SRPlageIteration  (TTIter d, TTIter f) : m_debut(d), m_fin(f) {}
   SRPlageIteration  (const SRPlageIteration& p) : m_debut(p.m_debut), m_fin(p.m_fin) {}
   ~SRPlageIteration () {}
   SRPlageIteration& operator= (const SRPlageIteration& p) { m_debut = p.m_debut; m_fin = p.m_fin; return *this; }

   TTIter reqDebut () { return m_debut; }
   TTIter reqFin   () { return m_fin;   }

protected:
   TTIter m_debut;
   TTIter m_fin;
};

#endif  // SRPLAGEITERATION_H_DEJA_INCLU
