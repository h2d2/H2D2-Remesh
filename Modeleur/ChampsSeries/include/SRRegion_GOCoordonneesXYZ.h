//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: SRRegion_GOCoordonneesXYZ.h
//
// Classe:  SRRegion template spécialisé pour les GOCoordonneesXYZ
//
// Description:
//    Spécialisation du template SRRegion pour les GOCoordonneesXYZ. 
//    Décrit une région spatiale 2D, compatible avec GEOS (OpenGIS).
//    La classe est définie dans GORegion.cpp, ce qui la rend utilisable
//    indépendemment du module ChampSeries. 
//
// Attributs:
//    GOMultiPolygone  multiPoly : multipolygone GEOS identifiant la région
//************************************************************************
#ifndef SRREGION_GOCOORDONNEESXYZ_H_DEJA_INCLU
#define SRREGION_GOCOORDONNEESXYZ_H_DEJA_INCLU

/*
#ifndef MODULE_CHAMPSSERIES
#  define MODULE_CHAMPSSERIES 1
#endif
*/

#include "sytypes.h"
#include "erexcept.h"

#include "GORegion.h"
#include "GOCoord3.h"

#include "SRRegion.hf"

template <>
class SRRegion<GOCoordonneesXYZ> : public GORegion
{
public:
      SRRegion() : GORegion() {}
      SRRegion(const SRRegion<GOCoordonneesXYZ>& o) : GORegion(o) {}
};

#endif  // SRREGION_GOCOORDONNEESXYZ_H_DEJA_INCLU
