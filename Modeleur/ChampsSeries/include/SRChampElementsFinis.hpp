//************************************************************************
// --- Copyright (c) 2003-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
#ifndef SRCHAMPELEMENTSFINIS_HPP_DEJA_INCLU
#define SRCHAMPELEMENTSFINIS_HPP_DEJA_INCLU

#include "MGInterpole.h"
#include "ALIntegreChamp.h"

#include <algorithm>
#include <functional>
#include <iterator>

//************************************************************************
// Sommaire:  
//    Constructeur sans paramètre d'un SRChampElementsFinis vide
//
// Description: 
//    Le constructeur publique <code>SRChampElementsFinis()</code> construit
//    un champ éléments finis vide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
SRChampElementsFinis<TTDonnee, TTMaillage>::SRChampElementsFinis()
   : TCParent()
   , maillageP(NUL)
   , epsilon(0.0)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Constructeur d'un SRChampElementsFinis vide avec un maillage et une région en
//    paramètre
//
// Description: 
//    Le constructeur publique <code>SRChampElementsFinis(...)</code> construit
//    un champ éléments finis vide associé à un maillage et à une région.
//
// Entrée:
//    ConstTCMaillageP mailP : le maillage associé au champ
//    ConstTCRegionP   regP  : la région associée au champ
//
// Sortie:
//
// Notes:
//    A cause de l'héritage virtuel de la hiérarchie SRChamp, les classes
//    publiées en Python avec un wrapper appellent directement le
//    constructeur par défaut de SRChamp. La variable m_regionP n'est donc
//    jamais initialisée.
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
SRChampElementsFinis<TTDonnee, TTMaillage>::SRChampElementsFinis(ConstTCMaillageP mailP,
                                                                 typename TCSelf::ConstTCRegionP regP)
   : TCParent(regP)
   , maillageP(mailP)
   , valeursNodales(mailP->reqNbrNoeud(), TTDonnee())
   , epsilon(0.0)
{
#ifdef MODE_DEBUG
   PRECONDITION(regP != NUL);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   TCSelf::m_regionP = regP;      // cf note;
}

//************************************************************************
// Sommaire:  
//    Constructeur copie d'un SRChampElementsFinis
//
// Description:
//    Le constructeur copie <code>SRChampElementsFinis(...)</code>d'un 
//    SRChampElementsFinis.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//    A cause de l'héritage virtuel de la hiérarchie SRChamp, les classes
//    publiées en Python avec un wrapper appellent directement le
//    constructeur par défaut de SRChamp. La variable m_regionP n'est donc
//    jamais initialisée.
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
SRChampElementsFinis<TTDonnee, TTMaillage>::SRChampElementsFinis(const TCSelf& obj)
   : TCParent (obj.m_regionP)
   , maillageP(obj.maillageP)
   , valeursNodales(obj.valeursNodales)   
   , epsilon(obj.epsilon)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   TCSelf::m_regionP = obj.m_regionP;      // cf note;
}

//************************************************************************
// Sommaire:  
//    Destructeur d'un SRChampElementsFinis
//
// Description: 
//    Le destructeur public <code>~SRChampElementsFinis()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
SRChampElementsFinis<TTDonnee, TTMaillage>::~SRChampElementsFinis()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Opérateur d'assignation
//
// Description: 
//    L'opérateur public <code>operator=(...)</code> d'assignation 
//    d'un SRChampElementsFinis.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator=(const TCSelf& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (this != &obj)
   {
      TCParent::operator=(obj);
      maillageP = obj.maillageP;
      valeursNodales = obj.valeursNodales;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  
//    Assigne la tolérance pour l'interpolation
//
// Description: 
//    La méthode publique <code>asgEpsilon(...)</code> assigne la valeur 
//    de tolérance à utiliser lors de l'interpolation sur le maillage.
//
// Entrée:
//    cosnt DReel&   e        : La tolérance
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
ERMsg 
SRChampElementsFinis<TTDonnee, TTMaillage>::asgEpsilon(const DReel& e)
{
#ifdef MODE_DEBUG
   PRECONDITION(e >= 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   epsilon = e;

#ifdef MODE_DEBUG   
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  
//    Assigne les valeurs nodales au champ
//
// Description: 
//    La méthode publique <code>asgValeurs(...)</code> assigne les valeurs 
//    nodales au champ.
//
// Entrée:
//    TTIter 
//    TTIter 
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTContainer, typename TTMaillage, typename TTIter>
ERMsg 
SRChampElementsFinisDetail::asgValeurs(TTContainer& valeursNodales,
                                       TTMaillage*  maillageP,
                                       TTIter debut, TTIter fin)
{
#ifdef MODE_DEBUG
//   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   typedef typename std::iterator_traits<TTIter>::difference_type TCDistance;
   TCDistance dimVno = std::distance(debut, fin);
//   Entier dimVno = std::distance(debut, fin);
   if (dimVno == 0 || 
       (dimVno > 0 && maillageP->reqNbrNoeud() == static_cast<EntierN>(dimVno)))
   {
      valeursNodales.clear();
      valeursNodales.reserve(dimVno);
      valeursNodales.insert(valeursNodales.end(), debut, fin);
   }
   else
   {
      msg = ERMsg(ERMsg::ERREUR, "ERR_NBR_NOEUD_MAILLAGE_VALEURS_CHAMP_INCOHÉRENT");
   }

#ifdef MODE_DEBUG   
//   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  
//    Informe si le vecteur de valeur nodale contient des valeurs ou non
//
// Description: 
//    La méthode publique <code>estCharge(...)</code> retourne VRAI
//    si le vecteur de valeurs nodales contient des valeurs et FAUX sinon.
//
// Entrée:
//
// Sortie:  Un booleen à vrai si le vecteur de valeur nodale n'est pas vide.
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
inline Booleen 
SRChampElementsFinis<TTDonnee, TTMaillage>::estCharge() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return (valeursNodales.size() > 0);
}

//************************************************************************
// Sommaire: 
//    Execute un algorithme sur le champ
//
// Description: 
//    La méthode publique <code>executeAlgorithme(...)</code> execute 
//    un algorithme sur le champ.
//
// Entrée:
//    AlgorithmeChamp& algo : l'algorithme à exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
ERMsg 
SRChampElementsFinis<TTDonnee, TTMaillage>::executeAlgorithme(typename TCSelf::AlgorithmeChamp&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}

//************************************************************************
// Sommaire: 
//    Execute un algorithme sur le champ
//
// Description: 
//    La méthode publique <code>executeAlgorithme(...)</code> execute 
//    un algorithme sur le champ.
//
// Entrée:
//    AlgorithmeChamp& algo : l'algorithme à exécuter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
ERMsg 
SRChampElementsFinis<TTDonnee, TTMaillage>::executeAlgorithme(typename TCSelf::ConstAlgorithmeChamp&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg(ERMsg::OK);
}

#ifdef BLOC_EN_COMMENTAIRE

//************************************************************************
// Sommaire: 
//    Opérateur trouvant la dérivée en X d'un point du champ
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
/*template <typename TTDonnee, typename TTMaillage>
TTDonnee& SRChampElementsFinis<TTDonnee, TTMaillage>::opDeriveX(const TCCoord& b) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
*/
//************************************************************************
// Sommaire: 
//    Opérateur trouvant la dérivée en Y d'un point du champ
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
/*template <typename TTDonnee, typename TTMaillage>
TTDonnee& SRChampElementsFinis<TTDonnee, TTMaillage>::opDeriveY(const TCCoord& b) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
*/
//************************************************************************
// Sommaire: 
//    Opérateur trouvant la dérivée en Z d'un point du champ
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
/*template <typename TTDonnee, typename TTMaillage>
TTDonnee& SRChampElementsFinis<TTDonnee, TTMaillage>::opDeriveZ(const TCCoord& b) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
*/
#endif   // BLOC_EN_COMMENTAIRE

//************************************************************************
// Sommaire: 
//    Opérateur d'intégration sur le champ
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
ERMsg SRChampElementsFinis<TTDonnee, TTMaillage>::opIntegre(typename TCSelf::TCDonnee& res) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   ALIntegreChamp<TCSelf> algo;
   msg = algo.integre(res, *this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Opérateur de négation
//
// Description: 
//    L'opérateur public <code>operator-</code> de négation
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator-() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);    
   retour *= -1.0;
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur d'addition de deux champs
//
// Description: 
//    L'opérateur public <code>operator+(...)</code> additionne deux champs.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator+(const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   PRECONDITION(valeursNodales.size() == obj.valeursNodales.size());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);
   retour += obj;

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur d'addition de deux champs
//
// Description: 
//    L'opérateur public <code>operator+=(...)</code> additionne deux champs.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator+=(const TCSelf& obj) 
{
#ifdef MODE_DEBUG
   PRECONDITION(valeursNodales.size() == obj.valeursNodales.size());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   std::transform(    valeursNodales.begin(),
                      valeursNodales.end  (),
                  obj.valeursNodales.begin(),
                      valeursNodales.begin(),
                  std::plus<TTDonnee>());

   return *this;
}

//************************************************************************
// Sommaire: 
//    Opérateur de soustraction de deux champs
//
// Description: 
//    L'opérateur public <code>operator-</code> soustrait deux champs
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator-(const TCSelf& obj) const
{
#ifdef MODE_DEBUG
   PRECONDITION(valeursNodales.size() == obj.valeursNodales.size());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);    
   retour -= obj;
 
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur de soustraction de deux champs
//
// Description: 
//    L'opérateur public <code>operator-=(...)</code> soustrait deux champs
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator-=(const TCSelf& obj) 
{
#ifdef MODE_DEBUG
   PRECONDITION(valeursNodales.size() == obj.valeursNodales.size());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   std::transform(    valeursNodales.begin(),
                      valeursNodales.end  (),
                  obj.valeursNodales.begin(),
                      valeursNodales.begin(),
                  std::minus<TTDonnee>());

   return *this;
}

//************************************************************************
// Sommaire: 
//    Opérateur de multiplication d'un champ avec un DReel
//
// Description: 
//    L'opérateur public <code>operator*</code> multiplie un champ avec
//    un DReel.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator*(const DReel& mul) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);    
   retour *= mul;
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur de multiplication d'un champ avec un DReel
//
// Description: 
//    L'opérateur public <code>operator*=(...)</code> multiplie un champ avec
//    un DReel.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator*=(const DReel& mul) 
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

         TCVnoIterateur vI    = valeursNodales.begin();
   const TCVnoIterateur vFinI = valeursNodales.end();
   for ( ; vI != vFinI; ++vI)
      (*vI) *= mul;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire: 
//    Opérateur de division d'un champ avec un DReel
//
// Description: 
//    L'opérateur public <code>operator/</code> divise un champ avec
//    un DReel.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator/(const DReel& div) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);    
   retour /= div;
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur de division d'un champ avec un DReel
//
// Description: 
//    L'opérateur public <code>operator/=(...)</code> divise un champ avec
//    un DReel.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator/=(const DReel& div) 
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   (*this) *= (1.0 / div);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire: 
//    Opérateur d'addition de deux champs
//
// Description: 
//    L'opérateur public <code>operator+(...)</code> additionne deux champs.
//
// Entrée:
//    const TCParent& obj: un objet du type générique
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator+(const TCParent& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);
   retour += obj;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur d'addition de deux champs
//
// Description: 
//    L'opérateur public <code>operator+=(...)</code> additionne deux champs.
//
// Entrée:
//    const TCParent& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator+=(const TCParent& obj) 
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::iterateurNoeudConst TCIterNoeud;

         TCIterNoeud nI    = maillageP->reqNoeudDebut();
   const TCIterNoeud nFinI = maillageP->reqNoeudFin();
   for ( ; nI != nFinI; ++nI)
   {
      typename TCSelf::TCDonnee v;
      ERMsg msg = obj.reqValeur(v, (*nI)->reqCoordonnees());
      (*this)[(*nI)->reqIndVno()] += v;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire: 
//    Opérateur d'addition de deux champs
//
// Description: 
//    L'opérateur public <code>operator-</code> soustrait deux champs
//
// Entrée:
//    const TCParent& obj: un objet du type générique
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator-(const TCParent& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);
   retour -= obj;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur d'addition de deux champs
//
// Description: 
//    L'opérateur public <code>operator-=</code> soustrait deux champs
//
// Entrée:
//    const TCParent& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator-=(const TCParent& obj) 
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::iterateurNoeudConst TCIterNoeud;

         TCIterNoeud nI    = maillageP->reqNoeudDebut();
   const TCIterNoeud nFinI = maillageP->reqNoeudFin();
   for ( ; nI != nFinI; ++nI)
   {
      typename TCSelf::TCDonnee v;
      ERMsg msg = obj.reqValeur(v, (*nI)->reqCoordonnees());
      (*this)[(*nI)->reqIndVno()] -= v;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire: 
//    Opérateur de multiplication d'un champ avec un champ scalaire
//
// Description: 
//    L'opérateur public <code>operator*</code> multiplie un champ avec
//    un champ scalaire.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator*(const SRChamp<DReel, TCCoord>& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);    
   retour *= obj;
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur de multiplication d'un champ avec un DReel
//
// Description: 
//    L'opérateur public <code>operator*=(...)</code> multiplie un champ avec
//    un DReel.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator*=(const SRChamp<DReel, TCCoord>& obj) 
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::iterateurNoeudConst TCIterNoeud;

         TCIterNoeud nI    = maillageP->reqNoeudDebut();
   const TCIterNoeud nFinI = maillageP->reqNoeudFin();
   for ( ; nI != nFinI; ++nI)
   {
      DReel v;
      ERMsg msg = obj.reqValeur(v, (*nI)->reqCoordonnees());
      (*this)[(*nI)->reqIndVno()] *= v;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire: 
//    Opérateur de division d'un champ avec un DReel
//
// Description: 
//    L'opérateur public <code>operator/</code> divise un champ avec
//    un DReel.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf
SRChampElementsFinis<TTDonnee, TTMaillage>::operator/(const SRChamp<DReel, TCCoord>& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG
 
   TCSelf retour(*this);    
   retour /= obj;
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return retour;
}

//************************************************************************
// Sommaire: 
//    Opérateur de division d'un champ avec un DReel
//
// Description: 
//    L'opérateur public <code>operator/=(...)</code> divise un champ avec
//    un DReel.
//
// Entrée:
//    const SRChampElementsFinis& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCSelf&
SRChampElementsFinis<TTDonnee, TTMaillage>::operator/=(const SRChamp<DReel, TCCoord>& obj) 
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef typename TCMaillage::iterateurNoeudConst TCIterNoeud;

         TCIterNoeud nI    = maillageP->reqNoeudDebut();
   const TCIterNoeud nFinI = maillageP->reqNoeudFin();
   for ( ; nI != nFinI; ++nI)
   {
      DReel v;
      ERMsg msg = obj.reqValeur(v, (*nI)->reqCoordonnees());
      (*this)[(*nI)->reqIndVno()] /= v;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire: 
//    Retourne la dimension du vecteur des valeurs nodales
//
// Description:
//    La méthode publique <code>reqNbrValeursNodales()</code> retourne la
//    dimension du vecteur en appelant la métode size() du vector de STL.
//
// Entrée:
//
// Sortie:
//    Entier     :  Dimension du vecteur
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
inline EntierN
SRChampElementsFinis<TTDonnee, TTMaillage>::reqNbrValeursNodales() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return static_cast<EntierN>( valeursNodales.size() );
}

//************************************************************************
// Sommaire: 
//    Retourne la valeur du champ à un point
//
// Description: 
//    La méthode publique <code>reqValeur()</code> retourne la valeur du
//    champ en un point.
//  
// Entrée:
//    TCCoord coord : le point dont on veut la valeur
//
// Sortie:
//
// Notes:
// 
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
ERMsg 
SRChampElementsFinis<TTDonnee, TTMaillage>::reqValeur(
               typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCDonnee& val,
         const typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCCoord& coord) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   MGInterpole<TCSelf> inter(*maillageP, *this);
   msg = inter.interpole(val, coord, epsilon);
      
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Retourne la valeur nodale à l'indice demandé
//
// Description:
//    La opérateur public <code>operator[](...)</code> retourne la valeur 
//    nodale à l'indice demandé.
//
// Entrée:
//    EntierN ind : l'indice de la valeur nodale
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
inline typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCDonnee& 
SRChampElementsFinis<TTDonnee, TTMaillage>::operator[] (const EntierN& ind)
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < valeursNodales.size());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return valeursNodales[ind];
}

//************************************************************************
// Sommaire: 
//    Retourne la valeur nodale à l'indice demandé
//
// Description:
//    La opérateur public <code>operator[](...)</code> retourne la valeur 
//    nodale à l'indice demandé.
//
// Entrée:
//    EntierN ind : l'indice de la valeur nodale
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
inline const typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCDonnee& 
SRChampElementsFinis<TTDonnee, TTMaillage>::operator[] (const EntierN& ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < valeursNodales.size());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return valeursNodales[ind];
}

//************************************************************************
// Sommaire: Retourne un pointeur vers le maillage associer au champ.
//
// Description:
//    Retourne un pointeur vers le maillage associé à ce champ.
//
// Entrée:
//
// Sortie:
//    Retourne le pointeur vers le maillage.
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
inline typename SRChampElementsFinis<TTDonnee, TTMaillage>::ConstTCMaillageP
SRChampElementsFinis<TTDonnee, TTMaillage>::reqMaillage() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return maillageP;
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de valeurs nodales du champ
//
// Description:
//    La méthode publique <code>reqVnoDebut()</code> retourne un itérateur 
//    de valeurs nodales du champ. L'itérateur est positionné au début du 
//    vecteur.
//  
// Entrée:
//    
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCVnoIterateur 
SRChampElementsFinis<TTDonnee, TTMaillage>::reqVnoDebut()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return valeursNodales.begin();
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de valeurs nodales du champ
//
// Description:
//    La méthode publique <code>reqVnoFin()</code> retourne un itérateur 
//    de valeurs nodales du champ indiquant la fin. 
//  
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCVnoIterateur 
SRChampElementsFinis<TTDonnee, TTMaillage>::reqVnoFin()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return valeursNodales.end();
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de valeurs nodales du champ
//
// Description:
//    La méthode publique <code>reqVnoDebut()</code> retourne un itérateur 
//    de valeurs nodales du champ. L'itérateur est positionné au début du 
//    vecteur.
//  
// Entrée:
//    
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCConstVnoIterateur 
SRChampElementsFinis<TTDonnee, TTMaillage>::reqVnoDebut()const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return valeursNodales.begin();
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de valeurs nodales du champ
//
// Description:
//    La méthode publique <code>reqVnoFin()</code> retourne un itérateur 
//    de valeurs nodales du champ indiquant la fin. 
//  
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCConstVnoIterateur 
SRChampElementsFinis<TTDonnee, TTMaillage>::reqVnoFin()const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return valeursNodales.end();
}

//************************************************************************
// Sommaire: 
//    Retourne la valeur minimum d'un champ
//
// Description:
//    La méthode publique <code>reqVnoMin()</code> retourne la valeur min
//    d'un champ de vno. 
//  
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCDonnee
SRChampElementsFinis<TTDonnee, TTMaillage>::reqVnoMin()const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
      
   typename SRChampElementsFinis::TCDonnee minCourant = SRChampElementsFinis::TCDonnee();
   TCConstVnoIterateur it      = valeursNodales.begin();
   TCConstVnoIterateur itFin   = valeursNodales.end();

   if (it != itFin)
   {
      minCourant = *it;
   }

   for (; it != itFin; it++)
   {
      if (*it < minCourant)
      {
         minCourant = *it;
      }
   }
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return minCourant;
}

//************************************************************************
// Sommaire: 
//    Retourne la valeur maximum d'un champ
//
// Description:
//    La méthode publique <code>reqVnoMax()</code> retourne la valeur max
//    d'un champ de vno. 
//  
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTDonnee, typename TTMaillage>
typename SRChampElementsFinis<TTDonnee, TTMaillage>::TCDonnee
SRChampElementsFinis<TTDonnee, TTMaillage>::reqVnoMax()const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
      
   typename SRChampElementsFinis::TCDonnee maxCourant = SRChampElementsFinis::TCDonnee();
   TCConstVnoIterateur it      = valeursNodales.begin();
   TCConstVnoIterateur itFin   = valeursNodales.end();

   if (it != itFin)
   {
      maxCourant = *it;
   }

   for (; it != itFin; it++)
   {
      if (*it > maxCourant)
      {
         maxCourant = *it;
      }
   }
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return maxCourant;
}

#endif  // SRCHAMPELEMENTSFINIS_HPP_DEJA_INCLU


//************************************************************************
// Sommaire: Sauvegarde la dimension et tous tes éléments.
//
// Description: Opérateur d'extraction qui sauvegarde la dimension du VNO
//              et ses valeurs.
//
// Entrée:
//   FIFichier& os                        : Fichier dans lequel on sauvegarde
//   const SRChamp& vno                   : Objet SNValeurNodale à sauvegarder
//
// Sortie:
//   FIFichier&                           : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTMaillage>
void 
SRChampElementsFinis<TTDonnee, TTMaillage>::ecrisVirtuel(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (os)
   {
      TCParent::ecrisVirtuel(os);
   }

   if (os)
   {       
      for (TCConstVnoIterateur vI = reqVnoDebut();
           vI != reqVnoFin();
           ++vI)
      {
         os << (*vI) << FINL;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire: Lit l'objet à partir d'un fichier
//
// Description: Opérateur d'extraction qui restaure l'objet à partir d'un fichier.
//
// Entrée:
//  FIFichier& is                  : Fichier dans lequel on lit.
//  SRChamp& vno                   : Objet que l'on lit
//
// Sortie:
//  FIFichier&                     : Référence au fichier
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
template <typename TTDonnee, typename TTMaillage>
void 
SRChampElementsFinis<TTDonnee, TTMaillage>::lisVirtuel(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
  
   EntierN dim;
   if (is)
   {
      TCParent::lisVirtuel(is);
      dim = parametres.reqNbrValeurs();
   }
   if ( dim != reqNbrValeursNodales())
   {
      ERMsg msg = ERMsg(ERMsg::ERREUR);
      CLChaine ch;
      msg.ajoute("ERR_DIMENSION");
      ch << dim;
      msg.ajoute(ch.reqConstCarP());
      ch << reqNbrValeursNodales();
      msg.ajoute(ch.reqConstCarP());
      is.asgErreur(msg);
   }
   if (is)
   {
      for (TCVnoIterateur vI = reqVnoDebut();
           vI != reqVnoFin();
           ++vI)
      {
         is >> (*vI);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire:
//    Ecris les données du VNO dans un fichier de format intermédiaire
//
// Description:
//    Cette méthode publique virtuelle écris les données du VNO dans un
//    fichier. Ce fichier est de format intermédiaire. Il est utiliser
//    pour l'exportation du VNO.
//
// Entrée:
//    FIFichier& os: le fichier dans lequel on écrit.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT

template <typename TTDonnee>
void SRChampElementsFinisDetail::init(TTDonnee& v)
{
}

#include "SRGardeValeurIndirecte.h"
#include "EFMaillage3D.h"
typedef SRChampElementsFinis<DReel, EFMaillage3D>   SRChampEFScalaire;
typedef  SRGardeValeurIndirecte<SRChampEFScalaire>        TCTEST;
template <>     
inline void SRChampElementsFinisDetail::init<TCTEST>(TCTEST& v)
{
   SRChampEFScalaire* tP = new SRChampEFScalaire;
   v = TCTEST(tP);
}

template <typename TTDonnee>
void SRChampElementsFinisDetail::exporte(TTDonnee& v, FIFichier& os)
{
   v.exporte(os);
}

template <>
inline void SRChampElementsFinisDetail::exporte<DReel>(DReel& v, FIFichier& os)
{
   os << v << ESPACE;
}

template <>
inline void SRChampElementsFinisDetail::exporte<Entier>(Entier& v, FIFichier& os)
{
   os << v << ESPACE;
}

template <>
inline void SRChampElementsFinisDetail::exporte<const DReel>(const DReel& v, FIFichier& os)
{
   os << v << ESPACE;
}

template <>
inline void SRChampElementsFinisDetail::exporte<const Entier>(const Entier& v, FIFichier& os)
{
   os << v << ESPACE;
}


template <typename TTDonnee>
void SRChampElementsFinisDetail::importe(TTDonnee& v, FIFichier& is)
{
   v.importe(is);
}

template <>
inline void SRChampElementsFinisDetail::importe<DReel>(DReel& v, FIFichier& is)
{
   is >> v;
}

template <>
inline void SRChampElementsFinisDetail::importe<Entier>(Entier& v, FIFichier& is)
{
   is >> v ;
}
#endif   // MODE_IMPORT_EXPORT


//************************************************************************
// Sommaire:
//    Ecris les données du VNO dans un fichier de format intermédiaire
//
// Description:
//    Cette méthode publique virtuelle écris les données du VNO dans un
//    fichier. Ce fichier est de format intermédiaire. Il est utiliser
//    pour l'exportation du VNO.
//
// Entrée:
//    FIFichier& os: le fichier dans lequel on écrit.
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTDonnee, typename TTMaillage>
void SRChampElementsFinis<TTDonnee, TTMaillage>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCParent::exporte(os);

   os << reqNbrValeursNodales() << ESPACE;  

   for (TCConstVnoIterateur vI = reqVnoDebut();
        vI != reqVnoFin() && os;
        ++vI)
   {
       SRChampElementsFinisDetail::exporte(*vI, os);
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_IMPORT_EXPORT

#ifdef MODE_IMPORT_EXPORT
template <typename TTDonnee, typename TTMaillage>
void SRChampElementsFinis<TTDonnee, TTMaillage>::importe(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCParent::importe(is);

   EntierN nbreVal;
   is >> nbreVal ;

   std::vector<TTDonnee> valeurs;
   //Recupération des valeurs du champs
   for (EntierN compteur = 0 ; compteur < nbreVal ; ++compteur)
   {
      TTDonnee ttd;
      SRChampElementsFinisDetail::init(ttd);
      SRChampElementsFinisDetail::importe(ttd, is);
      valeurs.push_back(ttd);
   }
   this->asgValeurs(valeurs.begin(), valeurs.end());
   
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif   // MODE_IMPORT_EXPORT
