//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GOEllipseErreur.cpp
// Classe:   GOEllipseErreur
//*****************************************************************************
// 05-12-96  Yves Secretan    Version originale
//*****************************************************************************
#ifndef GOELLIPSEERREUR_HPP_DEJA_INCLU
#define GOELLIPSEERREUR_HPP_DEJA_INCLU

#include <algorithm>
#include <math.h>

//************************************************************************
// Sommaire:  Intersecte avec une autre ellipse.
//
// Description:
//    La méthode publique <code>intersecte()</code> calcule l'intersection
//    avec une autre ellipse et assigne le résultat à l'objet.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
void GOEllipseErreur::intersecte(const GOEllipseErreur& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   *this = intersection(*this, autre);
}

//************************************************************************
// Sommaire:  Limite la taille des demi-axes vers le bas.
//
// Description:
//    La méthode publique <code>limiteBas()</code> limite la taille des
//    demi-axes (rayons) vers le bas.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
void GOEllipseErreur::limiteBas(const DReel& h)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   petitAxe = std::max(petitAxe, h);
   grandAxe = std::max(grandAxe, h);
}

//************************************************************************
// Sommaire:  Limite la taille des demi-axes vers le haut.
//
// Description:
//    La méthode publique <code>limiteHaut()</code> limite la taille des
//    demi-axes (rayons) vers le haut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
void GOEllipseErreur::limiteHaut(const DReel& h)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   petitAxe = std::min(petitAxe, h);
   grandAxe = std::min(grandAxe, h);
}

//************************************************************************
// Sommaire:  Limite la tailles des demi-axes.
//
// Description:
//    La méthode publique <code>limiteHaut()</code> limite la taille des
//    demi-axes (rayons).
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
void GOEllipseErreur::limite(const DReel& hmin, const DReel& hmax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   limiteBas (hmin);
   limiteHaut(hmax);
}

//************************************************************************
// Sommaire:  Normalise l'ellipse.
//
// Description:
//    La méthode publique <code>normalise()</code> normalise l’ellipse,
//    i.e. la rend de norme 1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
void GOEllipseErreur::normalise()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel nrm = 1.0 / reqNorme();
   grandAxe *= nrm;
   petitAxe *= nrm;
}

//************************************************************************
// Sommaire:  Retourne le facteur d'anisotropie.
//
// Description:
//    La méthode publique <code>reqAnisotropie()</code> retourne le facteur 
//    d'anisotropie de l'ellipse, à savoir le rapport entre le grand axe
//    et le petit axe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GOEllipseErreur::reqAnisotropie() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (grandAxe / petitAxe);
}

//************************************************************************
// Sommaire:  Retourne la norme de l'ellipse en métrique euclidienne.
//
// Description:
//    La méthode publique <code>reqNorme()</code> retourne la norme de
//    l'ellipse, comme la racine carrée du produits des axes qui est
//    l'équivalent du diamètre pour un cercle.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GOEllipseErreur::reqNorme() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return sqrt(grandAxe*petitAxe);
}

//************************************************************************
// Sommaire:  Retourne le grand axe de l'ellipse.
//
// Description:
//    La méthode publique <code>reqGrandAxe()</code> retourne le grand axe de
//    l'ellipse.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GOEllipseErreur::reqGrandAxe() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return grandAxe;
}

//************************************************************************
// Sommaire:  Retourne le petit axe de l'ellipse.
//
// Description:
//    La méthode publique <code>reqPetitAxe()</code> retourne le petit axe de
//    l'ellipse.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GOEllipseErreur::reqPetitAxe() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return petitAxe;
}

//************************************************************************
// Sommaire:  Retourne l'inclinaison de l'ellipse.
//
// Description:
//    La méthode publique <code>reqInclinaison()</code> retourne l'inclinaison
//    de l'ellipse. L'inclinaison est mesurée en radian positive dans le
//    sens trigonométrique.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GOEllipseErreur::reqInclinaison() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return angle;
}

//************************************************************************
// Sommaire:  Retourne la valeur propre min.
//
// Description:
//    La méthode publique <code>reqEigenValMin()</code> retourne la valeur
//    propre min.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GOEllipseErreur::reqEigenValMin() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return 1.0 / (grandAxe * grandAxe);
}

//************************************************************************
// Sommaire:  Retourne la valeur propre max.
//
// Description:
//    La méthode publique <code>reqEigenValMax()</code> retourne la valeur
//    propre max.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
DReel GOEllipseErreur::reqEigenValMax() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return 1.0 / (petitAxe*petitAxe);
}

//************************************************************************
// Sommaire:  Mise à l'échelle des axes.
//
// Description:
//    La méthode publique <code>scale()</code> fait une mise à l'échelle
//    des axes.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
void GOEllipseErreur::scale(const DReel& f)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   grandAxe *= f;
   petitAxe *= f;
}

//************************************************************************
// Sommaire:  Surcharge des opérateurs d’addition et de soustraction
//
// Description:
//    Les opérateurs publiques
//    <code>operator +()</code>, <code>operator +=()</code>,
//    <code>operator -()</code>, <code>operator -=()</code> redéfinissent
//    les opérateur d'addition et de soustraction pour des objets de type
//    <code>GOEllipseErreur</code>.
//
// Entrée:
//    const GOEllipseErreur& cri    L'objet à additionner ou soustraire
//
// Sortie:
//
// Notes:
//    L'interpolation des ellipses est faite sur chaque composante, grand
//    axe, petite axe et angle. Il faut donc sommer chaque composante et
//    diviser/multiplier par un réel.
//************************************************************************
inline 
GOEllipseErreur GOEllipseErreur::operator + (const GOEllipseErreur& cri) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GOEllipseErreur tmp(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return tmp += cri;
}

inline 
const GOEllipseErreur& GOEllipseErreur::operator += (const GOEllipseErreur& other)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   DReel a11, a12, a22;
   DReel b11, b12, b22;
   this->reqComposantes(a11, a12, a22);
   other.reqComposantes(b11, b12, b22);

   this->asgComposantes(a11+b11, a12+b12, a22+b22);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}

inline 
GOEllipseErreur GOEllipseErreur::operator - (const GOEllipseErreur& cri) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GOEllipseErreur tmp(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return tmp -= cri;
}

inline 
const GOEllipseErreur& GOEllipseErreur::operator -= (const GOEllipseErreur& other)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   DReel a11, a12, a22;
   DReel b11, b12, b22;
   this->reqComposantes(a11, a12, a22);
   other.reqComposantes(b11, b12, b22);

   this->asgComposantes(a11-b11, a12-b12, a22-b22);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  Surcharge des opérateurs de multiplication et de division par un réel
//
// Description:
//    Les opérateurs publiques
//    <code>operator *()</code>, <code>operator *=()</code>,
//    <code>operator /()</code>, <code>operator /=()</code> redéfinissent
//    les opérateur de  multiplication et de division par un réel pour
//    des objets de type <code>GOEllipseErreur</code>.
//
// Entrée:
//    const Reel& val      La valeur par laquelle multiplier ou diviser
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline 
GOEllipseErreur GOEllipseErreur::operator / (const DReel& val) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GOEllipseErreur tmp(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return tmp /= val;
}

inline 
const GOEllipseErreur& GOEllipseErreur::operator /= (const DReel& val)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   DReel a11, a12, a22;
   this->reqComposantes(a11, a12, a22);
   this->asgComposantes(a11/val, a12/val, a22/val);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}

inline 
GOEllipseErreur GOEllipseErreur::operator * (const DReel& val) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   GOEllipseErreur tmp(*this);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return tmp *= val;
}

inline 
const GOEllipseErreur& GOEllipseErreur::operator *= (const DReel& val)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   DReel a11, a12, a22;
   this->reqComposantes(a11, a12, a22);
   this->asgComposantes(a11*val, a12*val, a22*val);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}

inline
GOEllipseErreur operator * (const DReel& val, const GOEllipseErreur& e)
{
   return (e*val);
}

//************************************************************************
// Sommaire:  Surcharge des opérateurs de comparaison
//
// Description:
//    Les opérateurs publiques
//    <code>operator <()</code>, <code>operator <=()</code>,
//    <code>operator >()</code>, <code>operator >=()</code> redéfinissent
//    pour des objets de type <code>GOEllipseErreur</code>.
//    Ce sont des opérateurs d'ordonnancement.
//
// Entrée:
//    const GOEllipseErreur& cri    L'objet à comparer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline
bool GOEllipseErreur::operator < (const GOEllipseErreur& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (grandAxe < autre.grandAxe ||
            (grandAxe == autre.grandAxe && (petitAxe < autre.petitAxe ||
               (petitAxe == autre.petitAxe && angle < autre.angle))));
}

inline
bool GOEllipseErreur::operator <= (const GOEllipseErreur& autre) const
{
   return (!(autre < *this));
}

inline
bool GOEllipseErreur::operator > (const GOEllipseErreur& autre) const
{
   return (autre < *this);
}

inline
bool GOEllipseErreur::operator >= (const GOEllipseErreur& autre) const
{
   return (!(*this < autre));
}

//************************************************************************
// Sommaire:  Surcharge des opérateurs de comparaison d'inégalité
//
// Description:
//    Les opérateurs publiques
//    <code>operator ==()</code>, <code>operator !=()</code>,
//    pour des objets de type <code>GOEllipseErreur</code>.
//
// Entrée:
//    const GOEllipseErreur& cri    L'objet à comparer
//
// Sortie:
//
// Notes:
//************************************************************************
inline
bool GOEllipseErreur::operator != (const GOEllipseErreur& autre) const
{
   return (!(*this == autre));
}

#endif // ifndef GOELLIPSEERREUR_HPP_DEJA_INCLU
