//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOMultiPolyligne.h
//
// Classe:  GOMultiPolyligne
//
// Description: La classe définit une ensemble de polylignes.
//   Elle fait appel aux méthodes de la bibliothèque GEOS, classe 
//    MultiLineString dans certaines de ses méthodes.  
//   
// Attributs:
//   TCContainer     m_multiPolyligne;   Vecteur de GOPolylignes
//   GOGenerateurIDP m_genIdP;         Générateur d'ID (assure un ID
//      distinct pour chaque polyligne, utilse pour calculs topologiques).
//   Booleen         m_proprioGenId;   ...
//
// Notes: 
//    SCDOC vérifier la définition ci-dessus.
//************************************************************************
#ifndef GOMULTIPOLYLIGNE_H_DEJA_INCLU
#define GOMULTIPOLYLIGNE_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.h"

#include "GOGenerateurID.hf"
#include "GOPolyligne.hf"
#include "GOMultiPolyligne.hf"

#include <boost/iterator/indirect_iterator.hpp>
#include "GOPolyligne.h"
BOOST_TT_BROKEN_COMPILER_SPEC(GOPolyligne)

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOMultiPolyligne
{
public:  
   typedef GOPoint          TCSommet;
   typedef GOPoint::TCCoord TCCoord;

   typedef  std::vector<GOPolyligne*>                  TCContainer;
   typedef  TCContainer::const_iterator                TCContainerIter;
   typedef  boost::indirect_iterator<TCContainerIter>  TCConstIterateur;
               
                           GOMultiPolyligne   ();
                           GOMultiPolyligne   (const GOMultiPolyligne&);
                           ~GOMultiPolyligne  ();
   GOMultiPolyligne&       operator=          (const GOMultiPolyligne&);

   void                    asgGenerateurID   (GOGenerateurIDP);
         GOGenerateurIDP   reqGenerateurID();
         
   // ---  Parcours
         TCConstIterateur  reqDebut          () const;
         TCConstIterateur  reqFin            () const;
         EntierN           reqNbrPolylignes  () const;
         EntierN           reqNbrSommets     () const;
   const GOPolyligne&      operator[]        (EntierN) const;

   // ---  Edite
         ERMsg             ajoutePolyligne  (const GOPolyligne&);
         template <typename TTIterateur>
         ERMsg             ajoutePolyligne  (TTIterateur, TTIterateur);
         ERMsg             ajoutePTdePL     (EntierN, EntierN, const TCSommet&);
         ERMsg             deplacePL        (const TCCoord&, EntierN);
         ERMsg             deplacePTdePL    (const TCCoord&, EntierN, EntierN);
         ERMsg             retirePL         (EntierN);
         ERMsg             retirePTdePL     (EntierN, EntierN);

   // ---  Algo
         Booleen           pointInterieur   (const TCCoord&) const;
         Booleen           estUnSommet      (EntierN&, const GOPoint&, const DReel& = GOPoint::tolerance) const;
//         ERMsg           reqProprietaire(TCContainer&, const TCCoord&);
   
protected:
         void              invariant        (ConstCarP) const;

private:
   TCContainer     m_multiPolyligne;
   GOGenerateurIDP m_genIdP;
   Booleen         m_proprioGenId;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOMultiPolyligne::invariant(ConstCarP conditionP) const
{
   INVARIANT(m_genIdP != NUL, conditionP);
}      
#else
inline void GOMultiPolyligne::invariant(ConstCarP) const
{
}      
#endif

#include "GOMultiPolyligne.hpp"

#endif  // GOMULTIPOLYLIGNE_H_DEJA_INCLU
