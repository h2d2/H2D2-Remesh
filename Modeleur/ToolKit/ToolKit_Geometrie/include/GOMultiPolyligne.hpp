//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: GOMultiPolyligne.hpp
// Classe : GOMultiPolyligne
//************************************************************************
// 13-02-2004  Maude Giasson          Version initiale
//************************************************************************
#ifndef GOMULTIPOLYLIGNE_HPP_DEJA_INCLU
#define GOMULTIPOLYLIGNE_HPP_DEJA_INCLU


//************************************************************************
// Sommaire:  Ajoute une polyligne au multiPolyligne
//
// Description:
//    La méthode publique <code>ajoutePolylgine(...)</code> ajoute la
//    polyligne passée en paramètre au vecteur de polyligne formant
//    la GOMultiPolyligne et redonne un pointeur sur la polyligne 
//    ajoutée.
//    La polyligne à ajouter est passée en paramètre par 
//    l'intermédiaire d'itérateurs sur ses sommets.
//    
// Entrée:
//    TTIterateur debutI         Polyligne à ajouter sous forme d'itérateurs
//    TTIterateur finI           (début fin) sur ses sommets
//
// Sortie: 
//
// Notes:  modifier au besoin pour qu'il y aie retour d'un ERMsg
//************************************************************************
template <typename TTIterateur>
ERMsg GOMultiPolyligne::ajoutePolyligne(TTIterateur debutI, TTIterateur finI)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   GOPolyligneP polyligneP = new GOPolyligne();
   polyligneP->assigne(debutI, finI);
   m_multiPolyligne.push_back(polyligneP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif   // GOMULTIPOLYLIGNE_HPP_DEJA_INCLU


