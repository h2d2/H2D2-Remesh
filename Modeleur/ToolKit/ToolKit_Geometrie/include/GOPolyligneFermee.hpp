//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPolyligneFermee.hpp
// Classe : GOPolyligneFermee
//************************************************************************
// 16-02-2004  Maude Giasson          Version initiale
//************************************************************************
#ifndef GOPOLYLIGNEFERMEE_HPP_DEJA_INCLU
#define GOPOLYLIGNEFERMEE_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:   Ajoute un sommet à la fin de la polyligne.
//
// Description:
//    La méthode <code>ajouteSommet(...)</code> ajoute le sommet passé
//    en paramètre à la fin de la polyligne.
//
// Entrée: 
//    const TCCoord& sommet      Coordonnées du sommet à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline ERMsg GOPolyligneFermee::ajouteSommet(const TCCoord& posi)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return ajouteSommet(TCSommet(posi));   
}

//************************************************************************
// Sommaire:  Ajoute un sommet à la polyligne à la position souhaitée.
//    
//
// Description:
//    La méthode <code>ajouteSommet(...)</code> ajoute le sommet passé
//    en paramètre à la polyligne à la position souhaitée.
//    Par défaut, ce sommet  se retrouve comme étant le dernier 
//    parmis l'ensemble ordonné de sommets formant la polyligne. 
//
// Entrée: 
//    const TCCoord& sommet   Coordonnées du sommet à ajouter
//    EntierN indice          Indice où placer le sommet. indice représente
//                            l'indice que portera le sommet après son ajout.
//                            Il doit être entre 0 et le nombre de sommet de
//                            la polyligne (avant ajout du sommet).
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline ERMsg GOPolyligneFermee::ajouteSommet(const TCCoord& posi, EntierN ind)
{
#ifdef MODE_DEBUG
   PRECONDITION(ind <= reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   return ajouteSommet(TCSommet(posi), ind);
}

//************************************************************************
// Sommaire:  Assigne les sommets à une polyligne vide.
//
// Description:
//    La méthode <code>assigne(...)</code> assigne la liste de sommets
//    passés en paramètre (sous forme d'une paire d'itérateur de début
//    et fin) à la polyligne. La polyligne est préalablement vidée.
//    Au besoin, on ferme la polyligne assignée.
//
// Entrée:
//    TTIterateur debutI            Itérateur de début et de fin
//    TTIterateur finI              sur les sommets à assigner.
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename TTIterateur>
ERMsg GOPolyligneFermee::assigne(TTIterateur debutI, TTIterateur finI)
{
#ifdef MODE_DEBUG
   PRECONDITION(std::distance(debutI, finI) > 2);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   msg = GOPolyligne::assigne(debutI, finI);
   if (m_sommets.front().reqPosition() != m_sommets.back().reqPosition())
      m_sommets.push_back(m_sommets.front());
   else
      m_sommets.back() = m_sommets.front();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Translate la polyligne.
//
// Description:
//    La méthode <code>deplace(...)</code> translate la polyligne selon le
//    vecteur de déplacement passé en paramètre.
//  
// Entrée:
//    const TCCoord& vctDeplacement       Vecteur de déplacement
//
// Sortie:
//
// Notes:
//************************************************************************
inline 
ERMsg GOPolyligneFermee::deplace (const TCCoord& vctDeplacement)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return GOPolyligne::deplace(vctDeplacement);
}

#endif   // GOPOLYLIGNEFERMEE_HPP_DEJA_INCLU
