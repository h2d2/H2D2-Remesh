//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPolyligne.h
//
// Classe:  GOPolyligne
//
// Description: La classe GOPolyligne définit un objet géométrique de type 
//   polyligne dans un système cartésien tri-dimensionnel.
//   Elle fait appel aux méthodes de la bibliothèque GEOS, classe
//    LineString au besoin (voir par ex. méthodes ...).  
//   
// Attributs:
//    TCContainerSommets m_sommets;       Liste des sommets (GOPoint)
//    GOGenerateurID&    m_genID;         Generateur d'ID pour les sommets
//
// Notes: 
//    SCDOC vérifier la définition ci-dessus.
//************************************************************************
#ifndef GOPOLYLIGNE_H_DEJA_INCLU
#define GOPOLYLIGNE_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.h"
#include "GOGenerateurID.h"   // .h à cause de l'invariant

#include "GOGeometrie.hf"
#include "GOPolyligne.hf"

#include <list>

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOPolyligne
{
public:         
   typedef GOPoint           TCSommet;
   typedef GOPoint::TCCoord  TCCoord;

   typedef std::list<TCSommet>                TCContainerSommets;
   typedef TCContainerSommets::const_iterator TCConstIterateur;

                     GOPolyligne    ();
                     GOPolyligne    (const GOPolyligne&);
                    ~GOPolyligne    ();
   GOPolyligne&      operator =     (const GOPolyligne&);

   void                    asgGenerateurID(GOGenerateurIDP);
         GOGenerateurIDP   reqGenerateurID();
           
   // ---  Parcours
   TCConstIterateur  reqDebut       () const;
   TCConstIterateur  reqFin         () const;
   EntierN           reqNbrSommets  () const;
   const TCSommet&   operator[]     (EntierN)  const;

   template <typename TTIterateur>
   ERMsg             assigne        (TTIterateur, TTIterateur);

   // ---  Edite
   ERMsg             ajouteSommet   (const TCCoord&);
   ERMsg             ajouteSommet   (const TCCoord&, EntierN);
   ERMsg             ajouteSommet   (const TCSommet&);
   ERMsg             ajouteSommet   (const TCSommet&, EntierN);
   ERMsg             ajoutePointsIntersection(const GOPolyligne&);
   ERMsg             deplace        (const TCCoord&);
   ERMsg             deplace        (const TCCoord&, EntierN);
   ERMsg             modifie        (const TCCoord&, EntierN);
   ERMsg             supprime       (EntierN);
              
   // ---  Algo
   ERMsg             contient       (Booleen&, const GOPolyligne&) const;
   ERMsg             intersection   (GOGeometrie&, const GOPolyligne&) const;
   Booleen           pointInterieur (const TCCoord&) const;
   Booleen           estUnSommet    (EntierN&, const GOPoint&, const DReel& = GOPoint::tolerance) const;
   ERMsg             retourne       ();

protected:
   void              invariant      (ConstCarP) const;

protected:
   TCContainerSommets m_sommets;
   GOGenerateurIDP    m_genIdP;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//    L'invariant sur la taille de la polyligne entre en conflit avec la
//    construction via ajouteSommet()
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOPolyligne::invariant(ConstCarP conditionP) const
{
//   INVARIANT(m_sommets.size() == 0 || m_sommets.size() > 1, conditionP);
   INVARIANT(m_genIdP != NUL, conditionP);
         TCContainerSommets::const_iterator pI    = m_sommets.begin();
   const TCContainerSommets::const_iterator pFinI = m_sommets.end();
   while (pI != pFinI)
   {
      INVARIANT((*pI++).reqID() != GOGenerateurID::NON_INITIALISE, conditionP);
   }
}      
#else
inline void GOPolyligne::invariant(ConstCarP) const
{
}      
#endif

#include "GOPolyligne.hpp"

#endif  // GOPOLYLIGNE_H_DEJA_INCLU
