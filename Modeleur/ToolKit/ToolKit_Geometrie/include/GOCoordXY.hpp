//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOCoordXY.hpp
// Classe : GOCoordXY
//************************************************************************
// 01-03-1995  Yves Secretan      Version initiale
// 11-11-1997  Yves Roy           Utilisation de GOEpsilon pour la comparaison de bas
//                                niveau entre des doubles...
// 22-04-1998  Yves Roy           Ajout d'une fonction d'assignation au minGlobal et au maxGlobal
// 03-05-1999  Yves Secretan      Supporte les iostream du standard et les nouveaux
//                                entêtes du standard
// 27-05-2003  Dominique Richard  Port multi-compilateur
// 21-11-2003  Olivier Kaczor     Ajout de la méthode reqNbrDim
// 11-02-2004  Maude Giasson      Modifications templates (class -> typename)
// 16-02-2004  Maude Giasson      TT->TC à l'intérieur de la classe
// 18-02-2004  Yves Secretan      Port GCC
//************************************************************************
#ifndef GOCOOXY_HPP_DEJA_INCLU
#define GOCOOXY_HPP_DEJA_INCLU

#include "GOEpsilon.h"
#include "GOCoordXYZ.h"

#include <cmath>
#include <iostream>
#include <limits>

template <typename TTCoord>
const GOCoordXY<TTCoord> GOCoordXY<TTCoord>::GRAND = GOCoordXY(std::numeric_limits<TTCoord>::max(),
                                                               std::numeric_limits<TTCoord>::max());
template <typename TTCoord>
const GOCoordXY<TTCoord> GOCoordXY<TTCoord>::PETIT = GOCoordXY(std::numeric_limits<TTCoord>::min(),
                                                               std::numeric_limits<TTCoord>::min());

//************************************************************************
// Sommaire: Constructeur par défaut.
//
// Description:
//    Le constructeur publique <pre>GOCoordXY()</pre> est le constructeur par
//    défaut de la classe. Il initialise les coordonnées à 0.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXY<TTCoord>::GOCoordXY ()
   : leX(0), leY(0)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Constructeur qui prend deux paramètres.
//
// Description:
//    Le constructeur publique <pre>GOCoordXY(const TTCoord&, const TTCoord&)</pre>
//    construit un objet à partir de deux coordonnées.
//
// Entrée:
//    const TCCoord& unX
//    const TCCoord& unY
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXY<TTCoord>::GOCoordXY (const TCCoord& unX, const TCCoord& unY)
   : leX(unX), leY(unY)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur publique <pre>GOCoordXY(const GOCoordXY&)</pre> est le
//    constructeur copie de la classe. Il construit l'objet comme copie de celui
//    qui est pasé en argument.
//
// Entrée:
//    const GOCoordXY& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXY<TTCoord>::GOCoordXY (const TCSelf& p)
   : leX(p.leX), leY(p.leY)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur publique <code>GOCoordXY(const GOCoordXYZ&)</code> est un
//    opérateur cast à partir d'un <code>GOCoordXYZ</code>. La conposante en z
//    est ignorée.
//
// Entrée:
//    const GOCoordXYZ& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXY<TTCoord>::GOCoordXY (const GOCoordXYZ<TCCoord>& p)
   : leX(p.x()), leY(p.y())
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Destructeur de la classe
//
// Description:
//    Le destructeur publique <pre>~GOCoordXY()</pre> ne fait rien de
//    spécifique.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXY<TTCoord>::~GOCoordXY ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

}  // GOCoordXY::~GOCoordXY<TTCoord>

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXY<TTCoord>::operator GOCoordXYZ<TTCoord>() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return GOCoordXYZ<TCCoord>(leX, leY, 0);
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Nombre de dimension de la coordonnée
//
// Description:
//    La méthode publique <code>reqNbrDim()</code> retourne la dimension 
//    de la coordonnée
//
// Entrée:
//
// Sortie:
//   EntierN : la dimension de la coordonnée
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline EntierN
GOCoordXY<TTCoord>::reqNbrDim() 
{
   return 2;
}

//************************************************************************
// Sommaire: Composante x de la coordonnée
//
// Description:
//    La méthode publique <pre>x()</pre> retourne la coordonnée x (première
//    composante) du point. La coordonnées est retournée par référence, ce qui
//    permet à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCCoord&
GOCoordXY<TTCoord>::x ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leX;
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Composante x de la coordonnée
//
// Description:
//    La méthode publique <pre>x()</pre> retourne la coordonnée x (première
//    composante) du point. La coordonnées est retournée par référence constante,
//    ce qui ne permet pas à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordXY<TTCoord>::TCCoord&
GOCoordXY<TTCoord>::x () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leX;
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Composante y de la coordonnée
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCCoord&
GOCoordXY<TTCoord>::y ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leY;
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Composante y de la coordonnée
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordXY<TTCoord>::TCCoord&
GOCoordXY<TTCoord>::y () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leY;
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Composante z de la coordonnée (invalide)
//
// Description:
//    La méthode public <code>z()</code> est implantée pour permettre des
//    algorithme génériques. Comme cette composante n'existe pas, on retourne
//    une valeur invalide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCCoord
GOCoordXY<TTCoord>::z () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return std::numeric_limits<TTCoord>::infinity();
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire:  Définition de l'opérateur [] pour des GOCoordXY
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCCoord&
GOCoordXY<TTCoord>::operator[] (Entier i)
{
#ifdef MODE_DEBUG
   PRECONDITION(i == 0 || i == 1);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (i == 0) ? leX : leY;
}  // GOCoordXY::operator[]

//************************************************************************
// Sommaire:  Définition de l'opérateur [] pour des GOCoordXY
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordXY<TTCoord>::TCCoord&
GOCoordXY<TTCoord>::operator[] (Entier i) const
{
#ifdef MODE_DEBUG
   PRECONDITION(i == 0 || i == 1);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (i == 0) ? leX : leY;
}  // GOCoordXY::operator[]

//************************************************************************
// Sommaire:  Définition de l'opérateur + pour des GOCoordXY
//
// Description:
//    L'opérateur publique <pre>operator+</pre> définit l'additon entre deux
//    <pre>GOCoordXY</pre>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf
GOCoordXY<TTCoord>::operator+ (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res += p;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordXY::operator+

//************************************************************************
// Sommaire:
//    Définition de l'opérateur += pour des GOCoordXY
//
// Description:
//    L'opérateur publique <pre>operator+</pre> définit l'additon entre deux
//    <pre>GOCoordXY</pre>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf&
GOCoordXY<TTCoord>::operator+= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX += p.leX;
   leY += p.leY;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordXY::operator+=

//************************************************************************
// Description:
//    Définition de l'opérateur + pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf
GOCoordXY<TTCoord>::operator- (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res -= p;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordXY::operator-

//************************************************************************
// Description:
//    Définition de l'opérateur - pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf
GOCoordXY<TTCoord>::operator- () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res;
   res.leX = -leX;
   res.leY = -leY;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordXY::operator-

//************************************************************************
// Description:
//    Définition de l'opérateur -= pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf&
GOCoordXY<TTCoord>::operator-= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX -= p.leX;
   leY -= p.leY;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordXY::operator-=

//************************************************************************
// Description:
//    Définition de l'opérateur * pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf
GOCoordXY<TTCoord>::operator* (const TCCoord& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res *= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordXY::operator*

//************************************************************************
// Description:
//    Définition de l'opérateur *= pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf&
GOCoordXY<TTCoord>::operator*= (const TCCoord& val)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX *= val;
   leY *= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordXY::operator*=

//************************************************************************
// Description:
//    Définition de l'opérateur / pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf
GOCoordXY<TTCoord>::operator/ (const TCCoord& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res /= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordXY::operator/

//************************************************************************
// Description:
//    Définition de l'opérateur /= pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf&
GOCoordXY<TTCoord>::operator/= (const TCCoord& val)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX /= val;
   leY /= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordXY::operator/=

//************************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCSelf&
GOCoordXY<TTCoord>::operator= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (this != &p)
   {
      leX = p.leX;
      leY = p.leY;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return *this;
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Description:
//    Définition de l'opérateur == pour des GOCoordXY.
//    On utilise l'objet GOEpsilon pour faire la comparaison entre deux
//    double...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordXY<TTCoord>::operator== (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return (GOEpsilon(leX) == p.leX && GOEpsilon(leY) == p.leY);
}  // GOCoordXY::operator==

//************************************************************************
// Description:
//    Définition de l'opérateur != pour des GOCoordXY
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordXY<TTCoord>::operator!= (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return !(*this == p);
}  // GOCoordXY::operator!=

//************************************************************************
// Sommaire:   Retourne VRAI si le point est à l'intérieur
//
// Description:
//    La méthode publique <code>estDedans()</code> retourne VRAI si 
//    le point est compris dans le volume délimité par les deux  
//    points min et max passés en argument.
//
// Entrée:
//    const TCSelf& pMin     // Point Min
//    const TCSelf& pMax     // Point Max
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen
GOCoordXY<TTCoord>::estDedans (const TCSelf& pMin,
                                const TCSelf& pMax) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   // Puisqu'il n'y a pas d'opérateur >=, on emploie l'expression négative !(<)
   return (!(leX < pMin.leX) && !(leX > pMax.leX)  &&
           !(leY < pMin.leY) && !(leY > pMax.leY));
}

//************************************************************************
// Description:
//    Définition de l'opérateur < pour des GOCoordXY.  On utilise l'objet
//    GOEpsilon pour faire la comparaison entre deux doubles...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordXY<TTCoord>::operator< (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return (GOEpsilon(leX) < p.leX) ||
          (GOEpsilon(leX) == p.leX && GOEpsilon(leY) < p.leY);
}  // GOCoordXY::operator<


//************************************************************************
// Description:
//    Définition de l'opérateur > pour des GOCoordXY.  On utilise l'objet
//    GOEpsilon pour faire la comparaison entre deux doubles...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordXY<TTCoord>::operator> (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return (GOEpsilon(leX) > p.leX) ||
          (GOEpsilon(leX) == p.leX && GOEpsilon(leY) > p.leY);
}  // GOCoordXY::operator>

//************************************************************************
// Description:
//    Définition de l'opérateur * pour des GOCoordXY et Reel.
//    Cette fonction est nécessaire pour permettre une opération
//    avec arguments inversés.  En effet, p1 * val n'est pas équivalent
//    à val * p1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline GOCoordXY<TTCoord>
operator* (const TTCoord& val, const GOCoordXY<TTCoord>& p)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return (p * val);
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un minimum global
//
// Description:  La méthode publique minGlobal(...) permet de contribuer
//               à la détermination d'un minimum global à une collection
//               d'objet de ce type.  On passe un minGlobal en argument et
//               si l'une de nos composantes est plus petite, on assigne
//               celle-ci au minimum global.
//
// Entrée:
//
// Sortie:       TCSelft& minGlobal : le minimum global.
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline void
GOCoordXY<TTCoord>::minGlobal(TCSelf& minGlobal) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (leX < minGlobal.x()) minGlobal.x() = leX;
   if (leY < minGlobal.y()) minGlobal.y() = leY;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un maximum global
//
// Description:  La méthode publique maxGlobal(...) permet de contribuer
//               à la détermination d'un maximum global à une collection
//               d'objet de ce type.  On passe un maxGlobal en argument et
//               si l'une de nos composantes est plus grande, on assigne
//               celle-ci au maximum global.
//
// Entrée:
//
// Sortie:       TCSelft& maxGlobal : le maximum global.
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline void
GOCoordXY<TTCoord>::maxGlobal(TCSelf& maxGlobal) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (leX > maxGlobal.x()) maxGlobal.x() = leX;
   if (leY > maxGlobal.y()) maxGlobal.y() = leY;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La méthode publique <code>normalise()</code> normalise le vecteur (point),
//    donc le rend tel que sa norme soit 1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
void
GOCoordXY<TTCoord>::normalise()
{
#ifdef MODE_DEBUG
   PRECONDITION(norme2() != 0.0);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   double nrm = static_cast<double>(leX*leX + leY*leY);
   nrm = 1.0 / INRS_LIBC_STD::sqrt(nrm);
   leX *= nrm;
   leY *= nrm;

#ifdef MODE_DEBUG
   POSTCONDITION(INRS_LIBC_STD::fabs(norme2()-1.0) <= 1.0e-15);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La fonction <code>normalise()</code> normalise le point (vecteur) 
//    passé en argument, donc le rend tel que sa norme soit 1.
//
// Entrée:
//    const GOCoordXY<TTCoord>& p     Point à normaliser
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
GOCoordXY<TTCoord>
normalise(const GOCoordXY<TTCoord>& p)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   typename GOCoordXY<TTCoord>::TCSelf tmp = p;
   tmp.normalise();

#ifdef MODE_DEBUG
#endif   // MODE_DEBUG
   return tmp;
}

//************************************************************************
// Description:
//    Retourne la norme du vecteur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCCoord
GOCoordXY<TTCoord>::norme() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   double prod2 = static_cast<double>(leX*leX + leY*leY);
   return static_cast<TCCoord>(INRS_LIBC_STD::sqrt(prod2));
}  // TTCoord GOCoordXY<TTCoord>::norme ()

//************************************************************************
// Description:
//    Retourne le carré de la norme du vecteur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXY<TTCoord>::TCCoord
GOCoordXY<TTCoord>::norme2() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCCoord prod = leX*leX + leY*leY;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return prod;
}  // TTCoord GOCoordXY<TTCoord>::norme2 ()

//************************************************************************
// Description:
//    Retourne le produit scalaire entre les deux vecteurs. Il correspond
//    à la projection d'un vecteur sur l'autre.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
prodScal(const GOCoordXY<TTCoord>& p1, const GOCoordXY<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXY<TTCoord>::TCCoord TCCoord;
   double prod2 = static_cast<double>(p1.leX*p2.leX + p1.leY*p2.leY);

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return static_cast<TCCoord>(sqrt(prod2));
}  // TTCoord prodScal (const GOCoordXY<TTCoord> &p1, const GOCoordXYZ<TTCoord> &p2)

//************************************************************************
// Description:
//    Retourne le carré du produit scalaire entre les deux vecteurs.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
prodScal2(const GOCoordXY<TTCoord>& p1, const GOCoordXY<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXY<TTCoord>::TCCoord TCCoord;
   TCCoord prod = p1.leX*p2.leX + p1.leY*p2.leY;

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return prod;
}  // TTCoord prodScal2 (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Description:
//    Retourne le produit vectoriel entre deux vecteurs. Il correspond à
//    l'aire du parallélogramme construit sur les deux vecteurs.
//
// Entrée:
//    const TCSelf& p  // Premier vecteur
//    const TCSelf& p  // Deuxième vecteur
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
GOCoordXYZ<TTCoord>
prodVect(const GOCoordXY<TTCoord>& p1, const GOCoordXY<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXY<TTCoord>::TCCoord TCCoord;
   TCCoord prod = p1.leX*p2.leY - p1.leY*p2.leX;

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return GOCoordXYZ<TTCoord>(0, 0, prod);
}  // TCCoord prodVect (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Description:
//    Retourne la distance euclidienne entre deux points.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline TTCoord
dist(const GOCoordXY<TTCoord>& p1, const GOCoordXY<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXY<TTCoord>::TCCoord TCCoord;
   TCCoord dx = p2.leX - p1.leX;
   TCCoord dy = p2.leY - p1.leY;
   double prod2 = static_cast<double>(dx*dx + dy*dy);

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return static_cast<TCCoord>(sqrt(prod2));
}  // TTCoord prodScal (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Description:
//    Retourne le carré de la distance euclidienne entre deux points.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline TTCoord
dist2(const GOCoordXY<TTCoord>& p1, const GOCoordXY<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXY<TTCoord>::TCCoord TCCoord;
   TCCoord dx = p2.leX - p1.leX;
   TCCoord dy = p2.leY - p1.leY;

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (dx*dx + dy*dy);
}  // TTCoord dist2 (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXY</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>istream</code> pour celle-ci.
//
// Entrée:
//    istream& is                   // Stream de lecture
//    TCSelf& p         // GOCoordXY à lire
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
std::istream&
operator >> (std::istream& is, GOCoordXY<TTCoord>& p)
{
#ifdef MODE_DEBUG
   p.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXY<TTCoord>::TCCoord TCCoord;

   if (is)
   {
      TCCoord leX, leY;
      is >> leX >> leY;
      if (is)
      {
         p.leX = leX;
         p.leY = leY;
      }
   }

#ifdef MODE_DEBUG
   p.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return is;
}  // PRStreamLecture& operator >> (istream&, TCSelf&)

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator <<(...)</code> friend de la classe
//    <code>GOCoordXY</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>ostream</code> pour celle-ci.
//
// Entrée:
//    ostream& os                   // Stream d'écriture
//    const TCSelf& p               // GOCoordXY à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
std::ostream&
operator << (std::ostream& os, const GOCoordXY<TTCoord>& p)
{
#ifdef MODE_DEBUG
   p.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (os)
   {
      os << p.leX << " " << p.leY;
   }

#ifdef MODE_DEBUG
   p.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return os;
}  // ostream& operator << (ostream&, const TCSelf&)


//************************************************************************
// Description:
//    Surcharge de l'opérateur d'extraction >>.
//
// Entrée:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXY</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Sortie:
//    FIFichier& is                 // Fichier de lecture
//    GOCoordXY<TTCoord>& p        // GOCoordXY à lire
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
#ifdef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator >> <>(FIFichier& is, GOCoordXY<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (is)
      {
         TTCoord leX, leY;
         is >> leX >> leY;
         if (is)
         {
            p.leX = leX;
            p.leY = leY;
         }
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return is;
   }
#else //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator >> (FIFichier& is, GOCoordXY<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (is)
      {
         TTCoord leX, leY;
         is >> leX >> leY;
         if (is)
         {
            p.leX = leX;
            p.leY = leY;
         }
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return is;
   }
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator <<(...)</code> friend de la classe
//    <code>GOCoordXY</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& os                 // Fichier de écrire
//    GOCoordXY<TTCoord>& p        // GOCoordXY à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
#ifdef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator << <>(FIFichier& os, const GOCoordXY<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (os)
      {
         os << p.leX << " " << p.leY;
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return os;
   }
#else //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator << (FIFichier& os, const GOCoordXY<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (os)
      {
         os << p.leX << " " << p.leY;
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return os;
   }
   #endif   // MODE_PERSISTANT
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM

//************************************************************************
// Description:
//    Surcharge de l'opérateur d'extraction >>.
//
// Entrée:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXYZ</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Sortie:
//    FIFichier& is                 // Fichier de lecture
//    GOCoordXY<TTCoord>& p         // GOCoordXY à lire
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void GOCoordXY<TTCoord>::importe(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (is)
   {
      TTCoord x, y;
      is >> x >> y;
      if (is)
      {
         leX = x;
         leY = y;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return;
}
#endif   // MODE_IMPORT_EXPORT


//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXY</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& os                 // Fichier de écrire
//    GOCoordXY<TTCoord>& p         // GOCoordXY à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void GOCoordXY<TTCoord>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (os)
   {
      os << leX << " " << leY;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return os;
}
#endif   // MODE_IMPORT_EXPORT

#endif   // GOCOOXY_HPP_DEJA_INCLU

