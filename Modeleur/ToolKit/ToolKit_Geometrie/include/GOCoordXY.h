//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier:  GOCoordXY.h
//
// Classe:   GOCoordXY
//
// Sommaire: Coordonnées cartésiennes d'un point 2D.
//
// Description:
//    La classe <pre>GOCoordXY</pre> représente les coordonnées d'un point
//    ou un vecteur dans un système cartésien bi-dimensionnel. <p>
//    La classe est paramétrisée par le type qui représente une coordonnée
//    (réel, entier ...). Les exigences de la classe par rapport à ce paramètre
//    sont:
//       un constructeur-copie
//       l'opérateur d'assignation =
//       les opérateurs mathématiques +, +=, -, -=, *, *=, /, /=,
//       les opérateur logiques  == et <
//       les opérateur d'insertion-extraction << et >>
//
// Attributs:
//    TTCoord  x, y      Position du point
//
// Notes:
//
//************************************************************************
// 16-10-1997  Yves Secretan      Version initiale
// 02-03-1998  Yves Roy           Ajout de la persistence
// 22-04-1998  Yves Roy           Ajout d'une fonction d'assignation au minGlobal et au maxGlobal
// 28-10-1998  Yves Secretan      Remplace #include "ererreur"
// 03-05-1999  Yves Secretan      Supporte les iostream du standard
// 27-05-2003  Dominique Richard  Port multi-compilateur
// 21-11-2003  Olivier Kaczor     Ajout du typedef TTValeur et de la méthode reqNbrDim
// 11-02-2004  Maude Giasson      Modifications templates (TT ->TC)
// 18-02-2004  Yves Secretan      Port GCC
// 08-05-2004  Yves Secretan      Port MSVC 7.1
// 22-06-2004  Yves Secretan      Port MSVC 7.1 - Supprime des friend
//************************************************************************
#ifndef GOCOOXY_H_DEJA_INCLU
#define GOCOOXY_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include <iosfwd>

#ifdef MODE_PERSISTANT
DECLARE_CLASS(FIFichier);
#endif   // MODE_PERSISTANT

// Les fonctions friend doivent être déclarés avant, à
// l'extérieur du corps de la classe.
#include "GOCoordXY.hf"
#include "GOCoordXYZ.hf"

template <typename TTCoord>
class GOCoordXY
{
public:
   typedef GOCoordXY<TTCoord> TCSelf;
   typedef TTCoord            TCCoord;

   //Le présent template décrit le cas où les données portées par le x 
   //et le y sont du même type. Toutefois, on veut pouvoir y accéder à ces
   //types séparément, de manière standard, quel que soit le type d'objet
   //coordonnées utilisé (y compris objet coordonées avec des types différents)
   //d'ou les typedef ci-dessous.
   typedef TTCoord TCTypeX;
   typedef TTCoord TCTypeY;

                  GOCoordXY     ();
                  GOCoordXY     (const TCCoord&, const TCCoord&);
                  GOCoordXY     (const TCSelf&);
   explicit       GOCoordXY     (const GOCoordXYZ<TCCoord>&);
                  ~GOCoordXY    ();

                  operator GOCoordXYZ<TTCoord> () const;
   TCSelf&        operator=     (const TCSelf&);

         TCCoord& x             ();
   const TCCoord& x             () const;
         TCCoord& y             ();
   const TCCoord& y             () const;
         TCCoord  z             () const;

         TCCoord& operator[]    (Entier);
   const TCCoord& operator[]    (Entier) const;

   TCSelf         operator+     (const TCSelf&) const;
   TCSelf&        operator+=    (const TCSelf&);
   TCSelf         operator-     (const TCSelf&) const;
   TCSelf&        operator-=    (const TCSelf&);
   TCSelf         operator*     (const TCCoord&) const;
   TCSelf&        operator*=    (const TCCoord&);
   TCSelf         operator/     (const TCCoord&) const;
   TCSelf&        operator/=    (const TCCoord&);

   TCSelf         operator-     () const;

   Booleen        operator==    (const TCSelf&) const;
   Booleen        operator!=    (const TCSelf&) const;
   Booleen        operator<     (const TCSelf&) const;
   Booleen        operator>     (const TCSelf&) const;

   Booleen        estDedans     (const TCSelf&, const TCSelf&) const;

   void           minGlobal     (TCSelf&) const;
   void           maxGlobal     (TCSelf&) const;

   void           normalise     ();
   TCCoord        norme         () const;
   TCCoord        norme2        () const;

   static EntierN reqNbrDim     () ;

   static const TCSelf  GRAND;
   static const TCSelf  PETIT;

#ifndef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   friend TTCoord             dist       <>(const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend TTCoord             dist2      <>(const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend TTCoord             prodScal   <>(const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend GOCoordXYZ<TTCoord> prodVect   <>(const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend std::istream&       operator>> <>(std::istream&, GOCoordXY<TTCoord>&);
   friend std::ostream&       operator<< <>(std::ostream&, const GOCoordXY<TTCoord>&);
#else
   friend TTCoord             dist         (const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend TTCoord             dist2        (const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend TTCoord             prodScal     (const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend GOCoordXYZ<TTCoord> prodVect     (const GOCoordXY<TTCoord>&, const GOCoordXY<TTCoord>&);
   friend std::istream&       operator>>   (std::istream&, GOCoordXY<TTCoord>&);
   friend std::ostream&       operator<<   (std::ostream&, const GOCoordXY<TTCoord>&);
#endif   

protected:
   void           invariant     (ConstCarP) const;

private:
   TCCoord leX;
   TCCoord leY;

#ifdef MODE_PERSISTANT
public:
#ifndef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   friend FIFichier&             operator>> <>(FIFichier&, GOCoordXY<TTCoord>&);
   friend FIFichier&             operator<< <>(FIFichier&, const GOCoordXY<TTCoord>&);
#else
   friend FIFichier&             operator>>   (FIFichier&, GOCoordXY<TTCoord>&);
   friend FIFichier&             operator<<   (FIFichier&, const GOCoordXY<TTCoord>&);
#endif   //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
#endif   // MODE_PERSISTANT

#ifdef MODE_IMPORT_EXPORT
public:
   void                          exporte      (FIFichier&) const;
   void                          importe      (FIFichier&);
#endif   // MODE_IMPORT_EXPORT
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
template <typename TTCoord>
inline void
GOCoordXY<TTCoord>::invariant (ConstCarP /*conditionP*/) const
{
//   INVARIANT(VRAI, conditonP);
}
#else
template <typename TTCoord>
inline void
GOCoordXY<TTCoord>::invariant (ConstCarP) const
{
}
#endif

#include "GOCoordXY.hpp"

#endif // GOCOOXY_H_DEJA_INCLU


