//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

#ifndef GOGENERATEURID_HPP_DEJA_INCLU
#define GOGENERATEURID_HPP_DEJA_INCLU

#include "GOPoint.h"

//***********************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Notes:
//
//************************************************************************
inline GOGenerateurID::GOGenerateurID() : m_idSuivante(0)
{
}

//***********************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Notes:
//
//************************************************************************
inline GOGenerateurID::~GOGenerateurID()
{
}

//***********************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Notes:
//
//************************************************************************
template <typename TTIterateur>
inline void GOGenerateurID::asgID(TTIterateur pI, TTIterateur finI)
{
   while (pI != finI)
   {
      (*pI).asgID(m_idSuivante++);
      ++pI;
   }
}

#endif  // GOGENERATEURID_HPP_DEJA_INCLU
