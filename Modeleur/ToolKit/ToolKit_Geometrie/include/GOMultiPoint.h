//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOMultiPoint.h
//
// Classe:  GOMultiPoint
//
// Description: La classe définit une ensemble de points.
//   Elle fait appel aux méthodes de la bibliothèque GEOS, classe 
//    MultiPoint dans certaines de ses méthodes.  
//   
// Attributs:
//   TCContainer     m_multiPoint;      Vecteur de GOPoint
//   GOGenerateurIDP m_genIdP;         Générateur d'ID (assure un ID
//      distinct pour chaque point, utilse pour calculs topologiques).
//   Booleen         m_proprioGenId;   ...
//
// Notes: 
//    SCDOC vérifier la définition ci-dessus.
//************************************************************************
#ifndef GOMULTIPOINT_H_DEJA_INCLU
#define GOMULTIPOINT_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.h"
#include "GOMultiPoint.h"     // .h à cause de l'invariant

#include "GOGenerateurID.hf"
#include "GOMultiPoint.hf"

#include <vector>

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOMultiPoint
{
public:
   typedef GOPoint               TCSommet;
   typedef GOPoint::TCCoord      TCCoord;

   typedef  std::vector<TCSommet>       TCContainer;
   typedef  TCContainer::const_iterator TCConstIterateur;
           
                           GOMultiPoint   ();
                           GOMultiPoint   (const GOMultiPoint&);
                           ~GOMultiPoint  ();
   GOMultiPoint&           operator=      (const GOMultiPoint&);

   void                    asgGenerateurID(GOGenerateurIDP);
         GOGenerateurIDP   reqGenerateurID();
         
   // ---  Parcours
         TCConstIterateur  reqDebut       () const;
         TCConstIterateur  reqFin         () const;
         EntierN           reqNbrPoints   () const;
   const TCSommet&         operator[]     (EntierN) const;

   // ---  Edite
         ERMsg             ajoutePoint    (const TCSommet&);
         ERMsg             deplacePoint   (const TCCoord&, EntierN);
         ERMsg             retirePoint    (EntierN);
         
   // ---  Algo
         Booleen           estUnSommet    (EntierN&, const GOPoint&, const DReel& = GOPoint::tolerance) const;
         ERMsg             reqProprietaire(TCContainer&, const TCCoord&);
   
protected:
         void              invariant      (ConstCarP) const;

private:
   TCContainer     m_multiPoint;
   GOGenerateurIDP m_genIdP;
   Booleen         m_proprioGenId;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOMultiPoint::invariant(ConstCarP conditionP) const
{
   INVARIANT(m_genIdP != NUL, conditionP);
         TCContainer::const_iterator itI = m_multiPoint.begin();
   const TCContainer::const_iterator finI= m_multiPoint.end();
   while (itI != finI)
   {
      INVARIANT((*itI++).reqID() != GOGenerateurID::NON_INITIALISE, conditionP);
   }
}      
#else
inline void GOMultiPoint::invariant(ConstCarP) const
{
}      
#endif

#endif  // GOMULTIPOINT_H_DEJA_INCLU
