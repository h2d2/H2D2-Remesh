//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Classe : GOCoordX
//************************************************************************
#ifndef GOCOOX_HPP_DEJA_INCLU
#define GOCOOX_HPP_DEJA_INCLU

#include "GOEpsilon.h"
#include "GOCoordXY.h"
#include "GOCoordXYZ.h"

#include <cmath>
#include <iostream>
#include <limits>

template <typename TTCoord>
const GOCoordX<TTCoord> GOCoordX<TTCoord>::GRAND = GOCoordX(std::numeric_limits<TTCoord>::max());
template <typename TTCoord>
const GOCoordX<TTCoord> GOCoordX<TTCoord>::PETIT = GOCoordX(std::numeric_limits<TTCoord>::min());

//************************************************************************
// Sommaire: Constructeur par défaut.
//
// Description:
//    Le constructeur public <code>GOCoordX()</code> est le constructeur par
//    défaut de la classe. Il initialise la coordonnée à 0.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordX<TTCoord>::GOCoordX ()
   : leX(0)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur à partir d'une valeur.
//
// Description:
//    Le constructeur public <code>GOCoordX(const TTCoord&, const TTCoord&)</code>
//    construit une coordonnée à partir d'une valeur.
//
// Entrée:
//    const TCCoord& unX
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordX<TTCoord>::GOCoordX (const TCCoord& unX)
   : leX(unX)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur public <code>GOCoordX(const GOCoordX&)</code> construit
//    une copie de la coordonnée passée en argument.
//
// Entrée:
//    const GOCoordX& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordX<TTCoord>::GOCoordX (const TCSelf& p)
   : leX(p.leX)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur à partir d'un point 2D
//
// Description:
//    Le constructeur public <code>GOCoordX(const GOCoordXYZ&)</code> 
//    construit une coordonnée à une dimension à partir de coordonnée
//    à deux dimensions.
//    La composante y est ignorée.
//
// Entrée:
//    const GOCoordXY& p      Point dont on copie le x
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordX<TTCoord>::GOCoordX (const GOCoordXY<TCCoord>& p)
   : leX(p.x())
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur à partir d'un point 3D
//
// Description:
//    Le constructeur public <code>GOCoordX(const GOCoordXYZ&)</code> 
//    construit une coordonnée à une dimension à partir de coordonnée
//    à deux dimensions.
//    Les composantes y et z sont ignorées.
//
// Entrée:
//    const GOCoordXYZ& p      Point dont on copie le x
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordX<TTCoord>::GOCoordX (const GOCoordXYZ<TCCoord>& p)
   : leX(p.x())
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
} 

//************************************************************************
// Sommaire: Destructeur de la classe
//
// Description:
//    Le destructeur public <code>~GOCoordX()</code> ne fait rien de
//    spécifique.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordX<TTCoord>::~GOCoordX ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordX<TTCoord>::operator GOCoordXYZ<TCCoord>() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return GOCoordXYZ<TCCoord>(leX, 0, 0);
}  // GOCoordXY::GOCoordXY<TTCoord>

//************************************************************************
// Sommaire: Nombre de dimensions de la coordonnée
//
// Description:
//    La méthode static public <code>reqNbrDim()</code> retourne la dimension 
//    de la coordonnée
//
// Entrée:
//
// Sortie:
//   EntierN : la dimension de la coordonnée
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline EntierN
GOCoordX<TTCoord>::reqNbrDim() 
{
   return 1;
}

//************************************************************************
// Sommaire: Composante x de la coordonnée
//
// Description:
//    La méthode public <code>x()</code> retourne la coordonnée x (première
//    composante) du point. La coordonnées est retournée par référence, ce qui
//    permet à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCCoord&
GOCoordX<TTCoord>::x ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leX;
}  // GOCoordX::GOCoordX<TTCoord>

//************************************************************************
// Sommaire: Composante x de la coordonnée
//
// Description:
//    La méthode public <code>x()</code> retourne la coordonnée x (première
//    composante) du point. La coordonnées est retournée par référence constante,
//    ce qui ne permet pas à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordX<TTCoord>::TCCoord&
GOCoordX<TTCoord>::x () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leX;
}  // GOCoordX::GOCoordX<TTCoord>

//************************************************************************
// Sommaire: Composante y de la coordonnée (invalide)
//
// Description:
//    La méthode public <code>y()</code> est implantée pour permettre des
//    algorithme génériques. Comme cette composante n'existe pas, on retourne
//    une valeur invalide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordX<TTCoord>::TCCoord
GOCoordX<TTCoord>::y () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return std::numeric_limits<TTCoord>::infinity();
}  // GOCoordX::GOCoordX<TTCoord>

//************************************************************************
// Sommaire: Composante z de la coordonnée (invalide)
//
// Description:
//    La méthode public <code>z()</code> est implantée pour permettre des
//    algorithme génériques. Comme cette composante n'existe pas, on retourne
//    une valeur invalide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordX<TTCoord>::TCCoord
GOCoordX<TTCoord>::z () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return std::numeric_limits<TTCoord>::infinity();
}  // GOCoordX::GOCoordX<TTCoord>

//************************************************************************
// Sommaire:  Définition de l'opérateur []
//
// Description:   N'accepte que l'indice 0 et retourne la valeur du x
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCCoord&
GOCoordX<TTCoord>::operator[] (Entier i)
{
#ifdef MODE_DEBUG
   PRECONDITION(i == 0);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return leX;
}  // GOCoordX::operator[]

//************************************************************************
// Sommaire:  Définition de l'opérateur []
//
// Description:   N'accepte que l'indice 0 et retourne la valeur du x
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordX<TTCoord>::TCCoord&
GOCoordX<TTCoord>::operator[] (Entier
#ifdef MODE_DEBUG
                               i
#endif   // MODE_DEBUG
                               ) const
{
#ifdef MODE_DEBUG
   PRECONDITION(i == 0);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return leX;
}  // GOCoordX::operator[]

//************************************************************************
// Sommaire:  Définition de l'opérateur + pour des GOCoordX
//
// Description:
//    L'opérateur public <code>operator+</code> définit l'addition entre deux
//    <code>GOCoordX</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf
GOCoordX<TTCoord>::operator+ (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res += p;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordX::operator+

//************************************************************************
// Sommaire:
//    Définition de l'opérateur += pour des GOCoordX
//
// Description:
//    L'opérateur public <code>operator+</code> définit l'additon entre deux
//    <code>GOCoordX</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf&
GOCoordX<TTCoord>::operator+= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX += p.leX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordX::operator+=

//************************************************************************
// Description:
//    Définition de l'opérateur + pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf
GOCoordX<TTCoord>::operator- (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res -= p;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordX::operator-

//************************************************************************
// Description:
//    Définition de l'opérateur - pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf
GOCoordX<TTCoord>::operator- () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res;
   res.leX = -leX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordX::operator-

//************************************************************************
// Description:
//    Définition de l'opérateur -= pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf&
GOCoordX<TTCoord>::operator-= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX -= p.leX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordX::operator-=

//************************************************************************
// Description:
//    Définition de l'opérateur * pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf
GOCoordX<TTCoord>::operator* (const TCCoord& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res *= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordX::operator*

//************************************************************************
// Description:
//    Définition de l'opérateur *= pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf&
GOCoordX<TTCoord>::operator*= (const TCCoord& val)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX *= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordX::operator*=

//************************************************************************
// Description:
//    Définition de l'opérateur / pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf
GOCoordX<TTCoord>::operator/ (const TCCoord& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res /= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordX::operator/

//************************************************************************
// Description:
//    Définition de l'opérateur /= pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf&
GOCoordX<TTCoord>::operator/= (const TCCoord& val)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX /= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordX::operator/=

//************************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCSelf&
GOCoordX<TTCoord>::operator= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (this != &p)
   {
      leX = p.leX;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return *this;
}  // GOCoordX::GOCoordX<TTCoord>

//************************************************************************
// Description:
//    Définition de l'opérateur == pour des GOCoordX.
//    On utilise l'objet GOEpsilon pour faire la comparaison entre deux
//    double...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordX<TTCoord>::operator== (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return (GOEpsilon(leX) == p.leX);
}  // GOCoordX::operator==

//************************************************************************
// Description:
//    Définition de l'opérateur != pour des GOCoordX
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordX<TTCoord>::operator!= (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return !(*this == p);
}  // GOCoordX::operator!=

//************************************************************************
// Sommaire:   Retourne VRAI si le point est à l'intérieur
//
// Description:
//    La méthode publique <code>estDedans()</code> retourne VRAI si 
//    le point est compris dans le volume délimité par les deux  
//    points min et max passés en argument.
//
// Entrée:
//    const TCSelf& pMin     // Point Min
//    const TCSelf& pMax     // Point Max
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen
GOCoordX<TTCoord>::estDedans (const TCSelf& pMin,
                              const TCSelf& pMax) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   // Puisqu'il n'y a pas d'opérateur >=, on emploie l'expression négative !(<)
   return (!(leX < pMin.leX) && !(leX > pMax.leX));
}

//************************************************************************
// Description:
//    Définition de l'opérateur < pour des GOCoordX.  On utilise l'objet
//    GOEpsilon pour faire la comparaison entre deux doubles...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordX<TTCoord>::operator< (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return (GOEpsilon(leX) < p.leX);
}  // GOCoordX::operator<

//************************************************************************
// Description:
//    Définition de l'opérateur > pour des GOCoordX.  On utilise l'objet
//    GOEpsilon pour faire la comparaison entre deux doubles...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordX<TTCoord>::operator> (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return (GOEpsilon(leX) > p.leX);
}  // GOCoordX::operator>

//************************************************************************
// Description:
//    Définition de l'opérateur * pour des GOCoordX et Reel.
//    Cette fonction est nécessaire pour permettre une opération
//    avec arguments inversés.  En effet, p1 * val n'est pas équivalent
//    à val * p1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline GOCoordX<TTCoord>
operator* (const TTCoord& val, const GOCoordX<TTCoord>& p)
{
#ifdef MODE_DEBUG
   p.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (p * val);
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un minimum global
//
// Description:  La méthode public minGlobal(...) permet de contribuer
//               à la détermination d'un minimum global à une collection
//               d'objet de ce type.  On passe un minGlobal en argument et
//               si l'une de nos composantes est plus petite, on assigne
//               celle-ci au minimum global.
//
// Entrée:
//
// Sortie:       TCSelft& minGlobal : le minimum global.
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline void
GOCoordX<TTCoord>::minGlobal(TCSelf& minGlobal) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (leX < minGlobal.x()) minGlobal.x() = leX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un maximum global
//
// Description:  La méthode public maxGlobal(...) permet de contribuer
//               à la détermination d'un maximum global à une collection
//               d'objet de ce type.  On passe un maxGlobal en argument et
//               si l'une de nos composantes est plus grande, on assigne
//               celle-ci au maximum global.
//
// Entrée:
//
// Sortie:       TCSelft& maxGlobal : le maximum global.
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline void
GOCoordX<TTCoord>::maxGlobal(TCSelf& maxGlobal) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (leX > maxGlobal.x()) maxGlobal.x() = leX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La méthode publique <code>normalise()</code> normalise le vecteur (point),
//    donc le rend tel que sa norme soit 1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
void
GOCoordX<TTCoord>::normalise()
{
#ifdef MODE_DEBUG
   PRECONDITION(norme2() != 0.0);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   double nrm = static_cast<double>(leX*leX);
   nrm = 1.0 / INRS_LIBC_STD::sqrt(nrm);
   leX *= nrm;

#ifdef MODE_DEBUG
   POSTCONDITION(INRS_LIBC_STD::fabs(norme2()-1.0) <= 1.0e-15);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La fonction <code>normalise()</code> normalise le point (vecteur) 
//    passé en argument, donc le rend tel que sa norme soit 1.
//
// Entrée:
//    const GOCoordX<TTCoord>& p     Point à normaliser
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
GOCoordX<TTCoord>
normalise(const GOCoordX<TTCoord>& p)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   typename GOCoordX<TTCoord>::TCSelf tmp = p;
   tmp.normalise();

#ifdef MODE_DEBUG
#endif   // MODE_DEBUG
   return tmp;
}

//************************************************************************
// Description:
//    Retourne la norme du vecteur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCCoord
GOCoordX<TTCoord>::norme() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   double prod2 = static_cast<double>(leX*leX);
   return static_cast<TCCoord>(INRS_LIBC_STD::sqrt(prod2));
}  // TTCoord GOCoordX<TTCoord>::norme ()

//************************************************************************
// Description:
//    Retourne le carré de la norme du vecteur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordX<TTCoord>::TCCoord
GOCoordX<TTCoord>::norme2() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCCoord prod = leX*leX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return prod;
}  // TTCoord GOCoordX<TTCoord>::norme2 ()

//************************************************************************
// Description:
//    Retourne le produit scalaire entre les deux vecteurs. Il correspond
//    à la projection d'un vecteur sur l'autre.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
prodScal(const GOCoordX<TTCoord>& p1, const GOCoordX<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordX<TTCoord>::TCCoord TCCoord;
   double prod2 = static_cast<double>(p1.leX*p2.leX);

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return static_cast<TCCoord>(sqrt(prod2));
}  // TTCoord prodScal (const GOCoordX<TTCoord> &p1, const GOCoordXYZ<TTCoord> &p2)

//************************************************************************
// Description:
//    Retourne le carré du produit scalaire entre les deux vecteurs.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
prodScal2(const GOCoordX<TTCoord>& p1, const GOCoordX<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordX<TTCoord>::TCCoord TCCoord;
   TCCoord prod = p1.leX*p2.leX;

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return prod;
}  // TTCoord prodScal2 (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Description:
//    Retourne le produit vectoriel entre deux vecteurs. Il correspond à
//    l'aire du parallélogramme construit sur les deux vecteurs.
//
// Entrée:
//    const TCSelf& p  // Premier vecteur
//    const TCSelf& p  // Deuxième vecteur
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
GOCoordXYZ<TTCoord>
prodVect(const GOCoordX<TTCoord>& p1, const GOCoordX<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return GOCoordXYZ<TTCoord>(0, 0, 0);
}  // TCCoord prodVect (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Description:
//    Retourne la distance euclidienne entre deux points.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
dist(const GOCoordX<TTCoord>& p1, const GOCoordX<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordX<TTCoord>::TCCoord TCCoord;
   TCCoord dx = p2.leX - p1.leX;
   double prod2 = static_cast<double>(dx*dx);

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return static_cast<TCCoord>(sqrt(prod2));
}  // TTCoord prodScal (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Description:
//    Retourne le carré de la distance euclidienne entre deux points.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
dist2(const GOCoordX<TTCoord>& p1, const GOCoordX<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordX<TTCoord>::TCCoord TCCoord;
   TCCoord dx = p2.leX - p1.leX;

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (dx*dx);
}  // TTCoord dist2 (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordX</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>istream</code> pour celle-ci.
//
// Entrée:
//    istream& is                   // Stream de lecture
//    TCSelf& p         // GOCoordX à lire
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
std::istream&
operator >> (std::istream& is, GOCoordX<TTCoord>& p)
{
#ifdef MODE_DEBUG
   p.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordX<TTCoord>::TCCoord TCCoord;

   if (is)
   {
      TCCoord leX;
      is >> leX;
      if (is)
      {
         p.leX = leX;
      }
   }

#ifdef MODE_DEBUG
   p.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return is;
}  // PRStreamLecture& operator >> (istream&, TCSelf&)

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator <<(...)</code> friend de la classe
//    <code>GOCoordX</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>ostream</code> pour celle-ci.
//
// Entrée:
//    ostream& os                   // Stream d'écriture
//    const TCSelf& p               // GOCoordX à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
std::ostream&
operator << (std::ostream& os, const GOCoordX<TTCoord>& p)
{
#ifdef MODE_DEBUG
   p.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (os)
   {
      os << p.leX;
   }

#ifdef MODE_DEBUG
   p.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return os;
}  // ostream& operator << (ostream&, const TCSelf&)

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordX</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& is                 // Fichier de lecture
//    GOCoordX<TTCoord>& p        // GOCoordX à lire
//
// Sorties:
//    FIFichier& : Référence vers le fichier lu.
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
#ifdef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator >> <>(FIFichier& is, GOCoordX<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (is)
      {
         TTCoord leX;
         is >> leX;
         if (is)
         {
            p.leX = leX;
         }
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return is;
   }
#else //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator >> (FIFichier& is, GOCoordX<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (is)
      {
         TTCoord leX;
         is >> leX;
         if (is)
         {
            p.leX = leX;
         }
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return is;
   }
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator <<(...)</code> friend de la classe
//    <code>GOCoordX</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& os                 // Fichier de écrire
//    GOCoordX<TTCoord>& p        // GOCoordX à écrire
//
// Sortie:
//    FIFichier& : Référence vers le fichier écrit
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
#ifdef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator << <>(FIFichier& os, const GOCoordX<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (os)
      {
         os << p.leX;
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return os;
   }
#else //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator << (FIFichier& os, const GOCoordX<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (os)
      {
         os << p.leX;
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return os;
   }
   #endif   // MODE_PERSISTANT
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordX</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& is                 // Fichier de lecture
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void GOCoordX<TTCoord>::importe(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (is)
   {
      TTCoord x;
      is >> x;
      if (is)
      {
         leX = x;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return;
}
#endif   // MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordX</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& os : Référence vers le fichier lu
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void GOCoordX<TTCoord>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (os)
   {
      os << leX;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return os;
}
#endif   // MODE_IMPORT_EXPORT

#endif //GOCOOX_HPP_DEJA_INCLU
