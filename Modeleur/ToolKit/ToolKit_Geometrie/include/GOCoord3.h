//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: gocoord3.h
//
// Classe: GOCoordonneesXYZ
//
// Sommaire:
//   Déclaration de la classe GOCoordonneesXYZ
//
// Description:
//   La classe <code>GOCoordonneesXYZ</code> est déclarée comme instance
//   de la classe tempalte <code>GOCoordXYZ<DReel></code>.
//
// Attributs:
//
// Notes:
//
//************************************************************************
// 16-10-1997  Yves Secretan      Version initiale
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#ifndef GOCOORD3_H_DEJA_INCLU
#define GOCOORD3_H_DEJA_INCLU

#include "sytypes.h"
#include "GOCoordXYZ.h"

DECLARE_TYPE(GOCoordXYZ<DReel>, GOCoordonneesXYZ);

#endif // GOCOORD3_H_DEJA_INCLU
