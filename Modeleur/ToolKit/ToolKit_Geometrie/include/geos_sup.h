//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: geos_sup.h
//
// Classe:  namespace geos_sup
//
// Description: Établit les liens entre les classes Modeleur et les
//    classes de geos.
//   
// Attributs:
//
// Notes: 
//
//************************************************************************
//************************************************************************
#ifndef GEOS_SUP_H_DEJA_INCLU
#define GEOS_SUP_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.hf"
#include "GOPolyligne.hf"
#include "GOPolyligneFermee.hf"
#include "GOPolygone.hf"
#include "GOMultiPoint.hf"
#include "GOMultiPolyligne.hf"
#include "GOMultiPolygone.hf"
#include "GOGeometrie.hf"
#include "GOEnveloppe.hf"

#include <geos_c.h>

#include <memory>
#include <string>
#include <boost/interprocess/smart_ptr/unique_ptr.hpp>

extern "C"
{
   typedef void (*TTGeomDeleter)(GEOSGeometry *);
   typedef void (*TTCharDeleter)(char *);
}

namespace geos_sup
{
//   typedef void (*TTGeomDeleter)(GEOSGeometry *);
//   typedef void (*TTCharDeleter)(void *);
   typedef boost::interprocess::unique_ptr<GEOSGeometry, TTGeomDeleter> TTGeomUniquePtr;
   typedef boost::interprocess::unique_ptr<char, TTCharDeleter>         TTCharUniquePtr;

   DLL_IMPEXP(TOOLKIT_GEOMETRIE) GEOSContextHandle_t reqContexte();
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) ERMsg               reqMsgErreur();

   DLL_IMPEXP(TOOLKIT_GEOMETRIE) GEOSCoordSequence* Coordinate     (const ::GOPoint&);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    Point          (const ::GOPoint&);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    LinearRing     (const ::GOPolyligneFermee&);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    LineString     (const ::GOPolyligne&);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    Polygon        (const ::GOPolygone&);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    MultiPoint     (const ::GOMultiPoint&); 
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    MultiLineString(const ::GOMultiPolyligne&); 
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    MultiPolygon   (const ::GOMultiPolygone&); 
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    Geometry       (const ::GOGeometrie&);
   //N'utilise pas de smart-pointeur car l'enveloppe appartient à la géométrie, 
   //et sera détruite automatiquement lors de la destruction de celle-ci.
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr    Envelope      (const ::GOEnveloppe&);

   DLL_IMPEXP(TOOLKIT_GEOMETRIE) ::GOPoint                            GOPoint           (GEOSCoordSequence const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) ::GOPoint                            GOPoint           (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOPolyligne>        GOPolyligne       (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOPolyligneFermee>  GOPolyligneFermee (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOPolygone>         GOPolygone        (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOMultiPoint>       GOMultiPoint      (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOMultiPolyligne>   GOMultiPolyligne  (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOMultiPolygone>    GOMultiPolygone   (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOGeometrie>        GOGeometrie       (GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::unique_ptr< ::GOEnveloppe>        GOEnveloppe       (GEOSGeometry const*);

   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOPolyligne&,       GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOPolyligneFermee&, GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOPolygone&,        GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOMultiPoint&,      GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOMultiPolyligne&,  GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOMultiPolygone&,   GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOGeometrie&,       GEOSGeometry const*);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void  transfer(::GOEnveloppe&,       GEOSGeometry const*);

   DLL_IMPEXP(TOOLKIT_GEOMETRIE) void                                lis  (::GOGeometrie&, const std::string&);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTGeomUniquePtr                     lis  (const std::string&);

   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTCharUniquePtr                     ecris(const ::GOGeometrie&);
   DLL_IMPEXP(TOOLKIT_GEOMETRIE) TTCharUniquePtr                     ecris(GEOSGeometry const*);
}

#endif  // GEOS_SUP_H_DEJA_INCLU
