//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPolygone.h
//
// Classe:  GOPolygone
//      
// Description: La classe GOPolygone définit un objet géométrique de type
//   polygone dans un système cartésien tri-dimensionnel. 
//   Elle fait appel aux méthodes de la bibliothèque GEOS, classe
//     Polygon, au besoin.  
//   
// Attributs:
//    TCContainerPolylignes m_polylignes;   Liste des polylignes fermées 
//        représentant le contour extérieur du polygone et les contours
//       intérieurs  (trous) s'il y a lieu.
//    GOGenerateurIDP       m_genIdP;      Générateur d'id pour les arêtes
//
// Notes: 
//    SCDOC vérifier la définition ci-dessus.
//   Les invariants restent à définir. Actuellement, deux polylignes fermées
//   qui ne sont pas l'une à l'intérieur de l'autre (càd un multipolygone)
//   seraient acceptées par la présente classe.
//************************************************************************
#ifndef GOPOLYGONE_H_DEJA_INCLU
#define GOPOLYGONE_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.h"
#include "GOPolyligneFermee.h"

#include "geos.hf"
#include "GOGenerateurID.hf"
#include "GOGeometrie.hf"
#include "GOMultiPolygone.hf"
#include "GOPolygone.hf"

#include <list>

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOPolygone
{
public:   
   typedef GOPoint          TCSommet;
   typedef GOPoint::TCCoord TCCoord;

   typedef std::list<GOPolyligneFermee>          TCContainerPolylignes;
   typedef TCContainerPolylignes::iterator       TCIterateur;
   typedef TCContainerPolylignes::const_iterator TCConstIterateur;

                       GOPolygone     ();
                       GOPolygone     (const GOPolygone&);
                       ~GOPolygone    ();
         GOPolygone&   operator =     (const GOPolygone&);

   void                    asgGenerateurID(GOGenerateurIDP);
         GOGenerateurIDP   reqGenerateurID();

   // ---  Parcours
         TCConstIterateur   reqDebut         () const;
         TCConstIterateur   reqFin           () const;
         EntierN            reqNbrPolylignes () const;
         EntierN            reqNbrSommets    () const;
   const GOPolyligneFermee& operator []      (EntierN) const;

   // ---  Edite
         ERMsg         ajoutePolyligne (const GOPolyligneFermee&);
         template <typename TTIterateur>
         ERMsg         ajoutePolyligne (TTIterateur, TTIterateur);
         template <typename TTIterateur>
         ERMsg         ajouteSommets   (EntierN, EntierN, TTIterateur, TTIterateur);

         template <typename TTIterateur>
         ERMsg         assigne         (TTIterateur, TTIterateur);

         ERMsg         ajoutePointsCroisement(const GOPolygone&);
         ERMsg         deplace         (const TCCoord&);
         ERMsg         deplacePL       (const TCCoord&, EntierN);
         ERMsg         deplacePTdePL   (const TCCoord&, EntierN, EntierN);
         ERMsg         difference      (GOGeometrie&, const GOPolygone&) const;
         ERMsg         intersecte      (Booleen&, const GOPolyligne&) const; // à enlever
         ERMsg         intersection    (GOGeometrie&, const GOPolygone&) const;
         ERMsg         soisAntiHoraire ();

         Booleen       operator ==     (const GOPolygone&) const;
         Booleen       operator !=     (const GOPolygone&) const;
      
         Booleen       pointInterieur  (const TCCoord&) const;
         Booleen       estUnSommet     (EntierN&, const GOPoint&, const DReel& = GOPoint::tolerance) const;
//         ERMsg         renumerote      (const EntierN&);
         ERMsg         supprimePL      (EntierN);
         ERMsg         supprimePTdePL  (EntierN, EntierN);

protected:
   void     invariant       (ConstCarP) const;

private:
   TCContainerPolylignes m_polylignes;
   GOGenerateurIDP       m_genIdP;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//   
//************************************************************************
#ifdef MODE_DEBUG
inline void GOPolygone::invariant(ConstCarP conditionP) const
{
   INVARIANT(m_genIdP != NUL, conditionP);
//   INVARIANT(m_polygoneP != NUL, conditionP);

   //TODO SC - L'intégrité géométrique n'est pas vérifiée, voir notes MG ci-dessous
   // Le plus simple serait de tenter de convertir l'objet vers geos.

   //VOIR QUOI VÉRIFIER DANS L'INVARIANT ET QUOI VÉRIFIER AU FUR ET À MESURE
   //-toutes les polylignes ont 4 sommets ou plus
   //-toutes les polylignes sont fermées
   //-les polylignes de 1 à nbrePoly -1 sont toutes dans la polyligne 0 sans lui
   // toucher
   //-les polylignes de 1 à nbrPoly - 1 se touchent 2 à 2 en un pt au plus
   //- les polylignes sont simples
   // - le polygone est connexe
   // size() est plus petit que la val max d'un entier (et non d'un entierN)

}      
#else
inline void GOPolygone::invariant(ConstCarP) const
{
}
#endif

#include "GOPolygone.hpp"

#endif  // GOPOLYGONE_H_DEJA_INCLU
