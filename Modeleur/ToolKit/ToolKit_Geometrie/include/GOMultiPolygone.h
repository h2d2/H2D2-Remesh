//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOMultiPolygone.h
//
// Classe:  GOMultiPolygone
//
// Description: La classe définit un ensemble de polygones.
//   Elle fait appel aux méthodes de la bibliothèque GEOS, classe 
//     MultiPolygon dans certaines de ses méthodes.  
//   
// Attributs:
//   TCContainer     m_multiPolygone;   Vecteur de pointeurs aux GOPolygones
//   GOGenerateurIDP m_genIdP;         Générateur d'ID (assure un ID
//       distinct pour chaque polygone, utilisé pour calculs topologiques).
//   Booleen         m_proprioGenId;   ...
//   GOEnveloppe     m_enveloppe;      Rectangle englobant le multipolygone
//   Booleen         m_geom_changee;   Indique que la géométrie a changé
//       et que l'enveloppe devra être recalculée à la prochaine demande.
//       L'enveloppe et son booléen sont mutable afin de permettre de définir
//       un accesseur reqEnveloppe constant (le recalcul a lieu sur reqEnveloppe
//       afin d'optimiser, mais l'objet n'a pas réellement changé).
//
// Notes: 
//  TOUTE méthode qui modifie la géométrie de l'objet doit indiquer que la
//    géométrie a changé en indiquant m_geom_changee = VRAI, sans quoi
//    l'enveloppe ne sera pas mise à jour.
//  Les règles d'intégrité (par exemple polygones ne peuvent se chevaucher)
//    ne sont pas implantées.
//************************************************************************
#ifndef GOMULTIPOLYGONE_H_DEJA_INCLU
#define GOMULTIPOLYGONE_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOCoord3.h"
#include "GOPoint.h"
#include "GOGenerateurID.hf"
#include "GOGeometrie.hf"
#include "GOMultiPoint.hf"
#include "GOMultiPolygone.hf"
#include "GOPolygone.hf" 
#include "GOPolyligneFermee.hf"

#include "GOEnveloppe.h"

#include <boost/iterator/indirect_iterator.hpp>
#include "GOPolygone.h"
BOOST_TT_BROKEN_COMPILER_SPEC(GOPolygone)

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOMultiPolygone
{
public:
   typedef GOPoint               TCSommet;   //TODO pourquoi renommer
   typedef GOPoint::TCCoord      TCCoord;

   typedef  std::vector<GOPolygoneP>                   TCContainer;
   typedef  TCContainer::const_iterator                TCContainerIter;
   typedef  boost::indirect_iterator<TCContainerIter>  TCConstIterateur;
            
                          GOMultiPolygone   ();
                          GOMultiPolygone   (const GOMultiPolygone&);
                          ~GOMultiPolygone  ();
   GOMultiPolygone&       operator=         (const GOMultiPolygone&);

   void                   asgGenerateurID(GOGenerateurIDP);
         GOGenerateurIDP  reqGenerateurID();
         
   Booleen       operator ==     (const GOMultiPolygone&) const;

   // ---  Parcours
         TCConstIterateur reqDebut       () const;
         TCConstIterateur reqFin         () const;
         EntierN          reqNbrPolygones() const;
         EntierN          reqNbrSommets  () const;
   const GOPolygone&      operator[]     (EntierN) const;

   // --- Attributs
         GOEnveloppe&     reqEnveloppe   () const; //TODO non const

   // ---  Edite
         ERMsg ajoutePointsCroisement(const GOMultiPolygone&);
         ERMsg ajoutePolygone    (const GOPolygone&);
         ERMsg ajoutePolygone    (const GOPolyligneFermee&);
         template <typename TTIterateur>
         ERMsg ajoutePolygone    (TTIterateur, TTIterateur);
         template <typename TTIterateur>
         ERMsg ajoutePLdePG      (EntierN, TTIterateur, TTIterateur);
         template <typename TTIterateur>
         ERMsg ajoutePTdePLdePG  (EntierN, EntierN, EntierN, TTIterateur, TTIterateur);
         ERMsg deplacePG         (const TCCoord&, EntierN);
         ERMsg deplacePLdePG     (const TCCoord&, EntierN, EntierN);
         ERMsg deplacePTdePLdePG (const TCCoord&, EntierN, EntierN, EntierN);
         ERMsg retirePG          (EntierN);
         ERMsg retirePLdePG      (EntierN, EntierN);
         ERMsg retirePTdePLdePG  (EntierN, EntierN, EntierN);

   // ---  Algo
         ERMsg    difference     (const GOMultiPolygone&);
         ERMsg    difference     (GOGeometrie&, const GOMultiPolygone&) const;
         Booleen  estUnSommet    (EntierN&, const GOPoint&, const DReel& = GOPoint::tolerance) const;
         ERMsg    intersection   (GOGeometrie&, const GOMultiPoint&) const;
         ERMsg    intersection   (GOGeometrie&, const GOMultiPolygone&) const;
         ERMsg    intersection   (GOGeometrie&, const GOGeometrie&) const;
         Booleen  pointInterieur (const TCCoord&)     const;
         ERMsg    soisAntiHoraire();
/*
         ERMsg    reqProprietaire(TCContainer&, const TCCoord&);
*/

protected:
         void     invariant      (ConstCarP) const;

private:
         ERMsg    recalculeEnveloppe() const;

            TCContainer     m_multiPolygone;
            GOGenerateurIDP m_genIdP;
            Booleen         m_proprioGenId;
   mutable  Booleen         m_geom_changee;
   mutable  GOEnveloppe     m_enveloppe;
   
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOMultiPolygone::invariant(ConstCarP conditionP) const
{
   INVARIANT(m_genIdP != NUL, conditionP);
   //TODO il faudrait vérifer la validité du multipolygone suite à l'ajout
   //de polygones (ne doivent pas se croiser). Ceci pourrait être fait en
   //tenant de convertir le multipoly à geos.
}
#else
inline void GOMultiPolygone::invariant(ConstCarP) const
{
}      
#endif

#include "GOMultiPolygone.hpp"

#endif  // GOMULTIPOLYGONE_H_DEJA_INCLU
