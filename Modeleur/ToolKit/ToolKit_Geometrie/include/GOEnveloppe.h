//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOEnveloppe.h
//
// Classe:  GOEnveloppe version de SREnveloppe spécialisée 
//    pour les GOCoordonneesXYZ
//
// Description: 
//    La classe <code>GOEnveloppe</code> décrit une enveloppe (boudning-box) 2D,
//    compatible avec GEOS (OpenGIS). Elle est structurée pour être compatible
//    avec une spécialisation du template SRRegion pour les GOCoordonneesXYZ.
//    Elle est déclarée comme une classe régulière plutôt que comme une 
//    spécialisation de template afin d'être utilisable indépendemment du
//    module ChampSeries. Elle est employée comme spécialisation de la classe
//    template SREnveloppe.
//
// Attributs:
//    GOCoordonneesXYZ m_min     Coordonnées minimales et maximales de l'enveloppe.
//    GOCoordonneesXYZ m_max
//    
// Note:
//
//************************************************************************
#ifndef GOENVELOPPE_H_DEJA_INCLU
#define GOENVELOPPE_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOCoord3.h"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOEnveloppe
{
public:
                     GOEnveloppe     ();
                     GOEnveloppe     (const GOEnveloppe&);
                    ~GOEnveloppe     ();
   GOEnveloppe&      operator=       (const GOEnveloppe&); 

   ERMsg             asgLimites      (const GOCoordonneesXYZ&, const GOCoordonneesXYZ&);
   Booleen           estValide       () const;
   Booleen           pointInterieur  (const GOCoordonneesXYZ&) const;

   GOCoordonneesXYZ  reqCoordMin     () const;
   GOCoordonneesXYZ  reqCoordMax     () const;  

   Booleen           operator==      (const GOEnveloppe&) const;
   Booleen           operator!=      (const GOEnveloppe&) const;
          
protected:
   void              invariant       (ConstCarP) const;

private:
   GOCoordonneesXYZ m_min;
   GOCoordonneesXYZ m_max;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOEnveloppe::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GOEnveloppe::invariant(ConstCarP) const
{
}
#endif      

#endif  // GOENVELOPPE_H_DEJA_INCLU
