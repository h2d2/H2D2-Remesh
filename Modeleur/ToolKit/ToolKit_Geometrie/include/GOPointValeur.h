//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPointValeur.h
//
// Classe:  GOPointValeur
//
// Description: La classe GOPointValeur est un adaptateur pour les 
//   points définis avec TerraLib. La classe GOPointValeur 
//   constitue donc un interface. Ceci permet de pouvoir utiliser nos 
//   propres types de données. 
//   
// Attributs:
//    TePointP m_pointP : un pointTerralib
//
// Notes: 
//
//************************************************************************
// 10-03-2004  Maude Giasson          Version initiale
// 23-06-2004  Maude Giasson          Ajout d'un ID
//************************************************************************
#ifndef GOPOINTVALEUR_H_DEJA_INCLU
#define GOPOINTVALEUR_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.h"
#include "GOEllipseErreur.h"

class GOPointValeur : public GOPoint
{
public:         
   typedef GOEllipseErreur  TCValeur;

                     GOPointValeur  ();
                     GOPointValeur  (const TCCoord&, const TCValeur& = TCValeur());
                     GOPointValeur  (const GOPointValeur&);
                    ~GOPointValeur  ();
   GOPointValeur&    operator=      (const GOPointValeur&);

   void              asgValeur      (const TCValeur&);
   const TCValeur&   reqValeur      () const;

   Booleen           operator ==    (const GOPointValeur&) const;
   Booleen           operator !=    (const GOPointValeur&) const;
    
protected:
   void              invariant      (ConstCarP) const;

private:
   TCValeur m_valeur;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOPointValeur::invariant(ConstCarP conditionP) const
{
   GOPoint::invariant(conditionP);
}      
#else
inline void GOPointValeur::invariant(ConstCarP) const
{
}      
#endif

#endif  // GOPOINTVALEUR_H_DEJA_INCLU
