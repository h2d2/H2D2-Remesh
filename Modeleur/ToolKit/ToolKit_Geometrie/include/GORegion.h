//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GORegion.h
//
// Classe:  GORegion version de SRRegion spécialisée pour les GOCoordonneesXYZ
//
// Description: 
//    Décrit une région spatiale 2D, compatible avec GEOS (OpenGIS).
//    Spécialisation du template SRRegion pour les GOCoordonneesXYZ.
//    Est déclarée comme une classe régulière plutôt que comme une 
//    spécialisation de template afin d'être utilisable indépendemment du
//    module ChampSeries. Elle est employée comme spécialisation de la classe
//    template SRRegion.
//
// Attributs:
//    GOMultiPolygone  multiPoly : multipolygone GEOS identifiant la région
//
//************************************************************************
#ifndef GOREGION_H_DEJA_INCLU
#define GOREGION_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOCoord3.h"
#include "GOMultiPolygone.h"
#include "GOPolygone.h"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GORegion
{
public:

   GORegion                ();
   GORegion                (const GORegion&);
   ~GORegion               ();
   GORegion&  operator=    (const GORegion&); 

   void                    asgMultiPolygone  (const GOMultiPolygone&); 
   ERMsg                   ajoutePolygone    (const GOPolygone&); 
   Booleen                 estValide         () const;
   const GOEnveloppe&      reqEnveloppe      () const;
   const GOMultiPolygone&  reqMultiPolygone  () const;

   Booleen  pointInterieur (const GOCoordonneesXYZ&) const;

   Booleen  operator==     (const GORegion&) const;
   Booleen  operator!=     (const GORegion&) const;
          
protected:
   void     invariant      (ConstCarP) const;

private:
   GOMultiPolygone multiPoly;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//************************************************************************
#ifdef MODE_DEBUG
inline void GORegion::invariant(ConstCarP /*conditionP*/) const
{
   //TODO il faudrait vérifer la validité du multipolygone suite à l'ajout
   //de polygones (ne doivent pas se croiser). Ceci devrait être fait dans 
   //GOMultiPolygone.
}      
#else
inline void GORegion::invariant(ConstCarP) const
{
}      
#endif

#endif  // GOREGION_H_DEJA_INCLU
