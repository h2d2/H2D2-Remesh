//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: GOPolygone.hpp
// Classe : GOPolygone
//************************************************************************
// 18-02-2004  Maude Giasson          Version initiale
//************************************************************************
#ifndef GOPOLYGONE_HPP_DEJA_INCLU
#define GOPOLYGONE_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:  Ajoute une une polyligne au polygone
//
// Description:
//    La méthode <code>ajoutePolyligne(...)</code> ajoute une polyligne
//    au polygone.
//
// Entrée:
//    TTIterateur debutI      Itérateur sur les sommets à ajouter
//    TTIterateur finI        
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename TTIterateur>
ERMsg GOPolygone::ajoutePolyligne(TTIterateur debutI, TTIterateur finI)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   m_polylignes.push_back(GOPolyligneFermee());    // Ajoute une PL vide
   GOPolyligneFermee& pL = m_polylignes.back();    // Récupère la référence
   msg = pL.assigne(debutI, finI);                 // Remplis la PL

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute un liste de sommets à une polyligne du polygone
//
// Description:
//    La méthode <code>ajouteSommets(...)</code> ajoute une série de sommets
//    à une polyligne du polygone.
//
// Entrée:
//    const Entier& indPL : indice de la polyligne à laquelle ajouter les
//                          sommets. Il doit exister au moins une polyligne.
//    const Entier& indPT : indice qu'aura le premier sommet après son
//                          ajout. Doit être entre 1 et nbrSommet(de la PL)
//                          car sinon il est assuré que la PL n'est plus 
//                          simple et fermée.
//    TTIterateur debutI, finI : itérateurs sur le début et la fin 
//                         d'un vecteur de sommets (GOPoints) à ajouter.
//
// Sortie:
//
//TODO
// Notes: - Il faudra revenir voir comment on gère la cohérence du polygone
//        - Il est possible de trouver un polygone P et un ensemble de pt 
//          à ajouter E t.q les ajouts indiduels des pts de E soient impossibles
//          (mènent à un P incohérents) alors que l'ajout de tous les pts de E
//          donne une structure valide. C'est pourquoi l'opération d'ajout doit
//          permettre de prendre plus d'un pt à la fois en entrée.
//************************************************************************
template <typename TTIterateur>
ERMsg GOPolygone::ajouteSommets(EntierN indPL,
                                EntierN indPT,
                                TTIterateur debutI, TTIterateur finI)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());  
   TCContainerPolylignes::iterator pre_polyI = m_polylignes.begin();
   std::advance(pre_polyI, indPL);
   PRECONDITION(indPT < pre_polyI->reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   TCContainerPolylignes::iterator polyI = m_polylignes.begin();
   std::advance(polyI, indPL);
   
   // ---  Ajoute les sommets un à un à la polyligne
   Entier i = indPT;
   for (TTIterateur cI = debutI; msg && cI != finI; ++cI, ++i)
   {
      msg = polyI->ajouteSommet(*cI, i);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Assigne à un polygone vide une liste de polylignes.
//
// Description:
//    La méthode <code>assigne(...)</code> ajoute une série de polylignes à
//    un polygone vide. Les polylignes sont chacune définies à l'aide 
//    d'une paire d'itérateurs (début et fin) sur leurs sommets.
//    Le polygone est préalablement vidé.
//
// Entrée:
//    TTIterateur debutI      // Iterateurs sur des std::pair<TTIter, TTIter>
//    TTIterateur finI        // pour chaque le polyligne du polygone.
//
// Sortie:
//
// Notes:
//        
//************************************************************************
template <typename TTIterateur>
ERMsg GOPolygone::assigne(TTIterateur debutI, TTIterateur finI)
{
#ifdef MODE_DEBUG
   PRECONDITION(std::distance(debutI, finI) > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   m_polylignes.clear();
   for (TTIterateur pI = debutI; msg && pI != finI; ++pI)
   {
      msg = ajoutePolyligne((*pI).first, (*pI).second);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif   // GOPOLYGONE_HPP_DEJA_INCLU
