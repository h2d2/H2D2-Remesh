//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOMultiPolygone.hpp
// Classe : GOMultiPolygone
//************************************************************************
// 13-02-2004  Maude Giasson          Version initiale
//************************************************************************
#ifndef GOMULTIPOLYGONE_HPP_DEJA_INCLU
#define GOMULTIPOLYGONE_HPP_DEJA_INCLU

#include "GOPolygone.h"
#include "GOCoord3.h"

//************************************************************************
// Sommaire:  Ajoute un polygone au multiPolygone
//    
// Description:
//    La méthode <code>ajoutePolygone(...)</code> ajoute le polylgone passé
//    en paramètre au GOMultiPolygone et redonne un pointeur sur le polygone
//    ajouté.
//    Le polygone est passé en par l'intermédiaire d'itérateurs sur des
//    paires d'itérateur pour les polylignes du polygone.
//    
//
// Entrée:
//    TTIterateur debutI         Polylgone à ajouter sous forme d'itérateurs
//    TTIterateur finI           (début fin) sur ses polylignes
//
// Sortie: 
//
// Notes:
//************************************************************************
template <typename TTIterateur>
ERMsg GOMultiPolygone::ajoutePolygone(TTIterateur debutI, TTIterateur finI)  
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   GOPolygoneP polygoneP = new GOPolygone();
   msg = polygoneP->assigne(debutI, finI);
   m_multiPolygone.push_back(polygoneP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Ajoute une polyligne à un polygone du GOMultiPolygone
//
// Description:
//    La méthode publique <code>ajoutePLdePG(...)</code> ajoute une polyligne 
//    à un polygone du GOMultiPolygone.
//  
// Entrée:
//    EntierN indPG indice du polygone auquel ajouter une polyligne
//    TTIterateur debutI         Polylgone à ajouter sous forme d'itérateurs
//    TTIterateur finI           (début fin) sur ses sommets
//                                                         de paire d'itérateurs
//
// Sortie: 
//
// Notes:- Venir mettre à jour pour qu'elle puisse retourner un ERMsg autre que OK.
//       - La polyligne est toujours ajoutée à la fin du container qu'est le 
//         polygone
//************************************************************************
template <typename TTIterateur>
ERMsg GOMultiPolygone::ajoutePLdePG(EntierN indPG,  
                                    TTIterateur debutI, TTIterateur finI)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   msg = m_multiPolygone[indPG]->ajoutePolyligne(debutI, finI);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute un liste de sommets à un polygone du multiPolygone
//
// Description:
//    La méthode <code>ajoutePTdePLdePG(...)</code> ajoute une série
//    de sommets à une des polyligne d'un polygone
//
// Entrée:
//    EntierN indPG:       Indice du polygone du multi-polygone
//    EntierN indPL:       Indice de la polyligne du polygone
//    EntierN indPT:       Indice qu'aura le premier sommet après son
//                         ajout. Doit être entre 1 et nbrSommet(de la PL)
//                         car sinon il est assuré que la PL n'est plus une
//                         simple et fermée.
//    TTIterateur debutI   Points à ajouter sous forme d'itérateurs
//    TTIterateur finI     (début fin) sur ses sommets
//
// Sortie:
//
// Notes:
//    - Il est possible de trouver un polygone P et un ensemble de pt 
//      à ajouter E t.q les ajouts indiduels des pts de E soient impossibles
//      (mènent à un P incohérents) alors que l'ajout de tous les pts de E
//      donne une structure valide. C'est pourquoi l'opération d'ajout doit
//      permettre de prendre plus d'un pt à la fois en entrée.
//************************************************************************
template <typename TTIterateur>
ERMsg GOMultiPolygone::ajoutePTdePLdePG(EntierN indPG, 
                                        EntierN indPL, 
                                        EntierN indPT,
                                        TTIterateur debutI, TTIterateur finI)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   msg = m_multiPolygone[indPG]->ajouteSommets(indPL, indPT, debutI, finI);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif   // GOMULTIPOLYGONE_HPP_DEJA_INCLU
