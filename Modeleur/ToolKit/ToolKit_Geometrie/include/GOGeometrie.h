//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOGeometrie.h
//
// Classe:  GOGeometrie
//
// Description: La classe définit un ensemble d'objets géométriques 
//    (points, polygones et polylignes). Elle fait appel aux méthodes 
//   de la bibliothèque GEOS, classe Geometry.  
//   La classe ne correspond PAS à Geometry de OpenGIS/GEOS, cette 
//    dernière étant une classe abstraite parente de toutes les géométries.
//   Elle correspond de plus près à GeometryCollection, bien que cette 
//   dernière soit une généralisation des ensemble d'objets géométriques
//   alors que la présente classe est une construction de trois ensembles
//   d'objets géométriques.
//   
// Attributs:
//    GOMultiPolygone   m_multiPolygone;   Ensemble de GOPolygones.
//    GOMultiPolyligne  m_multiPolyligne;   Ensemble de GOPolylignes.
//    GOMultiPoint      m_multiPoint;      Ensemble de GOPoints.
//
// Notes: 
//   Pour l'instant, cette classe ne fait que regrouper les diverses
//   géométrie. Il faudra voir si l'on souhaite encapsuler un peu plus
//   l'information en la munissant de diverses fonctionnalités.
//
//   SC: L'utilisation de GEOS::Geometry plutôt que GEOS:GeometryCollection
//   est à vérifier.
//    SCDOC vérifier la définition ci-dessus.
//************************************************************************
#ifndef GOGEOMETRIE_H_DEJA_INCLU
#define GOGEOMETRIE_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOGenerateurID.h"

#include "GOMultiPolygone.hf"
#include "GOMultiPolyligne.hf"
#include "GOMultiPoint.hf"
#include "GOGeometrie.hf"

#include <iostream>
#include <memory>

DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::istream& operator>> (std::istream&, GOGeometrie&);
DLL_IMPEXP(TOOLKIT_GEOMETRIE) std::ostream& operator<< (std::ostream&, const GOGeometrie&);
          
class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOGeometrie
{
public:
                            GOGeometrie      ();
                            GOGeometrie      (const GOGeometrie&);
                           ~GOGeometrie      ();
       GOGeometrie&           operator=      (const GOGeometrie&);

         GOMultiPolygone&   reqMultiPolygone ();
   const GOMultiPolygone&   reqMultiPolygone () const;
         GOMultiPolyligne&  reqMultiPolyligne();
   const GOMultiPolyligne&  reqMultiPolyligne() const;
         GOMultiPoint&      reqMultiPoint    ();
   const GOMultiPoint&      reqMultiPoint    () const;

protected:
         void               invariant        (ConstCarP) const;
   
private:
   std::unique_ptr<GOMultiPolygone>   m_multiPolygoneP;
   std::unique_ptr<GOMultiPolyligne>  m_multiPolyligneP;
   std::unique_ptr<GOMultiPoint>      m_multiPointP;
   GOGenerateurID                   m_genId;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOGeometrie::invariant(ConstCarP conditionP) const
{
   INVARIANT(m_multiPolygoneP.get() != NUL, conditionP);
   INVARIANT(m_multiPolyligneP.get()!= NUL, conditionP);
   INVARIANT(m_multiPointP.get()    != NUL, conditionP);
}      
#else
inline void GOGeometrie::invariant(ConstCarP) const
{
}
#endif

#endif  // GOGEOMETRIE_H_DEJA_INCLU
