//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier:     GOEpsilon.h
//
// Classe:      GOEpsilon
//
// Sommaire:    Classe utilitaire qui permet de comparaison entre double.
//
// Description: La classe GOEpsilon permet les comparaison entre double.
//              On veut remplacer les comparaisons entre réels en appelant
//              des fonctions de comparaison qui sont un peu plus restrictive
//              que la comparaison standard. On applique un epsilon qui détermine
//              la précision à utiliser lors de la comparaison de bas niveau.
//              Cet epsilon s'applique strictement sur la comparaison d'égalité
//              et d'infériorité.  Les autres operateurs sont basés sur ces
//              deux comparaisons.
//              <p>
//              L'utilisation de cette classe ne se fera pas accidentellement
//              car on exige une conversion explicite d'un des deux arguments
//              lors de la comparaison.  On utilise le mot clé explicit devant
//              le constructeur acceptant un double.
//              <p>
//              Exemple d'utilisation <p>
//              <pre>
//              DReel d1 = 1.33;
//              if (GOEpsilon(d1) == 1.33)...
//              if (d1 == GOEpsilon(d2 + d2 + 0.44)) ...
//              </pre>
//              <p>
//              L'epsilon utilisé est par défaut la valeur de la constante
//              DOUBLE_EPSILON qui est actuellement à 1.0e-15.  Par contre,
//              si l'on désire utiliser un epsilon plus restrictif, on peut
//              le faire en passant comme second argument l'epsilon désiré.
//              Il existe aussi deux constantes dans l'interface publique
//              qui permettent de spécifier explicitement l'epsilon sur un
//              double (DOUBLE_EPSILON) et sur un float (FLOAT_EPSILON).
//              On tiendra compte que de l'argument de gauche.
//              Exemple d'utilisation <p>
//              <pre>
//              DReel d1 = 1.33;
//              if (GOEpsilon(d1, 1e-8) == 1.33)...
//              </pre>
//
// Attributs:   double valeur  : la valeur réelle
//              double epsilon : l'epsilon à utiliser lors de la comparaison.
//
// Notes:
//
//************************************************************************
// 07-11-1997  Yves Roy           Version initiale
// 07-11-1997  Yves Roy           Fusion de syftnnum et sydreel...
// 11-11-1997  Yves Roy           Ajout d'un epsilon pour float.
// 28-10-1998  Yves Secretan      Remplace #include "ererreur"
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#ifndef GOEPSILON_H_DEJA_INCLU
#define GOEPSILON_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"

#include "GOEpsilon.hf"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOEpsilon
{
public:
   static const double DOUBLE_EPSILON;
   static const double FLOAT_EPSILON;

   explicit    GOEpsilon       (const double&, const double& = DOUBLE_EPSILON);

   bool        operator ==     (const GOEpsilon&) const;
   bool        operator ==     (const double&) const;
   bool        operator !=     (const GOEpsilon&) const;
   bool        operator !=     (const double&) const;
   bool        operator <      (const GOEpsilon&) const;
   bool        operator <      (const double&) const;
   bool        operator <=     (const GOEpsilon&) const;
   bool        operator <=     (const double&) const;
   bool        operator >      (const GOEpsilon&) const;
   bool        operator >      (const double&) const;
   bool        operator >=     (const GOEpsilon&) const;
   bool        operator >=     (const double&) const;

   friend bool operator ==     (const double&, const GOEpsilon&);
   friend bool operator !=     (const double&, const GOEpsilon&);
   friend bool operator <      (const double&, const GOEpsilon&);
   friend bool operator >      (const double&, const GOEpsilon&);
   friend bool operator <=     (const double&, const GOEpsilon&);
   friend bool operator >=     (const double&, const GOEpsilon&);

protected:
   void        invariant       (ConstCarP) const;

private:
   double valeur;
   double epsilon;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifndef MODE_DEBUG
inline void GOEpsilon::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GOEpsilon::invariant(ConstCarP) const
{
}
#endif

#include "GOEpsilon.hpp"

#endif   // GOEPSILON_H_DEJA_INCLU
