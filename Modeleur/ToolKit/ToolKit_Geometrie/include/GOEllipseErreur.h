//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2004
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOEllipseErreur.h
//
// Classe: GOEllipseErreur
//
// Sommaire:  Classe de métrique d'ellipse
//
// Description:
//    La classe <code>GOEllipseErreur</code> implante la notion de métrique
//    utilisée dans la génération de maillage. Cette métrique se laisse
//    représenter sous la forme d'une ellipse.
//    <p>
//    L'ellipse représente également les valeurs propres/vecteurs propres
//    d'un système. Le grand axe/petit axe correspond à la plus grande/
//    plus petite valeur propre. 
//    <p>
//    Le grand axe et le petit axe correspondent aux rayons de l'ellipse.
//    L'angle est spécifié en radians.
//
// Attributs:
//   DReel grandAxe     // Paramètres de l'ellipse
//   DReel petitAxe
//   DReel angle
//
// Notes:
//
//************************************************************************
// 14-10-2004  Sébastien Labbé   Version originale
//************************************************************************
#ifndef GOELLIPSEERREUR_H_DEJA_INCLU
#define GOELLIPSEERREUR_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"

#ifdef MODE_PERSISTANT
   DECLARE_CLASS(FIFichier);
#endif // MODE_PERSISTANT

#include "GOEllipseErreur.hf"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOEllipseErreur
{
public:
         GOEllipseErreur(DReel = 1.0, DReel = 1.0, DReel = 0.0);
         ~GOEllipseErreur();

   void  asgGrandAxe    (const DReel&);
   void  asgPetitAxe    (const DReel&);
   void  asgInclinaison (const DReel&);
   void  asgComposantes (const DReel&, const DReel&, const DReel&);

   void  intersecte     (const GOEllipseErreur&);
   void  limite         (const DReel&, const DReel&);
   void  limiteBas      (const DReel&);
   void  limiteHaut     (const DReel&);
   void  normalise      ();
   void  scale          (const DReel&);

   DReel reqGrandAxe    () const;
   DReel reqPetitAxe    () const;
   DReel reqInclinaison () const;

   DReel reqAnisotropie () const;
   void  reqComposantes (DReel&, DReel&, DReel&) const;
   DReel reqEigenValMin () const;
   DReel reqEigenValMax () const;
   void  reqEigenVec    (DReel&, DReel&) const;
   DReel reqNorme       () const;

         GOEllipseErreur  operator +  (const GOEllipseErreur&) const;
   const GOEllipseErreur& operator += (const GOEllipseErreur&);
         GOEllipseErreur  operator -  (const GOEllipseErreur&) const;
   const GOEllipseErreur& operator -= (const GOEllipseErreur&);

         GOEllipseErreur  operator /  (const DReel&) const;
   const GOEllipseErreur& operator /= (const DReel&);
         GOEllipseErreur  operator *  (const DReel&) const;
   const GOEllipseErreur& operator *= (const DReel&);

   friend DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOEllipseErreur operator *  (const DReel&, const GOEllipseErreur&);

   bool                   operator <  (const GOEllipseErreur&) const; 
   bool                   operator <= (const GOEllipseErreur&) const; 
   bool                   operator >  (const GOEllipseErreur&) const; 
   bool                   operator >= (const GOEllipseErreur&) const; 
   bool                   operator == (const GOEllipseErreur&) const; 
   bool                   operator != (const GOEllipseErreur&) const;
    
protected:
   void invariant (ConstCarP) const;

private:
   DReel grandAxe;
   DReel petitAxe;
   DReel angle;
   
#ifdef MODE_PERSISTANT
public:

   friend DLL_IMPEXP(TOOLKIT_GEOMETRIE) FIFichier&      operator <<   (FIFichier&, const GOEllipseErreur&);
   friend DLL_IMPEXP(TOOLKIT_GEOMETRIE) FIFichier&      operator >>   (FIFichier&, GOEllipseErreur&);

   void                          exporte      (FIFichier&) const;
   void                          importe      (FIFichier&);
#endif   // MODE_PERSISTANT

};

GOEllipseErreur intersection(const GOEllipseErreur&, const GOEllipseErreur&);

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOEllipseErreur::invariant(ConstCarP /*conditionP*/) const
{
}
#else
inline void GOEllipseErreur::invariant(ConstCarP) const
{
}
#endif  //MODE_DEBUG

#include "GOEllipseErreur.hpp"

#endif // ifndef GOELLIPSEERREUR_H_DEJA_INCLU
