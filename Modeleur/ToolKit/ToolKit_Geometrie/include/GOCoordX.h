//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier:  GOCoordX.h
//
// Classe:   GOCoordX
//
// Sommaire: Coordonnées cartésiennes d'un point 1D.
//
// Description:
//    La classe <pre>GOCoordX</pre> représente les coordonnées d'un point.
//    <p>
//    La classe est paramétrisée par le type qui représente une coordonnée
//    (réel, entier ...). Les exigences de la classe par rapport à ce paramètre
//    sont:
//       un constructeur-copie
//       l'opérateur d'assignation =
//       les opérateurs mathématiques +, +=, -, -=, *, *=, /, /=,
//       les opérateur logiques  == et <
//       les opérateur d'insertion-extraction << et >>
//
// Attributs:
//    TTCoord  x     Position du point
//
// Notes:
//    Ajouter méthodes d'interface Vecteur (norme, prodSalaire, prodVect)
//    si c'est utile pour un vecteur 1D ?
//    Remplacé TCCoord par TCTypeX
//    afin d'éviter la confusion avec TCCoord qui, dans d'autres classes, 
//    réfère à une classe de coordonnées: ici, cela réfère ua type d'une
//    des coordonnées.
//************************************************************************
#ifndef GOCOOX_H_DEJA_INCLU
#define GOCOOX_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include <iosfwd>

// Les fonctions friend doivent être déclarés avant, à
// l'extérieur du corps de la classe.
#include "GOCoordX.hf"
#include "GOCoordXY.hf"
#include "GOCoordXYZ.hf"

template <typename TTCoord> 
class GOCoordX
{
public:
   typedef GOCoordX<TTCoord>  TCSelf;
   typedef TTCoord            TCCoord;

   typedef TTCoord            TCTypeX;

                  GOCoordX    ();
                  GOCoordX    (const TCCoord&);
                  GOCoordX    (const TCSelf&);
   explicit       GOCoordX    (const GOCoordXY<TCCoord>&);
   explicit       GOCoordX    (const GOCoordXYZ<TCCoord>&);
                 ~GOCoordX    ();

                  operator GOCoordXYZ<TCCoord>() const;
   TCSelf&        operator=   (const TCSelf&);

         TCCoord& x           ();
   const TCCoord& x           () const;
   const TCCoord  y           () const;
   const TCCoord  z           () const;

         TCCoord& operator[]  (Entier);
   const TCCoord& operator[]  (Entier) const;

   TCSelf         operator+   (const TCSelf&) const;
   TCSelf&        operator+=  (const TCSelf&);
   TCSelf         operator-   (const TCSelf&) const;
   TCSelf&        operator-=  (const TCSelf&);
   TCSelf         operator*   (const TCCoord&) const;
   TCSelf&        operator*=  (const TCCoord&);
   TCSelf         operator/   (const TCCoord&) const;
   TCSelf&        operator/=  (const TCCoord&);

   TCSelf         operator-   () const;

   Booleen        operator==  (const TCSelf&) const;
   Booleen        operator!=  (const TCSelf&) const;
   Booleen        operator<   (const TCSelf&) const;
   Booleen        operator>   (const TCSelf&) const;

   Booleen        estDedans   (const TCSelf&, const TCSelf&) const;

   void           minGlobal   (TCSelf&) const;
   void           maxGlobal   (TCSelf&) const;

   void           normalise   ();
   TCCoord        norme       () const;
   TCCoord        norme2      () const;

   static EntierN reqNbrDim   () ;

   static const TCSelf  GRAND;
   static const TCSelf  PETIT;

#ifndef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   friend TTCoord             dist       <>(const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend TTCoord             dist2      <>(const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend TTCoord             prodScal   <>(const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend GOCoordXYZ<TTCoord> prodVect   <>(const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend std::istream&       operator>> <>(std::istream&, GOCoordX<TTCoord>&);
   friend std::ostream&       operator<< <>(std::ostream&, const GOCoordX<TTCoord>&);
#else
   friend TTCoord             dist         (const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend TTCoord             dist2        (const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend TTCoord             prodScal     (const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend GOCoordXYZ<TTCoord> prodVect     (const GOCoordX<TTCoord>&, const GOCoordX<TTCoord>&);
   friend std::istream&       operator>>   (std::istream&, GOCoordX<TTCoord>&);
   friend std::ostream&       operator<<   (std::ostream&, const GOCoordX<TTCoord>&);
#endif   

protected:
   void           invariant     (ConstCarP) const;

private:
   TCCoord leX;

#ifdef MODE_PERSISTANT
public:
#ifndef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   friend FIFichier&             operator>> <>(FIFichier&, GOCoordX<TTCoord>&);
   friend FIFichier&             operator<< <>(FIFichier&, const GOCoordX<TTCoord>&);
#else
   friend FIFichier&             operator>>   (FIFichier&, GOCoordX<TTCoord>&);
   friend FIFichier&             operator<<   (FIFichier&, const GOCoordX<TTCoord>&);
#endif   //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
#endif   // MODE_PERSISTANT

#ifdef MODE_IMPORT_EXPORT
public:
   void                          exporte      (FIFichier&) const;
   void                          importe      (FIFichier&);
#endif   // MODE_IMPORT_EXPORT
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants 
//    de la classe. Cette méthode doit obligatoirement appeler les invariants
//    de ses parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
template <typename TTCoord>
inline void
GOCoordX<TTCoord>::invariant (ConstCarP /*conditionP*/) const
{
//   INVARIANT(VRAI, conditionP);
}
#else
template <typename TTCoord>
inline void
GOCoordX<TTCoord>::invariant (ConstCarP) const
{
}
#endif

#include "GOCoordX.hpp"

#endif // GOCOOX_H_DEJA_INCLU
