//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id: "
//
// Classe: GOCoordonneesX
//
// Sommaire:
//
// Description:
//
// Invariants:
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef GOCOORD1_H_DEJA_INCLU
#define GOCOORD1_H_DEJA_INCLU

#include "sytypes.h"
#include "GOCoordX.h"

DECLARE_TYPE(GOCoordX<DReel>, GOCoordonneesX);

#endif // GOCOORD1_H_DEJA_INCLU

