//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPolyligneFermee.h
//
// Classe:  GOPolyligneFermee
//
// Description: La classe définit un objet géométrique de type polyligne
//   fermée. Spécialisation d'une GOPolyligne.
//   Elle fait appel aux méthodes de la bibliothèque GEOS, classe LinearRing
//    au besoin (voir par ex. méthodes ...).  
//   Contrairement à LinearRing de OpenGIS, les lignes pourraient se croiser.
//   
// Attributs:
//    TCContainerSommets m_sommets;       Liste des sommets (GOPoint)
//    GOGenerateurID&    m_genID;         Generateur d'ID pour les sommets
//
// Notes: 
//    SCDOC vérifier la définition ci-dessus.
//************************************************************************
#ifndef GOPOLYLIGNEFERMEE_H_DEJA_INCLU
#define GOPOLYLIGNEFERMEE_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.h"
#include "GOPolyligne.h"

#include "GOGenerateurID.hf"
#include "GOGeometrie.hf"
#include "GOPolyligneFermee.hf"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOPolyligneFermee : public GOPolyligne
{
public:         
   typedef GOPoint           TCSommet;
   typedef GOPoint::TCCoord  TCCoord;

                      GOPolyligneFermee ();
                      GOPolyligneFermee (const GOPolyligneFermee&);
   explicit           GOPolyligneFermee (const GOPolyligne&);
                     ~GOPolyligneFermee ();
   GOPolyligneFermee& operator =        (const GOPolyligneFermee&);

   void                    asgGenerateurID   (GOGenerateurIDP);
         GOGenerateurIDP   reqGenerateurID();

   template <typename TTIterateur>
   ERMsg              assigne           (TTIterateur, TTIterateur);

   // ---  Edite
   ERMsg              ajouteSommet      (const TCCoord&);
   ERMsg              ajouteSommet      (const TCCoord&, EntierN);
   ERMsg              ajouteSommet      (const TCSommet&);
   ERMsg              ajouteSommet      (const TCSommet&, EntierN);
   ERMsg              deplace           (const TCCoord&);
   ERMsg              deplace           (const TCCoord&, EntierN);
   ERMsg              modifie           (const TCCoord&, EntierN);
   ERMsg              supprime          (EntierN);
              
   // ---  Algo
   ERMsg              contient          (Booleen&, const GOPolyligne&) const;
   ERMsg              estAntiHoraire    (Booleen&) const;
   ERMsg              soisAntiHoraire   ();
   ERMsg              intersection      (GOGeometrie&, const GOPolyligneFermee&) const;
      
protected:
   void              invariant      (ConstCarP) const;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//    L'invariant pourrait être plus sévère avec m_sommets.size() > 2, mais
//    ça entre en conflit avec la construition via ajouteSommet.
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOPolyligneFermee::invariant(ConstCarP conditionP) const
{
   GOPolyligne::invariant(conditionP);
   INVARIANT( m_sommets.size() == 0 || 
             (m_sommets.size() > 1 && m_sommets.front() == m_sommets.back()),
             conditionP);
}      
#else
inline void GOPolyligneFermee::invariant(ConstCarP) const
{
}      
#endif

#include "GOPolyligneFermee.hpp"

#endif  // GOPOLYLIGNEFERMEE_H_DEJA_INCLU
