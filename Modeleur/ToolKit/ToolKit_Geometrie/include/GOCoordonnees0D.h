//************************************************************************
// --- Copyright (c) 1992-2005
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier:  $Id$
//
// Classe:   GOCoordonnees0D
//
// Sommaire: Coordonnées cartésiennes d'un point 0D.
//
// Description:
//    La classe <pre>GOCoordonnees0D</pre> représente les coordonnées d'un point.
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef GOCOORDONNEES0D_H_DEJA_INCLU
#define GOCOORDONNEES0D_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"

#include <iosfwd>

#include "GOCoordonnees0D.hf"
#include "GOCoord1.hf"
#include "GOCoord2.hf"
#include "GOCoord3.hf"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOCoordonnees0D
{
public:
   typedef GOCoordonnees0D TCSelf;
   typedef DReel           TCCoord;

   typedef TCCoord         TCTypeX;

                  GOCoordonnees0D    ();
                  GOCoordonnees0D    (const TCSelf&);
   explicit       GOCoordonnees0D    (const GOCoordonneesX&);
   explicit       GOCoordonnees0D    (const GOCoordonneesXY&);
   explicit       GOCoordonnees0D    (const GOCoordonneesXYZ&);
                 ~GOCoordonnees0D    ();

                  operator GOCoordonneesXYZ () const;
   TCSelf&        operator=   (const TCSelf&);

         TCCoord  x           () const;
         TCCoord  y           () const;
         TCCoord  z           () const;

         TCCoord  operator[]  (Entier);
         TCCoord  operator[]  (Entier) const;

   TCSelf         operator+   (const TCSelf&) const;
   TCSelf&        operator+=  (const TCSelf&);
   TCSelf         operator-   (const TCSelf&) const;
   TCSelf&        operator-=  (const TCSelf&);
   TCSelf         operator*   (const TCCoord&) const;
   TCSelf&        operator*=  (const TCCoord&);
   TCSelf         operator/   (const TCCoord&) const;
   TCSelf&        operator/=  (const TCCoord&);

   TCSelf         operator-   () const;

   Booleen        operator==  (const TCSelf&) const;
   Booleen        operator!=  (const TCSelf&) const;
   Booleen        operator<   (const TCSelf&) const;
   Booleen        operator>   (const TCSelf&) const;

   Booleen        estDedans   (const TCSelf&, const TCSelf&) const;

   void           minGlobal   (TCSelf&) const;
   void           maxGlobal   (TCSelf&) const;

   void           normalise   ();
   TCCoord        norme       () const;
   TCCoord        norme2      () const;

   static EntierN reqNbrDim   () ;

   static const TCSelf  GRAND;
   static const TCSelf  PETIT;

   friend TCCoord          dist       (const TCSelf&, const TCSelf&);
   friend TCCoord          dist2      (const TCSelf&, const TCSelf&);
   friend TCCoord          prodScal   (const TCSelf&, const TCSelf&);
   friend GOCoordonneesXYZ prodVect   (const TCSelf&, const TCSelf&);
   friend std::istream&    operator>> (std::istream&, TCSelf&);
   friend std::ostream&    operator<< (std::ostream&, const TCSelf&);

protected:
   void           invariant     (ConstCarP) const;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants 
//    de la classe. Cette méthode doit obligatoirement appeler les invariants
//    de ses parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void
GOCoordonnees0D::invariant (ConstCarP /*conditionP*/) const
{
//   INVARIANT(VRAI, conditionP);
}
#else
inline void
GOCoordonnees0D::invariant (ConstCarP) const
{
}
#endif

#endif // GOCOORDONNEES0D_H_DEJA_INCLU
