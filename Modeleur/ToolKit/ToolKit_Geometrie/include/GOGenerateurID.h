//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOGenerateurID.h
//
// Classe:  GOGenerateurID
//
// Description: La classe définit un générateur d'identificateurs.
//    Elle est employée pour générer des identificateurs distincts pour les
//   sommets d'objets géométriques, facilitant certains types de calculs. 
//   
// Attributs:
//   EntierN  m_idSuivante;      Identificateur suivant.
//
// Notes: 
//    SCDOC vérifier la définition ci-dessus.
//************************************************************************
#ifndef GOGENERATEURID_H_DEJA_INCLU
#define GOGENERATEURID_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOPoint.hf"
#include "GOGenerateurID.hf"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOGenerateurID
{
public:         
   static GOGenerateurID generiqueGenID;
   static const EntierN  NON_INITIALISE;

        GOGenerateurID ();
       ~GOGenerateurID ();

   void asgID(GOPoint&);

   template <typename TTIterateur>
   void asgID(TTIterateur, TTIterateur);

   EntierN reqIDSuivant();
 
protected:
   void              invariant      (ConstCarP) const;

private:
   EntierN  m_idSuivante;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOGenerateurID::invariant(ConstCarP /*conditionP*/) const
{
}      
#else
inline void GOGenerateurID::invariant(ConstCarP) const
{
}      
#endif

#include "GOGenerateurID.hpp"

#endif  // GOGENERATEURID_H_DEJA_INCLU
