//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier:  GOEpsilon.hpp
// Classe:   GOEpsilon
//************************************************************************
// 04-11-1997  Yves Roy           Version initiale
// 07-11-1997  Yves Roy           Fusion de syftnnum et sydreel...
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#ifndef GOEPSILON_HPP_DEJA_INCLU
#define GOEPSILON_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:     Constructeur d'un GOEpsilon à l'aide d'un double
//
// Description:  Le constructeur prend un double en entrée pour créer un GOEpsilon.
//
// Entrée:       const double& dbl   : la valeur à assigner.
//               const double& epsi : l'epsilon à utiliser.
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline GOEpsilon::GOEpsilon(const double& dbl, const double& epsi)
: valeur(dbl), epsilon(epsi)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:     Opérateur ==
//
// Description:  L'opérateur == fait la comparaison entre un GOEpsilon et un double.
//
// Entrée:       const double& val : le double à comparer.
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator==(const double& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (*this == GOEpsilon(val, epsilon));
}

//************************************************************************
// Sommaire:     Opérateur !=
//
// Description:  L'opérateur != fait la comparaison en convertissant la
//               comparaison en !(*this == obj).
//
// Entrée:       const GOEpsilon& obj : un objet du même type
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator!=(const GOEpsilon& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
    return !(*this == obj);
}

//************************************************************************
// Sommaire:     Opérateur !=
//
// Description:  L'opérateur != fait la comparaison entre un GOEpsilon et un double.
//
// Entrée:       const double& val : le double à comparer.
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator!=(const double& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (*this != GOEpsilon(val, epsilon));
}

//************************************************************************
// Sommaire:     Opérateur <
//
// Description:  L'opérateur < fait la comparaison entre un GOEpsilon et un double.
//
// Entrée:       const double& val : le double à comparer.
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator<(const double& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (*this < GOEpsilon(val, epsilon));
}

//************************************************************************
// Sommaire:     Opérateur >
//
// Description:  L'opérateur > fait la comparaison en convertissant la
//               comparaison en (obj < *this).
//
// Entrée:       const GOEpsilon& obj : un objet du même type
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator>(const GOEpsilon& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
    return (obj < *this);
}

//************************************************************************
// Sommaire:     Opérateur >
//
// Description:  L'opérateur > fait la comparaison entre un GOEpsilon et un double.
//
// Entrée:       const double& val : le double à comparer.
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator>(const double& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (*this > GOEpsilon(val, epsilon));
}

//************************************************************************
// Sommaire:     Opérateur <=
//
// Description:  L'opérateur <= fait la comparaison en convertissant la
//               comparaison en !(obj < *this).
//
// Entrée:       const GOEpsilon& obj : un objet du même type
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator<=(const GOEpsilon& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
    return !(obj < *this);
}

//************************************************************************
// Sommaire:     Opérateur <=
//
// Description:  L'opérateur <= fait la comparaison entre un GOEpsilon et un double.
//
// Entrée:       const double& val : le double à comparer.
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator<=(const double& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (*this <= GOEpsilon(val, epsilon));
}

//************************************************************************
// Sommaire:     Opérateur >=
//
// Description:  L'opérateur >= fait la comparaison en convertissant la
//               comparaison en !(*this < obj).
//
// Entrée:       const GOEpsilon& obj : un objet du même type
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator>=(const GOEpsilon& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
    return !(*this < obj);
}

//************************************************************************
// Sommaire:     Opérateur >=
//
// Description:  L'opérateur >= fait la comparaison entre un GOEpsilon et un double.
//
// Entrée:       const double& val : le double à comparer.
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
inline bool GOEpsilon::operator>=(const double& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (*this >= GOEpsilon(val, epsilon));
}

//************************************************************************
// Sommaire:     Fonction friend à la classe GOEpsilon pour comparer ==
//
// Description:  La fonction friend operator== permet de comparer un double
//               avec un GOEpsilon
//
// Entrée:       const double& val : le double à comparer
//               const GOEpsilon& obj  : un objet GOEpsilon à comparer.
//
// Sortie:       Retourne true si succès et false autrement
//
// Notes:
//
//************************************************************************
inline bool operator == (const double& val, const GOEpsilon& obj)
{
   return (GOEpsilon(val, obj.epsilon) == obj);
}

//************************************************************************
// Sommaire:     Fonction friend à la classe GOEpsilon pour comparer !=
//
// Description:  La fonction friend operator!= permet de comparer un double
//               avec un GOEpsilon
//
// Entrée:       const double& val : le double à comparer
//               const GOEpsilon& obj  : un objet GOEpsilon à comparer.
//
// Sortie:       Retourne true si succès et false autrement
//
// Notes:
//
//************************************************************************
inline bool operator != (const double& val, const GOEpsilon& obj)
{
   return (GOEpsilon(val, obj.epsilon) != obj);
}

//************************************************************************
// Sommaire:     Fonction friend à la classe GOEpsilon pour comparer <
//
// Description:  La fonction friend operator< permet de comparer un double
//               avec un GOEpsilon
//
// Entrée:       const double& val : le double à comparer
//               const GOEpsilon& obj  : un objet GOEpsilon à comparer.
//
// Sortie:       Retourne true si succès et false autrement
//
// Notes:
//
//************************************************************************
inline bool operator < (const double& val, const GOEpsilon& obj)
{
   return (GOEpsilon(val, obj.epsilon) < obj);
}

//************************************************************************
// Sommaire:     Fonction friend à la classe GOEpsilon pour comparer >
//
// Description:  La fonction friend operator> permet de comparer un double
//               avec un GOEpsilon
//
// Entrée:       const double& val : le double à comparer
//               const GOEpsilon& obj  : un objet GOEpsilon à comparer.
//
// Sortie:       Retourne true si succès et false autrement
//
// Notes:
//
//************************************************************************
inline bool operator > (const double& val, const GOEpsilon& obj)
{
   return (GOEpsilon(val, obj.epsilon) > obj);
}

//************************************************************************
// Sommaire:     Fonction friend à la classe GOEpsilon pour comparer <=
//
// Description:  La fonction friend operator<= permet de comparer un double
//               avec un GOEpsilon
//
// Entrée:       const double& val : le double à comparer
//               const GOEpsilon& obj  : un objet GOEpsilon à comparer.
//
// Sortie:       Retourne true si succès et false autrement
//
// Notes:
//
//************************************************************************
inline bool operator <= (const double& val, const GOEpsilon& obj)
{
   return (GOEpsilon(val, obj.epsilon) <= obj);
}

//************************************************************************
// Sommaire:     Fonction friend à la classe GOEpsilon pour comparer >=
//
// Description:  La fonction friend operator>= permet de comparer un double
//               avec un GOEpsilon
//
// Entrée:       const double& val : le double à comparer
//               const GOEpsilon& obj  : un objet GOEpsilon à comparer.
//
// Sortie:       Retourne true si succès et false autrement
//
// Notes:
//
//************************************************************************
inline bool operator >= (const double& val, const GOEpsilon& obj)
{
   return (GOEpsilon(val, obj.epsilon) >= obj);
}

#endif   // GOEPSILON_H_DEJA_INCLU
