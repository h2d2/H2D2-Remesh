//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPoint.h
//
// Classe:  GOPoint
//
// Description:
//    La classe <code>GOPoint</code> représente un objet géométrique de
//    type point dans un système cartésien tri-dimensionnel.
//    Elle fait appel aux méthodes de la bibliothèque GEOS, classe Point
//      au besoin (voir par ex. méthodes distance et possedeMemeCoordonnees).  
//   
// Attributs:
//   TCCoord  m_coord;      Les coordonnées du point (un objet GOCoordonneesXYZ)
//   EntierN  m_id;         L'identificateur du point.
//
// Notes: 
//    SCDOC vérifier la définition ci-dessus.
//************************************************************************
#ifndef GOPOINT_H_DEJA_INCLU
#define GOPOINT_H_DEJA_INCLU

#ifndef TOOLKIT_GEOMETRIE
#  define TOOLKIT_GEOMETRIE 1
#endif

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "GOCoord3.h"
#include "GOGenerateurID.h"

#include "GOPoint.hf"

class DLL_IMPEXP(TOOLKIT_GEOMETRIE) GOPoint
{
public:         
   typedef GOCoordonneesXYZ TCCoord;

                     GOPoint        (EntierN = GOGenerateurID::NON_INITIALISE);
                     GOPoint        (const TCCoord&, EntierN = GOGenerateurID::NON_INITIALISE);
                     GOPoint        (const GOPoint&);
                    ~GOPoint        ();
   GOPoint&          operator=      (const GOPoint&);

   // ---  Edite
   void              asgID          (EntierN);
   void              asgPosition    (const TCCoord&);

   void              deplace        (const TCCoord&);
   EntierN           reqID          () const;
   const TCCoord&    reqPosition    () const;

   // ---  Algo
   DReel             distance       (const GOPoint&) const;
   Booleen           possedeMemeCoordonnees(const GOPoint&, const DReel& = GOPoint::tolerance) const;

   Booleen           operator ==    (const GOPoint&) const;
   Booleen           operator !=    (const GOPoint&) const;
    
   static DReel tolerance;

protected:
   void              invariant      (ConstCarP) const;

private:
   TCCoord  m_coord;
   EntierN  m_id;
};

//***********************************************************************
// Sommaire: Test les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant()</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Notes:
//
//************************************************************************
#ifdef MODE_DEBUG
inline void GOPoint::invariant(ConstCarP /*conditionP*/) const
{
}      
#else
inline void GOPoint::invariant(ConstCarP) const
{
}      
#endif

#endif  // GOPOINT_H_DEJA_INCLU
