//************************************************************************
// Fichier: $Id$
// Classe : GOCoordXYZ
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
// 16-10-1997  Yves Secretan      Version originale
// 30-10-1997  Yves Roy           Les opérateurs ==, != et < utilisent compareEgal
//                                et compareInferieur dans syftnnum.
// 05-11-1997  Yves Roy           L'utilisation de syftnnum est caché dans le type DReel
// 11-11-1997  Yves Roy           On utilise GOEpsilon pour faire les comparaisons de doubles...
// 22-04-1998  Yves Roy           Ajout d'une fonction d'assignation au minGlobal et au maxGlobal
// 03-05-1999  Yves Secretan      Supporte les iostream du standard et les nouveaux
//                                entêtes du standard
// 27-05-2003  Dominique Richard  Port multi-compilateur
// 21-11-2003  Olivier Kaczor     Ajout de la méthode reqNbrDim
// 16-02-2004  Maude Giasson      TT->TC à l'intérieur de la classe
// 18-02-2004  Yves Secretan      Port GCC
// 22-06-2004  Yves Secretan      Port MSVC 7.1 - Supprime deux friend
//************************************************************************
#ifndef GOCOOXYZ_HPP_DEJA_INCLU
#define GOCOOXYZ_HPP_DEJA_INCLU

#include "GOEpsilon.h"

#include <cmath>
#include <iostream>
#include <limits>

#ifdef MODE_PERSISTANT
   #include "fifichie.h"
#endif   // MODE_PERSISTANT

template <typename TTCoord>
const GOCoordXYZ<TTCoord> GOCoordXYZ<TTCoord>::GRAND = GOCoordXYZ(std::numeric_limits<TTCoord>::max(),
                                                                  std::numeric_limits<TTCoord>::max(),
                                                                  std::numeric_limits<TTCoord>::max());
template <typename TTCoord>
const GOCoordXYZ<TTCoord> GOCoordXYZ<TTCoord>::PETIT = GOCoordXYZ(std::numeric_limits<TTCoord>::min(),
                                                                  std::numeric_limits<TTCoord>::min(),
                                                                  std::numeric_limits<TTCoord>::min());

//************************************************************************
// Sommaire: Constructeur par défaut.
//
// Description:
//    Le constructeur public <code>GOCoordXYZ()</code> est le constructeur
//    par défaut de la classe. Il initialise les coordonnées à 0.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXYZ<TTCoord>::GOCoordXYZ ()
   : leX(0), leY(0), leZ(0)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur qui prend trois paramètres.
//
// Description:
//    Le constructeur public <code>GOCoordXYZ()</code>
//    construit un objet à partir de trois coordonnées.
//
// Entrée:
//    const TCCoord& unX      Valeur des coordonnées
//    const TCCoord& unY
//    const TCCoord& unZ
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXYZ<TTCoord>::GOCoordXYZ (const TCCoord& unX,
                                 const TCCoord& unY,
                                 const TCCoord& unZ)
   : leX(unX), leY(unY), leZ(unZ)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur public <code>GOCoordXYZ(const GOCoordXYZ&)</code> est le
//    constructeur copie de la classe. Il construit l'objet comme copie de celui
//    qui est passé en argument.
//
// Entrée:
//    const GOCoordXYZ& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXYZ<TTCoord>::GOCoordXYZ (const TCSelf & p)
   : leX(p.leX), leY(p.leY), leZ(p.leZ)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Destructeur de la classe
//
// Description:
//    Le destructeur public <code>~GOCoordXYZ()</code> ne fait rien de
//    spécifique.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline
GOCoordXYZ<TTCoord>::~GOCoordXYZ ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

}

//************************************************************************
// Sommaire:
//    Retourne la distance euclidienne entre deux points.
//
// Description:
//    La méthode <code>dist()</code> friend de la classe
//    <code>GOCoodrXYZ</code> calcule la distance euclidienne entre les deux
//    points passés en argument.
//
// Entrée:
//    const TCSelf& p1       Points
//    const TCSelf& p2
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
dist(const GOCoordXYZ<TTCoord>& p1, const GOCoordXYZ<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXYZ<TTCoord>::TCCoord TCCoord;

   TCCoord dx = p2.leX - p1.leX;
   TCCoord dy = p2.leY - p1.leY;
   TCCoord dz = p2.leZ - p1.leZ;
   double prod2 = static_cast<double>(dx*dx + dy*dy + dz*dz);

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return static_cast<TCCoord>(sqrt(prod2));
}

//************************************************************************
// Description:
//    Retourne le carré de la distance euclidienne entre deux points.
//
// Entrée:
//    La méthode <code>dist()</code> friend de la classe
//    <code>GOCoodrXYZ</code> calcule le carré de la distance euclidienne
//    entre les deux points passés en argument.
//
// Sortie:
//    const TCSelf& p1       Points
//    const TCSelf& p2
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
dist2(const GOCoordXYZ<TTCoord>& p1, const GOCoordXYZ<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXYZ<TTCoord>::TCCoord TCCoord;

   TCCoord dx = p2.leX - p1.leX;
   TCCoord dy = p2.leY - p1.leY;
   TCCoord dz = p2.leZ - p1.leZ;

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (dx*dx + dy*dy + dz*dz);
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un minimum global
//
// Description:
//    La méthode publique <code>minGlobal(...)</code> contribue
//    à la détermination d'un minimum global à une collection
//    d'objet de ce type.  On passe un minGlobal en argument et
//    si l'une de nos composantes est plus petite, on assigne
//    celle-ci au minimum global.
//
// Entrée:
//
// Sortie:
//    TCSelf& minGlobal : le minimum global.
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline void
GOCoordXYZ<TTCoord>::minGlobal(TCSelf& minGlobal) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (leX < minGlobal.x()) minGlobal.x() = leX;
   if (leY < minGlobal.y()) minGlobal.y() = leY;
   if (leZ < minGlobal.z()) minGlobal.z() = leZ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un maximum global
//
// Description:
//    La méthode publique <code>maxGlobal(...)</code> contribue
//    à la détermination d'un maximum global à une collection
//    d'objet de ce type.  On passe un maxGlobal en argument et
//    si l'une de nos composantes est plus grande, on assigne
//    celle-ci au maximum global.
//
// Entrée:
//
// Sortie:
//    TCSelf& maxGlobal : le maximum global.
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline void
GOCoordXYZ<TTCoord>::maxGlobal(TCSelf& maxGlobal) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (leX > maxGlobal.x()) maxGlobal.x() = leX;
   if (leY > maxGlobal.y()) maxGlobal.y() = leY;
   if (leZ > maxGlobal.z()) maxGlobal.z() = leZ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La méthode publique <code>normalise()</code> normalise le vecteur (point),
//    donc le rend tel que sa norme soit 1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
void
GOCoordXYZ<TTCoord>::normalise()
{
#ifdef MODE_DEBUG
   PRECONDITION(norme2() != 0.0);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   double nrm = static_cast<double>(leX*leX + leY*leY + leZ*leZ);
   nrm = 1.0 / INRS_LIBC_STD::sqrt(nrm);
   leX *= nrm;
   leY *= nrm;
   leZ *= nrm;

#ifdef MODE_DEBUG
   POSTCONDITION(INRS_LIBC_STD::fabs(norme2()-1.0) <= 1.0e-15);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La fonction <code>normalise()</code> normalise le point (vecteur) 
//    passé en argument, donc le rend tel que sa norme soit 1.
//
// Entrée:
//    const GOCoordXYZ<TTCoord>& p     Point à normaliser
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
GOCoordXYZ<TTCoord>
normalise(const GOCoordXYZ<TTCoord>& p)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   typename GOCoordXYZ<TTCoord>::TCSelf tmp = p;
   tmp.normalise();

#ifdef MODE_DEBUG
#endif   // MODE_DEBUG
   return tmp;
}

//************************************************************************
// Sommaire: Calcule et retourne la norme du vecteur.
//
// Description:
//    La méthode publique <code>norme()</code> calcule la norme euclidienne
//    du vecteur (point).
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCCoord
GOCoordXYZ<TTCoord>::norme() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   double nrm2 = static_cast<double>(leX*leX + leY*leY + leZ*leZ);

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return static_cast<TCCoord>(INRS_LIBC_STD::sqrt(nrm2));
}

//************************************************************************
// Sommaire: Calcule et retourne le carré de la norme du vecteur.
//
// Description:
//    La méthode publique <code>norme2()</code> calcule le carré de la norme
//    euclidienne du vecteur (point).
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCCoord
GOCoordXYZ<TTCoord>::norme2() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   const TCCoord nrm = leX*leX + leY*leY + leZ*leZ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return nrm;
}

//************************************************************************
// Sommaire:   Calcule et retourne le produit scalaire de deux vecteurs.
//
// Description:
//    La fonction <code>prodScal(...)</code> friend de la classe
//    <code>GOCoordXYZ<TTCoord></code> calcule le produit scalaire
//    entre les deux vecteurs (points) passée en argument. Le produit
//    scalaire correspond à la projection d'un vecteur sur l'autre.
//
// Entrée:
//    const GOCoordXYZ<TTCoord>& p1       Premier vecteur
//    const GOCoordXYZ<TTCoord>& p2       Deuxième vecteur
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
TTCoord
prodScal(const GOCoordXYZ<TTCoord>& p1, const GOCoordXYZ<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXYZ<TTCoord>::TCCoord TCCoord;
   TCCoord prod = p1.leX*p2.leX + p1.leY*p2.leY + p1.leZ*p2.leZ;

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return prod;
}

//************************************************************************
// Sommaire:   Calcule et retourne le produit vectoriel entre deux vecteurs.
//
// Description:
//    La fonction <code>prodVect()</code> friend de la classe
//    <code>GOCoordXYZ</code> calcule le produit vectoriel entre les deux
//    vecteurs passée en argument. Le résultat est un vecteur normal au plan
//    formé par les deux vecteurs de base.
//
// Entrée:
//    const GOCoordXYZ<TTCoord>& p1       Premier vecteur
//    const GOCoordXYZ<TTCoord>& p2       Deuxième vecteur
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
GOCoordXYZ<TTCoord>
prodVect(const GOCoordXYZ<TTCoord>& p1, const GOCoordXYZ<TTCoord>& p2)
{
#ifdef MODE_DEBUG
   p1.INVARIANTS("PRECONDITION");
   p2.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

  typedef typename GOCoordXYZ<TTCoord>::TCSelf TCSelf;
  TCSelf res(p1.leY*p2.leZ - p1.leZ*p2.leY,
             p1.leZ*p2.leX - p1.leX*p2.leZ,
             p1.leX*p2.leY - p1.leY*p2.leX);

#ifdef MODE_DEBUG
   p1.INVARIANTS("POSTCONDITION");
   p2.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}

//************************************************************************
// Sommaire: Nombre de dimension de la coordonnée
//
// Description:
//    La méthode publique <code>reqNbrDim()</code> retourne la dimension 
//    de la coordonnée.
//
// Entrée:
//
// Sortie:
//   EntierN : la dimension de la coordonnée
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline EntierN
GOCoordXYZ<TTCoord>::reqNbrDim() 
{
   return 3;
}

//************************************************************************
// Sommaire: Composante x de la coordonnée
//
// Description:
//    La méthode publique <code>x()</code> retourne la coordonnée x (première
//    composante) du point. La coordonnées est retournée par référence, ce qui
//    permet à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::x ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leX;
}

//************************************************************************
// Sommaire: Composante x de la coordonnée
//
// Description:
//    La méthode publique <code>x()</code> retourne la coordonnée x (première
//    composante) du point. La coordonnée est retournée par référence constante,
//    ce qui ne permet pas à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::x () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leX;
}

//************************************************************************
// Sommaire: Composante y de la coordonnée
//
// Description:
//    La méthode publique <code>y()</code> retourne la coordonnée y (deuxième
//    composante) du point. La coordonnées est retournée par référence, ce qui
//    permet à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::y ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leY;
}

//************************************************************************
// Sommaire: Composante y de la coordonnée
//
// Description:
//    La méthode publique <code>y()</code> retourne la coordonnée y (deuxième
//    composante) du point. La coordonnées est retournée par référence constante,
//    ce qui ne permet pas à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::y () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leY;
}

//************************************************************************
// Sommaire: Composante z de la coordonnée
//
// Description:
//    La méthode publique <code>z()</code> retourne la coordonnée z (troisième
//    composante) du point. La coordonnées est retournée par référence, ce qui
//    permet à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::z ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leZ;
}

//************************************************************************
// Sommaire: Composante z de la coordonnée
//
// Description:
//    La méthode publique <code>z()</code> retourne la coordonnée z (troisième
//    composante) du point. La coordonnées est retournée par référence constante,
//    ce qui ne permet pas à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::z () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return leZ;
}

//************************************************************************
// Sommaire:  Définition de l'opérateur [] pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator []()</code> retourne la coordonnée
//    dont l'indice est donné en argument. L'indice doit obligatoirement
//    être soit 0, soit 1 ou encore 2. La coordonnées est retournée par
//    référence, ce qui permet à l'utilisateur de la changer.
//
// Entrée:
//    Entier i       Indice de la coordonnée à retourner
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::operator[] (Entier i)
{
#ifdef MODE_DEBUG
   PRECONDITION(i == 0 || i == 1 || i == 2);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (i == 0) ? leX : (i == 1) ? leY : leZ;
}

//************************************************************************
// Sommaire:  Définition de l'opérateur [] pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator []()</code> retourne la coordonnée
//    dont l'indice est donné en argument. L'indice doit obligatoirement
//    être soit 0, soit 1 ou encore 2. La coordonnées est retournée par
//    référence constante, ce ne qui permet pas à l'utilisateur de la changer.
//
// Entrée:
//    Entier i       Indice de la coordonnée à retourner
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline const typename GOCoordXYZ<TTCoord>::TCCoord&
GOCoordXYZ<TTCoord>::operator[] (Entier i) const
{
#ifdef MODE_DEBUG
   PRECONDITION(i == 0 || i == 1 || i == 2);
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (i == 0) ? leX : (i == 1) ? leY : leZ;
}

//************************************************************************
// Sommaire:  Définition de l'opérateur + pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator+()</code> définit l’addition
//    entre deux <code>GOCoordXYZ</code>. Les composantes sont additionnées
//    membre à membre.
//
// Entrée:
//    const TCSelf& p     Point à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf
GOCoordXYZ<TTCoord>::operator+ (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   GOCoordXYZ res(*this);
   res += p;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}

//************************************************************************
// Sommaire:
//    Définition de l'opérateur += pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator+=</code> définit l'incrémentation
//    d'un <code>GOCoordXYZ</code>.
//
// Entrée:
//    const TCSelf& p     Point à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf&
GOCoordXYZ<TTCoord>::operator+= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX += p.leX;
   leY += p.leY;
   leZ += p.leZ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}

//************************************************************************
// Sommaire:
//    Définition de l'opérateur - pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator-()</code> définit la soustraction
//    entre deux <code>GOCoordXYZ</code>. Les composantes sont soustraites
//    membre à membre.
//
// Entrée:
//    const GOCoordXYZ<TTCoord>& p     Point à retrancher
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf
GOCoordXYZ<TTCoord>::operator- (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res -= p;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}

//************************************************************************
// Sommaire:
//    Définition de l'opérateur de négation pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator-()</code> définit la négation
//    d'un <code>GOCoordXYZ</code>. La négation est faite membre à membre.
//
// Entrée:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf
GOCoordXYZ<TTCoord>::operator- () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res;
   res.leX = -leX;
   res.leY = -leY;
   res.leZ = -leZ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}

//************************************************************************
// Sommaire:   Définition de l'opérateur -= pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator-=()</code> définit la décrémentation
//    d'un <code>GOCoordXYZ</code>.
//
// Entrée:
//    const TCSelf& p     Point à retrancher
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf&
GOCoordXYZ<TTCoord>::operator-= (const TCSelf& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX -= p.leX;
   leY -= p.leY;
   leZ -= p.leZ;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}

//************************************************************************
// Sommaire:   Définition de l'opérateur * pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator*()</code> définit l'opérateur *
//    pour des <code>GOCoordXYZ</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCSelf
GOCoordXYZ<TTCoord>::operator* (const TCCoord& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res *= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordXYZ::operator*

//************************************************************************
// Sommaire: Définition de l'opérateur *= pour des GOCoordXYZ
//
// Description:
//    Définition de l'opérateur *= pour des GOCoordXYZ
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf&
GOCoordXYZ<TTCoord>::operator*= (const TCCoord& val)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX *= val;
   leY *= val;
   leZ *= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordXYZ::operator*=

//************************************************************************
// Description:
//    L'opérateur <code>operator*()</code> définit la multiplication
//    d'un <code>GOCoordXYZ</code> par un Reel.
//
// Entrée:
//    const TTCoord& val               Valeur 
//    const GOCoordXYZ<TTCoord>& p     Point à multiplier
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline GOCoordXYZ<TTCoord>
operator* (const TTCoord& val, const GOCoordXYZ<TTCoord>& p)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return (p * val);
}

//************************************************************************
// Description:
//    Définition de l'opérateur / pour des GOCoordXYZ
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline typename GOCoordXYZ<TTCoord>::TCSelf
GOCoordXYZ<TTCoord>::operator/ (const TCCoord& val) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   TCSelf res(*this);
   res /= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return res;
}  // GOCoordXYZ::operator/

//************************************************************************
// Description:
//    Définition de l'opérateur /= pour des GOCoordXYZ
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf&
GOCoordXYZ<TTCoord>::operator/= (const TCCoord& val)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   leX /= val;
   leY /= val;
   leZ /= val;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return (*this);
}  // GOCoordXYZ::operator/=

//************************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
typename GOCoordXYZ<TTCoord>::TCSelf&
GOCoordXYZ<TTCoord>::operator= (const GOCoordXYZ& p)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (this != &p)
   {
      leX = p.leX;
      leY = p.leY;
      leZ = p.leZ;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return *this;
}  // GOCoordXYZ::GOCoordXYZ<TTCoord>

//************************************************************************
// Sommaire:   Définition de l'opérateur ==
//
// Description:
//    Définition de l'opérateur == pour des GOCoordXYZ
//    On utilise l'objet SOEpsilon pour faire la comparaison de bas niveau
//    entre deux doubles...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordXYZ<TTCoord>::operator== (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return GOEpsilon(leX) == p.leX &&
          GOEpsilon(leY) == p.leY &&
          GOEpsilon(leZ) == p.leZ;
}  // GOCoordXYZ::operator==

//************************************************************************
// Description:
//    Définition de l'opérateur != pour des GOCoordXYZ
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
inline Booleen
GOCoordXYZ<TTCoord>::operator!= (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return !(*this == p);
}  // GOCoordXYZ::operator!=

//************************************************************************
// Sommaire:   Retourne VRAI si le point est à l'intérieur
//
// Description:
//    La méthode publique <code>estDedans()</code> retourne VRAI si l'objet
//    est compris dans le volume délimité par les deux points min et max
//    passés en argument.
//
// Entrée:
//    const TCSelf& pMin     // Point Min
//    const TCSelf& pMax     // Point Max
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen
GOCoordXYZ<TTCoord>::estDedans (const TCSelf& pMin,
                                const TCSelf& pMax) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (!(leX < pMin.leX) && !(pMax.leX < leX)  &&
           !(leY < pMin.leY) && !(pMax.leY < leY)  &&
           !(leZ < pMin.leZ) && !(pMax.leZ < leZ));
}

//************************************************************************
// Sommaire:   Définition de l'opérateur < pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator<(...)</code> définit l'opérateur
//    d’ordonnancement < pour des GOCoordXYZ. On utilise l'objet GOEpsilon
//    pour faire les comparaisons de bas niveau entre des doubles.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen
GOCoordXYZ<TTCoord>::operator< (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return  GOEpsilon(leX) < p.leX ||
          (GOEpsilon(leX) == p.leX && GOEpsilon(leY) < p.leY) ||
          (GOEpsilon(leX) == p.leX && GOEpsilon(leY) == p.leY && GOEpsilon(leZ) < p.leZ);
}

//************************************************************************
// Sommaire:   Définition de l'opérateur > pour des GOCoordXYZ
//
// Description:
//    L'opérateur publique <code>operator<(...)</code> définit l'opérateur
//    > pour des GOCoordXYZ. On utilise l'objet GOEpsilon pour faire les 
//    comparaisons de bas niveau entre des doubles.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
Booleen
GOCoordXYZ<TTCoord>::operator> (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return  GOEpsilon(leX) > p.leX ||
          (GOEpsilon(leX) == p.leX && GOEpsilon(leY) > p.leY) ||
          (GOEpsilon(leX) == p.leX && GOEpsilon(leY) == p.leY && GOEpsilon(leZ) > p.leZ);
}

//************************************************************************
// Sommaire:   Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXYZ</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>istream</code> pour celle-ci.
//
// Entrée:
//    istream& is                   // Stream de lecture
//    GOCoordXYZ<TTCoord>& p        // GOCoordXYZ à lire
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
std::istream&
operator >> (std::istream& is, GOCoordXYZ<TTCoord>& p)
{
#ifdef MODE_DEBUG
   p.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   typedef typename GOCoordXYZ<TTCoord>::TCCoord TCCoord;

   if (is)
   {
      TCCoord leX, leY, leZ;
      is >> leX >> leY >> leZ;
      if (is)
      {
         p.leX = leX;
         p.leY = leY;
         p.leZ = leZ;
      }
   }

#ifdef MODE_DEBUG
   p.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return is;
}

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator <<(...)</code> friend de la classe
//    <code>GOCoordXYZ</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>ostream</code> pour celle-ci.
//
// Entrée:
//    ostream& os                   // Stream d'écriture
//    const GOCoordXYZ<TTCoord>& p  // GOCoordXYZ à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTCoord>
std::ostream&
operator << (std::ostream& os, const GOCoordXYZ<TTCoord>& p)
{
#ifdef MODE_DEBUG
   p.INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (os)
   {
      os << p.leX << " " << p.leY << " " << p.leZ;
   }

#ifdef MODE_DEBUG
   p.INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return os;
}

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXYZ</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& is                 // Fichier de lecture
//    GOCoordXYZ<TTCoord>& p        // GOCoordXYZ à lire
//
// Sortie:
//    FIFichier& : Référence vers le fichier lu.
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
#ifdef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator >> <>(FIFichier& is, GOCoordXYZ<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (is)
      {
         TTCoord leX, leY, leZ;
         is >> leX >> leY >> leZ;
         if (is)
         {
            p.leX = leX;
            p.leY = leY;
            p.leZ = leZ;
         }
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return is;
   }
#else //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator >> (FIFichier& is, GOCoordXYZ<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (is)
      {
         TTCoord leX, leY, leZ;
         is >> leX >> leY >> leZ;
         if (is)
         {
            p.leX = leX;
            p.leY = leY;
            p.leZ = leZ;
         }
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return is;
   }
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
#endif   // MODE_PERSISTANT

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator <<(...)</code> friend de la classe
//    <code>GOCoordXYZ</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& os                 // Fichier de écrire
//    GOCoordXYZ<TTCoord>& p        // GOCoordXYZ à écrire
//
// Sortie:
//    FIFichier& : Référence vers le fichier écrit.
//
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
#ifdef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator << <>(FIFichier& os, const GOCoordXYZ<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (os)
      {
         os << p.leX << " " << p.leY << " " << p.leZ;
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return os;
   }
#else //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
   template <class TTCoord>
   FIFichier&
   operator << (FIFichier& os, const GOCoordXYZ<TTCoord>& p)
   {
   #ifdef MODE_DEBUG
      p.INVARIANTS("PRECONDITION");
   #endif   // MODE_DEBUG

      if (os)
      {
         os << p.leX << " " << p.leY << " " << p.leZ;
      }

   #ifdef MODE_DEBUG
      p.INVARIANTS("POSTCONDITION");
   #endif   // MODE_DEBUG
      return os;
   }
   #endif   // MODE_PERSISTANT
#endif //INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM


//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXYZ</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& is                 // Fichier de lecture
//    GOCoordXYZ<TTCoord>& p        // GOCoordXYZ à lire
//
// Sortie:
//    
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void GOCoordXYZ<TTCoord>::importe(FIFichier& is)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (is)
   {
      TTCoord x, y, z;
      is >> x >> y >> z;
      if (is)
      {
         leX = x;
         leY = y;
         leZ = z;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   return;
}
#endif   // MODE_IMPORT_EXPORT

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordXYZ</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>FIFIchier</code> pour celle-ci.
//
// Entrée:
//    FIFichier& os                 // Fichier de écrire
//    GOCoordXYZ<TTCoord>& p        // GOCoordXYZ à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
#ifdef MODE_IMPORT_EXPORT
template <typename TTCoord>
void GOCoordXYZ<TTCoord>::exporte(FIFichier& os) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   if (os)
   {
      os << leX << " " << leY << " " << leZ;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
   //return os;
}
#endif   // MODE_IMPORT_EXPORT

#endif   // GOCOOXYZ_HPP_DEJA_INCLU
