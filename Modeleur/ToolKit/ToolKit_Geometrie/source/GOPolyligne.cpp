//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPolyligne.cpp
// Classe : GOPolyligne
//************************************************************************
// 16-02-2004  Maude Giasson          Version initiale
//************************************************************************
#include "GOPolyligne.h"

#include "GOCoord3.h"
#include "GOPoint.h"
#include "GOGeometrie.h"
#include "GOMultiPoint.h"
#include "GOMultiPolyligne.h"
#include "GOMultiPolygone.h"
#include "GOGenerateurID.h"

//-- Inclusions GEOS :
#include "geos_sup.h"
#include <geos_c.h>
//#include <geos/geosAlgorithm.h>

#include <algorithm>
#include <iterator>
#include <vector>
#include <map>

//************************************************************************
// Sommaire:  Constructeur par défaut
//
// Description:
//    Le constructeur par défaut <code>GOPolyligne()</code> crée une 
//    polyligne vide.
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPolyligne::GOPolyligne()
   : m_sommets(), m_genIdP(&GOGenerateurID::generiqueGenID)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur de copie
//
// Description:
//    Constructeur par copie. La donnée membre m_polyligneP est initialisée
//    en copiant celle de la polyligne passée en paramètre.
//    
// Entrée: 
//    const GOPolyligne & autre        Polyligne à copier
//
// Sortie:
//
// Notes: 
//************************************************************************
GOPolyligne::GOPolyligne(const GOPolyligne& autre)
   : m_sommets(autre.m_sommets), m_genIdP(autre.m_genIdP)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Destructeur.
//
// Description: 
//    Destructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPolyligne::~GOPolyligne()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   m_sommets.clear();
}

//************************************************************************
// Sommaire:   Opérateur d'affectation.
//
// Description:
//    L'opérateur public <code>operator=(...)</code> est l'opérateur
//    d'affectation de la classe. L'objet est en tout point identique
//    à la polyligne passée en argument.
//    
// Entrée: 
//    const GOPolyligne& autre;     La polyligne à copier
//
// Sortie:
//
// Notes: 
//************************************************************************
GOPolyligne& GOPolyligne::operator = (const GOPolyligne& autre)
{ 
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_sommets = autre.m_sommets;
   m_genIdP  = autre.m_genIdP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode <code>asgGenerateurID(...)</code>
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOPolyligne::asgGenerateurID(GOGenerateurIDP genIdP)
{
#ifdef MODE_DEBUG
   PRECONDITION(genIdP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   m_genIdP = genIdP;
   m_genIdP->asgID(m_sommets.begin(), m_sommets.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>reqGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
GOGenerateurIDP GOPolyligne::reqGenerateurID()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_genIdP;
}

//************************************************************************
// Sommaire:   Ajoute un sommet à la fin de la polyligne.
//
// Description:
//    La méthode <code>ajouteSommet(...)</code> ajoute le sommet passé
//    en paramètre à la fin de la polyligne.
//
// Entrée: 
//    const TCCoord& sommet      Coordonnées du sommet à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::ajouteSommet(const TCSommet& sommet)
{
#ifdef MODE_DEBUG
   EntierN nbrSommetsAvant = reqNbrSommets();
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   ERMsg msg = ERMsg::OK;
   m_sommets.push_back(sommet);
   m_genIdP->asgID(m_sommets.back());

#ifdef MODE_DEBUG
   POSTCONDITION(reqNbrSommets() == (nbrSommetsAvant + 1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute un sommet à la polyligne à la position souhaitée.
//    
//
// Description:
//    La méthode <code>ajouteSommet(...)</code> ajoute le sommet passé
//    en paramètre à la polyligne à la position souhaitée.
//    Par défaut, ce sommet  se retrouve comme étant le dernier 
//    parmis l'ensemble ordonné de sommets formant la polyligne. 
//
// Entrée: 
//    const TCCoord& sommet   Coordonnées du sommet à ajouter
//    EntierN indice          Indice où placer le sommet. indice représente
//                            l'indice que portera le sommet après son ajout.
//                            Il doit être entre 0 et le nombre de sommet de
//                            la polyligne (avant ajout du sommet).
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::ajouteSommet(const TCSommet& sommet, EntierN ind)
{
#ifdef MODE_DEBUG
   EntierN nbrSommetsAvant = reqNbrSommets();
   PRECONDITION(ind <= reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   ERMsg msg = ERMsg::OK;

   // ---  Insère le sommet
   TCContainerSommets::iterator sommetI = m_sommets.begin();
   std::advance(sommetI, ind);

   TCContainerSommets::iterator nI = m_sommets.insert(sommetI, sommet);
   m_genIdP->asgID(*nI);

#ifdef MODE_DEBUG
   POSTCONDITION(reqNbrSommets() == (nbrSommetsAvant + 1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Ajoute des sommets aux intersections avec une polyligne
//
// Description:
//    La méthode <code>ajoutePointsIntersection(...)</code> trouve les points
//    d'intersection entre la polyligne courante et la polyligne passée en
//    paramètre. Elle ajoute des sommets à la polyligne courante à ces endroits.
//
// Entrée:
//    const GOPolyligne& autre :  polyligne qui servira à calculer les pts
//                                d'intersection avec la polyligne courante.
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::ajoutePointsIntersection(const GOPolyligne& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Boucle sur les segments de la polyligne
         TCContainerSommets::iterator s1I;
         TCContainerSommets::iterator s2I   = m_sommets.begin();
   const TCContainerSommets::iterator sFinI = --m_sommets.end();
   while (s2I != sFinI)
   {
      s1I = s2I;
      ++s2I;

      // ---  Contruis la polyligne du segment
      GOPolyligne segment;
      segment.ajouteSommet(*s1I);
      segment.ajouteSommet(*s2I);

      // ---  Calcule l'intersection avec la polyligne "autre"
      GOGeometrie geoIntersection;
      segment.intersection(geoIntersection, autre);

      typedef std::map<double, GOPoint> TTMap;
      TTMap mapDistance;

      // ---  Charge les pts d'intersection dans un map
      //      trié sur la distance au premier point du segment
      {
         const GOMultiPoint& mpt = geoIntersection.reqMultiPoint();
               GOMultiPoint::TCConstIterateur ptI    = mpt.reqDebut();
         const GOMultiPoint::TCConstIterateur ptFinI = mpt.reqFin();
         for ( ; ptI != ptFinI; ++ptI)
         {
            if (*ptI == *s1I) continue;
            if (*ptI == *s2I) continue;
            double dist = (*s1I).distance(*ptI);
            mapDistance.insert(TTMap::value_type(dist, *ptI));
         }
      }

      {
         const GOMultiPolyligne& mpl = geoIntersection.reqMultiPolyligne();
               GOMultiPolyligne::TCConstIterateur plI    = mpl.reqDebut();
         const GOMultiPolyligne::TCConstIterateur plFinI = mpl.reqFin();
         for ( ; plI != plFinI; ++plI)
         {
                  GOPolyligne::TCConstIterateur ptI    = (*plI).reqDebut();
            const GOPolyligne::TCConstIterateur ptFinI = (*plI).reqFin();
            for ( ; ptI != ptFinI; ++ptI)
            {
               if (*ptI == *s1I) continue;
               if (*ptI == *s2I) continue;
               double dist = (*s1I).distance(*ptI);
               mapDistance.insert(TTMap::value_type(dist, *ptI));
            }
         }
      }

      // ---  Insère les points triés
      //      l'insertion n'invalide pas les itérateurs de la std::list
            TTMap::const_iterator ptI    = mapDistance.begin();
      const TTMap::const_iterator ptFinI = mapDistance.end();
      for ( ; ptI != ptFinI; ++ptI)
      {
         TCContainerSommets::iterator nI = m_sommets.insert(s2I, (*ptI).second);
         m_genIdP->asgID(*nI);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return ERMsg::OK;
}

//************************************************************************
// Sommaire:  Test si une polyligne est contenue dans l'objet.
//
// Description:
//    La fonction <code>contient(...)</code> 
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::contient(Booleen& reponse, const GOPolyligne& autre) const
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Construis les objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::LineString(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::LineString(autre);

   // ---  Calcul
   char r = GEOSContains(g_selfP.get(), g_autreP.get());
   if (r == 2) msg = geos_sup::reqMsgErreur();
   reponse = (r == 1);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Translate la polyligne.
//
// Description:
//    La méthode <code>deplace(...)</code> translate la polyligne selon le
//    vecteur de déplacement passé en paramètre.
//  
// Entrée:
//    const TCCoord& vctDeplacement       Vecteur de déplacement
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOPolyligne::deplace(const TCCoord& vctDeplacement)
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   TCContainerSommets::iterator pI   = m_sommets.begin();
   TCContainerSommets::iterator finI = m_sommets.end();
   for ( ; pI != finI; ++pI)
   {
      (*pI).deplace(vctDeplacement);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Translate un sommet de la polyligne.
//
// Description:
//    La méthode <code>deplace(...)</code> deplace un point de la 
//    polyligne selon le vecteur de déplacement passé en paramètre.
//
// Entrée:
//    const TCCoord& vctDeplacement       Vecteur de déplacement
//    const Entier& ind                   Indice du sommet à déplacer
//    
// Sortie:
//
// Notes:-Pour l'instant, le message d'erreur retourné est toujours OK
//************************************************************************
ERMsg GOPolyligne::deplace (const TCCoord& vctDeplacement, EntierN ind)
{
#ifdef MODE_DEBUG
    PRECONDITION(ind < reqNbrSommets());
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Deplace le sommet
   TCContainerSommets::iterator sommetI = m_sommets.begin();
   std::advance(sommetI, ind);
   (*sommetI).deplace(vctDeplacement);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  
//
// Description:
//    La fonction <code>intersection(...)</code> 
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::intersection(GOGeometrie& intersection,
                                const GOPolyligne& autre) const
{
#ifdef MODE_DEBUG
   //intersection est vide
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Construis les objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::LineString(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::LineString(autre);

   // ---  Calcul
   geos_sup::TTGeomUniquePtr g_resP(GEOSIntersection(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(intersection, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Modifie les coordonnées d'un sommet de la polyligne. 
//
// Description:
//    La méthode <code>modifie(...)</code> modifie les coordonnées du sommet
//    dont l'indice est passé en paramètre. Les nouvelles coordonnées sont
//    passées en paramètre.
//
// Entrée:
//    const TCCoord & coord         Nouvelles coordonnées du sommet
//    EntierN indice                Indice du sommet à modifier.
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::modifie (const TCCoord& coord, EntierN ind)
{
#ifdef MODE_DEBUG
   EntierN nbrSommetsAvant = reqNbrSommets();
   PRECONDITION(ind < reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Remplace le sommet
   TCContainerSommets::iterator sommetI = m_sommets.begin();
   std::advance(sommetI, ind);
   sommetI->asgPosition(coord);

#ifdef MODE_DEBUG
   POSTCONDITION(reqNbrSommets() == nbrSommetsAvant);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Retourne la coordonnée à l'indice demandé.
//
// Description:
//    L'opérateur public <code>operator[](...)</code> retourne la coordonnée à
//    l'indice demandé.
//
// Entrée:
//    EntierN ind : l'indice du sommet dont les coordonnées seront retournées
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOPolyligne::TCSommet& GOPolyligne::operator[] (EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
 
   TCContainerSommets::const_iterator sommetI = m_sommets.begin();
   std::advance(sommetI, ind);
   return (*sommetI);
}

//************************************************************************
// Sommaire:   Retourne true si le point passé en se retrouve sur la polyligne
//
// Description:
//    La méthode public <code>pointInterieur(...)</code> sert à savoir si un point
//    se trouve à l'intérieur de la polyligne. Elle retourne true si la coordonnée passée
//    en paramètre est sur la polyligne, false sinon.
//    Si la polyligne a moins de 2 pts, aucune coordonné ne peut être sur cette
//    dernière.
//
// Entrée:
//    const TCCoord & coord : coordonnée qui sera testée pour savoir 
//                            si elle est sur la polyligne.
//    
// Sortie:
//
// Notes:
//    1- Possibilité d'ajouter un facteur de tolérance.
//************************************************************************
Booleen GOPolyligne::pointInterieur(const TCCoord& coord) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   // --- Construction de l'objet GEOS correspondant 
   geos_sup::TTGeomUniquePtr g_selfP = geos_sup::LineString(*this);
   geos_sup::TTGeomUniquePtr g_autreP   = geos_sup::Point(coord);

   // --- Test
   char r = GEOSIntersects(g_autreP.get(), g_selfP.get());
   if (r == 2) throw geos_sup::reqMsgErreur();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG 
    return (r == 1);
}

//************************************************************************
// Sommaire:   Retourne true si le point est un des sommets.
//
// Description:
//    La méthode public <code>estUnSommet(...)</code> sert à savoir
//    si un point est l'un des sommets de la polyligne.  Elle retourne true si
//    le point passée en paramètre est sur l'un des sommets, false sinon.
//
// Entrée:
//    const GOPoint & point : point qui sera testé pour savoir 
//                            s'il est sur un des sommets.
//    const DReel& tolerance :      degré de tolérance
//
// Sortie:
//    EntierN& ID           : l'ID du point trouvé s'il a lieu
//
// Notes:
//
//************************************************************************
Booleen GOPolyligne::estUnSommet(EntierN& ID, const GOPoint& point, const DReel& tolerance) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCConstIterateur sommetI    = m_sommets.begin();
   TCConstIterateur sommetFinI = m_sommets.end();
   Booleen reponse = false;
   
   for( ; sommetI != sommetFinI && !reponse; ++sommetI)
   {
      reponse = sommetI->possedeMemeCoordonnees(point, tolerance);
      if(reponse)
         ID = sommetI->reqID();
   }


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return reponse;
}

//************************************************************************
// Sommaire: 
//    Retourne le nombre de sommet.
//
// Description:
//    La méthode publique <code>reqNbrSommets(...)</code> retourne le nombre de 
//    de sommets de la polyligne.
//
// Entrée:
//    EntierN ind : l'indice de la valeur nodale
//
// Sortie:
//
// Notes:
//
//************************************************************************
EntierN GOPolyligne::reqNbrSommets() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG

   return static_cast<EntierN>(m_sommets.size());
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur sur les coordonnées formant la polyligne.
//
// Description:
//    La méthode publique <code>reqDebut(...)</code> retourne un itérateur
//    permettant de parcourir les divers sommets de la polyligne. Ces sommets
//    seront ceux d'une GOPolyligne, donc des TCCoord.
//
// Entrée:
//
// Sortie: GOIterPolyligne un itérateur sur les sommets d'une polyligne
//
// Notes:
//
//************************************************************************
GOPolyligne::TCConstIterateur GOPolyligne::reqDebut() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG

   return m_sommets.begin();
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur sur les coordonnées formant la polyligne.
//
// Description:
//    La méthode publique <code>reqFin(...)</code> retourne un itérateur
//    pointant sur la fin de la polyligne. 
//
// Entrée:
//
// Sortie: GOIterPolyligne un itérateur sur les sommets d'une polyligne
//
// Notes:
//
//************************************************************************
GOPolyligne::TCConstIterateur GOPolyligne::reqFin() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
   return m_sommets.end();
}

//************************************************************************
// Sommaire:   Retourne le polygone.
//
// Description:
//    La méthode publique <code>retourne(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::retourne()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   std::reverse(m_sommets.begin(), m_sommets.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//   Supprime un sommet d'une polyligne.
//
// Description:
//    La méthode publique <code>supprime(...)</code> supprime le sommet de
//    la polyligne se trouvant à l'indice "indice".
//
// Entrée: const EntierN &indice : indice du sommet à supprimer
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligne::supprime(EntierN ind)
{
#ifdef MODE_DEBUG
   EntierN nbrSommetAvant = reqNbrSommets();
   PRECONDITION(ind < reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Efface le sommet se trouvant à cet endroit
   TCContainerSommets::iterator sommetI = m_sommets.begin();
   std::advance(sommetI, ind);
   m_sommets.erase(sommetI);

#ifdef MODE_DEBUG
   POSTCONDITION( reqNbrSommets() == (nbrSommetAvant - 1) );
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}
