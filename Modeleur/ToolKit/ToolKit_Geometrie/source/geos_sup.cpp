//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: geos_sup.cpp
// Classe : geos_sup
//************************************************************************
// 16-02-2004  Maude Giasson          Version initiale
//************************************************************************
#include "geos_sup.h"

#include "GOCoord3.h"
#include "GOPoint.h"
#include "GOPolyligne.h"
#include "GOPolygone.h"
#include "GOMultiPoint.h"
#include "GOMultiPolyligne.h"
#include "GOMultiPolygone.h"
#include "GOGeometrie.h"

#include <map>
#include <stdio.h>      /* snprintf */
#include <stdarg.h>     /* va_list, va_start, va_arg, va_end */

struct GOInfo
{
   GEOSContextHandle_t handle;
   ERMsg               msg;
};
extern "C" void infoHandler (const char *fmt, ...);
extern "C" void errorHandler(const char *fmt, ...);

#if defined(__WIN32__)
#  include <windows.h>
#  define INRS_THREAD_ID_T  DWORD
#  define INRS_THREAD_GETID GetCurrentThreadId
#else
#  include <unistd.h>
#  define INRS_THREAD_ID_T  pid_t
#  define INRS_THREAD_GETID getpid
#endif
typedef std::map<INRS_THREAD_ID_T, GOInfo*> TTCtxts;
TTCtxts ctxts;

GEOSContextHandle_t geos_sup::reqContexte()
{
   GEOSContextHandle_t handle = NULL;
   try
   {
      handle = ctxts[INRS_THREAD_GETID()]->handle;
   }
   catch (...)
   {
      GOInfo* infoP = new GOInfo();
      infoP->handle = initGEOS_r(infoHandler, errorHandler);
      ctxts.insert( TTCtxts::value_type(INRS_THREAD_GETID(), infoP) ) ;
      handle = ctxts[INRS_THREAD_GETID()]->handle;
   }
   return handle;
}
void infoHandler(const char *fmt, ...)
{
#if defined(__WIN32__)
#  define snprintf _snprintf
#endif

   char buf[256];
   va_list ap;

   va_start(ap, fmt);
   snprintf(buf, sizeof(buf)-1, fmt, ap); // -1 pour msvc
   buf[sizeof(buf)-1] = 0x0;
   va_end  (ap);

   GOInfo* infoP = ctxts[INRS_THREAD_GETID()];
   infoP->msg = ERMsg(ERMsg::ERREUR, buf);
}
void errorHandler(const char */*fmt*/, ...)
{
}
extern "C" void charDeleter(char* s)
{
   if (s) delete s;
}

//************************************************************************
// Sommaire:   Retourne le ERMsg du contexte.
//
// Description:
//    La fonction <code>reqMsgErreur(...)</code> retourne le
//    <code>ERMsg</code> du contexte GEOS.
//
// Entrée:   
//
// Sortie:
//
// Notes: 
//
//************************************************************************
ERMsg geos_sup::reqMsgErreur()
{
   return ctxts[INRS_THREAD_GETID()]->msg;
}

//************************************************************************
// Sommaire:   Contruis une GEOSCoordSequence à partir d'un GOPoint.
//
// Description:
//    La fonction <code>Coordinate(...)</code> construit une nouvelle
//    GEOSCoordSequence à partir de l'objet passé en argument. Il est
//    de la responsabilité de l'appelant de disposer de l'objet retourné.
//
// Entrée:   
//
// Sortie:
//
// Notes: 
//
//************************************************************************
GEOSCoordSequence* geos_sup::Coordinate(const ::GOPoint& p)
{
   const GOCoordonneesXYZ& c = p.reqPosition();
   GEOSCoordSequence* cs = GEOSCoordSeq_create(1, 3); // 1 point, 3D
   GEOSCoordSeq_setX(cs, 0, c.x());
   GEOSCoordSeq_setY(cs, 0, c.y());
   GEOSCoordSeq_setZ(cs, 0, c.z());
   return cs;
}

//************************************************************************
// Sommaire:   Contruis un Point GEOS à partir d'un GOPoint.
//
// Description:
//    La fonction <code>Point(...)</code> construit un Point GEOS 
//    à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie:
//
// Notes: 
//
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::Point(const ::GOPoint& p)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   // ---  Construction du Point GEOS qui prend contrôle de la liste
   GEOSCoordSequence* cs = geos_sup::Coordinate(p);
   GEOSGeometry*      go = GEOSGeom_createPoint(cs);

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Contruis un LinearRing GEOS à partir d'une GOPolyligne.
//
// Description:
//    La fonction <code>LinearRing(...)</code> construit un LinearRing
//    GEOS à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie: 
//    std::unique_ptr<geos::geom::LinearRing>      LinearRing GEOS créé.
//
// Notes: 
//
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::LinearRing(const ::GOPolyligneFermee& p)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   
   // ---  Construis et remplis une liste de point
   const EntierN size = p.reqNbrSommets();
   GEOSCoordSequence* cs = GEOSCoordSeq_create(size, 3);
   for (EntierN i = 0; i < size; ++i)
   {
      const GOCoordonneesXYZ& c = p[i].reqPosition();
      GEOSCoordSeq_setX(cs, 0, c.x());
      GEOSCoordSeq_setY(cs, 0, c.y());
      GEOSCoordSeq_setZ(cs, 0, c.z());
   }

   // ---  Construis le LinearRing GEOS qui prend contrôle de la liste
   GEOSGeometry* go = GEOSGeom_createLinearRing(cs);

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Contruis une LineString GEOS à partir de d'une GOPolyligne.
//
// Description:
//    La fonction <code>LineString(...)</code> construit une LineString
//    GEOS à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie:
//    geos_sup::TTGeomUniquePtr      LineString GEOS créé.
//
// Notes: 
//
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::LineString(const ::GOPolyligne& p)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   
   // ---  Construis et remplis une liste de point
   const EntierN size = p.reqNbrSommets();
   GEOSCoordSequence* cs = GEOSCoordSeq_create(size, 3);
   for (EntierN i = 0; i < size; ++i)
   {
      const GOCoordonneesXYZ& c = p[i].reqPosition();
      GEOSCoordSeq_setX(cs, 0, c.x());
      GEOSCoordSeq_setY(cs, 0, c.y());
      GEOSCoordSeq_setZ(cs, 0, c.z());
   }

   // ---  Construis le LinearRing GEOS qui prend contrôle de la liste
   GEOSGeometry* go = GEOSGeom_createLineString(cs);

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Contruis un Polygon GEOS à partir d'un GOPolygone.
//
// Description:
//    La méthode privée <code>construitGeos(...)</code> construit un polygone
//    geos à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie:
//    geos_sup::TTGeomUniquePtr       Polygone GEOS créé.
//
// Notes: 
//
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::Polygon(const ::GOPolygone& p)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   
         ::GOPolygone::TCConstIterateur plI    = p.reqDebut();
   const ::GOPolygone::TCConstIterateur plFinI = p.reqFin();

   // ---  Construis la frontière extérieure
   GEOSGeometry* g_shellP = NULL;
   if (plI != plFinI)
   {
      g_shellP = geos_sup::LinearRing(*plI).release();
      ++plI;
   }

   // ---  Construis et remplis le vecteur contenant les ring des trous
   typedef std::vector<GEOSGeometry*> TTGeometries;
   TTGeometries g_holes;
   for ( ; plI != plFinI; ++plI)
   {
      g_holes.push_back( geos_sup::LinearRing(*plI).release() );
   }

   // ---  Construis le Poylgone GEOS
   GEOSGeometry* go = GEOSGeom_createPolygon(g_shellP,
                                             &g_holes.front(),
                                             static_cast<unsigned>(g_holes.size()));

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Contruis un MultiPoint GEOS à partir d'un GOMultiPoint.
//
// Description:
//    La fonction <code>MultiPoint(...)</code> construit un MultiPoint
//    geos à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie:
//    std::unique_ptr<geos::geom::MultiPoint>       MultiPoint GEOS créée.
//
// Notes: 
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::MultiPoint(const ::GOMultiPoint& multi)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef std::vector<GEOSGeometry*> TTGeometries;
   TTGeometries g_geoms;

   // ---  Remplis les géométries
         ::GOMultiPoint::TCConstIterateur pI    = multi.reqDebut();
   const ::GOMultiPoint::TCConstIterateur pFinI = multi.reqFin();
   while (pI != pFinI)
   {
      g_geoms.push_back( geos_sup::Point(*pI).release() );
      ++pI;
   }

   // ---  Construis le MultiPoint GEOS
   GEOSGeometry* go = GEOSGeom_createCollection(GEOS_MULTIPOINT,
                                                &g_geoms.front(),
                                                static_cast<unsigned>(g_geoms.size()));

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Contruis un MultiLineString GEOS à partir d'une GOMultiPolyligne.
//
// Description:
//    La fonction <code>MultiLineString(...)</code> construit une MultiLineString
//    geos à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie:
//    std::unique_ptr<geos::geom::MultiLineString>       MultiLineString GEOS créée.
//
// Notes: 
//    Non implantée
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::MultiLineString(const ::GOMultiPolyligne& multi)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef std::vector<GEOSGeometry*> TTGeometries;
   TTGeometries g_geoms;

   // ---  Remplis les géométries
         ::GOMultiPolyligne::TCConstIterateur pI    = multi.reqDebut();
   const ::GOMultiPolyligne::TCConstIterateur pFinI = multi.reqFin();
   while (pI != pFinI)
   {
      g_geoms.push_back(geos_sup::LineString(*pI).release());
      ++pI;
   }

   // ---  Construis le MultiPoint GEOS
   GEOSGeometry* go = GEOSGeom_createCollection(GEOS_MULTILINESTRING,
                                                &g_geoms.front(),
                                                static_cast<unsigned>(g_geoms.size()));

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Contruis un MultiPolygon GEOS à partir d'une GOMultiPolygon.
//
// Description:
//    La fonction <code>MultiPolygon(...)</code> construit un MultiPolygon
//    geos à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie:
//    std::unique_ptr<geos::geom::MultiPolygon>       MultiPolygon GEOS créée.
//
// Notes: 
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::MultiPolygon(const ::GOMultiPolygone& multi)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef std::vector<GEOSGeometry*> TTGeometries;
   TTGeometries g_geoms;

   // ---  Remplis les géométries
         ::GOMultiPolygone::TCConstIterateur pI    = multi.reqDebut();
   const ::GOMultiPolygone::TCConstIterateur pFinI = multi.reqFin();
   while (pI != pFinI)
   {
      g_geoms.push_back(geos_sup::Polygon(*pI).release());
      ++pI;
   }

   // ---  Construis le MultiPolygon GEOS
   GEOSGeometry* go = GEOSGeom_createCollection(GEOS_MULTIPOLYGON,
                                                &g_geoms.front(),
                                                static_cast<unsigned>(g_geoms.size()));

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Contruis un Geometry GEOS à partir d'une GOGeometrie.
//
// Description:
//    La fonction <code>Geometry(...)</code> construit une Geometry
//    geos à partir de l'objet passé en argument.
//
// Entrée:   
//
// Sortie:
//    geos_sup::TTGeomUniquePtr       Geometry GEOS créée.
//
// Notes: 
//    Non implantée
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::Geometry(const ::GOGeometrie& g)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef std::vector<GEOSGeometry*> TTGeometries;
   TTGeometries g_geoms;

   // ---  Remplis les géométries
   geos_sup::TTGeomUniquePtr g_mptP = geos_sup::MultiPoint     (g.reqMultiPoint());
   geos_sup::TTGeomUniquePtr g_mplP = geos_sup::MultiLineString(g.reqMultiPolyligne());
   geos_sup::TTGeomUniquePtr g_mpgP = geos_sup::MultiPolygon   (g.reqMultiPolygone());
   if (! GEOSisEmpty(g_mptP.get())) g_geoms.push_back(g_mptP.release()); 
   if (! GEOSisEmpty(g_mplP.get())) g_geoms.push_back(g_mplP.release());
   if (! GEOSisEmpty(g_mpgP.get())) g_geoms.push_back(g_mpgP.release());

   // ---  Construis le MultiPolygon GEOS
   GEOSGeometry* go = GEOSGeom_createCollection(GEOS_GEOMETRYCOLLECTION,
                                                &g_geoms.front(),
                                                static_cast<unsigned>(g_geoms.size()));

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return geos_sup::TTGeomUniquePtr(go, &GEOSGeom_destroy);
}

/*
//************************************************************************
// Sommaire:   Contruit une Envelope GEOS à partir d'un GOEnveloppe.
//
// Description:
//    La fonction <code>Envelope(...)</code> construit une Envelope
//    GEOS à partir de la GOEnveloppe modeleur passé en argument.
//
// Entrée:   
//
// Sortie:
//
// Notes: 
//
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::Envelope(const ::GOEnveloppe& env)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   geos::geom::Envelope* g_envP(new geos::geom::Envelope(env.reqCoordMin().x(),
                                             env.reqCoordMax().x(),
                                             env.reqCoordMin().y(),
                                             env.reqCoordMax().y() ));
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return g_envP;
}
*/

//************************************************************************
// Sommaire: 
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::transfer(::GOPolyligne& poly, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   const int nbrSommets = GEOSGeomGetNumPoints(g_geoP);
   ASSERTION(nbrSommets >= 0);
   for (int i = 0; msg && i < nbrSommets; ++i)
   {  
     GEOSGeometry const* gP = GEOSGeomGetPointN(g_geoP, i);
     msg = poly.ajouteSommet( geos_sup::GOPoint(gP) );
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire: 
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::transfer(::GOPolyligneFermee& poly, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   const int nbrSommets = GEOSGeomGetNumPoints(g_geoP);
   ASSERTION(nbrSommets >= 0);
   for (int i = 0; msg && i < nbrSommets; ++i)
   {  
     GEOSGeometry const* gP = GEOSGeomGetPointN(g_geoP, i);
     msg = poly.ajouteSommet( geos_sup::GOPoint(gP) );
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire: 
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::transfer(::GOPolygone& poly, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  GOPolyligne contenant le contour
   if (msg)
   {
      ::GOPolyligneFermee pL;
      // ---  Construis la ligne de contour
      GEOSGeometry const* g_lsP = GEOSGetExteriorRing(g_geoP);  // borrowed
      ASSERTION(GEOSGeomType(g_lsP) == std::string("LinearRing"));
      const int nbrSommets = GEOSGeomGetNumPoints(g_lsP);
      ASSERTION(nbrSommets >= 0);
      for (int i = 0; msg && i < (nbrSommets-1); ++i)
      {  
         GEOSGeometry const* gP = GEOSGeomGetPointN(g_geoP, i);
         msg = pL.ajouteSommet(geos_sup::GOPoint(gP));
      }
      if (msg)
      {
         msg = poly.ajoutePolyligne(pL);
      }
   }

   // ---  Construis les trous et les ajoute au polygone.
   int nbrDeTrou = GEOSGetNumInteriorRings(g_geoP);
   ASSERTION(nbrDeTrou >= 0);
   for (int i = 0; msg && i < nbrDeTrou; ++i)
   {
      ::GOPolyligneFermee pL;
      GEOSGeometry const * g_lsP = GEOSGetInteriorRingN(g_geoP, i);  // borrowed
      ASSERTION(GEOSGeomType(g_lsP) == std::string("LinearRing"));
      const int nbrSommets = GEOSGeomGetNumPoints(g_lsP);
      ASSERTION(nbrSommets >= 0);
      for (int j = 0; msg && j < (nbrSommets-1); ++j)
      {
         GEOSGeometry const* gP = GEOSGeomGetPointN(g_lsP, j);
         msg = pL.ajouteSommet(geos_sup::GOPoint(gP));
      }
      if (msg)
      {
         msg = poly.ajoutePolyligne(pL);
      }
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire: 
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::transfer(::GOMultiPoint& mpt, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   const int nbrGeo = GEOSGetNumGeometries(g_geoP);
   ASSERTION(nbrGeo >= 0);
   for (int i = 0; i < nbrGeo; ++i)
   {
      GEOSGeometry const* gP = GEOSGetGeometryN(g_geoP, i);
      mpt.ajoutePoint(geos_sup::GOPoint(gP));
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire: 
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::transfer(::GOMultiPolyligne& mpl, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   const int nbrGeo = GEOSGetNumGeometries(g_geoP);
   ASSERTION(nbrGeo >= 0);
   for (int i = 0; i < nbrGeo; ++i)
   {
      GEOSGeometry const* gP = GEOSGetGeometryN(g_geoP, i);
      mpl.ajoutePolyligne( *geos_sup::GOPolyligne(gP) );
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire: 
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::transfer(::GOMultiPolygone& mpg, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   const int nbrGeo = GEOSGetNumGeometries(g_geoP);
   ASSERTION(nbrGeo >= 0);
   for (int i = 0; i < nbrGeo; ++i)
   {
      GEOSGeometry const* gP = GEOSGetGeometryN(g_geoP, i);
      mpg.ajoutePolygone( *geos_sup::GOPolygone(gP) );
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:  
//   
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void geos_sup::transfer(::GOGeometrie& geo, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   switch (GEOSGeomTypeId(g_geoP))
   {
      case GEOS_POINT :
      {
         ::GOPoint pt = geos_sup::GOPoint(g_geoP);
         geo.reqMultiPoint().ajoutePoint(pt);
         break;
      }

      case GEOS_LINESTRING :
      case GEOS_LINEARRING :
      {
         std::unique_ptr< ::GOPolyligne> pl = geos_sup::GOPolyligne(g_geoP);
         geo.reqMultiPolyligne().ajoutePolyligne(*pl);
         break;
      }

      case GEOS_POLYGON :
      {
         std::unique_ptr< ::GOPolygone> pg = geos_sup::GOPolygone(g_geoP);
         geo.reqMultiPolygone().ajoutePolygone(*pg);
         break;
      }

      case GEOS_MULTIPOINT :
      case GEOS_MULTILINESTRING :
      case GEOS_MULTIPOLYGON :
      case GEOS_GEOMETRYCOLLECTION :
      {
         const int nbrGeo = GEOSGetNumGeometries(g_geoP);
         ASSERTION(nbrGeo >= 0);
         for (int i = 0; i < nbrGeo; ++i)
         {
            GEOSGeometry const* gP = GEOSGetGeometryN(g_geoP, i);
            geos_sup::transfer(geo, gP);
         }
         break;
      }
   }

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:   Copie le contenu de l'objet GEOS dans l'objet Modeleur
//
// Description:   Copie les attributs de l'Envelope GEOS dans la
//                GOEnveloppe passée en paramètre
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::transfer(::GOEnveloppe& env, GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   //L'enveloppe GEOS peut être nulle si la géométrie correspondante est nulle.
   //TODO faut-il générer un msg d'erreur?
   //méthode non disponible dans la version actuelle de geos?
   //ASSERTION(!g_geoP.IsNull());
   GOCoordonneesXYZ pMin = GOCoordonneesXYZ::GRAND;
   GOCoordonneesXYZ pMax = GOCoordonneesXYZ::PETIT;

   const int nbrGeo = GEOSGetNumGeometries(g_geoP);
   ASSERTION(nbrGeo >= 0);
   for (int i = 0; i < nbrGeo; ++i)
   {
      GEOSGeometry const* gP = GEOSGetGeometryN(g_geoP, i);
      GOCoordonneesXYZ p = geos_sup::GOPoint(gP).reqPosition();
      pMin.minGlobal(p);
      pMax.maxGlobal(p);
   }

   msg = env.asgLimites(pMin, pMax);

#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
::GOPoint geos_sup::GOPoint(GEOSCoordSequence const * g_coP)
{
   double x, y, z;
   GEOSCoordSeq_getX(g_coP, 0, &x);
   GEOSCoordSeq_getX(g_coP, 0, &y);
   GEOSCoordSeq_getX(g_coP, 0, &z);
   return ::GOPoint(GOCoordonneesXYZ(x, y, z));
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
::GOPoint geos_sup::GOPoint(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   GEOSCoordSequence const * cs = GEOSGeom_getCoordSeq(g_geo);
   return geos_sup::GOPoint(cs);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOPolyligne> geos_sup::GOPolyligne(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOPolyligne TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOPolyligneFermee> geos_sup::GOPolyligneFermee(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOPolyligneFermee TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOPolygone> geos_sup::GOPolygone(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOPolygone TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOMultiPoint> geos_sup::GOMultiPoint(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOMultiPoint TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOMultiPolyligne> geos_sup::GOMultiPolyligne(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOMultiPolyligne TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOMultiPolygone> geos_sup::GOMultiPolygone(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOMultiPolygone TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOGeometrie> geos_sup::GOGeometrie(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOGeometrie TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:   Transfert le contenu de l'objet GEOS dans 
//                une nouvelle GOEnveloppe.
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::unique_ptr< ::GOEnveloppe> geos_sup::GOEnveloppe(GEOSGeometry const* g_geo)
{
#ifdef MODE_DEBUG
#endif  // ifdef MODE_DEBUG

   typedef ::GOEnveloppe TTGeo;
   TTGeo* goP = new TTGeo();
   transfer(*goP, g_geo);
   return std::unique_ptr<TTGeo>(goP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
geos_sup::TTGeomUniquePtr geos_sup::lis(const std::string& is)
{
#ifdef MODE_DEBUG   
#endif

   GEOSGeometry*  g_geoP = NUL;
   GEOSWKTReader* readerP = GEOSWKTReader_create();

   if (is.substr(0,5) == "SRID=")
   {
      std::string::size_type ipos = is.find(";");
      ASSERTION(ipos != std::string::npos);
      std::string nouveauIs = is.substr(ipos+1);
      g_geoP = GEOSWKTReader_read(readerP, nouveauIs.c_str());
   }
   else
   {
      g_geoP = GEOSWKTReader_read(readerP, is.c_str());
   }

   GEOSWKTReader_destroy(readerP);

#ifdef MODE_DEBUG   
#endif
   return geos_sup::TTGeomUniquePtr(g_geoP, &GEOSGeom_destroy);
}

//************************************************************************
// Sommaire:   Fonctions de lecture/écriture d'une géometrie
//
// Description:
//
// Entrée:  
//
// Sortie:
//
// Notes:
//
//************************************************************************
void geos_sup::lis(::GOGeometrie& geo, const std::string& is)
{
#ifdef MODE_DEBUG   
#endif

   TTGeomUniquePtr g_geoP = geos_sup::lis(is);
   transfer(geo, g_geoP.get());

#ifdef MODE_DEBUG   
#endif
   return;
}

geos_sup::TTCharUniquePtr geos_sup::ecris(GEOSGeometry const* g_geoP)
{
#ifdef MODE_DEBUG   
#endif

   GEOSWKTWriter* writerP = GEOSWKTWriter_create();
   char* r = GEOSWKTWriter_write(writerP, g_geoP);
   GEOSWKTWriter_destroy(writerP);

   return geos_sup::TTCharUniquePtr(r, &charDeleter);
}
geos_sup::TTCharUniquePtr geos_sup::ecris(const ::GOGeometrie& g)
{
#ifdef MODE_DEBUG   
#endif

   TTGeomUniquePtr g_geo = geos_sup::Geometry(g);
   return geos_sup::ecris(g_geo.get());
}

