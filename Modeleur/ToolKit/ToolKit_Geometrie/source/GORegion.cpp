//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GORegion.cpp
// Classe : GORegion
//          et template SRREgion spécialisé pour les GOCoordonneesXYZ
//************************************************************************
#include "GORegion.h"

//************************************************************************
// Sommaire:  
//    Constructeur.
//
// Description:
//    Constructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GORegion::GORegion()
  : multiPoly()
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Constructeur copie
//
// Description: 
//    Constructeur copie de la classe.
//
// Entrée:
//    const GORegion& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
GORegion::GORegion(const GORegion& obj)
   : multiPoly(obj.multiPoly)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Destructeur
//
// Description: 
//    Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GORegion::~GORegion()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Opérateur d'assignation
//
// Description: 
//    Opérateur d'assignation de la classe.
//
// Entrée:
//    const GORegion& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
GORegion& GORegion::operator=(const GORegion& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   multiPoly = obj.multiPoly;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return *this;
}

//************************************************************************
// Sommaire:  
//    Assigne un multiPolygone à la région. 
//
// Description: 
//    L'attribut multiPolygone de la classe correspondra au
//    GOMultiPolygone passé en paramètre.
//
// Entrée:
//    Un GOMultiPolygone.
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GORegion::asgMultiPolygone(const GOMultiPolygone& multi)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   multiPoly = multi;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Ajoute un polygone à la région. 
//
// Description: 
//    Ajoute un polygone au multipolygone de la région.
//
// Entrée:
//    Un GOPolygone.
//
// Sortie:
//    Le GOPolygone a été ajouté à l'attribut GOMultiPolygone.
//
// Notes:
//
//************************************************************************
ERMsg GORegion::ajoutePolygone(const GOPolygone& polygone)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   msg = multiPoly.ajoutePolygone(polygone);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  
//
// Description: 
//    La fonction publique estValide() retourne VRAI si la région est
//    valide. La région est valide si son enveloppe est également valide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GORegion::estValide() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return multiPoly.reqEnveloppe().estValide();
}

//************************************************************************
// Sommaire:  
//    La fonction publique reqEnveloppe() retourne l'enveloppe 
//    (le bounding box) de la région.
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOEnveloppe& GORegion::reqEnveloppe() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return multiPoly.reqEnveloppe();
}

//************************************************************************
// Sommaire:  
//    La fonction publique reqMultiPolygone() retourne le multipolygone 
//    décrivant la région.
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOMultiPolygone& GORegion::reqMultiPolygone() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return multiPoly;
}

//************************************************************************
// Sommaire:  
//    Détermine si le point donné se trouve à l'intérieur de la région.
//
// Description: 
//    Fait usage de la méthode pointInterieur du GOMultiPolygone.
//    Vrai si le point est sur la frontière.
//
// Entrée:
//    const GOCoordonneesXYZ& coord : coordonnées du point à vérifier
//
// Sortie:
//
// Notes:
//************************************************************************
Booleen GORegion::pointInterieur(const GOCoordonneesXYZ& coord) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return multiPoly.pointInterieur(coord);
}

//************************************************************************
// Sommaire: 
//    Operateur==
//
// Description: 
//    L'opérateur public operator==(...) compare la région à celle passée
//    en argument. Les régions sont égales si leur multiPolygones le sont.
//
// Entrée:
//    const GORegion& reg : une région
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GORegion::operator== (const GORegion& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return (multiPoly == obj.multiPoly);
}  

//************************************************************************
// Sommaire: 
//    Opérateur !=
//
// Description: 
//    L'opérateur public operator!=(...) compare la région à celle passée
//    en argument. Les régions sont différentes si leur multiPolygones le sont.
//
// Entrée:
//    const GORegion& reg : une région
//
// Sortie:
//
// Notes:
//************************************************************************
Booleen GORegion::operator!= (const GORegion& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return !(multiPoly == obj.multiPoly);
}
