//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOGeometrie.cpp
// Classe : GOGeometrie
//************************************************************************
// 18-02-2004  Maude Giasson          Version initiale
//************************************************************************
#include "GOGeometrie.h"

#include "GOMultiPolygone.h"
#include "GOMultiPolyligne.h"
#include "GOMultiPoint.h"

#include "geos_sup.h"
#include <geos_c.h>

//************************************************************************
// Sommaire:  Constructeur
//
// Description:   
//    Constructeur par défaut
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOGeometrie::GOGeometrie()
   :  m_multiPolygoneP(), m_multiPolyligneP(), m_multiPointP()
{
   m_multiPolygoneP  = std::unique_ptr<GOMultiPolygone>(new GOMultiPolygone());
   m_multiPolyligneP = std::unique_ptr<GOMultiPolyligne>(new GOMultiPolyligne());
   m_multiPointP     = std::unique_ptr<GOMultiPoint>(new GOMultiPoint());

   m_multiPolygoneP->asgGenerateurID(&m_genId);
   m_multiPolyligneP->asgGenerateurID(&m_genId);
   m_multiPointP->asgGenerateurID(&m_genId);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur par copie
//
// Description:   
//    Constructeur par copie
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOGeometrie::GOGeometrie(const GOGeometrie& GOGeom)
{
   m_multiPolygoneP  = std::unique_ptr<GOMultiPolygone>(new GOMultiPolygone());
   m_multiPolyligneP = std::unique_ptr<GOMultiPolyligne>(new GOMultiPolyligne());
   m_multiPointP     = std::unique_ptr<GOMultiPoint>(new GOMultiPoint());

   *m_multiPolygoneP = GOGeom.reqMultiPolygone();
   *m_multiPolyligneP = GOGeom.reqMultiPolyligne();
   *m_multiPointP = GOGeom.reqMultiPoint();

   m_multiPolygoneP->asgGenerateurID(&m_genId);
   m_multiPolyligneP->asgGenerateurID(&m_genId);
   m_multiPointP->asgGenerateurID(&m_genId);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOGeometrie::~GOGeometrie()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire:   Retourne le multiPolygone.
//
// Description:
//    La méthode publique <code>reqMultiPolygone(...)</code> retourne le 
//    multiPolygone contenant tous les polygones de la géométrie
//
// Entrée:
//
// Sortie: GOMultiPolygone & multiPolygone de la géométrie.
//
// Notes: 
//
//************************************************************************
GOMultiPolygone& GOGeometrie::reqMultiPolygone()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   return *m_multiPolygoneP;
}

//************************************************************************
// Sommaire:   Retourne le multiPolygone.
//
// Description:
//    La méthode publique <code>reqMultiPolygone(...)</code> retourne le 
//    multiPolygone contenant tous les polygones de la géométrie
//
// Entrée:
//
// Sortie: GOMultiPolygone & multiPolygone de la géométrie.
//
// Notes: 
//
//************************************************************************
const GOMultiPolygone& GOGeometrie::reqMultiPolygone() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
   
   return *m_multiPolygoneP;
}

//************************************************************************
// Sommaire: Retourne la multiPolyligne.
//
// Description:
//    La méthode publique <code>reqMultiPolylgine(...)</code> retourne la 
//    multiPolyligne contenant toutes les polylignes de la géométrie
//
// Entrée:
//
// Sortie: GOMultiPolylgine & multiPolyligne de la géométrie.
//
// Notes: 
//
//************************************************************************
GOMultiPolyligne& GOGeometrie::reqMultiPolyligne()
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
   
   return (*m_multiPolyligneP);
}

//************************************************************************
// Sommaire:   Retourne la multiPolyligne.
//
// Description:
//    La méthode publique <code>reqMultiPolylgine(...)</code> retourne la 
//    multiPolyligne contenant toutes les polylignes de la géométrie
//
// Entrée:
//
// Sortie: GOMultiPolylgine & multiPolyligne de la géométrie.
//
// Notes: 
//
//************************************************************************
const GOMultiPolyligne& GOGeometrie::reqMultiPolyligne() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
   
   return (*m_multiPolyligneP);
}

//************************************************************************
// Sommaire:   Retourne le multiPoint.
//
// Description:
//    La méthode publique <code>reqMultiPoint...)</code> retourne le 
//    multiPoint contenant tous les points de la géométrie
//
// Entrée:
//
// Sortie: GOMultiPoint & multiPoint de la géométrie.
//
// Notes: 
//
//************************************************************************
GOMultiPoint& GOGeometrie::reqMultiPoint()
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
   
   return (*m_multiPointP);
}

//************************************************************************
// Sommaire: Retourne le multiPoint.
//
// Description:
//    La méthode publique <code>reqMultiPoint...)</code> retourne le 
//    multiPoint contenant tous les points de la géométrie
//
// Entrée:
//
// Sortie: GOMultiPoint & multiPoint de la géométrie.
//
// Notes: 
//
//************************************************************************
const GOMultiPoint& GOGeometrie::reqMultiPoint() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
   
   return (*m_multiPointP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes: 
//
//************************************************************************
/*
std::ostream& operator << (std::ostream& os, const GOGeometrie& geo)
{
   try
   {
      os << geos_sup::ecris(geo);
   }
   catch (geos::util::GEOSException* eP)
   {
      throw geos_sup::reqMsgErreur(eP);
   }
   return os;
}

std::istream& operator >> (std::istream& is, GOGeometrie& geo)
{
   try
   {
      std::string txt;
      std::getline(is, txt);
      geos_sup::lis(geo, txt);
   }
   catch (geos::util::GEOSException* eP)
   {
      throw geos_sup::reqMsgErreur(eP);
   }
   return is;
}
*/

//************************************************************************
// Sommaire:  Operateur d'affectation
//
// Description:   
//    Surcharge de l'operateur = (constructeur par copie)
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOGeometrie& GOGeometrie::operator=  (const GOGeometrie& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (this != &autre)
   {
      *m_multiPolygoneP  = autre.reqMultiPolygone();
      *m_multiPolyligneP = autre.reqMultiPolyligne();
      *m_multiPointP     = autre.reqMultiPoint();

      m_multiPolygoneP->asgGenerateurID(&m_genId);
      m_multiPolyligneP->asgGenerateurID(&m_genId);
      m_multiPointP->asgGenerateurID(&m_genId);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}
