//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOMultiPolygone.cpp
// Classe : GOMultiPolygone
//************************************************************************
// 13-02-2004  Maude Giasson          Version initiale
// 30-05-2005  Sybil Christen         Ajout opérateur == requis par Region
//************************************************************************
#include "GOMultiPolygone.h"

#include "GOCoord3.h"
#include "GOGeometrie.h"
#include "GOPolygone.h"
#include "GOMultiPoint.h"
#include "GOMultiPolyligne.h"
#include "GOGenerateurID.h"

#include "geos_sup.h"
#include <geos_c.h>

//************************************************************************
// Sommaire:  Constructeur
//
// Description:
//    Constructeur par défaut
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolygone::GOMultiPolygone()
   : m_multiPolygone()
   , m_genIdP(NUL)
   , m_proprioGenId(FAUX)
   , m_geom_changee(FAUX)
   , m_enveloppe()
{
   m_genIdP = new GOGenerateurID();
   m_proprioGenId = VRAI;
   m_geom_changee = VRAI; //indique que l'enveloppe sera à recalculer au besoin

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur de copie
//
// Description:
//    Constructeur de copie. 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolygone::GOMultiPolygone(const GOMultiPolygone& autre)
   : m_multiPolygone()
   , m_genIdP(NUL)
   , m_proprioGenId(FAUX)
   , m_geom_changee(FAUX)
   , m_enveloppe()
{
   m_genIdP = new GOGenerateurID();
   m_proprioGenId = VRAI;
   m_geom_changee = VRAI; //indique que l'enveloppe sera à recalculer au besoin

   //-- Remplir le vecteur
   m_multiPolygone.reserve(autre.m_multiPolygone.size());
   for (TCContainer::const_iterator polyI = autre.m_multiPolygone.begin();
        polyI != autre.m_multiPolygone.end();
        ++polyI)
   {
      GOPolygoneP nouveauP = new GOPolygone(*(*polyI));
      nouveauP->asgGenerateurID(m_genIdP);
      m_multiPolygone.push_back(nouveauP);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolygone::~GOMultiPolygone()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   for (TCContainer::iterator polyI = m_multiPolygone.begin();
        polyI != m_multiPolygone.end();
        ++polyI)
   {
      delete (*polyI);
   } 
   m_multiPolygone.clear();

   if (m_proprioGenId) delete m_genIdP;
   m_genIdP = NUL;   
}

//************************************************************************
// Sommaire:   Opérateur d'affectation
//
// Description:
//    L'opérateur publique <code>operator=(...)</code> permet de copier le 
//    multipolygone passé en paramètre dans l'objet courant.
//
// Entrée:
//    GOMultiPolygone & autre;  à  copier dans l'objet courant.
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolygone& GOMultiPolygone::operator=  (const GOMultiPolygone& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   m_geom_changee = VRAI; //indique que l'enveloppe sera à recalculer au besoin

   if (this != &autre)
   {
      //-- 1. Vider le vecteur
      for (TCContainer::iterator polyI = m_multiPolygone.begin();
           polyI != m_multiPolygone.end();
           ++polyI)
      {
         delete (*polyI);
      }
      m_multiPolygone.clear();

      //-- 2. Supprimer le générateur d'ID s'il était propriétaire et 
      //--    le faire pointer vers celui de l'autre
      if (m_proprioGenId) delete m_genIdP;
      m_genIdP         = new GOGenerateurID();
      m_proprioGenId   = VRAI;

      //-- 3. Remplir le vecteur
      m_multiPolygone.reserve(autre.m_multiPolygone.size());
      for (TCContainer::const_iterator polyI = autre.m_multiPolygone.begin();
         polyI != autre.m_multiPolygone.end();
         ++polyI)
      {
         GOPolygoneP nouveauP = new GOPolygone(*(*polyI));
         nouveauP->asgGenerateurID(m_genIdP);
         m_multiPolygone.push_back(nouveauP);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}  

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>asgGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
void GOMultiPolygone::asgGenerateurID(GOGenerateurIDP genIdP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (m_proprioGenId)
      delete m_genIdP;
   m_genIdP = genIdP;
   m_proprioGenId = FAUX;

   for (TCContainer::iterator polyI = m_multiPolygone.begin();
        polyI != m_multiPolygone.end();
        ++polyI)
   {
      (*polyI)->asgGenerateurID(m_genIdP);
   }
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>reqGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
GOGenerateurIDP GOMultiPolygone::reqGenerateurID()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_genIdP;
}

//************************************************************************
// Sommaire:  
//
// Description: 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOMultiPolygone::ajoutePointsCroisement(const GOMultiPolygone& autre)
{
#ifdef MODE_DEBUG
   //Il faut des mpg sans auto intersection sinon peu de sens..
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   ERMsg msg = ERMsg::OK;

   m_geom_changee = VRAI; //indique que l'enveloppe sera à recalculer au besoin

   TCContainer::iterator courantI = m_multiPolygone.begin();
   TCContainer::iterator courantFinI = m_multiPolygone.end();
   for(; courantI != courantFinI; ++courantI)
   {
      TCContainer::const_iterator autreI = autre.m_multiPolygone.begin();
      TCContainer::const_iterator autreFinI = autre.m_multiPolygone.end();
      for(; autreI != autreFinI; ++ autreI)
      {
         (*courantI)->ajoutePointsCroisement(**autreI);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute un polygone au multiPolygone
//
// Description: 
//    La fonction <code>ajoutePolygone(...)</code> ajoute le polygone
//    passé en paramètre au vecteur de polygone formant le GOMultiPolygone
//
// Entrée:
//    const GOPolygone& polygone:         polygone à ajouter
//
// Sortie:
//
// Notes: 
//************************************************************************
ERMsg GOMultiPolygone::ajoutePolygone(const GOPolygone& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_geom_changee = VRAI; //indique que l'enveloppe sera à recalculer au besoin

   ERMsg msg = ERMsg::OK;

   GOPolygoneP nouveauP = new GOPolygone(autre);
   nouveauP->asgGenerateurID(m_genIdP);
   m_multiPolygone.push_back(nouveauP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute un polygone au multiPolygone
//
// Description: 
//    La fonction <code>ajoutePolygone(...)</code> crée un polygone 
//    à partir de la polyligne passée en paramètre 
//    et l'ajoute au vecteur de polygone formant le GOMultiPolygone
//
// Entrée:
//    const GOPolygone& polygone:         polygone à ajouter
//
// Sortie:
//
// Notes: 
//************************************************************************
ERMsg GOMultiPolygone::ajoutePolygone(const GOPolyligneFermee& contour)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_geom_changee = VRAI; //indique que l'enveloppe sera à recalculer au besoin

   ERMsg msg = ERMsg::OK;

   GOPolygoneP nouveauP = new GOPolygone();
   nouveauP->asgGenerateurID(m_genIdP);
   nouveauP->ajoutePolyligne(contour);
   m_multiPolygone.push_back(nouveauP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Deplace le polygone dont l'indice est passé en paramètre
//
// Description:
//    La méhtode publique <code>deplacePG(...)</code> deplace le polygone
//    dont l'indice est passé en paramètre.
//  
// Entrée:
//    const TCCoord& vctDeplacement    vecteur de déplacement
//    Entier indPG                     indice du polygone à deplacer
//
// Sortie:
//
// Notes:-Pour l'instant, le message d'erreur retourné est toujours OK
//       -Il n'y a pas de condition sur l'intersection des divers polygone
//        de ce multiPolygone
//************************************************************************
ERMsg GOMultiPolygone::deplacePG(const TCCoord& vctDeplacement,
                                 EntierN indPG)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_geom_changee = VRAI; //indique que l'enveloppe sera à recalculer au besoin

   ERMsg msg = ERMsg::OK;
   m_multiPolygone[indPG]->deplace(vctDeplacement);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Deplace un polyligne d'un des polygone
//
// Description: La fonction <code>deplacePLdePG(...)</code> deplace une
//              une polyligne du polygone dont l'indice est passé en paramètre.
//  
// Entrée:  const TCCoord& vctDeplacement vecteur de déplacement
//          const Entier & indPG          indice du polygone à altérer
//          const Entier & indPL          indice de la polyligne à déplacer dans
//                                        le polygone
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::deplacePLdePG(const TCCoord& vctDeplacement, 
                                     EntierN indPG, 
                                     EntierN indPL)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

  //indique que l'enveloppe sera à recalculer au besoin
  m_geom_changee = VRAI;

   ERMsg msg = ERMsg::OK;
   m_multiPolygone[indPG]->deplacePL(vctDeplacement, indPL);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Deplace un sommet d'une polyligne d'un des polygone
//
// Description: La fonction <code>deplacePTdePLdePG(...)</code> deplace un sommet
//              d'une polyligne du polygone dont l'indice est passé en paramètre.
//  
// Entrée:  const TCCoord& vctDeplacement vecteur de déplacement
//          const Entier & indPG          indice du polygone à altérer
//          const Entier & indPL          indice de la polyligne à altérer
//          EntierN indPT           indice du sommet de la polyligne
//                                        à déplacer.
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::deplacePTdePLdePG(const TCCoord& vctDeplacement, 
                                         EntierN indPG, 
                                         EntierN indPL,
                                         EntierN indPT)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //indique que l'enveloppe sera à recalculer au besoin
   m_geom_changee = VRAI;

   ERMsg msg = ERMsg::OK;
   msg = m_multiPolygone[indPG]->deplacePTdePL(vctDeplacement, indPL, indPT);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//
// Description: La fonction <code>difference(...)</code>
//  
// Entrée: 
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::difference(const GOMultiPolygone& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   GOGeometrie diff;
   msg = this->difference(diff, autre);

   //  On mime un oparateur = !!!!
   if (msg)
   {
      // ---  Vide this
      for (TCContainer::iterator polyI = m_multiPolygone.begin();
           polyI != m_multiPolygone.end();
           ++polyI)
      {
         delete (*polyI);
      }
      m_multiPolygone.clear();

      // ---  Copie de l'autre
      const GOMultiPolygone& mpg = diff.reqMultiPolygone();
      for (TCConstIterateur pgI = mpg.reqDebut(); pgI != mpg.reqFin(); ++pgI)
      {
         this->ajoutePolygone(*pgI);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//
// Description: La fonction <code>difference(...)</code>
//  
// Entrée: 
//
// Sortie:
//
// Notes: 
//
//************************************************************************
ERMsg GOMultiPolygone::difference(GOGeometrie& resultat, const GOMultiPolygone& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::MultiPolygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::MultiPolygon(autre);

   // ---  Demande à GEOS d'effectuer la différence
   geos_sup::TTGeomUniquePtr g_resP(GEOSDifference(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(resultat, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//
// Description: La fonction <code>intersection(...)</code>
//  
// Entrée: 
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::intersection(GOGeometrie& resultat, const GOMultiPoint& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP = geos_sup::MultiPolygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP   = geos_sup::MultiPoint  (autre);

   // ---  Demande à GEOS d'effectuer l'opération
   geos_sup::TTGeomUniquePtr g_resP(GEOSIntersection(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(resultat, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//
// Description: La fonction <code>intersection(...)</code>
//  
// Entrée: 
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::intersection(GOGeometrie& resultat, const GOMultiPolygone& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::MultiPolygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::MultiPolygon(autre);

   // ---  Demande à GEOS d'effectuer l'opération
   geos_sup::TTGeomUniquePtr g_resP(GEOSIntersection(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(resultat, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}
//************************************************************************
// Sommaire: 
//
// Description: La fonction <code>intersection(...)</code>
//  
// Entrée: 
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::intersection(GOGeometrie& resultat, const GOGeometrie& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::MultiPolygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::Geometry    (autre);

   // ---  Demande à GEOS d'effectuer l'opération
   geos_sup::TTGeomUniquePtr g_resP(GEOSIntersection(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(resultat, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Retourne le polygone à l'indice demandé.
//
// Description:
//    L' opérateur public <code>operator[](...)</code> retourne le polygone à
//    l'indice demandé. 
// Entrée:
//    EntierN ind : l'indice du polygone voulu
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOPolygone& GOMultiPolygone::operator[] (EntierN indPG) const
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

  return (*m_multiPolygone[indPG]);
}

//************************************************************************
// Sommaire: 
//    Indique si les deux GOMultiPolygones sont identiques.
//
// Description:
//    Retourne true si les deux GOMultiPolygones sont identiques.
//    Ils sont identiques si l'ensemble de leurs GOPolygones le sont.
//
// Entrée:
//    GOMultiPolygone&     Multipolygone à  comparer avec l'objet courant.
//
// Sortie:
//
// Notes:
//    Attention car l'opérateur== de GOPolygone retourne toujours VRAI
//************************************************************************
Booleen GOMultiPolygone::operator== (const GOMultiPolygone& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   //comparer nombre de polygones
   if (m_multiPolygone.size() != autre.m_multiPolygone.size()) return FAUX;

   TCContainer::const_iterator polyI     = m_multiPolygone.begin();
   TCContainer::const_iterator polyFinI  = m_multiPolygone.end();

   TCContainer::const_iterator autrePolyI    = autre.m_multiPolygone.begin();
   TCContainer::const_iterator autrePolyFinI = autre.m_multiPolygone.begin();
   
   while(polyI != polyFinI && autrePolyI != autrePolyFinI)
   {
      if (!(polyI == autrePolyI)) return FAUX;
      polyI++;
      autrePolyI++;
   }
   return VRAI;
}

//************************************************************************
// Sommaire:   Rendre les polylignes fermées dans le sens anti-horaire.
//
// Description:
//    La méthode publique <code>soisAntiHoraire(...)</code> oblige à toutes
//    les polylignes fermées de tous les polygones à s'orienter dans le 
//    sens anti-horaire.  
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::soisAntiHoraire()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   ERMsg msg = ERMsg::OK;

         TCContainer::iterator pgI    = m_multiPolygone.begin();
   const TCContainer::iterator pgFinI = m_multiPolygone.end();
   for( ; pgI != pgFinI && msg; ++pgI)
   {
      msg = (*pgI)->soisAntiHoraire();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return msg;
}

//************************************************************************
// Sommaire: 
//    Retourne true si le point passé en paramètre est dans un des polygone, 
//    false sinon.
//
// Description:
//    La méthode publique <code>pointInterieur(...)</code> sert à savoir
///   si un point se trouve à l'intérieur d'un des polygone du multipolygone. 
//    Elle retourne true si la coordonnée passée en paramètre fait partie 
//    d'un des polygones, false sinon.
//
// Entrée:
//    const TCCoord& coord        : coordonnée qui sera testée pour savoir 
//                                  si elle fait partie d'un des polygone.
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOMultiPolygone::pointInterieur(const TCCoord& coord) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   Booleen reponse = false;

         TCContainer::const_iterator pgI    = m_multiPolygone.begin();
   const TCContainer::const_iterator pgFinI = m_multiPolygone.end();
   for ( ; pgI != pgFinI && !reponse; ++pgI)
   {
      reponse = (*pgI)->pointInterieur(coord);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return reponse;
}

//************************************************************************
// Sommaire:   Retourne true si le point est un de ses sommets.
//
// Description:
//    La méthode public <code>estUnSommet(...)</code> sert à savoir
//    si un point est l'un des sommets du multipolygone.  Elle retourne true si
//    le point passée en paramètre est sur l'un des sommets, false sinon.
//
// Entrée:
//    const GOPoint & point :    point qui sera testé pour savoir 
//                               s'il est sur un des sommets.
//    const DReel& tolerance :   degré de tolérance
//
// Sortie:
//    EntierN& ID           : l'ID du point trouvé s'il a lieu
//
// Notes:
//
//************************************************************************
Booleen GOMultiPolygone::estUnSommet(EntierN& ID, 
                                     const GOPoint& point, 
                                     const DReel& tolerance) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCConstIterateur polygoneI    = m_multiPolygone.begin();
   TCConstIterateur polygoneFinI = m_multiPolygone.end();
   Booleen reponse = false;
   
   for( ; polygoneI != polygoneFinI && !reponse; ++polygoneI)
   {
      reponse = polygoneI->estUnSommet(ID, point, tolerance);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return reponse;
}

//************************************************************************
// Sommaire: 
//    Retire le polygone dont l'indice est passé en paramètre du multiPolygone
//
// Description:
//    La méthode publique <code>retirePG(...)</code> retire le polygone dont
//    l'indice est passé en paramètre au multiPolygone.
//
// Entrée:
//    EntierN indPG           : indice du polygone à retirer
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOMultiPolygone::retirePG(EntierN indPG)
{
#ifdef MODE_DEBUG
   EntierN nbrPgAvant = reqNbrPolygones();
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //indique que l'enveloppe sera à recalculer au besoin
   m_geom_changee = VRAI;

   ERMsg msg = ERMsg::OK;
  
   delete m_multiPolygone[indPG];
   m_multiPolygone.erase(m_multiPolygone.begin() + indPG);

#ifdef MODE_DEBUG
   POSTCONDITION(reqNbrPolygones() == nbrPgAvant - 1);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Retire une polyligne à un des polygone du multiPolygone.
//
// Description:
//    La méthode publique <code>retirePLdePG(...)</code> retire une polyligne à
//    un des polygone du multiPolygone.
//
// Entrée:
//    EntierN indPG     Indice du polygone
//    EntierN indPL     Indice de la polyligne du polygone
//    
// Sortie:
//
// Notes:
//    -indicePL ne doit pas valloir 0. Un indicePL égal à 0 fait peu de
//     sens car on aurait un PG dont on retire la frontière extérieure.
//     -À vérifier (mémoire)
//************************************************************************
ERMsg GOMultiPolygone::retirePLdePG(EntierN indPG, 
                                    EntierN indPL)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   PRECONDITION(indPL != 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //indique que l'enveloppe sera à recalculer au besoin
   m_geom_changee = VRAI;

   ERMsg msg = ERMsg::OK;
   msg = m_multiPolygone[indPG]->supprimePL(indPL);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}


//************************************************************************
// Sommaire: 
//    Retire un sommet à une des polyligne d'un des polygone du multiPolygone.
//
// Description:
//    La méthode publique <code>retirePTdePLdePG(...)</code> retire un sommet à une
//    des polyligne d'un des polygone du multiPolygone.
//
// Entrée: 
//    EntierN indPG     Indice du polygone
//    EntierN indPL     Indice de la polyligne du polygone
//    EntierN indPT     Indice du point de la polyligne 
//
// Sortie:
//
// Notes:  - À vérifier (mémoire)
//
//************************************************************************
ERMsg GOMultiPolygone::retirePTdePLdePG(EntierN indPG, 
                                        EntierN indPL,
                                        EntierN indPT)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPG < reqNbrPolygones());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //indique que l'enveloppe sera à recalculer au besoin
   m_geom_changee = VRAI;

   ERMsg msg = ERMsg::OK;
   msg = m_multiPolygone[indPG]->supprimePTdePL(indPL, indPT);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Sert à trouver quel polygone contient la coordonnée passée en paramètre. 
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
/*
ERMsg GOMultiPolygone::reqProprietaire(TCContainer& proprietaires, const TCCoord& coord)
{
#ifdef MODE_DEBUG
  PRECONDITION(proprietaires.size()==0);  
  INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

     ERMsg msg = ERMsg::OK;
        
     TCContainer::const_iterator itI;
     TCContainer::const_iterator finI =  m_multiPolygone.end();
     for(itI = m_multiPolygone.begin(); itI != finI ; ++itI)
     {
         if ((*itI)->pointInterieur(coord))
         {
            proprietaires.push_back(*itI);
         }
     }
    
#ifdef MODE_DEBUG
    POSTCONDITION(proprietaires.size()>0);
    INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG     
     return msg;
}
*/

//************************************************************************
// Sommaire: Retourne le nombre de polygone formant le multipolygone.
//
// Description:
//    La méthode publique <code>reqNbrPolygones(...)</code> retourne le nombre
//    de polygone formant le multiPolygone. 
//
// Entrée:
//    
// Sortie:
//
// Notes: 
//
//************************************************************************
EntierN GOMultiPolygone::reqNbrPolygones() const
{
#ifdef MODE_DEBUG
  INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return static_cast<EntierN>(m_multiPolygone.size());
}

//************************************************************************
// Sommaire:   Retourne le nombre de sommets du multipolygone.
//
// Description:
//    La méthode publique <code>reqNbrSommets(...)</code> retourne le nombre
//    de sommets du multiPolygone.
//
// Entrée:
//    
// Sortie:
//
// Notes: 
//
//************************************************************************
EntierN GOMultiPolygone::reqNbrSommets() const
{
#ifdef MODE_DEBUG
  INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN nbrSommets = 0;
         TCConstIterateur pI   = reqDebut();
   const TCConstIterateur finI = reqFin();
   while (pI != finI)
   {
      nbrSommets += (*pI).reqNbrSommets();
      ++pI;
   }

#ifdef MODE_DEBUG
  INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return nbrSommets;
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de polygone du multipolygone
//
// Description:
//    La méthode publique <code>reqPolygoneDebut()</code> retourne un itérateur 
//    de polygone du multiPolygone. L'itérateur est positionné au début du 
//    vecteur.
//  
// Entrée:
//    
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolygone::TCConstIterateur
GOMultiPolygone::reqDebut() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_multiPolygone.begin();
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de polygone du multiPolygone
//
// Description:
//    La méthode publique <code>reqPolygoneFin()</code> retourne un itérateur 
//    de polygone du multiPolygone indiquant la fin. 
//  
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolygone::TCConstIterateur
GOMultiPolygone::reqFin()const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_multiPolygone.end();
}

//************************************************************************
// Sommaire: 
//    Retourne l'enveloppe du multipolygone
//
// Description:
//    La méthode publique <code>reqEnveloppe()</code> retourne l'enveloppe
//    du multiPolygone. L'enveloppe est recalculée si la géométrie a changé.
//  
// Entrée: n/a
//    
// Sortie: GOEnveloppe
//
// Notes:
//
//************************************************************************
GOEnveloppe& GOMultiPolygone::reqEnveloppe() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (m_geom_changee)
   {
      ERMsg msg = this->recalculeEnveloppe();
      //TODO Comment traiter l'erreur? donner le m_enveloppe précédent??
      m_geom_changee = FAUX;
   }
   return m_enveloppe;

}//reqEnveloppe

//************************************************************************
// Sommaire: Recalcule l'enveloppe
//
// Description: La fonction privée <code>recalculeEnveloppe(...)</code>
//    obtient l'enveloppe (rectangle englobant entièrement la géométrie) 
//    du multipolygone. Fait usage des méthodes GEOS.
//  
// Entrée: n/a
//
// Sortie: m_enveloppe mis à jour
//
// Notes:
//************************************************************************
ERMsg GOMultiPolygone::recalculeEnveloppe() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP = geos_sup::MultiPolygon(*this);

   // ---  Demande à GEOS d'effectuer l'opération
   // Note: getEnvelopeInternal retourne une Envelope
   // et getEnvelope retourne une Geometry.
   //L'utilisation d'un auto-pointer crée un problème lors de la destruction
   //de la géométrie, qui tente de détruire elle-meme son enveloppe, qui
   //n'existe deja plus.
   //std::unique_ptr<const geos::Envelope> g_resP(g_thisP->getEnvelopeInternal());
   geos_sup::TTGeomUniquePtr g_resP(GEOSEnvelope(g_selfP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans l'attribut de classe m_enveloppe
   if (msg) geos_sup::transfer(m_enveloppe, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}//recalculeEnveloppe

