//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOMultiPoint.cpp
// Classe : GOMultiPoint
//************************************************************************
// 11-03-2004  Maude Giasson          Version initiale
//************************************************************************
#include "GOMultiPoint.h"

#include "GOCoord3.h"
#include "GOPoint.h"
#include "GOGenerateurID.h"

//************************************************************************
// Sommaire:   Constructeur
//
// Description:
//    Constructeur par défaut
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPoint::GOMultiPoint()
   : m_multiPoint(), m_genIdP(NUL), m_proprioGenId(FAUX)
{
   m_genIdP = new GOGenerateurID();
   m_proprioGenId = VRAI;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur de copie
//
// Description:
//    Constructeur de copie. 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPoint::GOMultiPoint(const GOMultiPoint& autre)
   :  m_multiPoint(autre.m_multiPoint)
   ,  m_genIdP(NUL)
   ,  m_proprioGenId(FAUX)
{
   m_genIdP = new GOGenerateurID();
   m_proprioGenId = VRAI;

   m_genIdP->asgID(m_multiPoint.begin(), m_multiPoint.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPoint::~GOMultiPoint()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (m_proprioGenId) delete m_genIdP;
   m_genIdP = NUL;
}

//************************************************************************
// Sommaire:   Opérateur d'affectation
//
// Description:
//    L'opérateur publique <code>operator=(...)</code> permet de copier le 
//    multipoint passé en paramètre dans l'objet courant.
//
// Entrée:
//    GOMultiPoint & autre;  à  copier dans l'objet courant.
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPoint& GOMultiPoint::operator=  (const GOMultiPoint& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (this != &autre)
   {
      m_multiPoint   = autre.m_multiPoint;

      m_genIdP = new GOGenerateurID();
      m_proprioGenId = VRAI;

      m_genIdP->asgID(m_multiPoint.begin(), m_multiPoint.end());
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>asgGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
void GOMultiPoint::asgGenerateurID(GOGenerateurIDP genIdP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (m_proprioGenId)
      delete m_genIdP;
   m_genIdP = genIdP;
   m_proprioGenId = FAUX;

   m_genIdP->asgID(m_multiPoint.begin(), m_multiPoint.end());
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>reqGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
GOGenerateurIDP GOMultiPoint::reqGenerateurID()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_genIdP;
}

//************************************************************************
// Sommaire:  Ajoute un point au multiPoint
//
// Description: 
//    La fonction <code>ajoutePoint(...)</code> ajoute le point passé 
//    en paramètre au vecteur de point formant le GOMultiPoint.
//
// Entrée:
//    TCSommet& point:         point à ajouter
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPoint::ajoutePoint(const TCSommet& point)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   m_multiPoint.push_back(point);
   m_genIdP->asgID(m_multiPoint.back());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Deplace un point du multiPoint
//
// Description:
//    La fonction <code>deplacePoint(...)</code> deplace le point dont
//    l'indice est passé en paramètre.
//
// Entrée:
//    const TCCoord& vctDeplacement    vecteur de déplacement du point
//    const Entier& ind                indice du point dans le multiPoint
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOMultiPoint::deplacePoint(const TCCoord& vctDeplacement,
                                 EntierN ind)
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < reqNbrPoints());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
  
   m_multiPoint[ind].deplace(vctDeplacement);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Enlève un point au multiPoint
//    
// Description:
//    La fonction <code>retirePoint(...)</code> retire le point dont l'indice
//    est passé en paramètre du vecteur de point formant le GOMultiPoint
//    
// Entrée:
//    Entier & ind: indice du point à retirer
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOMultiPoint::retirePoint(EntierN ind)
{
#ifdef MODE_DEBUG
   EntierN nbrPtAvant = reqNbrPoints();
   PRECONDITION(ind < reqNbrPoints());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   TCContainer::iterator itI = m_multiPoint.begin();
   m_multiPoint.erase(itI + ind);

#ifdef MODE_DEBUG
   POSTCONDITION(reqNbrPoints() == (nbrPtAvant - 1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

 //************************************************************************
// Sommaire: 
//    Retourne le point à l'indice demandé.
//
// Description:
//    L' opérateur public <code>operator[](...)</code> retourne le point à
//    l'indice demandé. 
// Entrée:
//    EntierN ind : l'indice du point voulu
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOMultiPoint::TCSommet& 
GOMultiPoint::operator[] (EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < reqNbrPoints());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

  return m_multiPoint[ind];
}

//************************************************************************
// Sommaire:   Retourne true si le point est un des sommets.
//
// Description:
//    La méthode public <code>estUnSommet(...)</code> sert à savoir
//    si un point est l'un des sommets du multipoint.  Elle retourne true si
//    le point passée en paramètre est sur l'un des sommets, false sinon.
//
// Entrée:
//    const GOPoint & point : point qui sera testé pour savoir 
//                            si elle est sur un des sommets.
//    const DReel& tolerance :      degré de tolérance
//
// Sortie:
//    EntierN& ID           : l'ID du point trouvé s'il a lieu
//
// Notes:
//
//************************************************************************
Booleen GOMultiPoint::estUnSommet(EntierN& ID, const GOPoint& point, const DReel& tolerance) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCConstIterateur sommetI    = m_multiPoint.begin();
   TCConstIterateur sommetFinI = m_multiPoint.end();
   Booleen reponse = false;

   for( ; sommetI != sommetFinI && !reponse; ++sommetI)
   {
      reponse = sommetI->possedeMemeCoordonnees(point, tolerance);
      if(reponse)
         ID = sommetI->reqID();
   }


#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return reponse;
}

//************************************************************************
// Sommaire: 
//    Sert à trouver sur quels points se trouve la coordonnée passée en 
//    paramètre. 
//
// Description:
//    La méthode public <code>pointInterieur(...)</code> sert à savoir si un point
//    se trouve à l'intérieur d'un des point du multipoint. 
//    Elle retourne true si la coordonnée passée en paramètre fait partie 
//    d'un des points, false sinon.
//
// Entrée: const TCCoord & coord : coordonnée qui sera testée pour savoir 
//                                 si elle fait partie d'un des point.
//    
//
// Sortie:
//
// Notes: Évidemment, une coordonnée donnée "fait partie" seulement des points
//        se trouvant précisément au même endroit. Le container retourné peut
//        par exemple, servir à déplacer tous ces points.
//
//        Doit-on trouver au moins un propriétaire ? Doit on retourner une
//        liste de propriétaires ou le premier ?
//************************************************************************
ERMsg GOMultiPoint::reqProprietaire(TCContainer& proprietaires,
                                    const TCCoord& coord)
{
#ifdef MODE_DEBUG
   PRECONDITION(proprietaires.size()==0);  
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
        
   TCContainer::const_iterator itI;
   TCContainer::const_iterator finI= m_multiPoint.end();
   for (itI = m_multiPoint.begin(); itI != finI ; ++itI)
   {
      if ((*itI).possedeMemeCoordonnees(coord))
      {
         proprietaires.push_back(*itI);
      }
   }
    
#ifdef MODE_DEBUG
   POSTCONDITION(proprietaires.size()>0);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG     
   return msg;
}

//************************************************************************
// Sommaire:   Retourne le nombre de point du multipoint.
//
// Description:
//    La méthode publique <code>reqNbrPoint(...)</code> retourne le nombre
//    de point formant le multiPoint. 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//
//************************************************************************
EntierN GOMultiPoint::reqNbrPoints() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return static_cast<EntierN>(m_multiPoint.size());
}

//************************************************************************
// Sommaire:   Retourne l'itérateur de début du multipoint
//
// Description:
//    La méthode publique <code>reqDebut()</code> retourne un itérateur 
//    de point du multiPoint. L'itérateur est positionné au début du 
//    vecteur.
//  
// Entrée:
//    
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPoint::TCConstIterateur GOMultiPoint::reqDebut() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_multiPoint.begin();
}

//************************************************************************
// Sommaire:   Retourne l'itérateur de fin du multipoint
//
// Description:
//    La méthode publique <code>reqFin()</code> retourne un itérateur 
//    de point du multiPoint indiquant la fin. 
//  
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPoint::TCConstIterateur GOMultiPoint::reqFin() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_multiPoint.end();
}
