//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPolygone.cpp
// Classe : GOPolygone
//************************************************************************
// 18-02-2004  Maude Giasson          Version initiale
//************************************************************************
#include "GOPolygone.h"

#include "GOCoord3.h"
#include "GOMultiPoint.h"
#include "GOMultiPolygone.h"
#include "GOMultiPolyligne.h"
#include "GOGeometrie.h"
#include "GOGenerateurID.h"

#include "geos_sup.h"
#include <geos_c.h>
//#include <geos/geosAlgorithm.h>

#include <iterator>
#include <memory>
#include <vector>

//************************************************************************
// Sommaire:   Constructeur
//
// Description:
//    Constructeur par défaut.
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPolygone::GOPolygone()
   :  m_polylignes(), m_genIdP(&GOGenerateurID::generiqueGenID)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur
//
// Description:
//    Constructeur par copie;
//    
// Entrée:
//    const GOPolygone& polygone       Polygone à copier
//
// Sortie:
//
// Notes:
//************************************************************************
GOPolygone::GOPolygone(const GOPolygone& autre)
   : m_polylignes(autre.m_polylignes), m_genIdP(autre.m_genIdP)
{
   
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPolygone::~GOPolygone()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_polylignes.clear();
}

//************************************************************************
// Sommaire:   Opérateur d'affectation
//
// Description:
//    L'opérateur public <code>operator=(...)</code> est l'opérateur
//    d'affectation de la classe. L'objet est en tout point identique
//    au polygone passée en argument.
//
// Entrée:
//    const GOPolygone& polyogne;      Polygone à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPolygone& GOPolygone::operator= (const GOPolygone& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   m_polylignes = autre.m_polylignes;
   m_genIdP     = autre.m_genIdP;

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return(*this);
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode <code>asgGenerateurID(...)</code>
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOPolygone::asgGenerateurID(GOGenerateurIDP genIdP)
{
#ifdef MODE_DEBUG
   PRECONDITION(genIdP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   m_genIdP = genIdP;

         TCContainerPolylignes::iterator pgI    = m_polylignes.begin();
   const TCContainerPolylignes::iterator pgFinI = m_polylignes.end();
   for ( ; pgI != pgFinI; ++pgI)
   {
      (*pgI).asgGenerateurID(m_genIdP);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>reqGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
GOGenerateurIDP GOPolygone::reqGenerateurID()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_genIdP;
}

//************************************************************************
// Sommaire:  Ajoute les intersections avec un autre polygone
//
// Description:
//    La méthode <code>ajoutePointsCroisement(...)</code> ajoute au polygone
//    courant des sommets aux endroits où il rencontre le polygone passé en 
//    paramètre.
//
// Entrée:
//    const GOPolygone&       polygone qui servira à trouver les points de croisement
//                            où ajouter des sommets au polygone courant
//
// Sortie:
//
// Notes: 
//************************************************************************
ERMsg GOPolygone::ajoutePointsCroisement(const GOPolygone& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   TCContainerPolylignes::iterator pgCourantI    = m_polylignes.begin();
   TCContainerPolylignes::iterator pgCourantFinI = m_polylignes.end();
   for ( ; pgCourantI != pgCourantFinI; ++pgCourantI)
   {
      TCContainerPolylignes::const_iterator pgAutreI    = autre.m_polylignes.begin();
      TCContainerPolylignes::const_iterator pgAutreFinI = autre.m_polylignes.end(); 
      for ( ;pgAutreI != pgAutreFinI; ++pgAutreI)
      {
         pgCourantI->ajoutePointsIntersection(*pgAutreI);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute une polyligne au polygone.
//
// Description:
//    La méthode <code>ajoutePolyligne(...)</code> ajoute une polyligne
//    au polygone. Si c'est la première polyligne, il s'agit de la 
//    frontière extérieure du polygone, sinon, d'un de ses trou.
//
// Entrée:
//    const GOPolyligne & polyligne
//
// Sortie:
//
// Notes:
//        
//************************************************************************
ERMsg GOPolygone::ajoutePolyligne(const GOPolyligneFermee& polyligne)
{
#ifdef MODE_DEBUG
   PRECONDITION(polyligne.reqNbrSommets() > 1);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   
   m_polylignes.push_back(polyligne);
   m_polylignes.back().asgGenerateurID(m_genIdP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Translate le polygone.
//
// Description:
//    La fonction <code>deplace(...)</code> deplace le polygone selon le
//    vecteur de déplacement passé en paramètre.
//
// Entrée:
//    const TCCoord& vctDeplacement       Vecteur de déplacement
//
// Sortie:
//
// Notes:-Pour l'instant, le message d'erreur retourné est toujours OK
//       -On ne peut pas appeler une à une déplacePLdePG car ces dernières 
//        peuvent retourner des erreurs alors que le déplacement est globalement
//        bon.
//       -Venir mettre à jour le box de chaque pl du pg et du pg
//************************************************************************
ERMsg GOPolygone::deplace(const TCCoord& vctDeplacement)
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   
         TCContainerPolylignes::iterator plI    = m_polylignes.begin();
   const TCContainerPolylignes::iterator plFinI = m_polylignes.end();
   for ( ; plI != plFinI; ++plI)
   {
      (*plI).deplace(vctDeplacement);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Translate une polyligne du polygone
//
// Description: 
//    La fonction <code>deplacePL(...)</code> deplace une polyligne
//    du polygone.
//  
// Entrée:
//    const TCCoord& vctDeplacement    vecteur de déplacement
//    EntierN indPL                    indice de la polyligne à déplacer
//    
// Sortie:
//
// Notes:
//    - Pour l'instant, le message d'erreur retourné est toujours OK, mais
//        il DEVRA être modifié.
//    - venir mettre à jour le box de la pl du pg et du pg
//************************************************************************
ERMsg GOPolygone::deplacePL(const TCCoord& vctDeplacement,
                            EntierN indPL)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   //message d'erreur à faire :
   //1- si la polyligne est la frontière extérieure (indPL ==0) :
   //      -Les autres polylignes (frontières intérieures) doivent encore être
   //       dans la frontière extérieure après son déplacement et ne pas lui
   //       toucher
   // 2- si la polyligne est une frontière intérieure (indPL > 0) :
   //      -Après son déplacement, la polyligne doit toujours être dans la
   //       la frontière extérieure et ne pas lui toucher
   //      -Après son déplacement, la polyligne doit toucher à chacune des autre
   //       frontière extérieure en un point au plus.
   //      -Après le déplacement de la polyligne, le polygone doit toujours être 
   //       connexe.
   
   // ---  Trouve l'itérateur de l'indice
   TCContainerPolylignes::iterator polyI = m_polylignes.begin();
   std::advance(polyI, indPL);

   // ---  Demander à la polyligne de se déplacer
   polyI->deplace(vctDeplacement);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Translate un sommet d'une polyligne du polygone
//
// Description:
//    La fonction <code>deplacePTdePL(...)</code> deplace un sommet
//    d'une polyligne du polygone.
//  
// Entrée:
//    const TCCoord& vctDeplacement    vecteur de déplacement
//    const Entier& indPL              indice de la polyligne à altérer
//    const Entier& indPT              indice du sommet à déplacer dans la PL
//    
//
// Sortie:
//
// Notes:-Pour l'instant, le message d'erreur retourné est toujours OK, mais
//        il DEVRA être modifié.
//       -venir mettre à jour le box de la pl du pg et du pg
//************************************************************************
ERMsg GOPolygone::deplacePTdePL(const TCCoord& vctDeplacement, 
                                EntierN indPL,
                                EntierN indPT)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());
  // PRECONDITION(static_cast<EntierN>(indPT) < (*m_polygoneP)[indPL].size() - 1);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   //message d'erreur à faire :
   //1- si la polyligne est la frontière extérieure (indPL ==0) :
   //      -Les autres polylignes (frontières intérieures) doivent encore être
   //       dans la frontière extérieure après son déplacement et ne pas lui
   //       toucher
   // 2- si la polyligne est une frontière intérieure (indPL > 0) :
   //      -Après son déplacement, la polyligne doit toujours être dans la
   //       la frontière extérieure et ne pas lui toucher
   //      -Après son déplacement, la polyligne doit toucher à chacune des autre
   //       frontière extérieure en un point au plus.
   //      -Après le déplacement de la polyligne, le polygone doit toujours être 
   //       connexe.
  
   // ---  Demande à la polyligne de déplacer son point
   TCContainerPolylignes::iterator polyI = m_polylignes.begin();
   std::advance(polyI, indPL);
   polyI->deplace(vctDeplacement, indPT);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:
// 
// Description: La fonction <code>difference(...)</code>
//  
// Entrée: 
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOPolygone::difference(GOGeometrie& resultat, const GOPolygone& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   ERMsg msg = ERMsg::OK;

   // ---  Transforme en polygones GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::Polygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::Polygon(autre);

   // ---  Demande à GEOS d'effectuer la différence
   geos_sup::TTGeomUniquePtr g_resP(GEOSDifference(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(resultat, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//
// Description:
//  
// Entrée: 
//    
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOPolygone::intersecte(Booleen& reponse, const GOPolyligne& pl) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   ERMsg msg = ERMsg::OK;

   // --- Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::Polygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::LineString(pl);

   // --- Demande à GEOS d'effectuer l'intersection
   char r = GEOSIntersects(g_selfP.get(), g_autreP.get());
   if (r == 2) msg = geos_sup::reqMsgErreur();

   reponse = (r == 1);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//
// Description:
//    La fonction <code>intersection(...)</code>
//  
// Entrée: 
//    
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOPolygone::intersection(GOGeometrie& resultat, const GOPolygone&  autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   ERMsg msg = ERMsg::OK;

   // ---  Transforme les polygones en polygones GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::Polygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::Polygon(autre);
   
   // --- Demande à GEOS d'effectuer l'intersection
   geos_sup::TTGeomUniquePtr g_resP(GEOSIntersection(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(resultat, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Retourne la polyligne à l'indice demandé.
//
// Description:
//    La opérateur public <code>operator[](...)</code> retourne la polyligne à
//    l'indice demandé. La polyligne à l'indice 0 est la frontière extérieure du
//    polygone. Les polylignes aux indices suivants sont les contours des trous du
//    polygone.
//
// Entrée:
//    EntierN ind : l'indice de la polyligne voulue
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOPolyligneFermee& GOPolygone::operator [] (EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < m_polylignes.size());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCContainerPolylignes::const_iterator polyI = m_polylignes.begin();
   std::advance(polyI, ind);
   return (*polyI);
}

//************************************************************************
// Sommaire: 
//    Retourne un booleen indiquant si les deux GOPolygone sont identiques ou non.
//
// Description:
//    L' opérateur publique <code>operator==(...)</code> retourne true si les
//    deux GOPolyligne sont identique, false sinon.
//
// Entrée:
//    GOPolygone & polyogne; Polygone à  comparer avec l'objet courant.
//
// Sortie:
//
// Notes:
//    Retourne toujours VRAI actuellement!
//************************************************************************
Booleen GOPolygone::operator== (const GOPolygone& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCContainerPolylignes::const_iterator polyI     = m_polylignes.begin();
   TCContainerPolylignes::const_iterator polyFinI  = m_polylignes.end();

   TCContainerPolylignes::const_iterator autrePolyI    = autre.m_polylignes.begin();
   TCContainerPolylignes::const_iterator autrePolyFinI = autre.m_polylignes.begin();
   
   //TODO: retourne toujours vrai!
   //UTILISER GEOS OU FAIRE À LA MAIN À VOIR --> les ID des sommets doivent ils être
   //LES MÊMES POUR DIRE QUE C'EST ÉGAL ?
   return VRAI;
}

Booleen GOPolygone::operator!= (const GOPolygone& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   return !(*this == autre);
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur constant sur les polylignes formant le polygone.
//
// Description:
//    La méthode publique <code>reqDebut(...)</code> retourne un itérateur
//    permettant de parcourir les diverses polyligne du polygone. 
//
// Entrée:
//
// Sortie: GOConstIterPolygone un itérateur sur les polylignes d'un polygone
//
// Notes:
//
//************************************************************************
GOPolygone::TCConstIterateur GOPolygone::reqDebut() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_polylignes.begin();
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur constant sur les polylignes formant le polygone.
//
// Description:
//    La méthode publique <code>reqFin(...)</code> retourne un itérateur
//    indiquant la fin du container de ligne d'un polygone 
//
// Entrée:
//
// Sortie:
//    GOConstIterPolygone un itérateur sur les polylignes d'un polygone
//
// Notes:
//
//************************************************************************
GOPolygone::TCConstIterateur GOPolygone::reqFin() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_polylignes.end();
}

//************************************************************************
// Sommaire: 
//    Retourne le nombre de polylignes formant le polygone.
//
// Description:
//    La méthode public <code>reqNbrPolylignes(...)</code> retourne le nombre
//    de polylignes formant le polygone. 
//
// Entrée:
//    
//
// Sortie:
//
// Notes: 
//
//************************************************************************
EntierN GOPolygone::reqNbrPolylignes() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   return static_cast<EntierN>(m_polylignes.size());
}
//************************************************************************
// Sommaire: 
//    Retourne le nombre de sommet du polygone.
//
// Description:
//    La méthode public <code>reqNbrSommets(...)</code> retourne le nombre
//    de sommet formant le polygone. Les sommets formants les polylignes fermées
//    du polygone ne sont calculés qu'une fois chacun. Les sommets de la frontière
//    extérieure et des frontières intérieures sont calculés.
//
// Entrée:
//    
// Sortie:
//
// Notes:
//
//************************************************************************
EntierN GOPolygone::reqNbrSommets() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
   
   GOPolygone::TCConstIterateur pGI = reqDebut();
   GOPolygone::TCConstIterateur finPGI = reqFin();

   EntierN cpt = 0;
   for ( ;pGI != finPGI; ++pGI)
   {
      cpt += pGI->reqNbrSommets()-1;
   }

 #ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
 #endif  // ifdef MODE_DEBUG  
   return cpt;
}

//************************************************************************
// Sommaire:   Rendre les polylignes fermées dans le sens anti-horaire.
//
// Description:
//    La méthode publique <code>soisAntiHoraire(...)</code> oblige à toutes
//    les polylignes fermées du polygone à s'orienter dans le sens anti-horaire.  
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg GOPolygone::soisAntiHoraire()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   ERMsg msg = ERMsg::OK;

   TCIterateur polyligneI    = m_polylignes.begin();
   TCIterateur polyligneFinI = m_polylignes.end();
   
   for( ; polyligneI != polyligneFinI && msg; ++polyligneI)
   {
      msg = polyligneI->soisAntiHoraire();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return msg;
}

//************************************************************************
// Sommaire: 
//    Retourne true si le point passé en paramètre est dans le polygone, false sinon.
//
// Description:
//    La méthode public <code>pointInterieur(...)</code> sert à savoir si un point
//    se trouve à l'intérieur du polygone. Elle retourne true si la coordonnée passée
//    en paramètre fait partie du polygone, false sinon.
//
// Entrée: const TCCoord & coord : coordonnée qui sera testée pour savoir 
//                                 si elle fait partie du polygone.
//    
//
// Sortie:
//
// Notes: Venir gérer ERMsg; remplace Booleen par un msg
//
//************************************************************************
Booleen GOPolygone::pointInterieur(const TCCoord& coord) const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG

   // ---  Construis le polygone GEOS
   geos_sup::TTGeomUniquePtr g_selfP  = geos_sup::Polygon(*this);
   geos_sup::TTGeomUniquePtr g_autreP = geos_sup::Point  (coord);
   
   // ---  Test
   char r = GEOSIntersects(g_autreP.get(), g_selfP.get());
   if (r == 2) throw geos_sup::reqMsgErreur();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG 
   return (r == 1);
}

//************************************************************************
// Sommaire:   Retourne true si le point est un de ses sommets.
//
// Description:
//    La méthode public <code>estUnSommet(...)</code> sert à savoir
//    si un point est l'un des sommets du polygone.  Elle retourne true si
//    le point passée en paramètre est sur l'un des sommets, false sinon.
//
// Entrée:
//    const GOPoint & point : point qui sera testé pour savoir 
//                            s'il est sur un des sommets.
//    const DReel& tolerance :      degré de tolérance
//
// Sortie:
//    EntierN& ID           : l'ID du point trouvé s'il a lieu
//
// Notes:
//
//************************************************************************
Booleen GOPolygone::estUnSommet(EntierN& ID, const GOPoint& point, const DReel& tolerance) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCConstIterateur polyligneI    = m_polylignes.begin();
   TCConstIterateur polyligneFinI = m_polylignes.end();
   Booleen reponse = false;
   
   for( ; polyligneI != polyligneFinI && !reponse; ++polyligneI)
   {
      reponse = polyligneI->estUnSommet(ID, point, tolerance);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return reponse;
}

//************************************************************************
// Sommaire: 
//    Supprime une polyligne du polygone
//
// Description:
//    La méthode public <code>supprimePL(...)</code> supprime une polyligne
//    du polygone. Ce ne doit pas être la première (indice 0 ).
//
// Entrée: const Entier& indice : indice de la polyligne à supprimer
//    
//
// Sortie:
//
// Notes: - Ne doit pas être la première polyligne du polygone
//
//************************************************************************
ERMsg GOPolygone::supprimePL(EntierN indPL)
{
#ifdef MODE_DEBUG
   EntierN nbrPLAvant = reqNbrPolylignes();
   PRECONDITION(indPL < reqNbrPolylignes());
   PRECONDITION(indPL >= 1);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   ERMsg msg = ERMsg::OK;

   TCContainerPolylignes::iterator polyI = m_polylignes.begin();
   std::advance(polyI, indPL);
   
   m_polylignes.erase(polyI);

#ifdef MODE_DEBUG
   POSTCONDITION( reqNbrPolylignes() == (nbrPLAvant - 1) );
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Supprime un point d'une polyligne du polygone
//
// Description:
//    La méthode public <code>supprimePTdePL(...)</code> supprime un point
//    d'une polyligne du polygone.
//
// Entrée: const Entier& indPL : indice de la polyligne à supprimer
//         const Entier& indPT : indice du PT à supprimer dans la PL    
//
// Sortie:
//
// Notes: - La polyligne doit demeurer une ring
//             - elle doit avoir au départ plus de 3 sommets de qui veut dire
//               que le nombre de sommet doit être plus grand que 4 car le premier
//               est toujours calculé en double.
//             - si le sommet à retirer est le premier ou le dernier, on
//               retire le premier ET le dernier et referme le tout en ajoutant
//               le "nouveau premier" à la fin.
//        - La cohérence du polygone n'est pas assurée. C'est toutefois un 
//          premier effort dans ce sens - nécessaire pour pouvoir enlever le premier
//          ou dernier sommet de manière cohérente.
//
//************************************************************************
ERMsg GOPolygone::supprimePTdePL(EntierN indPL, EntierN indPT)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());
//   PRECONDITION((*m_polygoneP)[indicePL].size()>4);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   TCContainerPolylignes::iterator polyI = m_polylignes.begin();
   std::advance(polyI, indPL);
   ASSERTION((*polyI).reqNbrSommets() > 4);
   
   // ---  S'il s'agit d'une extrémité, refermer le cercle
   EntierN indDernierSommet = polyI->reqNbrSommets() - 1;
   if (indPT == 0 || indPT == indDernierSommet)
   {
      polyI->supprime(indDernierSommet);
      polyI->supprime(0);
      //Trouver le nouveau premier sommet et le rajouter à la fin
      polyI->ajouteSommet((*polyI)[0]);
   }
   
   //s'il ne s'agit pas d'une extrémitée, enlever simplement le sommet
   else
   {
      polyI->supprime(indPT);
   }

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

/*
void GOPolygone::estValide() const
{
  //-toutes les polylignes ont 4 sommets ou plus
   //-toutes les polylignes sont fermées
   // toutes les polylignes sont simples
   //-les polylignes de 1 à nbrePoly -1 sont toutes dans la polyligne 0 sans lui
   // toucher
   //-les polylignes de 1 à nbrPoly - 1 se touchent 2 à 2 en un pt au plus
   //- les polylignes sont simples
   // - le polygone est connexe
   // size() est plus petit que la val max d'un entier (et non d'un entierN)
}
*/

