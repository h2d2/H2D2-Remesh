//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOMultiPolyligne.cpp
// Classe : GOMultiPolyligne
//************************************************************************
// 13-02-2004  Maude Giasson          Version initiale
//************************************************************************
#include "GOMultiPolyligne.h"

#include "GOGenerateurID.h"
#include "GOPolyligne.h"

//************************************************************************
// Sommaire:  
//    Constructeur
//
// Description:
//    Constructeur par défaut
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolyligne::GOMultiPolyligne()
   : m_multiPolyligne(), m_genIdP(NUL), m_proprioGenId(FAUX)
{
   m_genIdP = new GOGenerateurID();
   m_proprioGenId = VRAI;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur de copie
//
// Description:
//    Constructeur de copie. 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolyligne::GOMultiPolyligne(const GOMultiPolyligne& autre)
   :  m_multiPolyligne()
   ,  m_genIdP(NUL)
   ,  m_proprioGenId(FAUX)
{
   m_genIdP = new GOGenerateurID();
   m_proprioGenId = VRAI;

   //-- Remplir le vecteur
   m_multiPolyligne.reserve(autre.m_multiPolyligne.size());
   for (TCContainer::const_iterator polyI = autre.m_multiPolyligne.begin();
      polyI != autre.m_multiPolyligne.end();
      ++polyI)
   {
      GOPolyligneP nouvelleP = new GOPolyligne(*(*polyI));
      nouvelleP->asgGenerateurID(m_genIdP);
      m_multiPolyligne.push_back(nouvelleP);
   }
   
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut, detruit toutes le polylignes du container.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolyligne::~GOMultiPolyligne()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   for (TCContainer::iterator polyI = m_multiPolyligne.begin();
        polyI != m_multiPolyligne.end();
        ++polyI)
   {
      delete (*polyI);
   }

   if (m_proprioGenId) delete m_genIdP;
   m_genIdP = NUL;
}

//************************************************************************
// Sommaire:   Opérateur d'affectation
//
// Description:
//    L'opérateur publique <code>operator=(...)</code> permet de copier le 
//    multipolyligne passé en paramètre dans l'objet courant.
//
// Entrée:
//    GOMultiPolyligne & autre;  à  copier dans l'objet courant.
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolyligne& GOMultiPolyligne::operator=  (const GOMultiPolyligne& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (this != &autre)
   {
      //-- 1. Vider le vecteur
      for (TCContainer::iterator polyI = m_multiPolyligne.begin();
           polyI != m_multiPolyligne.end();
           ++polyI)
      {
         delete (*polyI);
      }
      m_multiPolyligne.clear();

      //-- 2. Supprimer le générateur d'ID s'il était propriétaire et 
      //--    le faire pointer vers celui de l'autre
      if (m_proprioGenId) delete m_genIdP;
      m_genIdP         = new GOGenerateurID();
      m_proprioGenId   = VRAI;

      //-- 3. Remplir le vecteur
      m_multiPolyligne.reserve(autre.m_multiPolyligne.size());
      for (TCContainer::const_iterator polyI = autre.m_multiPolyligne.begin();
           polyI != autre.m_multiPolyligne.end();
           ++polyI)
      {
         GOPolyligneP nouvelleP = new GOPolyligne(*(*polyI));
         nouvelleP->asgGenerateurID(m_genIdP);
         m_multiPolyligne.push_back(nouvelleP);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}  

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>asgGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
void GOMultiPolyligne::asgGenerateurID(GOGenerateurIDP genIdP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (m_proprioGenId)
      delete m_genIdP;
   m_genIdP = genIdP;
   m_proprioGenId = FAUX;

   for (TCContainer::iterator polyI = m_multiPolyligne.begin();
        polyI != m_multiPolyligne.end();
        ++polyI)
   {
      (*polyI)->asgGenerateurID(m_genIdP);
   }
 
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>reqGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
GOGenerateurIDP GOMultiPolyligne::reqGenerateurID()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_genIdP;
}

//************************************************************************
// Sommaire:  Ajoute une ligne à la multiPolyligne
//
// Description: 
//    La fonction <code>ajoutePolyligne(...)</code> ajoute la polyligne
//    passée en paramètre à l'objet courant.
//
// Entrée:
//    GOPolyligne & polyligne : polyligne à ajouter
//
// Sortie:
//
// Notes: 
//************************************************************************
ERMsg GOMultiPolyligne::ajoutePolyligne(const GOPolyligne& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   GOPolyligneP nouvelleP = new GOPolyligne(autre);
   nouvelleP->asgGenerateurID(m_genIdP);
   m_multiPolyligne.push_back(nouvelleP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute un sommet à une polyligne du multiPolyligne
//
// Description:
//    La méthode publique <code>ajoutePTdePL(...)</code> un point à la
//    polyligne dont l'indice est passé en paramètre.
//
// Entrée:
//    Entier indPL               indice de la polyligne à modifier
//    Entier indPT               indice du point dans la polyligne, 
//    const TCSommet& coord      coordonnée du point à ajouter   
//
// Sortie: 
//
// Notes:  modifier au besoin pour qu'il y aie retour d'un ERMsg
//************************************************************************
ERMsg GOMultiPolyligne::ajoutePTdePL(EntierN indPL, 
                                     EntierN indPT, 
                                     const TCSommet& point)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   msg = m_multiPolyligne[indPL]->ajouteSommet(point, indPT);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Deplace la polyligne dont l'indice est passé en paramètre
//
// Description:
//    La méthode publique <code>deplacePL(...)</code> deplace la 
//    la polyligne dont l'indice est passé en paramètre. 
//  
// Entrée:
//    const TCCoord&          Vecteur de deplacement
//    EntierN ind             Indice de la polyligne à deplacer
//
// Sortie:
//
// Notes:-Pour l'instant, le message d'erreur retourné est toujours OK
//************************************************************************
ERMsg GOMultiPolyligne::deplacePL(const TCCoord& vctDeplacement,
                                  EntierN indPL)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   msg = m_multiPolyligne[indPL]->deplace(vctDeplacement);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Deplace un point d'une polyligne.
//
// Description:
//    La méthode publique <code>deplacePTdePL(...)</code> deplace le sommet
//    d'indice donnée de la polyligne d'indice donné. 
//  
// Entrée:
//    const TCCoord& vctDeplacement    Vecteur de déplacement
//    Entier indPL                     Indice de la polyligne
//    Entier indPT                     Indice du point de la polyligne
//
// Sortie:
//
// Notes:  -Pour l'instant, le message d'erreur retourné est toujours OK
//************************************************************************
ERMsg GOMultiPolyligne::deplacePTdePL(const TCCoord& vctDeplacement, 
                                      EntierN indPL, 
                                      EntierN indPT)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   msg = m_multiPolyligne[indPL]->deplace(vctDeplacement, indPT);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Retourne la polyligne à l'indice demandé.
//
// Description:
//    L' opérateur public <code>operator[](...)</code> retourne la polyligne à
//    l'indice demandé. 
//
// Entrée:
//    EntierN ind : l'indice de la polyligne voulue
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOPolyligne& GOMultiPolyligne::operator[](EntierN indice) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return (*m_multiPolyligne[indice]);
}

//************************************************************************
// Sommaire: 
//    Retourne true si le point passé en paramètre est sur une des polyligne, 
//    false sinon.
//
// Description:
//    La méthode publique <code>pointInterieur(...)</code> sert à savoir si 
//    un point se trouve à l'intérieur d'une des polyligne du multipolyligne. 
//    Elle retourne true si la coordonnée passée en paramètre est sur 
//    une des polylignes, false sinon.
//
// Entrée: const TCCoord & coord : coordonnée qui sera testée pour savoir 
//                                 si elle est sur une des polyligne.
//    
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOMultiPolyligne::pointInterieur(const TCCoord& coord) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCContainer::const_iterator itI  = m_multiPolyligne.begin();
   TCContainer::const_iterator finI = m_multiPolyligne.end();
   Booleen reponse = false;
     
   for ( ; itI != finI && !reponse; ++itI)
   {
      reponse = (*itI)->pointInterieur(coord);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return reponse;
}

//************************************************************************
// Sommaire:   Retourne true si le point est un de ses sommets.
//
// Description:
//    La méthode public <code>estUnSommet(...)</code> sert à savoir
//    si un point est l'un des sommets du multipolyligne.  Elle retourne true si
//    le point passée en paramètre est sur l'un des sommets, false sinon.
//
// Entrée:
//    const GOPoint & point : point qui sera testé pour savoir 
//                            s'il est sur un des sommets.
//    const DReel& tolerance :      degré de tolérance
//
// Sortie:
//    EntierN& ID           : l'ID du point trouvé s'il a lieu
//
// Notes:
//
//************************************************************************
Booleen GOMultiPolyligne::estUnSommet(EntierN& ID, const GOPoint& point, const DReel& tolerance) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   TCConstIterateur polyligneI    = m_multiPolyligne.begin();
   TCConstIterateur polyligneFinI = m_multiPolyligne.end();
   Booleen reponse = false;
   
   for( ; polyligneI != polyligneFinI && !reponse; ++polyligneI)
   {
      reponse = polyligneI->estUnSommet(ID, point, tolerance);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
    return reponse;
}

//************************************************************************
// Sommaire:   Retire une polyligne.
//
// Description:
//    La méthode publique <code>retirePL(...)</code> retire une polyligne du
//    GOMulitPolyligne. La polyligne retirée est à l'indice ind.
//
// Entrée:
//    const Entier & ind indice de la polyligne à retirer.
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOMultiPolyligne::retirePL(EntierN indPL)
{
#ifdef MODE_DEBUG
   EntierN nbrPlAvant = reqNbrPolylignes();
   PRECONDITION(indPL < reqNbrPolylignes());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   
   delete m_multiPolyligne[indPL];
   m_multiPolyligne.erase(m_multiPolyligne.begin() + indPL);

#ifdef MODE_DEBUG
   POSTCONDITION(reqNbrPolylignes() == (nbrPlAvant - 1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Retire un point à une des polylignes
//
// Description:
//    La méthode publique <code>retirePTdePL(...)</code> retire un sommet à une
//    des polyligne du vecteur de polyligne
//
// Entrée:
//    Entier indPL            Indice de la polyligne
//    Entier indPT            Indice du pt à retirer dans la PL.
//
// Sortie:
//
// Notes:
//    Que se passe-t-il si la polyligne est vide?? Est-ce possible?
//************************************************************************
ERMsg GOMultiPolyligne::retirePTdePL(EntierN indPL, EntierN indPT)
{
#ifdef MODE_DEBUG
   PRECONDITION(indPL < reqNbrPolylignes());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   m_multiPolyligne[indPL]->supprime(indPT);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//    Sert à trouver quelles polylignes contiennet la coordonnée passée en 
//    paramètre. 
//
// Description:
//    La méthode publique <code>reqProprietaires(...)</code> sert à savoir 
//    quelles polylignes passent sur un point donné. Elle retourne un container
//    contenant les propriétaires du point.
//
// Entrée: const TCCoord & coord : coordonnée qui sera testée pour savoir 
//                                 si elle est sur une des polyligne.
//         TCContainer &proprietaire : vide au départ, contiendra les 
//                                     propriétaires du point.
// Sortie:
//
// Notes: Revoir les préconditions et post. Doit on avoir au moins un
//        proprietaire ?  
//
//************************************************************************
/*
ERMsg GOMultiPolyligne::reqProprietaire(TCContainer & proprietaires, const TCCoord & coord)
{
 #ifdef MODE_DEBUG
   PRECONDITION(proprietaires.size()==0);  
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG
     ERMsg msg = ERMsg::OK;
        
     std::vector<GOPolyligneP>::const_iterator itI;
     std::vector<GOPolyligneP>::const_iterator finI= m_multiPolyligne.end();
     for(itI = m_multiPolyligne.begin(); itI != finI ; ++itI)
     {
         if((*itI)->pointInterieur(coord))
         {
            proprietaires.push_back(*itI);
         }
     }
    
 #ifdef MODE_DEBUG
     POSTCONDITION(proprietaires.size()>0);
     INVARIANTS("POSTCONDITION");
 #endif  // ifdef MODE_DEBUG
     return msg;
}
*/

//************************************************************************
// Sommaire: 
//    Retourne le nombre de polyligne formant le multipolyligne.
//
// Description:
//    La méthode publique <code>reqNbrPolylignes(...)</code> retourne le nombre
//    de polyligne formant le multiPolyligne. 
//
// Entrée:
//    
//
// Sortie:
//
// Notes: 
//
//************************************************************************
EntierN GOMultiPolyligne::reqNbrPolylignes() const
{
 #ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
 #endif  // ifdef MODE_DEBUG

   return static_cast<EntierN>(m_multiPolyligne.size());
}

//************************************************************************
// Sommaire:   Retourne le nombre de sommets du multipolygone.
//
// Description:
//    La méthode publique <code>reqNbrSommets(...)</code> retourne le nombre
//    de sommets du multiPolygone.
//
// Entrée:
//    
// Sortie:
//
// Notes: 
//
//************************************************************************
EntierN GOMultiPolyligne::reqNbrSommets() const
{
#ifdef MODE_DEBUG
  INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN nbrSommets = 0;
         TCConstIterateur pI   = reqDebut();
   const TCConstIterateur finI = reqFin();
   while (pI != finI)
   {
      nbrSommets += (*pI).reqNbrSommets();
      ++pI;
   }

#ifdef MODE_DEBUG
  INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return nbrSommets;
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de polyligne du multipolyligne
//
// Description:
//    La méthode publique <code>reqVnoDebut()</code> retourne un itérateur 
//    de polyligne du multiPolyligne. L'itérateur est positionné au début du 
//    vecteur.
//  
// Entrée:
//    
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolyligne::TCConstIterateur 
GOMultiPolyligne::reqDebut() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_multiPolyligne.begin();
}

//************************************************************************
// Sommaire: 
//    Retourne un itérateur de polyligne du multiPolyligne
//
// Description:
//    La méthode publique <code>reqPolyligneFin()</code> retourne un itérateur 
//    de polyligne du multiPolyligne indiquant la fin. 
//  
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOMultiPolyligne::TCConstIterateur 
GOMultiPolyligne::reqFin()const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_multiPolyligne.end();
}


/*
GOMultiPolyligne& GOMultiPolyligne::operator=       (const TCSelf&)
{
   GOMultiPolyligne multiPolyligne;
   for(int i = 0 ; i < reqNbrPolylignes() ; i++)
   {
      multiPolyligne.ajoutePolyligne(*(m_multiPolyligne[i]));
   }
   return multiPolyligne;
}
*/




