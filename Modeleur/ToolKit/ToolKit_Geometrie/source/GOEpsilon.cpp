//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier:  GOEpsilon.cpp
// Classe:   GOEpsilon
//************************************************************************
// 28-10-1996  Eric Chamberland   Version initiale
// 29-10-1996  Eric Chamberland   Correction pour comparaisons entre nombre qui
//                                changent de signe dans compareInferieur
// 07-05-1997  Yves Roy           Correction pour rendre déterministe les comparaisons
//                                entre deux nombre.
// 16-10-1997  Yves Secretan      Passage à la double précision
// 07-11-1997  Yves Roy           Fusion de syftnnum et sydreel...
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#include "GOEpsilon.h"

#include <math.h>
#include <stdlib.h>

//************************************************************************
// Notes:
// Il est important de faire attention à la façon d'utiliser l'erreur, car
// si on l'augmente trop, on doit être sûr d'utiliser les fonctions de comparaison
// à bon escient.  Par exemple, si quelqu'un utilise ces fonctions dans les
// GOCoordonneesXYZ (ce qui est le cas actuellement) lors de l'assemblage de deux
// maillages (ou simplement avec un MAP ou un SET), mais que cette mème personne n'as pas
// utilisé ces fonctions de comparaisons (ou plutôt l'objet GOCoordonneesXYZ) lors de
// la construction du maillage, et bien il se peut qu'il y ait des points qui, dû
// au choix du système de transformation, soient différents mais devraient ètre égaux.
// Pour contrevenir à un tel phénomène, cette mème personne devraient TOUJOURS
// utiliser l'objet GOCoordonneesXYZ pour savoir si 2 points sont égaux ou non.
// Pour donner un exemple concret de problème, imaginons que nous générions un maillage
// en T3 avec des points séparés par EPSILON selon X.  Si nous calculons des points
// milieux sur les T3 pour en faire un T6, et bien nous nous retrouvons avec une erreur
// sur les 3 points côtés telle que: a == b, b == c mais a != c.
//                                          Eric Chamberland   08-11-96.
//************************************************************************

const double GOEpsilon::DOUBLE_EPSILON =  1.0e-15;
const double GOEpsilon::FLOAT_EPSILON  =  1.0e-6;



//************************************************************************
// Sommaire:     Opérateur ==
//
// Description:  L'opérateur == fait la comparaison en fixant une précision
//               qui est légèrement plus restrictive que la comparaison bit à bit.
//               Retourne si les deux double sont égaux selon une erreur
//               sur la mantisse passée en paramètre.  Si
//               l'une des deux valeur vaut exactement 0, on doit comparer l'autre
//               directement avec epsilon.
//
// Entrée:       const GOEpsilon& obj : un objet du même type
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
bool GOEpsilon::operator==(const GOEpsilon& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   double mantisseX, mantisseY;
   int    exposantX, exposantY;

   // --- Si l'epsilon est plus petit que notre borne de référence,
   // --- on retourne le résultat de la comparaison
   if (epsilon < DOUBLE_EPSILON)
   {
      return valeur == obj.valeur;
   }

   // --- On prends l'exposant et la mantisse de chacun des double
   mantisseX = frexp(valeur,&exposantX);
   mantisseY = frexp(obj.valeur,&exposantY);
   if (abs(exposantX - exposantY) <= 1)
   {
      int differenceExp = exposantY - exposantX ;

      if (differenceExp != 0)
      {
         if (differenceExp == 1)
         {
            mantisseY *= 2;
         }
         else
         {
            mantisseX *= 2;
         }
      }
      if (epsilon < fabs(mantisseX - mantisseY))
      {
         return false;
      }
   }
   else
   {
      // --- Ici on doit d'abord vérifier s'il n'y aurait pas un des deux double
      // --- qui vaut exactement 0, auquel cas, on compare l'autre
      // --- avec epsilon.
      if (mantisseX == double(0.0))
      {
         if (epsilon >= fabs(obj.valeur))
         {
            return true;
         }
      }
      else if (mantisseY == double(0.0))
      {
         if (epsilon >= fabs(valeur))
         {
            return true;
         }
      }

      return false;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return true;
}

//************************************************************************
// Sommaire:     Opérateur <
//
// Description:  L'opérateur < fait la comparaison en appelant la fonction
//               compareInferieur(const double&, const double&).  Cette
//               fonction fixe une précision qui est légèrement plus restrictive
//               que la comparaison bit à bit.
//
//               Retourne true si le premier flaot est plus petit que le 2e selon
//               une erreur sur la mantisse définie par EPSILON_SUR_FLOAT.  Si
//               l'une des deux valeur vaut exactement 0, on doit comparer l'autre
//               directement avec le epsilon.
//
// Entrée:       const GOEpsilon& obj : un objet du même type
//
// Sortie:       Retourne true si le test réussi et false autrement
//
// Notes:
//
//************************************************************************
bool GOEpsilon::operator<(const GOEpsilon& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   double mantisseX, mantisseY;
   int    exposantX, exposantY;

   // --- Si l'epsilon est plus petit que notre borne de référence,
   // --- on retourne le résultat de la comparaison
   if (epsilon < DOUBLE_EPSILON)
   {
      return valeur < obj.valeur;
   }
   // --- On prends l'exposant et la mantisse de chacun des double
   mantisseX = frexp(valeur,&exposantX);
   mantisseY = frexp(obj.valeur,&exposantY);
   if (abs(exposantX - exposantY) <= 1)
   {
      int differenceExp = exposantY - exposantX ;

      if (differenceExp != 0)
      {
         if (differenceExp == 1)
         {
            mantisseY *= 2;
         }
         else
         {
            mantisseX *= 2;
         }
      }
      if (epsilon < fabs(mantisseX - mantisseY))
      {
         return (mantisseX < mantisseY);
      }
   }
   else
   {
      // --- Ici on doit d'abord vérifier si'il n'y aurait pas un des deux double
      // --- qui vaut exactement 0, auquel cas, on compare l'autre
      // --- avec epsilon.
      if (mantisseX == double(0.0))
      {
         if (epsilon < fabs(obj.valeur))
         {
            return mantisseX < mantisseY;
         }
         else
         {
            return false;
         }
      }
      else if (mantisseY == double(0.0))
      {
         if (epsilon < fabs(valeur))
         {
            return mantisseX < mantisseY;
         }
         else
         {
            return false;
         }
      }
      // --- Ici on retourne simplement x < y car la différence entre les
      // --- exposants est suffisamment grande pour une comparaison des nombres
      return (valeur < obj.valeur);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return false;
}


