//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPoint.cpp
// Classe : GOPoint
//************************************************************************
// 10-03-2004  Maude Giasson          Version initiale
//************************************************************************
#include "GOPoint.h"

#include "geos_sup.h"
#include <geos_c.h>

DReel GOPoint::tolerance = 0.0;

//************************************************************************
// Sommaire:   Constructeur
//
// Description:
//    Constructeur par défaut. 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPoint::GOPoint(EntierN id)
   :  m_coord()
   ,  m_id(id)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur
//
// Description:
//    Constructeur par défaut. 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPoint::GOPoint(const TCCoord& p, EntierN id)
   :  m_coord(p)
   ,  m_id(id)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur de copie
//
// Description:
//    Constructeur de copie. 
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPoint::GOPoint(const GOPoint &autre)
   :  m_coord(autre.m_coord)
   ,  m_id(autre.m_id)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut. Libère la mémoire allouée pour m_pointP
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPoint::~GOPoint()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Opérateur d'affectation
//
// Description:
//    L'opérateur publique <code>operator=(...)</code> permet de copier le 
//    point passé en paramètre dans l'objet courant.
//
// Entrée:
//    GOPolygone & point; Polygone à  copier dans l'objet courant.
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPoint& GOPoint::operator =(const GOPoint& autre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   m_coord  = autre.m_coord;
   m_id = autre.m_id;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  Modifie les coordonnée du point 
//
// Description: La méthode <code>modifie(...)</code> modifie les coordonnées
//              du point
//
// Entrée: const TCCoord & coord Nouvelle coordonnées du point.
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOPoint::asgPosition (const TCCoord& coord)
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

    m_coord = coord;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Assigne la valeur portée par le point
//
// Description:
//    La méthode <code>asgValeur(...)</code>
//
// Entrée:
//    const TCCoord & coord Nouvelle coordonnées du point.
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOPoint::asgID (EntierN id)
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

    m_id = id;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode <code>deplace(...)</code> deplace le point à la
//    nouvelle coordonnée.
//
// Entrée:
//    const TCCoord & coord Nouvelle coordonnées du point.
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOPoint::deplace (const TCCoord& coord)
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

    m_coord += coord;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode <code>modifie(...)</code> modifie les coordonnées
//    du point
//
// Entrée:
//    const TCCoord & coord Nouvelle coordonnées du point.
//
// Sortie:
//
// Notes:
//
//************************************************************************
DReel GOPoint::distance(const GOPoint& point) const
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   double r;
   geos_sup::TTGeomUniquePtr self  = geos_sup::Point(*this);
   geos_sup::TTGeomUniquePtr autre = geos_sup::Point(point);
   GEOSDistance(self.get(), autre.get(), &r);
   return r;
}

//************************************************************************
// Sommaire: 
//    Retourne true si le point passé se retrouve sur le point,
//    false sinon.
//
// Description:
//    La méthode publique <code>possedeMemeCoordonnees(...)</code> sert à savoir si 
//    deux points ont les memes coordonnées. Elle retourne true si le point
//    passé en paramètre correspond à l'endroit où se situe le point, 
//    false sinon.
//
// Entrée:
//    const GOPoint & point :       point qui sera testée pour savoir 
//                                  s'il a les mêmes coordonnées.
//    const DReel& tolerance :      degré de tolérance
//
// Sortie: 
//    Booleen valant true si le point a les mêmes coordonnées, false sinon.
//
// Notes:
//    Facteur de tolérance!!!
//************************************************************************
Booleen GOPoint::possedeMemeCoordonnees (const GOPoint& point, const DReel& tolerance) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   geos_sup::TTGeomUniquePtr self  = geos_sup::Point(*this);
   geos_sup::TTGeomUniquePtr autre = geos_sup::Point(point);
   return GEOSEqualsExact(self.get(), autre.get(), tolerance);
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode <code>modifie(...)</code> modifie les coordonnées
//              du point
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
const GOPoint::TCCoord& GOPoint::reqPosition() const
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

    return m_coord;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode <code>modifie(...)</code> modifie les coordonnées
//              du point
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
EntierN GOPoint::reqID() const
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

    return m_id;
}

//************************************************************************
// Sommaire: 
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOPoint::operator == (const GOPoint& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
 
    return ((m_coord == p.m_coord) && (m_id == p.m_id));
}
Booleen GOPoint::operator != (const GOPoint& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
 
    return !(*this == p);
}


