//************************************************************************
// $Id$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOEnveloppe.cpp
// Classe : GOEnveloppe
//          et template SREnveloppe spécialisé pour les GOCoordonneesXYZ
//************************************************************************
#include "GOEnveloppe.h"

//************************************************************************
// Sommaire:  Constructeur par défaut.
//
// Description:
//    Le constructeur par défaut <code>GOEnveloppe()</code> initialise
//    l'objet comme une envelopppe de taille nulle.
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOEnveloppe::GOEnveloppe()
   : m_min()
   , m_max()
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  Constructeur copie
//
// Description: 
//    Le constructeur copie de la classe.
//
// Entrée:
//    const GOEnveloppe& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOEnveloppe::GOEnveloppe(const GOEnveloppe& obj)
   : m_min(obj.m_min)
   , m_max(obj.m_max)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Destructeur par défaut.
//
// Description: 
//    Destructeur par défaut, ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOEnveloppe::~GOEnveloppe()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:  
//    Opérateur d'assignation
//
// Description: 
//    Opérateur d'assignation de la classe.
//
// Entrée:
//    const GOEnveloppe& obj: un objet du même type
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOEnveloppe& GOEnveloppe::operator=(const GOEnveloppe& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (*this != obj)
   {
      m_min = obj.m_min;
      m_max = obj.m_max;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return *this;
}

//************************************************************************
// Sommaire:  
//    Fonction publique asgLimites
//
// Description: 
//    La fonction publique <code>asgLimites(...)</code> assigne les coordonnées
//    minimales et maximales de l'enveloppe.
//
// Entrée:
//    const GOCoordonneesXYZ& min, 
//    const GOCoordonneesXYZ& max
//
// Sortie:
//
// Notes:
//    TODO: éliminer? l'enveloppe est recalculée. Cette méthode est 
//    utile seul. pour les tests indépendants?
//************************************************************************
ERMsg GOEnveloppe::asgLimites(const GOCoordonneesXYZ& min, 
                              const GOCoordonneesXYZ& max)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;
   if (min < max)
   {
      m_min = min;
      m_max = max;
   }
   else
   {
      msg = ERMsg(ERMsg::ERREUR, "ERR_LIMITES_ENVELOPPE_INVALIDE");
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Teste si l'enveloppe est valide.
//
// Description: 
//    La méthode publique <code>estValide(...)</code> retourne VRAI si
//    l'enveloppe est valide, si elle a été initialisée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOEnveloppe::estValide() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return (m_min < m_max);
}

//************************************************************************
// Sommaire:  Teste si un point est à l'intérieur de l'enveloppe.
//
// Description: 
//    La méthode publique <code>pointInterieur(...)</code> détermine si
//    le point passé en argument se trouve à l'intérieur de l'enveloppe.
//    Si le point est sur le contour, estDedans retourne vrai.
//
// Entrée:
//    const GOCoordonneesXYZ& coord : la coordonnée du point à vérifier
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOEnveloppe::pointInterieur(const GOCoordonneesXYZ& coord) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return coord.estDedans(m_min, m_max);
}

//************************************************************************
// Sommaire:  Coordonnées minimales de l'enveloppe
//
// Description: 
//    La méthode publique <code>reqCoordMin()</code> retourne les
//    coordonnées minimales.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ GOEnveloppe::reqCoordMin() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_min;
}

//************************************************************************
// Sommaire: 
//
// Description: 
//    La méthode publique <code>reqCoordMax()</code> retourne les
//    coordonnées maximales.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ GOEnveloppe::reqCoordMax() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_max;
}

//************************************************************************
// Sommaire:   Operateur==
//
// Description: 
//    L'opérateur public <code>operator==(...)</code> compare l'enveloppe à
//    celle passée en argument. Deux enveloppes sont égales si leurs paires de
//    coordonnées minimales et maximales sont égales.
//
// Entrée:
//    const GOEnveloppe& obj     L'enveloppe à comparer
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOEnveloppe::operator== (const GOEnveloppe& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return (m_min == obj.m_min && m_max == obj.m_max);
}  

//************************************************************************
// Sommaire: 
//    Opérateur !=
//
// Description: 
//    L'opérateur public operator==(...) compare l'enveloppe à celle passée
//    en argument. Deux enveloppe sont égales si leur paire de coordonnées
//    minimales et maximales est égale.
//
// Entrée:
//    const GOEnveloppe& obj
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOEnveloppe::operator!= (const GOEnveloppe& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return !(*this == obj);
}
