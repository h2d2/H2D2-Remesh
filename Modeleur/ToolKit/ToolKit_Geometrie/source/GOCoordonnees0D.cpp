//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Classe : GOCoordonnees0D
//************************************************************************

#include "GOCoordonnees0D.h"

#include "GOCoord3.h"

#include <iostream>
#include <limits>

const GOCoordonnees0D GOCoordonnees0D::GRAND = GOCoordonnees0D();
const GOCoordonnees0D GOCoordonnees0D::PETIT = GOCoordonnees0D();

//************************************************************************
// Sommaire: Constructeur par défaut.
//
// Description:
//    Le constructeur publique <pre>GOCoordonnees0D()</pre> est le constructeur par
//    défaut de la classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::GOCoordonnees0D ()
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur publique <pre>GOCoordonnees0D(const GOCoordonnees0D&)</pre> est le
//    constructeur copie de la classe. Il construit l'objet comme copie de celui
//    qui est pasé en argument.
//
// Entrée:
//    const GOCoordonnees0D& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::GOCoordonnees0D (const TCSelf&)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur publique <code>GOCoordonnees0D(const GOCoordonneesX&)</code> est un
//    opérateur cast à partir d'un <code>GOCoordonneesX</code>. Toutes les conposantes
//    sont ignorées.
//
// Entrée:
//    const GOCoordonneesXYZ& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::GOCoordonnees0D (const GOCoordonneesX&)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur publique <code>GOCoordonnees0D(const GOCoordonneesXY&)</code> est un
//    opérateur cast à partir d'un <code>GOCoordonneesXY</code>. Toutes les conposantes
//    sont ignorées.
//
// Entrée:
//    const GOCoordonneesXY& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::GOCoordonnees0D (const GOCoordonneesXY&)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Constructeur copie.
//
// Description:
//    Le constructeur publique <code>GOCoordonnees0D(const GOCoordonneesXYZ&)</code> est un
//    opérateur cast à partir d'un <code>GOCoordonneesXYZ</code>. Toutes les conposantes
//    sont ignorées.
//
// Entrée:
//    const GOCoordonneesXYZ& p      Point à copier
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::GOCoordonnees0D (const GOCoordonneesXYZ&)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // MODE_DEBUG
}

//************************************************************************
// Sommaire: Destructeur de la classe
//
// Description:
//    Le destructeur publique <pre>~GOCoordonnees0D()</pre> ne fait rien de
//    spécifique.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::~GOCoordonnees0D ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::operator GOCoordonneesXYZ() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return GOCoordonneesXYZ(0, 0, 0);
}  // GOCoordonnees0D::GOCoordonnees0D

//************************************************************************
// Sommaire: Nombre de dimension de la coordonnée
//
// Description:
//    La méthode publique <code>reqNbrDim()</code> retourne la dimension 
//    de la coordonnée
//
// Entrée:
//
// Sortie:
//   EntierN : la dimension de la coordonnée
//
// Notes:
//
//************************************************************************
EntierN GOCoordonnees0D::reqNbrDim() 
{
   return 0;
}

//************************************************************************
// Sommaire: Composante x de la coordonnée
//
// Description:
//    La méthode publique <pre>x()</pre> retourne la coordonnée x (première
//    composante) du point. La coordonnées est retournée par référence constante,
//    ce qui ne permet pas à l'utilisateur de la changer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
GOCoordonnees0D::x () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return std::numeric_limits<DReel>::infinity();
}  // GOCoordonnees0D::GOCoordonnees0D

//************************************************************************
// Sommaire: Composante y de la coordonnée
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
GOCoordonnees0D::y () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return std::numeric_limits<DReel>::infinity();
}  // GOCoordonnees0D::GOCoordonnees0D

//************************************************************************
// Sommaire: Composante y de la coordonnée
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
GOCoordonnees0D::z () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG
   return std::numeric_limits<DReel>::infinity();
}

//************************************************************************
// Sommaire:  Définition de l'opérateur [] pour des GOCoordonnees0D
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord 
GOCoordonnees0D::operator[] (Entier)
{
#ifdef MODE_DEBUG
   PRECONDITION(FAUX);  // On ne devrait jamais arriver ici
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return std::numeric_limits<DReel>::infinity();
}

//************************************************************************
// Sommaire:  Définition de l'opérateur [] pour des GOCoordonnees0D
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
GOCoordonnees0D::operator[] (Entier) const
{
#ifdef MODE_DEBUG
   PRECONDITION(FAUX);  // On ne devrait jamais arriver ici
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return std::numeric_limits<DReel>::infinity();
}

//************************************************************************
// Sommaire:  Définition de l'opérateur + pour des GOCoordonnees0D
//
// Description:
//    L'opérateur publique <pre>operator+</pre> définit l'additon entre deux
//    <pre>GOCoordonnees0D</pre>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf
GOCoordonnees0D::operator+ (const TCSelf&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Sommaire:
//    Définition de l'opérateur += pour des GOCoordonnees0D
//
// Description:
//    L'opérateur publique <pre>operator+</pre> définit l'additon entre deux
//    <pre>GOCoordonnees0D</pre>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf&
GOCoordonnees0D::operator+= (const TCSelf&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur + pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf
GOCoordonnees0D::operator- (const TCSelf&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur - pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf
GOCoordonnees0D::operator- () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur -= pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf&
GOCoordonnees0D::operator-= (const TCSelf&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur * pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf
GOCoordonnees0D::operator* (const TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur *= pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf&
GOCoordonnees0D::operator*= (const TCCoord&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur / pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf
GOCoordonnees0D::operator/ (const TCCoord&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur /= pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf&
GOCoordonnees0D::operator/= (const TCCoord&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}  // GOCoordonnees0D::operator/=

//************************************************************************
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCSelf&
GOCoordonnees0D::operator= (const TCSelf&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (*this);
}

//************************************************************************
// Description:
//    Définition de l'opérateur == pour des GOCoordonnees0D.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen
GOCoordonnees0D::operator==(const TCSelf&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return VRAI;
}

//************************************************************************
// Description:
//    Définition de l'opérateur != pour des GOCoordonnees0D
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOCoordonnees0D::operator!= (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return !(*this == p);
}

//************************************************************************
// Sommaire:   Retourne VRAI si le point est à l'intérieur
//
// Description:
//    La méthode publique <code>estDedans()</code> retourne VRAI si 
//    le point est compris dans le volume délimité par les deux  
//    points min et max passés en argument.
//
// Entrée:
//    const TCSelf& pMin     // Point Min
//    const TCSelf& pMax     // Point Max
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOCoordonnees0D::estDedans (const TCSelf&, const TCSelf&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return VRAI;
}

//************************************************************************
// Description:
//    Définition de l'opérateur < pour des GOCoordonnees0D.
//    Il s'agit ici strictement d'un opérateur d'ordonnancement.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOCoordonnees0D::operator< (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (this < &p);
}  // GOCoordonnees0D::operator<


//************************************************************************
// Description:
//    Définition de l'opérateur > pour des GOCoordonnees0D.  On utilise l'objet
//    GOEpsilon pour faire la comparaison entre deux doubles...
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
Booleen GOCoordonnees0D::operator> (const TCSelf& p) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return (this > &p);
}

//************************************************************************
// Description:
//    Définition de l'opérateur * pour des GOCoordonnees0D et Reel.
//    Cette fonction est nécessaire pour permettre une opération
//    avec arguments inversés.  En effet, p1 * val n'est pas équivalent
//    à val * p1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D
operator* (const GOCoordonnees0D::TCCoord& val, const GOCoordonnees0D& p)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return (p * val);
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un minimum global
//
// Description:  La méthode publique minGlobal(...) permet de contribuer
//               à la détermination d'un minimum global à une collection
//               d'objet de ce type.  On passe un minGlobal en argument et
//               si l'une de nos composantes est plus petite, on assigne
//               celle-ci au minimum global.
//
// Entrée:
//
// Sortie:       TCSelft& minGlobal : le minimum global.
//
// Notes:
//
//************************************************************************
void GOCoordonnees0D::minGlobal(TCSelf&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return;
}

//************************************************************************
// Sommaire:     Contribue à l'assignation d'un maximum global
//
// Description:  La méthode publique maxGlobal(...) permet de contribuer
//               à la détermination d'un maximum global à une collection
//               d'objet de ce type.  On passe un maxGlobal en argument et
//               si l'une de nos composantes est plus grande, on assigne
//               celle-ci au maximum global.
//
// Entrée:
//
// Sortie:       TCSelft& maxGlobal : le maximum global.
//
// Notes:
//
//************************************************************************
void GOCoordonnees0D::maxGlobal(TCSelf&) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return;
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La méthode publique <code>normalise()</code> normalise le vecteur (point),
//    donc le rend tel que sa norme soit 1.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOCoordonnees0D::normalise()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return;
}

//************************************************************************
// Sommaire:   Normalise le vecteur.
//
// Description:
//    La fonction <code>normalise()</code> normalise le point (vecteur) 
//    passé en argument, donc le rend tel que sa norme soit 1.
//
// Entrée:
//    const GOCoordonnees0D& p     Point à normaliser
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D normalise(const GOCoordonnees0D& p)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return p;
}

//************************************************************************
// Description:
//    Retourne la norme du vecteur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
GOCoordonnees0D::norme() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return GOCoordonnees0D::TCCoord(0);
}

//************************************************************************
// Description:
//    Retourne le carré de la norme du vecteur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
GOCoordonnees0D::norme2() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // MODE_DEBUG

   return GOCoordonnees0D::TCCoord(0);
}  // TTCoord GOCoordonnees0D::norme2 ()

//************************************************************************
// Description:
//    Retourne le produit scalaire entre les deux vecteurs. Il correspond
//    à la projection d'un vecteur sur l'autre.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
prodScal(const GOCoordonnees0D&, const GOCoordonnees0D&)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return GOCoordonnees0D::TCCoord(0);
}  // TTCoord prodScal (const GOCoordonnees0D &p1, const GOCoordonneesXYZ<TTCoord> &p2)

//************************************************************************
// Description:
//    Retourne le carré du produit scalaire entre les deux vecteurs.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
prodScal2(const GOCoordonnees0D&, const GOCoordonnees0D&)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return GOCoordonnees0D::TCCoord(0);
}

//************************************************************************
// Description:
//    Retourne le produit vectoriel entre deux vecteurs. Il correspond à
//    l'aire du parallélogramme construit sur les deux vecteurs.
//
// Entrée:
//    const TCSelf& p  // Premier vecteur
//    const TCSelf& p  // Deuxième vecteur
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonneesXYZ
prodVect(const GOCoordonnees0D&, const GOCoordonnees0D&)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return GOCoordonneesXYZ(0,0,0);
}

//************************************************************************
// Description:
//    Retourne la distance euclidienne entre deux points.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
dist(const GOCoordonnees0D&, const GOCoordonnees0D&)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return GOCoordonnees0D::TCCoord(0);
}  // TTCoord prodScal (const TCSelf &p1, const TCSelf &p2)

//************************************************************************
// Description:
//    Retourne le carré de la distance euclidienne entre deux points.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOCoordonnees0D::TCCoord
dist2(const GOCoordonnees0D&, const GOCoordonnees0D&)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return GOCoordonnees0D::TCCoord(0);
}

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'extraction >>.
//
// Description:
//    L'opérateur <code>operator >>(...)</code> friend de la classe
//    <code>GOCoordonnees0D</code> est une surcharge de l'opérateur d'extraction
//    sur un <code>istream</code> pour celle-ci.
//
// Entrée:
//    istream& is                   // Stream de lecture
//    GOCoordonnees0D& p            // GOCoordonnees0D à lire
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::istream& operator>> (std::istream& is, GOCoordonnees0D&)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return is;
}

//************************************************************************
// Sommaire:
//    Surcharge de l'opérateur d'insertion <<.
//
// Description:
//    L'opérateur <code>operator <<(...)</code> friend de la classe
//    <code>GOCoordonnees0D</code> est une surcharge de l'opérateur d'insertion
//    sur un <code>ostream</code> pour celle-ci.
//
// Entrée:
//    ostream& os                   // Stream d'écriture
//    const TCSelf& p               // GOCoordonnees0D à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
std::ostream& operator<< (std::ostream& os, const GOCoordonnees0D&)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   return os;
}
