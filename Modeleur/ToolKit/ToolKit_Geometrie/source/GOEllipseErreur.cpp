//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2004
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  GOEllipseErreur.cpp
// Classe:   GOEllipseErreur
//*****************************************************************************
// 14-10-2004  Sébastien Labbé   Version originale
//*****************************************************************************
#include "GOEllipseErreur.h"
#include "GOCoord3.h"

#ifdef MODE_IMPORT_EXPORT
   #include "fifichie.h"
#endif // MODE_IMPORT_EXPORT

#include <cmath>
//#include <mkl_lapack.h>

static const DReel PI = acos((long double) -1);

class Vec2
{
private:
   DReel A[2];

public:
   Vec2() : A{ 0.0 } { }
   Vec2(DReel a, DReel b) : A{ a, b} { }

         DReel& operator() (int i)       { return A[i]; }
   const DReel& operator() (int i) const { return A[i]; }

   Vec2 normalize() const
   {
      const DReel n = hypot(A[0], A[1]);
      return Vec2(A[0]/n, A[1]/n);
   }
};

class Mat22
{
private:
   DReel A[2][2];

public:
   Mat22(): A{ {0.0} } { }
   Mat22(DReel a00, DReel a01, DReel a10, DReel a11) : A{ {a00, a01}, {a10, a11} } { }
   Mat22(const GOEllipseErreur& e) { e.reqComposantes(A[0][0], A[1][0], A[1][1]); A[0][1] = A[1][0]; };

         DReel& operator() (int i, int j)       { return A[i][j]; }
   const DReel& operator() (int i, int j) const { return A[i][j]; }

   Mat22 inverse() const
   {
      const DReel det = 1.0 / (A[0][0]*A[1][1] - A[0][1]*A[1][0]);
      const DReel r00 =  A[1][1] * det;
      const DReel r01 = -A[0][1] * det;
      const DReel r10 = -A[1][0] * det;
      const DReel r11 =  A[0][0] * det;
      return Mat22(r00, r01, r10, r11);
   }

   Mat22 transpose() const
   {
      return Mat22(A[0][0], A[1][0], A[0][1], A[1][1]);
   }

   Vec2 eigenVal() const
   {
      // http ://www.math.harvard.edu/archive/21b_fall_04/exhibits/2dmatrices/index.html
      // https://people.math.harvard.edu/~knill/teaching/math21b2004/exhibits/2dmatrices/index.html
      const DReel a = A[0][0];
      const DReel b = A[0][1];
      const DReel c = A[1][0];
      const DReel d = A[1][1];

      const DReel tr = a + d;          // TRACE
      const DReel dt = a*d - b*c;      // DETERMINANT
      const DReel tt = (0.25 * tr * tr - dt);
      DReel l1, l2;
      if (tt >= 0.0)
      {
         l1 = 0.5 * tr - sqrt(tt);
         l2 = 0.5 * tr + sqrt(tt);
      }
      else
      {
         l1 = 1.0e-16;
         l2 = 1.0e-16;
      }
      return Vec2(std::max(l1, l2), std::min(l1, l2));
   }

   /*
   * Eigenvector as column vector
   */
   Mat22 eigenVec() const
   {
      const Vec2 ev = eigenVal();
      return eigenVec(ev);
   }
   Mat22 eigenVec(const Vec2& ev) const
   {
      Mat22 R;
      const DReel a = A[0][0];
      const DReel b = A[0][1];
      const DReel c = A[1][0];
      const DReel d = A[1][1];
      if (c != 0.0 && (ev(0)-d) != (ev(1)-d))
      {
         const Vec2 v1 = Vec2(ev(0) - d, c).normalize();
         const Vec2 v2 = Vec2(ev(1) - d, c).normalize();
         R = Mat22(v1(0), v2(0), v1(1), v2(1));
      }
      else if (b != 0.0 && (ev(0)-a) != (ev(1)-a))
      {
         const Vec2 v1 = Vec2(b, ev(0) - a).normalize();
         const Vec2 v2 = Vec2(b, ev(1) - a).normalize();
         R = Mat22(v1(0), v2(0), v1(1), v2(1));
      }
      else
      {
         R = Mat22(1.0, 0.0, 0.0, 1.0);
      }
      return R;
   }
};

Mat22 operator * (const Mat22& A, const Mat22& B)
{
   Mat22 res;
   res(0,0) = A(0,0)*B(0,0) + A(0,1)*B(1,0);
   res(0,1) = A(0,0)*B(0,1) + A(0,1)*B(1,1);
   res(1,0) = A(1,0)*B(0,0) + A(1,1)*B(1,0);
   res(1,1) = A(1,0)*B(0,1) + A(1,1)*B(1,1);
   return res;
}

Vec2 operator * (const Mat22& M, const Vec2& V)
{
   Vec2 R;
   R(0) = M(0,0)*V(0) + M(0,1)*V(1);
   R(1) = M(1,0)*V(0) + M(1,1)*V(1);
   return R;
}

Vec2 operator * (const Vec2& V, const Mat22& M)
{
   Vec2 R;
   R(0) = V(0)*M(0,0) + V(1)*M(1,0);
   R(1) = V(0)*M(0,1) + V(1)*M(1,1);
   return R;
}

Mat22 maximum (const Mat22& A, const Mat22& B)
{
   Mat22 res;
   res(0,0) = std::max(A(0,0), B(0,0));
   res(0,1) = std::max(A(0,1), B(0,1));
   res(1,0) = std::max(A(1,0), B(1,0));
   res(1,1) = std::max(A(1,1), B(1,1));
   return res;
}

Mat22 minimum(const Mat22& A, const Mat22& B)
{
   Mat22 res;
   res(0, 0) = std::min(A(0, 0), B(0, 0));
   res(0, 1) = std::min(A(0, 1), B(0, 1));
   res(1, 0) = std::min(A(1, 0), B(1, 0));
   res(1, 1) = std::min(A(1, 1), B(1, 1));
   return res;
}

//************************************************************************
// Sommaire:  Constructeur.
//
// Description:
//    Le constructeur public <code>GOEllipseErreur(...)</code> construit
//    un objet à partir des arguments.
//
// Entrée:
//   DReel g              // Paramètres de l'ellipse
//   DReel p
//   DReel a
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOEllipseErreur::GOEllipseErreur (DReel g, DReel p, DReel a)
   : grandAxe(g), petitAxe(p), angle(a)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}


//************************************************************************
// Sommaire:  Destructeur.
//
// Description:
//    Le destructeur public <code>~GOEllipseErreur()</code> ne fait rien.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOEllipseErreur::~GOEllipseErreur()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

}

//************************************************************************
// Sommaire:  Assigne le grand axe de l'ellipse
//
// Description:
//    La méthode publique <code>asgGrandAxe()</code> assigne le grand axe de
//    l'ellipse.
//
// Entrée:
//    const DReel& nv : la nouvelle valeur du grand axe
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOEllipseErreur::asgGrandAxe (const DReel& nv)
{
#ifdef MODE_DEBUG
   PRECONDITION(nv > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   grandAxe = nv;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}   // GOEllipseErreur::asgGrandAxe

//************************************************************************
// Sommaire:  Assigne le petit axe de l'ellipse
//
// Description:
//    La méthode publique <code>asgPetitAxe()</code> assigne le petit axe de
//    l'ellipse.
//
// Entrée:
//    const DReel& nv : la nouvelle valeur du petit axe
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOEllipseErreur::asgPetitAxe (const DReel& nv)
{
#ifdef MODE_DEBUG
   PRECONDITION(nv > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   petitAxe = nv;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}   // GOEllipseErreur::asgPetitAxe

//************************************************************************
// Sommaire:  Assigne l'angle d'inclinaison de l'ellipse
//
// Description:
//    La méthode publique <code>asgInclinaison()</code> assigne l'angle
//    d'inclinaison de l'ellipse.
//
// Entrée:
//    const DReel& nv : la nouvelle valeur de l'inclinaison
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOEllipseErreur::asgInclinaison (const DReel& nv)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   angle = nv;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}   // GOEllipseErreur::asgInclinaison

//************************************************************************
// Sommaire:  Intersection entre deux métriques.
//
// Description:
//    La fonction <code>intersection(...)</code> calcule la métrique
//    résultant de l'intersection entre deux métriques. Il s'agit de
//    la plus grande ellipse inscrite dans l'intersection des deux
//    ellipses.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    https://publications.polymtl.ca/245/1/2010_SimonBelanger.pdf
//************************************************************************
GOEllipseErreur intersection(const GOEllipseErreur& E1,
                             const GOEllipseErreur& E2)
{

   // ---  Matrices des métriques
   const Mat22 M1(E1);
   const Mat22 M2(E2);

   // ---  Métrique de l'intersection
   const Mat22 N  = M1.inverse() * M2;       // N = M1^-1 @ M2
   const Mat22 PN = N.eigenVec();
   const Mat22 PN_1 = PN.inverse();

   const Mat22 L1 = PN.transpose() * M1 * PN;
   const Mat22 L2 = PN.transpose() * M2 * PN;
   const Mat22 Li = maximum(L1, L2);
   const Mat22 Mi = PN_1.transpose() * Li * PN_1;

   GOEllipseErreur ret;
   ret.asgComposantes(Mi(0,0), Mi(0,1), Mi(1,1));
   return ret;
}

//************************************************************************
// Sommaire:  Surcharge de l'opérateur de comparaison d'égalité
//
// Description:
//    Les opérateurs publiques
//    <code>operator ==()</code>
//    pour des objets de type <code>GOEllipseErreur</code>.
//
// Entrée:
//    const GOEllipseErreur& autre    L'objet à comparer
//
// Sortie:
//
// Notes:
//    1. La fonction fmod calcule de le reste de la division.
//    2. La fonction fabs retourne la valeur absolue.
//************************************************************************
bool GOEllipseErreur::operator == (const GOEllipseErreur& autre) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (grandAxe == autre.grandAxe &&
           petitAxe == autre.petitAxe &&
           fabs(fmod(angle, 2*PI )) == fabs(fmod(autre.angle, 2*PI) ));
}

//************************************************************************
// Sommaire:  Assigne les composantes de la métrique.
//
// Description:
//    La méthode publique <code>asgComposantes()</code> assigne les
//    composantes de la métrique associée à l'ellipse.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOEllipseErreur::asgComposantes(const DReel& a11, const DReel& a12, const DReel& a22)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel MinEigenvalue = 1.0e-12;
   const DReel AniRatio = 1.0e+12;

   const Mat22 M(a11, a12, a12, a22);
   Vec2  eVal = M.eigenVal();
   Mat22 eVec = M.eigenVec(eVal);

   // ---  Force elliptical
   eVal(0) = fabs(eVal(0));
   eVal(1) = fabs(eVal(1));
   // ---  Min eigenvalue first (major axis)
   if (eVal(0) > eVal(1))
   {
      std::swap(eVec(0,0), eVec(0,1));
      std::swap(eVec(1,0), eVec(1,1));
      std::swap(eVal(0), eVal(1));
   }
   // ---  Angle in [-p1/2, pi/2]
   if (eVec(0,0) < 0)
   {
      eVec(0,0) = -eVec(0,0);
      eVec(1,0) = -eVec(1,0);
   }
   // ---  Limit
   eVal(0) = std::max(eVal(0), MinEigenvalue);
   eVal(1) = std::min(eVal(1), eVal(0) * AniRatio);
   eVal(1) = std::max(eVal(1), MinEigenvalue);

   grandAxe = 1.0 / sqrt(eVal(0));
   petitAxe = 1.0 / sqrt(eVal(1));
   angle = atan2(eVec(1,0), eVec(0,0));
}

//************************************************************************
// Sommaire:  Retourne les composantes de la matrice.
//
// Description:
//    La méthode publique <code>reqComposantes()</code> retourne les
//    composantes de la matrice associée à l'ellipse.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOEllipseErreur::reqComposantes(DReel& a11, DReel& a12, DReel& a22) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const DReel cosAlfa = cos(angle);
   const DReel sinAlfa = sin(angle);
   const DReel lambda1 = reqEigenValMin();
   const DReel lambda2 = reqEigenValMax();

   a11 = cosAlfa*cosAlfa*lambda1 + sinAlfa*sinAlfa*lambda2;
   a12 = cosAlfa*sinAlfa*(lambda1 - lambda2);
   a22 = sinAlfa*sinAlfa*lambda1 + cosAlfa*cosAlfa*lambda2;
}

//************************************************************************
// Sommaire:  Surcharge de l'opérateur de redirection
//
// Description:
//    <code>operator <<() </code> pour des objets de type
//    <code>GOEllipseErreur</code>.
//
// Entrée:
//    FIFichier&
//    const GOEllipseErreur& valeur
//
// Sortie:
//    FIFichier&
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
FIFichier& operator << (FIFichier& os, const GOEllipseErreur& valeur)
{
#ifdef MODE_DEBUG
   PRECONDITION(os.estOuvert());
   PRECONDITION(os.reqAcces() != FIFichier::LECTURE);
   valeur.INVARIANTS ("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   os << valeur.reqGrandAxe() << " "
      << valeur.reqPetitAxe() << " "
      << valeur.reqInclinaison();

#ifdef MODE_DEBUG
   POSTCONDITION(os.estOuvert());
   valeur.INVARIANTS ("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return (os);
}
#endif // MODE_PERSISTANT

//************************************************************************
// Sommaire:  Surcharge de l'opérateur de redirection
//
// Description:
//    <code>operator >>() </code> pour des objets de type
//    <code>GOEllipseErreur</code>.
//
// Entrée:
//    FIFichier&
//    const GOEllipseErreur& valeur
//
// Sortie:
//    FIFichier&
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
FIFichier& operator >> (FIFichier& is, GOEllipseErreur& valeur)
{
#ifdef MODE_DEBUG
   PRECONDITION(is.estOuvert());
   PRECONDITION(is.reqAcces() == FIFichier::LECTURE);
   valeur.INVARIANTS ("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   is >> valeur.grandAxe >> valeur.petitAxe >> valeur.angle;

#ifdef MODE_DEBUG
   POSTCONDITION(is.estOuvert());
   valeur.INVARIANTS ("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return (is);
}
#endif // MODE_PERSISTANT


//************************************************************************
// Sommaire:  Surcharge de l'opérateur de redirection
//
// Description:
//    <code>operator <<() </code> pour des objets de type
//    <code>GOEllipseErreur</code>.
//
// Entrée:
//    FIFichier&
//    const GOEllipseErreur& valeur
//
// Sortie:
//    FIFichier&
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
void GOEllipseErreur::exporte (FIFichier& os) const
{
#ifdef MODE_DEBUG
   PRECONDITION(os.estOuvert());
   PRECONDITION(os.reqAcces() != FIFichier::LECTURE);
   INVARIANTS ("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   os << grandAxe << " "
      << petitAxe << " "
      << angle;

#ifdef MODE_DEBUG
   POSTCONDITION(os.estOuvert());
   INVARIANTS ("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_PERSISTANT

//************************************************************************
// Sommaire:  Surcharge de l'opérateur de redirection
//
// Description:
//    <code>operator >>() </code> pour des objets de type
//    <code>GOEllipseErreur</code>.
//
// Entrée:
//    FIFichier&
//    const GOEllipseErreur& valeur
//
// Sortie:
//    FIFichier&
// Notes:
//
//************************************************************************
#ifdef MODE_PERSISTANT
void GOEllipseErreur::importe (FIFichier& is)
{
#ifdef MODE_DEBUG
   PRECONDITION(is.estOuvert());
   PRECONDITION(is.reqAcces() == FIFichier::LECTURE);
   INVARIANTS ("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   is >> grandAxe >> petitAxe >> angle;

#ifdef MODE_DEBUG
   POSTCONDITION(is.estOuvert());
   INVARIANTS ("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}
#endif // MODE_PERSISTANT
