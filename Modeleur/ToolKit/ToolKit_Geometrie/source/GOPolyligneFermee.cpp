//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: GOPolyligneFermee.cpp
// Classe : GOPolyligneFermee
//************************************************************************
// 16-02-2004  Maude Giasson          Version initiale
//************************************************************************
#include "GOPolyligneFermee.h"

#include "GOCoord3.h"
#include "GOPoint.h"
#include "GOGeometrie.h"
#include "GOMultiPoint.h"
#include "GOMultiPolyligne.h"
#include "GOMultiPolygone.h"
#include "GOGenerateurID.h"

//-- Inclusions GEOS :
#include "geos_sup.h"
#include <geos_c.h>
//#include <geos/algorithm/CGAlgorithms.h>

#include <iterator>
#include <vector>
#include <map>

//************************************************************************
// Sommaire:  Constructeur par défaut
//
// Description:
//    Le constructeur par défaut <code>GOPolyligneFermee()</code> crée une 
//    polyligne vide.
//    
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPolyligneFermee::GOPolyligneFermee()
   : GOPolyligne()
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur de copie
//
// Description:
//    Constructeur par copie. La donnée membre m_polyligneP est initialisée
//    en copiant celle de la polyligne passée en paramètre.
//    
// Entrée: 
//    const GOPolyligneFermee & autre        Polyligne à copier
//
// Sortie:
//
// Notes: 
//************************************************************************
GOPolyligneFermee::GOPolyligneFermee(const GOPolyligneFermee& autre)
   : GOPolyligne(autre)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Constructeur de copie
//
// Description:
//    Constructeur par copie. La donnée membre m_polyligneP est initialisée
//    en copiant celle de la polyligne passée en paramètre.
//    
// Entrée: 
//    const GOPolyligneFermee & autre        Polyligne à copier
//
// Sortie:
//
// Notes: 
//************************************************************************
GOPolyligneFermee::GOPolyligneFermee(const GOPolyligne& autre)
   : GOPolyligne(autre)
{

   if (m_sommets.front() != m_sommets.back())
      m_sommets.push_back(m_sommets.front());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//************************************************************************
// Sommaire:   Destructeur.
//
// Description: 
//    Destructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
GOPolyligneFermee::~GOPolyligneFermee()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
}

//************************************************************************
// Sommaire:   Opérateur d'affectation.
//
// Description:
//    L'opérateur public <code>operator=(...)</code> est l'opérateur
//    d'affectation de la classe. L'objet est en tout point identique
//    à la polyligne passée en argument.
//    
// Entrée: 
//    const GOPolyligneFermee& autre;     La polyligne à copier
//
// Sortie:
//
// Notes: 
//************************************************************************
GOPolyligneFermee& GOPolyligneFermee::operator = (const GOPolyligneFermee& autre)
{ 
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   this->GOPolyligne::operator=(autre);

   return *this;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La méthode <code>asgGenerateurID(...)</code>
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
void GOPolyligneFermee::asgGenerateurID(GOGenerateurIDP genIdP)
{
#ifdef MODE_DEBUG
   PRECONDITION(genIdP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   m_genIdP = genIdP;
   m_genIdP->asgID(m_sommets.begin(), --m_sommets.end());
   m_sommets.back() = m_sommets.front();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire:
//
// Description: 
//    La fonction <code>reqGenerateurID(...)</code> 
//
// Entrée:
//
// Sortie:
//
// Notes: 
//************************************************************************
GOGenerateurIDP GOPolyligneFermee::reqGenerateurID()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return m_genIdP;
}

//************************************************************************
// Sommaire:   Ajoute un sommet à la fin de la polyligne.
//
// Description:
//    La méthode <code>ajouteSommet(...)</code> ajoute le sommet passé
//    en paramètre à la fin de la polyligne.
//
// Entrée: 
//    const TCCoord& sommet      Coordonnées du sommet à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::ajouteSommet(const TCSommet& sommet)
{
#ifdef MODE_DEBUG
//   PRECONDITION(reqNbrSommets() > 2);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   if (reqNbrSommets() == 0)
   {
      msg = GOPolyligne::ajouteSommet(sommet);
      m_sommets.push_back(m_sommets.front());
   }
   else
   {
      msg = GOPolyligne::ajouteSommet(sommet, reqNbrSommets()-1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Ajoute un sommet à la polyligne à la position souhaitée.
//    
//
// Description:
//    La méthode <code>ajouteSommet(...)</code> ajoute le sommet passé
//    en paramètre à la polyligne à la position souhaitée.
//    Par défaut, ce sommet  se retrouve comme étant le dernier 
//    parmis l'ensemble ordonné de sommets formant la polyligne. 
//
// Entrée: 
//    const TCCoord& sommet   Coordonnées du sommet à ajouter
//    EntierN indice          Indice où placer le sommet. indice représente
//                            l'indice que portera le sommet après son ajout.
//                            Il doit être entre 0 et le nombre de sommet de
//                            la polyligne (avant ajout du sommet).
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::ajouteSommet(const TCSommet& sommet, EntierN ind)
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   
   return GOPolyligne::ajouteSommet(sommet, ind);
}

//************************************************************************
// Sommaire:  Test si une polyligne est contenue dans l'objet.
//
// Description:
//    La fonction <code>contient(...)</code> 
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::contient(Booleen& reponse, const GOPolyligne& autre) const
{
#ifdef MODE_DEBUG
    INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP = geos_sup::LinearRing(*this);
   geos_sup::TTGeomUniquePtr g_autreP   = geos_sup::LineString(autre);

   // ---  Calcul
   char r = GEOSContains(g_selfP.get(), g_autreP.get());
   if (r == 2) msg = geos_sup::reqMsgErreur();

   reponse = (r == 1);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: Translate un sommet de la polyligne.
//
// Description:
//    La méthode <code>deplace(...)</code> deplace un point de la 
//    polyligne selon le vecteur de déplacement passé en paramètre.
//
// Entrée:
//    const TCCoord& vctDeplacement       Vecteur de déplacement
//    const Entier& ind                   Indice du sommet à déplacer
//    
// Sortie:
//
// Notes:-Pour l'instant, le message d'erreur retourné est toujours OK
//************************************************************************
ERMsg GOPolyligneFermee::deplace (const TCCoord& vctDeplacement, EntierN ind)
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < reqNbrSommets());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Deplace le sommet
   msg = GOPolyligne::deplace(vctDeplacement, ind);
   m_sommets.back() = m_sommets.front();   

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   VRAI si la GOPolyligneFermee est anti-horaire
//           
// Description:
//    La méthode publique <code>estAntiHoraire(...)</code> vérifie le sens 
//    dans lequel les sommets sont ordonnés. Elle place la variable "reponse"
//    à true si les sommets sont dans le sens anti-horaire, false sinon. 
//
// Entrée: 
//
// Sortie:
//    Booleen reponse :    VRAI si la GOPolyligneFermee est anti-horaire
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::estAntiHoraire(Booleen &reponse) const
{
#ifdef MODE_DEBUG
   PRECONDITION(reqNbrSommets() > 3);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

/*
   http://www.iitk.ac.in/esc101/02sem1/labs/Classes/Polygon.java
   public boolean isCCW()
   {
      double min = vertex[0].getY();
      int minIndex = 0;
      for (int i = 1; i < numVertices; i++)
      {  
         if (vertex[i].getY() < min)
         { 
            min = vertex[i].getY() ;
            minIndex = i;
         }    
      }
      Point2D.Double prev = vertex[(minIndex - 1 + vertex.length) % vertex.length ]; 
      Point2D.Double next = vertex[(minIndex + 1) % vertex.length ];
      return (turnTest (prev, vertex[minIndex], next));
   }
*/

   // ---  Point de y min
   TCConstIterateur pI = reqDebut();
   TCConstIterateur eI = reqFin();
   TCConstIterateur mI = pI++;
   while (pI != eI)
   {
      if (pI->reqPosition().y() < mI->reqPosition().y()) mI = pI;
   }

   // ---  Point précédant et suivant
   TCConstIterateur p2I = mI;
   TCConstIterateur p1I = (  mI == reqDebut()) ? --reqFin()   : --mI;
   mI = p2I;
   TCConstIterateur p3I = (++mI == reqFin())   ?   reqDebut() :   mI;

   // ---  Produit vectoriel
   const TCCoord& p1 = p1I->reqPosition();
   const TCCoord& p2 = p2I->reqPosition();
   const TCCoord& p3 = p3I->reqPosition();
   reponse = (prodVect(p2-p1, p3-p2).z() > 0);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Rendre la GOPolyligneFermee anti-horaire
//           
// Description:
//    La méthode publique <code>soisAntiHoraire(...)</code> oblige à la
//    polyligne fermée de s'orienter dans le sens anti-horaire.  
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::soisAntiHoraire()
{
#ifdef MODE_DEBUG
   PRECONDITION(reqNbrSommets() > 3);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   Booleen estAH;
   if(msg)
   {
      msg = estAntiHoraire(estAH);
   }
   if(msg && !estAH)
   {
      msg = retourne();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  
//
// Description:
//    La fonction <code>intersection(...)</code> 
//
// Entrée: 
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::intersection(GOGeometrie& intersection,
                                      const GOPolyligneFermee& autre) const
{
#ifdef MODE_DEBUG
   //intersection est vide
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Transforme en objets GEOS
   geos_sup::TTGeomUniquePtr g_selfP = geos_sup::LinearRing(*this);
   geos_sup::TTGeomUniquePtr g_autreP   = geos_sup::LinearRing(autre);

   // ---  Calcul
   geos_sup::TTGeomUniquePtr g_resP(GEOSIntersection(g_selfP.get(), g_autreP.get()), &GEOSGeom_destroy);
   if (g_resP == NULL) msg = geos_sup::reqMsgErreur();

   // ---  Transfert dans le résultat
   if (msg) geos_sup::transfer(intersection, g_resP.get());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:  Modifie les coordonnées d'un sommet de la polyligne. 
//
// Description:
//    La méthode <code>modifie(...)</code> modifie les coordonnées du sommet
//    dont l'indice est passé en paramètre. Les nouvelles coordonnées sont
//    passées en paramètre.
//
// Entrée:
//    const TCCoord & coord         Nouvelles coordonnées du sommet
//    EntierN indice                Indice du sommet à modifier.
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::modifie (const TCCoord& coord, EntierN ind)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Remplace le sommet
   msg = GOPolyligne::modifie(coord, ind);
   m_sommets.back() = m_sommets.front();   

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire: 
//   Supprime un sommet d'une polyligne.
//
// Description:
//    La méthode publique <code>supprime(...)</code> supprime le sommet de
//    la polyligne se trouvant à l'indice "indice".
//
// Entrée: const EntierN &indice : indice du sommet à supprimer
//
// Sortie:
//
// Notes:
//
//************************************************************************
ERMsg GOPolyligneFermee::supprime(EntierN ind)
{
#ifdef MODE_DEBUG
   PRECONDITION(reqNbrSommets() > 4);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Efface le sommet se trouvant à cet endroit
   if (ind == reqNbrSommets()-1) ind = 0;
   msg = GOPolyligne::supprime(ind);
   m_sommets.back() = m_sommets.front();   

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}
