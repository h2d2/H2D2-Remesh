//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: clvect.h
//
// Classe:  CLVecteur
//
// Description:
//    La classe vecteur permet d'avoir un vecteur d'un type
//    défini dans la structure "Type".
//
// Attributs:
//    EntierN              bloc
//    EntierN              borneSuperieur;
//    EntierN              dim;
//    EntierN              indice;
//    SYCouleurRGBP        teteCouleurP;
//    CLChaineP            teteChaineP;
//    EntierP              teteEntierP;
//    RGOrdreGraphiqueP    teteOrdreGraphiqueP;
//    ReelP                teteReelP;
//    Type                 type;
//
// Notes:
//************************************************************************
// 26-01-1992  Marc Hughes        Version initiale
// 20-12-1993  André Houde        Modifications
// 08-03-1996  Bernard Doyon      Ajout de la méthode maximum(surcharge)
// 06-11-1996  Serge Dufour       Vérification des dépendances
// 27-05-1997  Pascal Noe         Passage au nouveau format de prepostconditions
// 16-10-1997  Yves Secretan      Passage à la double précision
// 05-11-1997  Yves Roy           Cast en DReel dans operator<< pour lever ambiguïté
// 28-10-1998  Yves Secretan      Remplace #include "ererreur"
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#ifndef CLVECT_H_DEJA_INCLU
#define CLVECT_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

// --- Codes d'erreurs.
#define DIVIZERO  10
#define DOMAINERR 11

#define MAXREEL 2.2250738585072014E+308
#define MINREEL MAXREEL / 2 - 1

DECLARE_CLASS(CLChaine);
//DECLARE_CLASS(RGOrdreGraphique);
DECLARE_CLASS(SYCouleurRGB);
DECLARE_CLASS(CLVecteur);

class CLVecteur
{
public:
   enum Type {RGORDREGRAPHIQUE, SYCHAINE, SYCOULEURRGB, ENTIER, REEL};

private:
   EntierN    bloc;
   EntierN    dim;
   EntierN    indice;
   union
   {
      SYCouleurRGBP         teteCouleurP;
      CLChaineP             teteChaineP;
      EntierP               teteEntierP;
      DReelP                teteReelP;
   } val;
   Type       type;

protected:
   void       invariant(ConstCarP) const;

public:
               CLVecteur(Type,EntierN dimVec = 1);
               CLVecteur(const CLVecteur&);
               ~CLVecteur();
   Entier      ajoute(Entier);
   Entier      ajoute(DReel);
   Entier      ajoute(CLChaine&);
   Entier      ajoute(SYCouleurRGB&);
   Entier      alloue(EntierN dimVec = 1);
   Entier      compacte();
   Entier      detruis(EntierN);
   Entier      dimVec(EntierN &dimVec) const { dimVec = dim; return OK;}
   Entier      efface();
   Entier      expMoinsUn();
   Entier      expCarre();
   Entier      exposant(DReel);
   Entier      expX(DReel);
   Entier      insere(Entier&, EntierN);
   Entier      insere(DReel&, EntierN);
   Entier      insere(CLChaine&, EntierN);
   Entier      insere(SYCouleurRGB&, EntierN);
   Entier      inverse();
   Entier      ln();
   Entier      loga();
   Entier      nbrElem(EntierN &nombre) const { nombre = indice; return OK;}
   Entier      maximum(DReel&) const;
   Entier      maximum(DReel&, EntierN&) const;
   Entier      mediane(DReel&);
   Entier      minimum(DReel&) const;
   Entier      moyenne(DReel&) const;
   Entier      opACos();
   Entier      opASin();
   Entier      opATan();
   Entier      opCos();
   Entier      opSin();
   Entier      opSqrt();
   Entier      opTan();
   Entier      prodScal(CLVecteur&, DReel&);
   Entier      recherche(EntierN&, const Entier);
   Entier      reqType(Type&) const;
   Entier      remplace(Entier, EntierN);
   Entier      remplace(DReel, EntierN);
   Entier      remplace(CLChaine&, EntierN);
   Entier      remplace(SYCouleurRGB&, EntierN);
   Entier      reqElement(Entier&,EntierN) const;
   Entier      reqElement(DReel&, EntierN) const;
   Entier      reqElement(CLChaine&, EntierN) const;
   Entier      reqElement(SYCouleurRGB&, EntierN) const;
   Entier      tete(VoidP &) const;
   Entier      tri();
   Entier      variance(DReel&);
   Entier      zeroDedans(Booleen&) const;

   CLVecteur   operator+ (const DReel);
   CLVecteur   operator+ (const CLVecteur&);
   CLVecteur   operator- (const DReel);
   CLVecteur   operator- (const CLVecteur&);
   CLVecteur   operator* (const DReel);
   CLVecteur   operator* (const CLVecteur&);
   CLVecteur   operator/ (const DReel);
   CLVecteur   operator/ (const CLVecteur&);
   CLVecteur&  operator= (const DReel);
   CLVecteur&  operator= (const CLVecteur&);
   CLVecteur&  operator+=(const DReel c)       { *this = *this + c; return *this; }
   CLVecteur&  operator+=(const CLVecteur& c)  { *this = *this + c; return *this; }
   CLVecteur&  operator-=(const DReel c)       { *this = *this - c; return *this; }
   CLVecteur&  operator-=(const CLVecteur& c)  { *this = *this - c; return *this; }
   CLVecteur&  operator*=(const DReel c)       { *this = *this * c; return *this; }
   CLVecteur&  operator*=(const CLVecteur& c)  { *this = *this * c; return *this; }
   CLVecteur&  operator/=(const DReel c)       { *this = *this / c; return *this; }
   CLVecteur&  operator/=(const CLVecteur& c)  { *this = *this / c; return *this; }

   CLVecteur& operator << (const Entier &n)  { ajoute(Entier(n));  return (*this); }
   CLVecteur& operator << (const DReel &n)   { ajoute(DReel(n));  return (*this); }

};

//**************************************************************
//
// Description:
//
// Visibilité: protected
//
// Entrée:
//     ConstCarP conditionP:   PRECONDITION ou POSTCONDITION
//
// Sortie:
//     Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
#ifdef MODE_DEBUG
inline void CLVecteur::invariant(ConstCarP conditionP) const
{
   INVARIANT(indice <= dim, conditionP);
}
#else
inline void CLVecteur::invariant(ConstCarP) const
{
}
#endif

#endif // CLVECT_H_DEJA_INCLU
