//*****************************************************************************
// $Id$
// $Date$
//*****************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//*****************************************************************************
// Fichier:  clchaine.h
//
// Classe:   CLChaine.
//
// Sommaire: Classe utilitaire de gestion de chaines de caractères
//
// Description:
//    Classe contenant les fonctionnalités et opérations d'une chaîne de
//    caractères.
//
// Attributs:
//    CarP    chaineP   : Pointeur à la chaîne de caractères.
//    EntierN dimension : Dimension de mémoire allouée (et non la longueur
//                        de la chaine de caractères).
//
// Notes:
//*****************************************************************************
// 24-02-1993  André Houde        Version initiale
// 09-02-1994  André Houde        Modification
// 27-09-1995  Yves Roy           Révision de l'interface et de l'implémentation
//                                Transfert des préconditions, postconditions
//                                et invariant vers la nouvelle façon de faire.
// 05-12-1995  Yves Roy           Révision de code
// 06-11-1996  Serge Dufour       Vérification des dépendances
// 18-12-1996  Yves Roy           Ajout de la méthode Booleen contient(const CLChaine&) const
// 22-10-1997  Bernard Doyon      Passage à la double précision.
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 28-04-1999  Yves Secretan      Supporte les iostream du standard
// 27-05-2003  Dominique Richard  Port multi-compilateur
//*****************************************************************************
#ifndef CLCHAINE_H_DEJA_INCLU
#define CLCHAINE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include <string>

#ifndef INRS_SANS_STREAMS_DU_STANDARD
#   include <iosfwd>
#else
#   include "syiosfwd.hf"
#endif   //   INRS_SANS_STREAM_STANDARD

DECLARE_CLASS(CLChaine);

class CLChaine
{
public:
   static const Car TABULATEUR;

                  CLChaine                   ();
                  CLChaine                   (ConstCarP);
                  CLChaine                   (const Car);
                  CLChaine                   (const std::string&);
                  CLChaine                   (const CLChaine&);
   virtual        ~CLChaine                  ();

   void           ajoute                     (ConstCarP);
   void           ajoute                     (const Car c);
   void           ajoute                     (const CLChaine &ch);
   virtual void   ajoute                     (const Entier&);
   virtual void   ajoute                     (const EntierN&);
   virtual void   ajoute                     (const DReel&);
   virtual void   ajoute                     (const Reel&);
   virtual void   ajouteHeure                (const EntierN&);
   virtual void   ajouteHeure                (const Reel&);
   virtual void   ajouteHeure                (const DReel&);
   virtual void   ajouteHeureMinutes         (const EntierN&);
   virtual void   ajouteHeureMinutesSecondes (const EntierN&);
   void           alloueMemoire              (const EntierN);
   void           asgCaractere               (const Car, const EntierN);
   Booleen        contient                   (const CLChaine&) const;
   Booleen        convertis                  (Entier&) const;
   Booleen        convertis                  (EntierN&) const;
   Booleen        convertis                  (DReel&) const;
   Booleen        convertis                  (Reel&) const;
   Booleen        convertisHeureEnSec        (Reel&) const;
   Booleen        convertisHeureEnSec        (DReel&) const;
   Booleen        convertisHeureEnSec        (EntierN&) const;
   void           efface                     ();
   void           elimineCaractere           (const Car);
   void           enleveCaracteres           (const EntierN, const EntierN=1);
   void           enleveDebut                (const EntierN);
   void           enleveEspaces              ();
   void           enleveEspacesDebut         ();
   void           enleveEspacesFin           ();
   void           enleveFin                  (const EntierN);
   Booleen        estVide                    () const;
   void           insere                     (const EntierN, ConstCarP);
   void           insere                     (const EntierN, const Car);
   void           insere                     (const EntierN, const CLChaine&);
   void           majuscule                  ();
   void           minuscule                  ();
   Booleen        recherche                  (EntierN&, const Car, const EntierN = 0) const;
   Booleen        recherche                  (EntierN&, ConstCarP, const EntierN = 0) const;
   Booleen        recherche                  (EntierN&, const CLChaine&, const EntierN = 0) const;
   Booleen        rechercheArriere           (EntierN&, const Car,
                                              const Entier = -1) const;
   Booleen        rechercheArriere           (EntierN&, ConstCarP,
                                              const Entier = -1) const;
   Booleen        rechercheArriere           (EntierN&, const CLChaine&,
                                              const Entier =-1) const;
   ConstCarP      reqConstCarP               () const;
   void           reqSousChaine              (CLChaine&, const EntierN, const EntierN) const;
   void           reqSousChaineDroite        (CLChaine&, const EntierN) const;
   void           reqSousChaineDroite        (CLChaine&, const Car) const;
   void           reqSousChaineGauche        (CLChaine&, const EntierN) const;
   void           reqSousChaineGauche        (CLChaine&, const Car) const;
   EntierN        reqTaille                  () const;
   void           tronque                    (const EntierN);

   CLChaine& operator << (ConstCarP);
   CLChaine& operator << (const Car);
   CLChaine& operator << (const CLChaine&);
   CLChaine& operator << (const Entier&);
   CLChaine& operator << (const EntierN&);
   CLChaine& operator << (const Reel&);
   CLChaine& operator << (const DReel&);
   CLChaine& operator =  (ConstCarP);
   CLChaine& operator =  (const Car);
   CLChaine& operator =  (const std::string&);
   CLChaine& operator =  (const CLChaine&);
   CLChaine  operator +  (ConstCarP) const;
   CLChaine  operator +  (const Car) const;
   CLChaine  operator +  (const CLChaine&) const;
   CLChaine& operator += (ConstCarP);
   CLChaine& operator += (const Car);
   CLChaine& operator += (const CLChaine&);
   Booleen   operator == (ConstCarP) const;
   Booleen   operator == (const Car) const;
   Booleen   operator == (const std::string&) const;
   Booleen   operator == (const CLChaine&) const;
   Booleen   operator != (ConstCarP) const;
   Booleen   operator != (const Car) const;
   Booleen   operator != (const CLChaine&) const;
   Booleen   operator >  (ConstCarP) const;
   Booleen   operator >  (const Car) const;
   Booleen   operator >  (const CLChaine&) const;
   Booleen   operator <  (ConstCarP) const;
   Booleen   operator <  (const Car) const;
   Booleen   operator <  (const CLChaine&) const;
   Booleen   operator >= (ConstCarP) const;
   Booleen   operator >= (const Car) const;
   Booleen   operator >= (const CLChaine&) const;
   Booleen   operator <= (ConstCarP) const;
   Booleen   operator <= (const Car) const;
   Booleen   operator <= (const CLChaine&) const;
   Car       operator[]  (const EntierN) const;
   Car       reqCaractere(const EntierN) const;
             operator std::string () const;

   friend CLChaine operator +  (ConstCarP, const CLChaine&);
   friend CLChaine operator +  (const Car, const CLChaine&);
   friend Booleen  operator == (ConstCarP, const CLChaine&);
   friend Booleen  operator == (const Car, const CLChaine&);
   friend Booleen  operator == (const std::string&, const CLChaine&);
   friend Booleen  operator != (ConstCarP, const CLChaine&);
   friend Booleen  operator != (const Car, const CLChaine&);
   friend Booleen  operator >  (ConstCarP, const CLChaine&);
   friend Booleen  operator >  (const Car, const CLChaine&);
   friend Booleen  operator <  (ConstCarP, const CLChaine&);
   friend Booleen  operator <  (const Car, const CLChaine&);
   friend Booleen  operator >= (ConstCarP, const CLChaine&);
   friend Booleen  operator >= (const Car, const CLChaine&);
   friend Booleen  operator <= (ConstCarP, const CLChaine&);
   friend Booleen  operator <= (const Car, const CLChaine&);

   friend INRS_IOS_STD::ostream& operator<< (INRS_IOS_STD::ostream&, const CLChaine&);
   friend INRS_IOS_STD::istream& operator>> (INRS_IOS_STD::istream&, CLChaine&);

protected:
   void           invariant(ConstCarP) const;

private:
   CarP           chaineP;
   EntierN        dimension;

   static const EntierN TAILLE_BLOC;
};

//*****************************************************************************
// Sommaire: Invariant de classe.
//
// Description:
//    La chaine doit toujours être non NUL
//
// Entrée:
//     ConstCarP condition:   PRECONDITION ou POSTCONDITION
//
// Notes:
//
//*****************************************************************************
inline void CLChaine::invariant(ConstCarP) const
{
}

#include "clchaine.hpp"

#endif // ifndef CLCHAINE_H_DEJA_INCLU
