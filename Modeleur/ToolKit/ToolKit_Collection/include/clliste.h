//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: clliste.h
//
// Classe:  CLListe
//
// Description:   Liste de forme vectoriel de type VoidP.
//
// Attributs:
//     Entier    blocAllocation              Grandeur de la section de la liste
//     EniterN   indice                      Nombre d'éléments dans la liste
//     EntierN   nbrBloc                     Nombre de sections de la liste
//     VoidP     *tetePV                     Le pointeur à la liste d'éléments
//
// Notes:
//
//************************************************************************
// 26-01-1992  Marc Hughes        Version initiale
// 07-06-1994  André Houde        Ajout de la fonction template videListe
// 07-02-1996  Yves Roy           Revision de code
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#ifndef CLLISTE_H_DEJA_INCLU
#define CLLISTE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

const  EntierN DIMVEC = 10;   // Section de mémoire allouée par
                             // défault (nbrBloc * DIMVEC).

DECLARE_CLASS(CLListe);

class CLListe
{
private:
   Entier   blocAllocation;
   Entier   indice;
   Entier   nbrBloc;
   VoidP    *tetePV;

   void     realloue         ();

protected:
   void     invariant        (ConstCarP) const;

public:
            CLListe          (EntierN = DIMVEC);
            CLListe          (const CLListe&);
            CLListe          (ConstVoidP);
            ~CLListe         ();
   void     ajoute           (VoidP);
   void     alloue           (const EntierN&);
   void     detruis          (const EntierN&);
   void     dimListe         (EntierN&) const;
   void     effaceLstChaines ();
   void     effacerListe     ();
   void     insere           (VoidP, const EntierN&);
   void     nbrElem          (EntierN&) const;
   EntierN  dimension        () const;
   Entier   recherche        (EntierN&, VoidP) const;
   void     remplace         (VoidP, const EntierN&);
   void     reqElement       (VoidP&, const EntierN&) const;
   void     tete             (VoidP*&) const;

   CLListe& operator =       (const CLListe&);
   VoidP&   operator[]       (const EntierN&) const;

}; // Classe CLListe


//**************************************************************
// Description:
//
// Entrée:
//     CarP condition:   PRECONDITION ou POSTCONDITION
//
// Sortie:
//     Fin du programme en cas d'erreur.
//
// Notes:
//
//****************************************************************
#ifdef MODE_DEBUG
inline void CLListe::invariant(ConstCarP conditionP) const
{
   INVARIANT(indice >= 0 && indice <= (nbrBloc*blocAllocation), conditionP);
}
#else
inline void CLListe::invariant(ConstCarP /*conditionP*/) const
{
}
#endif

#include "clliste.hpp"

#endif // CLLISTE_H_DEJA_INCLU




