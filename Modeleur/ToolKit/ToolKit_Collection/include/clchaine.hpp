//*****************************************************************************
// $Id$
// $Date$
//*****************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//*****************************************************************************
// Fichier: clchaine.hpp
// Classe:  CLChaine
//*****************************************************************************
// 24-02-1993  André Houde        Version initiale
// 27-09-1995  Yves Roy           Révision de l'interface et de l'implémentation
//                                Transfert des préconditions, postconditions
//                                et invariant vers la nouvelle façon de faire.
// 08-12-1995  Yves Roy           Renforcement des contrats
// 04-04-1998  Yves Secretan      Profiling
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 27-05-2003  Dominique Richard  Port multi-compilateur
//*****************************************************************************
#ifndef CLCHAINE_HPP_DEJA_INCLU
#define CLCHAINE_HPP_DEJA_INCLU

#include <string.h>

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn -inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

//*****************************************************************************
// Sommaire: Remplace un caractère.
//
// Description:
//    Remplace un caractère dans la chaîne à la position voulue.
//    On ne permet pas à l'usager d'assigner un '\0' dans la chaine pour
//    ne pas lui laisser penser que l'implentation n'est rien d'autre qu'une
//    chaine C.  La notion de '\0' n'existe plus dans le contexte d'utilisation
//    de cette chaine.
//
// Entrée:
//    Car     carSource : Nouveau caractère qui n'est pas '\0'.
//    EntierN position  : Position valide dans la chaîne.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
inline void CLChaine::asgCaractere(const Car carSource, const EntierN position)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position < strlen(chaineP));
   PRECONDITION(carSource != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   chaineP[position] = carSource;

#ifdef MODE_DEBUG
   POSTCONDITION(chaineP[position] == carSource);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::asgCaractere


//*****************************************************************************
// Sommaire:    Détermine si la chaîne courante contient la chaîne passée en paramètre.
//
// Description: La méthode publique Booleen contient(const CLChaine&) permet
//              de déterminer si la chaîne contient celle passée en paramètre.
//
// Entrée:      const CLChaine& ch : la chaîne à rechercher.
//
// Sortie:      Retourne VRAI si la chaîne contient ch et FAUX autrement.
//
// Notes:
//
//*****************************************************************************
inline Booleen CLChaine::contient(const CLChaine& ch) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   EntierN pos;
   return recherche(pos, ch);
}

//*****************************************************************************
// Sommaire: Vide la chaine de caractère.
//
// Description:
//    Vide le contenu de la chaîne. Elle devient alors égale à
//    la chaine vide (et non pas la chaine NUL).
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
inline void CLChaine::efface()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   chaineP[0] = '\0';

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::efface

//*****************************************************************************
// Sommaire: Retourne VRAI si la chaine est vide.
//
// Description:
//    Retourne un Booleen à VRAI si la chaine est vide
//    et à FAUX autrement.
//
// Entrée:
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen CLChaine::estVide() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return strlen(chaineP) == 0;
}

//*****************************************************************************
// Sommaire: Insère un caractère à une position donnée.
//
// Description:
//   Insère un caractère à une position donnée dans la chaine.
//
// Entrée:
//    EntierN   position    : emplacement où l'on droit insérer la sous-chaîne
//    Car       caractere   : caractère à insérer
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline void CLChaine::insere(const EntierN position, const Car c)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   PRECONDITION(position <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;
   insere(position, strP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::insere

//*****************************************************************************
// Sommaire: Insère un CLChaine à une position donnée.
//
// Description:
//   Insère une sous-chaîne CLChaine à une position donnée dans la chaine.
//
// Entrée:
//    EntierN   position    : emplacement où l'on droit insérer la sous-chaîne
//    const CLChaine ch     : sous-chaîne à insérer
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline void CLChaine::insere(const EntierN position, const CLChaine& ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   insere(position, ch.reqConstCarP());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::insere


//*****************************************************************************
// Sommaire: Recherche un caractère.  Si trouvé, donne la position.
//
// Description:
//    Procure la position de la première occurence du caractère en commençant
//    la recherche à la position spécifiée (0 par défaut)
//
// Entrée:
//    Car car            : caractere recherché.
//    EntierN posiDepart : Position de début de recherche
//
// Sortie:
//    Position de début de la chaîne.
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::recherche(EntierN &positionTrouve,
                                   const Car c,
                                   const EntierN positionDepart) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   PRECONDITION(positionDepart <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;
   return recherche(positionTrouve, strP, positionDepart);
}  // CLChaine::recherche(EntierN&, Car, EntierN)


//*****************************************************************************
// Sommaire: Recherche une sous-chaine à partir d'une position donnée
//           dans la chaine.
//
// Description:
//    Procure la position du début de la sous-chaîne en commençant
//    la recherche à la position spécifiée (0 par défaut)
//
// Entrée:
//    const CLChaine& ch        : Chaîne recherchée.
//    EntierN   positionDepart  : Position de début de recherche
//
// Sortie:
//    Position de début de la chaîne.
//
// Notes:
//
//*****************************************************************************
inline Booleen CLChaine::recherche(EntierN &positionTrouve,
                                   const CLChaine& ch,
                                   const EntierN positionDepart) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(positionDepart <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return recherche(positionTrouve, ch.reqConstCarP(), positionDepart);
}

//*****************************************************************************
// Sommaire: Recherche un caractère à rebours.
//
// Description:
//    Effectue une recherche à l'envers, soit de la fin vers le début.
//    Procure la position de la première occurence du caractère en commeçant
//    la recherche à la position spécifiée (-1 par défaut, signifiant
//    la position de fin de chaîne)
//
// Entrée:
//    Car car                : caractere recherché.
//    Entier  positionDepart : Position de début de recherche
//
// Sortie:
//    EntierN& positionTrouve : Position du caractère.
//
// Notes:
//
//*****************************************************************************
inline Booleen CLChaine::rechercheArriere(EntierN &positionTrouve,
                                          const Car c,
                                          const Entier positionDepart) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   PRECONDITION(positionDepart <= Entier(strlen(chaineP)));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;
   return rechercheArriere(positionTrouve, strP, positionDepart);

}  // CLChaine::rechercheArriere(...const Car...)

//*****************************************************************************
// Sommaire: Recherche à rebours d'un sous-chaine
//
// Description:
//    Effectue une recherche à l'envers, soit de la fin vers le début.
//    Procure la position du début de la sous-chaîne en commençant
//    la recherche à la position spécifiée (-1 par défaut, signifiant
//    la position de fin de chaîne)
//
// Entrée:
//    const CLChaine &ch      : chaîne recherchée.
//    Entier  positionDepart  : Position de début de recherche
//
// Sortie:
//    EntierN& positionTrouve : Position de début de la chaîne.
//
// Notes:
//
//*****************************************************************************
inline Booleen CLChaine::rechercheArriere(EntierN& positionTrouve,
                                          const CLChaine& ch,
                                          const Entier positionDepart) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(positionDepart <= Entier(strlen(chaineP)));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return rechercheArriere(positionTrouve, ch.reqConstCarP(), positionDepart);
}

//*****************************************************************************
// Sommaire: Retourne un pointeur constant sur la chaine de caractères.
//
// Description:
//    La méthode retourne un pointeur constant sur la chaine de caractères à
//    l'interne.  On ne doit pas conserver ce pointeur ailleurs.  Pour usage
//    immédiat seulement.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
inline ConstCarP CLChaine::reqConstCarP() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

    return chaineP;
}  // CLChaine::reqCarP

//*****************************************************************************
// Sommaire: Retourne la taille de la chaine de caractères
//
// Description:
//    Donne la taille de la chaîne sans compter le caractère NUL.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
inline EntierN CLChaine::reqTaille() const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

    return static_cast<EntierN>(strlen(chaineP));
}  // CLChaine::reqTaille

//*****************************************************************************
// Sommaire: Retourne le caractère à la position X.
//
// Description:
//    Procure le caractère à la position X par valeur
//
// Entrée:
//    EntierN position : Position du caractère
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Car CLChaine::reqCaractere(const EntierN position) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position < strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return chaineP[position];
}  // CLChaine::reqCaractere

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur []
//
// Description:
//    Retourne la référence à un caractère. Permet une lecture
//    seulement.
//
// Entrée:
//
// Sortie:
//    Le caractère à la position pos est retourné.
//
// Notes:
//*****************************************************************************
inline Car CLChaine::operator[](const EntierN pos) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(pos < strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return chaineP[pos];
}  // CLChaine::operator[]

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'assignation
//
// Description:
//    Affecte cette chaîne avec le contenu du paramètre.
//
// Entrée:
//    const CarP strP : Chaîne à affecter.
//
// Sortie:
//
// Notes:  On ne peut teste l'auto-assignation parce que l'opérateur ne porte
//         pas sur un CLChaine.
//*****************************************************************************
inline CLChaine& CLChaine::operator=(ConstCarP strP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   efface();
   alloueMemoire(static_cast<EntierN>(strlen(strP))+1);
   strcpy_s(chaineP, dimension, strP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this);
}  //lint !e1529      Not first checking for assignement to this
   // CLChaine::operator=

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'assignation
//
// Description:
//    Affecte cette chaîne avec le contenu du paramètre.
//
// Entrée:
//    const Car caractère : Caractère à affecter
//
// Sortie:
//
// Notes:  On ne peut teste l'auto-assignation parce que l'opérateur ne porte
//         pas sur un CLChaine.
//*****************************************************************************
inline CLChaine& CLChaine::operator=(const Car c)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;
   *this = strP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this);
}  //lint !e1529      Not first checking for assignement to this

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'assignation
//
// Description:
//    Affecte cette chaîne avec le contenu du paramètre.
//
// Entrée:
//    const CLChaine& obj : Chaîne à affecter.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline CLChaine& CLChaine::operator=(const std::string& str)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return (*this) = str.c_str();
}  // CLChaine::operator=

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'assignation
//
// Description:
//    Affecte cette chaîne avec le contenu du paramètre.
//
// Entrée:
//    const CLChaine& obj : Chaîne à affecter.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline CLChaine& CLChaine::operator=(const CLChaine& obj)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   if (this != &obj)
   {
      *this = obj.chaineP;  // --- Appel de operator=(ConstCarP)
   }

#ifdef MODE_DEBUG
   POSTCONDITION(*this == obj);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this);
}  // CLChaine::operator=

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'addition.
//
// Description:
//    Additionne cette chaine et le CLChaine reçu en paramètre et
//    retourne le résultat par valeur.  La fonction appelle l'operator+
//    avec un ConstCarP pour faire le travail.
//
// Entrée:
//    const CLChaine &obj : CLChaine à ajouter
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline CLChaine CLChaine::operator+(const CLChaine &obj) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return (*this) + obj.chaineP;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur += avec un ConstCarP en paramètre
//
// Description:
//    Additionne un ConstCarP à la chaine courante.
//
// Entrée:
//    ConstCarP strP : Une chaine.
//
// Sortie:
//
// Notes:  Appelle la méthode ajoute(ConstCarP)
//*****************************************************************************
inline CLChaine& CLChaine::operator+=(ConstCarP strP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ajoute(strP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this);
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur += avec un caractère en paramètre
//
// Description:
//    Additionne un caractère à la chaine courante.
//
// Entrée:
//    const Car c : Un caractère
//
// Sortie:
//
// Notes:  Appelle la méthode operator+=(ConstCarP)
//*****************************************************************************
inline CLChaine& CLChaine::operator+=(const Car c)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

  Car strP[2] = "0";
   *strP = c;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this) += strP;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur += avec un const CLChaine& en paramètre
//
// Description:
//    Additionne un const CLChaine& à la chaine courante.
//
// Entrée:
//    const CLChaine &obj : Une chaine.
//
// Sortie:
//
// Notes:  Appelle la méthode ajoute(ConstCarP)
//*****************************************************************************
inline CLChaine& CLChaine::operator+=(const CLChaine &obj)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return (*this) += obj.chaineP;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'insertion.
//
// Description: La méthode accepte un caractère et l'ajoute à la fin de la
//              chaine.
//
// Entrée:
//    const Car c : Caractère à ajouter
//
// Sortie:
//
// Notes: On utilise la méthode ajoute(const Car)
//*****************************************************************************
inline CLChaine& CLChaine::operator<<(const Car c)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
#endif   // #ifdef MODE_DEBUG

   ajoute(c);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return *this;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'insertion.
//
// Description: La méthode accepte un ConstCarP et l'ajoute à la fin de la
//              chaine.
//
// Entrée:
//    ConstCarP strP : Chaine à ajouter
//
// Sortie:
//
// Notes: On utilise la méthode ajoute(ConstCarP)
//*****************************************************************************
inline CLChaine& CLChaine::operator<<(ConstCarP cP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(cP != NUL);
#endif   // #ifdef MODE_DEBUG

   ajoute(cP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return *this;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'insertion.
//
// Description: La méthode accepte un CLChaine& et l'ajoute à la fin de la
//              chaine.
//
// Entrée:
//    const CLChaine &obj: Chaine à ajouter
//
// Sortie:
//
// Notes: On utilise la méthode ajoute(const CLChaine&)
//*****************************************************************************
inline CLChaine& CLChaine::operator<<(const CLChaine &c)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ajoute(c);
   return *this;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'insertion.
//
// Description: La méthode accepte un const Entier& et l'ajoute à la fin de la
//              chaine.
//
// Entrée:
//    const Entier &nombre: nombre à ajouter
//
// Sortie:
//
// Notes: On utilise la méthode ajoute(const Entier&)
//*****************************************************************************
inline CLChaine& CLChaine::operator<<(const Entier &n)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ajoute(n);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return *this;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'insertion.
//
// Description: La méthode accepte un const EntierN& et l'ajoute à la fin de la
//              chaine.
//
// Entrée:
//    const EntierN &nombre: nombre à ajouter
//
// Sortie:
//
// Notes: On utilise la méthode ajoute(const EntierN&)
//*****************************************************************************
inline CLChaine& CLChaine::operator<< (const EntierN &n)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ajoute(n);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this);
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'insertion.
//
// Description: La méthode accepte un const Reel& et l'ajoute à la fin de la
//              chaine.
//
// Entrée:
//    const Reel &nombre: nombre à ajouter
//
// Sortie:
//
// Notes: On utilise la méthode ajoute(const Reel&)
//*****************************************************************************
inline CLChaine& CLChaine::operator<< (const Reel &n)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ajoute(n);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this);
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'insertion.
//
// Description: La méthode accepte un const DReel& et l'ajoute à la fin de la
//              chaine.
//
// Entrée:
//    const DReel &nombre: nombre à ajouter
//
// Sortie:
//
// Notes: On utilise la méthode ajoute(const DReel&)
//*****************************************************************************
inline CLChaine& CLChaine::operator<< (const DReel &n)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ajoute(n);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return (*this);
}

//*****************************************************************************
// Sommaire: Opérateur de comparaison d'égalité.
//
// Description: La fonction utilise la fonction strcmp.
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//
// Sortie:
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::operator==(const std::string& str) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return strcmp(chaineP, str.c_str()) == 0;
}  // CLChaine::operator==

//*****************************************************************************
// Sommaire: Opérateur de comparaison d'égalité.
//
// Description: La fonction utilise la fonction strcmp.
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//
// Sortie:
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::operator==(ConstCarP strP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return strcmp(chaineP, strP) == 0;
}  // CLChaine::operator==

//*****************************************************************************
// Sommaire: Opérateur de comparaison d'égalité.
//
// Description: La fonction se sert de operator==(ConstCarP)
//
// Entrée:
//    const Car : caractère à comparer.
//
// Sortie:
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::operator==(const Car c) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;

   return (*this) == strP;
}

//*****************************************************************************
// Sommaire: Opérateur de comparaison d'égalité.
//
// Description: La fonction utilise la fonction strcmp.
//
// Entrée:
//    const CLChaine obj : CLChaîne à comparer.
//
// Sortie:
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::operator==(const CLChaine &obj) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return (*this) == obj.chaineP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND de surcharge de l'opérateur ==
//
// Description: Cette fonction reçoit un ConstCarP et un CLChaine&
//              et elle intervertit les arguments pour appeller
//              l'opérateur == de l'objet.
//
// Entrée:
//    ConstCarP strP  : Première chaine
//    const CLChaine& : L'autre chaine
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator==(const std::string& str, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return (ch == str);
}

//*****************************************************************************
// Sommaire: Fonction FRIEND de surcharge de l'opérateur ==
//
// Description: Cette fonction reçoit un ConstCarP et un CLChaine&
//              et elle intervertit les arguments pour appeller
//              l'opérateur == de l'objet.
//
// Entrée:
//    ConstCarP strP  : Première chaine
//    const CLChaine& : L'autre chaine
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator==(ConstCarP chP, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return (ch == chP);
}

//*****************************************************************************
// Sommaire: Fonction FRIEND de surcharge de l'opérateur ==
//
// Description: Cette fonction reçoit un caractère et un CLChaine&
//              et elle intervertit les arguments pour appeller
//              l'opérateur == de l'objet.

// Entrée:
//    const Car c     : Un caractère
//    const CLChaine& : L'autre chaine
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator==(const Car c, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return (ch == c);
}

//*****************************************************************************
// Sommaire: Opérateur de comparaison d'inégalité.
//
// Description: La fonction utilise la négation de l'opérateur
//              d'égalité.
//
// Entrée:
//    ConstCarP strP : La chaine à comparer.
//
// Sortie:
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::operator!=(ConstCarP strP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !(*this == strP);
}

//*****************************************************************************
// Sommaire: Opérateur de comparaison d'inégalité.
//
// Description: La fonction utilise la négation de l'opérateur
//              d'égalité.
//
// Entrée:
//    const Car c : le caractère à comparer.
//
// Sortie:
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::operator!=(const Car c) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !(*this == c);
}

//*****************************************************************************
// Sommaire: Opérateur de comparaison d'inégalité.
//
// Description: La fonction utilise la négation de l'opérateur
//              d'égalité.
//
// Entrée:
//    const CLChaine obj : CLChaîne à comparer.
//
// Sortie:
//
// Note:
//*****************************************************************************
inline Booleen CLChaine::operator!=(const CLChaine& obj) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !(*this == obj);
}

//*****************************************************************************
// Sommaire: Fonction FRIEND de surcharge de l'opérateur !=
//
// Description: Cette fonction reçoit un ConstCarP et un CLChaine&
//              et elle intervertit les arguments pour appeller
//              l'opérateur != de l'objet.

// Entrée:
//    ConstCarP strP  : Première chaine
//    const CLChaine& : L'autre chaine
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator!=(ConstCarP chP, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch != chP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND de surcharge de l'opérateur !=
//
// Description: Cette fonction reçoit un caractère et un CLChaine&
//              et elle intervertit les arguments pour appeller
//              l'opérateur != de l'objet.

// Entrée:
//    const Car c     : Un caractère
//    const CLChaine& : L'autre chaine
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator!=(const Car c, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch != c;
}

//*****************************************************************************
// Sommaire: Opérateur de comparaison >
//
// Description: La méthode utilise le strcmp
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen CLChaine::operator>(ConstCarP strP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return strcmp(chaineP, strP) > 0;
}  // CLChaine::operator>

//*****************************************************************************
// Sommaire: Opérateur de comparaison >
//
// Description: Compare un caractère avec la chaine courante
//
// Entrée:
//    const Car c : un caractère
//
// Sortie:
//
// Notes: On appelle operator>(ConstCarP)
//*****************************************************************************
inline Booleen CLChaine::operator>(const Car c) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;

   return (*this) > strP;
}

//*****************************************************************************
// Sommaire: Opérateur de comparaison >
//
// Description: Compare un const CLChaine & avec la chaine courante
//
// Entrée:
//    const CLChaine& : Chaîne à comparer.
//
// Sortie:
//
// Notes: La méthode appelle operator>(ConstCarP)
//*****************************************************************************
inline Booleen CLChaine::operator>(const CLChaine &obj) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return (*this) > obj.chaineP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND à la classe CLChaine pour l'opérateur
//           de comparaison >
//
// Description: Compare un ConstCarP avec un CLChaine. On inverse simplement
//              les arguments pour appeler operator<(ConstCarP) de l'objet.
//
// Entrée:
//    ConstCarP strP  : Chaîne à comparer.
//    const CLChaine& : L'autre chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator>(ConstCarP chP, const CLChaine &obj)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return obj < chP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND à la classe CLChaine pour l'opérateur
//           de comparaison >
//
// Description: Compare un caractère avec un CLChaine. On inverse simplement
//              les arguments pour appeler operator<(const Car) de l'objet.
//
// Entrée:
//    const Car c     : Caractère à comparer
//    const CLChaine& : L'autre chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator>(const Car c, const CLChaine &obj)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return obj < c;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator <
//
// Description: La méthode fait la comparaison entre ConstCarP et un CLChaine&.
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//
// Sortie:
//
// Notes: La méthode utilise le strcmp
//*****************************************************************************
inline Booleen CLChaine::operator<(ConstCarP strP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return strcmp(chaineP, strP) < 0;
}  // CLChaine::operator<

//*****************************************************************************
// Sommaire: Surcharge de l'opérator <
//
// Description: La méthode fait la comparaison entre un caractère
//              et un CLChaine&.
//
// Entrée:
//    const Car c: caractère à comparer.
//
// Sortie:
//
// Notes: La méthode appelle operator<(ConstCarP)
//*****************************************************************************
inline Booleen CLChaine::operator<(const Car c) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;

   return (*this) < strP;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator <
//
// Description: La méthode fait la comparaison deux CLChaine&.
//
// Entrée:
//    const CLChaine& : Chaîne à comparer.
//
// Sortie:
//
// Notes: La méthode appelle operator<(ConstCarP)
//*****************************************************************************
inline Booleen CLChaine::operator<(const CLChaine &obj) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return (*this) < obj.chaineP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND pour la surcharge de l'opérator <
//
// Description: La méthode fait la comparaison entre ConstCarP et un CLChaine&.
//              On inserse simplement les arguments et on appelle la méthode
//              de classe operator>(ConstCarP)
//
// Entrée:
//    ConstCarP strP  : Chaîne à comparer.
//    const CLChaine &: L'autre chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator<(ConstCarP chP, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch > chP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND pour la surcharge de l'opérator <
//
// Description: La méthode fait la comparaison entre un caractère et
//              un CLChaine&. On inserse simplement les arguments et
//              on appelle la méthode de classe operator>(const Car)
//
// Entrée:
//    const Car c     : Caractère à comparer
//    const CLChaine &: L'autre chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator<(const Car c, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch > c;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator >=
//
// Description: La méthode fait la comparaison avec un ConstCarP
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//
// Sortie:
//
// Notes: La méthode utilise la négation de operator<(ConstCarP)
//*****************************************************************************
inline Booleen CLChaine::operator>=(ConstCarP strP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !((*this) < strP);
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator >=
//
// Description: La méthode fait la comparaison avec un caractère
//
// Entrée:
//    const Car c : Caractère à comparer.
//
// Sortie:
//
// Notes: La méthode utilise la négation de operator<(const Car)
//*****************************************************************************
inline Booleen CLChaine::operator>=(const Car c) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !((*this) < c);
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator >=
//
// Description: La méthode fait la comparaison avec un const CLChaine&
//
// Entrée:
//    const CLChaine & : Chaîne à comparer.
//
// Sortie:
//
// Notes: La méthode utilise la négation de operator<(const CLChaine&)
//*****************************************************************************
inline Booleen CLChaine::operator>=(const CLChaine& obj) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !((*this) < obj);
}

//*****************************************************************************
// Sommaire: Fonction FRIEND à la classe CLChaine pour la surcharge de
//           l'opérator >=
//
// Description: La méthode fait la comparaison avec un ConstCarP et un
//              const CLChaine&.  On inverse simplement les arguments pour
//              appeler la méthode de classe operator<=(constCarP).
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//    const CLChaine&: La seconde chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator>=(ConstCarP chP, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch <= chP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND à la classe CLChaine pour la surcharge de
//           l'opérator >=
//
// Description: La méthode fait la comparaison avec un caractère et un
//              const CLChaine&.  On inverse simplement les arguments pour
//              appeler la méthode de classe operator<=(const Car).
//
// Entrée:
//    const Car c    : Caractère à comparer.
//    const CLChaine&: La seconde chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator>=(const Car c, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch <= c;
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator <=
//
// Description: La méthode fait la comparaison avec un ConstCarP
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//
// Sortie:
//
// Notes: La méthode utilise la négation de operator>(ConstCarP)
//*****************************************************************************
inline Booleen CLChaine::operator<=(ConstCarP strP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !((*this) > strP);
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator <=
//
// Description: La méthode fait la comparaison avec un caractère
//
// Entrée:
//    const Car c : Caractère à comparer.
//
// Sortie:
//
// Notes: La méthode utilise la négation de operator>(const Car)
//*****************************************************************************
inline Booleen CLChaine::operator<=(const Car c) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !((*this) > c);
}

//*****************************************************************************
// Sommaire: Surcharge de l'opérator <=
//
// Description: La méthode fait la comparaison avec un const CLChaine&
//
// Entrée:
//    const CLChaine& : Chaîne à comparer.
//
// Sortie:
//
// Notes: La méthode utilise la négation de operator>(const CLChaine&)
//*****************************************************************************
inline Booleen CLChaine::operator<=(const CLChaine &obj) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   return !((*this) > obj);
}

//*****************************************************************************
// Sommaire: Fonction FRIEND à la classe CLChaine pour la surcharge de
//           l'opérator <=
//
// Description: La méthode fait la comparaison avec un ConstCarP et un const
//              CLChaine&.  On inserse simplement les arguments pour appeler
//              la méthode de classe operator>=(ConstCarP).
//
// Entrée:
//    ConstCarP strP : Chaîne à comparer.
//    const CLChaine&  : L'autre chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator<=(ConstCarP chP, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch >= chP;
}

//*****************************************************************************
// Sommaire: Fonction FRIEND à la classe CLChaine pour la surcharge de
//           l'opérator <=
//
// Description: La méthode fait la comparaison avec un caractère et un const
//              CLChaine&.  On inserse simplement les arguments pour appeler
//              la méthode de classe operator>=(const Car).
//
// Entrée:
//    const Car c : Caractère à comparer.
//    const CLChaine&  : L'autre chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
inline Booleen operator<=(const Car c, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   return ch >= c;
}

#if defined(MODE_TRACE) || defined(MODE_PROFILE)
#  pragma warn .inl
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

#endif   // #ifdef CLCHAINE_HPP_DEJA_INCLU
