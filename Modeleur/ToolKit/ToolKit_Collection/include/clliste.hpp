//************************************************************************
// $Id$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: clliste.hpp
// Classe:  CLListe
//************************************************************************
// 07-02-1996  Yves Roy           Transfert de quelques méthodes inline
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************

//****************************************************************************
// Description:  Cette méthode publique dimension() retourne le nombre
//               d'éléments dans la liste.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************************
inline EntierN CLListe::dimension() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return indice;
}

//****************************************************************************
// Description:  Cette méthode publique nbrElem(...) retourne le nombre
//               d'éléments dans la liste.
//
// Entrée:
//
// Sortie:
//    EntierN& position.
//
// Notes:
//
//****************************************************************************
inline void CLListe::nbrElem(EntierN& position) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   position = indice;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//****************************************************************************
// Description:  Cette méthode publique dimListe(...) retourne la dimension
//               allouée de la liste, i.e. le nombre de blocs alloués
//               multipliés par la dimension du bloc.
//
// Entrée:
//
// Sortie:
//   EntierN& dimension
//
// Notes:
//
//****************************************************************************
inline void CLListe::dimListe(EntierN& dimension) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   dimension = nbrBloc * blocAllocation;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//****************************************************************************
// Description:
//    Fonction effaçant le contenu de la liste (chaque item) ainsi
//    que la liste elle même (appel de effacerListe()).  Cette fonction est
//    de type template et doit prendre un paramètre supplémentaire, soit la
//    classe représentant le type de données contenu dans la liste. L'appel
//    se fait avec la liste en premier paramètre et un constructeur de la classe
//    contenue en deuxième (ex: videCLListe(lst, CLChaine()); ).
//
// Entrée:
//    CLListe &lst   : Liste à vider.
//    A& p           : Paramètre bidon décrivant le type contenu dans la liste.
//
// Sortie:
//
// Notes:
//    Ceci est une fonction template car une méthode template (même statique)
//    est impossible sauf si la classe est template également.
//
//****************************************************************************
template <typename A> Entier videCLListe(CLListe& lst, const A&)
{
   EntierN nbr;
   lst.nbrElem(nbr);
   for (EntierN i=0; i < nbr; i++)
      delete (A*)lst[i];
   lst.effacerListe();
   return OK;
}  // videCLListe


