//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// NOM DU FICHIER: clvect.cpp
//************************************************************************
//
// Classe :       CLVecteur
//
// Attributs:
//    EntierN              bloc
//    EntierN              borneSuperieur;
//    EntierN              dim;
//    EntierN              indice;
//    EntierP              teteEntierP;
//    CLChaineP            teteChaineP;
//    RGOrdreGraphiqueP    teteOrdreGraphiqueP;
//    DReelP                teteReelP;
//    SYCouleurRGBP        teteRGColeurP;
//    Type                 type;
//
// Méthodes:
//           CLVecteur(Type, EntierN);
//           ~CLVecteur();
//   Entier     ajoute(Entier);
//   Entier     ajoute(DReel);
//   Entier     ajoute(RGOrdreGraphique&);
//   Entier     ajoute(CLChaine&);
//   Entier     ajoute(SYCouleurRGB&);
//   Entier     alloue(EntierN);
//   Entier     compacte();
//   Entier     detruis();
//   Entier     dimVec(EntierN &dimVec) { dimVec = dim; return OK;}
//   Entier     efface();
//   expCarre()          : Éleve chacun des éléments d'un vecteur a la puissance
//                         2(carré).
//   expMoinsUn()        : Éleve les éléments du vecteur a la puissance -1.
//   exposant(DReel)      : Est l'équivalent de 10^x de chaque élément.
//   expX(DReel)          : Éleve les élément a la puissance x spécifiée.
//   Entier     insere(Entier&, EntierN);
//   Entier     insere(DReel&, EntierN);
//   Entier     insere(RGOrdreGraphique&, EntierN);
//   Entier     insere(CLChaine, EntierN);
//   Entier     insere(SYCouleurRGB&, EntierN);
//   inverse()           : Inverse le signe de chaque élément du vecteur.
//   log()               : Logarithme(2) de chacun des éléments.
//   ln()                : Logarithme(10) de chacun des éléments.
//   maximum(DReel&)      : Trouve le maximum d'un vecteur.
//   maximum(DReel&, EntierN) : Trouve le maximum d'un vecteur et donne
//                             l'indice correspondant.
//   mediane(DReel&)      : Trouve la médiane d'un vecteur.
//   minimum(DReel&)      : Trouve le minimum d'un vecteur.
//   moyenne(DReel&)      : Trouve la moyenne d'un vecteur.
//   Entier     nbrElem(EntierN &nombre) { nombre = indice; return OK;}
//   opACos()            : Opération arc cosinus sur les éléments du vecteur.
//   opASin()            : Opération arc sinus sur les éléments du vecteur.
//   opATan()            : Opération arc tangeante sur les éléments du vecteur.
//   opCos()             : Opération de cosinus sur les éléments du vecteur.
//   opSin()             : Opération de sinus sur les éléments du vecteur.
//   opSqrt()            : Extrait la racine carrée des éléments du vecteur.
//   opTan()             : Opération de tangeante sur les éléments du vecteur.
//   prodScal(CLVecteur) : Produit scalaire de deux vecteurs.
//   Entier     remplace(Entier&, EntierN);
//   Entier     remplace(DReel&, EntierN);
//   Entier     remplace(RGOrdreGraphique&, EntierN);
//   Entier     remplace(CLChaine&, EntierN);
//   Entier     remplace(SYCouleurRGB&, EntierN);
//   Entier     reqElement(Entier&,EntierN);
//   Entier     reqElement(DReel&, EntierN);
//   Entier     reqElement(RGOrdreGraphique&, EntierN);
//   Entier     reqElement(CLChaine&, EntierN);
//   Entier     reqElement(SYCouleurRGB&, EntierN);
//   Entier     reqType(Type&)
//   Entier     tete(VoidP &);
//   Entier     tri();
//   variance(DReel&)     : Variance d'un vecteur.
//   zeroDedans(Booleen&): Vérification d'élément zéro dans le vecteur.
//   void       invariant(CarP, CarP);
//   CLVecteurP& operator+ (DReel)       : Additionne un réel a un vecteur.
//   CLVecteurP& operator+ (CLVecteur&) : Additionne deux vecteurs.
//   CLVecteurP& operator- (DReel)       : Soustrait un réel a un vecteur.
//   CLVecteurP& operator- (CLVecteur&) : Soustrait deux vecteurs.
//   CLVecteurP& operator* (DReel)       : Multiplie un réel avec un vecteur.
//   CLVecteurP& operator* (CLVecteur&) : Multiplie deux vecteurs élément
//                                        par élément.
//   CLVecteurP& operator/ (DReel)       : Divise un vecteur par un réel.
//   CLVecteurP& operator/ (CLVecteur&) : Divise deux vecteur élément par
//                                        élément.
//   CLVecteur& operator << (const Entier &n)  { ajoute(n);  return (*this); }
//   CLVecteur& operator << (const DReel &n)    { ajoute(n);  return (*this); }
//
// Fonctions:
//
//************************************************************************
// 16-11-1995  Serge Dufour       Arrangement des vect.tete(...) pour compilation watcom.
// 08-03-1996  Bernard Doyon      Ajoût de la méthode maximum(surcharge)
// 25-03-1996  Bernard Doyon      Révision de la méthode maximum(surcharge)
// 16-10-1997  Yves Secretan      Passage à la double précision : 1.ère passe
// 26-10-1997  Yves Secretan      Passage à la double précision : 2.ème passe
// 25-09-1998  Yves Secretan      Change le scope des for et while
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#include <math.h>
#include <string.h>

#include "clvect.h"
//#include "rgordgra.h"
#include "clchaine.h"
#include "syclrrgb.h"


//**************************************************************
// METHODE: CLVecteur::CLVecteur(Type, EntierN)
//
// Créé le 26 janvier 1993            par Marc Hughes
// Modifié le 29 juillet 1993 par Guy Tremblay
//**************************************************************
//
// Description:
//                 Ce constructeur intialise le pointeur au vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//                 Type typeVec   enumération de type de vecteur possible
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 indice == 0
//                 dim == 0
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur::CLVecteur(Type typeVec, EntierN dimVec)
{
   indice = 0;
   dim = 0;
//   borneSuperieur = 0;
   bloc = dimVec;
   type = typeVec;
   switch (type)
   {
      //case CLVecteur::RGORDREGRAPHIQUE:
      //   val.teteOrdreGraphiqueP = NUL;
      //   break;
      case CLVecteur::SYCHAINE:
         val.teteChaineP = NUL;
         break;
      case CLVecteur::SYCOULEURRGB:
         val.teteCouleurP = NUL;
         break;
      case CLVecteur::ENTIER:
         val.teteEntierP = NUL;
         break;
      case CLVecteur::REEL:
         val.teteReelP = NUL;
         break;
      default:
         break;
   }
   // --- Allocation de la mémoire pour le vecteur
   alloue(bloc);

#ifdef MODE_DEBUG
   POSTCONDITION(indice == 0);
   POSTCONDITION(dim == bloc);
   POSTCONDITION(type == typeVec);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Constructeur CLVecteur(Type)


//**************************************************************
// METHODE: CLVecteur::CLVecteur(CLVecteur&)
//
// Créé le 09 Décembre 1993  par Christian Marcoux
//**************************************************************
//
// Description:
//    Constructeur copie du CLVecteur.
//
// Visibilité:
//    public.
//
// Entrée:
//    CLVecteur& vect : Vecteur sur lequel la copie se base.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 indice == 0
//                 dim == 0
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur::CLVecteur(const CLVecteur& vect)
{
   VoidP teteP;
   dim = 0;
   indice = 0;
//   borneSuperieur = 0;
   bloc = vect.bloc;
   vect.reqType(type);

   // --- Allocation de l'espace mémoire de la meme taille que le
   // --- vecteur a copier.
   // --- Copie du contenu du vecteur par un memcpy.
   alloue(vect.dim);
   vect.tete(teteP);
   switch (type)
   {
      //case CLVecteur::RGORDREGRAPHIQUE:
      //   memcpy(val.teteOrdreGraphiqueP, teteP, dim*sizeof(RGOrdreGraphique));
      //   break;
      case CLVecteur::ENTIER:
         memcpy(val.teteEntierP, teteP, dim*sizeof(Entier));
         break;
      case CLVecteur::REEL:
         memcpy(val.teteReelP, teteP, dim*sizeof(DReel));
         break;
      case CLVecteur::SYCHAINE:
         memcpy(val.teteChaineP, teteP, dim*sizeof(CLChaine));
         break;
      case CLVecteur::SYCOULEURRGB:
         memcpy(val.teteCouleurP, teteP, dim*sizeof(SYCouleurRGB));
         break;
      default:
         break;
   }
//   borneSuperieur = dim - 1;
   indice = vect.indice;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

} // CLVecteur(CLVecteur&).


//**************************************************************
// METHODE: CLVecteur::~CLVecteur()
//
// Créé le 26 janvier 1993            par Marc Hughes
// Modifié le 29 juillet 1993 par Guy Tremblay
//**************************************************************
//
// Description:
//                 Le destructeur détrui le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur::~CLVecteur()
{
   switch (type)
   {
      //case CLVecteur::RGORDREGRAPHIQUE:
      //   delete [] val.teteOrdreGraphiqueP;
      //   val.teteOrdreGraphiqueP = NUL;
      //   break;
      case CLVecteur::SYCHAINE:
         delete [] val.teteChaineP;
         val.teteChaineP = NUL;
         break;
      case CLVecteur::SYCOULEURRGB:
         delete [] val.teteCouleurP;
         val.teteCouleurP = NUL;
         break;
      case CLVecteur::ENTIER:
         delete [] val.teteEntierP;
         val.teteEntierP = NUL;
         break;
      case CLVecteur::REEL:
         delete [] val.teteReelP;
         val.teteReelP = NUL;
         break;
      default:
         break;
   }
   indice = 0;
   dim = 0;
//   borneSuperieur = 0;
} // Destructeur ~CLVecteur()


//**************************************************************
// METHODE: CLVecteur::ajoute(Entier)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode ajoute un entier à la suite dans le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//                 Entier   valeur    Entier que l'on veut ajouter au vecteur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 val.teteEntierP[indiceAvant] == valeur
//                 indice == indiceAvant + 1
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::ajoute(Entier valeur)
{
#ifdef MODE_DEBUG
   EntierN indiceAvant = indice;
#endif  // ifdef MODE_DEBUG

   if ( indice >= dim)
   {
      alloue(bloc);
   }
   val.teteEntierP[indice] = valeur;
   indice++;
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(val.teteEntierP[indiceAvant] == valeur);
   POSTCONDITION(indice == indiceAvant + 1);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode ajoute(Entier)

//**************************************************************
// METHODE: CLVecteur::ajoute(DReel)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode ajoute un Réel à la suite dans le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//                 DReel   valeur   Réel que l'on veut ajouter au vecteur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 val.teteReelP[indiceAvant] == valeur
//                 indice == indiceAvant + 1
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::ajoute(DReel valeur)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
#ifdef MODE_DEBUG
   EntierN indiceAvant = indice;

#endif  // ifdef MODE_DEBUG

   if ( indice >= dim)
   {
      alloue(bloc);
   }
   val.teteReelP[indice] = valeur;
   indice++;
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(val.teteReelP[indiceAvant] == valeur);
   POSTCONDITION(indice == indiceAvant + 1);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode ajouter(DReel)

//**************************************************************
// METHODE: CLVecteur::ajoute(RGOrdreGraphique&)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode ajoute Ordre Graphique à la suite
//                 dans le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//                 RGOrdreGraphique&  valeur  Ordre graphique ajouter au vecteur
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 val.teteOrdreGraphiqueP[indiceAvant] == valeur
//                 indice == indiceAvant + 1
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
/*
Entier CLVecteur::ajoute(RGOrdreGraphique& valeur)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
#ifdef MODE_DEBUG
   EntierN indiceAvant = indice;
   
#endif  // ifdef MODE_DEBUG

   if ( indice >= dim)
   {
      alloue(bloc);
   }
   val.teteOrdreGraphiqueP[indice] = valeur;
   indice++;
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(indice == indiceAvant + 1);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode ajoute(RGOrdreGraphique)
*/

//**************************************************************
// METHODE: CLVecteur::ajoute(CLChaine)
//
// Créé le 29 juillet 1993            par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode ajoute une chaine à la suite
//                 dans le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//                 CLChaine&  valeur  Chaîne ajouter au vecteur
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 val.teteChaineP[indiceAvant] == valeur
//                 indice == indiceAvant + 1
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::ajoute(CLChaine &valeur)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
#ifdef MODE_DEBUG
   EntierN indiceAvant = indice;

#endif  // ifdef MODE_DEBUG

   if (indice >= dim)
   {
      alloue(bloc);
   }
   val.teteChaineP[indice] = valeur;
   indice++;
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(indice == indiceAvant + 1);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode ajoute(CLChaine&)

//**************************************************************
// METHODE: CLVecteur::ajoute(SYCouleurRGB&)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode ajoute une couleur à la suite
//                 dans le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//                 SYCouleurRGB&  valeur  Couleur ajouter au vecteur
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 val.teteCouleurP[indiceAvant] == valeur
//                 indice == indiceAvant + 1
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::ajoute(SYCouleurRGB &valeur)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
#ifdef MODE_DEBUG
   EntierN indiceAvant = indice;
   
#endif  // ifdef MODE_DEBUG

   if ( indice >= dim)
   {
      alloue(bloc);
   }
   val.teteCouleurP[indice] = valeur;
   indice++;
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(indice == indiceAvant + 1);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode ajoute(SYCouleurRGB&)


//**************************************************************
// METHODE: CLVecteur::alloue(EntierN)
//
// Créé le 26 janvier 1993            par Marc Hughes
// Modifié le 29 juillet 1993 par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode permet d'allouer un vecteur de grandeur dimVec.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   dimVec   Grandeur de l'espace alloué
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//                 dimVec > 0
//
// Postconditions:
//                 dim == dimAvant + dimVec
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::alloue(EntierN dimVec)
{
#ifdef MODE_DEBUG
   PRECONDITION(dimVec > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
#ifdef MODE_DEBUG
   EntierN dimAvant = dim;

#endif  // ifdef MODE_DEBUG

   switch (type)
   {
      //case CLVecteur::RGORDREGRAPHIQUE:
      //{
      //   if (dim != 0)
      //   {
      //      RGOrdreGraphiqueP copieP = new RGOrdreGraphique[dim + dimVec];
      //      memcpy(copieP,val.teteOrdreGraphiqueP,dim*sizeof(RGOrdreGraphique));
      //      delete [] val.teteOrdreGraphiqueP;
      //      val.teteOrdreGraphiqueP = copieP;
      //      dim = dim + dimVec;
      //   }
      //   else
      //   {
      //      dim = dimVec;
      //      val.teteOrdreGraphiqueP = new RGOrdreGraphique[dim];
      //   }
      //   break;
      //}
      case CLVecteur::ENTIER:
      {
         if (dim != 0)
         {
            Entier *copieP = new Entier[dim + dimVec];
            memcpy(copieP,val.teteEntierP,dim*sizeof(Entier));
            delete [] val.teteEntierP;
            val.teteEntierP = copieP;
            dim = dim + dimVec;
         }
         else
         {
            dim = dimVec;
            val.teteEntierP = new Entier[dim];
         }
         break;
      }
      case CLVecteur::REEL:
      {
         if (dim != 0)
         {
            DReel *copieP = new DReel[dim + dimVec];
            memcpy(copieP,val.teteReelP,dim*sizeof(DReel));
            delete [] val.teteReelP;
            val.teteReelP = copieP;
            dim = dim + dimVec;
         }
         else
         {
            dim = dimVec;
            val.teteReelP = new DReel[dim];
         }
         break;
      }
      case CLVecteur::SYCHAINE:
      {
         if (dim != 0)
         {
            CLChaineP copieP = new CLChaine[dim + dimVec];
            memcpy(copieP,val.teteChaineP,dim*sizeof(CLChaine));
            delete [] val.teteChaineP;
            val.teteChaineP = copieP;
            dim = dim + dimVec;
         }
         else
         {
            dim = dimVec;
            val.teteChaineP = new CLChaine[dim];
         }
         break;
      }
      case CLVecteur::SYCOULEURRGB:
      {
         if (dim != 0)
         {
            SYCouleurRGBP copieP = new SYCouleurRGB[dim + dimVec];
            memcpy(copieP,val.teteCouleurP,dim*sizeof(SYCouleurRGB));
            delete [] val.teteCouleurP;
            val.teteCouleurP = copieP;
            dim = dim + dimVec;
         }
         else
         {
            dim = dimVec;
            val.teteCouleurP = new SYCouleurRGB[dim];
         }
         break;
      }
      default:
      {
         break;
      }
   }
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(dim == dimAvant + dimVec);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode alloue(EntierN)


//**************************************************************
// METHODE: CLVecteur::compacte()
//
// Créé le 26 janvier 1993  par Marc Hughes
// Modifiée le 29 juillet   par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode permet rendre disponible la mémoire qui n'est
//                 utilisé par le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                 dim == indice
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::compacte()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (indice != 0)
   {
      switch (type)
      {
         //case CLVecteur::RGORDREGRAPHIQUE:
         //{
         //   RGOrdreGraphique *copieP = new RGOrdreGraphique[indice];
         //   memcpy(copieP,val.teteOrdreGraphiqueP,indice*sizeof(RGOrdreGraphique));
         //   delete [] val.teteOrdreGraphiqueP;
         //   val.teteOrdreGraphiqueP = copieP;
         //   break;
         //}
         case CLVecteur::SYCHAINE:
         {
            CLChaine *copieP = new CLChaine[indice];
            memcpy(copieP,val.teteChaineP,indice*sizeof(CLChaine));
            delete [] val.teteChaineP;
            val.teteChaineP = copieP;
            break;
         }
         case CLVecteur::SYCOULEURRGB:
         {
            SYCouleurRGB *copieP = new SYCouleurRGB[indice];
            memcpy(copieP,val.teteCouleurP,indice*sizeof(SYCouleurRGB));
            delete [] val.teteCouleurP;
            val.teteCouleurP = copieP;
            break;
         }
         case CLVecteur::ENTIER:
         {
            Entier *copieP = new Entier[indice];
            memcpy(copieP,val.teteEntierP,indice*sizeof(Entier));
            delete [] val.teteEntierP;
            val.teteEntierP = copieP;
            break;
         }
         case CLVecteur::REEL:
         {
            DReel *copieP = new DReel[indice];
            memcpy(copieP,val.teteReelP,indice*sizeof(DReel));
            delete [] val.teteReelP;
            val.teteReelP = copieP;
            break;
         }
         default:
            break;
      } // switch
   } // if
   dim = indice;
//   borneSuperieur = indice - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(dim == indice);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode compacte()


//**************************************************************
// METHODE: CLVecteur::detruis(EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
// Modifié le 29 juillet 1993 par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode détruis un Element du vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position  Position dans le vecteur
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//                 position < indice
//
// Postconditions:
//                 indice == indiceAvant - 1
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::detruis(EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(position < indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
#ifdef MODE_DEBUG
   EntierN indiceAvant = indice;

#endif  // ifdef MODE_DEBUG

   if (indice <= 1)
      efface();
   else
   {
      switch (type)
      {
         //case CLVecteur::RGORDREGRAPHIQUE:
         //{
         //   for (Entier i = position; i < indice; i++)
         //      val.teteOrdreGraphiqueP[i] = val.teteOrdreGraphiqueP[i + 1];
         //   break;
         //}
         case CLVecteur::SYCHAINE:
         {
            for (EntierN i = position; i < indice; i++)
               val.teteChaineP[i] = val.teteChaineP[i + 1];
            break;
         }
         case CLVecteur::SYCOULEURRGB:
         {
            for (EntierN i = position; i < indice; i++)
               val.teteCouleurP[i] = val.teteCouleurP[i + 1];
            break;
         }
         case CLVecteur::ENTIER:
         {
            for (EntierN i = position; i < indice; i++)
               val.teteEntierP[i] = val.teteEntierP[i + 1];
            break;
         }
         case CLVecteur::REEL:
         {
            for (EntierN i = position; i < indice; i++)
               val.teteReelP[i] = val.teteReelP[i + 1];
            break;
         }
         default:
         {
            break;
         }
      }
      indice--;
//      borneSuperieur = dim - 1;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(indice == indiceAvant - 1);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode detruis(EntierN)


//**************************************************************
// METHODE: CLVecteur::efface()
//
// Créé le 26 janvier 1993            par Marc Hughes
// Modifié le 29 juillet 1993 par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode permet d'éffacer le vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//                  indice == 0
//                  dim == 0
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::efface()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   switch (type)
   {
      //case CLVecteur::RGORDREGRAPHIQUE:
      //   delete [] val.teteOrdreGraphiqueP;
      //   val.teteOrdreGraphiqueP = NUL;
      //   break;
      case CLVecteur::SYCHAINE:
         delete [] val.teteChaineP;
         val.teteChaineP = NUL;
         break;
      case CLVecteur::SYCOULEURRGB:
         delete [] val.teteCouleurP;
         val.teteCouleurP = NUL;
         break;
      case CLVecteur::ENTIER:
         delete [] val.teteEntierP;
         val.teteEntierP = NUL;
         break;
      case CLVecteur::REEL:
         delete [] val.teteReelP;
         val.teteReelP = NUL;
         break;
      default:
         break;
   }
   indice = 0;
   dim = 0;
//   borneSuperieur = 0;

#ifdef MODE_DEBUG
   POSTCONDITION(indice == 0);
   POSTCONDITION(dim == 0);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode effacer()


//**************************************************************
// METHODE: CLVecteur::expCarre()
//
// Crée le 12 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Éleve chacun des éléments d'un vecteur a la puissance 2(carré).
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::expCarre()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = pow(val.teteReelP[i], 2);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // expCarre()


//**************************************************************
// METHODE: CLVecteur::expMoinsUn()
//
// Crée le 12 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Éleve les éléments du vecteur a la puissance -1.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//    DIVIZERO : Division par zéro.
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::expMoinsUn()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Booleen bZero;

   // --- Vérification de la division par zéro.
   zeroDedans(bZero);
   if(!bZero)
   {
      for(EntierN i = 0;i < indice;i++)
      {
         val.teteReelP[i] = 1 / val.teteReelP[i];
      }
   }
   else
   {
      return DIVIZERO;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // expMoinsUn()


//**************************************************************
// METHODE: CLVecteur::exposant(DReel)
//
// Crée le 12 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Miltiplication de chacun des éléments par 10^x.
//
// Visibilité:
//    public
//
// Entrée:
//    DReel exp : Puissance a laquelle on éleve 10.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::exposant(DReel exp)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   DReel valTmp = pow(10, exp);
   for(EntierN i = 0;i < indice;i++)
      val.teteReelP[i] = val.teteReelP[i] * valTmp;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

  return OK;
} // exposant(DReel)


//**************************************************************
// METHODE: CLVecteur::expX(DReel)
//
// Crée le 12 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Élévation de chacun des éléments a la puissance x.
//
// Visibilité:
//    public
//
// Entrée:
//    DReel exp : puissance a laquelle on éleve le vecteur.
//
// Sortie:
//
// Codes d'erreur:
//    DIVIZERO : division par zéro.
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::expX(DReel exp)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (exp < DReel(0.0))
   {
      Booleen bZero;
      zeroDedans(bZero);
      if (bZero) return DIVIZERO;
   }

   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = pow(val.teteReelP[i], exp);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // expX(DReel)


//**************************************************************
// METHODE: CLVecteur::insere(Entier&, EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'insérer un element dans le
//                 vecteur de type Entier.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN  position    Position dans le vecteur
//
// Sortie:
//                 Entier   elem        Elément de retour
//
// Codes d'erreur:
//
// Préconditions:
//                 position >= 0
//
// Postconditions:
//                 elem == teteEntierP[position]
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::insere(Entier &elem, EntierN position)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (position >= indice)
   {
      while (position >= dim)
      {
         alloue(bloc);
      }
      val.teteEntierP[position] = elem;
      indice = position + 1;
   }
   else
   {
      if (indice >= dim)
         alloue(bloc);
      EntierP copieP = new Entier[dim];
      for (EntierN i = 0; i < position; ++i)
         copieP[i] = val.teteEntierP[i];
      copieP[position] = elem;
      for (EntierN i = position; i < indice; ++i)
         copieP[i + 1] = val.teteEntierP[i];
      indice++;
      delete [] val.teteEntierP;
      val.teteEntierP = copieP;
   }
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(val.teteEntierP[position] == elem);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode insere(Entier&, EntierN)


//**************************************************************
// METHODE: CLVecteur::insere(DReel&, EntierN)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'insérer un element dans le
//                 vecteur de type DReel.
//
// Visibilité:     publique
//
// Entrée:
//                 EntierN  position    Position dans le vecteur
//
// Sortie:
//                 Entier   elem        Elément de retour
//
// Codes d'erreur:
//
// Préconditions:
//                 position >= 0
//
// Postconditions:
//                 elem == val.teteReelP[position]
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************

Entier CLVecteur::insere(DReel &elem, EntierN position)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if ( position >= indice)
   {
      while (position >= dim)
      {
         alloue(bloc);
      }
      val.teteReelP[position] = elem;
      indice = position + 1;
   }
   else
   {
      if (indice >= dim)
         alloue(bloc);
      DReelP copieP = new DReel[dim];
      for (EntierN i = 0; i < position; ++i)
         copieP[i] = val.teteReelP[i];
      copieP[position] = elem;
      for (EntierN i = position; i < indice; ++i)
         copieP[i + 1] = val.teteReelP[i];
      indice++;
      delete [] val.teteReelP;
      val.teteReelP = copieP;
   }
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   POSTCONDITION(elem == val.teteReelP[position]);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode insere(DReel&, EntierN)

//**************************************************************
// METHODE: CLVecteur::insere(RGOrdreGraphique&, EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'insérer un element dans le vecteur
//                 de type Ordre Graphique.
//
// Visibilité:     public
//
// Entrée:
//                 EntierN  position    Position dans le vecteur
//
// Sortie:
//                 Entier   elem        Elément de retour
//
// Codes d'erreur:
//
// Préconditions:
//                 position >= 0
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
/*
Entier CLVecteur::insere(RGOrdreGraphique &elem, EntierN position)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if ( position >= indice)
   {
      while (position >= dim)
      {
         alloue(bloc);
      }
      val.teteOrdreGraphiqueP[position] = elem;
      indice = position + 1;
   }
   else
   {
      if (indice >= dim)
         alloue(bloc);
      RGOrdreGraphiqueP copieP = new RGOrdreGraphique[dim];
      for (Entier i = 0; i < position; ++i)
         copieP[i] = val.teteOrdreGraphiqueP[i];
      copieP[position] = elem;
      for (Entier i = position; i < indice; ++i)
         copieP[i + 1] = val.teteOrdreGraphiqueP[i];
      indice++;
      delete [] val.teteOrdreGraphiqueP;
      val.teteOrdreGraphiqueP = copieP;
   }
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode insere(RGOrdreGraphique&, EntierN)
*/

//**************************************************************
// METHODE: CLVecteur::insere(CLChaine&, EntierN)
//
// Crée le 27 juillet 1993            par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode permet d'insérer une chaîne dans le vecteur.
//
// Visibilité:     public
//
// Entrée:
//                 EntierN  position    Position dans le vecteur
//
// Sortie:
//                 Entier   elem        Elément de retour
//
// Codes d'erreur:
//
// Préconditions:
//                 position >= 0
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::insere(CLChaine &elem, EntierN position)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if ( position >= indice)
   {
      while (position >= dim)
      {
         alloue(bloc);
      }
      val.teteChaineP[position] = elem;
      indice = position + 1;
   }
   else
   {
      if (indice >= dim)
         alloue(bloc);
      CLChaineP copieP = new CLChaine[dim];
      for (EntierN i = 0; i < position; ++i)
         copieP[i] = val.teteChaineP[i];
      copieP[position] = elem;
      for (EntierN i = position; i < indice; ++i)
         copieP[i + 1] = val.teteChaineP[i];
      indice++;
      delete [] val.teteChaineP;
      val.teteChaineP = copieP;
   }
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode insere(CLChaine&, EntierN)

//**************************************************************
// METHODE: CLVecteur::insere(SYCouleurRGB&, EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'insérer un element dans le vecteur
//                 de type Ordre Graphique.
//
// Visibilité:     public
//
// Entrée:
//                 EntierN  position    Position dans le vecteur
//
// Sortie:
//                 Entier   elem        Elément de retour
//
// Codes d'erreur:
//
// Préconditions:
//                 position >= 0
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::insere(SYCouleurRGB &elem, EntierN position)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if ( position >= indice)
   {
      while (position >= dim)
      {
         alloue(bloc);
      }
      val.teteCouleurP[position] = elem;
      indice = position + 1;
   }
   else
   {
      if (indice >= dim)
         alloue(bloc);
      SYCouleurRGBP copieP = new SYCouleurRGB[dim];
      for (EntierN i = 0; i < position; ++i)
         copieP[i] = val.teteCouleurP[i];
      copieP[position] = elem;
      for (EntierN i = position; i < indice; ++i)
         copieP[i + 1] = val.teteCouleurP[i];
      indice++;
      delete [] val.teteCouleurP;
      val.teteCouleurP = copieP;
   }
//   borneSuperieur = dim - 1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode insere(SYCouleurRGB&, EntierN)


//**************************************************************
// METHODE: CLVecteur::inverse()
//
// Crée le 11 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Inversion du signe de chacun des éléments du vecteur.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::inverse()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = -val.teteReelP[i];
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // inverse()


//**************************************************************
// METHODE: CLVecteur::ln()
//
// Crée le 16 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Log naturel.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::ln()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // --- Verification du domaine d'application x >= 0.
   for (EntierN i = 0; i < indice; i++)
   {
      if(val.teteReelP[i] < 0)
      {
         return !OK;
      }
   }
   for (EntierN i = 0; i < indice; i++)
   {
      val.teteReelP[i] = log(val.teteReelP[i]);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // ln()


//**************************************************************
// METHODE: CLVecteur::log()
//
// Crée le 16 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Logorithme.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::loga()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = log10(val.teteReelP[i]);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // loga()


//**************************************************************
// METHODE: CLVecteur::maximum(DReel&)
//
// Crée le 11 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Cette méthode retourne le maximum du vecteur de réel.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//    DReel max : Maximum du vecteur.
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::maximum(DReel& max) const
{
#ifdef MODE_DEBUG
   PRECONDITION(indice > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   max = val.teteReelP[0];
   for(EntierN i = 0;i < indice;i++)
   {
      if(val.teteReelP[i] > max)
      {
         max = val.teteReelP[i];
      }
   }

#ifdef MODE_DEBUG
   POSTCONDITION(max >= val.teteReelP[0]);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // maximum(DReel&)


//**************************************************************
//
// Description:
//   Cette méthode retourne le maximum du vecteur de réel ainsi
//   que l'indice correspondant.
//
// Entrée:
//
// Sortie:
//   DReel&    max :  Maximum du vecteur.
//   EntierN& ind :  Indice de l'élément.
//
// Notes:
//
//**************************************************************
Entier CLVecteur::maximum (DReel& max, EntierN& ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(indice > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   max = val.teteReelP[0] ;
   ind = 0 ;
   for (EntierN i=0 ; i<indice ; i++)
   {
      if (val.teteReelP[i] > max)
      {
         max = val.teteReelP[i] ;
         ind = i ;
      }
   }

#ifdef MODE_DEBUG
   POSTCONDITION(max >= val.teteReelP[0]);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK ;

} // CLVecteur::maximum (DReel&, EntierN&)


//**************************************************************
// METHODE: CLVecteur::mediane(DReel&)
//
// Crée le 17 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Trouve la médiane d'un vecteur.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//    DReel med : Mediane du vecteur.
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::mediane(DReel& med)
{
#ifdef MODE_DEBUG
   PRECONDITION(indice > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   EntierN milieu;
   tri();
   milieu = indice % 2;
   if(milieu != 0)
   {
      med = val.teteReelP[EntierN(indice / 2 - 0.5)];
   }
   else
   {
      med = (val.teteReelP[EntierN(indice / 2 - 1)] + val.teteReelP[EntierN(indice / 2)]) / 2;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // mediane(DReel&)


//**************************************************************
// METHODE: CLVecteur::minimum(DReel&)
//
// Crée le 11 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Cette méthode retourne le minimum du vecteur de réel.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//    DReel min : minimum du vecteur.
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::minimum(DReel& min) const
{
#ifdef MODE_DEBUG
   PRECONDITION(indice > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   min = val.teteReelP[0];
   for(EntierN i = 0;i < indice;i++)
   {
      if(val.teteReelP[i] < min)
      {
         min = val.teteReelP[i];
      }
   }

#ifdef MODE_DEBUG
   POSTCONDITION(min <= val.teteReelP[0]);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // minimum(DReel&)


//**************************************************************
// METHODE: CLVecteur::moyenne(DReel&)
//
// Crée le 13 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Fait la moyenne d'un vecteur.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//    DReel moy : moyenne du vecteur.
//
// Codes d'erreur:
//
// Préconditions:
//    Le nombre de composante du vecteur doit etre supérieur a zéro.
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::moyenne(DReel& moy) const
{
#ifdef MODE_DEBUG
   PRECONDITION(indice > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   for(EntierN i = 0;i < indice;i++)
   {
         moy += val.teteReelP[i];
   }
   moy = moy / indice;

   return OK;
} // moyenne(DReel&)


//**************************************************************
// METHODE: CLVecteur::opACos()
//
// Crée le 16 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Opération arc cosinus.
//
// Visibilité:     public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//    DOMAINERR : Erreur de domaine [-1,1].
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::opACos()
{
   // --- Vérification du domaine.
   for (EntierN i = 0; i < indice; ++i)
   {
      if(!(val.teteReelP[i] > -1) && (val.teteReelP[i] < 1))
      {
         return DOMAINERR;
      }
   }
   for (EntierN i = 0; i < indice; ++i)
   {
      val.teteReelP[i] = acos(val.teteReelP[i]);
   }
   return OK;
} // opACos()


//**************************************************************
// METHODE: CLVecteur::opASin()
//
// Crée le 16 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Opération arc sinus.
//
// Visibilité:     public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//    DOMAINERR : Erreur de domaine [-1,1].
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::opASin()
{
   // --- Vérification du domaine.
   for (EntierN i = 0; i < indice; ++i)
   {
      if(!(val.teteReelP[i] > -1) && (val.teteReelP[i] < 1))
      {
         return DOMAINERR;
      }
   }
   for (EntierN i = 0; i < indice; ++i)
   {
      val.teteReelP[i] = asin(val.teteReelP[i]);
   }
   return OK;
} // opASin()


//**************************************************************
// METHODE: CLVecteur::opATan()
//
// Crée le 16 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Opération arc tangeante.
//
// Visibilité:     public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::opATan()
{
   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = atan(val.teteReelP[i]);
   }
   return OK;
} // opATan()


//**************************************************************
// METHODE: CLVecteur::opCos()
//
// Crée le 12 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Opération cosinus.
//
// Visibilité:     public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::opCos()
{
   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = cos(val.teteReelP[i]);
   }
   return OK;
} // opCos()


//**************************************************************
// METHODE: CLVecteur::opSin()
//
// Crée le 12 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Opération sinus.
//
// Visibilité:     public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::opSin()
{
   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = sin(val.teteReelP[i]);
   }
   return OK;
} // opSin()


//**************************************************************
// METHODE: CLVecteur::opSqrt()
//
// Crée le 16 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Extraction de la racine carrée.
//
// Visibilité:
//    public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//    DOMAINERR : Erreur de domaine [0,inf
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::opSqrt()
{
   // --- Vérification du domaine.
   for (EntierN i = 0; i < indice; ++i)
   {
      if (val.teteReelP[i] < DReel(0.0)) return DOMAINERR;
   }

   for (EntierN i = 0; i < indice; ++i)
   {
      val.teteReelP[i] = sqrt(val.teteReelP[i]);
   }
   return OK;
} // opSqrt()


//**************************************************************
// METHODE: CLVecteur::opTan()
//
// Crée le 13 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Opération tangeante.
//
// Visibilité:     public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//    Multiple de pi/2 donne une erreur.
//
//**************************************************************
Entier CLVecteur::opTan()
{
   // --- Multiple de pi/2 donne une erreur.
   for(EntierN i = 0;i < indice;i++)
   {
      val.teteReelP[i] = tan(val.teteReelP[i]);
   }
   return OK;
} // opTan()


//**************************************************************
// METHODE: CLVecteur::operator +(DReel)
//
// Crée le 9 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Addition du réel a un vecteur.
//
// Visibilité:     public
//
// Entrée:
//    DReel nombre : Nombre a aditionner.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator +(const DReel nombre)
{
   // --- Variables temporaires.
   DReel reponse;
   CLVecteur retour(REEL);

   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] + nombre;
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator +(DReel)


//**************************************************************
// METHODE: CLVecteur::operator +(CLVecteur&)
//
// Crée le 9 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Addition de deux vecteurs élément par élément.
//
// Visibilité:     public
//
// Entrée:
//    CLVecteur vect : Deuxieme vecteur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator +(const CLVecteur& vect)
{
   // --- Variables temporaires.
   DReel reponse;
   VoidP tempP = NUL;
   CLVecteur retour(REEL);

   vect. tete(tempP);
   DReelP teteP = DReelP(tempP);
   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] + teteP[i];
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator +(CLVecteur&)


//**************************************************************
// METHODE: CLVecteur::operator- (DReel)
//
// Crée le 11 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Opération de soustraction d'un réel aux éléments du vecteur.
//
// Visibilité:     public
//
// Entrée:
//    DReel nombre : Nombre a soustraire.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator -(const DReel nombre)
{
   // --- Variables temporaires.
   DReel reponse;
   CLVecteur retour(REEL);

   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] - nombre;
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator -(DReel)


//**************************************************************
// METHODE: CLVecteur::operator -(CLVecteur&)
//
// Crée le 11 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Soustraction de deux vecteur de meme dimension.
//
// Visibilité:     public
//
// Entrée:
//    CLVecteur vect : Deuxieme vecteur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator -(const CLVecteur& vect)
{
   // --- Variables temporaires.
   DReel reponse;
   VoidP tempP = NUL;
   CLVecteur retour(REEL);
                  
   vect.tete(tempP);
   DReelP teteP = DReelP(tempP);
   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] - teteP[i];
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator -(CLVecteur&)


//**************************************************************
// METHODE: CLVecteur::operator *(DReel)
//
// Crée le 9 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Multiplication des éléments d'un vecteur par un réel.
//
// Visibilité:     public
//
// Entrée:
//    DReel nombre : Multiplicateur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator *(const DReel nombre)
{
   // --- Variables temporaires.
   DReel reponse;
   CLVecteur retour(REEL);

   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] * nombre;
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator *(DReel)


//**************************************************************
// METHODE: CLVecteur::operator *(CLVecteur&)
//
// Crée le 9 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Multiplication de deux vecteurs.
//
// Visibilité:     public
//
// Entrée:
//    CLVecteur vect : Deuxieme vecteur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator *(const CLVecteur& vect)
{
   // --- Variables temporaires.
   DReel reponse;
   CLVecteur retour(REEL);
   VoidP tempP = NUL;   

   vect.tete(tempP);
   DReelP teteP = DReelP(tempP);
   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] * teteP[i];
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator *(CLVecteur&)


//**************************************************************
// METHODE: CLVecteur::operator /(DReel)
//
// Crée le 12 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Division des éléments d'un vecteur par un réel.
//
// Visibilité:     public
//
// Entrée:
//    Entier nombre : Diviseur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator /(const DReel nombre)
{
   // --- Variables temporaires.
   DReel reponse;
   CLVecteur retour(REEL);

   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] / nombre;
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator /(DReel)


//**************************************************************
// METHODE: CLVecteur::operator /(CLVecteur&)
//
// Crée le 12 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Division des éléments d'un vecteur.
//
// Visibilité:     public
//
// Entrée:
//    CLVecteur vect : Deuxieme vecteur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur CLVecteur::operator /(const CLVecteur& vect)
{
   // --- Variables temporaires.
   DReel reponse;
   CLVecteur retour(REEL);
   VoidP tempP = NUL;

   vect.tete(tempP);
   DReelP teteP = DReelP(tempP);
   for(EntierN i = 0;i < indice;i++)
   {
      reponse = val.teteReelP[i] / teteP[i];
      retour.ajoute(DReel(reponse));
   }
   return retour;
} // operator /(CLVecteur&)


//**************************************************************
// METHODE: CLVecteur::operator =(DReel)
//
// Crée le 20 décembre 1993   par André Houde
//**************************************************************
//
// Description:
//    Division des éléments d'un vecteur par un réel.
//
// Visibilité:     public
//
// Entrée:
//    Entier nombre : Diviseur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur& CLVecteur::operator =(const DReel nombre)
{

   for (EntierN i = 0;i < indice;i++)
      val.teteReelP[i] = nombre;

   return *this;
} // operator /(DReel)


//**************************************************************
// METHODE: CLVecteur::operator =(CLVecteur&)
//
// Crée le 20 décembre 1993   par André Houde
//**************************************************************
//
// Description:
//    Division des éléments d'un vecteur.
//
// Visibilité:     public
//
// Entrée:
//    CLVecteur vect : Deuxieme vecteur.
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLVecteur& CLVecteur::operator =(const CLVecteur& vect)
{
   // --- Variables temporaires.
   VoidP tempP = NUL;

   efface();
   if (vect.indice > 0)
      alloue(vect.indice);

   vect.tete(tempP);
   DReelP teteP = DReelP(tempP);
   for (EntierN i = 0;i < vect.indice;i++)
      ajoute(teteP[i]);

   return *this;
} // operator =(CLVecteur&)


//**************************************************************
// METHODE: CLVecteur::prodScal(CLVecteur&)
//
// Crée le 19 Aout 1993   par Christian Marcoux
//**************************************************************
//
// Description:
//    Produit scalaire de deux vecteurs.
//
// Visibilité:     public
//
// Entrée:
//    CLVecteur vect : Deuxieme vecteur.
//
// Sortie:
//    DReel reponse : produit scalaire.
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::prodScal(CLVecteur& vect, DReel& reponse)
{
   VoidP tempP = NUL;

   reponse = 0;
   vect.tete(tempP);
   DReelP teteP = DReelP(tempP);
   for(EntierN i = 0;i < indice; i++)
   {
      reponse = reponse + val.teteReelP[i] * teteP[i];
   }
   return OK;
} // prodScal(CLVecteur, DReel&)


//**************************************************************
// METHODE: CLVecteur::recherche
//
// Créé le 11 mars 1994                par André Houde
//**************************************************************
//
// Description:
//    Tente de trouver l'élément dans le vecteur
//
// Visibilité:  public.
//
// Entrée:
//    const Entier element : Elément à trouver dans le vecteur.
//
// Sortie:
//    EntierN& position    : Position de l'élément.
//
// Codes d'erreur:
//    OK    : Aucune erreur.
//    !OK   : L'élément n'est pas dans le vecteur.
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::recherche(EntierN& position, const Entier element)
{

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Entier err = !OK;

   for (EntierN i=0; i < indice; i++)
      if (val.teteEntierP[i] == element)
      {
         position = i;
         err = OK;
         break;
      }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return err;
}  // CLVecteur::recherche


//**************************************************************
// METHODE: CLVecteur::remplace(Entier, EntierN)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet de remplacer un element du
//                 vecteur de type Entier.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position   Position dans le vecteur
//
// Sortie:
//                 Entier    &elem      Element à remplacer
//
// Codes d'erreur:
//
// Préconditions:
//                 position < dim
//
// Postconditions:
//                 elem == val.teteEntierP[position]
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::remplace(Entier elem, EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(position < dim);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   val.teteEntierP[position] = elem;
   if (position >= indice) indice = position + 1;

#ifdef MODE_DEBUG
   POSTCONDITION(val.teteEntierP[position] == elem);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode remplace(Entier, EntierN)

//**************************************************************
// METHODE: CLVecteur::remplace(DReel, EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet de remplacer un element
//                 du vecteur de type DReel.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position   Position dans le vecteur
//
// Sortie:
//                 Entier    elem      Element à remplacer
//
// Codes d'erreur:
//
// Préconditions:
//                 position < dim
//
// Postconditions:
//                 elem == val.teteReelP[position]
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::remplace(DReel elem, EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(position < dim);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   val.teteReelP[position] = elem;
   if (position >= indice) indice = position + 1;

#ifdef MODE_DEBUG
   POSTCONDITION(val.teteReelP[position] == elem);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode remplace(DReel, EntierN)

//**************************************************************
// METHODE: CLVecteur::remplace(RGOrdreGraphique&, EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet de remplacer un element
//                 du vecteur de type Ordre Graphique.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position   Position dans le vecteur
//
// Sortie:
//                 RGOrdreGraphique    &elem      Element à remplacer
//
// Codes d'erreur:
//
// Préconditions:
//                 position < dim
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
/*
Entier CLVecteur::remplace(RGOrdreGraphique &elem, EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(position < dim);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   val.teteOrdreGraphiqueP[position] = elem;
   if (position >= indice) indice = position + 1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode remplace(RGOrdreGraphique&, EntierN)
*/

//**************************************************************
// METHODE: CLVecteur::remplace(CLChaine&, EntierN)
//
// Crée le 29 juillet 1993            par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode permet de remplacer un element
//                 du vecteur de type Chaîne.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position   Position dans le vecteur
//
// Sortie:
//                 CLChaine    &elem      Element à remplacer
//
// Codes d'erreur:
//
// Préconditions:
//                 position < dim
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::remplace(CLChaine &elem, EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(position < dim);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   val.teteChaineP[position] = elem;
   if (position >= indice) indice = position + 1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode remplace(CLChaine&, EntierN)

//**************************************************************
// METHODE: CLVecteur::remplace(SYCouleurRGB&, EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet de remplacer un element
//                 du vecteur de type Couleur.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position   Position dans le vecteur
//
// Sortie:
//                 SYCouleurRGB    &elem      Element à remplacer
//
// Codes d'erreur:
//
// Préconditions:
//                 position < dim
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::remplace(SYCouleurRGB &elem, EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(position < dim);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   val.teteCouleurP[position] = elem;
   if (position >= indice) indice = position + 1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode remplace(SYCouleurRGB&, EntierN)

//**************************************************************
// METHODE: CLVecteur::reqElement(Entier&, EntierN)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'avoir un element du vecteur
//                 de type Entier.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position   Position dans le vecteur
//
// Sortie:
//                 Entier    &elem      Valeur de retour
//
// Codes d'erreur:
//
// Préconditions:
//                 position < indice;
//
// Postconditions:
//                 elem == val.teteEntierP[position]
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::reqElement(Entier &elem, EntierN position) const
{
#ifdef MODE_DEBUG
   PRECONDITION(position < indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   elem = val.teteEntierP[position];

#ifdef MODE_DEBUG
   POSTCONDITION(elem == val.teteEntierP[position]);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode element(Entier&, EntierN)

//**************************************************************
// METHODE: CLVecteur::reqElement(DReel&, EntierN)
//
// Crée le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'avoir un element
//                 du vecteur de type DReel.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position   Position de l'élément dans le vecteur
//
// Sortie:
//                 DReel      &elem      Elément de retour
//
// Codes d'erreur:
//
// Préconditions:
//                 position < indice
//
// Postconditions:
//                 elem == val.teteReelP[position]
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::reqElement(DReel &elem, EntierN position) const
{
#ifdef MODE_DEBUG
   PRECONDITION(position < indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   elem = val.teteReelP[position];

#ifdef MODE_DEBUG
   POSTCONDITION(elem == val.teteReelP[position]);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode element(DReel&, EntierN)

//**************************************************************
// METHODE: CLVecteur::reqElement(RGOrdreGraphique&, EntierN)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'avoir un element du vecteur
//                 de type RGOrdreGraphique.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position      Position dans le vecteur de l'élément
//
// Sortie:
//                 RGOrdreGraphique  &elem  Valeur de retour de l'élément
//
// Codes d'erreur:
//
// Préconditions:
//                 position < indice
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
/*
Entier CLVecteur::reqElement(RGOrdreGraphique &elem, EntierN position) const
{
#ifdef MODE_DEBUG
   PRECONDITION(position < indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   elem = val.teteOrdreGraphiqueP[position];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode element(RGOrdreGraphique&, EntierN)
*/

//**************************************************************
// METHODE: CLVecteur::reqElement(CLChaine&, EntierN)
//
// Créé le 29 juillet 1993            par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode permet d'avoir un element du vecteur
//                 de type CLChaine.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position      Position dans le vecteur de l'élément
//
// Sortie:
//                 CLChaine  &elem  Valeur de retour de l'élément
//
// Codes d'erreur:
//
// Préconditions:
//                 position < indice
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::reqElement(CLChaine &elem, EntierN position) const
{
#ifdef MODE_DEBUG
   PRECONDITION(position < indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   elem = val.teteChaineP[position];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode reqElement(CLChaine&, EntierN)


//**************************************************************
// METHODE: CLVecteur::reqElement(SYCouleurRGB&, EntierN)
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet d'avoir un element du vecteur
//                 de type SYCouleurRGB.
//
// Visibilité:     Publique
//
// Entrée:
//                 EntierN   position      Position dans le vecteur de l'élément
//
// Sortie:
//                 SYCouleurRGB  &elem  Valeur de retour de l'élément
//
// Codes d'erreur:
//
// Préconditions:
//                 position < indice
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::reqElement(SYCouleurRGB &elem, EntierN position) const
{
#ifdef MODE_DEBUG
   PRECONDITION(position < indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   elem = val.teteCouleurP[position];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // Méthode reqElement(SYCouleurRGB&, EntierN)


//**************************************************************
// METHODE: CLVecteur::reqType(Type&) const
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode retourne le type du vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//
// Sortie:         Type &vecType
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::reqType(Type &vecType) const
{
   vecType = type;
   return OK;

} // Méthode reqType(Type&)


//**************************************************************
// METHODE: CLVecteur::tete(VoidP&)
//
// Créé le 26 janvier 1993            par Marc Hughes
// Modifié le 29 juillet 1993 par Guy Tremblay
//**************************************************************
//
// Description:
//                 Cette méthode permet d'optenir la tête du vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//
// Sortie:
//                 VoidP &tete   Valeur de la tête de la liste
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//    Il ne faut pas ajouter d'éléments dans le vecteur
//    sans passer par la méthode ajouter().
//
//**************************************************************
Entier CLVecteur::tete(VoidP &copieP) const
{
   switch (type)
   {
      //case CLVecteur::RGORDREGRAPHIQUE:
      //   copieP = VoidP (val.teteOrdreGraphiqueP);
      //   break;
      case CLVecteur::SYCHAINE:
         copieP = VoidP (val.teteChaineP);
         break;
      case CLVecteur::SYCOULEURRGB:
         copieP = VoidP (val.teteCouleurP);
         break;
      case CLVecteur::ENTIER:
         copieP = VoidP (val.teteEntierP);
         break;
      case CLVecteur::REEL:
         copieP = VoidP (val.teteReelP);
         break;
      default:
         copieP = 0;
         break;
   }
   return OK;

} // Méthode tete(VoidP&)

//**************************************************************
// METHODE: CLVecteur::tri()
//
// Créé le 26 janvier 1993            par Marc Hughes
//**************************************************************
//
// Description:
//                 Cette méthode permet de faire le tri du vecteur
//                 selon une méthode.
//
// Visibilité:     Publique
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::tri()
{
   switch (type)
   {
      case RGORDREGRAPHIQUE:
      break;
      case SYCOULEURRGB:
      break;
      case SYCHAINE:
      break;
      case ENTIER:
      break;
      case REEL:
      break;
      default:
      break;
   }
   return OK;

} // Méthode tri()


//**************************************************************
// METHODE: CLVecteur::variance(DReel&)
//
// Créé le 17 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Calcul de la variance d'un vecteur.
//
// Visibilité:     Publique
//
// Entrée:
//
// Sortie:
//    DReel var : Variance du vecteur.
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::variance(DReel& var)
{
   DReel moy;
   var = 0;
   moyenne(moy);
   for(EntierN i = 0;i < indice;i++)
   {
      var += pow(val.teteReelP[i] - moy, 2);
   }
   var /= indice;
   return OK;
} // variance(Booleen&)


//**************************************************************
// METHODE: CLVecteur::zeroDedans(Booleen&)
//
// Créé le 12 Aout 1993            par Christian Marcoux
//**************************************************************
//
// Description:
//    Vérification de la présence de zéro dans le vecteur.
//
// Visibilité:     Public
//
// Entrée:
//
// Sortie:
//    Booleen bZero : Vrai indique la présence de zéro.
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLVecteur::zeroDedans(Booleen& bZero) const
{
   EntierN i = 0;
   bZero = FAUX;
   while((i < indice) && (!bZero))
   {
      if(val.teteReelP[i] == 0)
      {
         bZero = VRAI;
      }
      i++;
   }
   return OK;
} // zeroDedans(Booleen&)

