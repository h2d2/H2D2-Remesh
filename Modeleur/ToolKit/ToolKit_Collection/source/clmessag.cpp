//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//******************************************************************************
// Fichier: clmessag.cpp
// Classe:  CLMessage
//******************************************************************************
// 05-03-1997  Yves Roy           Version initiale
// 27-05-2003  Dominique Richard  Port multi-compilateur
//******************************************************************************

#include "clmessag.h"

//******************************************************************************
// Sommaire:    Constructeur par défaut.
//
// Description: Cette méthode initialise les attributs de la classe
//              avec des valeurs par défaut (la liste de messages est
//              laissée vide et le type (optionnel) est initialisé à prmType).
//              Le type est initialisé par défaut à OK.
//
// Entrée:      Type prmType (optionnel)    Type du message
//
// Sortie:
//
// Notes:
//******************************************************************************
CLMessage::CLMessage(CLMessage::Type prmType)
: type(prmType), compteReference(1)
{
#ifdef MODE_DEBUG
   POSTCONDITION(lstMessages.dimension() == 0);
   POSTCONDITION(compteReference == 1);
   INVARIANTS ("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:     Construit un objet CLMessage et assigne aux
//               attributs les valeurs passées en paramètres.
//
// Description:  Les paramètres passés sont le message et le type du message.
//               La longueur de la liste de messages égalera donc 1.
//
// Entrée:       typeMessage: le type de message (OK,AVERTISSEMENT,ERREUR,FATAL)
//               message:     une chaine
//
// Sortie:
//
// Notes:
//******************************************************************************
CLMessage::CLMessage(CLMessage::Type typeMessage, const CLChaine& message)
: type(typeMessage), compteReference(1)
{
   lstMessages.ajoute(new CLChaine(message));

#ifdef MODE_DEBUG
   POSTCONDITION(typeMessage == type);
   POSTCONDITION(lstMessages.dimension() == 1);
   INVARIANTS ("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:  Constructeur copie de la classe.
//
// Description: Construit un objet CLMessage et initialise le nouvel objet
//              avec les valeurs emmagasinées dans l'objet passé en
//              paramètre.
//
// Entrée:  obj: l'objet du mème type
//
// Sortie:
//
// Notes:
//******************************************************************************
CLMessage::CLMessage(const CLMessage& obj)
: type(obj.type), compteReference(1)
{
   int nbr = obj.lstMessages.dimension();
   for (int i=0; i<nbr; i++)
   {
      lstMessages.ajoute(new CLChaine(*CLChaineP(lstMessages[i])));
   }

#ifdef MODE_DEBUG
   POSTCONDITION(lstMessages.dimension() == obj.lstMessages.dimension());
   INVARIANTS("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:  Destructeur.
//
// Description:  Aucune ressource à libérer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//******************************************************************************
CLMessage::~CLMessage()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   videCLListe(lstMessages, CLChaine());
}

//******************************************************************************
// Sommaire:  Opérateur d'assignation
//
// Description:  Cette méthode implante la surcharge de l'opérateur d'assigna-
//               tion.  On assume qu'un objet de type CLChaine a défini l'opérateur =.
//
// Entrée:       obj: un objet du mème type.
//
// Sortie:       On retourne l'objet par référence pour permettre les appels
//               en cascade.
//
// Notes:
//******************************************************************************
const CLMessage& CLMessage::operator=(const CLMessage &obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (this != &obj)
   {
      type = obj.type;
      compteReference = obj.compteReference;
      int nbr = obj.lstMessages.dimension();
      for (int i=0; i<nbr; i++)
      {
         lstMessages.ajoute(new CLChaine(*CLChaineP(lstMessages[i])));
      }
   }

#ifdef MODE_DEBUG
   POSTCONDITION(type == obj.type);
   POSTCONDITION(lstMessages.dimension() == obj.lstMessages.dimension());
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//******************************************************************************
// Sommaire:    Assigne le type de message
//
// Description: Cette méthode permet d'assigner le type du message.
//
// Entrée:      prmType: le type de message
//
// Sortie:
//
// Notes:
//******************************************************************************
void CLMessage::asgType(Type prmType)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   type = prmType;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:     Cette méthode ajoute un message à la fin de la liste.
//
// Description:  Par exemple, le premier message de la liste peut ètre
//               "Fichier introuvable" et le second message peut contenir
//               le nom du fichier.
//
// Entrée:       const CLChaine& message    le message à ajouter
//
// Sortie:
//
// Notes:
//******************************************************************************
void CLMessage::ajoute(const CLChaine& message)
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif

   // ---  Ajoute à la liste
   lstMessages.ajoute(new CLChaine(message));

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:     Retourne le type de message.
//
// Description:  Cette méthode retourne le type du message couramment stocké.
//
// Entrée:
//
// Sortie:       CLMessage::Type : le type de message
//
// Notes:
//******************************************************************************
CLMessage::Type CLMessage::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return type;
}

//******************************************************************************
// Sommaire:    Retourne le nombre de messages dans la liste.
//
// Description: Cette méthode retourne le nombre de messages dans la liste.
//
// Entrée:
//
// Sortie:
//
// Notes:
//******************************************************************************
EntierN CLMessage::reqNbrMessages() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return lstMessages.dimension();
}

//******************************************************************************
// Sommaire:    Retourne le ieme message
//
// Description: Cette méthode retourne le ieme message dans la liste.
//              ind doit ètre compris entre 0 et la longueur de la liste - 1.
//
// Entrée:      EntierN ind    : Indice du message à retourner
//
// Sortie:
//
// Notes:
//******************************************************************************
CLChaine CLMessage::operator [] (EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < lstMessages.dimension());
   INVARIANTS("PRECONDITION");
#endif
   return *CLChaineP(lstMessages[ind]);
}

//******************************************************************************
// Sommaire:    Permet de faire un test logique sur l'état du message
//
// Description: Cette méthode permet de faire un test logique sur l'état du
//              message.  Ainsi, si le message est du type OK, le test sera
//              VRAI et si le message est d'un autre type, il sera FAUX.
//              <p>
//              Il ne s'agit pas d'un cast en void* traditionnel, i.e. qu'on
//              obtient pas l'adresse de la structure comme résultat.
//              <p>
//              Exemple:
//              <p>
//              <pre>
//              CLMessage une_methode();
//              CLMessage ret;
//              ...
//              ret = une_methode();   // utilisation de l'opérateur =
//              if (ret) {             // utilisation de l'opérateur void*
//                 ...
//                 cout << "Il n'y a pas d'erreur" << endl;
//              }
//              else
//              {
//                 cout << "Il y a une erreur" << endl;
//              }
//              </pre>
//
// Entrée:
//
// Sortie:
//
// Notes:       Nous avons préféré l'utilisation du cast en void* à celui en
//              int pour éviter l'utilisation du résultat dans des opérations
//              arithmétiques.
//******************************************************************************
CLMessage::operator void*() const
{
   if (type == CLMessage::OK)
   {
      return (void*)VRAI;
   }
   else
   {
      return (void*)FAUX;
   }
}




