//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: clliste.cpp
// Classe:  CLListe
//************************************************************************
// 26-01-1993  Marc Hughes        Version initiale
// 03-12-1995  Yves Roy           Implantation de la méthode effaceLstChaines() sans
//                                appel de la fonction template videCLListe() pour
//                                éviter l'instanciation de la template.
// 07-02-1996  Yves Roy           Revision de code
// 27-09-1996  Yves Secretan      memove dans detruis et insere
// 28-01-1997  Yves Secretan      memcpy conditionnel dans realloue
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 23-01-1999  Yves Secretan      realloue() est rendu "amortized constant"
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#include "clliste.h"
#include "clchaine.h"
#include <string.h>

//**************************************************************
// Description:    Le constructeur fait l'initialisation du pointeur tête,
//                 de l'indice, du bloc allocation mémoire et du
//                 nombre de blocs de la liste.
//
// Entrée:
//                 dimVec     grandeur du bloc de mémoire alloué
//
// Sortie:
//
// Notes:
//
//**************************************************************
CLListe::CLListe (EntierN dimBloc)
{
#ifdef MODE_DEBUG
   PRECONDITION(dimBloc > 0);
#endif  // ifdef MODE_DEBUG

   tetePV = NUL;
   indice = 0;
   nbrBloc = 0;
   blocAllocation = STATIC_CAST(Entier)(dimBloc);

#ifdef MODE_DEBUG
   POSTCONDITION(tetePV == NUL);
   POSTCONDITION(indice == 0);
   POSTCONDITION(nbrBloc == 0);
   POSTCONDITION(blocAllocation == STATIC_CAST(Entier)(dimBloc));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Constructeur CLListe::CLListe(EntierN)

//**************************************************************
// Description:    Le constructeur fait l'initialisation du pointeur tête,
//                 de l'indice, du bloc allocation mémoire et du
//                 nombre de blocs de la liste.
//
// Entrée:         dimVec     grandeur du bloc de mémoire alloué
//
// Sortie:
//
// Notes:
//
//**************************************************************
CLListe::CLListe (ConstVoidP pP)
{
#ifdef MODE_DEBUG
   PRECONDITION(pP != NUL);
#endif  // ifdef MODE_DEBUG

   tetePV = NUL;
   indice = 0;
   nbrBloc = 0;
   blocAllocation = DIMVEC;

   if (indice >= (nbrBloc * blocAllocation)) realloue();

   tetePV[indice] = VoidP(pP);
   indice++;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Constructeur CLListe::CLListe(VoidP)

//**************************************************************
// Description: Copie constructeur.
//
// Entrée:
//    const CLListe& lst   : Liste à copier.
//
// Sortie:
//
// Notes:
//
//**************************************************************
CLListe::CLListe(const CLListe& lst)
{

   tetePV = NUL;
   indice = 0;
   nbrBloc = 0;
   blocAllocation = 1;

   (*this) = lst;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // CLListe::CLListe(const CLListe&)

//**************************************************************
// Description:    Le destructeur libère la mémoire occupée par la liste.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
CLListe::~CLListe()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   effacerListe();
   blocAllocation = 0;
} // Destructeur CLListe::~CLListe()

//**************************************************************
// Description:    Cette méthode ajoute un pointeur de type void
//                 à la fin de la liste.
//
// Entrée:
//
// Sortie:         VoidP     tempP     Pointeur à l'élément
//
// Notes:
//
//**************************************************************
void CLListe::ajoute(VoidP tempP)
{
#ifdef MODE_DEBUG
   PRECONDITION(tempP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (indice >= (nbrBloc * blocAllocation)) realloue();

   tetePV[indice] = tempP;
   indice++;

#ifdef MODE_DEBUG
   POSTCONDITION(tetePV[indice - 1] == tempP);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::ajoute()

//**************************************************************
// Description:    Cette méthode alloue la mémoire du vecteur de pointeur
//                 de grandeur dimVec.
//
// Entrée:         const EntierN&   dimVec     Grandeur du pointeur
//
// Sortie:
//
// Notes:
//
//**************************************************************
void CLListe::alloue(const EntierN& dimBloc)
{
#ifdef MODE_DEBUG
   PRECONDITION(dimBloc > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (nbrBloc > 0)
      nbrBloc = (nbrBloc*blocAllocation -1) / dimBloc;

   blocAllocation = STATIC_CAST(Entier)(dimBloc);
   realloue();

#ifdef MODE_DEBUG
   POSTCONDITION(blocAllocation == STATIC_CAST(Entier)(dimBloc));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::alloue(EntierN)

//**************************************************************
// Description: Cette méthode permet de détruire un élément de la liste.
//
// Entrée:
//    const Entier& position   Position de l'élément dans la liste
//
// Sortie:
//
// Notes:
//
//**************************************************************
void CLListe::detruis(const EntierN& position)
{
#ifdef MODE_DEBUG
   Entier indiceAvant = indice;
   PRECONDITION(STATIC_CAST(Entier)(position) < indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (indice <= 1)
   {
      effacerListe();
   }
   else
   {
      if (STATIC_CAST(Entier)(position) != indice-1)
      {
         size_t nbytes = (indice-position-1)*sizeof(VoidP);
         memmove(&tetePV[position], &tetePV[position+1], nbytes);
      }
      indice--;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(indice == (indiceAvant - 1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::detruis(EntierN)

//**************************************************************
// Description:
//    Efface le contenu de la liste en prenant pour acquis qu'elle
//    est composée de CLChaine. La liste elle même est aussi vidée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void CLListe::effaceLstChaines()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const EntierN nbr = dimension();
   for (EntierN i = 0; i < nbr; i++)
      delete CLChaineP((*this)[i]);
   effacerListe();

#ifdef MODE_DEBUG
   POSTCONDITION(dimension() == 0);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}  // CLListe::effaceLstChaines

//**************************************************************
// Description:  Cette méthode permet d'éffacer et de réinitialiser la
//               liste.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
void CLListe::effacerListe()
{
   delete [] tetePV;
   tetePV = NUL;
   indice = 0;
   nbrBloc  = 0;

#ifdef MODE_DEBUG
   POSTCONDITION(tetePV == NUL);
   POSTCONDITION(indice == 0);
   POSTCONDITION(nbrBloc == 0);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::effacerListe()

//**************************************************************
// Description:    Cette méthode ajoute un pointeur de type void
//                 à la position spécifiée par position.
//
// Entrée:
//   const EntierN&  position   Position du pointeur à insérer.
//
// Sortie:
//   VoidP     tempP     Pointeur à l'élément
//
// Notes:
//
//**************************************************************
void CLListe::insere(VoidP tempP, const EntierN& position)
{
#ifdef MODE_DEBUG
   Entier indiceAvant = indice;
   PRECONDITION(STATIC_CAST(Entier)(position) <= indice);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   // ---  Alloue la mémoire
   if (indice >= (nbrBloc * blocAllocation))
      realloue();

   // ---  Déplace les valeurs
   if (STATIC_CAST(Entier)(position) < indice)
   {
      size_t nbytes = (indice-position)*sizeof(VoidP);
      memmove(&tetePV[position+1], &tetePV[position], nbytes);
   }

   // ---  Insère la valeur
   tetePV[position] = tempP;
   indice++;

#ifdef MODE_DEBUG
   POSTCONDITION(indice == (indiceAvant + 1));
   POSTCONDITION(tetePV[position] == tempP);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::insere(VoidP, EntierN)

//**************************************************************
// Description:
//    Cette méthode retourne le pointeur de l'élément
//    désiré dans la liste.
//
// Entrée:
//    const EntierN& position    position de l'élément dans la liste
//
// Sortie:
//    Retourne le pointeur à l'élément.
//
// Notes:
//
//**************************************************************
VoidP& CLListe::operator[](const EntierN& position) const
{
#ifdef MODE_DEBUG
   PRECONDITION(STATIC_CAST(Entier)(position) < (nbrBloc * blocAllocation));
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG
   return tetePV[position];
} // operator[]

//**************************************************************
// Description:    Cette méthode permet de réallouer de la mémoire à la liste.
//
// Entrée:
//
// Sortie:
//
// Notes:          Cette méthode est local à la classe.
//
//**************************************************************
void CLListe::realloue()
{
#ifdef MODE_DEBUG
   Entier nbrBlocAvant = nbrBloc;
#endif  // ifdef MODE_DEBUG

   nbrBloc = (nbrBloc == 0) ? 1 : (nbrBloc+nbrBloc);
   VoidP* copiePV = new VoidP[nbrBloc * blocAllocation];
   memset(copiePV, 0, (nbrBloc * blocAllocation)*sizeof(VoidP));
   if (tetePV != NUL)
   {
      memcpy(copiePV, tetePV, indice*sizeof(VoidP));
      delete [] tetePV;
   }
   tetePV = copiePV;

#ifdef MODE_DEBUG
   POSTCONDITION(nbrBloc >= (nbrBlocAvant + 1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::realloue()

//**************************************************************
// Description:    Cette méthode recherche un pointeur de type void
//                 dans la liste et retourne sa position dans la liste.
//
// Entrée:
//    VoidP    tempP        Pointeur à l'élément
//
// Sortie:
//    EntierN  &position     Position de l'élément
//
// Notes:
//
//**************************************************************
Entier CLListe::recherche(EntierN &position, VoidP tempP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(tempP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Entier i = 0;

   while ((i < indice) && (tetePV[i] != tempP))
   {
      i++;
   }
   if (i >= indice)
   {
      position = 0;
      return !OK;
   }
   else
   {
      position = i;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(STATIC_CAST(Entier)(position) < indice);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return OK;
} // Méthode CLListe::recherche(EntierN&, VoidP)

//**************************************************************
// Description:    Cette méthode remplace un pointeur de type void
//                 à la position spécifiée par position.
//
// Entrée:
//    const EntierN&  position   Position du pointeur à insérer.
//
// Sortie:
//    VoidP &tempP    Pointeur à l'élément
//
// Notes:
//
//**************************************************************
void CLListe::remplace(VoidP tempP, const EntierN& position)
{
#ifdef MODE_DEBUG
   PRECONDITION(tempP != NUL);
   PRECONDITION(STATIC_CAST(Entier)(position) < (nbrBloc *blocAllocation));
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   tetePV[position] = tempP;

   if (STATIC_CAST(Entier)(position) >= indice) indice = position + 1;

#ifdef MODE_DEBUG
   POSTCONDITION(tetePV[position] == tempP);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::remplace(VoidP, EntierN)

//**************************************************************
// Description:    Cette méthode retourne le pointeur de l'élément
//                 désiré dans la liste.
//
// Entrée:
//    const EntierN&   position    position de l'élément dans la liste
//
// Sortie:
//    VoidP &tempP      pointeur à l'élément
//
// Notes:
//
//**************************************************************
void CLListe::reqElement(VoidP &tempP, const EntierN& position) const
{
#ifdef MODE_DEBUG
   PRECONDITION(STATIC_CAST(Entier)(position) < (nbrBloc * blocAllocation));
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   tempP = tetePV[position];

#ifdef MODE_DEBUG
   POSTCONDITION(tempP == tetePV[position]);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::element()

//**************************************************************
// Description:    Cette méthode retourne le pointeur de tête de la liste.
//
// Entrée:
//
// Sortie:
//     VoidP*    &tempPV   pointeur de retour de la tête du vecteur
//
// Notes:   Le pointeur de tête doit seulement être utilisé pour
//          parcourir la liste.
//
//**************************************************************
void CLListe::tete(VoidP* &tempPV) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

    tempPV = tetePV;

#ifdef MODE_DEBUG
   POSTCONDITION(tempPV == tetePV);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
} // Méthode CLListe::tete(VoidP&);

//**************************************************************
// Description:
//    Affecte le contenu d'une liste à celle-ci.
//
// Entrée:
//    const CLListe& lst   : Liste à copier.
//
// Sortie:
//
// Notes:
//
//**************************************************************
CLListe& CLListe::operator=(const CLListe& lst)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (this != &lst)
   {
      effacerListe();

      indice = lst.indice;
      nbrBloc = lst.nbrBloc;
      blocAllocation = lst.blocAllocation;

      if (lst.tetePV != NUL && lst.indice > 0)
      {
         tetePV = new VoidP[nbrBloc * blocAllocation];
         memset(tetePV,0,(nbrBloc * blocAllocation)*sizeof(VoidP));
         memcpy(tetePV,lst.tetePV,indice*sizeof(VoidP));
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return (*this);
}  // CLListe::operator=









