//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: clenvmsg.cpp
// Classe:  CLEnveloppeMessage
//************************************************************************
// 05-03-1997  Yves Roy           Version initiale
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************

#include "clenvmsg.h"

//************************************************************************
// Sommaire:    Constructeur par défaut de la classe.
//
// Description: Ce constructeur initialise les attributs à des valeurs
//              par défaut. Si le type du message est OK ou omis, messageP
//              est initialisé à NUL.
//              Le type du message est un paramètre optionnel initialisé
//              par défaut à OK.
//
// Entrée:      Type& unType: le type du message.
//
// Sortie:
//
// Notes:
//************************************************************************
CLEnveloppeMessage::CLEnveloppeMessage(const Type& unType)
: messageP(NUL)
{
   // --- Si le type du message est autre que OK, un objet
   // --- SYMessage est physiquement créé.
   if (unType != OK)
   {
      messageP = new CLMessage(CLMessage::Type(unType));
   }

#ifdef MODE_DEBUG
   POSTCONDITION(unType != OK || messageP == NUL);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Constructeur de la classe
//
// Description: Ce constructeur crée un CLCompteReference en
//              lui assignant un messsge contruit à partir des arguments.
//
// Entrée:      Type&    type         :  le type du message.
//              const CLChaine& chaine:  la chaîne contenant le message.
//
// Sortie:
//
// Notes:
//************************************************************************
CLEnveloppeMessage::CLEnveloppeMessage(const Type& type,
                                       const CLChaine& chaine)
: messageP(new CLMessage((CLMessage::Type)type, chaine))
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Constructeur copie de la classe
//
// Description: Ce constructeur construit l'objet à partir d'un objet du
//              mème type passé en paramètre.
//
// Entrée:      obj: L'objet sur lequel se construire.
//
// Sortie:
//
// Notes:
//************************************************************************
CLEnveloppeMessage::CLEnveloppeMessage(const CLEnveloppeMessage &obj)
: messageP(0)
{
   // --- Appel de la méthode privée qui s'occupe des copies et du compte
   // --- de références.
   copie(obj);

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == obj.messageP);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Destructeur de la classe
//
// Description: Ce destructeur gère le compte de références avant de
//              laisser détruire l'objet
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
CLEnveloppeMessage::~CLEnveloppeMessage()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   // --- Appel à la méthode privée qui s'occupe de libérer les ressources
   // --- et qui gère le compte de références.
   if (messageP != NUL)
   {
      libere();
   }
}

//************************************************************************
// Sommaire:     Cette méthode ajoute un message à la fin de la liste.
//
// Description:  Cette méthode réimplante la méthode de l'objet auquel
//               on réfère.  On passe par le CLCompteReference
//               pour accéder à l'objet.
//               <p>
//               Notons que dans CLCompteReference on a défini deux
//               méthodes reqObjet, une const et l'autre non const.
//               La méthode non const est utilisée ici.
//               <p>
//               On doit implanter dans cette méthode la notion du copy on
//               write puisque que cette méthode modifie l'objet.  Ainsi,
//               lorsque cette méthode est appelée on doit cloner l'objet
//               référencé pour avoir un comportement prévisible.
//               <p>
//               Si le pointeur à l'objet CLCompteReference (messageP)
//               est nul, un tel objet doit évidemment ètre créé.
//
// Entrée:       chaine: le message à ajouter.
//
// Sortie:
//
// Notes:
//************************************************************************
void CLEnveloppeMessage::ajoute(const CLChaine& chaine)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (messageP != NUL)
   {
      // --- On applique la théorie du copie on write
      // --- On appelle clone() avant de modifier.
      clone();
      messageP->ajoute(chaine);
   }
   else
   {
      messageP = new CLMessage(CLMessage::OK, chaine);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:     Cette méthode assigne un type au message.
//
// Description:  Cette méthode réimplante la méthode de l'objet auquel
//               on réfère.  On passe par le CLCompteReference
//               pour accéder à l'objet.
//               <p>
//               Notons que dans CLCompteReference on a défini deux
//               méthodes reqObjet, une const et l'autre non const.
//               La méthode non const est utilisée ici.
//               <p>
//               On doit implanter dans cette méthode la notion du copy on
//               write puisque que cette méthode modifie l'objet.  Ainsi,
//               lorsque cette méthode est appelée on doit cloner l'objet
//               référencé pour avoir un comportement prévisible.
//               <p>
//               Si le pointeur à l'objet CLCompteReference (messageP)
//               est nul, un tel objet doit évidemment ètre créé.
//
// Entrée:       const Type& unType: le type du message.
//
// Sortie:
//
// Notes:
//************************************************************************
void CLEnveloppeMessage::asgType (const Type& unType)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (messageP != NUL)
   {
      // --- On applique la théorie du copie on write
      // --- On appelle clone() avant de modifier.
      clone();
      messageP->asgType((CLMessage::Type)unType);
   }
   else
   {
      // --- Un nouveau message de type 'unType' est créé.
      messageP = new CLMessage((CLMessage::Type)unType);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Créer une copie de l'objet courant.
//
// Description: Cette méthode privée crée un clone de l'objet couramment
//              référencé.  On doit donc décrémenter notre compte de
//              références sur l'objet courant et ensuite se cloner.  Par
//              contre, si le compte de références est égal à 1, on n'a
//              pas besoin de cloner puisqu'on est seul à référer l'objet.
//              <p>
//              Donc pour implanter le copie on write, on n'a qu'à appeler
//              cette méthode à l'intérieur de chacune des méthodes modifiant
//              l'objet, i.e. les méthodes non const.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void CLEnveloppeMessage::clone()
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP != NUL);
   INVARIANTS("PRECONDITION");
#endif
   // --- S'il ne reste que nous-mème à référer à l'erreur
   // --- on n'a pas besoin de cloner.
   if (messageP->compteReference != 1) {

      // --- Cloner signifie ne plus référer à l'ancien message
      // --- mais plutôt prendre une copie de celui-ci et
      // --- partir une nouvelle branche.
      messageP->compteReference--;
      messageP = new CLMessage(*messageP);
   }

#ifdef MODE_DEBUG
   POSTCONDITION(messageP->compteReference == 1);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Gère l'allocation des ressources et le compte de
//              références
//
// Description: Cette méthode privée gère l'allocation de ressources et le
//              compte de références sur l'objet. Toutes les copies
//              doivent passer par ici pour ne pas créer de trous dans
//              la gestion du compte de références.
//
// Entrée:      L'objet du mème type à assigner.
//
// Sortie:
//
// Notes:
//************************************************************************
void CLEnveloppeMessage::copie(const CLEnveloppeMessage &obj)
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP == NUL);
   INVARIANTS("PRECONDITION");
#endif

   messageP = obj.messageP;
   if (messageP != NUL)
   {
      messageP->compteReference++;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == obj.messageP);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Gère la désallocation des ressources et le compte de
//              références
//
// Description: Cette méthode privée gère la désallocation de ressources
//              et le compte de références sur l'objet.  Toutes les
//              libérations doivent passer par ici pour ne pas créer de
//              trous dans la gestion du compte de références.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void CLEnveloppeMessage::libere()
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP != NUL);
   INVARIANTS("PRECONDITION");
#endif

   if (messageP->compteReference == 1)
   {
      // --- Il ne reste qu'une seule référence à l'objet,
      // --- on doit alors détruire l'objet.
      delete messageP;
   }
   else
   {
      messageP->compteReference--;
   }
   messageP = NUL;

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == NUL);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:     Cette méthode retourne le nombre de messages dans la liste.
//
// Description:  reqNbrMessages retourne le nombre de messages dans la liste.
//               <p>
//               Cette méthode réimplante la méthode de l'objet auquel
//               on référence.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
EntierN CLEnveloppeMessage::reqNbrMessages() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   EntierN  n = 0;
   if (messageP != NUL)
   {
      n = messageP->reqNbrMessages();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return n;
}

//************************************************************************
// Sommaire:     Méthode d'accès au type du message
//
// Description:  Cette méthode réimplante la méthode de l'objet auquel
//               on référence.
//               <p>
//               Si le pointeur à l'objet messageP
//               est nul, on assume que le type du message est 'OK'.
//
// Entrée:
//
// Sortie:       Une référence au type de l'erreur.
//
// Notes:
//************************************************************************
CLEnveloppeMessage::Type CLEnveloppeMessage::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLEnveloppeMessage::Type typeRet = CLEnveloppeMessage::OK;
   if (messageP != NUL)
   {
      typeRet = (CLEnveloppeMessage::Type)messageP->reqType();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return typeRet;
}

//************************************************************************
// Sommaire:    Opérateur d'assignation de la classe
//
// Description: Cet opérateur d'assignation permet d'utiliser
//              l'assignation de façon cohérente et sans danger pour
//              l'intégrité du système de la lettre et de l'enveloppe.
//              <p>
//              Il faut d'abord libérer les ressources qu'on possède et gérer
//              le compte de références sur l'objet.  Ensuite on doit assigner
//              la nouvelle référence et gérer le compte de références.
//
// Entrée:      L'objet du mème type à assigner.
//
// Sortie:      Un référence constante à l'objet lui-mème.
//
// Notes:
//************************************************************************
const CLEnveloppeMessage&
CLEnveloppeMessage::operator=(const CLEnveloppeMessage &obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   // --- Vérification de l'auto-assignation.
   if (this != &obj)
   {
      if (messageP != NUL)
      {
         libere();
      }
      copie(obj);
   }

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == obj.messageP);
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void CLEnveloppeMessage::ajoute(const CLEnveloppeMessage &obj)
{
#ifdef MODE_DEBUG
   PRECONDITION(!obj);
   INVARIANTS("PRECONDITION");
#endif

   if (messageP == NUL || (void*)messageP)
   {
      *this = obj;
   }
   else
   {
      EntierN nbrMsg = obj.reqNbrMessages();
      for (EntierN i=0; i<nbrMsg; i++)
      {
         ajoute(obj[i]);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:     Retourne le message de la liste en position ind.
//
// Description:  Le message est composé d'une liste de sous-messages.
//               ex: cet énoncé retourne le sous-message de m en position i:
//               <p>
//               <code>
//               m [i];
//               </code>
//
// Entrée:
//    EntierN ind    : indice du sous-message à retourner
//
// Sortie:
//
// Notes:
//************************************************************************
CLChaine CLEnveloppeMessage::operator [] (const EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP != NUL);
   PRECONDITION(ind < this->reqNbrMessages());
   INVARIANTS("PRECONDITION");
#endif

   return messageP->operator[](ind);
}

//************************************************************************
// Sommaire:     Permet d'offrir un raccourci pour savoir s'il y a une
//               erreur.
//
// Description:  La surcharge de l'opérateur void* permet de retourner
//               implicitement VRAI ou FAUX lorsqu'on utilise l'objet
//               dans une expression logique.
//               Ainsi, si le message est du type OK, le test sera
//               VRAI et si le message est d'un autre type, il sera FAUX.
//               Il ne s'agit pas du vrai opérateur void*, i.e. que ce
//               cast ne retourne pas l'adresse de la structure.
//               <p>
//               Exemple:
//               <p>
//               <code>
//               ERMsg msg = ERMsg::OK;
//               msg = obj.uneMethode();
//               ...
//               if (msg) {
//                  ...
//                  cout << "Il n'y a pas d'erreur" << endl;
//               }
//               else
//               {
//                  cout << "Il y a une erreur" << endl;
//               }
//               </code>
//
// Entrée:
//
// Sortie:
//
// Notes:        On n'a pas utilisé le cast en int pour faire le test
//               pour éviter l'utilisation du résultat dans des calculs
//               arithmétique.  Le cast en void* nous assure que le
//               résultat ne peut ètre utilisé pour autre chose.
//************************************************************************
CLEnveloppeMessage::operator void*() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   void* voidP;

   // --- Si le pointeur au message n'est pas NUL, on retourne le statut
   // --- de l'objet (VRAI si le message n'est pas en erreur, FAUX sinon).
   // --- Si le pointeur au message est NUL, on assume que le type celui-ci
   // --- est OK, ainsi on retourne VRAI.
   if (messageP != NUL)
   {
      voidP = (void*)messageP;
   }
   else
   {
      voidP = (void*)VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return voidP;
}




