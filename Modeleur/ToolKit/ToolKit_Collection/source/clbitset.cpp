//*****************************************************************************
// $Id$
// $Date$
//*****************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  clbitset.cpp
// Classe:   CLBitSet.
//*****************************************************************************
// 24-07-1996  Yves Roy           Version initiale
// 28-07-1996  Yves Roy           Ajout d'un entier dans le vecteur (maintenant 4)
// 25-01-1999  Yves Secretan      Complète les PRE-POST
// 27-05-2003  Dominique Richard  Port multi-compilateur
//*****************************************************************************
#include "clbitset.h"
#include "fifichie.h"

//*****************************************************************************
// Sommaire:     Constructeur par défaut.
//
// Description:  Le constructeur par défaut du CLBitSet initialise ses structures
//               de données à 0.
//
// Entrée:
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLBitSet::CLBitSet()
{
#ifdef MODE_DEBUG
   PRECONDITION(sizeof(data[0]) == 4);
#endif

   vide();

#ifdef MODE_DEBUG
   POSTCONDITION(estVide());
   INVARIANTS("POSTCONDITION");
#endif
}

//*****************************************************************************
// Sommaire:     Constructeur copie
//
// Description:  Le constructeur copie de CLBitSet initialise ses structures
//               de données en prenant copie du bitset de l'objet passé en
//               paramètre.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLBitSet::CLBitSet(const CLBitSet& obj)
{
#ifdef MODE_DEBUG
   PRECONDITION(sizeof(data[0]) == 4);
#endif

   vide();
   assigne(obj);

#ifdef MODE_DEBUG
   POSTCONDITION(*this == obj);
   INVARIANTS("POSTCONDITION");
#endif
}

//*****************************************************************************
// Sommaire:     Constructeur acceptant un numéro de bit à allumer.
//
// Description:  Le constructeur du CLBitSet acceptant un numéro de bit permet
//               d'initialiser l'objet en allumant le bit concerné.  Ce numéro
//               doit toutefois être dans les limites de la capacité de
//               stockage de l'objet, soit pour l'instant 0-128.
//               0 indique un objet vide, la numérotation des bits allant de
//               1 à 128.
//
// Entrée:       EntierN n: le numéro du bit.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLBitSet::CLBitSet(EntierN n)
{
#ifdef MODE_DEBUG
   PRECONDITION(sizeof(data[0]) == 4);
   PRECONDITION(n <= 128);
#endif

   vide();
   EntierN ind = reqIndice(n);
   EntierN bit = reqBit(n);

   data[ind] |= bit;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//*****************************************************************************
// Sommaire:     Destructeur de CLBitSet.
//
// Description:  Ne fait rien de spécifique.
//
// Entrée:
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLBitSet::~CLBitSet()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

}

//*****************************************************************************
// Sommaire:     Ajoute les bits contenu dans l'objet passé en paramètre.
//
// Description:  Le méthode publique ajoute(const CLBitSet&) permet d'ajouter
//               les bits contenus dans l'objet passé en paramète en effectuant
//               un OU logique entre les deux chaînes de bits.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLBitSet::ajoute(const CLBitSet& obj)
{
   data[0] |= obj.data[0];
   data[1] |= obj.data[1];
   data[2] |= obj.data[2];
   data[3] |= obj.data[3];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//*****************************************************************************
// Sommaire:     Remplace le bitset par celui contenu dans l'objet passé en paramètre.
//
// Description:  Le méthode publique assigne(const CLBitSet&) permet de remplacer
//               les bits contenus dans l'objet passé en paramète en effectuant
//               une copie complète des bits sur les bits de l'objet courant.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLBitSet::assigne(const CLBitSet& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   data[0] = obj.data[0];
   data[1] = obj.data[1];
   data[2] = obj.data[2];
   data[3] = obj.data[3];

#ifdef MODE_DEBUG
   POSTCONDITION(*this == obj);
   INVARIANTS("POSTCONDITION");
#endif
}

//*****************************************************************************
// Sommaire:     Vérifie la complète inclusion des bits de l'objet passé dans l'objet courant.
//
// Description:  Le méthode publique contient(const CLBitSet&) permet de vérifier
//               la complète inclusion des bits de l'objet passé dans l'objet
//               courant.  On vérifie que ObjetCourant & ObjetPasse == ObjetPasse.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retourne VRAI si inclusion complète et FAUX autrement.
//
// Notes:
//*****************************************************************************
Booleen CLBitSet::contient(const CLBitSet& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (data[0] & obj.data[0]) == obj.data[0] &&
          (data[1] & obj.data[1]) == obj.data[1] &&
          (data[2] & obj.data[2]) == obj.data[2] &&
          (data[3] & obj.data[3]) == obj.data[3];
}

//*****************************************************************************
// Sommaire:     Enlève les bits contenu dans l'objet passé en paramètre.
//
// Description:  Le méthode publique enleve(const CLBitSet&) permet d'enlever
//               les bits contenus dans l'objet passé en paramète en effectuant
//               un ET logique entre l'objet courant et la négation de l'objet
//               passé en paramètre.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLBitSet::enleve(const CLBitSet& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   data[0] &= ~obj.data[0];
   data[1] &= ~obj.data[1];
   data[2] &= ~obj.data[2];
   data[3] &= ~obj.data[3];

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//*****************************************************************************
// Sommaire:     Vérifie que tous les bits sont à zéro.
//
// Description:  Le méthode publique estVide() permet de vérifier que tous les
//               bits de l'objet courant sont à zéro.
//
// Entrée:
//
// Sortie:       Retourne VRAI si tous les bits sont à 0 et FAUX autrement.
//
// Notes:
//*****************************************************************************
Booleen CLBitSet::estVide() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (data[0] == 0) &&
          (data[1] == 0) &&
          (data[2] == 0) &&
          (data[3] == 0);
}

//*****************************************************************************
// Sommaire:     Met à 0 tous les bits
//
// Description:  Le méthode publique vide() permet de mettre à zéro tous les bits
//               de l'objet courant.
//
// Entrée:
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLBitSet::vide()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   data[0] = 0;
   data[1] = 0;
   data[2] = 0;
   data[3] = 0;

#ifdef MODE_DEBUG
   POSTCONDITION(estVide());
   INVARIANTS("POSTCONDITION");
#endif
}

//*****************************************************************************
// Sommaire:     Opérateur d'assignation.
//
// Description:  L'opérateur d'assignation permet de remplacer tous les bits
//               de l'objet courant par ceux de l'objet passé en paramètre.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retourne par référence l'objet courant pour permettre les appels
//               en cascade.
//
// Notes:
//*****************************************************************************
CLBitSet& CLBitSet::operator=(const CLBitSet& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (this != &obj)
   {
      assigne(obj);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//*****************************************************************************
// Sommaire:     Opérateur d'addition.
//
// Description:  L'opérateur d'addition permet de faire l'addition entre l'objet
//               courant et l'objet passé en paramètre et de retourner le
//               résultat par valeur.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retour par valeur du résultat.
//
// Notes:
//*****************************************************************************
CLBitSet CLBitSet::operator+(const CLBitSet& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLBitSet b(*this);
   b += obj;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return b;
}

//*****************************************************************************
// Sommaire:     Opérateur +=
//
// Description:  L'opérateur += permet d'ajouter à l'objet courant les bits
//               contenus dans l'objet passé en paramètre.  Si un des bits s'y
//               retrouver déjà on n'en tiend pas compte.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retourne par référence l'objet courant pour permettre les appels
//               en cascade.
//
// Notes:
//*****************************************************************************
CLBitSet& CLBitSet::operator+=(const CLBitSet& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   ajoute(obj);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//*****************************************************************************
// Sommaire:     Opérateur de soustraction
//
// Description:  L'opérateur de soustraction permet de faire la soustraction
//               entre l'objet courant et l'objet passé en paramètre et de
//               retourner par valeur le résultat.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retour par valeur du résultat.
//
// Notes:
//*****************************************************************************
CLBitSet CLBitSet::operator-(const CLBitSet& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLBitSet b(*this);
   b -= obj;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return b;
}

//*****************************************************************************
// Sommaire:     Opérateur -=
//
// Description:  L'opérateur -= permet d'enlever de l'objet courant les bits
//               spécifiés dans l'objet passé en paramètre.  Si un des bits ne
//               s'y trouvait pas, on n'en tiend pas compte.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retourne par référence l'objet courant pour permettre les appels
//               en cascade.
//
// Notes:
//*****************************************************************************
CLBitSet& CLBitSet::operator-=(const CLBitSet& obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   enleve(obj);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//*****************************************************************************
// Sommaire:     Opérateur de comparaison d'égalité
//
// Description:  L'opérateur de comparaison d'égalité permet de comparer le
//               contenu de la chaine de bits.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retourne VRAI si tous les bits correspondent et FAUX autrement.
//
// Notes:
//*****************************************************************************
Booleen CLBitSet::operator==(const CLBitSet& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return data[0] == obj.data[0] &&
          data[1] == obj.data[1] &&
          data[2] == obj.data[2] &&
          data[3] == obj.data[3];
}

//*****************************************************************************
// Sommaire:     Opérateur de comparaison d'inégalité
//
// Description:  L'opérateur de comparaison d'inégalité permet de comparer le
//               contenu de la chaîne de bits.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retourne VRAI si un des bits ne correspond pas et FAUX autrement.
//
// Notes:        L'opérateur est implanté en utilisant l'opérateur ==
//*****************************************************************************
Booleen CLBitSet::operator!=(const CLBitSet& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return !(*this == obj);
}

//*****************************************************************************
// Sommaire:     Opérateur de comparaison d'infériorité
//
// Description:  L'opérateur de comparaison d'infériorité permet de comparer
//               si l'objet courant est plus petit que l'objet passé en
//               paramètre.
//
// Entrée:       const CLBitSet& obj: un objet du mème type.
//
// Sortie:       Retourne VRAI si plus petit et FAUX autrement.
//
// Notes:
//*****************************************************************************
Booleen CLBitSet::operator<(const CLBitSet& obj) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return ((data[0] < obj.data[0]) ||
           (!(data[0] < obj.data[0]) && data[1] < obj.data[1]) ||
           (!(data[0] < obj.data[0] && data[1] < obj.data[1]) && data[2] < obj.data[2]) ||
           (!(data[0] < obj.data[0] && data[1] < obj.data[1] && data[2] < obj.data[2]) && data[3] << obj.data[3]));
}

FIFichier& operator<<(FIFichier& os, const CLBitSet& b)
{
   if (os)
   {
      os << b.data[0] << ESPACE << b.data[1] << ESPACE << b.data[2] << ESPACE << b.data[3];
   }
   return os;
}

FIFichier& operator>>(FIFichier& is, CLBitSet& b)
{
   if (is)
   {
      is >> b.data[0] >> b.data[1] >> b.data[2] >> b.data[3];
   }
   return is;
}


