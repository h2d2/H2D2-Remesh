//*****************************************************************************
// $Id$
// $Date$
//*****************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  clchform.cpp
// Classe :  CLChaineFormattee
//*****************************************************************************
// 16-02-1994  André  Houde       Version initiale
// 11-10-1995  Yves   Roy         Revision majeure du code, pré et post conditions
// 07-08-1996  Martin Langlois    Utilisation d'une variable temporaire dans
//                                ajoute(const Reel&)
// 25-09-1998  Yves Secretan      Change le scope des for et while
// 27-05-2003  Dominique Richard  Port multi-compilateur
//*****************************************************************************
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "clchform.h"

// --- Définition des valeurs par défaut
const EntierN CLChaineFormattee::CL_OPTIONS_DEF = 0;
const EcourtN CLChaineFormattee::CL_LARGEUR_DEF = 0;
const CarN    CLChaineFormattee::CL_PRECISION_DEF = 4;
const Car     CLChaineFormattee::CL_CARTAMPON_DEF = ' ';

//*****************************************************************************
// Sommaire:    Constructeur par défaut.
//
// Description: On appelle d'abord le constructeur de CLChaine
//
// Entrée:
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaineFormattee::CLChaineFormattee()
: CLChaine()
{
   asgDefaut();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef POSTCONDITION
}  // CLChaineFormattee::CLChaineFormattee

//*****************************************************************************
// Sommaire: Constructeur avec un ConstCarP
//
// Description: On appelle d'abord le constructeur de CLChaine
//
// Entrée:
//    ConstCarP chP    : Chaine d'initialisation.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaineFormattee::CLChaineFormattee(ConstCarP chP)
: CLChaine(chP)
{
   asgDefaut();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef POSTCONDITION
}  // CLChaineFormattee::CLChaineFormattee(const CarP)

//*****************************************************************************
// Sommaire: Constructeur avec un objet CLChaine
//
// Description: On appelle d'abord le constructeur de CLChaine
//
// Entrée:
//    const CLChaine& ch   : Chaine d'initialisation.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaineFormattee::CLChaineFormattee(const CLChaine& ch)
: CLChaine(ch)
{
   asgDefaut();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef POSTCONDITION
}  // CLChaineFormattee::CLChaineFormattee(const CLChaine&)

//*****************************************************************************
// Sommaire: Constructeur copie
//
// Description: On appelle d'abord le constructeur de CLChaine
//
// Entrée:
//    const CLChaineFormattee& ch   : Chaine d'initialisation.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaineFormattee::CLChaineFormattee(const CLChaineFormattee& ch)
: CLChaine(ch)
{
   asgOptions(ch.options, ch.precision, ch.largeur, ch.carTampon);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef POSTCONDITION
}  // CLChaineFormattee::CLChaineFormattee(const CLChaineFormattee&)

//*****************************************************************************
// Sommaire: Destructeur
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
CLChaineFormattee::~CLChaineFormattee()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}  // CLChaineFormattee::~CLChaineFormattee

//*****************************************************************************
// Sommaire: ajoute un entier formate
//
// Description:
//    Ajoute l'entier à la chaine selon le format spécifié
//    dans cette chaine (attribut options). On
//    formatte le nombre selon la largeur spécifiée, le caractère
//    tampon et l'endroit de la chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const Entier nbr  : Nombre à écrire.
//
// Sortie:
//
// Notes:
//    En utilisant le format de sprintf, on se forme une chaine
//    de format (variable format) selon les options spécifiés.
//*****************************************************************************
void CLChaineFormattee::ajoute(const Entier& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine format;
   format << '%';

   if (options & POSITIF)
      format << '+';
   if (options & BASE)
      format << '#';

   if (options & OCT)
      format << "lo";
   else if (options & HEX)
   {
      if (options & MAJUSCULE)
         format << "lX";
      else
         format << "lx";
   }
   else
      format << "ld";

   Car c[35];
   sprintf(c, format.reqConstCarP(), nbr);

   CLChaine ch(c);
   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajoute(const Entier&)

//*****************************************************************************
// Sommaire: Ajoute un entier non signé et formaté
//
// Description:
//    Ajoute l'entier à la chaine selon le format spécifié
//    dans cette chaine (attribut options). On
//    formatte le nombre selon la largeur spécifiée, le caractère
//    tampon et l'endroit de la chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const EntierN nbr : Nombre à écrire.
//
// Sortie:
//
// Notes:
//    En utilisant le format de sprintf, on se forme une chaine
//    de format (variable format) selon les options spécifiés.
//*****************************************************************************
void CLChaineFormattee::ajoute(const EntierN& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine format;
   format << '%';

   if (options & POSITIF)
      format << '+';
   if (options & BASE)
      format << '#';

   if (options & OCT)
      format << "lo";
   else if (options & HEX)
   {
      if (options & MAJUSCULE)
         format << "lX";
      else
         format << "lx";
   }
   else
      format << "lu";

   Car c[35];
   sprintf(c, format.reqConstCarP(), nbr);

   CLChaine ch(c);
   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajoute(const EntierN)

//*****************************************************************************
// Sommaire: Ajoute un réel formaté.
//
// Description:
//    Ajoute le nombre à la chaine selon le format spécifié
//    dans cette chaine (attribut options). On
//    formatte le nombre selon la largeur spécifiée, le caractère
//    tampon et l'endroit de la chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const Reel& nbr   : Nombre à écrire.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::ajoute(const Reel& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   DReel val = nbr;
   ajoute(val);
//   ajoute((const DReel&)nbr);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajoute(const DReel...)

//*****************************************************************************
// Sommaire: Ajoute un double réel formaté.
//
// Description:
//    Ajoute le nombre à la chaine selon le format spécifié
//    dans cette chaine (attribut options). On
//    formatte le nombre selon la largeur spécifiée, le caractère
//    tampon et l'endroit de la chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const DReel& nbr   : Nombre à écrire.
//
// Sortie:
//
// Notes:
//    En utilisant le format de sprintf, on se forme une chaine
//    de format (variable format) selon les options spécifiés.
//*****************************************************************************
void CLChaineFormattee::ajoute(const DReel& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (options & INGENIEUR)
   {
      formateIngenieur(nbr);
   }
   else
   {
      CLChaine format;
      format << '%';

      if (options & POSITIF)
         format << '+';

      if (options & POINT)
      {
         format << '.';
         format << EntierN(precision);
      }

      if (options & MAJUSCULE)
      {
         if (options & SCIENTIFIC)
            format << 'E';
         else if (options & FIXE)
            format << 'f';
         else
            format << 'G';
      }
      else
      {
         if (options & SCIENTIFIC)
            format << 'e';
         else if (options & FIXE)
            format << 'f';
         else
            format << 'g';
      }

      Car c[35];
      sprintf(c, format.reqConstCarP(), nbr);

      CLChaine ch(c);
      ajusteChaine(ch);
      CLChaine::ajoute(ch);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajoute(const DReel...)

//*****************************************************************************
// Sommaire: Ajoute une heure.
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM:SS ou HH:MM
//    (si les secondes sont à zéro) à la fin de la chaîne. On
//    formatte le nombre selon la largeur spécifiée, le caractère
//    tampon et l'endroit de la chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const EntierN& nbr : Heure à écrire (en secondes).
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::ajouteHeure(const EntierN& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine ch;
   ch.ajouteHeure(nbr);
   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajouteHeure

//*****************************************************************************
// Sommaire: Ajoute un nombre en secondes sous le format HH;MM:SS.DD.
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM:SS.DD
//    à la fin de la chaîne. On formatte le nombre selon la largeur spécifiée,
//    le caractère tampon et l'endroit de la chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const Reel& nbr : Heure à écrire (en secondes).
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::ajouteHeure(const Reel& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine ch;
   ch.ajouteHeure(nbr);
   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajouteHeure(const Reel&)

//*****************************************************************************
// Sommaire: Ajoute un nombre en secondes sous le format HH;MM:SS.DD.
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM:SS.DD
//    à la fin de la chaîne. On formatte le nombre selon la largeur spécifiée,
//    le caractère tampon et l'endroit de la chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const DReel& nbr : Heure à écrire (en secondes).
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::ajouteHeure(const DReel& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine ch;
   ch.ajouteHeure(nbr);
   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajouteHeure(const Reel&)

//*****************************************************************************
// Sommaire: Ajoute un nombre sous le format HH:MM
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM
//    à la fin de la chaîne. On formatte cette chaine selon la largeur
//    spécifiée, le caractère tampon et l'endroit de la
//    chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const EntierN& nbr : Heure à écrire (en secondes).
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::ajouteHeureMinutes(const EntierN& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine ch;
   ch.ajouteHeureMinutes(nbr);
   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajouteHeureMinutes

//*****************************************************************************
// Sommaire: Ajoute un nombre sous le format HH:MM:SS
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM:SS
//    à la fin de la chaîne. On formatte cette chaine selon la largeur
//    spécifiée, le caractère tampon et l'endroit de la
//    chaine (GAUCHE, DROITE, ...).
//
// Entrée:
//    const EntierN& nbr : Heure à écrire (en secondes).
//
// Sortie:
//
// Notes:
//**************************************************************
void CLChaineFormattee::ajouteHeureMinutesSecondes(const EntierN& nbr)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine ch;
   ch.ajouteHeureMinutesSecondes(nbr);
   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajouteHeureMinutesSecondes

//*****************************************************************************
// Sommaire: Formatte la chaine avec les valeurs par défaut.
//
// Description:
//    Cette méthode reçoit une chaine. On formatte cette chaine selon la
//    largeur spécifiée, le caractère tampon et l'endroit de la chaine
//    (GAUCHE, DROITE, ...).
//
// Entrée:
//    CLChaine &ch : Chaine contenant le texte à justifier.
//
// Sortie:
//    CLChaine &ch : Chaine contenant le texte justifié.
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::ajusteChaine(CLChaine &ch)
{
#ifdef MODE_DEBUG
   PRECONDITION(!ch.estVide());
   INVARIANTS("PRECONDITION");
#endif

   // --- On vérifie si on doit justifier la chaine.
   EntierN taille = ch.reqTaille();
   if (taille < largeur)
   {

      CLChaine format;
      if (options & CENTRE)
      {
         // --- On doit centrer le texte, on place des carTampon à gauche d'abord.
         EntierN nbr = (largeur-taille) / 2;
         for (Entier i=nbr; i > 0; --i)
            format << carTampon;

         // --- On place le nombre et des carTampon à droite.
         format << ch;
         for (Entier i=largeur-taille-nbr; i > 0; --i)
            format << carTampon;

         ch = format;
      }
      else
      {
         // --- On place d'abord le nombre de car. manquants.
         for (Entier i = largeur-taille; i > 0; --i)
            format << carTampon;

         if (options & DROITE)
            // --- On justifie à droite, on ajoute le nombre.
            format << ch;
         else
            // --- On justifie à gauche, on insère au début.
            format.insere(0, ch);

         ch = format;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::ajusteChaine

//*****************************************************************************
// Sommaire: Remet les attributs à leurs valeurs par défaut.
//
// Description:
//    Remet les attributs (options, largeur, etc...) à leurs
//    valeurs par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//**************************************************************
void CLChaineFormattee::asgDefaut()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   options     = CL_OPTIONS_DEF;
   largeur     = CL_LARGEUR_DEF;
   precision   = CL_PRECISION_DEF;
   carTampon   = CL_CARTAMPON_DEF;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::asgDefaut

//*****************************************************************************
// Sommaire: Affecte tous les paramètres de formattage.
//
// Description:
//    Affecte tous les paramètres de formattage. ATTENTION, le paramètre
//    indiquant les options n'est pas affecté directement. Les options
//    passées en paramètres sont ajoutées à celles présentes.
//
// Entrée:
//    EntierN par    : Options s'ajoutant aux options présentes. Ces options
//                     peuvent être ajoutées avec l'aide de l'opérateur ou (|).
//    Car prec       : Précision pour les réels (4 par défaut).
//    ECourtN larg   : Largeur minimum pour écrire un nombre (0 par défaut).
//    Car fill       : Caractère de remplissage (espace par défaut).
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::asgOptions(const EntierN par, const CarN prec,
                                   const EcourtN larg, const Car fill)
{
#ifdef MODE_DEBUG
   PRECONDITION(fill != '\0');
   INVARIANTS("PRECONDITION");
#endif

   precision = prec;
   largeur = larg;
   carTampon = fill;

   if (par & CENTRE || par & GAUCHE || par & DROITE)
   {
      // --- On garde une exclusivité entre les options GAUCHE, DROITE et CENTRE
      options &= ~CENTRE & ~GAUCHE & ~DROITE;
      if (par & GAUCHE)
         options |= GAUCHE;
      else if (par & DROITE)
         options |= DROITE;
      else
         options |= CENTRE;
   }

   if (par & DEC || par & OCT || par & HEX)
   {
      // --- On garde une exclusivité entre les options DEC, OCT et HEX
      options &= ~DEC & ~OCT & ~HEX;
      if (par & DEC)
         options |= DEC;
      else if (par & OCT)
         options |= OCT;
      else
         options |= HEX;
   }

   if (par & FIXE || par & SCIENTIFIC || par & INGENIEUR)
   {
      // --- On garde une exclusivité entre les options FIXE, SCIENTIFIC et INGENIEUR
      options &= ~FIXE & ~SCIENTIFIC &~INGENIEUR;
      if (par & FIXE)
         options |= FIXE;
      else if (par & SCIENTIFIC)
         options |= SCIENTIFIC;
      else
         options |= INGENIEUR;
   }

   // --- On traite les autres options
   options |= (par&MAJUSCULE) | (par&POINT) | (par&POSITIF) | (par&BASE);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::asgOptions

//*****************************************************************************
// Sommaire:
//   Ajoute un double réel dont le format est du type ingénieur.
//
// Description:
//   On trouve la valeur de l'exposant et de la mantisse du nombre à formater
//   et on crée la chaine de caractères.
//
// Entrée:
//   const DReel& nbr   : Nombre à écrire.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::formateIngenieur(const DReel& a)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   CLChaine ch;

   if (options & POSITIF)
   {
      if (a >= 0)
         ch.ajoute('+');
   }

   //--- On cherche l'exposant du nombre transformé
   DReel logA = log10(a);
   Entier intLogA = Entier(logA);
   Entier exposant = (intLogA/3) * 3;

   //--- On cherche la mantisse du nombre transformé
   DReel logMantisse = logA - exposant;
   DReel mantisse = pow(10, logMantisse);

   if (precision == 0)
   {
      ch.ajoute(Entier(mantisse));
   }
   else
   {
      ch.ajoute(mantisse);

      //--- On tronque ou on ajoute des zéros selon la précision
      EntierN taille = ch.reqTaille();
      EntierN point = -1;
      ch.recherche(point, '.');
      if (point == EntierN(-1))
      {
         ch.ajoute('.');
         for (EntierN i=0; i<EntierN(precision); i++)
         {
            ch.ajoute('0');
         }
      }
      else if (taille-point <= precision)
      {
         for (EntierN j=taille-point; j<=precision; j++)
         {
            ch.ajoute('0');
         }
      }
      else if (taille-point > precision)
      {
         ch.tronque(point+precision+1);
      }
    }

   //--- On insère le 'E' ou le 'e'
   if (options & MAJUSCULE)
   {
      ch.ajoute('E');
   }
   else
   {
      ch.ajoute('e');
   }

   //--- On ajoute l'exposant
   ch.ajoute(exposant);

   ajusteChaine(ch);
   CLChaine::ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::formateIngenieur(const DReel&)

//*****************************************************************************
// Sommaire: Remplace les chaines de recherche par la chaine de remplacement.
//
// Description:
//    Remplace toutes les chaines de recherche par la chaine de
//    remplacement à partir de la position courante. La position
//    courante est avancée après la dernière occurence
//    de la chaine de remplacement.
//
// Entrée:
//    ConstCarP rechercheP    : Chaine de recherche (à remplacer).
//    ConstCarP remplaceP     : Chaine de remplacement.
//    const EntierN position  : Position de départ de remplacement.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::remplace(ConstCarP rechercheP, ConstCarP remplaceP,
                                 const EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(rechercheP != NUL);
   PRECONDITION(remplaceP != NUL);
   INVARIANTS("PRECONDITION");
#endif

   // --- On vérifie si la position actuelle est dans la chaine.
   EntierN taille = reqTaille();
   if (position < taille)
   {
      EntierN tailleRech = static_cast<EntierN>(strlen(rechercheP));

      // --- On utilise la sous-chaine rech pour faire la recherche. La
      // --- variable finale contient le texte final remplacé.
      CLChaine finale, rech, tmp;

      // --- On prend le texte à partir de la position actuelle.
      EntierN positionFinale = position;
      reqSousChaineDroite(rech, positionFinale);

      // --- On tente de trouver la chaine de recherche.
      EntierN pos;
      while (positionFinale < taille && rech.recherche(pos, rechercheP))
      {
         // --- On a une chaine à remplacer.
         // --- On prend la partie avant la chaine pour l'ajouter au texte final.
         rech.reqSousChaineGauche(tmp, pos);
         finale << tmp;

         // --- On place le texte de remplacement.
         finale << remplaceP;

         // --- On reprend une nouvelle chaine de recherche et on
         // --- actualise la position.
         rech.reqSousChaineDroite(rech, pos+tailleRech);
         positionFinale += pos+tailleRech;
      }

      finale << rech;
      efface();
      CLChaine::ajoute(finale);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::remplace

//*****************************************************************************
// Sommaire: Remplace tous les caractères de recherche par le caractères de
//           remplacement.
//
// Description:
//    Remplace tous les caractères de recherche par le caractère de
//    remplacement à partir de la position courante. La position
//    courante est avancée après la dernière occurence
//    du caractère de remplacement.
//
// Entrée:
//    const Car carRech       : Caractère de recherche (à remplacer).
//    const Car carRemplace   : Caractère de remplacement.
//    const EntierN position  : Position de départ de remplacement.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaineFormattee::remplace(const Car carRech, const Car carRemplace,
                                 const EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(carRech != '\0');
   PRECONDITION(carRemplace != '\0');
   INVARIANTS("PRECONDITION");
#endif

   EntierN taille = reqTaille();
   for (EntierN i=position; i<taille; i++)
   {
      if (reqCaractere(i) == carRech)
         asgCaractere(carRemplace, i);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}  // CLChaineFormattee::remplace

//*****************************************************************************
// Sommaire: Opérateur d'assignation.
//
// Description:
//    Affecte cette chaine avec la chaine reçue (ainsi que le attributs).
//
// Entrée:
//    const CLChaineFormattee &ch : Chaine pour l'affectation.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaineFormattee& CLChaineFormattee::operator=(const CLChaineFormattee& ch)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (this != &ch) {
      CLChaine::operator=(ch);
      asgOptions(ch.options, ch.precision, ch.largeur, ch.carTampon);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif

   return (*this);
}  // CLChaineFormattee::operator=





