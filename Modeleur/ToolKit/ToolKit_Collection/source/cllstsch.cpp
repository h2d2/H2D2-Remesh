//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// NOM DU FICHIER: cllstsch.cpp
//************************************************************************
//
// Classe : CLListeSimplementChainee
//
// Attributs:
//   noeudP  teteLibreP             // La tête de la liste enlevé non détruit
//   noeudP  teteListeP             // La tête de la liste chaînée
//   noeudP  queueListeP            // La queue de la liste chaînée
//   noeudP  teteVectListeP         // Vecteur contenant toute la liste
//   noeudP  courantListeP          // Le courant de la liste chaînée
//   EntierN courantIndice          // L'indice correspondant à courantListeP
//   EntierN nbrItemsListe          // Le nombre d'items de la liste chaînée
//   EntierN indice                 // Prochain item à être alloué
//   EntierN dimListeChainee        // Dimension de la liste chaînée
//   EntierN dimAjoute              // Dimension de réallocation
//
// Méthodes:
//   CLListeSimplementChainee       // Dimension de réallocation = DIMVECT
//   CLListeSimplementChainee       // Dimension de réallocation = paramètre
//   ~CLListeSimplementChainee      //
//   invariant                      //
//   alloue                         // Allocation de mémoire
//   detruis                        // Detruis l'élément courant de la liste
//   detruisPremier                 // Detruis le premier élément de la liste
//   detruisDernier                 // Detruis le dernier élément de la liste
//   effaceListe                    // Efface la liste au complet
//   insere                         // Insère après l'élément courant
//   insereDebut                    // Insère un élément au début de la liste
//   insereFin                      // Insère un élément à la fin de la liste
//   listeVide                      // Retourne VRAI si la liste est vide.
//   nbrElem                        // Retourne le nombre d'items de la liste
//   prochainNoeudDisponible        // Retourne le prochain noeud disponible
//   remplace                       // Remplace une information par une autre
//   reqElement                     // Retourne l'information du noeud
//
// Fonctions:
//
//************************************************************************

#include "cllstsch.h"

const Entier DIMVECT = 10;

//**************************************************************
// METHODE: CLListeSimplementChainee::CLListeSimplementChainee()
//
// Créé le  28 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Constructeur par défaut, la dimension de réallocation est fixée à
//   DIMVECT.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//   On s'assure que tous les attributs sont initialisés aux bonnes valeurs.
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLListeSimplementChainee::CLListeSimplementChainee()
{
   teteVectListeP = teteLibreP = teteListeP = queueListeP =
   courantListeP = NUL;
   courantIndice = 1;
   nbrItemsListe = 0;
   dimListeChainee = 0;
   indice = 0;
   dimAjoute = DIMVECT;

#ifdef MODE_DEBUG
   POSTCONDITION(teteVectListeP == NUL);
   POSTCONDITION(teteLibreP == NUL);
   POSTCONDITION(teteListeP == NUL);
   POSTCONDITION(queueListeP == NUL);
   POSTCONDITION(courantListeP == NUL);
   POSTCONDITION(courantIndice == 1);
   POSTCONDITION(nbrItemsListe == 0);
   POSTCONDITION(dimListeChainee == 0);
   POSTCONDITION(indice == 0);
   POSTCONDITION(dimAjoute == DIMVECT);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

}  // CLListeSimplementChainee()


//**************************************************************
// METHODE: CLListeSimplementChainee::CLListeSimplementChainee (EntierN)
//
// Créé le  28 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Surcharge du constructeur, la dimension de réallocation est passée
//   en paramètre.
//
// Visibilité: public
//
// Entrée:
//    EntierN dimension: la dimension de réallocation
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//   On s'assure que tous les attributs sont initialisés aux bonnes valeurs.
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLListeSimplementChainee::CLListeSimplementChainee (EntierN dimension)
{
   teteVectListeP = teteLibreP = teteListeP = queueListeP =
   courantListeP = NUL;
   courantIndice = 1;
   nbrItemsListe = 0;
   dimListeChainee = 0;
   indice = 0;
   dimAjoute = dimension;

#ifdef MODE_DEBUG
   POSTCONDITION(teteVectListeP == NUL);
   POSTCONDITION(teteLibreP == NUL);
   POSTCONDITION(teteListeP == NUL);
   POSTCONDITION(queueListeP == NUL);
   POSTCONDITION(courantListeP == NUL);
   POSTCONDITION(courantIndice == 1);
   POSTCONDITION(nbrItemsListe == 0);
   POSTCONDITION(dimListeChainee == 0);
   POSTCONDITION(indice == 0);
   POSTCONDITION(dimAjoute == dimension);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

}  // CLListeSimplementChainee (EntierN)


//**************************************************************
// METHODE: CLListeSimplementChainee::~CLListeSimplementChainee()
//
// Créé le  28 avril 1993  par  François Gingras
//**************************************************************
//
// Description: On détruit la liste au complet et tout ce qui s'y rapporte.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
CLListeSimplementChainee::~CLListeSimplementChainee()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   delete []teteVectListeP;
   teteVectListeP = teteLibreP = teteListeP = queueListeP =
   courantListeP = NUL;
}  // ~CLListeSimplementChainee()


//**************************************************************
// METHODE: CLListeSimplementChainee::ajoute (VoidP)
//
// Créé le  14 mai 1993  par  François Gingras
//**************************************************************
//
// Description: Insère un élément au début de la liste.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//   VoidP infP: pointeur vers l'information à ajouter.
//
// Codes d'erreur:
//
// Préconditions:
//   pointeur vers l'information à ajouter doit être non nul
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé au noeud que l'on vient d'ajouter
//
//**************************************************************
Entier CLListeSimplementChainee::ajoute (VoidP infP)
{
#ifdef MODE_DEBUG
   PRECONDITION(infP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   insereFin (infP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // ajoute(VoidP&)


//**************************************************************
// METHODE: CLListeSimplementChainee::alloue()
//
// Créé le  30 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Effectue l'allocation ou la réallocation de mémoire pour la liste
//   chaînée.
//
// Visibilité: privé.
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que l'on peut réallouer de la mémoire
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLListeSimplementChainee::alloue()
{
#ifdef MODE_DEBUG
   PRECONDITION((dimListeChainee + dimAjoute) != 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //--- Si dimListeChainee == 0, alors on alloue pour la première fois
   if (dimListeChainee != 0)
   {
      noeudP copieLP = new noeud[dimListeChainee+dimAjoute], tempP = teteListeP;

      //--- On fait une copie élément par élément
      for (EntierN k=0; k<dimListeChainee; k++)
      {
         copieLP[k].informationP = tempP->informationP;
         copieLP[k].suivantP = copieLP+k+1;
         tempP = tempP->suivantP;
      }
      teteListeP = copieLP;
      queueListeP = copieLP+dimListeChainee-1;
      queueListeP->suivantP = NUL;
      delete []teteVectListeP;
      teteVectListeP = copieLP;
      dimListeChainee = dimListeChainee + dimAjoute;
   }
   else
   {
      dimListeChainee = dimAjoute;
      teteVectListeP = new noeud[dimListeChainee];
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // alloue()


//**************************************************************
// METHODE: CLListeSimplementChainee::detruis (EntierN)
//
// Créé le  29 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Détruis l'élément qui est situé en position "position" dans la liste.
//
// Visibilité: public
//
// Entrée:
//   EntierN position: la position de l'élément dans la liste
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que la liste n'est pas vide
//   Vérifie que la position n'est pas plus grande que la dimension de liste
//   Vérifie que la position n'est pas nulle (non valide)
//
// Postconditions:
//    Vérifie que le nombre d'items dans la liste est bel et bien décrémenté
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé au noeud suivant de position
//   la liste est numérotée de 1 à nbrItemsListe
//
//**************************************************************
Entier CLListeSimplementChainee::detruis (EntierN position)
{
#ifdef MODE_DEBUG
   EntierN nbrItemsAvant;
   nbrItemsAvant = nbrItemsListe;
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   PRECONDITION(nbrItemsListe != 0);
   PRECONDITION(position != 0);
   PRECONDITION(position <= nbrItemsListe);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (position == 1)
      detruisPremier();
   else if (position == nbrItemsListe)
      detruisDernier();
   else
   {
      noeudP precedentListeP;
      EntierN k;

      //--- si égal, alors on veut le précédent du courant
      if (courantIndice == position)
      {
         precedentListeP = teteListeP;
         //--- on part de 2 car on veut le précédent au courantListeP
         for (k=2; k<courantIndice; k++)
            precedentListeP = precedentListeP->suivantP;
      }

      //--- si non, on veut le précédent et le courant
      else
      {
         if (courantIndice > position)
         {
            courantListeP = teteListeP;
            courantIndice = 1;
         }
         for (k=courantIndice; k<position; k++)
         {
            precedentListeP = courantListeP;
            courantListeP = courantListeP->suivantP;
         }
      }
      nbrItemsListe--;
      courantIndice = position;
      precedentListeP->suivantP = courantListeP->suivantP;
      courantListeP->suivantP = teteLibreP;
      teteLibreP = courantListeP;
      courantListeP = precedentListeP->suivantP;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsAvant == (nbrItemsListe+1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // detruis(EntierN)


//**************************************************************
// METHODE: CLListeSimplementChainee::detruisPremier()
//
// Créé le  29 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Détruis le premier élément de la liste chaînée.
//
// Visibilité:
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que la liste n'est pas vide
//
// Postconditions:
//    Vérifie que le nombre d'items dans la liste est bel et bien décrémenté
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé à teteListeP
//
//**************************************************************
Entier CLListeSimplementChainee::detruisPremier()
{
#ifdef MODE_DEBUG
   EntierN nbrItemsAvant;
   nbrItemsAvant = nbrItemsListe;
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   PRECONDITION(nbrItemsListe != 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   noeudP tempP;

   nbrItemsListe--;
   courantIndice = 1;
   tempP = teteListeP->suivantP;
   teteListeP->suivantP = teteLibreP;
   teteLibreP = teteListeP;
   if (tempP == NUL)
      teteListeP = queueListeP = courantListeP = NUL;
   else
      teteListeP = courantListeP = tempP;

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsAvant == (nbrItemsListe+1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // detruisPremier()


//**************************************************************
// METHODE: CLListeSimplementChainee::detruisDernier()
//
// Créé le  29 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Détruis le dernier élément de la liste chaînée.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que la liste n'est pas vide.
//
// Postconditions:
//    Vérifie que le nombre d'items dans la liste est bel et bien décrémenté
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé à queueListeP
//
//**************************************************************
Entier CLListeSimplementChainee::detruisDernier()
{
#ifdef MODE_DEBUG
   EntierN nbrItemsAvant;
   nbrItemsAvant = nbrItemsListe;
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   PRECONDITION(nbrItemsListe != 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   //--- On va trouver le pointeur vers le noeud qui précède la queue
   if (nbrItemsListe == 1) // Si un seul élément dans la liste
   {
      nbrItemsListe = 0;
      queueListeP->suivantP = teteLibreP;
      teteLibreP = queueListeP;
      teteListeP = queueListeP = courantListeP = NUL;
   }
   else  // Si plus d'un élément dans la liste
   {
      noeudP precedentListeP;
      EntierN k, valeur;

      //--- Si courant == queue, alors on ne cherche que le précédent du
      //--- courant.
      if (nbrItemsListe == courantIndice)
      {
         precedentListeP = teteListeP;
         valeur = 2;
      }
      else
      {
         precedentListeP = courantListeP;
         valeur = courantIndice+1;
      }
      for (k=valeur; k<nbrItemsListe; k++)
         precedentListeP = precedentListeP->suivantP;
      nbrItemsListe--;
      courantIndice = nbrItemsListe;
      precedentListeP->suivantP = NUL;
      queueListeP->suivantP = teteLibreP;
      teteLibreP = queueListeP;
      queueListeP = courantListeP = precedentListeP;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsAvant == (nbrItemsListe+1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // detruisDernier()


//**************************************************************
// METHODE: CLListeSimplementChainee::effaceListe()
//
// Créé le  3 mai 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Efface la liste chaînée de la mémoire et réinitialise toutes les autres
//   valeurs.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//    Vérifie que la liste est effectivement vide.
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLListeSimplementChainee::effaceListe()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (dimListeChainee != 0)
   {
      delete []teteVectListeP;
      teteVectListeP = teteLibreP = teteListeP = queueListeP =
      courantListeP = NUL;
      courantIndice = 1;
      nbrItemsListe = 0;
      dimListeChainee = 0;
      indice = 0;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsListe == 0);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // effaceListe()


//**************************************************************
// METHODE: CLListeSimplementChainee::insere (VoidP, EntierN)
//
// Créé le  28 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Insère un élément de la liste après le noeud situé en position
//   "position".
//
// Visibilité: public
//
// Entrée:
//   EntierN position: le numéro du noeud après lequel on insère
//
// Sortie:
//   VoidP infP: pointeur vers l'information à ajouter.
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que la position n'est pas plus grande que la dimension de liste
//   Vérifie que la position n'est pas nulle (non valide)
//   Pointeur vers l'information à insérer doit être non nul
//
// Postconditions:
//    Vérifie que le nombre d'items dans la liste est bel et bien incrémenté
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé au noeud que l'on vient d'ajouter
//
//**************************************************************
Entier CLListeSimplementChainee::insere (VoidP infP, EntierN position)
{
#ifdef MODE_DEBUG
   EntierN nbrItemsAvant;
   nbrItemsAvant = nbrItemsListe;
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   PRECONDITION(position != 0);
   PRECONDITION(position <= nbrItemsListe);
   PRECONDITION(infP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (position == 1)
      insereDebut (infP);
   else if (position == nbrItemsListe)
      insereFin (infP);
   else
   {
      noeudP noeudCourantP;

      if (courantIndice == position)
         noeudCourantP = courantListeP;
      else
      {
         if (courantIndice < position)  // on part de la position courante
            noeudCourantP = courantListeP;
         else
         {                              // si non, on part du début
            noeudCourantP = teteListeP;
            courantIndice = 1;
         }
         for (EntierN k=courantIndice; k<position; k++)
            noeudCourantP = noeudCourantP->suivantP;
      }

      //--- Si noeudCourantP == NUL, alors l'élément courant n'existe pas
      prochainNoeudDisp (courantListeP);
      courantListeP->informationP = infP;
      nbrItemsListe++;
      courantIndice = position+1;
      courantListeP->suivantP = noeudCourantP->suivantP;
      noeudCourantP->suivantP = courantListeP;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsListe == (nbrItemsAvant+1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // insere (VoidP&, EntierN)


//**************************************************************
// METHODE: CLListeSimplementChainee::insereDebut (VoidP)
//
// Créé le  28 avril 1993  par  François Gingras
//**************************************************************
//
// Description: Insère un élément au début de la liste.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//   VoidP infP: pointeur vers l'information à ajouter.
//
// Codes d'erreur:
//
// Préconditions:
//   pointeur vers l'information à insérer doit être non nul
//
// Postconditions:
//    Vérifie que le nombre d'items dans la liste est bel et bien incrémenté
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé au noeud que l'on vient d'ajouter
//
//**************************************************************
Entier CLListeSimplementChainee::insereDebut (VoidP infP)
{
#ifdef MODE_DEBUG
   EntierN nbrItemsAvant;
   nbrItemsAvant = nbrItemsListe;
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   PRECONDITION(infP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   nbrItemsListe++;
   courantIndice = 1;
   prochainNoeudDisp (courantListeP);
   courantListeP->informationP = infP;
   if (teteListeP == NUL)
      queueListeP = courantListeP;
   courantListeP->suivantP = teteListeP;
   teteListeP = courantListeP;

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsListe == (nbrItemsAvant+1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // insereDebut(VoidP)


//**************************************************************
// METHODE: CLListeSimplementChainee::insereFin (VoidP)
//
// Créé le  29 avril 1993  par  François Gingras
//**************************************************************
//
// Description: Insère un élément à la fin de la liste.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//   VoidP infP: pointeur vers l'information à ajouter.
//
// Codes d'erreur:
//
// Préconditions:
//   pointeur vers l'information à insérer doit être non nul
//
// Postconditions:
//    Vérifie que le nombre d'items dans la liste est bel et bien incrémenté
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé au noeud que l'on vient d'ajouter
//
//**************************************************************
Entier CLListeSimplementChainee::insereFin (VoidP infP)
{
#ifdef MODE_DEBUG
   EntierN nbrItemsAvant;
   nbrItemsAvant = nbrItemsListe;
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   PRECONDITION(infP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   nbrItemsListe++;
   courantIndice = nbrItemsListe;
   prochainNoeudDisp (courantListeP);
   courantListeP->informationP = infP;
   courantListeP->suivantP = NUL;
   if (queueListeP != NUL)
      queueListeP->suivantP = courantListeP;
   else
      teteListeP = courantListeP;
   queueListeP = courantListeP;

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsListe == (nbrItemsAvant+1));
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // insereFin(VoidP)


//**************************************************************
// METHODE: CLListeSimplementChainee::listeVide (Booleen&)
//
// Créé le  3 mai 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Retourne VRAI si la liste est vide.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//   Boolean& vide: valeur à modifier
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLListeSimplementChainee::listeVide (Booleen& vide)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (nbrItemsListe == 0)
      vide = VRAI;
   else
      vide = FAUX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
} // listeVide (Booleen&)


//**************************************************************
// METHODE: CLListeSimplementChainee::nbrElem (EntierN&)
//
// Créé le  29 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Retourne le nombre d'items que la liste contient.
//
// Visibilité: public
//
// Entrée:
//
// Sortie:
//   EntierN& nombre: la valeur à modifier
//
// Codes d'erreur:
//
// Préconditions:
//
// Postconditions:
//    On vérifie que la bonne valeur a été assignée.
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLListeSimplementChainee::nbrElem (EntierN &nombre)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   nombre = nbrItemsListe;

#ifdef MODE_DEBUG
   POSTCONDITION(nombre == nbrItemsListe);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // nbrElem(EntierN&)


//**************************************************************
// METHODE: CLListeSimplementChainee::prochainNoeudDisp (noeudP&)
//
// Créé le  28 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Retourne le prochain noeud disponible pour utilisation.
//
// Visibilité: privé
//
// Entrée:
//
// Sortie:
//   noeudP& noeudDispP: le noeud à rendre disponible.
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que l'on peut réallouer de la mémoire
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//
//**************************************************************
Entier CLListeSimplementChainee::prochainNoeudDisp (noeudP &noeudDispP)
{
#ifdef MODE_DEBUG
   PRECONDITION((dimListeChainee + dimAjoute) != 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (teteLibreP == NUL)
   {
      //--- Si l'on est rendu au bout de la mémoire déjà allouée
      if (indice == dimListeChainee)
         alloue();
      noeudDispP = teteVectListeP+indice;
      indice++;
   }
   else
   {
      noeudDispP = teteLibreP;
      teteLibreP = teteLibreP->suivantP;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // prochainNoeudDisp(noeudP&)


//**************************************************************
// METHODE: CLListeSimplementChainee::remplace (VoidP, EntierN)
//
// Créé le  3 mai 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Remplace l'information concernant le (numero)ième noeud de la liste.
//
// Visibilité: public
//
// Entrée:
//   EntierN numero: le numéro du noeud à modifier
//
// Sortie:
//   VoidP infP: pointeur vers la nouvelle information
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que la liste n'est pas vide
//   Vérifie que le numéro ne dépasse le nombre d'items de la liste
//   Vérifie que le numéro n'est pas nul
//   Pointeur vers l'information avec laquelle on remplace doit être non nul
//
// Postconditions:
//    Vérifie qu'aucun item n'a été ajouté à la liste
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé au noeud que l'on remplace
//   les noeuds sont numérotés de 1 à nbrItemsListe
//
//**************************************************************
Entier CLListeSimplementChainee::remplace (VoidP infP, EntierN position)
{
#ifdef MODE_DEBUG
   EntierN nbrItemsAvant;
   nbrItemsAvant = nbrItemsListe;
#endif  // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   PRECONDITION(nbrItemsListe != 0);
   PRECONDITION(position != 0);
   PRECONDITION(position <= nbrItemsListe);
   PRECONDITION(infP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (courantIndice == position)
      courantListeP->informationP = infP;
   else
   {
      if (courantIndice > position)
      {
         courantListeP = teteListeP;
         courantIndice = 1;
      }
      for (EntierN k=courantIndice; k<position; k++)
         courantListeP = courantListeP->suivantP;
      courantIndice = position;
      courantListeP->informationP = infP;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(nbrItemsAvant == nbrItemsListe);
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // remplace (VoidP, EntierN)


//**************************************************************
// METHODE: CLListeSimplementChainee::reqElement (VoidP&, EntierN)
//
// Créé le  29 avril 1993  par  François Gingras
//**************************************************************
//
// Description:
//   Retourne l'information concernant le noeud en position "position"
//   de la liste.
//
// Visibilité: public
//
// Entrée:
//   EntierN position: le numéro du noeud à consulter
//
// Sortie:
//   VoidP& infP: l'information à retourner
//
// Codes d'erreur:
//
// Préconditions:
//   Vérifie que la liste n'est pas vide
//   Vérifie que le numéro ne dépasse le nombre d'items de la liste
//   Vérifie que le numéro n'est pas nul
//
// Postconditions:
//
// Algorithmes:
//
// Références:
//
// Notes:
//   courantListeP est initialisé au noeud qui est retourné
//   les noeuds sont numérotés de 1 à nbrItemsListe
//
//**************************************************************
Entier CLListeSimplementChainee::reqElement (VoidP& infP, EntierN position)
{
#ifdef MODE_DEBUG
   PRECONDITION(nbrItemsListe != 0);
   PRECONDITION(position != 0);
   PRECONDITION(position <= nbrItemsListe);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   if (courantIndice == position)
      infP = courantListeP->informationP;
   else
   {

      //--- Si courantIndice < position, on part de la position déterminée
      //--- par courantListeP, et si non on part de la tête
      if (courantIndice > position)
      {
         courantListeP = teteListeP;
         courantIndice = 1;
      }
      for (EntierN k=courantIndice; k<position; k++)
         courantListeP = courantListeP->suivantP;
      infP = courantListeP->informationP;
      courantIndice = position;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG

   return OK;
}  // reqElement(VoidP&, EntierN)





