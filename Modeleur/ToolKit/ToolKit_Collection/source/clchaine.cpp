//*****************************************************************************
// $Id$
// $Date$
//*****************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//*****************************************************************************
// Fichier: clchaine.cpp
// Classe:  CLChaine
//*****************************************************************************
// 24-02-1993  André Houde        Version initiale
// 27-09-1995  Yves Roy           Révision de l'interface et de l'implémentation
//                                Transfert des préconditions, postconditions
//                                et invariant vers la nouvelle façon de faire.
// 07-12-1995  Yves Roy           Renforcement des contrats
// 05-09-1996  Yves Roy           Révision de la précision et du format des conversions
//                                de réels pour avoir le mème comportement que dans FIFichier.
// 25-09-1996  Yves Secretan      Conversion des entiers en mode décimal.
// 28-01-1997  Yves Secretan      Cast en CarN les parametres à toupper et tolower
// 22-10-1997  Bernard Doyon      Passage à la double précision.
// 04-04-1998  Yves Secretan      Profiling
// 15-07-1998  Yves Secretan      GNU-Egcs
// 06-08-1998  Yves Secretan      GNU-Egcs (passe 2)
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 28-04-1999  Yves Secretan      Supporte les iostream du standard
// 27-05-2003  Dominique Richard  Port multi-compilateur
//*****************************************************************************
#include "clchaine.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sstream>

const EntierN CLChaine::TAILLE_BLOC = 32;
const Car     CLChaine::TABULATEUR  =  '\t';

//*****************************************************************************
// Sommaire: Constructeur par défaut
//
// Description:
//    Constructeur par défaut. Initialise le pointeur à chaine vide ("") et
//    la dimension à TAILLE_BLOC.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
CLChaine::CLChaine()
   : chaineP(NUL), dimension(0)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // #ifdef MODE_DEBUG

   alloueMemoire(TAILLE_BLOC);
   chaineP[0] = '\0';

#ifdef MODE_DEBUG
   POSTCONDITION(dimension == TAILLE_BLOC);
   POSTCONDITION(*this == "");
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::CLChaine


//*****************************************************************************
// Sommaire: Constructeur prenant un ConstCarP
//
// Description:
//    Constructeur initialisant la chaine au paramètre reçu.
//
// Entrée:
//    ConstCarP strP : Chaîne de caractères à copier.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
CLChaine::CLChaine(ConstCarP strP)
: chaineP(NUL), dimension(0)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
#endif   // #ifdef MODE_DEBUG

   alloueMemoire( static_cast<EntierN>(strlen(strP)+1) );
   strcpy(chaineP, strP);

#ifdef MODE_DEBUG
   POSTCONDITION(*this == strP);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::CLChaine(ConstCarP)

//*****************************************************************************
// Sommaire: Constructeur prenant un const Car
//
// Description:
//    Constructeur initialisant la chaine au paramètre reçu.
//
// Entrée:
//    const Car c : caractère à copier.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
CLChaine::CLChaine(const Car c)
: chaineP(NUL), dimension(0)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
#endif   // #ifdef MODE_DEBUG

   alloueMemoire(TAILLE_BLOC);
   chaineP[0] = c;
   chaineP[1] = '\0';

#ifdef MODE_DEBUG
   POSTCONDITION(dimension == TAILLE_BLOC);
   POSTCONDITION(*this == c);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::CLChaine(ConstCarP)

//*****************************************************************************
// Sommaire: Constructeur prenant un ConstCarP
//
// Description:
//    Constructeur initialisant la chaine au paramètre reçu.
//
// Entrée:
//    ConstCarP strP : Chaîne de caractères à copier.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
CLChaine::CLChaine(const std::string& str)
   : chaineP(NUL), dimension(0)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // #ifdef MODE_DEBUG

   alloueMemoire( static_cast<EntierN>(str.size()+1) );
   strcpy(chaineP, str.c_str());

#ifdef MODE_DEBUG
   POSTCONDITION(*this == str);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::CLChaine(ConstCarP)

//*****************************************************************************
// Sommaire: Constructeur copie
//
// Description:
//
// Entrée:
//    const CLChaine &copie : Chaîne à copier.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
CLChaine::CLChaine(const CLChaine &obj)
   : chaineP(NUL), dimension(0)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
#endif   // #ifdef MODE_DEBUG

   alloueMemoire(obj.dimension);
   strcpy(chaineP, obj.chaineP);

#ifdef MODE_DEBUG
   POSTCONDITION(dimension == obj.dimension);
   POSTCONDITION(*this == obj);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::CLChaine(const CLChaine&)

//*****************************************************************************
// Sommaire: Destructeur
//
// Description: Libère la chaine de caractères
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
CLChaine::~CLChaine()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   delete [] chaineP;

}  // CLChaine::~CLChaine

//*****************************************************************************
// Sommaire: Ajoute à la chaine une autre chaine ConstCarP
//
// Description:
//    Ajoute une chaîne à la fin de la chaîne courante.
//
// Entrée:
//    ConstCarP strP : Pointeur à la chaîne à ajouter.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajoute(ConstCarP strP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   alloueMemoire( static_cast<EntierN>(strlen(chaineP) + strlen(strP) + 1) );
   strncat(chaineP, strP, strlen(strP));

#ifdef MODE_DEBUG
   CarP rP = chaineP + strlen(chaineP) - strlen(strP);
   POSTCONDITION(strcmp(rP, strP) == 0);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return;
}  // CLChaine::ajoute(ConstCarP)

//*****************************************************************************
// Sommaire: Ajoute une CLChaine à la fin de celle-ci.
//
// Description:
//    Ajoute une chaîne à la fin de la chaîne courante.
//
// Entrée:
//    const CLChaine& ch : référence à la chaîne à ajouter.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajoute(const CLChaine& ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   alloueMemoire( static_cast<EntierN>(strlen(chaineP) + strlen(ch.chaineP) + 1) );
   strcat(chaineP, ch.chaineP);

#ifdef MODE_DEBUG
   CarP rP = chaineP + strlen(chaineP) - strlen(ch.chaineP);
   POSTCONDITION(strcmp(rP, ch.chaineP) == 0);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajoute(ConstCLChaineP)

//*****************************************************************************
// Sommaire: Ajoute un caractère à la fin de la chaine.
//
// Description:
//    Ajoute un caractère à la fin de la chaîne courante.
//
// Entrée:
//    Car c : un caractère à la chaîne à ajouter.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajoute(const Car c)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   alloueMemoire( static_cast<EntierN>(strlen(chaineP) + 2) );
   CarP cP = chaineP + strlen(chaineP);
   *cP++ = c,
   *cP   = '\0';

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}

//*****************************************************************************
// Sommaire: Ajoute un entier à la fin de la chaine.
//
// Description:
//    Ecrit un entier à la fin de la chaîne.
//
// Entrée:
//    const Entier& nbr : Nombre à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajoute(const Entier& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car c[36];
   if (sprintf(c, "%ld", nbr) != EOF)
   {
      ajoute(c);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajoute(const Entier&)

//*****************************************************************************
// Sommaire: Ajoute un EntierN à la fin de la chaine.
//
// Description:
//    Ecrit un entier à la fin de la chaîne en le transformant dans en
//    char[].
//
// Entrée:
//    const EntierN& nbr : Nombre à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajoute(const EntierN& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car c[36];
   if (sprintf(c, "%lu", nbr) != EOF)
   {
      ajoute(c);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajoute(const EntierN&)

//*****************************************************************************
// Sommaire: Ajoute un Réel à la fin de la chaine.
//
// Description:
//    Ecrit un réel à la fin de la chaîne en le transformant d'abord en
//    DReel
//
// Entrée:
//    const Reel& nbr : Réel à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajoute(const Reel& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car chNbr[50];
   if (sprintf(chNbr, "%.7e", nbr) != EOF)
   {
      ajoute(chNbr);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG

}  // CLChaine::ajoute(const DReel&)

//*****************************************************************************
// Sommaire: Ajoute un Réel à la fin de la chaine.
//
// Description:
//    Ecrit un réel à la fin de la chaîne en le transformant d'abord en
//    char[].
//
// Entrée:
//    const DReel& nbr : Réel à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajoute(const DReel& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car chNbr[50];

   if (sprintf(chNbr, "%.16e", nbr) != EOF)
   {
      ajoute(chNbr);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajoute(const DReel&)

//*****************************************************************************
// Sommaire: Écrit le nombre sous le format HH::MM:SS à la fin
//           de la chaine.
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM:SS ou HH:MM
//    (si les secondes sont à zéro) à la fin de la chaîne.
//
// Entrée:
//    const EntierN& nbr : Nombre à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajouteHeure(const EntierN& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   EntierN secondes = (nbr % 3600) % 60;
   if (secondes > 0)
   {
      ajouteHeureMinutesSecondes(nbr);
   }
   else
   {
      ajouteHeureMinutes(nbr);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajouteHeure

//*****************************************************************************
// Sommaire:  Ajoute une heure sous le format HH::MM:SS.DD donc
//            avec les décimales.
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM:SS.DD
//    à la fin de la chaîne.
//
// Entrée:
//    const Reel& nbr : Nombre à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajouteHeure(const Reel& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(nbr >= 0.0);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   EntierN secondes = (EntierN(nbr) % 3600) % 60;

   if (nbr - EntierN(nbr) > 0.0 || secondes > 0)
      ajouteHeureMinutesSecondes(EntierN(nbr));
   else
      ajouteHeureMinutes(EntierN(nbr));

   Car chNbr[50];
   if (sprintf(chNbr, "%g", Reel(nbr - EntierN(nbr))) != EOF && strstr(chNbr, ".") != NUL)
   {
      (*this) << strstr(chNbr, ".");
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajouteHeure(const Reel&)

//*****************************************************************************
// Sommaire:
//   Ajoute une heure sous le format HH::MM:SS.DD donc avec les décimales.
//
// Description:
//   Ecrit le nombre (en secondes) sous format HH:MM:SS.DD à la fin de la
//   chaîne.
//
// Entrée:
//   const DReel& nbr :  Le nombre à écrire.
//
// Sortie:
//
// Notes:
//*****************************************************************************
void CLChaine::ajouteHeure(const DReel& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(nbr >= 0.0);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   EntierN secondes = (EntierN(nbr) % 3600) % 60;

   if (nbr - EntierN(nbr) > 0.0 || secondes > 0)
      ajouteHeureMinutesSecondes(EntierN(nbr));
   else
      ajouteHeureMinutes(EntierN(nbr));

   CLChaine ch;
   ch << Reel(nbr - EntierN(nbr));
   ch.reqSousChaineDroite(ch, '.');
   if (!ch.estVide())
   {
      (*this) << '.' << ch;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajouteHeure (const DReel&)

//*****************************************************************************
// Sommaire: Ajoute un nombre de secondes à la fin de la chaine
//           sous le format HH::MM.
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM
//    à la fin de la chaîne.  S'il reste des secondes, on n'en tiend pas
//    compte.
//
// Entrée:
//    const EntierN& nbr : Nombre à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajouteHeureMinutes(const EntierN& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   CLChaine ch;

   EntierN heures  = nbr / 3600;
   EntierN minutes = (nbr % 3600) / 60;

   ch << heures << ':';
   if (minutes < 10)
      ch << '0';
   ch << minutes;

   ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajouteHeureMinutes

//*****************************************************************************
// Sommaire: Ajoute le format HH:MM:SS à la fin de la chaine
//
// Description:
//    Ecrit le nombre (en secondes) sous format HH:MM:SS
//    à la fin de la chaîne.
//
// Entrée:
//    const EntierN& nbr : Nombre à écrire.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::ajouteHeureMinutesSecondes(const EntierN& nbr)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   CLChaine ch;

   EntierN secondes = (nbr % 3600) % 60;

   ch << ':';
   if (secondes < 10)
      ch << '0';
   ch << secondes;

   ajouteHeureMinutes(nbr);
   ajoute(ch);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::ajouteHeureMinutesSecondes

//*****************************************************************************
// Sommaire: Alloue la mémoire nécessaire à la chaine.
//
// Description: Alloue la mémoire pour un multiple de CLCHAINE_TAILLE_BLOC
//              et recopie chaineP dedans.
//
// Entrée:
//    EntierN taille : nouvelle taille nécessaire à la chaîne.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::alloueMemoire(const EntierN taille)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(taille > 0);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   if (dimension >= taille)
   {
      return;
   }

   // --- On calcule la dimension nécessaire donnant un multiple de
   // --- TAILLE_BLOC.  On alloue et on met tout à 0.
   dimension = (taille / TAILLE_BLOC) * TAILLE_BLOC;
   if (taille % TAILLE_BLOC > 0)
   {
      dimension = dimension + TAILLE_BLOC;
   }

   // --- On alloue la mémoire et on la met à zéro
   CarP tempP = new Car[dimension];
   memset(tempP, 0, dimension);

   if (chaineP != NUL)
   {
      memcpy(tempP, chaineP, strlen(chaineP)+1);
      delete [] chaineP;
   }
   else
   {
      tempP[0] = '\0';
   }
   chaineP = tempP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::alloueMemoire

//*****************************************************************************
// Sommaire: Convertis la chaine en entier.
//
// Description:
//    Convertis cette chaîne en entier.
//
// Entrée:
//
// Sortie:
//    Entier &nbr     : le nombre contenu dans la chaîne.
//
// Notes:
//    La transformation se fait en mode décimal.
//
//*****************************************************************************
Booleen CLChaine::convertis(Entier &nbr) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Booleen succes = FAUX;
   Entier buffer;

   INRS_IOS_STD::istringstream is(chaineP);
   is >> INRS_IOS_STD::dec >> buffer;
   if (is)
   {
      nbr = buffer;
      succes = VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return succes;
}  // CLChaine::convertis(Entier&)

//*****************************************************************************
// Sommaire: Convertis la chaine en EntierN
//
// Description:
//    Convertis cette chaîne en entier non-signé.
//
// Entrée:
//
// Sortie:
//    EntierN &nbr    : le nombre contenu dans la chaîne.
//
// Note:
//    La transformation se fait en mode décimal.
//
//*****************************************************************************
Booleen CLChaine::convertis(EntierN &nbr) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Booleen succes = VRAI;

   // --- On vérifie d'abord si le nombre est négatif
   CarP carErrP = chaineP;
   while (*carErrP == ' ')
   {
      carErrP++;
   }
   if (*carErrP == '-')
   {
      succes = FAUX;
   }

   if (succes)
   {
      succes = FAUX;
      EntierN buffer;

      INRS_IOS_STD::istringstream is(chaineP);
      is >> INRS_IOS_STD::dec >> buffer;
      if (is)
      {
         nbr = buffer;
         succes = VRAI;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return succes;
}  // CLChaine::convertis(EntierN&)

//*****************************************************************************
// Sommaire: Convertir la chaine en réel
//
// Description:
//    Convertis cette chaîne en réel.
//
// Entrée:
//
// Sortie:
//    DReel &nbr      : le nombre contenu dans la chaîne.
//
// Notes:
//
//*****************************************************************************
Booleen CLChaine::convertis(DReel &nbr) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Booleen succes = FAUX;
   DReel buffer;

   INRS_IOS_STD::istringstream is(chaineP);
   is >> buffer;
   if (is)
   {
      nbr = buffer;
      succes = VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return succes;
}  // CLChaine::convertis(DReel&)

//*****************************************************************************
// Sommaire: Convertir la chaine en simple réel
//
// Description:
//    Convertis cette chaîne en réel.
//
// Entrée:
//
// Sortie:
//    Reel &nbr       : le nombre contenu dans la chaîne.
//
// Notes:
//*****************************************************************************
Booleen CLChaine::convertis(Reel &nbr) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Booleen succes = FAUX;
   Reel buffer;

   INRS_IOS_STD::istringstream is(chaineP);
   is >> buffer;
   if (is)
   {
      nbr = buffer;
      succes = VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return succes;
}  // CLChaine::convertis(Reel)

//*****************************************************************************
// Sommaire: Convertir la chaine en secondes.
//
// Description:
//    Convertis cette chaîne en secondes. On s'attend à un format du
//    type HEURE:MINUTES:SECONDES.DECIMALES.  Les espaces sont acceptés mais
//    tout autre caractère superflu ou manquant retourne une erreur.
//    Les formats des heures seulement (HEURE), heures-minutes (HEURE:MINUTES)
//    ou heures-minutes-secondes(HEURE:MINUTES:SECONDES) sont aussi acceptés.
//
// Entrée:
//
// Sortie:
//    Reel &nbr   : la chaîne convertie en secondes.
//
// Notes:
//    Les minutes et les secondes débordant 59 sont converties en ajoutant
//    à l'entité précédente (soit les heures et les minutes).
//
//*****************************************************************************
Booleen CLChaine::convertisHeureEnSec(Reel &nbr) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   nbr = 0;

   CLChaine ch(*this);
   ch.elimineCaractere(' ');

   // --- On calcule les heures
   CLChaine temp;
   ch.reqSousChaineGauche(temp, ':');

   EntierN hr;
   Booleen succes = temp.convertis(hr);
   if (succes)
   {
      // --- On ajoute les secondes correspondant aux heures.
      nbr = static_cast<Reel>(hr*3600.0);
      ch.reqSousChaineDroite(ch, ':');
   }

   if (succes && !ch.estVide()) {
      // --- On a aussi des minutes
      ch.reqSousChaineGauche(temp, ':');

      EntierN min;
      succes = temp.convertis(min);
      if (succes)
      {
         // --- On ajoute les secondes correspondant aux minutes.
         nbr += min * 60;
         ch.reqSousChaineDroite(ch, ':');
      }
   }

   if (succes && !ch.estVide())
   {
      // --- On a aussi des secondes
      ch.reqSousChaineGauche(temp, '.');

      EntierN sec;
      succes = temp.convertis(sec);
      if (succes)
      {
         // --- On ajoute les secondes.
         nbr += sec;
         ch.reqSousChaineDroite(ch, '.');
      }
   }

   if (succes && !ch.estVide())
   {
      // --- On a aussi des fractions de secondes
      Reel fraction;
      ch.insere(0, "0.");
      succes = ch.convertis(fraction);
      if (succes)
      {
         nbr += fraction;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return succes;
}  // CLChaine::convertisHeureEnSec(Reel&)

//*****************************************************************************
// Sommaire:  Convertir la chaine en secondes.
//
// Description:
//   Convertis cette chaîne en secondes. On s'attend à un format du type
//   HEURE:MINUTES:SECONDES.DECIMALES.  Les espaces sont acceptés mais
//   tout autre caractère superflu ou manquant retourne une erreur.  Les
//   formats des heures seulement (HEURE), heures-minutes (HEURE:MINUTES)
//   ou heures-minutes-secondes(HEURE:MINUTES:SECONDES) sont aussi acceptés.
//
// Entrée:
//
// Sortie:
//   DReel& nbr :  La chaîne convertie en secondes.
//
// Notes:
//   Les minutes et les secondes débordant 59 sont converties en ajoutant
//   à l'entité précédente (soit les heures et les minutes).
//
//*****************************************************************************
Booleen CLChaine::convertisHeureEnSec (DReel& nbr) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   nbr = 0;

   CLChaine ch(*this);
   ch.elimineCaractere(' ');

   // --- On calcule les heures
   CLChaine temp;
   ch.reqSousChaineGauche(temp, ':');

   EntierN hr;
   Booleen succes = temp.convertis(hr);
   if (succes)
   {
      // --- On ajoute les secondes correspondant aux heures.
      nbr = hr*3600;
      ch.reqSousChaineDroite(ch, ':');
   }

   if (succes && !ch.estVide()) {
      // --- On a aussi des minutes
      ch.reqSousChaineGauche(temp, ':');

      EntierN min;
      succes = temp.convertis(min);
      if (succes)
      {
         // --- On ajoute les secondes correspondant aux minutes.
         nbr += min * 60;
         ch.reqSousChaineDroite(ch, ':');
      }
   }

   if (succes && !ch.estVide())
   {
      // --- On a aussi des secondes
      ch.reqSousChaineGauche(temp, '.');

      EntierN sec;
      succes = temp.convertis(sec);
      if (succes)
      {
         // --- On ajoute les secondes.
         nbr += sec;
         ch.reqSousChaineDroite(ch, '.');
      }
   }

   if (succes && !ch.estVide())
   {
      // --- On a aussi des fractions de secondes
      Reel fraction;
      ch.insere(0, "0.");
      succes = ch.convertis(fraction);
      if (succes)
      {
         nbr += fraction;
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return succes;
}  // CLChaine::convertisHeureEnSec (DReel&)

//*****************************************************************************
// Sommaire: Convertir la chaine en secondes.
//
// Description:
//     On appelle le convertisHeureEnSec(Reel&)
//
// Entrée:
//
// Sortie:
//    Reel &nbr   : la chaîne convertie en secondes.
//
// Notes:
//    Les minutes et les secondes débordant 59 sont converties en ajoutant
//    à l'entité précédente (soit les heures et les minutes).
//
//*****************************************************************************
Booleen CLChaine::convertisHeureEnSec(EntierN &nbr) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Reel r = 0.0;

   Booleen succes = convertisHeureEnSec(r);
   if (succes)
   {
      nbr = EntierN(r);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return succes;
}

//*****************************************************************************
// Sommaire: Élimine une sorte de caractère dans la chaine.
//
// Description:
//    Elimine une sorte de caractère dans la chaîne.
//
// Entrée:
//    Car caractere   : Caractère à éliminer.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::elimineCaractere(const Car caractere)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(caractere != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   if (caractere == '\n')
   {
      CarP c1P = chaineP;
      CarP c2P = c1P;
      while (*c2P != '\0')
      {
         if (*c2P != '\n' && *c2P != '\r')
            *c1P++ = *c2P;
         ++c2P;
      }
      *c1P = '\0';
   }
   else
   {
      CarP c1P = chaineP;
      CarP c2P = c1P;
      while (*c2P != '\0')
      {
         if (*c2P != caractere)
            *c1P++ = *c2P;
         ++c2P;
      }
      *c1P = '\0';
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::elimineCaractere

//****************************************************************************
// Sommaire: Enlève des caractères de la chaine à partir d'une position.
//
// Description:
//    Enlève des caractères de la chaîne à partir d'une position donnée.
//
// Entrée:
//    EntierN position : position de départ.
//    EntierN nombre   : nombre de caractères à enlever (par défaut = 1).
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::enleveCaracteres(const EntierN position, const EntierN nombre)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position < strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   CarP c1P = chaineP + position;
   if ((position+nombre) <= strlen(chaineP))
   {
      CarP c2P = c1P + nombre;
      while (*c2P != '\0')
      {
         *c1P++ = *c2P++;
      }
   }
   *c1P = '\0';

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::enleveCaracteres

//*****************************************************************************
// Sommaire: Enlève n caractères du début de la chaine.
//
// Description:
//    Enlève x caractères du début de cette chaîne.
//
// Entrée:
//    EntierN nombre : Nombre de caractères à enlever du début de la chaîne.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::enleveDebut(const EntierN nombre)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(nombre <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   CarP c1P = chaineP;
   CarP c2P = c1P + nombre;
   while (*c2P != '\0')
   {
      *c1P++ = *c2P++;
   }
   *c1P = '\0';

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::enleveDebut

//*****************************************************************************
// Sommaire: Enlève les espaces au début et à la fin de la chaine.
//
// Description:
//    Enlève les espaces du début et de la fin de cette chaîne.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::enleveEspaces()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   enleveEspacesDebut();
   enleveEspacesFin();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // enleveEspaces


//*****************************************************************************
// Sommaire: Enlève les espaces au début de la chaine.
//
// Description:
//    Enlève les espaces du début de cette chaîne.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::enleveEspacesDebut()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   CarP c1P = chaineP;
   CarP c2P = c1P;
   while (*c2P != '\0' && (*c2P == ' ' || *c2P == TABULATEUR))
      ++c2P;
   while (*c2P != '\0')
      *c1P++ = *c2P++;
   *c1P = '\0';

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // enleveEspacesDebut


//*****************************************************************************
// Sommaire: Enlève les espaces à la fin de la chaine.
//
// Description:
//    Enlève les espaces à la fin de cette chaîne.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::enleveEspacesFin()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   if (strlen(chaineP) > 0)
   {
      CarP cP = chaineP + strlen(chaineP) - 1;
      while (cP != chaineP && (*cP == ' ' || *cP == TABULATEUR))
         --cP;
      *++cP = '\0';
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // enleveEspacesFin

//*****************************************************************************
// Sommaire: Enlève n caractères à la fin de la chaine.
//
// Description:
//    Enlève x caractères à la fin de cette chaîne.
//
// Entrée:
//    EntierN nombre : Nombre de caractères à enlever à la fin.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::enleveFin(const EntierN nombre)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(nombre <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   chaineP[strlen(chaineP)-nombre] = '\0';

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::enleveFin

//*****************************************************************************
// Sommaire: Insère une sous-chaine à une position donnée.
//
// Description:
//   Insère une sous-chaîne à une position donnée dans la chaine.
//
// Entrée:
//    EntierN   position    : emplacement où l'on droit insérer la sous-chaîne
//    ConstCarP sousChaineP : sous-chaîne à insérer
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::insere(const EntierN position, ConstCarP sousChaineP)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(sousChaineP != NUL);
   PRECONDITION(position <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   alloueMemoire( static_cast<EntierN>(strlen(chaineP) + strlen(sousChaineP) + 1) );
   if (position == strlen(chaineP))
   {
      strcat(chaineP, sousChaineP);
   }
   else
   {
      CarP srcP = chaineP + position;
      CarP dstP = srcP + strlen(sousChaineP);
      size_t n  = strlen(chaineP) - position + 1;
      memmove(dstP, srcP, n);
      strncpy (srcP, sousChaineP, strlen(sousChaineP));
   }

#ifdef MODE_DEBUG
   CarP rP = chaineP + position;
   POSTCONDITION(strncmp(rP, sousChaineP, strlen(sousChaineP)) == 0);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::insere

//*****************************************************************************
// Sommaire: Transforme la chaine en majuscule.
//
// Description:
//    Transforme la chaîne en majuscules.  Les caractères non
//    alphabétiques sont laissés inchangés.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::majuscule()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   const char* srcP = chaineP;
         char* dstP = chaineP;
   while (*srcP) *dstP++ = toupper(*srcP++);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::majuscule

//*****************************************************************************
// Sommaire: Transforme la chaine en minuscules
//
// Description:
//    Transforme la chaîne en minuscules.  Les caractères non
//    alphabétiques sont laissés inchangés.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::minuscule()
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   const char* srcP = chaineP;
         char* dstP = chaineP;
   while (*srcP) *dstP++ = tolower(*srcP++);

#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::minuscule()

//*****************************************************************************
// Sommaire: Recherche une sous-chaine à partir d'une position donnée
//           dans la chaine.
//
// Description:
//    Procure la position du début de la sous-chaîne en commençant
//    la recherche à la position spécifiée (0 par défaut)
//
// Entrée:
//    ConstCarP chaineRechercheP : Chaîne recherchée.
//    EntierN   posiDepart       : Position de début de recherche
//
// Sortie:
//    Position de début de la chaîne.
//
// Notes:
//
//*****************************************************************************
Booleen CLChaine::recherche(EntierN&  positionTrouve,
                            ConstCarP chaineRechercheP,
                            const EntierN positionDepart) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(chaineRechercheP != NUL);
   PRECONDITION(positionDepart <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Booleen trouve = FAUX;
   CarP rP = strstr(chaineP + positionDepart, chaineRechercheP);
   if (rP != NUL)
   {
      trouve = VRAI;
      positionTrouve = static_cast<EntierN>(rP - chaineP);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return trouve;
}  // CLChaine::recherche(EntierN&, CarP, EntierN)


//*****************************************************************************
// Sommaire: Recherche à rebours d'une sous-chaine
//
// Description:
//
// Entrée:
//    ConstCarP chaineRechercheP : pointeur de début de chaîne recherchée.
//    Entier    positionDepart   : Position de début de recherche
//
// Sortie:
//    EntierN& positionTrouve    : Position de début de la chaîne.
//
// Notes:
//
//*****************************************************************************
Booleen CLChaine::rechercheArriere(EntierN &positionTrouve,
                                   ConstCarP chaineRechercheP,
                                   const Entier positionDepart) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(chaineRechercheP != NUL);
   PRECONDITION(positionDepart <= Entier(strlen(chaineP)));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Booleen trouve = FAUX;

   CarP finP = chaineP;
   if (positionDepart < 0)
      finP += strlen(chaineP);
   else
      finP += positionDepart+1;

   CarP chercheP = chaineP;
   CarP trouveP  = NUL;
   while (chercheP < finP)
   {
      CarP nP = strstr(chercheP, chaineRechercheP);
      if (nP == NULL || nP+strlen(chaineRechercheP) > finP)
         break;
      trouveP  = nP;
      chercheP = nP + 1;
   }
   if (trouveP != NUL)
   {
      trouve = VRAI;
      positionTrouve = static_cast<EntierN>(trouveP - chaineP);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return trouve;
}  // CLChaine::rechercheArriere(...ConstCarP...)

//*****************************************************************************
// Sommaire: Retourne une sous-chaine.
//
// Description:
//    Donne la sous chaine contenue à partir de la position donnée
//    pour une longueur de x caractères (nombre).
//
// Entrée:
//    EntierN position  : Position de début de la sous-chaine.
//    EntierN nombre    : Nombre de caractères à copier.
//
// Sortie:
//    CLChaine &ch      : Chaine contenant la sous-chaine.
//
// Notes:
//
//*****************************************************************************
void CLChaine::reqSousChaine(CLChaine &ch, const EntierN position,
                             const EntierN nombre) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position + nombre <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ch = *this;
   ch.enleveDebut(position);

   if (nombre < strlen(ch.chaineP))
   {
      ch.tronque(nombre);
   }

#ifdef MODE_DEBUG
   POSTCONDITION(ch.reqTaille() == nombre);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::reqSousChaine

//*****************************************************************************
// Sommaire: Retourne une sous-chaine à partir d'un certaine position.
//
// Description:
//    Donne la sous chaine contenu à partir de la position donnée
//    (position incluse).
//
// Entrée:
//    EntierN position  : Position de début de la sous-chaine.
//
// Sortie:
//    CLChaine &ch      : Chaine contenant la sous-chaine.
//
// Notes:
//
//*****************************************************************************
void CLChaine::reqSousChaineDroite(CLChaine &ch, const EntierN position) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ch = *this;
   ch.enleveDebut(position);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::reqSousChaineDroite

//*****************************************************************************
// Sommaire: Retourne une sous-chaine à partir d'un certain caractère
//
// Description:
//    Donne la sous chaine contenue à partir du caractère donnée
//    (caractère non-inclus).  Si le caractère ne se trouve pas dans la chaine
//    la sous-chaine demeure vide.
//
// Entrée:
//    Car element       : Caractère à rechercher.
//
// Sortie:
//    CLChaine &ch      : Chaine contenant la sous-chaine.
//
// Notes:
//     On utilise la première occurence du caractère.
//
//*****************************************************************************
void CLChaine::reqSousChaineDroite(CLChaine &ch, const Car element) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(element != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   EntierN position;
   if (recherche(position, element))
   {
      ch = *this;
      ch.enleveDebut(position+1);
   }
   else
   {
      ch = "";
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::reqSousChaineDroite(...const Car...)

//*****************************************************************************
// Sommaire: Retourne une sous-chaine à gauche de la position donnée
//
// Description:
//    Donne la sous chaine contenue à gauche de la position donnée
//    (position non-incluse).
//
// Entrée:
//    EntierN position  : Position de début de la sous-chaine.
//
// Sortie:
//    CLChaine &ch      : Chaine contenant la sous-chaine.
//
// Notes:
//
//*****************************************************************************
void CLChaine::reqSousChaineGauche(CLChaine &ch, const EntierN position) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   ch = *this;
   ch.tronque(position);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::reqSousChaineGauche

//*****************************************************************************
// Sommaire: Retourne une sous-chaine à gauche d'un caractère donnée.
//
// Description:
//    Donne la sous chaine contenue à gauche du caractère donnée
//    (caractère non-inclus).  Si le caractère ne se trouve pas dans la chaine
//    TOUTE la chaine devient la sous-chaine.
//
// Entrée:
//    Car element       : Caractère de début de la sous-chaine.
//
// Sortie:
//    CLChaine &ch      : Chaine contenant la sous-chaine.
//
// Notes:
//     On utilise la première occurence du caractère.
//
//*****************************************************************************
void CLChaine::reqSousChaineGauche(CLChaine &ch, const Car element) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(element != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   EntierN position;
   ch = *this;
   if (recherche(position, element))
   {
      ch.tronque(position);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::reqSousChaineGauche(...const Car...)

//*****************************************************************************
// Sommaire: Tronque la chaine à partir de la position donnée
//
// Description:
//    Enlève les éléments à la fin de la chaîne courante.
//
// Entrée:
//    EntierN position : Position de la nouvelle fin de la chaîne.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void CLChaine::tronque(const EntierN position)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(position <= strlen(chaineP));
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   chaineP[position] = '\0';

#ifdef MODE_DEBUG
   POSTCONDITION(strlen(chaineP)==position);
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
}  // CLChaine::tronque

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'addition.
//
// Description:
//    Additionne cette chaine et la chaine reçue en paramètre et
//    retourne le résultat par valeur.
//
// Entrée:
//    const CarP chP : Chaine à ajouter.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaine CLChaine::operator+(ConstCarP strP) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   CLChaine ch(*this);
   ch.ajoute(strP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // #ifdef MODE_DEBUG
   return ch;
}  // CLChaine::operator+

//*****************************************************************************
// Sommaire: Surcharge de l'opérateur d'addition.
//
// Description:
//    Additionne cette chaine et le caractère reçu en paramètre
//    retourne le résultat par valeur.  La fonction appelle l'operator+
//    avec un ConstCarP pour faire le travail.
//
// Entrée:
//    const Car c : Caractère à ajouter.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaine CLChaine::operator+(const Car c) const
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_METHODE();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
   INVARIANTS("PRECONDITION");
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;

   // --- Appel de operator+(ConstCarP)
   return ((*this) + strP);
}

//*****************************************************************************
// Sommaire: Fonction friend à la chaine qui additionne deux chaines
//
// Description:
//    Additionne les deux chaines reçues en paramètre et retourne
//    le résultat par valeur.  Cette fonction appelle la fonction
//    friend operator+ avec un CarP
//
// Entrée:
//    const Car c          : Un caractère
//    const CLChaine &ch   : Une chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaine operator+(const Car c, const CLChaine& obj)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(c != '\0');
#endif   // #ifdef MODE_DEBUG

   Car strP[2] = "0";
   *strP = c;
   return (strP + obj);
}

//*****************************************************************************
// Sommaire: Fonction friend à la chaine qui additionne deux chaines
//
// Description:
//    Additionne les deux chaines reçues en paramètre et retourne
//    le résultat par valeur.
//
// Entrée:
//    const CarP chP       : Première chaine.
//    const CLChaine &ch   : Deuxième chaine.
//
// Sortie:
//
// Notes:
//*****************************************************************************
CLChaine operator+(ConstCarP strP, const CLChaine &ch)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)
#ifdef MODE_DEBUG
   PRECONDITION(strP != NUL);
#endif   // #ifdef MODE_DEBUG

   CLChaine ret(strP);
   ret.ajoute(ch);

   return ret;
}  // operator+(const CarP...)

//*****************************************************************************
// Sommaire: Fonction friend à la classe de surcharge de l'opérateur <<
//
// Description:  Cette fonction permet d'écrire une CLChaine dans une stream
//               (ostream) en utilisant l'opérateur d'insertion.
//               La fonction retourne la stream par référence pour permettre
//               les appels en cascade.
//
// Entrée: ostream& ostrm      : une stream d'output
//         const CLChaine &obj : un objet CLChaine
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
INRS_IOS_STD::ostream& operator<<(INRS_IOS_STD::ostream& ostrm, const CLChaine& obj)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   if (ostrm)
   {
      ostrm << obj.chaineP;
   }

   return ostrm ;
}

//*****************************************************************************
// Sommaire: Fonction friend à la classe de surcharge de l'opérateur <<
//
// Description:  Cette fonction permet de lire une CLChaine à partir d'une
//               stream (istream) en utilisant l'opérateur d'insertion.
//               La fonction retourne la stream par référence pour permettre
//               les appels en cascade.
//
// Entrée: istream& istrm      : une stream d'input
//         const CLChaine &obj : un objet CLChaine
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
INRS_IOS_STD::istream& operator>>(INRS_IOS_STD::istream& is, CLChaine& obj)
{
#if defined(MODE_TRACE) || defined(MODE_PROFILE)
   INSTRUMENTE_FONCTION();
#endif   // #if defined(MODE_TRACE) || defined(MODE_PROFILE)

   if (is)
   {
      const size_t LONGUEUR_BUFFER = 1024;
      const size_t LONGUEUR_DONNEE = LONGUEUR_BUFFER-1;
      char buffer[LONGUEUR_BUFFER];

      obj = "";
      do
      {
         is.getline(buffer, LONGUEUR_BUFFER);
         if (is)
            obj.ajoute(buffer);
      }
      while (is && strlen(buffer) == LONGUEUR_DONNEE);
   }

   return is;
}

//****************************************************************************
// Description: Cette méthode publique operator std::string () permet d'obtenir la chaine
//              correspondant au nom du répertoire sous la forme d'un std::string.
//
// Entrée:
//
// Sortie:      Retourne un std::string par valeur
//
// Notes:
//****************************************************************************
CLChaine::operator std::string () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return chaineP;
}



