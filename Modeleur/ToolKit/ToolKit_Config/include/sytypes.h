//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: sytypes.h
//
// Sommaire:   Définition de tous les types et les constantes
//
// Description:
//    Fichier définissant tous les types et les constantes
//    utilisées pour le projet HYDREAU.
//
//************************************************************************
// 26-09-1995 Yves Roy             Changer le type du Booleen de unsigned char pour int
//                                 Pour se conformer à la pratique courante.
//                                 Toute les expressions logiques retournent un int.
// 16-07-1996 Yves Granger         Définition des macros DYNAMIC_CAST() et
//                                 DECLARE_DYNAMIC_CAST().
// 16-10-1997  Yves Secretan       Passage à la double précision
// 04-11-1997  Yves Roy            Implantation de la classe de réel DReel à la place du double
// 29-11-1997  Yves Secretan       Adapte les ifdef de CONST_CAST et DYNAMIC_CAST
// 03-12-1997  Yves Secretan       Corrige les ifdef de CONST_CAST et DYNAMIC_CAST
// 04-12-1997  Yves Secretan       Recorrige les ifdef de CONST_CAST et DYNAMIC_CAST
// 04-04-1998  Yves Secretan       Ajout des macros de trace et profiling
// 05-08-1998  Yves Secretan       Ajout de include "syconfig.h"
// 01-09-1998  Yves Roy            Ajout notice de copyright
// 28-04-1999  Yves Secretan       Nouveau define de garde contre les compilations multiples
// 27-05-2003  Dominique Richard   Port multi-compilateur
// 11-07-2003  Maxime Derenne      Suppression des pointeurs en référence dans les DECLARE_...
// 08-06-2004  Stéphane Lévesque   Ajout des entiers sur 64 bits
//************************************************************************
#ifndef SYTYPES_H_DEJA_INCLU
#define SYTYPES_H_DEJA_INCLU

#ifndef SYCONFIG_H_DEJA_INCLU
#   include "syconfig.h"
#endif   // SYCONFIG_H_DEJA_INCLU

#define DECLARE_CLASS_DECOREE(deco, nom)   \
class         deco nom;                    \
typedef       nom *             nom ## P;  \
typedef const nom *    Const ## nom ## P

#define DECLARE_CLASS(nom)                 \
class         nom;                         \
typedef       nom *             nom ## P;  \
typedef const nom *    Const ## nom ## P

#define DECLARE_STRUCT_DECOREE(deco, nom)  \
struct        deco nom;                    \
typedef       nom *             nom ## P;  \
typedef const nom *    Const ## nom ## P

#define DECLARE_STRUCT(nom)                \
struct        nom;                         \
typedef       nom *             nom ## P;  \
typedef const nom *    Const ## nom ## P

#define DECLARE_TYPE(type, nom)            \
typedef       type              nom;       \
typedef       type *            nom ## P;  \
typedef const type *   Const ## nom ## P;  \
typedef       type * &          nom ## PR

DECLARE_TYPE(char, Car);
DECLARE_TYPE(unsigned char, CarN);
DECLARE_TYPE(bool, Booleen);
DECLARE_TYPE(short int, Ecourt);
DECLARE_TYPE(unsigned short int, EcourtN);
DECLARE_TYPE(long int, Entier);
DECLARE_TYPE(unsigned long int, EntierN);
DECLARE_TYPE(void, Void);

#if defined(_MSC_VER)
DECLARE_TYPE(__int64, Entier64);
DECLARE_TYPE(unsigned __int64, EntierN64);
#else
DECLARE_TYPE(signed long long int, Entier64);
DECLARE_TYPE(unsigned long long int, EntierN64);
#endif

DECLARE_TYPE(float, Reel);
DECLARE_TYPE(double, DReel);
DECLARE_TYPE(long double, LReel);
DECLARE_TYPE(void, Void);

const Entier OK  = 0;
const Booleen FAUX = false;
const Booleen VRAI = true;

// --- Utilisation d'une macro de remplacement pour éviter les type mismatches
#define NUL (0)

// --- Macro de trace et de profile
#ifdef MODE_PROFILE
#  include "ouprofil.h"
#  ifdef MODE_TRACE
#     include "outrace.h"
#     define INSTRUMENTE_FONCTION()                 \
         static long PRF_index = -1;                \
         PRF_ObjetProfiler PRF_objPRF(PRF_index);   \
         TRA_ObjetTrace TRA_objTRA();
#     define INSTRUMENTE_METHODE()                  \
         static long PRF_index = -1;                \
         PRF_ObjetProfiler PRF_objPRF(PRF_index);   \
         TRA_ObjetTrace TRA_objTRA(this);
#  else
#     define INSTRUMENTE_FONCTION()                 \
         static long PRF_index = -1;                \
         PRF_ObjetProfiler PRF_objPRF(PRF_index)
#     define INSTRUMENTE_METHODE()                  \
         static long PRF_index = -1;                \
         PRF_ObjetProfiler PRF_objPRF(PRF_index)
#  endif
#else
#  ifdef MODE_TRACE
#     include "outrace.h"
#     define INSTRUMENTE_FONCTION()                 \
         TRA_ObjetTrace TRA_objTRA();
#     define INSTRUMENTE_METHODE()                  \
         TRA_ObjetTrace TRA_objTRA(this);
#  else
#     define INSTRUMENTE_FONCTION()
#     define INSTRUMENTE_METHODE()
#  endif
#endif

#endif // SYTYPES_H_DEJA_INCLU




