//************************************************************************
// $Id$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: syclrrgb.h
//
// Classe: SYCouleurRGB
//
// Description:
//    Couleur dans le système Red, Green, Blue. Bien que de type Entité,
//    cette classe ne devrait pas être appelée directement. Elle est
//    l'implantation C++ d'une structure en C.
//
// Attributs:
//    CarN  r, g, b;        Composantes Red, Green, Blue de la couleur
//
// Notes:
//
//************************************************************************
// 25-01-93  Yves Secretan    Version initiale
// 04-01-96  Yves Roy         Modification sur les operateurs == et !=
// 21-11-96  Serge Dufour     Vérification des dépendances
// 29-09-98  Yves Secretan    Ajout des constructeurs et de l'operator <
//                            Migre les méthods dans un .hpp
//************************************************************************
#ifndef SYCLRRGB_H_DEJA_INCLU
#define SYCLRRGB_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

DECLARE_CLASS(SYCouleurRGB);

class SYCouleurRGB
{
public:
           SYCouleurRGB();
           SYCouleurRGB(CarN, CarN, CarN);

   Booleen operator!=  (const SYCouleurRGB&) const;
   Booleen operator==  (const SYCouleurRGB&) const;
   Booleen operator <  (const SYCouleurRGB&) const;

   CarN r, g, b;
};

#include "syclrrgb.hpp"

#endif // ifndef SYCLRRGB_H_DEJA_INCLU




