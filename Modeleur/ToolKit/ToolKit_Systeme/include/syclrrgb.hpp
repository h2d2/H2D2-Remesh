//************************************************************************
// $Id$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: syclrrgb.hpp
// Classe: SYCouleurRGB
//************************************************************************
// 29-09-98  Yves Secretan  Version initiale
//************************************************************************
#ifndef SYCLRRGB_HPP_DEJA_INCLU
#define SYCLRRGB_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:   Constructeur par défaut
//
// Description:
//    Le constructeur public <code>SYCouleurRGB()</code> construit un objet
//    par défaut à la couleur noire.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline SYCouleurRGB::SYCouleurRGB()
   : r(0), g(0), b(0)
{
}

//************************************************************************
// Sommaire: Constructeur avec composantes de couleur.
//
// Description:
//    Le constructeur public <code>SYCouleurRGB(...)</code> construit un objet
//    à partir des composantes de couleur passées en argument.
//
// Entrée:
//    CarN r0           Composantes de la couleur
//    CarN g0
//    CarN b0
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline SYCouleurRGB::SYCouleurRGB(CarN r0, CarN g0, CarN b0)
   : r(r0), g(g0), b(b0)
{
}

//************************************************************************
// Sommaire:   Opérateur != pour des couleurs.
//
// Description:
//    L'opérateur public <code>operator !=(...)</code> définit l'opération
//    != pour des couleurs.
//
// Entrée:
//    const SYCouleurRGB& coul;        Couleur à comparer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen SYCouleurRGB::operator!=(const SYCouleurRGB& coul) const
{
   return !(*this == coul);
}  // SYCouleurRGB::operator!=

//************************************************************************
// Sommaire:   Opérateur == pour des couleurs.
//
// Description:
//    L'opérateur public <code>operator ==(...)</code> définit l'opération
//    d'égalité pour des couleurs. Deux couleurs sont égales si toutes leurs
//    composantes sont égales.
//
// Entrée:
//    const SYCouleurRGB& coul;        Couleur à comparer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen SYCouleurRGB::operator== (const SYCouleurRGB& coul) const
{
   return (r == coul.r && g == coul.g && b == coul.b);
}  // SYCouleurRGB::operator==


//************************************************************************
// Sommaire: Opérateur de comparaison d'infériorité stricte.
//
// Description:
//    L'opérateur public <code>operator <(...)</code> définit l'opération
//    de comparaison d'infériorité stricte pour des couleurs. Il s'agit plus
//    d'un opérateur d'ordonancement que de comparaison.
//
// Entrée:
//    const SYCouleurRGB& c;        Couleur à comparer
//
// Sortie:
//
// Notes:
//
//************************************************************************
inline Booleen SYCouleurRGB::operator < (const SYCouleurRGB& c) const
{
   EntierN e1 = (  r << 16) | (  g << 8) | (  b);
   EntierN e2 = (c.r << 16) | (c.g << 8) | (c.b);
   return (e1 < e2);
}

#endif // ifndef SYCLRRGB_HPP_DEJA_INCLU




