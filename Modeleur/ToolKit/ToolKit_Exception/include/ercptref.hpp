//************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: tpcptref.hpp
// Classe:  TPCompteReference
//************************************************************************
// 19-09-1995  Yves Roy           Version initiale
// 15-11-1995  Yves Roy           Enlever l'inclusion conditionnelle
// 16-10-1997  Yves Secretan      Passage à la double précision
// 11-08-1998  Yves Secretan      Introduis MODE_TEMPLATE_EXTERNE
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************

#ifndef TPCPTREF_HPP_DEJA_INCLU
#define TPCPTREF_HPP_DEJA_INCLU

//***************************************************************
// Ce fichier devra être remplacé par les smart pointers de Boost
// C'est une duplication du fichier tpcptref.hpp
//***************************************************************

//************************************************************************
// Sommaire:    Constructeur de la classe
//
// Description: Ce constructeur accepte un objet en parametre.
//              On place l'objet dans la variable interne et on initialise
//              le compte de référence à 1.
//
// Entrée:      Un objet par référence.
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename T>
inline TPCompteReference<T>::TPCompteReference(const T& obj)
: unObjet(obj), compteReference(1)
{
#ifdef MODE_DEBUG
   POSTCONDITION(compteReference == 1);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Destructeur de la classe
//
// Description: Ce destructeur ne fait rien de particulier sauf vérifier
//              ses invariants de classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename T>
inline TPCompteReference<T>::~TPCompteReference()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//************************************************************************
// Sommaire:    Retourne une reference à l'objet contenu dans la classe
//
// Description: La méthode retourne une référence à l'objet à l'intérieur.
//              L'objet peut être modifié.
//
// Entrée:
//
// Sortie:      La référence à l'objet (non constante)
//
// Notes:
//
//************************************************************************
template <typename T>
inline T& TPCompteReference<T>::reqObjet()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return unObjet;
}

//************************************************************************
// Sommaire:    Retourne une reference constante à l'objet contenu
//              dans la classe.
//
// Description: La méthode retourne une référence à l'objet à l'intérieur.
//              L'objet ne peut être modifié, la référence est constante.
//
// Entrée:
//
// Sortie:      La référence à l'objet constante
//
// Notes:
//
//************************************************************************
template <typename T>
inline const T& TPCompteReference<T>::reqObjet() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return unObjet;
}

//************************************************************************
// Sommaire:    Retourne le compte de références.
//
// Description: La méthode retourne simplement le compte de références
//              en retournant un entier non signé.  Le méthode est constante.
//
// Entrée:
//
// Sortie:      Le compteReference.
//
// Notes:
//
//************************************************************************
template <typename T>
inline EntierN TPCompteReference<T>::reqCompte() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (EntierN)compteReference;
}

//************************************************************************
// Sommaire:    Retourne un booleen indiquant si l'objet est référé une
//              seule fois.
//
// Description: Le but de la méthode est d'offrir une interface descriptive
//              et ainsi mieux documenter le code.
//              <p>
//              Cette fonction teste donc si le compteReference est à un.
//
// Entrée:
//
// Sortie:      Un booleen à VRAI si compteReference==1
//
// Notes:
//
//************************************************************************
template <typename T>
inline Booleen TPCompteReference<T>::referenceUnique() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (compteReference == 1);
}

//************************************************************************
// Sommaire:    Incrémente le compte de références.
//
// Description: La méthode incrémente le compte de références simplement
//              en ajoutant une unité à l'attribut compteReference.
//
// Entrée:
//
// Sortie:      Le nouveau compteReference.
//
// Notes:
//
//************************************************************************
template <typename T>
inline EntierN TPCompteReference<T>::incrementeCompte()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (EntierN)compteReference++;
}

//************************************************************************
// Sommaire:    Décrémente le compte de références.
//
// Description: La méthode décrémente le compte de références simplement
//              en enlevant une unité à l'attribut compteReference.
//
// Entrée:
//
// Sortie:      Le nouveau compteReference.
//
// Notes:
//
//************************************************************************
template <typename T>
inline EntierN TPCompteReference<T>::decrementeCompte()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   return (EntierN)compteReference--;
}

#endif   // TPCPTREF_HPP_DEJA_INCLU




