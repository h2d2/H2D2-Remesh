//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     erexcept.h
//
// Classes:     ERExceptionContrat, ERExceptionAssertion, ERExceptionPrecondition,
//              ERExceptionPostcondition, ERExceptionInvariant
//
// Sommaire:    Hiérarchie pour la gestion des erreurs de théorie du contrat
//
// Description: Ces classes constituent la hiérarchie pour la gestion de la
//              théorie du contrat.  Elle maintient les données nécessaires à
//              la sauvegarde des renseignements de l'erreur.  Cette classe et sa
//              hiérarchie sont intéressantes lors de l'utilisation des
//              exceptions.
//              <pre>
//              ERExceptionContrat:       Classe de base de la hiérarchie.
//              ERExceptionAssertion:     Classe de gestion des erreurs d'assertion.
//              ERExceptionPrecondition:  Classe de gestion des erreurs de précondition.
//              ERExceptionPostcondition: Classe de gestion des erreurs de postcondition.
//              ERExceptionInvariant:     Classe de gestion des erreurs d'invariant.
//              </pre>
//              <p>
//              On note que la hiérarchie est virtuelle et la classe de base
//              abstraite.  Ceci est nécessaire pour supporter le lancement
//              d'exceptions à partir de la méthode lanceException().
//              Chacune des classes dérivées doivent donc définir cette méthode.
//
// Attributs:   ConstCarP messageP:    Le message pouvant servir de titre.
//              ConstCarP expressionP: Le test logique qui a échoué.
//              ConstCarP fichierP:    Le nom du fichier.
//              Entier    ligne:       Le numéro de linge
//
// Notes:  Cette hiérarchie de classe est inspiré du livre de Horstmann:
//         Mastering object oriented design in C++, 1995, Ref:12-2007257.
//         Voir la classe ChiError sur la disquette fournie avec le livre.
//
//************************************************************************
// 16-11-1995  Yves Roy           Version initiale
// 03-12-1995  Yves Roy           Transfert des macros de définition des PRE-POST...
//                                dans ce fichier et transfert dans "erold.h" des
//                                vieilles méthodes à détruire.
// 05-12-1995  Yves Roy           Transformation de la hiérarchie ERExceptionContrat:
//                                La classe de base est devenue purement abstraite et
//                                chacune des classes héritières redéfinissent la méthode
//                                lanceException(Booleen) pour pouvoir faire un throw
//                                sur le bon type.
// 10-11-1997  Yves Secretan      Corrige les macro d'INVARIANTS
// 10-08-1998  Yves Secretan      Supprime les macros devenues inutiles
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************
#ifndef EREXCEPT_H_DEJA_INCLU
#define EREXCEPT_H_DEJA_INCLU

#ifndef MODULE_TOOLKIT_ERREUR
#  define MODULE_TOOLKIT_ERREUR 1
#endif

#include "sytypes.h"
#include <string>

#ifdef INRS_MSVC
#  pragma warning(disable: 4251)
#endif   // INRS_MSVC

class DLL_IMPEXP(MODULE_TOOLKIT_ERREUR) ERExceptionContrat
{
public:
                 ERExceptionContrat  (const std::string&, Entier, const std::string&, const std::string&);
   virtual       ~ERExceptionContrat ();
   static void   afficheErreur       (const std::string&, const std::string&);
   void          arreteProgramme     ();
   virtual void  lanceException      (Booleen) = 0;

private:
   std::string message;
   std::string expression;
   std::string fichier;
   Entier   ligne;
};

class DLL_IMPEXP(MODULE_TOOLKIT_ERREUR) ERExceptionAssertion : public ERExceptionContrat
{
public:
                ERExceptionAssertion(const std::string&, Entier, const std::string&);
   virtual     ~ERExceptionAssertion();
   virtual void lanceException(Booleen);
};

class DLL_IMPEXP(MODULE_TOOLKIT_ERREUR) ERExceptionPrecondition : public ERExceptionContrat
{
public:
                ERExceptionPrecondition(const std::string&, Entier, const std::string&);
   virtual     ~ERExceptionPrecondition();
   virtual void lanceException(Booleen);
};

class DLL_IMPEXP(MODULE_TOOLKIT_ERREUR) ERExceptionPostcondition : public ERExceptionContrat
{
public:
                ERExceptionPostcondition(const std::string&, Entier, const std::string&);
   virtual     ~ERExceptionPostcondition();
   virtual void lanceException(Booleen);
};

class DLL_IMPEXP(MODULE_TOOLKIT_ERREUR) ERExceptionInvariant : public ERExceptionContrat
{
public:
                ERExceptionInvariant(const std::string&, Entier, const std::string&, const std::string&);
   virtual     ~ERExceptionInvariant();
   virtual void lanceException(Booleen);
};


// --- Définition des macros de contrôle de la théorie du contrat
//     suivant les différents modes d'utilisation

// --- LE MODE TEST UNITAIRE
#if defined(MODE_TEST)

#  ifndef MODE_DEBUG
#     define MODE_DEBUG
#  endif

#  define INVARIANTS(c) \
      invariant(c)

#  define ASSERTION(f)     \
      ERExceptionAssertion(__FILE__,__LINE__, #f).lanceException(!(f))
#  define PRECONDITION(f)  \
      ERExceptionPrecondition(__FILE__, __LINE__, #f).lanceException(!(f))
#  define POSTCONDITION(f) \
      ERExceptionPostcondition(__FILE__, __LINE__, #f).lanceException(!(f))
#  define INVARIANT(f,c)   \
      ERExceptionInvariant(__FILE__,__LINE__, #f, c).lanceException(!(f))

// --- LE MODE DEBUG STANDARD
#elif defined(MODE_DEBUG)

#  define INVARIANTS(c) \
      invariant(c)

#  define ASSERTION(f)     \
      if (!(f)) ERExceptionAssertion(__FILE__,__LINE__, #f).arreteProgramme()
#  define PRECONDITION(f)  \
      if (!(f)) ERExceptionPrecondition(__FILE__, __LINE__, #f).arreteProgramme()
#  define POSTCONDITION(f) \
      if (!(f)) ERExceptionPostcondition(__FILE__, __LINE__, #f).arreteProgramme()
#  define INVARIANT(f,c)   \
      if (!(f)) ERExceptionInvariant(__FILE__,__LINE__, #f, c).arreteProgramme()

// --- LE MODE RELEASE
#else

#  define PRECONDITION(f);
#  define POSTCONDITION(f);
#  define INVARIANTS(c);
#  define INVARIANT(f,c);
#  define ASSERTION(f);

#endif  // --- if defined (MODE_TEST)

#ifdef INRS_MSVC
#  pragma warning(disable: 4251)
#endif   // INRS_MSVC

#endif // EREXCEPT_H_DEJA_INCLU



