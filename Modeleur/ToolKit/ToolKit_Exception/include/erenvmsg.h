//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:      erenvmsg.h
//
// Classe:       EREnveloppeMessage
//
// Sommaire:     Classe servant à gérer le compte
//               des références aux objets ERMessage.
//
// Description:  Cette classe, par l'entremise de la classe
//               TPCompteReference, gère le compte des références aux
//               objets ERMessage. Selon le paradigme de la lettre et de
//               l'enveloppe, cette classe est l'enveloppe.
//               <p>
//               Le concept du compte des références vise à optimiser
//               l'utilisation de la mémoire en évitant de recopier des
//               objets identiques.
//               <p>
//               Puisque la plupart des méthodes retournent un message,
//               il est beaucoup plus profitable de retourner l'enveloppe
//               du message plutôt que le message lui-même (c'est-à-dire
//               l'objet ERMessage).
//
// Attributs:    messageP: Un pointeur sur un compteur de références
//                         à des objets ERMessage.
//
// Notes:        Pour plus d'informations, voir:
//               Horstmann, Cay S., Mastering Object-Oriented Design in
//               C++, pp. 258-287.
//************************************************************************
// 22-09-1994  Eric Paquet & Yves Roy  - version initiale
// 02-10-1995  Eric Paquet               Lorsque le constructeur par défaut est utilisé, on ne crée
//                                       pas d'objet ERMessage. Dans ce cas, on assume que le type
//                                       du message est OK. La mécanique des méthodes touchées
//                                       par ce changement est ajustée.
// 11-10-1995  Yves Roy:                 Retour à l'énumération Type et à sa conversion vers ERMessage<T>
//                                       lorsque nécessaire. On enlève le typedef ERMessage<T> Type
//                                       pour mettre l'énumération.
// 25-01-1996  Yves Roy                  Ajout d'un appel à ERMessage pour écrire la pile d'appel
//                                       lorsque nécessaire.
// 21-02-1996  Yves Secretan             Enleve le include de ererreur.h
// 21-11-1996  Serge Dufour              Vérification des dépendances
// 19-08-1997  Yves Secretan             Ajoute un include de erexcept.h
// 16-10-1997  Yves Secretan             Passage à la double précision
// 16-07-1998  Yves Secretan             Passe Type par valeur
// 11-08-1998  Yves Secretan             Introduis MODE_TEMPLATE_EXTERNE
// 01-09-1998  Yves Roy                  Ajout notice copyright
// 28-10-1998  Yves Secretan             Remplace #include "ererreur"
// 27-05-2003  Dominique Richard         Port multi-compilateur
// 11-07-2003  Maxime Derenne            Change l'abréviation SY pour ER
//************************************************************************
#ifndef ERENVMSG_H_DEJA_INCLU
#define ERENVMSG_H_DEJA_INCLU

#ifndef MODULE_TOOLKIT_ERREUR
#  define MODULE_TOOLKIT_ERREUR 1
#endif

#include "sytypes.h"
#include "erexcept.h"

#include "ercptref.h"
#include "ermessag.h"

template <typename T>
class EREnveloppeMessage
{
public:
   enum Type { OK            = ERMessage<T>::OK,
               AVERTISSEMENT = ERMessage<T>::AVERTISSEMENT,
               ERREUR        = ERMessage<T>::ERREUR,
               FATAL         = ERMessage<T>::FATAL };

   EREnveloppeMessage (Type = OK);
   EREnveloppeMessage (Type, const T&);
   EREnveloppeMessage (const EREnveloppeMessage<T>&);
   ~EREnveloppeMessage();

   Type                       reqType() const;
   EntierN                    reqNbrMessages() const;

   void                       ajoute (const T&);
   void                       ajoute (const EREnveloppeMessage<T>&);
   void                       asgType(Type);
   void                       cumule (const EREnveloppeMessage<T>&);

   const EREnveloppeMessage&  operator=(const EREnveloppeMessage<T>&);
   const T&                   operator [] (const EntierN) const;
                              operator void* () const;

protected:
   void invariant(ConstCarP) const;

private:
   void copie(const EREnveloppeMessage<T>&);
   void libere();
   void clone();

   TPCompteReference<ERMessage<T> >  *messageP;
};

//**************************************************************
// Sommaire:    Invariants de classe
//
// Description:
//
// Entrée:
//    ConstCarP conditionP
//
// Sortie:
//
// Notes:
//
//**************************************************************
template <typename T>
inline void EREnveloppeMessage<T>::invariant(ConstCarP /*conditionP*/) const
{
}

#include "erenvmsg.hpp"

#endif // ifndef ERENVMSG_H_DEJA_INCLU
