//************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: erenvmsg.hpp
// Classe:  EREnveloppeMessage
//************************************************************************
// 22-09-1994  Eric Paquet & Yves Roy  - version initiale
// 02-10-1995  Eric Paquet               Lorsque le constructeur par défaut est utilisé, on
//                                       ne crée pas d'objet ERMessage si aucun type n'est
//                                       donné en paramètre. Dans ce cas, on assume que le
//                                       type du message est OK. La mécanique des méthodes
//                                       touchées par ce changement est ajustée.
// 10-10-1995  Yves Roy                  Ai enlevé une postcondition dans le constructeur
//                                       parce que ce test supposait que tous les types T
//                                       pouvaient être convertis en char*.
// 11-10-1995  Yves Roy                  Retour à l'énumération Type et à sa conversion vers
//                                       ERMessage<T> lorsque nécessaire.
// 22-02-1996  Yves Roy                  Dans libere, on met le pointeur messageP à NUL
//                                       dans les deux cas.
// 16-10-1997  Yves Secretan             Passage à la double précision
// 16-07-1998  Yves Secretan             Passe Type par valeur
// 11-08-1998  Yves Secretan             Introduis MODE_TEMPLATE_EXTERNE
// 01-09-1998  Yves Roy                  Ajout notice copyright
// 27-05-2003  Dominique Richard         Port multi-compilateur
// 11-07-2003  Maxime Derenne            Change l'abréviation SY pour ER
//************************************************************************
#ifndef ERENVMSG_HPP_DEJA_INCLU
#define ERENVMSG_HPP_DEJA_INCLU

//************************************************************************
// Sommaire:    Constructeur par défaut de la classe.
//
// Description: Ce constructeur initialise les attributs à des valeurs
//              par défaut. Si le type du message est OK ou omis, messageP
//              est initialisé à NUL.
//              Le type du message est un paramètre optionnel initialisé
//              par défaut à OK.
//
// Entrée:      Type& unType: le type du message.
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline EREnveloppeMessage<T>::EREnveloppeMessage(Type unType)
: messageP(NUL)
{

   // --- Si le type du message est autre que OK, un objet
   // --- ERMessage est physiquement créé.
#ifndef INRS_BUG_NE_SUR_ENUM
   if (unType != OK)
#else
   if (! (unType == OK))
#endif
   {
      messageP = new TPCompteReference< ERMessage<T> >
                                 (ERMessage<T> ((typename ERMessage<T>::Type)unType));
   }

#ifdef MODE_DEBUG
#ifndef INRS_BUG_NE_SUR_ENUM
   POSTCONDITION(unType != OK || messageP == NUL);
#else
   POSTCONDITION(!(unType == OK) || messageP == NUL);
#endif
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Constructeur de la classe
//
// Description: Ce constructeur crée un TPCompteReference<ERMessage> en
//              lui assignant un messsge contruit à partir des arguments.
//
// Entrée:      Type&    type:    le type du message.
//              const T& chaine:  la chaîne contenant le message.
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline EREnveloppeMessage<T>::EREnveloppeMessage(Type type, const T& chaine)
: messageP(new TPCompteReference< ERMessage<T> >
                        (ERMessage<T> ((typename ERMessage<T>::Type)type, chaine)))
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Constructeur copie de la classe
//
// Description: Ce constructeur construit l'objet à partir d'un objet du
//              même type passé en paramètre.
//
// Entrée:      obj: L'objet sur lequel se construire.
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline EREnveloppeMessage<T>::EREnveloppeMessage(const EREnveloppeMessage<T> &obj)
: messageP(0)
{
   // --- Appel de la méthode privée qui s'occupe des copies et du compte
   // --- de références.
   copie(obj);

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == obj.messageP);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Destructeur de la classe
//
// Description: Ce destructeur gère le compte de références avant de
//              laisser détruire l'objet
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline EREnveloppeMessage<T>::~EREnveloppeMessage()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
   // --- Appel à la méthode privée qui s'occupe de libérer les ressources
   // --- et qui gère le compte de références.
   if (messageP != NUL)
   {
      libere();
   }
}

//************************************************************************
// Sommaire:     Cette méthode ajoute un message à la fin de la liste.
//
// Description:  Cette méthode réimplante la méthode de l'objet auquel
//               on réfère.  On passe par le TPCompteReference<Erreur>
//               pour accéder à l'objet.
//               <p>
//               Notons que dans TPCompteReference<T> on a définit deux
//               méthodes reqObjet, une const et l'autre non const.
//               La méthode non const est utilisée ici.
//               <p>
//               On doit implanter dans cette méthode la notion du copy on
//               write puisque que cette méthode modifie l'objet.  Ainsi,
//               lorsque cette méthode est appelée on doit cloner l'objet
//               référencé pour avoir un comportement prévisible.
//               <p>
//               Si le pointeur à l'objet TPCompteReference (messageP)
//               est nul, un tel objet doit évidemment être créé.
//
// Entrée:       chaine: le message à ajouter.
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
void EREnveloppeMessage<T>::ajoute(const T& chaine)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (messageP != NUL)
   {
      // --- On applique la théorie du copie on write
      // --- On appelle clone() avant de modifier.
      clone();
      ERMessage<T>& objet = messageP->reqObjet();
      objet.ajoute(chaine);
   }
   else
   {
      messageP = new TPCompteReference< ERMessage<T> >
                     (ERMessage<T> (ERMessage<T>::OK, chaine));
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:   Ajoute un message.
//
// Description:
//    La méthode publique <code>ajoute(...)</code> ajoute un message à
//    l'objet courant.
//
// Entrée:
//    const EREnveloppeMessage<T> &obj;   Message à ajouter
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
void EREnveloppeMessage<T>::ajoute(const EREnveloppeMessage<T> &obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (messageP == NUL || (void*)messageP->reqObjet())
   {
      *this = obj;
   }
   else
   {
      EntierN nbrMsg = obj.reqNbrMessages();
      for (EntierN i=0; i<nbrMsg; i++)
      {
         ajoute(obj[i]);
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:     Cette méthode assigne un type au message.
//
// Description:  Cette méthode réimplante la méthode de l'objet auquel
//               on réfère.  On passe par le TPCompteReference<Erreur>
//               pour accéder à l'objet.
//               <p>
//               Notons que dans TPCompteReference<T> on a définit deux
//               méthodes reqObjet, une const et l'autre non const.
//               La méthode non const est utilisée ici.
//               <p>
//               On doit implanter dans cette méthode la notion du copy on
//               write puisque que cette méthode modifie l'objet.  Ainsi,
//               lorsque cette méthode est appelée on doit cloner l'objet
//               référencé pour avoir un comportement prévisible.
//               <p>
//               Si le pointeur à l'objet TPCompteReference (messageP)
//               est nul, un tel objet doit évidemment être créé.
//
// Entrée:       const Type& unType: le type du message.
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline void EREnveloppeMessage<T>::asgType (Type unType)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (messageP != NUL)
   {
      // --- On applique la théorie du copie on write
      // --- On appelle clone() avant de modifier.
      clone();
      ERMessage<T>& objet = messageP->reqObjet();
      objet.asgType((typename ERMessage<T>::Type)unType);
   }
   else
   {
      // --- Un nouveau message de type 'unType' est créé.
      messageP = new TPCompteReference< ERMessage<T> >
                              (ERMessage<T> ((typename ERMessage<T>::Type)unType));
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:   Ajoute un message.
//
// Description:
//    La méthode publique <code>ajoute(...)</code> ajoute un message à
//    l'objet courant.
//
// Entrée:
//    const EREnveloppeMessage<T> &obj;   Message à ajouter
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
void EREnveloppeMessage<T>::cumule(const EREnveloppeMessage<T> &obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   ajoute(obj);
   if (reqType() < obj.reqType()) asgType(obj.reqType());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Créer une copie de l'objet courant.
//
// Description: Cette méthode privée crée un clone de l'objet couramment
//              référencé.  On doit donc décrémenter notre compte de
//              références sur l'objet courant et ensuite se cloner.  Par
//              contre, si le compte de références est égal à 1, on n'a
//              pas besoin de cloner puisqu'on est seul à référer l'objet.
//              <p>
//              Donc pour implanter le copie on write, on n'a qu'à appeler
//              cette méthode à l'intérieur de chacune des méthodes modifiant
//              l'objet, i.e. les méthodes non const.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
void EREnveloppeMessage<T>::clone()
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP != NUL);
   INVARIANTS("PRECONDITION");
#endif
   // --- S'il ne reste que nous-même à référer à l'erreur
   // --- on n'a pas besoin de cloner.
   if (!messageP->referenceUnique()) {

      // --- Cloner signifie ne plus référer à l'ancien message
      // --- mais plutôt prendre une copie de celui-ci et
      // --- partir une nouvelle branche.
      messageP->decrementeCompte();
      messageP = new TPCompteReference<ERMessage<T> >
         (ERMessage<T>(messageP->reqObjet()));
   }

#ifdef MODE_DEBUG
   POSTCONDITION(messageP->referenceUnique());
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Gère l'allocation des ressources et le compte de
//              références
//
// Description: Cette méthode privée gère l'allocation de ressources et le
//              compte de références sur l'objet. Toutes les copies
//              doivent passer par ici pour ne pas créer de trous dans
//              la gestion du compte de références.
//
// Entrée:      L'objet du même type à assigner.
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline void EREnveloppeMessage<T>::copie(const EREnveloppeMessage &obj)
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP == NUL);
   INVARIANTS("PRECONDITION");
#endif

   messageP = obj.messageP;
   if (messageP != NUL)
   {
      messageP->incrementeCompte();
   }

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == obj.messageP);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:    Gère la désallocation des ressources et le compte de
//              références
//
// Description: Cette méthode privée gère la désallocation de ressources
//              et le compte de références sur l'objet.  Toutes les
//              libérations doivent passer par ici pour ne pas créer de
//              trous dans la gestion du compte de références.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline void EREnveloppeMessage<T>::libere()
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP != NUL);
   INVARIANTS("PRECONDITION");
#endif

   if (messageP->referenceUnique())
   {
      // --- Il ne reste qu'une seule référence à l'objet,
      // --- on doit alors détruire l'objet.
      delete messageP;
   }
   else
   {
      messageP->decrementeCompte();
   }
   messageP = NUL;

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == NUL);
   INVARIANTS("POSTCONDITION");
#endif
}

//************************************************************************
// Sommaire:     Cette méthode retourne le nombre de messages dans la liste.
//
// Description:  reqNbrMessages retourne le nombre de messages dans la liste.
//               <p>
//               Cette méthode réimplante la méthode de l'objet auquel
//               on référence.  On passe par le TPCompteReference<Erreur>
//               pour accéder à l'objet.
//               <p>
//               Notons que dans TPCompteReference<T> on a défini deux
//               méthodes reqObjet, une const et l'autre non const.
//               La méthode const est utilisée ici.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline EntierN EREnveloppeMessage<T>::reqNbrMessages() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   EntierN  n;

   if (messageP != NUL)
   {
      const ERMessage<T>& objet = messageP->reqObjet();
      n = objet.reqNbrMessages();
   }
   else
   {
      n = 0;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return n;
}

//************************************************************************
// Sommaire:     Méthode d'accès au type du message
//
// Description:  Cette méthode réimplante la méthode de l'objet auquel
//               on référence.  On passe par le TPCompteReference<Erreur>
//               pour accéder à l'objet.
//               <p>
//               Notons que dans TPCompteReference<T> on a défini deux
//               méthodes reqObjet, une const et l'autre non const.
//               La méthode const est utilisée ici.
//               <p>
//               Si le pointeur à l'objet TPCompteReference (messageP)
//               est nul, on assume que le type du message est 'OK'.
//
// Entrée:
//
// Sortie:       Une référence au type de l'erreur.
//
// Notes:
//************************************************************************
template <typename T>
inline typename EREnveloppeMessage<T>::Type EREnveloppeMessage<T>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   typename EREnveloppeMessage<T>::Type typeRet;

   if (messageP != NUL)
   {
      const ERMessage<T>& objet = messageP->reqObjet();
      typeRet = (typename EREnveloppeMessage<T>::Type)objet.reqType();
   }
   else
   {
      typeRet = EREnveloppeMessage<T>::OK;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return typeRet;
}

//************************************************************************
// Sommaire:    Opérateur d'assignation de la classe
//
// Description: Cet opérateur d'assignation permet d'utiliser
//              l'assignation de façon cohérente et sans danger pour
//              l'intégrité du système de la lettre et de l'enveloppe.
//              <p>
//              Il faut d'abord libérer les ressources qu'on possède et gérer
//              le compte de références sur l'objet.  Ensuite on doit assigner
//              la nouvelle référence et gérer le compte de références.
//
// Entrée:      L'objet du même type à assigner.
//
// Sortie:      Un référence constante à l'objet lui-même.
//
// Notes:
//************************************************************************
template <typename T>
inline const EREnveloppeMessage<T>&
EREnveloppeMessage<T>::operator=(const EREnveloppeMessage<T> &obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   // --- Vérification de l'auto-assignation.
   if (this != &obj)
   {
      if (messageP != NUL)
      {
         libere();
      }
      copie(obj);
   }

#ifdef MODE_DEBUG
   POSTCONDITION(messageP == obj.messageP);
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//************************************************************************
// Sommaire:     Retourne le message de la liste en position ind.
//
// Description:  Le message est composé d'une liste de sous-messages.
//               ex: cet énoncé retourne le sous-message de m en position i:
//               <p>
//               <pre>
//               m [i];
//               </pre>
//
// Entrée:
//    EntierN ind    : indice du sous-message à retourner
//
// Sortie:
//
// Notes:
//************************************************************************
template <typename T>
inline const T& EREnveloppeMessage<T>::operator [] (const EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(messageP != NUL);
   PRECONDITION(ind < this->reqNbrMessages());
   INVARIANTS("PRECONDITION");
#endif

   ERMessage<T>& objet = messageP->reqObjet();
   return objet[ind];
}

//************************************************************************
// Sommaire:     Permet d'offrir un raccourci pour savoir s'il y a une
//               erreur.
//
// Description:  La surcharge de l'opérateur void* permet de retourner
//               implicitement VRAI ou FAUX lorsqu'on utilise l'objet
//               dans une expression logique.
//               Ainsi, si le message est du type OK, le test sera
//               VRAI et si le message est d'un autre type, il sera FAUX.
//               Il ne s'agit pas du vrai opérateur void*, i.e. que ce
//               cast ne retourne pas l'adresse de la structure.
//               <p>
//               Exemple:
//               <p>
//               <pre>
//               ERMsg msg = ERMsg::OK;
//               msg = obj.uneMethode();
//               ...
//               if (msg)
//               {
//                  ...
//                  cout << "Il n'y a pas d'erreur" << endl;
//               }
//               else
//               {
//                  cout << "Il y a une erreur" << endl;
//               }
//               </pre>
//
// Entrée:
//
// Sortie:
//
// Notes:        On n'a pas utilisé le cast en int pour faire le test
//               pour éviter l'utilisation du résultat dans des calculs
//               arithmétique.  Le cast en void* nous assure que le
//               résultat ne peut être utilisé pour autre chose.
//************************************************************************
template <typename T>
inline EREnveloppeMessage<T>::operator void*() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   void* voidP;

   // --- Si le pointeur au message n'est pas NUL, on retourne le statut
   // --- de l'objet (VRAI si le message n'est pas en erreur, FAUX sinon).
   // --- Si le pointeur au message est NUL, on assume que le type celui-ci
   // --- est OK, ainsi on retourne VRAI.
   if (messageP != NUL)
   {
      voidP = (void*)messageP->reqObjet();
   }
   else
   {
      voidP = (void*)VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
   return voidP;
}

#endif   // #ifndef ERENVMSG_HPP_DEJA_INCLU
