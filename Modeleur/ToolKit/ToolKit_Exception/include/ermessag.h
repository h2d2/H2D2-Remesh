//************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//******************************************************************************
// Fichier: ermessag.h
//
// Classe: ERMessage
//
// Sommaire:
//     La classe ERMessage permet de créer des messages.
//
// Description:
//     Cette classe est une template paramétrisée par un type T.
//     Elle permet de décrire un message sous la forme d'une
//     suite d'objets de type T. L'objet ERMessage est associé à un type OK,
//     AVERTISSEMENT, ERREUR ou FATAL.
//     <p>
//     L'objet de type T doit avoir défini: un constructeur par défaut,
//     un constructeur copie, les opérateurs =, == et <. Ce dernier
//     opérateur doit être défini pour les besoins du tri de lstMessages.
//     <p>
//     Voici un exemple d'utilisation de la classe ERMessage:
//     <pre>
//     typedef ERMessage<CLChaine> ERMsg;
//     ERMsg msg = ERMsg::OK;  // Création d'un message de type OK.
//     ...
//     if (erreur_trouvee)
//     {
//        // --- La méthode doit retourner une erreur
//        msg = ERMsg(ERMsg::ERREUR, "ERR_DESCRIPTION_ERREUR");
//     }
//     return msg;
//     </pre>
//
// Attributs:     Booleen pileAffichee  Dit si on a déjà écrit la pile d'appel
//                T       lstMessages   Liste des messages
//                Type    type          Type du message
//
// Notes:
//******************************************************************************
// 14-08-1995  Pierre Trudel      Version initiale
// 25-09-1995  Yves Secretan,     Yves Roy, Eric Paquet
// 25-01-1996  Yves Roy           Rendre void*() const, ajout de la pile d'appel
// 15-04-1996  Yves Secretan      Prise en compte des namespace
// 21-11-1996  Serge Dufour       Vérification des dépendances
// 11-08-1998  Yves Secretan      Introduis MODE_TEMPLATE_EXTERNE
//                                Change le deque pour un vector à cause de GNU
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 27-05-2003  Dominique Richard  Port multi-compilateur
// 11-07-2003  Maxime Derenne     Change l'abréviation SY pour ER
//******************************************************************************
#ifndef ERMESSAG_H_DEJA_INCLU
#define ERMESSAG_H_DEJA_INCLU

#ifndef MODULE_TOOLKIT_ERREUR
#  define MODULE_TOOLKIT_ERREUR 1
#endif

#include "sytypes.h"

#include <vector>

template <typename T>
class ERMessage
{
public:
   enum Type { OK, AVERTISSEMENT, ERREUR, FATAL };

                       ERMessage(typename ERMessage<T>::Type = OK);
                       ERMessage(typename ERMessage<T>::Type, const T&);
                       ERMessage(const ERMessage<T> &obj);
                       ~ERMessage();

   void                ajoute(const T&);
   void                asgType(Type);
   void                affichePileAppel();

   EntierN             reqNbrMessages() const;
   Type                reqType() const;
   const ERMessage<T>& operator=(const ERMessage<T>&);
   const T&            operator[](EntierN) const;
                       operator void*() const;

protected:
   void                invariant(ConstCarP) const;

private:
   Booleen             pileAffichee;
   Type                type;
   std::vector<T>      lstMessages;
};

#include "ermessag.hpp"

//******************************************************************************
// Description: Invariants de la classe
//
// Entrée:
//    ConstCarP  conditionP :
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
inline void ERMessage<T>::invariant(ConstCarP /*conditionP*/) const
{
}

#endif // ERMESSAG_H_DEJA_INCLU




