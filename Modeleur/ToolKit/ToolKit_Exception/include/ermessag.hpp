//************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//******************************************************************************
// Fichier: ermessag.hpp
// Classe:  ERMessage
//******************************************************************************
// 14-08-1995  Pierre Trudel           Version intiale
// 25-09-1995  Yves Roy, Eric Paquet   Modifications majeures au code,
//                                     modification des commentaires de façon à répondre
//                                     aux nouvelles normes de programmation.
// 29-09-1995  Eric Paquet             Inversion du code de retour (maintenant, si
//                                     ERMessage est OK, la valeur retournée est VRAI).
// 25-01-1996  Yves Roy                Mettre void*() const et ajout pile d'appel
// 11-08-1998  Yves Secretan           Introduis MODE_TEMPLATE_EXTERNE
// 01-09-1998  Yves Roy                Ajout notice copyright
// 27-05-2003  Dominique Richard       Port multi-compilateur
// 11-07-2003  Maxime Derenne          Change l'abréviation SY pour ER
//******************************************************************************
#ifndef ERMESSAG_HPP_DEJA_INCLU
#define ERMESSAG_HPP_DEJA_INCLU

#ifdef PILE_APPEL
#  include "oustack.h"  // pour pouvoir écrire la pile d'appel
#endif   // PILE_APPEL

//******************************************************************************
// Sommaire:    Constructeur par défaut.
//
// Description: Cette méthode initialise les attributs de la classe
//              avec des valeurs par défaut (la liste de messages est
//              laissée vide et le type (optionnel) est initialisé à prmType).
//              Le type est initialisé par défaut à OK.
//
// Entrée:      Type prmType (optionnel)    Type du message
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
ERMessage<T>::ERMessage(typename ERMessage<T>::Type prmType)
   : pileAffichee(FAUX), type(prmType)
{
#ifdef MODE_DEBUG
   POSTCONDITION(lstMessages.size() == 0);
   INVARIANTS ("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:     Construit un objet ERMessage et assigne aux
//               attributs les valeurs passées en paramètres.
//
// Description:  Les paramètres passés sont le message et le type du message.
//               La longueur de la liste de messages égalera donc 1.
//
// Entrée:       typeMessage: le type de message (OK,AVERTISSEMENT,ERREUR,FATAL)
//               message:     une chaine
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
ERMessage<T>::ERMessage (typename ERMessage<T>::Type typeMessage, const T& message)
   : pileAffichee(FAUX), type(typeMessage)
{
   lstMessages.push_back(message);

#ifdef MODE_DEBUG
   POSTCONDITION(typeMessage == type);
   POSTCONDITION(lstMessages.size() == 1);
   INVARIANTS ("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:  Constructeur copie de la classe.
//
// Description: Construit un objet ERMessage et initialise le nouvel objet
//              avec les valeurs emmagasinées dans l'objet passé en
//              paramètre.
//
// Entrée:  obj: l'objet du même type
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
ERMessage<T>::ERMessage(const ERMessage<T>& obj)
   : pileAffichee(obj.pileAffichee), type(obj.type), lstMessages(obj.lstMessages)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:  Destructeur.
//
// Description:  Aucune ressource à libérer.
//
// Entrée:
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
ERMessage<T>::~ERMessage()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif
}

//******************************************************************************
// Sommaire:  Opérateur d'assignation
//
// Description:  Cette méthode implante la surcharge de l'opérateur d'assigna-
//               tion.  On assume qu'un objet de type T a défini l'opérateur =.
//
// Entrée:       obj: un objet du même type.
//
// Sortie:       On retourne l'objet par référence pour permettre les appels
//               en cascade.
//
// Notes:
//******************************************************************************
template <typename T>
const ERMessage<T>& ERMessage<T>::operator=(const ERMessage<T> &obj)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   if (this != &obj)
   {
      type = obj.type;
      pileAffichee = obj.pileAffichee;
      lstMessages = obj.lstMessages;
   }

#ifdef MODE_DEBUG
   POSTCONDITION(type == obj.type);
   INVARIANTS("POSTCONDITION");
#endif
   return *this;
}

//******************************************************************************
// Sommaire:    Assigne le type de message
//
// Description: Cette méthode permet d'assigner le type du message.
//
// Entrée:      prmType: le type de message
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
void ERMessage<T>::asgType(Type prmType)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   type = prmType;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:     Cette méthode ajoute un message à la fin de la liste.
//
// Description:  Par exemple, le premier message de la liste peut être
//               "Fichier introuvable" et le second message peut contenir
//               le nom du fichier.
//
// Entrée: const T& message    le message à ajouter
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
void ERMessage<T>::ajoute(const T& message)
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif

   // ---  Ajoute à la liste
   lstMessages.push_back(message);

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:     Cette méthode permet d'écrire la pile d'appel pour le débuggage.
//
// Description:  Cette méthode publique permet d'écrire la pile d'appel pour le
//               débuggage.  Cette méthode, lorsqu'appelée, vérifie que la pile
//               n'a pas déjà été écrite pour cette erreur et que c'est effectivement
//               une erreur.
//
// Entrée:
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
void ERMessage<T>::affichePileAppel()
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif

#ifdef MODE_DEBUG
   if (!pileAffichee && (type == ERREUR || type == FATAL))
   {
#ifdef PILE_APPEL
      STK_affichePileAppel();
#endif
      pileAffichee = VRAI;
   }
#endif

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif
}

//******************************************************************************
// Sommaire:     Retourne le type de message.
//
// Description:  Cette méthode retourne le type du message couramment stocké.
//
// Entrée:
//
// Sortie:       typename ERMessage<T>::Type : le type de message
//
// Notes:
//******************************************************************************
template <typename T>
typename ERMessage<T>::Type ERMessage<T>::reqType() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return type;
}

//******************************************************************************
// Sommaire:    Retourne le nombre de messages dans la liste.
//
// Description: Cette méthode retourne le nombre de messages dans la liste.
//
// Entrée:
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
EntierN ERMessage<T>::reqNbrMessages() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return static_cast<EntierN>(lstMessages.size());
}


//******************************************************************************
// Sommaire:    Retourne le ieme message
//
// Description: Cette méthode retourne le ieme message dans la liste.
//              ind doit être compris entre 0 et la longueur de la liste - 1.
//
// Entrée:
//    EntierN ind    : Indice du message à retourner
//
// Sortie:
//
// Notes:
//******************************************************************************
template <typename T>
const T& ERMessage<T>::operator [] (EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ind < lstMessages.size());
   INVARIANTS("PRECONDITION");
#endif

   return lstMessages[ind];
}

//******************************************************************************
// Sommaire:    Permet de faire un test logique sur l'état du message
//
// Description: Cette méthode permet de faire un test logique sur l'état du
//              message.  Ainsi, si le message est du type OK, le test sera
//              VRAI et si le message est d'un autre type, il sera FAUX.
//              <p>
//              Il ne s'agit pas d'un cast en void* traditionnel, i.e. qu'on
//              obtient pas l'adresse de la structure comme résultat.
//              <p>
//              Exemple:
//              <p>
//              <pre>
//              ERMessage<CLChaine> une_methode();
//              ERMessage<CLChaine> ret;
//              ...
//              ret = une_methode();   // utilisation de l'opérateur =
//              if (ret) {             // utilisation de l'opérateur void*
//                 ...
//                 cout << "Il n'y a pas d'erreur" << endl;
//              }
//              else
//              {
//                 cout << "Il y a une erreur" << endl;
//                 cout << ret.reqMessageErreur() << endl;
//              }
//              </pre>
//
// Entrée:
//
// Sortie:
//
// Notes:       Nous avons préféré l'utilisation du cast en void* à celui en
//              int pour éviter l'utilisation du résultat dans des opérations
//              arithmétiques.
//******************************************************************************
template <typename T>
ERMessage<T>::operator void*() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif

   return (type == ERMessage<T>::OK) ? (void*)VRAI : (void*)FAUX;
}

#endif // ERMESSAG_HPP_DEJA_INCLU


