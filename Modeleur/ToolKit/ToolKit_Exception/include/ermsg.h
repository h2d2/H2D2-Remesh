//************************************************************************
// $Id$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:     ermsg.h
//
// Classe:      ERMsg
//
// Sommaire:    Interface de définition de la classe de messages d'erreur
//
// Description: Cette interface permet d'instancier la classe template
//              EREnveloppeMessage<T> sur le type de chaîne de caractères
//              désiré.  On fait un typedef pour définir le type ERMsg.
//
// Attributs:
//
// Note:
//
//************************************************************************
// 08-06-1998  Yves Roy           Version initiale
// 12-06-1998  Yves Roy           Commentaires
// 15-07-1998  Yves Secretan      Change std:: pour std::
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 27-05-2003  Dominique Richard  Port multi-compilateur
// 11-07-2003  Maxime Derenne     Change l'abréviation SY pour ER
//************************************************************************
#ifndef ERMSG_H_DEJA_INCLU
#define ERMSG_H_DEJA_INCLU

#ifndef MODULE_TOOLKIT_ERREUR
#  define MODULE_TOOLKIT_ERREUR 1
#endif

#include <string>
#include "erenvmsg.h"
typedef EREnveloppeMessage<std::string> ERMsg;

#endif
