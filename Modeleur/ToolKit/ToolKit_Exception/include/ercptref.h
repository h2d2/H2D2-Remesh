//************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier:      ercptref.h
//
// Classe:       TPCompteReference<T>
//
// Sommaire:     Classe utilitaire pour l'implantation du compte de références
//               sur un objet quelconque.
//
// Description:  Cette classe conserve simplement le compte de références
//               sur l'objet qu'elle possède.  Le compte de références est
//               piloté par l'enveloppe de l'objet.
//               <p>
//               Cette classe ne devrait donc pas être utiliser seule, elle
//               est conçue pour être utilisée à l'intérieur de n'importe quelle
//               enveloppe implantant le compte de références sur un objet.
//               <p>
//               Cette classe offre ou impose une interface pour faire le
//               compte de références sur n'importe quel objet.  L'objet lui-
//               même fonctionne normalement.
//
// Attributs:    unObjet: l'instance de l'objet lui-même
//               compteReference: le compte de référence sur cet objet
//
// Notes:
//
//************************************************************************
// 18-09-1994  Yves Roy           Version initiale
// 26-02-1996  Yves Roy           Enleve include "ererreur.h"
// 14-01-1997  Serge Dufour       Vérification des dépendances
// 16-10-1997  Yves Secretan      Passage à la double précision
// 11-08-1998  Yves Secretan      Introduis MODE_TEMPLATE_EXTERNE
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 28-10-1998  Yves Secretan      Remplace #include "ererreur"
// 27-05-2003  Dominique Richard  Port multi-compilateur
//************************************************************************

#ifndef TPCPTREF_H_DEJA_INCLU
#define TPCPTREF_H_DEJA_INCLU

//***************************************************************
// Ce fichier devra être remplacé par les smart pointers de Boost
// C'est une duplication du fichier tpcptref.h
//***************************************************************
#include "sytypes.h"
#include "erexcept.h"

#ifndef MODULE_TOOLKIT_ERREUR
#  define MODULE_TOOLKIT_ERREUR 1
#endif

template <typename T>
class TPCompteReference
{
public:
                      TPCompteReference(const T& obj);
                      ~TPCompteReference();
   T&                 reqObjet();
   const T&           reqObjet() const;
   EntierN            reqCompte() const;
   Booleen            referenceUnique() const;
   EntierN            incrementeCompte();
   EntierN            decrementeCompte();

protected:
   void               invariant(ConstCarP) const;

private:
   // --- Ne pas utiliser l'opérateur d'assignation
   TPCompteReference<T>& operator=(const TPCompteReference<T> &obj);

   T                  unObjet;
   Entier             compteReference;
};

//**************************************************************
// Sommaire:    Invariants de classe
//
// Description: Cette classe ne devrait jamais exister si le
//              compte de références est inférieur à zéro.  Donc l'invariant est
//              que le compte de références est toujours plus grand ou
//              égal à zéro.  Le compte de zéro est permis lors de l'événement
//              transitoire entre le décrément du compte et le delete de
//              cette classe.
//
// Entrée:
//    ConstCarP conditionP
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
template <typename T>
inline void TPCompteReference<T>::invariant(ConstCarP conditionP) const
{
   INVARIANT(compteReference >= 0, conditionP);
}
#else
template <typename T>
inline void TPCompteReference<T>::invariant(ConstCarP) const
{
}
#endif   // MODE_DEBUG

#include "ercptref.hpp"

#endif // TPCPTREF_H_DEJA_INCLU



