//*****************************************************************************
// $Id$
// $Date$
//*****************************************************************************
//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//*****************************************************************************
// Fichier:  erexcept.cpp
// Classe :  ERExceptionContrat et héritiers
//*****************************************************************************
// 15-11-1995  Yves Roy           Version initiale
// 03-12-1995  Yves Roy           Changement du nom de fichier de "ercontra.h" à "erexcept.h"
// 05-12-1995  Yves Roy           Transformation de la hiérarchie ERExceptionContrat:
//                                La classe de base est devenue purement abstraite et
//                                chacune des classes héritières redéfinissent la méthode
//                                lanceException(Booleen) pour pouvoir faire un throw
//                                sur le bon type.
// 27-10-1997  Yves Secretan      Compilation conditionnelle dans arreteProgramme pour
//                                limiter les dépendances
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 05-10-1998  Yves Secretan      Corrige l'utilisation des ostrstream dans arreteProgramme()
// 28-10-1998  Yves Secretan      #include "ererreur.h"
// 06-05-1999  Yves Secretan      Supporte les iostream du Standard
// 27-05-2003  Dominique Richard  Port multi-compilateur
//*****************************************************************************
#include "erexcept.h"

#include <sstream>
#include <string>

#ifdef MODE_TEXTE
#  include <iostream>
#else 
#  include <windows.h>
#endif

#ifdef PILE_APPEL
#  include "oustack.h"
#else
#  ifndef INRS_SANS_NOUVEAUX_ENTETES
#    include <cstdlib>
#  else
#    include <stdlib.h>
#  endif   //   INRS_SANS_NOUVEAUX_ENTETES
#endif   // PILE_APPEL

//*****************************************************************************
// Sommaire: Constructeur de la classe de base ERExceptionContrat
//
// Description:
//    Le constructeur public <code>ERExceptionContrat(...)</code>
//    initialise l'objet à partir des valeurs passées en argument.
//
// Entrée:
//    ConstCarP fichP   : Fichier source dans lequel a eu lieu l'erreur
//    EntierN   ligne   : Ligne à laquelle a eu lieu l'erreur
//    ConstCarP msgP    : Message décrivant l'erreur: Exception de precondition...
//    ConstCarP exprP   : Test logique qui a échoué
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionContrat::ERExceptionContrat(const std::string& fich, Entier prmLigne,
                                       const std::string& expr, const std::string& msg)
: message(msg), expression(expr), fichier(fich), ligne(prmLigne)
{
}

//*****************************************************************************
// Sommaire: Destructeur de la classe de base ERExceptionContrat
//
// Description:
//    Le destructeur <code>~ERExceptionContrat()</code>
//    est appelée par les héritiers au moment de leur destruction.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionContrat::~ERExceptionContrat()
{
}

//*****************************************************************************
// Sommaire: Affiche un message d'erreur.
//
// Description:
//    Affiche un message d'erreur dans une fenêtre relative au
//    système d'exploitation si on est en mode graphique.
//
// Entrée:
//    const CLChaine& titre    : Titre du message.
//    const CLChaine& message  : Message à afficher.
//
// Sortie:
//
// Notes:  Cette méthode doit fonctionner en mode production
//
//*****************************************************************************
void ERExceptionContrat::afficheErreur(const std::string& titre, const std::string& message)
{
#ifdef MODE_DEBUG
   PRECONDITION(!titre.empty());
   PRECONDITION(!message.empty());
#endif

#ifdef MODE_TEXTE
   std::cout << "MESSAGE: " << titre << std::endl << message << std::endl;
#else

#if defined(__WIN32__)
   HWND client = NUL;
   MessageBox(client, message.c_str(),
              titre.c_str(), MB_APPLMODAL | MB_ICONERROR | MB_OK);
#elif __GNUC__
   HWND client = NUL;
   MessageBox(client, message.c_str(),
              titre.c_str(), MB_APPLMODAL | MB_ICONERROR | MB_OK);
#else
#  error Le système d'exploitation demande n'est pas supporte
#endif // #ifdef __WIN32__

#endif // #ifdef MODE_TEXTE
}

//*****************************************************************************
// Sommaire:    Méthode d'interruption du programme.
//
// Description:
//    La méthode publique <code>arreteProgramme()</code> sert à interrompre
//    le programme. En mode normal, on crée un objet d'erreur de contrat et
//    on appelle immédiatement cette méthode. En mode exception, on crée un
//    objet erreur de contrat et on lance cet objet comme exception.  Lors du
//    traitement de l'exception, on pourra éventuellement appeler cette méthode.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void ERExceptionContrat::arreteProgramme()
{
   // ---  Prépare le message
   INRS_IOS_STD::ostringstream os;
   os << "Fichier : " << fichier    << INRS_IOS_STD::endl;
   os << "Ligne   : " << ligne      << INRS_IOS_STD::endl;
   os << "Test    : " << expression << INRS_IOS_STD::endl;
   std::string msg = os.str();

   // ---  Affiche
#ifdef MODE_TEXTE
   INRS_IOS_STD::cout << message << INRS_IOS_STD::endl << msg;
#else
   ERExceptionContrat::afficheErreur(message, msg);
#endif

#ifdef PILE_APPEL
   STK_afficheMessage(message.c_str());
   STK_afficheMessage(msg.c_str());
   STK_leveSignalUtilisateur();
#else
/*
   exit(1);
*/
   lanceException(true);
#endif
}

//*****************************************************************************
// Sommaire: Constructeur de la classe ERExceptionAssertion
//
// Description:
//    Le constructeur public <code>ERExceptionAssertion(...)</code> initialise
//    sa classe de base ERExceptionContrat. On n'a pas d'attribut local. Cette
//    classe est intéressante pour son TYPE lors du traitement des exceptions.
//
// Entrée:
//    const char* fichP   : Fichier source dans lequel a eu lieu l'erreur
//    EntierN ligne       : Ligne à laquelle a eu lieu l'erreur
//    const char* exprP   : Test logique qui a échoué
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionAssertion::ERExceptionAssertion(const std::string& fich, Entier prmLigne,
                                           const std::string& expr)
: ERExceptionContrat(fich, prmLigne, expr, "ERREUR D'ASSERTION")
{
}

//*****************************************************************************
// Sommaire: Destructeur de la classe de base ERExceptionAssertion
//
// Description:
//    Le destructeur <code>~ERExceptionAssertion()</code>
//    est le destructeur de la classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionAssertion::~ERExceptionAssertion()
{
}

//*****************************************************************************
// Sommaire:    Méthode de lancement de l'exception.
//
// Description:
//    La méthode publique <code>lanceException(...)</code> sert à lancer une
//    exception. Si le test logique est à VRAI, on lance l'exception. Donc, si
//    l'on utilise cette méthode dans le cadre des préconditions, postconditions...,
//    il faut nier le test à l'appel de la méthode. L'appel de la méthode est
//    virtuel pour permettre de lancer le bon type d'exception.
//
// Entrée:      Booleen erreur: A VRAI s'il y a une erreur.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void ERExceptionAssertion::lanceException(Booleen erreur)
{
   if (erreur)
   {
      throw (*this);
   }
}

//*****************************************************************************
// Sommaire: Constructeur de la classe ERExceptionPrecondition
//
// Description:
//    Le constructeur public <code>ERExceptionPrecondition(...)</code> initialise
//    sa classe de base ERExceptionContrat. On n'a pas d'attribut local. Cette
//    classe est intéressante pour son TYPE lors du traitement des exceptions.
//    La classe représente le erreur de précondition dans la théorie du contrat.
//
// Entrée:
//    ConstCarP fichP   : Fichier source dans lequel a eu lieu l'erreur
//    EntierN   ligne   : Ligne à laquelle a eu lieu l'erreur
//    ConstCarP exprP   : Test logique qui a échoué
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionPrecondition::ERExceptionPrecondition(const std::string& fich, Entier prmLigne,
                                                 const std::string& expr)
: ERExceptionContrat(fich, prmLigne, expr, "ERREUR DE PRECONDITION")
{
}

//*****************************************************************************
// Sommaire: Destructeur de la classe de base ERExceptionPrecondition
//
// Description:
//    Le destructeur <code>~ERExceptionPrecondition()</code>
//    est le destructeur de la classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionPrecondition::~ERExceptionPrecondition()
{
}

//*****************************************************************************
// Sommaire:    Méthode de lancement de l'exception.
//
// Description:
//    La méthode publique <code>lanceException(...)</code> sert à lancer une
//    exception. Si le test logique est à VRAI, on lance l'exception. Donc, si
//    l'on utilise cette méthode dans le cadre des préconditions, postconditions...,
//    il faut nier le test à l'appel de la méthode. L'appel de la méthode est
//    virtuel pour permettre de lancer le bon type d'exception.
//
// Entrée:      Booleen erreur: A VRAI s'il y a une erreur.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void ERExceptionPrecondition::lanceException(Booleen erreur)
{
   if (erreur)
   {
      throw (*this);
   }
}

//*****************************************************************************
// Sommaire: Constructeur de la classe ERExceptionPostcondition
//
// Description:
//    Le constructeur public <code>ERExceptionPostcondition(...)</code>
//    initialise sa classe de base ERExceptionContrat.  On n'a pas d'attribut
//    local. Cette classe est intéressante pour son TYPE lors du traitement des
//    exceptions. La classe représente des erreurs de postcondition dans la
//    théorie du contrat.
//
// Entrée:
//    ConstCarP fichP   : Fichier source dans lequel a eu lieu l'erreur
//    EntierN   ligne   : Ligne à laquelle a eu lieu l'erreur
//    ConstCarP exprP   : Test logique qui a échoué
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionPostcondition::ERExceptionPostcondition(const std::string& fich, Entier prmLigne,
                                                   const std::string& expr)
: ERExceptionContrat(fich, prmLigne, expr, "ERREUR DE POSTCONDITION")
{
}

//*****************************************************************************
// Sommaire: Destructeur de la classe de base ERExceptionPostcondition
//
// Description:
//    Le destructeur <code>~ERExceptionPostcondition()</code>
//    est le destructeur de la classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionPostcondition::~ERExceptionPostcondition()
{
}

//*****************************************************************************
// Sommaire:    Méthode de lancement de l'exception.
//
// Description:
//    La méthode publique <code>lanceException(...)</code> sert à lancer une
//    exception. Si le test logique est à VRAI, on lance l'exception. Donc, si
//    l'on utilise cette méthode dans le cadre des préconditions, postconditions...,
//    il faut nier le test à l'appel de la méthode. L'appel de la méthode est
//    virtuel pour permettre de lancer le bon type d'exception.
//
// Entrée:      Booleen erreur: A VRAI s'il y a une erreur.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void ERExceptionPostcondition::lanceException(Booleen erreur)
{
   if (erreur)
   {
      throw (*this);
   }
}

//*****************************************************************************
// Sommaire: Constructeur de la classe ERExceptionInvariant
//
// Description:
//    Le constructeur public <code>ERExceptionInvariant(...)</code> initialise
//    sa classe de base ERExceptionContrat. On n'a pas d'attribut local. Cette
//    classe est intéressante pour son TYPE lors du traitement des exceptions.
//    La classe représente des erreurs d'invariant dans la théorie du contrat.
//
// Entrée:
//    ConstCarP fichP   : Fichier source dans lequel a eu lieu l'erreur
//    EntierN   ligne   : Ligne à laquelle a eu lieu l'erreur
//    ConstCarP exprP   : Test logique qui a échoué
//    ConstCarP condP   : Condition: PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//    La condition n'est pas tenu en compte pour l'instant.
//
//*****************************************************************************
ERExceptionInvariant::ERExceptionInvariant(const std::string& fich, Entier prmLigne,
                                           const std::string& expr, const std::string& /*condP*/)
: ERExceptionContrat(fich, prmLigne, expr, "ERREUR D'INVARIANT")
{
}

//*****************************************************************************
// Sommaire: Destructeur de la classe de base ERExceptionInvariant
//
// Description:
//    Le destructeur <code>~ERExceptionInvariant()</code>
//    est le destructeur de la classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERExceptionInvariant::~ERExceptionInvariant()
{
}

//*****************************************************************************
// Sommaire:    Méthode de lancement de l'exception.
//
// Description:
//    La méthode publique <code>lanceException(...)</code> sert à lancer une
//    exception. Si le test logique est à VRAI, on lance l'exception. Donc, si
//    l'on utilise cette méthode dans le cadre des préconditions, postconditions...,
//    il faut nier le test à l'appel de la méthode. L'appel de la méthode est
//    virtuel pour permettre de lancer le bon type d'exception.
//
// Entrée:
//    Booleen erreur: A VRAI s'il y a une erreur.
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void ERExceptionInvariant::lanceException(Booleen erreur)
{
   if (erreur)
   {
      throw (*this);
   }
}




