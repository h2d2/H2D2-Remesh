//************************************************************************
// --- Copyright (c) INRS 2000-2016
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// $Id$
// Classe:  RemailleurH2D2
//************************************************************************
#include "RemailleurH2D2.h"

#include "EFMaillage3D.h"
#include "GOPolygone.h"
#include "SRRegion_GOCoordonneesXYZ.h"
#include "SRChampEFEllipseErreur.h"

#include "GGMailleurDelaunay.h"
#include "GGDelaunayArete.h"
#include "GGDelaunaySommet.h"
#include "GGDelaunaySurface.h"
#include "AlgoTransfertMaillage.h"
#include "GGDelaunayOperation.h"
#include "GGDelaunayScenario.h"
#include "GGDelaunayAvancement.h"

#include <map>
#include <sstream>
#include <string>

//***************************************************************************
// Sommaire: Constructeur par défaut de la classe RemailleurH2D2
//
// Description:
//    Constructeur par défaut <code>RemailleurH2D2(...)</code>. Appelle le
//    constructeur de la classe de base
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//***************************************************************************
RemailleurH2D2::RemailleurH2D2 ()
   : m_mailP(NUL)
   , m_champP(NUL)
   , m_regnP(NUL)
   , m_mllrP(NUL)
   , m_algoP(NUL)
   , m_info(NUL)
   , m_cb(NUL)
{

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif // ifdef MODE_DEBUG
}

//***************************************************************************
// Sommaire: Destructeur par défaut de la classe RemailleurH2D2
//
// Description:
//    Destructeur par défaut <code>RemailleurH2D2(...)</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//***************************************************************************
RemailleurH2D2::~RemailleurH2D2()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif // ifdef MODE_DEBUG

   if (m_algoP  != NUL) delete m_algoP;  m_algoP  = NUL;
   if (m_mllrP  != NUL) delete m_mllrP;  m_mllrP  = NUL;
   if (m_champP != NUL) delete m_champP; m_champP = NUL;
   if (m_regnP  != NUL) delete m_regnP;  m_regnP  = NUL;
   if (m_mailP  != NUL) delete m_mailP;  m_mailP  = NUL;
}

//***************************************************************************
// Sommaire:
//
// Description:
//    La méthode publique <code>adapte</code> va faire une passe d'adaptation
//    en appliquant toutes les actions demandées.
//
// Entrée:
//    int      ninfo    Nombre d'infos (==10)
//    int      ntgt     Nombre de noeuds cible
//    double   htgt     Taille de maille cible (dans la métrique riemannienne)
//    int      lmin     Niveau min de déraffinement ( -1, -2 ...)
//    int      lmax     Niveau max de raffinement ( 1, 2, ...)
//    int      n1       Taille d'une action (==4)
//    int      n2       Nombre d'actions
//    double*  actions  Table (n2, n1) des actions (en format C):
//                         (.,1) Type d'opérations
//                         (.,2) Nombre de répétition de l'action
//                         (.,3) Paramètre 1 de l'action
//                         (.,4) Paramètre 2 de l'action
//
// Sortie:
//    double*  info     Table de dimension ninfo des infos d'adaptation
//    int               Code de retour: 0 succès, -1 échec
//
// Notes:
//***************************************************************************
int RemailleurH2D2::adapte(fint_t ninfo, double* info, TTInfoCb cb,
                           fint_t ntgt, double htgt,
                           fint_t lmin, fint_t lmax,
                           fint_t n1, fint_t n2, double* actions)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_mailP   != NUL);
   PRECONDITION(m_champP != NUL);
   PRECONDITION(ninfo    == 10);
   PRECONDITION(n1       == 4);
   PRECONDITION(n2       >  0);
   PRECONDITION(actions != NUL);
#endif // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Détruis l'algo de transfert, rendant asgLimite invalide
   if (m_algoP  != NUL) delete m_algoP;
   m_algoP = NUL;

   // ---  Crée le scénario
   GGDelaunayScenario scenario;
   for (fint_t i = 0; i < n2; ++i)
   {
      Operation iop = static_cast<Operation>( static_cast<int>(*(actions+0) + 0.5) );
      int       cnt = static_cast<int>(*(actions+1) + 0.5);
      double    vl1 = *(actions+2);
      double    vl2 = *(actions+3);
      actions += n1;

      GGDelaunayOperation::Type opr = GGDelaunayOperation::INDEFINI;
      switch (iop)
      {
         case LISSE_METRIQUES:          opr = GGDelaunayOperation::LISSE_METRIQUES;          break;
         case RESCALE_METRIQUES:        opr = GGDelaunayOperation::RESCALE_METRIQUES;        break;
         case MAILLE_PEAU:              opr = GGDelaunayOperation::MAILLE_PEAU;              break;
         case RETOURNE_ARETES:          opr = GGDelaunayOperation::RETOURNE_ARETES;          break;
         case SOIGNE_PEAU:              opr = GGDelaunayOperation::SOIGNE_PEAU;              break;
         case SOIGNE_PONT:              opr = GGDelaunayOperation::SOIGNE_PONT;              break;
         case DERAFFINE_PAR_SOMMETS:    opr = GGDelaunayOperation::DERAFFINE_PAR_SOMMETS;    break;
         case RAFFINE_PAR_ARETES:       opr = GGDelaunayOperation::RAFFINE_PAR_ARETES;       break;
         case RAFFINE_PAR_SURFACES:     opr = GGDelaunayOperation::RAFFINE_PAR_SURFACES;     break;
         case REGULARISE_PAR_ARETES:    opr = GGDelaunayOperation::REGULARISE_PAR_ARETES;    break;
         case REGULARISE_PAR_SURFACES:  opr = GGDelaunayOperation::REGULARISE_PAR_SURFACES;  break;
         case REGULARISE_BARYCENTRIQUE: opr = GGDelaunayOperation::REGULARISE_BARYCENTRIQUE; break;
         case REGULARISE_ETOILES:       opr = GGDelaunayOperation::REGULARISE_ETOILES;       break;
         default:
            ASSERTION(iop >= MAILLE_PEAU && iop <= REGULARISE_ETOILES);
      }

      scenario.ajouteOperation(GGDelaunayOperation(opr, vl1, vl2), cnt);
   }

   // ---  Execute le scénario
   m_info = info;
   m_cb   = cb;
   Entier nnt_cible = (ntgt > std::numeric_limits<Entier>::max()) ? -1 : static_cast<Entier>(ntgt);
   Entier rMin = std::min(0, lmin);
   Entier rMax = std::max(0, lmax);
   GGDelaunayAvancementMth<RemailleurH2D2> avancement(this, &RemailleurH2D2::cbInfo);
   msg    = m_mllrP->appliqueScenario(avancement, scenario, nnt_cible, htgt, rMin, rMax);
   m_cb   = NUL;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif // ifdef MODE_DEBUG
   m_msg = msg;
   return (m_msg) ? 0 : -1;
}

//***************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//***************************************************************************
void RemailleurH2D2::cbInfo(GGDelaunayAvancement& avancement)
{

   fint_t iop = INDEFINI;
   switch (avancement.operation)
   {
      case GGDelaunayOperation::INDEFINI:                iop = INDEFINI;                break;
      case GGDelaunayOperation::ETAT_INITIAL:            iop = ETAT_INITIAL;            break;
      case GGDelaunayOperation::ETAT_FINAL:              iop = ETAT_FINAL;              break;
      case GGDelaunayOperation::MAILLE_PEAU:             iop = MAILLE_PEAU;             break;
      case GGDelaunayOperation::LISSE_METRIQUES:         iop = LISSE_METRIQUES;         break;
      case GGDelaunayOperation::RESCALE_METRIQUES:       iop = RESCALE_METRIQUES;       break;
      case GGDelaunayOperation::RETOURNE_ARETES:         iop = RETOURNE_ARETES;         break;
      case GGDelaunayOperation::SOIGNE_PEAU:             iop = SOIGNE_PEAU;             break;
      case GGDelaunayOperation::SOIGNE_PONT:             iop = SOIGNE_PONT;             break;
      case GGDelaunayOperation::DERAFFINE_PAR_SOMMETS:   iop = DERAFFINE_PAR_SOMMETS;   break;
//      DERAFFINE_PAR_SURFACES,          m_info[0] =
      case GGDelaunayOperation::RAFFINE_PAR_ARETES:      iop = RAFFINE_PAR_ARETES;      break;
      case GGDelaunayOperation::RAFFINE_PAR_SURFACES:    iop = RAFFINE_PAR_SURFACES;    break;
      case GGDelaunayOperation::REGULARISE_PAR_ARETES:   iop = REGULARISE_PAR_ARETES;   break;
      case GGDelaunayOperation::REGULARISE_PAR_SURFACES: iop = REGULARISE_PAR_SURFACES; break;
      case GGDelaunayOperation::REGULARISE_BARYCENTRIQUE:iop = REGULARISE_BARYCENTRIQUE;break;
      case GGDelaunayOperation::REGULARISE_ETOILES:      iop = REGULARISE_ETOILES;      break;
      default:
         ASSERTION(avancement.operation >= INDEFINI && avancement.operation <= REGULARISE_ETOILES);
   }

   m_info[0] = static_cast<double>(iop);
   m_info[1] = avancement.nbrSommets;
   m_info[2] = avancement.nbrSurfaces;
   m_info[3] = avancement.qualite;
   m_info[4] = avancement.sommetCmin;
   m_info[5] = avancement.sommetCmed;
   m_info[6] = avancement.sommetCmax;
   m_info[7] = avancement.areteCmin;
   m_info[8] = avancement.areteCmed;
   m_info[9] = avancement.areteCmax;

   if (m_cb) m_cb(&iop, m_info);
}

//***************************************************************************
// Sommaire: Assigne le champ d'erreur
//
// Description:
//    La méthode publique <code>asgChampErr</code> assigne le champ d'erreur
//    sous forme d'ellipses. Elle permet également une mise à l'échelle et
//    l'application de bornes.
//    La table ellps contient des suites de grand-axe, petit-axe et
//    inclinaisons pour chaque noeud.
//
// Entrée:
//    int nval       Dimensions de ellps
//    int nnt
//    double* ellps  Table des ellipses d'erreurs
//    double  hscl   Facteur d'échelle
//    double  hmin   Taille de maille min
//    double  hmax   Taille de maille max
//
// Sortie:
//
// Notes:
//    En mode release, il n'y a pas de précond. On se prémuni donc contre
//    des bornes irréaliste.
//***************************************************************************
int RemailleurH2D2::asgChampErr(fint_t nval, fint_t nnt,  double* ellps,
                                double hscl, double hmin, double hmax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_mailP != NULL);
   PRECONDITION(m_regnP != NULL);
   PRECONDITION(ellps!= NUL);
   PRECONDITION(hscl >= 1.0e-9);
   PRECONDITION(hscl <= 1.0e+9);
   PRECONDITION(hmin >= 1.0e-9);
   PRECONDITION(hmax <= 1.0e+9);
   PRECONDITION(hmax >= hmin);
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Contrôles
   Booleen enErreur = FAUX;
   if (nval != 3) enErreur = VRAI;
   if (nnt   < 0 || static_cast<EntierN>(nnt) != m_mailP->reqNbrNoeud()) enErreur = VRAI;
   if (enErreur) msg = ERMsg(ERMsg::ERREUR, "ERR_CHAMP_ERREUR_INVALIDE");

   // ---  Bornes les valeurs
   if (msg)
   {
      hscl = std::max(hscl, 1.0e-9);
      hscl = std::min(hscl, 1.0e+9);
      hmin = std::max(hmin, 1.0e-9);
      hmax = std::min(hmax, 1.0e+9);
   }

   // --- Construis le champ
   TCChampErr* champP = NUL;
   if (msg)
   {
      typedef TCChampErr::TCDonnee TTDonnee;
      std::vector<TTDonnee> e;
      e.reserve( static_cast<std::vector<TTDonnee>::size_type>(nnt) );
      for (fint_t i = 0; i < nnt; ++i)
      {
         TTDonnee d(*(ellps+0), *(ellps+1), *(ellps+2));
         d.scale (hscl);
         d.limite(hmin, hmax);
         e.push_back(d);
         ellps +=3;
      }

      champP = new TCChampErr(m_mailP, m_regnP);
      champP->asgValeurs(e.begin(), e.end());
      champP->asgEpsilon(1.0e-7);
   }

   // ---   Transfert la métrique
   if (msg)
   {
      msg = m_mllrP->asgCarteMetrique(*champP);
   }

   // ---  Conserve le champ
   if (m_champP != NUL) delete m_champP;
   m_champP = NUL;
   if (msg) m_champP = champP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   m_msg = msg;
   return (m_msg) ? 0 : -1;
}

//***************************************************************************
// Sommaire: Ajoute le champ d'erreur
//
// Description:
//    La méthode publique <code>ajtChampErr</code> ajoute un champ d'erreur
//    sous forme d'ellipses au champ d'erreur de l'objet. L'opération utilisée
//    est l'intersection des ellipses. La méthode permet également une mise à
//    l'échelle et l'application de bornes.
//    La table ellps contient des suites de grand-axe, petit-axe et
//    inclinaisons pour chaque noeud.
//
// Entrée:
//    int nval       Dimensions de ellps
//    int nnt
//    double* ellps  Table des ellipses d'erreurs
//    double  hscl   Facteur d'échelle
//    double  hmin   Taille de maille min
//    double  hmax   Taille de maille max
//
// Sortie:
//
// Notes:
//    En mode release, il n'y a pas de précond. On se prémuni donc contre
//    des bornes irréaliste.
//***************************************************************************
int RemailleurH2D2::ajtChampErr(fint_t nval, fint_t nnt,  double* ellps,
                                double hscl, double hmin, double hmax)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_mailP  != NULL);
   PRECONDITION(m_regnP  != NULL);
   PRECONDITION(m_champP!= NULL);
   PRECONDITION(ellps != NUL);
   PRECONDITION(hscl >= 1.0e-9);
   PRECONDITION(hscl <= 1.0e+9);
   PRECONDITION(hmin >= 1.0e-9);
   PRECONDITION(hmax <= 1.0e+9);
   PRECONDITION(hmax >= hmin);
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Contrôles
   Booleen enErreur = FAUX;
   if (nval != 3) enErreur = VRAI;
   if (nnt   < 0 || static_cast<EntierN>(nnt) != m_mailP->reqNbrNoeud()) enErreur = VRAI;
   if (enErreur) msg = ERMsg(ERMsg::ERREUR, "ERR_CHAMP_ERREUR_INVALIDE");

   // ---  Bornes les valeurs
   if (msg)
   {
      hscl = std::max(hscl, 1.0e-9);
      hscl = std::min(hscl, 1.0e+9);
      hmin = std::max(hmin, 1.0e-9);
      hmax = std::min(hmax, 1.0e+9);
   }

   // ---  Construis le champ
   if (msg)
   {
      typedef TCChampErr::TCDonnee TTDonnee;
      for (EntierN i = 0; i < nnt; ++i)
      {
         TTDonnee d(*(ellps+0), *(ellps+1), *(ellps+2));
         d.scale (hscl);
         d.limite(hmin, hmax);
         (*m_champP)[i].intersecte(d);
         ellps +=3;
      }
   }

   // ---   Transfert la métrique
   if (msg)
   {
      msg = m_mllrP->asgCarteMetrique(*m_champP);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   m_msg = msg;
   return (m_msg) ? 0 : -1;
}

//***************************************************************************
// Sommaire: Assigne le maillage initial.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//***************************************************************************
int RemailleurH2D2::asgMaillage(fint_t ndim, fint_t nnt, double* vcorg,
                                TypeElement itypv, fint_t nnelv, fint_t ncelv, fint_t neltv, fint_t* kngv,
                                bool debloqueAnglesPlats)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(nnelv <= ncelv);
   PRECONDITION(vcorg != NUL);
   PRECONDITION(kngv != NUL);
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Contrôles
   if (ndim != 2)
   {
      if (msg) msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_INVALIDE");
      msg.ajoute("MSG_NDIM_INVALIDE");
   }
   if (nnt   < 0 || nnt   > std::numeric_limits<EntierN>::max())
   {
      if (msg) msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_INVALIDE");
      msg.ajoute("MSG_NNT_INVALIDE");
   }
   if (neltv < 0 || neltv > std::numeric_limits<EntierN>::max())
   {
      if (msg) msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_INVALIDE");
      msg.ajoute("MSG_NELTV_INVALIDE");
   }
   if (nnelv != 3 && nnelv != 6)
   {
      if (msg) msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_INVALIDE");
      msg.ajoute("MSG_NNELV_INVALIDE");
   }

   // ---  Dimensionne les listes
   TCMaillage* maiP = NUL;
   if (msg)
   {
      maiP = new TCMaillage();
      maiP->dimListeNoeuds( static_cast<EntierN>(nnt) );
      maiP->dimListeElem  ( static_cast<EntierN>(neltv) );
   }

   // ---  Offset pour centrer les données
   if (msg)
   {
      TCCoord pMin =  TCCoord::GRAND;
      TCCoord pMax = -TCCoord::GRAND;
      double* vc = vcorg;
      for (fint_t i = 0; i < nnt; ++i)
      {
         TCCoord p(*(vc+0), *(vc+1));
         p.minGlobal(pMin);
         p.maxGlobal(pMax);
         vc += ndim;
      }
      m_off = 0.5 * (pMin + pMax);
   }

   // ---  Crée les noeuds
   TCCoord pMin =  TCCoord::GRAND;
   TCCoord pMax = -TCCoord::GRAND;
   if (msg)
   {
      double* vc = vcorg;
      for (EntierN i = 0; i < nnt; ++i)
      {
         TCCoord p(*(vc+0), *(vc+1));
         p -= m_off;
         p.minGlobal(pMin);
         p.maxGlobal(pMax);
         maiP->asgNoeud(i, p);
         vc += ndim;
      }
      maiP->asgMinMax(pMin, pMax);
   }

   // ---  Traduis le type d'élément
   EFMaillage3D::TCElement::Type typEle = EFMaillage3D::TCElement::TYPE_NIL;
   if (msg)
   {
      switch (itypv)
      {
         case ELEM_T3:  typEle = EFMaillage3D::TCElement::TYPE_T3;  break;
         case ELEM_T6:  typEle = EFMaillage3D::TCElement::TYPE_T6;  break;
         case ELEM_T6L: typEle = EFMaillage3D::TCElement::TYPE_T6L; break;
         default: ASSERTION(itypv == ELEM_T3);
      }
   }

   // ---  Crée les éléments
   if (msg)
   {
      EntierN nnel = static_cast<EntierN>(nnelv);
      EntierN kne[6];
      for (EntierN i = 0; i < neltv; ++i)
      {
         fint_t* k = kngv + i*ncelv;
         for (EntierN j = 0; j < nnel; ++j) kne[j] = static_cast<EntierN>(*k++);
         maiP->asgElement(typEle, i, nnel, kne);
      }
   }

   // --- Crée la région
   TCRegion* regionP = NULL;
   if (msg)
   {
      std::vector<TCCoord> ptsPolygone;
      ptsPolygone.push_back( TCCoord(pMin.x(), pMin.y()) );
      ptsPolygone.push_back( TCCoord(pMax.x(), pMin.y()) );
      ptsPolygone.push_back( TCCoord(pMax.x(), pMax.y()) );
      ptsPolygone.push_back( TCCoord(pMin.x(), pMax.y()) );

      GOPolygone contour;
      contour.ajoutePolyligne(ptsPolygone.begin(), ptsPolygone.end());

      regionP = new TCRegion();
      regionP->ajoutePolygone(contour);
   }

   // ---  (Re)-construis le mailleur
   if (m_mllrP  != NUL) delete m_mllrP;
   m_mllrP = NUL;
   if (msg) m_mllrP = new TCMailleur();

   // ---   (Re)-construis l'algo de transfert
   if (m_algoP  != NUL) delete m_algoP;
   m_algoP = NUL;
   if (msg) m_algoP = new TCAlgo();

   // ---  Transfert le maillage au mailleur
   if (msg)
   {
      msg = (*m_algoP)(*m_mllrP, *maiP, debloqueAnglesPlats);
   }

   // ---  Conserve le maillage et la région
   if (m_mailP != NUL) delete m_mailP;
   if (m_regnP != NUL) delete m_regnP;
   m_mailP = maiP;
   m_regnP = regionP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   m_msg = msg;
   return (m_msg) ? 0 : -1;
}

//***************************************************************************
// Sommaire: Assigne un maillage de limite.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//***************************************************************************
int RemailleurH2D2::asgLimite(TypeElement ityp, fint_t nnels, fint_t ncels, fint_t nelts, fint_t* kngs,
                              fint_t info, fint_t nclele, fint_t* kclele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_mailP != NUL);
   PRECONDITION(m_algoP != NUL);
   PRECONDITION(kngs != NUL);
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Contrôles
   if (nelts < 0 || nelts > std::numeric_limits<EntierN>::max())
   {
      if (msg) msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_LIMITE_INVALIDE");
      msg.ajoute("MSG_NELTS_INVALIDE");
   }
   if (nnels != 2 && nnels != 3)
   {
      if (msg) msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_LIMITE_INVALIDE");
      msg.ajoute("MSG_NNELS_INVALIDE");
   }
   if (nclele <= 0)
   {
      if (msg) msg = ERMsg(ERMsg::ERREUR, "ERR_MAILLAGE_LIMITE_INVALIDE");
      msg.ajoute("MSG_LIMITE_SANS_ELEMENTS");
   }

   // ---  Crée le maillage de limite
   TCMaillage* maiP = NUL;
   if (msg)
   {
      maiP = new TCMaillage(TCMaillage::PEAU);
      maiP->asgMaillageParent(m_mailP);
   }

   // ---  Dimensionne les listes
   if (msg)
   {
      maiP->dimListeElem( static_cast<EntierN>(nclele) );
   }

   // ---  Traduis le type d'élément
   EFMaillage3D::TCElement::Type typEle = EFMaillage3D::TCElement::TYPE_NIL;
   if (msg)
   {
      switch (ityp)
      {
         case ELEM_L2:  typEle = EFMaillage3D::TCElement::TYPE_L2;  break;
         case ELEM_L3:  typEle = EFMaillage3D::TCElement::TYPE_L3;  break;
         case ELEM_L3L: typEle = EFMaillage3D::TCElement::TYPE_L3L; break;
         default: ASSERTION(ityp == ELEM_L2);
      }
   }

   // ---  Crée les noeuds
   std::map<EntierN, EntierN> noeuds;    // <noeud_global, noeud_peau>
   if (msg)
   {
      EntierN noPeau = 0;
      for (fint_t ie = 0; ie < nclele; ++ie)
      {
         fint_t  ies = kclele[ie];
         fint_t* k = kngs + ies*ncels;
         for (fint_t j = 0; j < nnels; ++j)
         {
            const EntierN noGlob = static_cast<EntierN>(*k++);
            if (noeuds.find(noGlob) == noeuds.end())
            {
               TCMaillage::TCNoeudP nP = const_cast<TCMaillage::TCNoeudP>( m_mailP->reqNoeud(noGlob) );
               maiP->asgNoeud(nP);
               noeuds[noGlob] = noPeau++;
            }
         }
      }
   }

   // ---  Crée les éléments
   if (msg)
   {
      EntierN nnel = static_cast<EntierN>(nnels);
      EntierN kne[3];
      for (EntierN ie = 0; ie < nclele; ++ie)
      {
         fint_t  ies = kclele[ie];
         fint_t* k = kngs + ies*ncels;
         for (EntierN j = 0; j < nnel; ++j)
         {
            EntierN noGlob = static_cast<EntierN>(*k++);
            kne[j] = noeuds[noGlob];
         }
         maiP->asgElement(typEle, ie, nnel, kne);
      }
   }

   // ---  Transfert la frontière au mailleur
   if (msg)
   {
      intptr_t i = static_cast<intptr_t>(info);
      msg = (*m_algoP)(*m_mllrP, *maiP, reinterpret_cast<void*>(i));
   }

   // ---  Détruis le maillage de la limite
   if (maiP != NUL) delete maiP;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   m_msg = msg;
   return (m_msg) ? 0 : -1;
}

//************************************************************************
// Sommaire:     Génère le maillage.
//
// Description:
//    La méthode publique <code>genMaillage(...)</code> génère le maillage
//    résultant de l'adaptation. Les tables vcorg et kng doivent être
//    dimensionnées, les dimensions étant accessibles avec reqNbrNoeuds
//    et reqNbrElements.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
int RemailleurH2D2::genMaillage(TypeElement ityp,
                                fint_t ndim, fint_t nnt,  double* vcorg,
                                fint_t nnel, fint_t nelt, fint_t* kng) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ityp >= ELEM_T3 && ityp <= ELEM_T6L);
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   switch (ityp)
   {
      case ELEM_T3:  msg = genMaillageT3(ndim, nnt, vcorg, nnel, nelt, kng); break;
      case ELEM_T6:  msg = genMaillageT6(ndim, nnt, vcorg, nnel, nelt, kng); break;
      case ELEM_T6L: msg = genMaillageT6(ndim, nnt, vcorg, nnel, nelt, kng); break;
      default:
         ASSERTION(ityp >= ELEM_T3 && ityp <= ELEM_T6L);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   m_msg = msg;
   return (m_msg) ? 0 : -1;
}

//************************************************************************
// Sommaire:   Retourne VRAI si la numérotation originale est valide.
//
// Description:
//    La méthode privée <code>estNumNoeudValideT3(...)</code> retourne VRAI
//    si la numérotation des noeuds originale est toujours valide, donc
//    s'il n'y a pas eu d'ajout ou de retrait de noeuds/arête/élément.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
Booleen RemailleurH2D2::estNumNoeudValideT3 () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Booleen valide = VRAI;

   const fint_t nbNoeuds = reqNbrNoeuds(ELEM_T3);
   std::vector<Booleen> noeudPresent(nbNoeuds, FAUX);

   // ---  Itère sur les noeuds
   GGMailleurDelaunay::TCContainerSommet::const_iterator somI = m_mllrP->reqSommetDebut();
   GGMailleurDelaunay::TCContainerSommet::const_iterator esoI = m_mllrP->reqSommetFin();
   while (valide && somI != esoI)
   {
      GGDelaunayInfoSommetP infoP = dynamic_cast<GGDelaunayInfoSommetP>((*somI)->reqInfoClient());
      const int inod = (infoP == NULL) ? -1 : infoP->reqNoNoeud();
      if (inod < 0 || inod > nbNoeuds || noeudPresent[inod])
         valide = FAUX;
      else
         noeudPresent[inod] = VRAI;
      ++somI;
   }

   if (valide)
      valide = (std::find(noeudPresent.begin(), noeudPresent.end(), FAUX) == noeudPresent.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return valide;
}

//************************************************************************
// Sommaire:   Retourne VRAI si la numérotation originale est valide.
//
// Description:
//    La méthode privée <code>estNumNoeudValideT6(...)</code> retourne VRAI
//    si la numérotation des noeuds originale est toujours valide, donc
//    s'il n'y a pas eu d'ajout ou de retrait de noeuds/arête.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
Booleen RemailleurH2D2::estNumNoeudValideT6 () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Booleen valide = VRAI;

   const fint_t nbNoeuds = reqNbrNoeuds(ELEM_T6);
   std::vector<Booleen> noeudPresent(nbNoeuds, FAUX);

   // ---  Itère sur les noeuds
   GGMailleurDelaunay::TCContainerSommet::const_iterator somI = m_mllrP->reqSommetDebut();
   GGMailleurDelaunay::TCContainerSommet::const_iterator esoI = m_mllrP->reqSommetFin();
   while (valide && somI != esoI)
   {
      GGDelaunayInfoSommetP infoP = dynamic_cast<GGDelaunayInfoSommetP>((*somI)->reqInfoClient());
      const int inod = (infoP == NULL) ? -1 : infoP->reqNoNoeud();
      if (inod < 0 || inod > nbNoeuds || noeudPresent[inod])
         valide = FAUX;
      else
         noeudPresent[inod] = VRAI;
      ++somI;
   }

   // ---  Itère sur les arêtes
   GGMailleurDelaunay::TCContainerArete::const_iterator artI = m_mllrP->reqAreteDebut();
   GGMailleurDelaunay::TCContainerArete::const_iterator esaI = m_mllrP->reqAreteFin();
   while (valide && artI != esaI)
   {
      GGDelaunayInfoAreteP infoP = dynamic_cast<GGDelaunayInfoAreteP>((*artI)->reqInfoClient());
      const int inod = (infoP == NULL) ? -1 : infoP->reqNoNoeud();
      if (inod < 0 || inod > nbNoeuds || noeudPresent[inod])
         valide = FAUX;
      else
         noeudPresent[inod] = VRAI;
      ++artI;
   }

   if (valide)
      valide = (std::find(noeudPresent.begin(), noeudPresent.end(), FAUX) == noeudPresent.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return valide;
}

//************************************************************************
// Sommaire:   Retourne VRAI si la numérotation originale est valide.
//
// Description:
//    La méthode privée <code>estNumElemValideT3(...)</code> retourne VRAI
//    si la numérotation des éléments originale est toujours valide, donc
//    s'il n'y a pas eu d'ajout ou de retrait d'éléments.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
Booleen RemailleurH2D2::estNumElemValideT3() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Booleen valide = VRAI;

   const fint_t nbElements = reqNbrElements(ELEM_T3);
   std::vector<Booleen> elemPresent(nbElements, FAUX);

   // ---  Itère sur les éléments
   GGMailleurDelaunay::TCContainerSurface::const_iterator srfI = m_mllrP->reqSurfaceDebut();
   GGMailleurDelaunay::TCContainerSurface::const_iterator esrI = m_mllrP->reqSurfaceFin();
   while (valide && srfI != esrI)
   {
      GGDelaunayInfoSurfaceP infoP = dynamic_cast<GGDelaunayInfoSurfaceP>((*srfI)->reqInfoClient());
      const int iele = (infoP == NULL) ? -1 : infoP->reqNoElem();
      if (iele < 0 || iele > nbElements || elemPresent[iele])
         valide = FAUX;
      else
         elemPresent[iele] = VRAI;
      ++srfI;
   }

   if (valide)
      valide = (std::find(elemPresent.begin(), elemPresent.end(), FAUX) == elemPresent.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return valide;
}

//************************************************************************
// Sommaire:   Retourne VRAI si la numérotation originale est valide.
//
// Description:
//    La méthode privée <code>estNumElemValideT6(...)</code> retourne VRAI
//    si la numérotation des éléments originale est toujours valide, donc
//    s'il n'y a pas eu d'ajout ou de retrait d'éléments.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Code identique à T3 sauf pour reqNbrElements(ELEM_T6)
//************************************************************************
Booleen RemailleurH2D2::estNumElemValideT6() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   Booleen valide = VRAI;

   const fint_t nbElements = reqNbrElements(ELEM_T6);
   std::vector<Booleen> elemPresent(nbElements, FAUX);

   // ---  Itère sur les éléments
   GGMailleurDelaunay::TCContainerSurface::const_iterator srfI = m_mllrP->reqSurfaceDebut();
   GGMailleurDelaunay::TCContainerSurface::const_iterator esrI = m_mllrP->reqSurfaceFin();
   while (valide && srfI != esrI)
   {
      GGDelaunayInfoSurfaceP infoP = dynamic_cast<GGDelaunayInfoSurfaceP>((*srfI)->reqInfoClient());
      const int iele = (infoP == NULL) ? -1 : infoP->reqNoElem();
      if (iele < 0 || iele > nbElements || elemPresent[iele])
         valide = FAUX;
      else
         elemPresent[iele] = VRAI;
      ++srfI;
   }

   if (valide)
      valide = (std::find(elemPresent.begin(), elemPresent.end(), FAUX) == elemPresent.end());

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return valide;
}

//************************************************************************
// Sommaire:   Génère le champ d'erreur.
//
// Description:
//    La méthode publique <code>genChampErr(...)</code> assigne
//    à ellps les valeurs des ellipses d'erreur..
//    La table ellps doit être dimensionnée.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
int RemailleurH2D2::genChampErr(fint_t nval, fint_t nnt, double* ellps) const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_champP != NUL);
   PRECONDITION(nval == 3);
   PRECONDITION(nnt  == m_champP->reqNbrValeursNodales());
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Construis le champ
   for (auto dI = m_champP->reqVnoDebut(); dI != m_champP->reqVnoFin(); ++dI)
   {
      *(ellps + 0) = (*dI).reqGrandAxe();
      *(ellps + 1) = (*dI).reqPetitAxe();
      *(ellps + 2) = (*dI).reqInclinaison();
      ellps += 3;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   m_msg = msg;
   return (m_msg) ? 0 : -1;
}

//************************************************************************
// Sommaire:   Génère les numéros de noeuds.
//
// Description:
//    La méthode privée <code>genNumNoeudT3(...)</code> génère les
//    numéros de noeuds pour un maillage T3.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg RemailleurH2D2::genNumNoeudT3() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   EntierN ctr = 0;

   // ---  Génère les noeuds
   GGDelaunayInfoSommet infoSommet;
   GGMailleurDelaunay::TCContainerSommet::const_iterator somI = m_mllrP->reqSommetDebut();
   GGMailleurDelaunay::TCContainerSommet::const_iterator esoI = m_mllrP->reqSommetFin();
   while (somI != esoI)
   {
      if ((*somI)->reqInfoClient() == NULL) (*somI)->asgInfoClient(&infoSommet);
      dynamic_cast<GGDelaunayInfoSommetP>((*somI)->reqInfoClient())->asgNoNoeud(ctr);
      ++ctr;
      ++somI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Génère les numéros de noeuds.
//
// Description:
//    La méthode privée <code>genNumNoeudT3(...)</code> génère les
//    numéros de noeuds pour un maillage T6.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg RemailleurH2D2::genNumNoeudT6() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   EntierN ctr = 0;

   // ---  Génère les noeuds
   GGDelaunayInfoSommet infoSommet;
   GGMailleurDelaunay::TCContainerSommet::const_iterator somI = m_mllrP->reqSommetDebut();
   GGMailleurDelaunay::TCContainerSommet::const_iterator esoI = m_mllrP->reqSommetFin();
   while (somI != esoI)
   {
      if ((*somI)->reqInfoClient() == NULL) (*somI)->asgInfoClient(&infoSommet);
      dynamic_cast<GGDelaunayInfoSommetP>((*somI)->reqInfoClient())->asgNoNoeud(ctr);
      ++ctr;
      ++somI;
   }

   // ---  Génère les noeuds
   GGDelaunayInfoArete infoArete;
   GGMailleurDelaunay::TCContainerArete::const_iterator artI = m_mllrP->reqAreteDebut();
   GGMailleurDelaunay::TCContainerArete::const_iterator esaI = m_mllrP->reqAreteFin();
   while (artI != esaI)
   {
      if ((*artI)->reqInfoClient() == NULL) (*artI)->asgInfoClient(&infoArete);
      dynamic_cast<GGDelaunayInfoAreteP>((*artI)->reqInfoClient())->asgNoNoeud(ctr);
      ++ctr;
      ++artI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Génère les numéros d'éléments.
//
// Description:
//    La méthode privée <code>genNumNoeudT3(...)</code> génère les
//    numéros de noeuds pour un maillage T6.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg RemailleurH2D2::genNumElem() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   EntierN ctr = 0;

   // ---  Génère les noeuds
   GGDelaunayInfoSurface info;
   GGMailleurDelaunay::TCContainerSurface::const_iterator srfI = m_mllrP->reqSurfaceDebut();
   GGMailleurDelaunay::TCContainerSurface::const_iterator esrI = m_mllrP->reqSurfaceFin();
   while (srfI != esrI)
   {
      if ((*srfI)->reqInfoClient() == NULL) (*srfI)->asgInfoClient(&info);
      dynamic_cast<GGDelaunayInfoSurfaceP>((*srfI)->reqInfoClient())->asgNoElem(ctr);
      ++ctr;
      ++srfI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Génère le maillage avec des éléments T3.
//
// Description:
//    La méthode privée <code>genereElemT3(...)</code> assigne
//    à vcorg les coordonnées des sommets du mailleur et
//    à kng les connectivités des surfaces du mailleur sous forme
//    d'éléments T3.
//    Les tables vcorg et kng doivent être dimensionnées.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg RemailleurH2D2::genMaillageT3 (fint_t ndim, fint_t nnt,  double* vcorg,
                                     fint_t nnel, fint_t nelt, fint_t* kng) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ndim >= 2);
   PRECONDITION(nnt  == reqNbrNoeuds(ELEM_T3));
   PRECONDITION(nnel >= 3);
   PRECONDITION(nelt == reqNbrElements(ELEM_T3));
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Génère les numéros de noeuds
   if (! estNumNoeudValideT3()) msg = genNumNoeudT3();

   // ---  Génère les noeuds
   GGMailleurDelaunay::TCContainerSommet::const_iterator somI = m_mllrP->reqSommetDebut();
   GGMailleurDelaunay::TCContainerSommet::const_iterator esoI = m_mllrP->reqSommetFin();
   while (somI != esoI)
   {
      ConstGGDelaunayInfoSommetP infoP = dynamic_cast<ConstGGDelaunayInfoSommetP>((*somI)->reqInfoClient());
      const size_t ioff = infoP->reqNoNoeud() * ndim;
      const GOCoordonneesXYZ& p = (*somI)->reqPosition();
      *(vcorg+ioff+0) = p.x() + m_off.x();
      *(vcorg+ioff+1) = p.y() + m_off.y();
      ++somI;
   }

   // ---  Génère les éléments
   GGMailleurDelaunay::TCContainerSurface::const_iterator srfI = m_mllrP->reqSurfaceDebut();
   GGMailleurDelaunay::TCContainerSurface::const_iterator esrI = m_mllrP->reqSurfaceFin();
   while (srfI != esrI)
   {
      ConstGGDelaunayInfoSurfaceP infoP = dynamic_cast<ConstGGDelaunayInfoSurfaceP>((*srfI)->reqInfoClient());
      const size_t ioff = infoP->reqNoElem() * nnel;

      GGDelaunayInfoSommetP info1P = dynamic_cast<GGDelaunayInfoSommetP>((*srfI)->reqSommet0()->reqInfoClient());
      GGDelaunayInfoSommetP info2P = dynamic_cast<GGDelaunayInfoSommetP>((*srfI)->reqSommet1()->reqInfoClient());
      GGDelaunayInfoSommetP info3P = dynamic_cast<GGDelaunayInfoSommetP>((*srfI)->reqSommet2()->reqInfoClient());

      *(kng+ioff+0) = static_cast<int>(info1P->reqNoNoeud());
      *(kng+ioff+1) = static_cast<int>(info2P->reqNoNoeud());
      *(kng+ioff+2) = static_cast<int>(info3P->reqNoNoeud());
      ++srfI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Génère le maillage avec des éléments T6L.
//
// Description:
//    La méthode privée <code>genereElemT3(...)</code> assigne
//    à vcorg les coordonnées des sommets du mailleur et
//    à kng les connectivités des surfaces du mailleur sous forme
//    d'éléments T6L.
//    Les tables vcorg et kng doivent être dimensionnées.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
ERMsg RemailleurH2D2::genMaillageT6(fint_t ndim, fint_t nnt,  double* vcorg,
                                    fint_t nnel, fint_t nelt, fint_t* kng) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ndim >= 2);
   PRECONDITION(nnt > 0 && nnt  == reqNbrNoeuds(ELEM_T6));
   PRECONDITION(nnel >= 6);
   PRECONDITION(nelt > 0 && nelt == reqNbrElements(ELEM_T6));
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   // ---  Génère les numéros de noeuds
   if (! estNumNoeudValideT6()) msg = genNumNoeudT6();

   // ---  Génère les noeuds
   GGMailleurDelaunay::TCContainerSommet::const_iterator somI = m_mllrP->reqSommetDebut();
   GGMailleurDelaunay::TCContainerSommet::const_iterator esoI = m_mllrP->reqSommetFin();
   while (somI != esoI)
   {
      ConstGGDelaunayInfoSommetP infoP = dynamic_cast<ConstGGDelaunayInfoSommetP>((*somI)->reqInfoClient());
      const size_t ioff = infoP->reqNoNoeud() * ndim;
      const GOCoordonneesXYZ& p = (*somI)->reqPosition();
      *(vcorg+ioff+0) = p.x() + m_off.x();
      *(vcorg+ioff+1) = p.y() + m_off.y();
      ++somI;
   }

   // ---  Génère les noeuds
   GGMailleurDelaunay::TCContainerArete::const_iterator artI = m_mllrP->reqAreteDebut();
   GGMailleurDelaunay::TCContainerArete::const_iterator esaI = m_mllrP->reqAreteFin();
   while (artI != esaI)
   {
      ConstGGDelaunayInfoAreteP infoP = dynamic_cast<ConstGGDelaunayInfoAreteP>((*artI)->reqInfoClient());
      const size_t ioff = infoP->reqNoNoeud() * ndim;
      const GOCoordonneesXYZ& p = ((*artI)->reqSommet0()->reqPosition() +
                                   (*artI)->reqSommet1()->reqPosition()) * 0.5;
      *(vcorg+ioff+0) = p.x() + m_off.x();
      *(vcorg+ioff+1) = p.y() + m_off.y();
      ++artI;
   }

   // ---  Génère les numéros de noeuds
   if (!estNumElemValideT6()) msg = genNumElem();

   // ---  Génère les éléments
   GGMailleurDelaunay::TCContainerSurface::const_iterator srfI = m_mllrP->reqSurfaceDebut();
   GGMailleurDelaunay::TCContainerSurface::const_iterator esrI = m_mllrP->reqSurfaceFin();
   while (srfI != esrI)
   {
      ConstGGDelaunayInfoSurfaceP infoP = dynamic_cast<ConstGGDelaunayInfoSurfaceP>((*srfI)->reqInfoClient());
      const size_t ioff = infoP->reqNoElem() * nnel;

      GGDelaunayInfoSommetP info1P = dynamic_cast<GGDelaunayInfoSommetP>((*srfI)->reqSommet0()->reqInfoClient());
      GGDelaunayInfoAreteP  info2P = dynamic_cast<GGDelaunayInfoAreteP> ((*srfI)->reqArete0 ()->reqInfoClient());
      GGDelaunayInfoSommetP info3P = dynamic_cast<GGDelaunayInfoSommetP>((*srfI)->reqSommet1()->reqInfoClient());
      GGDelaunayInfoAreteP  info4P = dynamic_cast<GGDelaunayInfoAreteP> ((*srfI)->reqArete1 ()->reqInfoClient());
      GGDelaunayInfoSommetP info5P = dynamic_cast<GGDelaunayInfoSommetP>((*srfI)->reqSommet2()->reqInfoClient());
      GGDelaunayInfoAreteP  info6P = dynamic_cast<GGDelaunayInfoAreteP> ((*srfI)->reqArete2 ()->reqInfoClient());

      *(kng+ioff+0) = static_cast<int>(info1P->reqNoNoeud());
      *(kng+ioff+1) = static_cast<int>(info2P->reqNoNoeud());
      *(kng+ioff+2) = static_cast<int>(info3P->reqNoNoeud());
      *(kng+ioff+3) = static_cast<int>(info4P->reqNoNoeud());
      *(kng+ioff+4) = static_cast<int>(info5P->reqNoNoeud());
      *(kng+ioff+5) = static_cast<int>(info6P->reqNoNoeud());
      ++srfI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//************************************************************************
// Sommaire:   Retourne le nombre de noeuds.
//
// Description:
//    La méthode publique <code>reqNbrNoeuds(...)</code> retourne
//    le nombre du noeuds du maillage qui serait généré.
//
// Entrée:
//    TypeElement ityp     Le type d'élément à générer
//
// Sortie:
//
// Notes:
//************************************************************************
fint_t RemailleurH2D2::reqNbrNoeuds(TypeElement ityp) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ityp >= ELEM_T3 && ityp <= ELEM_T6L);
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   TCMailleur::TCContainerArete::size_type nbrNoeuds = m_mllrP->reqNbrSommets();
   switch (ityp)
   {
      case ELEM_T3:  break;
      case ELEM_T6:  nbrNoeuds += m_mllrP->reqNbrAretes(); break;
      case ELEM_T6L: nbrNoeuds += m_mllrP->reqNbrAretes(); break;
      default:
         ASSERTION(ityp >= ELEM_T3 && ityp <= ELEM_T6L);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return static_cast<fint_t>(nbrNoeuds);
}

//************************************************************************
// Sommaire:   Retourne le nombre d'éléments du maillage.
//
// Description:
//    La méthode publique <code>reqNbrElements(...)</code> retourne
//    le nombre d'éléments du maillage qui serait généré.
//
// Entrée:
//    TypeElement ityp     Le type d'élément à générer
//
// Sortie:
//
// Notes:
//************************************************************************
fint_t RemailleurH2D2::reqNbrElements(TypeElement ityp) const
{
#ifdef MODE_DEBUG
   PRECONDITION(ityp >= ELEM_T3 && ityp <= ELEM_T6L);
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   return static_cast<fint_t>(m_mllrP->reqNbrSurfaces());
}

//************************************************************************
// Sommaire:   Retourne le nombre de limites.
//
// Description:
//    La méthode publique <code>reqNbrLimites(...)</code> retourne
//    le nombre de limites du maillage qui serait généré.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
fint_t RemailleurH2D2::reqNbrLimites() const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef std::set<const void*> TCSet;
   TCSet table;
   const void* infoVide = GGDelaunayInfoArete().reqCrc32();

   GGMailleurDelaunay::TCContainerArete::const_iterator artI = m_mllrP->reqAreteDebut();
   GGMailleurDelaunay::TCContainerArete::const_iterator esaI = m_mllrP->reqAreteFin();
   while (artI != esaI)
   {
      if ((*artI)->estSurPeau())
      {
         GGDelaunayInfoAreteP infoP = dynamic_cast<GGDelaunayInfoAreteP>((*artI)->reqInfoClient());
         if (infoP != NULL)
         {
            const void* info = infoP->reqCrc32();
            if (info != infoVide)
            {
               table.insert(info);
            }
         }
      }
      ++artI;
   }

   return static_cast<fint_t>(table.size());
}

//************************************************************************
// Sommaire:   Retourne le nombre de noeuds des limites.
//
// Description:
//    La méthode publique <code>reqNbrNoeudsLimites(...)</code> retourne
//    le nombre du noeuds des limites du maillage qui serait généré.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
fint_t RemailleurH2D2::reqNbrNoeudsLimites() const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef GGMailleurDelaunay::TCContainerArete::const_iterator TTIterArete;
   typedef std::set<const void*> TCSetLimites;
   TCSetLimites tableLimites;
   const void* infoVide = GGDelaunayInfoArete().reqCrc32();

   // ---  Récupère les infos des limites
   for (TTIterArete artI = m_mllrP->reqAreteDebut(); artI != m_mllrP->reqAreteFin(); ++artI)
   {
      if ((*artI)->estSurPeau())
      {
         GGDelaunayInfoAreteP infoP = dynamic_cast<GGDelaunayInfoAreteP>((*artI)->reqInfoClient());
         if (infoP != NULL)
         {
            const void* info = infoP->reqCrc32();
            if (info != infoVide)
            {
               tableLimites.insert(info);
            }
         }
      }
   }

   // ---  Boucle sur chaque limite
   EntierN inoeud = 0;
   for (TCSetLimites::const_iterator lI = tableLimites.begin(); lI != tableLimites.end(); ++lI)
   {
      const void*  infoLimite = (*lI);

      typedef std::set<GGDelaunaySommetP> TCSetSommets;
      TCSetSommets tableSommets;

      for (TTIterArete artI = m_mllrP->reqAreteDebut(); artI != m_mllrP->reqAreteFin(); ++artI)
      {
         if ((*artI)->estSurPeau())
         {
            GGDelaunayInfoAreteP infoP = dynamic_cast<GGDelaunayInfoAreteP>((*artI)->reqInfoClient());
            if ((infoP != NULL) && (infoP->reqCrc32() == infoLimite))
            {
               tableSommets.insert((*artI)->reqSommet0());
               tableSommets.insert((*artI)->reqSommet1());

               const Entier n = infoP->reqNoNoeud();
               if (n >= 0) inoeud++;
            }
         }
      }

      // ---  Remplis avec les sommets de la limite
      inoeud += static_cast<EntierN>(tableSommets.size());
   }

   return inoeud;
}

//************************************************************************
// Sommaire:   Génère les limites.
//
// Description:
//    La méthode publique <code>genLimites(...)</code> retourne
//    les limites du maillage.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
int RemailleurH2D2::genLimites(fint_t ncllim, fint_t* kcllim, fint_t nclnod, fint_t* kclnod) const
{
#ifdef MODE_DEBUG
   PRECONDITION(m_mllrP != NUL);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typedef GGMailleurDelaunay::TCContainerArete::const_iterator TTIterArete;
   typedef std::set<const void*> TCSetLimites;
   TCSetLimites tableLimites;
   const void* infoVide = GGDelaunayInfoArete().reqCrc32();

   // ---  Récupère les infos des limites
   for (TTIterArete artI = m_mllrP->reqAreteDebut(); artI != m_mllrP->reqAreteFin(); ++artI)
   {
      if ((*artI)->estSurPeau())
      {
         GGDelaunayInfoAreteP infoP = dynamic_cast<GGDelaunayInfoAreteP>((*artI)->reqInfoClient());
         if (infoP != NULL)
         {
            const void* info = infoP->reqCrc32();
            if (info != infoVide)
            {
               tableLimites.insert(info);
            }
         }
      }
   }
   ASSERTION(ncllim == tableLimites.size());

   // ---  Boucle sur chaque limite
   EntierN inoeud = 0;
   for (TCSetLimites::const_iterator lI = tableLimites.begin(); lI != tableLimites.end(); ++lI)
   {
      const void*  infoLimite = (*lI);
      const Entier indeb = inoeud;

      typedef std::set<GGDelaunaySommetP> TCSetSommets;
      TCSetSommets tableSommets;

      for (TTIterArete artI = m_mllrP->reqAreteDebut(); artI != m_mllrP->reqAreteFin(); ++artI)
      {
         if ((*artI)->estSurPeau())
         {
            GGDelaunayInfoAreteP infoP = dynamic_cast<GGDelaunayInfoAreteP>((*artI)->reqInfoClient());
            if ((infoP != NULL)  && (infoP->reqCrc32() == infoLimite))
            {
               tableSommets.insert( (*artI)->reqSommet0());
               tableSommets.insert( (*artI)->reqSommet1());

               const Entier n = infoP->reqNoNoeud();
               if (n >= 0) kclnod[inoeud++] = static_cast<int>(n);
            }
         }
      }

      // ---  Remplis avec les sommets de la limite
      for (TCSetSommets::iterator sI = tableSommets.begin(); sI != tableSommets.end(); ++sI)
      {
         GGDelaunayInfoSommetP infoP = dynamic_cast<GGDelaunayInfoSommetP>((*sI)->reqInfoClient());
         kclnod[inoeud++] = static_cast<int>(infoP->reqNoNoeud());
      }
      ASSERTION(inoeud <= nclnod);

      *kcllim++ = static_cast<fint_t>( reinterpret_cast<intptr_t>(infoLimite) );
      *kcllim++ = static_cast<fint_t>(0);
      *kcllim++ = static_cast<fint_t>(indeb);
      *kcllim++ = static_cast<fint_t>(inoeud);
      *kcllim++ = static_cast<fint_t>(0);
      *kcllim++ = static_cast<fint_t>(0);
      *kcllim++ = static_cast<fint_t>(0);
   }
   ASSERTION(inoeud == nclnod);

   return 0;
}

//************************************************************************
// Sommaire:   Retourne le message d'erreur.
//
// Description:
//    La méthode publique <code>reqErrMsg(...)</code> retourne
//    le message d'erreur.
//
// Entrée:
//    fint_t lbuf    La dimension du buffer
//
// Sortie:
//    char* buf      Le buffer qui contient le message
//
// Notes:
//************************************************************************
int RemailleurH2D2::reqErrMsg(char* buf, fint_t lbuf) const
{
#ifdef MODE_DEBUG
   PRECONDITION(buf != NUL);
   PRECONDITION(lbuf > 0);
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   std::string s;
   if (! m_msg)
   {
      std::ostringstream os;
      const EntierN nbMsg = m_msg.reqNbrMessages();
      for (EntierN i = 0; i < nbMsg; ++i)
      {
         os << m_msg[i] << "; ";
      }
      os << std::ends;
      s = os.str();
   }

   memset(buf, 0x0, lbuf);
   if (s.size() > 0)
   {
      size_t l = std::min(s.size()-1, static_cast<size_t>(lbuf-1));
      memcpy(buf, s.c_str(), l);
   }

   return 0;
}
