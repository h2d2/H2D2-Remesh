//************************************************************************
// --- Copyright (c) INRS 2010-2016
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// $Id$
// Classe:  c_RemailleurH2D2
//************************************************************************
#include "c_RemailleurH2D2.h"
#include "RemailleurH2D2.h"

DLL_IMPEXP_REMAILLEURH2D2
RemailleurH2D2* ctrRemailleur()
{
   return new RemailleurH2D2();
}

DLL_IMPEXP_REMAILLEURH2D2
void dtrRemailleur(RemailleurH2D2* rP)
{
   delete rP; rP = NUL;
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t asgMaillage(RemailleurH2D2* rP,
                   fint_t ndim, fint_t nnt, double* vcorg,
                   fint_t itpv, fint_t nnelv, fint_t ncelv, fint_t nelv, fint_t* kngv,
                   bool dblcLmt)
{
   return rP->asgMaillage(ndim, nnt, vcorg,
                          static_cast<RemailleurH2D2::TypeElement>(itpv), nnelv, ncelv, nelv, kngv,
                          dblcLmt);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t asgLimite  (RemailleurH2D2* rP,
                   fint_t itps, fint_t nnels, fint_t ncels, fint_t nels, fint_t* kngs,
                   fint_t info, fint_t nclele, fint_t* kclele)
{
   return rP->asgLimite(static_cast<RemailleurH2D2::TypeElement>(itps), nnels, ncels, nels, kngs,
                        info, nclele, kclele);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t asgChampErr(RemailleurH2D2* rP, fint_t nval, fint_t nnt,  double* err, double hscl, double hmin, double hmax)
{
   return rP->asgChampErr(nval, nnt, err, hscl, hmin, hmax);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t ajtChampErr(RemailleurH2D2* rP, fint_t nval, fint_t nnt,  double* err, double hscl, double hmin, double hmax)
{
   return rP->ajtChampErr(nval, nnt, err, hscl, hmin, hmax);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t adapte(RemailleurH2D2* rP, fint_t ni, double* info, TTInfoCb cb, fint_t ntgt, double htgt, fint_t rmin, fint_t rmax, fint_t n1, fint_t n2, double* actions)
{
   return rP->adapte(ni, info, cb, ntgt, htgt, rmin, rmax, n1, n2, actions);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqNbrNoeuds(RemailleurH2D2* rP, fint_t ityp)
{
   return rP->reqNbrNoeuds(static_cast<RemailleurH2D2::TypeElement>(ityp));
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqNbrElements(RemailleurH2D2* rP, fint_t ityp)
{
   return rP->reqNbrElements(static_cast<RemailleurH2D2::TypeElement>(ityp));
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqChampErr(RemailleurH2D2* rP, fint_t nval, fint_t nnt, double* err)
{
   return rP->genChampErr(nval, nnt, err);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqMaillage(RemailleurH2D2* rP,
                   fint_t ityp,
                   fint_t ndim, fint_t nnt,  double* vcorg,
                   fint_t nnel, fint_t nelt, fint_t* kng)
{
   return rP->genMaillage(static_cast<RemailleurH2D2::TypeElement>(ityp), ndim, nnt, vcorg, nnel, nelt, kng);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqNbrLimites(RemailleurH2D2* rP)
{
   return rP->reqNbrLimites();
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqNbrNoeudsLimites(RemailleurH2D2* rP)
{
   return rP->reqNbrNoeudsLimites();
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqLimites (RemailleurH2D2* rP,
                   fint_t ncllim, fint_t* kcllim,
                   fint_t nclnod, fint_t* kclnod)
{
   return rP->genLimites(ncllim, kcllim, nclnod, kclnod);
}

DLL_IMPEXP_REMAILLEURH2D2
fint_t reqErrMsg(RemailleurH2D2* rP, char* buf, fint_t lbuf)
{
   return rP->reqErrMsg(buf, lbuf);
}

