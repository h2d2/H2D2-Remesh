//************************************************************************
// --- Copyright (c) INRS 2000-2016
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// $Id$
//
// Classe:  RemailleurH2D2
//
// Description:
//    La classe RemailleurH2D2
//
//    Le champ d'erreur est basé sur le maillage à adapter.
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef REMAILLEURH2D2_H_DEJA_INCLU
#define REMAILLEURH2D2_H_DEJA_INCLU

#ifdef MODE_DYNAMIC
#  undef MODE_DYNAMIC
#endif

#ifndef REMAILLEURH2D2
#  define REMAILLEURH2D2 1
#endif // REMAILLEURH2D2

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "f_types.h"

#include "SRChampEFEllipseErreur.hf"
#include "EFMaillage3D.hf"
#include "SRRegion.hf"
#include "AlgoTransfertMaillage.hf"
#include "GOCoord3.h"
DECLARE_CLASS(GGMailleurDelaunay);
DECLARE_CLASS(GGDelaunayScenario);
DECLARE_CLASS(GGDelaunayAvancement);

DECLARE_CLASS(RemailleurH2D2);

class DLL_IMPEXP(REMAILLEURH2D2) RemailleurH2D2
{
public:
   typedef SRChampEFEllipseErreur            TCChampErr;
   typedef EFMaillage3D                      TCMaillage;
   typedef GOCoordonneesXYZ                  TCCoord;
   typedef SRRegion<TCCoord>                 TCRegion;
   typedef GGMailleurDelaunay                TCMailleur;
   typedef AlgoTransfertMaillage<TCMaillage> TCAlgo;

   enum Operation
   {
      INDEFINI                = 0,
      ETAT_INITIAL            = 1,
      ETAT_FINAL              = 2,
      LISSE_METRIQUES         = 3,
      RESCALE_METRIQUES       = 4,
      MAILLE_PEAU             = 5,
      RETOURNE_ARETES         = 6,
      SOIGNE_PEAU             = 7,
      SOIGNE_PONT             = 8,
      DERAFFINE_PAR_SOMMETS   = 9,
//      DERAFFINE_PAR_SURFACES,
      RAFFINE_PAR_ARETES      =10,
      RAFFINE_PAR_SURFACES    =11,
      REGULARISE_PAR_ARETES   =12,
      REGULARISE_PAR_SURFACES =13,
      REGULARISE_BARYCENTRIQUE=14,
      REGULARISE_ETOILES      =15
   };
   enum TypeElement
   {
      ELEM_L2  = 1,
      ELEM_L3  = 2,
      ELEM_L3L = 3,
      ELEM_T3  = 4,
      ELEM_T6  = 5,
      ELEM_T6L = 6
   };

       RemailleurH2D2();
      ~RemailleurH2D2();


   int      adapte         (fint_t, double*, TTInfoCb, fint_t, double, fint_t, fint_t, fint_t, fint_t, double*);
   int      asgChampErr    (fint_t, fint_t, double*, double, double, double);
   int      ajtChampErr    (fint_t, fint_t, double*, double, double, double);
   int      asgMaillage    (fint_t, fint_t, double*, TypeElement, fint_t, fint_t, fint_t, fint_t*, bool);
   int      asgLimite      (TypeElement, fint_t, fint_t, fint_t, fint_t*, fint_t, fint_t, fint_t*);

   int      genChampErr    (fint_t, fint_t, double*) const;
   int      genMaillage    (TypeElement, fint_t, fint_t, double*, fint_t, fint_t, fint_t*) const;
   int      genLimites     (fint_t, fint_t*, fint_t, fint_t*) const;

   fint_t   reqNbrNoeuds   (TypeElement) const;
   fint_t   reqNbrElements (TypeElement) const;
   fint_t   reqNbrLimites      () const;
   fint_t   reqNbrNoeudsLimites() const;

   void     cbInfo         (GGDelaunayAvancement&);
   int      reqErrMsg      (char*, fint_t) const;

protected:
   void     invariant      (ConstCarP) const;

private:
   RemailleurH2D2 (const RemailleurH2D2 &);
   RemailleurH2D2& operator = (const RemailleurH2D2 &) const;

   Booleen estNumElemValideT3 () const;
   Booleen estNumElemValideT6 () const;
   Booleen estNumNoeudValideT3() const;
   Booleen estNumNoeudValideT6() const;
   ERMsg   genNumElem         () const;
   ERMsg   genNumNoeudT3      () const;
   ERMsg   genNumNoeudT6      () const;
   ERMsg   genMaillageT3      (fint_t, fint_t, double*, fint_t, fint_t, fint_t*) const;
   ERMsg   genMaillageT6      (fint_t, fint_t, double*, fint_t, fint_t, fint_t*) const;

   TCMaillage*   m_mailP;
   TCChampErr*   m_champP;
   TCRegion*     m_regnP;
   TCMailleur*   m_mllrP;
   TCAlgo*       m_algoP;
   mutable ERMsg m_msg;
   TCCoord       m_off;
   double*       m_info;
   TTInfoCb      m_cb;
};

//******************************************************************************
// Sommaire: Teste les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> implante les invariants de la
//    classe. Cette méthode doit obligatoirement appeler les invariants de ses
//    parents.
//
// Entrée:
//    ConstCarP conditionP :  PRECONDITION ou POSTCONDITION
//
// Sortie:
//
// Notes:
//
//******************************************************************************
#ifdef MODE_DEBUG
inline void RemailleurH2D2::invariant(ConstCarP conditionP) const
{
   INVARIANT(((m_mailP == NUL) == (m_regnP == NUL)), conditionP);
   INVARIANT(((m_mailP == NUL) == (m_mllrP == NUL)), conditionP);
}
#else
inline void RemailleurH2D2::invariant(ConstCarP) const
{
}
#endif

#endif  // REMAILLEURH2D2_H_DEJA_INCLU
