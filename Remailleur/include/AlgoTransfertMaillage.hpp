//************************************************************************
// --- Copyright (c) INRS 2000-2016
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// $Id$
// Classe:  AlgoTransfertMaillage
//************************************************************************
#ifndef TRANSFERTMAILLAGE_HPP_DEJA_INCLU
#define TRANSFERTMAILLAGE_HPP_DEJA_INCLU

#include "ALFiltrePeau.h"
#include "GGDelaunayInfo.h"
#include <utility> //pair

//**************************************************************
// Sommaire: Constructeur par défaut
//
// Description: Constructeur par défaut, initialise attributs privés.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
AlgoTransfertMaillage<TTMaillage>::AlgoTransfertMaillage()
   : m_sommets()
   , m_aretes()
   , m_mailleurP(NUL)
   , m_uniquerP(NUL)
   , m_infoP(NUL)
{
#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Destructeur par défaut
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//****************************************************************
template <typename TTMaillage>
AlgoTransfertMaillage<TTMaillage>::~AlgoTransfertMaillage()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire:
//
// Description:
//    Ajoute un sommet comme sommet normal. Les sommets de peau
//    seront bloqués par la suite.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
typename AlgoTransfertMaillage<TTMaillage>::TCSommetI
AlgoTransfertMaillage<TTMaillage>::ajouteSommet(const TCSommet& i,
                                                GGDelaunayInfoBaseP infoP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const static Booleen b = VRAI;   // sommet bougeable
   const static Booleen d = VRAI;   // sommet déraffinable
   const static Booleen p = FAUX;   // sommet sur peau

   TCSommetI iI = m_sommets.find(i);
   if (iI == m_sommets.end())
   {
      const GOCoordonneesXYZ& point = i.m_pP->reqCoordonnees();
      i.m_sP = m_mailleurP->ajouteSommet(point, b, d, p);

      if (infoP) i.m_sP->asgInfoClient(infoP);

      iI = m_sommets.insert(i).first;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return iI;
}

//**************************************************************
// Sommaire:
//
// Description:
//    Ajoute une arête comme arête normale. Les arêtes de peau
//    seront bloquées par la suite.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
typename AlgoTransfertMaillage<TTMaillage>::TCSetAretes::iterator
AlgoTransfertMaillage<TTMaillage>::ajouteArete(const TCArete& i,
                                               GGDelaunayInfoBaseP infoP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   const static Booleen r = VRAI;   // arête raffinable
   const static Booleen s = VRAI;   // arête retournable
   const static Booleen p = FAUX;   // arête sur peau

   TCAreteI aI = m_aretes.find(i);
   if (aI == m_aretes.end())
   {
      i.m_aP = m_mailleurP->ajouteArete(i.m_s1I->m_sP, i.m_s2I->m_sP, r, s, p);
      if (infoP) i.m_aP->asgInfoClient(infoP);
      aI = m_aretes.insert(i).first;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return aI;
}

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void
AlgoTransfertMaillage<TTMaillage>::ajouteSurface(const TCSurface& i,
                                                 GGDelaunayInfoBaseP infoP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   i.m_sP = m_mailleurP->ajouteSurface(i.m_s1I->m_sP, i.m_s2I->m_sP, i.m_s3I->m_sP,
                                       i.m_a1I->m_aP, i.m_a2I->m_aP, i.m_a3I->m_aP);
   if (infoP) i.m_sP->asgInfoClient(infoP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Flag le sommet comme sommet de peau.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void
AlgoTransfertMaillage<TTMaillage>::bloqueSommetPeau(GGDelaunaySommetP sP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   sP->asgBougeable   (false);
   sP->asgDeraffinable(false);
   sP->asgEstSurPeau  (true);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Flag le sommet comme sommet de peau.
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void
AlgoTransfertMaillage<TTMaillage>::bloqueAretePeau(GGDelaunayAreteP aP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   aP->asgRetournable(false);
   aP->asgEstSurPeau (true);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L2 dans le setElementP illégaux
//
// Description:
//
// Entrée:
//    const TCElementL2& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void
AlgoTransfertMaillage<TTMaillage>::filtreNoeudLimite(ConstTCNoeudP nP)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   typename TCUniquer::iterator iI = m_uniquerP->find(nP);
   if (iI == m_uniquerP->end())
      m_uniquerP->insert(nP);
   else
      m_uniquerP->erase(iI);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément L2 dans le setElementP illégaux
//
// Description:
//
// Entrée:
//    const TCElementL2& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void
AlgoTransfertMaillage<TTMaillage>::relacheAnglePlat(GGDelaunaySommetP sP,
                                                    GGDelaunayAreteP  a1P)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

   static double PI = atan(1.0)*4.0;
   static double DMAX = 5.0 / 180.0;   // delta max from flat angle: 5 degree

   // ---  L'autre arête de sommet sI et sur la peau
   GGDelaunayAreteP a2P = NULL;
   for (EntierN i = 0; i < sP->reqNbAretes(); ++i)
   {
      GGDelaunayAreteP aP = sP->reqArete(i);
      if (aP != a1P && aP->estSurPeau())
      {
         a2P = aP;
         break;
      }
   }
   ASSERTION(a2P != NULL);

   // ---  L'angle du sommet sI
   GOCoordonneesXYZ d21 = a1P->reqAutreSommet(sP)->reqPosition() - sP->reqPosition();
   GOCoordonneesXYZ d31 = a2P->reqAutreSommet(sP)->reqPosition() - sP->reqPosition();
   DReel alfa = atan2(prodVect(d21, d31).norme(), prodScal(d21, d31));
   alfa = fabs(alfa) / PI;
   alfa = fabs(alfa - 1.0);
   if (alfa < DMAX)
   {
      sP->asgBougeable   (true);
      sP->asgDeraffinable(true);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Insère l'élément P1 dans le setElementP illégaux
//
// Description:
//
// Entrée:
//    const TCElementP1& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementP1(const typename TCMaillage::TCElementP1&)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif  // ifdef MODE_DEBUG

}

//**************************************************************
// Sommaire: L'élément L2
//
// Description:
//
// Entrée:
//    const TCElementL2& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void
AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementL2(const typename TCMaillage::TCElementL2& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_action != NORMAL);
#endif  // ifdef MODE_DEBUG

   // ---  Les sommets
   ConstTCNoeudP n1P = ele.noeud1P;
   ConstTCNoeudP n2P = ele.noeud2P;
   TCSommetI s1I = m_sommets.find( TCSommet(n1P) );
   TCSommetI s2I = m_sommets.find( TCSommet(n2P) );
   ASSERTION(s1I != m_sommets.end());
   ASSERTION(s2I != m_sommets.end());

   // ---  L'arête
   TCAreteI aI = m_aretes.find( TCArete(s1I, s2I) );
   ASSERTION(aI != m_aretes.end());

   // ---  L'action
   switch (m_action)
   {
      case BLOQUE_SOMMETS_PEAU:
         bloqueSommetPeau(s1I->m_sP);
         bloqueSommetPeau(s2I->m_sP);
         bloqueAretePeau(aI->m_aP);
         break;
      case LIBERE_SOMMETS_PEAU_ANGLE_PLATS:
         relacheAnglePlat(s1I->m_sP, aI->m_aP);
         relacheAnglePlat(s2I->m_sP, aI->m_aP);
         break;
      case FILTRE_LIMITE:
         filtreNoeudLimite(n1P);
         filtreNoeudLimite(n2P);
         break;
      case ASSIGNE_INFO_LIMITE:
         if ((aI->m_aP)->reqInfoClient() != NULL)
         {
            GGDelaunayInfoAreteP iP = dynamic_cast<GGDelaunayInfoAreteP>((aI->m_aP)->reqInfoClient());
            iP->asgCrc32(m_infoP->reqCrc32());
         }
         else
         {
            (aI->m_aP)->asgInfoClient(m_infoP);
         }
         break;
      default:
         break;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: L'élément L3
//
// Description:
//
// Entrée:
//    const TCElementL3& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementL3(const typename TCMaillage::TCElementL3& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_action != NORMAL);
#endif  // ifdef MODE_DEBUG

   // ---  Les sommets
   ConstTCNoeudP n1P = ele.noeud1P;
   ConstTCNoeudP n2P = ele.noeud3P;
   TCSommetI s1I = m_sommets.find( TCSommet(n1P) );
   TCSommetI s2I = m_sommets.find( TCSommet(n2P) );
   ASSERTION(s1I != m_sommets.end());
   ASSERTION(s2I != m_sommets.end());

   // ---  L'arête
   TCAreteI aI = m_aretes.find( TCArete(s1I, s2I) );

   switch (m_action)
   {
      case BLOQUE_SOMMETS_PEAU:
         bloqueSommetPeau(s1I->m_sP);
         bloqueSommetPeau(s2I->m_sP);
         bloqueAretePeau(aI->m_aP);
         break;
      case LIBERE_SOMMETS_PEAU_ANGLE_PLATS:
         relacheAnglePlat(s1I->m_sP, aI->m_aP);
         relacheAnglePlat(s2I->m_sP, aI->m_aP);
         break;
      case FILTRE_LIMITE:
         filtreNoeudLimite(n1P);
         filtreNoeudLimite(n2P);
         break;
      case ASSIGNE_INFO_LIMITE:
         if ((aI->m_aP)->reqInfoClient() != NULL)
         {
            GGDelaunayInfoAreteP iP = dynamic_cast<GGDelaunayInfoAreteP>((aI->m_aP)->reqInfoClient());
            iP->asgCrc32(m_infoP->reqCrc32());
         }
         else
         {
            (aI->m_aP)->asgInfoClient(m_infoP);
         }
         break;
      default:
         break;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: L'élément L3L
//
// Description:
//
// Entrée:
//    const TCElementL3L& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_action != NORMAL);
#endif  // ifdef MODE_DEBUG

   // ---  Les sommets
   ConstTCNoeudP n1P = ele.noeud1P;
   ConstTCNoeudP n2P = ele.noeud3P;
   TCSommetI s1I = m_sommets.find( TCSommet(n1P) );
   TCSommetI s2I = m_sommets.find( TCSommet(n2P) );
   ASSERTION(s1I != m_sommets.end());
   ASSERTION(s2I != m_sommets.end());

   // ---  L'arête
   TCAreteI aI = m_aretes.find( TCArete(s1I, s2I) );

   switch (m_action)
   {
      case BLOQUE_SOMMETS_PEAU:
         bloqueSommetPeau(s1I->m_sP);
         bloqueSommetPeau(s2I->m_sP);
         bloqueAretePeau(aI->m_aP);
         break;
      case LIBERE_SOMMETS_PEAU_ANGLE_PLATS:
         relacheAnglePlat(s1I->m_sP, aI->m_aP);
         relacheAnglePlat(s2I->m_sP, aI->m_aP);
         break;
      case FILTRE_LIMITE:
         filtreNoeudLimite(n1P);
         filtreNoeudLimite(n2P);
         break;
      case ASSIGNE_INFO_LIMITE:
         if ((aI->m_aP)->reqInfoClient() != NULL)
         {
            GGDelaunayInfoAreteP iP = dynamic_cast<GGDelaunayInfoAreteP>((aI->m_aP)->reqInfoClient());
            iP->asgCrc32(m_infoP->reqCrc32());
         }
         else
         {
            (aI->m_aP)->asgInfoClient(m_infoP);
         }
         break;
      default:
         break;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:  L'élément Q4.
//
// Description:
//
// Entrée:
//    const TCElementQ4& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementQ4(const typename TCMaillage::TCElementQ4& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_action == NORMAL);
#endif  // ifdef MODE_DEBUG

   TCSommetI s1I, s2I, s3I, s4I;
   ConstTCNoeudP nP;

   // ---  Ajoute les sommets
   GGDelaunayInfoSommet infoSommet;
   nP = ele.noeud1P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s1I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud2P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s2I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud3P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s3I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud4P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s4I = ajouteSommet(TCSommet(nP), &infoSommet);

   // ---  Ajoute les arêtes
   ajouteArete(TCArete(s1I, s2I));
   ajouteArete(TCArete(s2I, s3I));
   ajouteArete(TCArete(s3I, s4I));
   ajouteArete(TCArete(s4I, s1I));

   // ---  Ajoute l'élément
//   ajouteSurface(TCSetItem(ele.noeud1P, ele.noeud2P, ele.noeud3P));
   //GGDelaunayInfoSurface infoSurface;
   //infoSurface.asgNoElem(ele.reqNoElement()); ajouteSurface(TCSurface(s1I, s2I, s3I, a1I, a2I, a3I), &infoSurface);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:  L'élément T3.
//
// Description:
//
// Entrée:
//    const TCElementT3& ele: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementT3(const typename TCMaillage::TCElementT3& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_action == NORMAL);
#endif  // ifdef MODE_DEBUG

   TCSommetI s1I, s2I, s3I;
   TCAreteI  a1I, a2I, a3I;
   ConstTCNoeudP nP;

   // ---  Ajoute les sommets
   GGDelaunayInfoSommet infoSommet;
   nP = ele.noeud1P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s1I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud2P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s2I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud3P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s3I = ajouteSommet(TCSommet(nP), &infoSommet);

   // ---  Ajoute les arêtes
   a1I = ajouteArete(TCArete(s1I, s2I));
   a2I = ajouteArete(TCArete(s2I, s3I));
   a3I = ajouteArete(TCArete(s3I, s1I));

   // ---  Ajoute l'élément
   GGDelaunayInfoSurface infoSurface;
   infoSurface.asgNoElem(ele.reqNoElement()); ajouteSurface(TCSurface(s1I, s2I, s3I, a1I, a2I, a3I), &infoSurface);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:  L'élément T6.
//
// Description:
//
// Entrée:  const TCElementT6& elemT6: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementT6(const typename TCMaillage::TCElementT6& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_action == NORMAL);
#endif  // ifdef MODE_DEBUG

   TCSommetI s1I, s2I, s3I;
   TCAreteI  a1I, a2I, a3I;
   ConstTCNoeudP nP;

   // ---  Ajoute les sommets
   GGDelaunayInfoSommet infoSommet;
   nP = ele.noeud1P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s1I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud3P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s2I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud5P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s3I = ajouteSommet(TCSommet(nP), &infoSommet);

   // ---  Ajoute les arêtes
   GGDelaunayInfoArete infoArete;
   nP = ele.noeud2P; infoArete.asgNoNoeud(nP->reqNoNoeud()); a1I = ajouteArete(TCArete(s1I, s2I), &infoArete);
   nP = ele.noeud4P; infoArete.asgNoNoeud(nP->reqNoNoeud()); a2I = ajouteArete(TCArete(s2I, s3I), &infoArete);
   nP = ele.noeud6P; infoArete.asgNoNoeud(nP->reqNoNoeud()); a3I = ajouteArete(TCArete(s3I, s1I), &infoArete);

   // ---  Ajoute l'élément
   GGDelaunayInfoSurface infoSurface;
   infoSurface.asgNoElem(ele.reqNoElement()); ajouteSurface(TCSurface(s1I, s2I, s3I, a1I, a2I, a3I), &infoSurface);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire:  L'élément T6L.
//
// Description:
//
// Entrée:  const TCElementT6L& elemT6L: élément à traiter
//
// Sortie:
//
// Notes:
//
//****************************************************************
template <typename TTMaillage>
void AlgoTransfertMaillage<TTMaillage>::executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L& ele)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   PRECONDITION(m_action == NORMAL);
#endif  // ifdef MODE_DEBUG

   TCSommetI s1I, s2I, s3I;
   TCAreteI  a1I, a2I, a3I;
   ConstTCNoeudP nP;

   // ---  Ajoute les sommets
   GGDelaunayInfoSommet infoSommet;
   nP = ele.noeud1P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s1I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud3P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s2I = ajouteSommet(TCSommet(nP), &infoSommet);
   nP = ele.noeud5P; infoSommet.asgNoNoeud(nP->reqNoNoeud()); s3I = ajouteSommet(TCSommet(nP), &infoSommet);

   // ---  Ajoute les arêtes
   GGDelaunayInfoArete infoArete;
   nP = ele.noeud2P; infoArete.asgNoNoeud(nP->reqNoNoeud()); a1I = ajouteArete(TCArete(s1I, s2I), &infoArete);
   nP = ele.noeud4P; infoArete.asgNoNoeud(nP->reqNoNoeud()); a2I = ajouteArete(TCArete(s2I, s3I), &infoArete);
   nP = ele.noeud6P; infoArete.asgNoNoeud(nP->reqNoNoeud()); a3I = ajouteArete(TCArete(s3I, s1I), &infoArete);

   // ---  Ajoute l'élément
   GGDelaunayInfoSurface infoSurface;
   infoSurface.asgNoElem(ele.reqNoElement()); ajouteSurface(TCSurface(s1I, s2I, s3I, a1I, a2I, a3I), &infoSurface);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
}

//**************************************************************
// Sommaire: Méthode porte d'entrée de l'algorithme:
//
// Description:
//    Assigne le maillage de volume.
//
// Entrée:
//    const TCMaillage& maillage: Maillage de volume
//
// Sortie:
//    GGMailleurDelaunay& mailleur
//
// Notes:
//    Introduis le maillage, tous noeuds libres
//    Bloque la peau
//    Libère la peau avec angle plat
//****************************************************************
template <typename TTMaillage>
ERMsg
AlgoTransfertMaillage<TTMaillage>::operator()(GGMailleurDelaunay& mailleur,
                                              const TTMaillage&   maillage,
                                              bool debloqueAnglesPlats)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   typename TCMaillage::TypeMaillage typeMaillage;
   maillage.reqTypeMaillage(typeMaillage);
   PRECONDITION(typeMaillage != TCMaillage::PEAU);
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   m_mailleurP = &mailleur;

   // ---  Filtre la peau
   TCMaillage mailPeau;
   if (msg)
   {
      ALFiltrePeau<TCMaillage> algo;
      msg = algo.filtrePeau(mailPeau, maillage);
   }

   // ---  Traite le maillage de volume
   if (msg)
   {
      m_action = NORMAL;
      this->itereSurTousMGElements(maillage);
   }

   // ---  Traite la peau pour bloquer les sommets
   if (msg)
   {
      m_action = BLOQUE_SOMMETS_PEAU;
      this->itereSurTousMGElements(mailPeau);
   }

   // ---  Traite la peau pour libérer les sommets plats
   if (msg && debloqueAnglesPlats)
   {
      m_action = LIBERE_SOMMETS_PEAU_ANGLE_PLATS;
      this->itereSurTousMGElements(mailPeau);
   }

   // ---  Book-keeping
   if (msg)
   {
      msg = m_mailleurP->enleveReferencesAjouteSommet();
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

//**************************************************************
// Sommaire: Méthode porte d'entrée de l'algorithme:
//
// Description:
//    Assigne les limites
//
// Entrée:
//    const TCMaillage& maiLmt: Maillage des limites
//
// Sortie:
//    GGMailleurDelaunay& mailleur
//
// Notes:
//    Introduis l'info sur les arêtes
//    Bloque les noeuds extremes de CL
//****************************************************************
template <typename TTMaillage>
ERMsg
AlgoTransfertMaillage<TTMaillage>::operator()(GGMailleurDelaunay& mailleur,
                                              const TTMaillage&   maiLmt,
                                              void* info)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
   typename TCMaillage::TypeMaillage typeMaillage;
   maiLmt.reqTypeMaillage(typeMaillage);
   PRECONDITION(typeMaillage == TCMaillage::PEAU);
   PRECONDITION(m_uniquerP == NUL);
#endif  // ifdef MODE_DEBUG

   ERMsg msg = ERMsg::OK;

   m_mailleurP = &mailleur;

   // ---  Filtres la peau i.e. les bouts
   TCUniquer uniquer;
   m_uniquerP = &uniquer;
   if (msg)
   {
      m_action = FILTRE_LIMITE;
      this->itereSurTousMGElements(maiLmt);
   }

   // ---  Assigne l'info
   if (msg)
   {
      GGDelaunayInfoArete infoArete;
      m_infoP = &infoArete;
      m_infoP->asgCrc32(info);
      m_action = ASSIGNE_INFO_LIMITE;
      this->itereSurTousMGElements(maiLmt);
      m_infoP = NULL;
   }

   // ---  Bloque les sommets
   if (msg)
   {
      m_action = BLOQUE_SOMMETS_PEAU;
      for (typename TCUniquer::iterator it = m_uniquerP->begin(); it != m_uniquerP->end(); ++it)
      {
         TCSommetI sI = m_sommets.find(TCSommet(*it));
         bloqueSommetPeau(sI->m_sP);
      }
   }

   // ---  Nettoie
   m_uniquerP = NUL;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif  // ifdef MODE_DEBUG
   return msg;
}

#endif   // ifndef TRANSFERTMAILLAGE_HPP_DEJA_INCLU
