//************************************************************************
// --- Copyright (c) INRS 2010-2016
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// $Id$
//
// Classe:  RemailleurH2D2
//
// Description:
//    La classe RemailleurH2D2
//
//    Le champ d'erreur est basé sur le maillage à adapter.
//
// Attributs:
//
// Notes:
//
//************************************************************************
#ifndef C_REMAILLEURH2D2_H_DEJA_INCLU
#define C_REMAILLEURH2D2_H_DEJA_INCLU

#ifndef REMAILLEURH2D2
#  define REMAILLEURH2D2 1
#endif // REMAILLEURH2D2

#include "sytypes.h"
#include "f_types.h"
DECLARE_CLASS(RemailleurH2D2);

#define DLL_IMPEXP_REMAILLEURH2D2 DLL_IMPEXP(REMAILLEURH2D2)

#ifdef __cplusplus
extern "C"
{
#endif

DLL_IMPEXP_REMAILLEURH2D2 RemailleurH2D2* ctrRemailleur ();
DLL_IMPEXP_REMAILLEURH2D2 void            dtrRemailleur (RemailleurH2D2*);

DLL_IMPEXP_REMAILLEURH2D2 fint_t          asgMaillage   (RemailleurH2D2*, fint_t, fint_t, double*, fint_t, fint_t, fint_t, fint_t, fint_t*, bool);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          asgLimite     (RemailleurH2D2*, fint_t, fint_t, fint_t, fint_t, fint_t*, fint_t, fint_t, fint_t*);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          asgChampErr   (RemailleurH2D2*, fint_t, fint_t, double*, double, double, double);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          ajtChampErr   (RemailleurH2D2*, fint_t, fint_t, double*, double, double, double);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          adapte        (RemailleurH2D2*, fint_t, double*, TTInfoCb, fint_t, double, fint_t, fint_t, fint_t, fint_t, double*);

DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqNbrNoeuds  (RemailleurH2D2*, fint_t);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqNbrElements(RemailleurH2D2*, fint_t);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqChampErr   (RemailleurH2D2*, fint_t, fint_t, double*);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqMaillage   (RemailleurH2D2*, fint_t, fint_t, fint_t, double*, fint_t, fint_t, fint_t*);

DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqNbrLimites      (RemailleurH2D2*);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqNbrNoeudsLimites(RemailleurH2D2*);
DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqLimites         (RemailleurH2D2*, fint_t, fint_t*, fint_t, fint_t*);

DLL_IMPEXP_REMAILLEURH2D2 fint_t          reqErrMsg     (RemailleurH2D2*, char*, fint_t);

#ifdef __cplusplus
}
#endif

#endif  // C_REMAILLEURH2D2_H_DEJA_INCLU
