//************************************************************************
// --- Copyright (c) INRS 2000-2016
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// $Id$
//
// Classe: AlgoTransfertMaillage
//
// Description:
//
// Attributs:
//
// Notes:
//************************************************************************
#ifndef TRANSFERTMAILLAGE_H_DEJA_INCLU
#define TRANSFERTMAILLAGE_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"
#include "ermsg.h"

#include "MGElement.h"
#include "GORegion.hf"
#include "MGConstElementAlgorithme.h"
DECLARE_CLASS(GGMailleurDelaunay);
DECLARE_CLASS(GGDelaunaySommet);
DECLARE_CLASS(GGDelaunayArete);
DECLARE_CLASS(GGDelaunaySurface);
DECLARE_CLASS(GGDelaunayInfoArete);

#include <algorithm>
#include <set>

template <typename TTMaillage>
class AlgoTransfertMaillage
   : public MGConstElementAlgorithme<TTMaillage>
{
public:
   typedef          AlgoTransfertMaillage<TTMaillage> TCSelf;
   typedef          TTMaillage                        TCMaillage;
   typedef typename TCMaillage::ConstTCNoeudP         ConstTCNoeudP;

                AlgoTransfertMaillage   ();
   virtual     ~AlgoTransfertMaillage   ();

   virtual void executeAlgoMGElementP1 (const typename TCMaillage::TCElementP1&);
   virtual void executeAlgoMGElementL2 (const typename TCMaillage::TCElementL2&);
   virtual void executeAlgoMGElementL3 (const typename TCMaillage::TCElementL3&);
   virtual void executeAlgoMGElementL3L(const typename TCMaillage::TCElementL3L&);
   virtual void executeAlgoMGElementQ4 (const typename TCMaillage::TCElementQ4&);
   virtual void executeAlgoMGElementT3 (const typename TCMaillage::TCElementT3&);
   virtual void executeAlgoMGElementT6 (const typename TCMaillage::TCElementT6&);
   virtual void executeAlgoMGElementT6L(const typename TCMaillage::TCElementT6L&);

   ERMsg        operator () (GGMailleurDelaunay&, const TTMaillage&, bool debloqueAnglesPlats = false);
   ERMsg        operator () (GGMailleurDelaunay&, const TTMaillage&, void*);

protected:
   virtual void  invariant       (ConstCarP) const;

private:
   enum Action{NORMAL,
               BLOQUE_SOMMETS_PEAU,
               LIBERE_SOMMETS_PEAU_ANGLE_PLATS,
               FILTRE_LIMITE,
               ASSIGNE_INFO_LIMITE};
   struct TCSommet;
   struct TCArete;
   typedef std::set<TCSommet> TCSetSommets;
   typedef std::set<TCArete>  TCSetAretes;
   typedef typename TCSetSommets::iterator TCSommetI;
   typedef typename TCSetAretes::iterator  TCAreteI;
   typedef std::set<typename TCMaillage::ConstTCNoeudP> TCUniquer;

   struct TCSommet
   {
      ConstTCNoeudP m_pP;
      mutable GGDelaunaySommetP m_sP;
      explicit TCSommet(ConstTCNoeudP pP) : m_pP(pP), m_sP(NUL) { }
      bool operator < (const TCSommet& o) const { return (m_pP < o.m_pP); }
   };
   struct TCArete
   {
      TCSommetI m_s1I, m_s2I;
      mutable GGDelaunayAreteP m_aP;
      explicit TCArete (TCSommetI s1I, TCSommetI s2I) : m_aP(NUL)
      {
         m_s1I = (s1I->m_pP < s2I->m_pP) ? s1I : s2I;
         m_s2I = (m_s1I == s1I) ? s2I : s1I;
      }
      bool operator < (const TCArete& o) const
      {
         return ((m_s1I->m_pP < o.m_s1I->m_pP) ||
                 (m_s1I->m_pP == o.m_s1I->m_pP && m_s2I->m_pP < o.m_s2I->m_pP));
      }
   };
   struct TCSurface
   {
      TCSommetI m_s1I, m_s2I, m_s3I;
      TCAreteI  m_a1I, m_a2I, m_a3I;
      mutable GGDelaunaySurfaceP m_sP;
      explicit TCSurface(TCSommetI s1I, TCSommetI s2I, TCSommetI s3I,
                         TCAreteI  a1I, TCAreteI  a2I, TCAreteI  a3I)
         : m_s1I(s1I), m_s2I(s2I), m_s3I(s3I), m_a1I(a1I), m_a2I(a2I), m_a3I(a3I), m_sP(NUL) {}
   };

   Action               m_action;
   TCSetSommets         m_sommets;
   TCSetAretes          m_aretes;
   GGMailleurDelaunayP  m_mailleurP;
   TCUniquer*           m_uniquerP;
   GGDelaunayInfoAreteP m_infoP;

   TCSommetI ajouteSommet      (const TCSommet&, GGDelaunayInfoBaseP = NULL);
   TCAreteI  ajouteArete       (const TCArete&,  GGDelaunayInfoBaseP = NULL);
   void      ajouteSurface     (const TCSurface&,GGDelaunayInfoBaseP = NULL);
   void      bloqueSommetPeau  (GGDelaunaySommetP);
   void      bloqueAretePeau   (GGDelaunayAreteP);
   void      filtreNoeudLimite(ConstTCNoeudP);
   void      relacheAnglePlat  (GGDelaunaySommetP, GGDelaunayAreteP);
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les
//    invariants de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
template <typename TTMaillage>
inline void AlgoTransfertMaillage<TTMaillage>::invariant(ConstCarP /*conditionP*/) const
{
}
#else
template <typename TTMaillage>
inline void AlgoTransfertMaillage<TTMaillage>::invariant(ConstCarP) const
{
}
#endif

#include "AlgoTransfertMaillage.hpp"

#endif // ifndef TRANSFERTMAILLAGE_H_DEJA_INCLU
