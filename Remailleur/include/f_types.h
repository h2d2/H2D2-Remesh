//************************************************************************
// --- Copyright (c) INRS 2010-2016
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// $Id$
//
// Description:
//
// Notes:
//************************************************************************
#ifndef F_TYPES_H_DEJA_INCLU
#define F_TYPES_H_DEJA_INCLU

// ---  Type pour les int
#if defined (__GNUC__)
#  include <stdint.h>
#elif defined (__INTEL_COMPILER)
#  include <stddef.h>
   typedef __int32 int32_t;
   typedef __int64 int64_t;
#elif defined (_MSC_VER)
#  include <stddef.h>
   typedef __int32 int32_t;
   typedef __int64 int64_t;
#elif defined (__SUNPRO_C) || defined (__SUNPRO_CC)
#  include <stdint.h>
#elif defined (__WATCOMC__)
#  include <stdint.h>
#else
#  error Configuring integer size - Unsupported compiler
#endif



#ifdef __cplusplus
extern "C"
{
#endif

#if defined(INRS_FORTRAN_INTEGER_SIZE)
#  if   (INRS_FORTRAN_INTEGER_SIZE == 4)
      typedef int32_t fint_t;
#  elif (INRS_FORTRAN_INTEGER_SIZE == 8)
      typedef int64_t fint_t;
#  else
#     error Unsupported INRS_FORTRAN_INTEGER_SIZE value. Expected values are [4, 8]
#  endif
#else
   typedef int fint_t;
#endif

typedef void (*TTInfoCb)(fint_t*, double*);

#ifdef __cplusplus
}
#endif

#endif  // F_TYPES_H_DEJA_INCLU
